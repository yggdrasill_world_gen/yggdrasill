/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/array.h>

#include <yggdrasill/commons.hpp>
#include <yggdrasill/error.hpp>
#include <yggdrasill/iterator.hpp>
#include <yggdrasill/string.hpp>
#include <yggdrasill/utils.hpp>

#include <string_view>
#include <type_traits>
#include <variant>

namespace yggdrasill {

	class Const_dictionary_view;
	class Dictionary_view;
	class Dictionary;

	/**An immutable view of a sequence container of elements of type T.
	 * 
	 * Objects of this type are managed internally by `Dictionary` objects and can't be constructed, copied or destroyed implicitly.
	 * 
	 * This type is a sequence container similar to std::vector, which means the elements are stored contiguously, pointers to elements can be used to
	 * access any other element in the array and appending elements might incur re-allocations (allocation a larger memory block and moving elements over),
	 * which also invalidates any pointers to elements. Which operations might invalidate pointers is also indicated on the methods below.
	 * 
	 * For arrays of unknown type ``T = void`` can be used. Also, in this case some operations, indicated below with ``requires(!std::is_void_v<T>)``, are not available.
	 * 
	 * \tparam T The element type. Should be one of the types listed in `Type`.
	 */
	template <typename T>
	class Const_array_view {
	  public:
		static_assert(!std::is_same_v<std::remove_cvref_t<T>, bool>,
		              "Array doesn't support bool. Use YGDL_Bool instead.");
		static_assert(!std::is_same_v<std::remove_cvref_t<T>, Dictionary>,
		              "Array doesn't support owning views of Dictionary.");
		static_assert(!std::is_reference_v<T>, "Array doesn't reference types.");

		static constexpr auto c_api_value_type = detail::c_api_type<T>; ///< \skip
		using const_view                       = Const_array_view<T>;   ///< \skip

		using difference_type = index_t; ///< Type to denote distance between two elements
		using size_type       = index_t; ///< Type used for indices and number of elements (e.g. `size()`)
		using value_type      = T;       ///< Type of the stored elements
		/// Type for constant references to stored elements. Either `const T&` or a view-wrapper-type that behaves like one.
		using const_reference =
		        std::conditional_t<detail::is_view_type<T>, detail::make_const<T>, std::add_lvalue_reference_t<const T>>;
		/// Type for references to stored elements. Identical to `const_reference` since the view is const.
		using reference = const_reference;
		/// Iterator type to constant elements. Either `const T*` or an iterator that wraps elements into the corresponding view-type
		using const_interator =
		        std::conditional_t<detail::is_view_type<T>, Wrapping_iterator<detail::make_const<T>>, const T*>;
		/// Iterator type to elements. Identical to `const_interator` since the view is const.
		using iterator = const_interator;

		/// Converting constructor from a reference to the C-API struct
		constexpr /*implicit*/ Const_array_view(const YGDL_Array& array) : array_(&array) {}
		Const_array_view(const YGDL_Array&&) = delete; ///< \skip forbid construction from temporary

		/// Converting constructor from array of derived type
		template <typename U>
		requires(!std::is_same_v<U, T> && std::is_base_of_v<T, U>)
		/*implicit*/ Const_array_view(Const_array_view<U> rhs) : array_(detail::c_ref(rhs))
		{
		}

		/// \returns The number of elements in the sequence
		[[nodiscard]] constexpr size_type size() const noexcept { return YGDL_array_size(c_ptr(*this)); }
		/// \returns true, if the sequence contains no elements
		[[nodiscard]] constexpr bool empty() const noexcept { return size() == 0; }
		/// \returns The enum value that represents the concrete type of the sequences elements
		[[nodiscard]] constexpr Type type() const noexcept
		{
			return static_cast<Type>(YGDL_array_type(c_ptr(*this)));
		}

		/// \returns An iterator/pointer to first element
		[[nodiscard]] const_interator begin() const requires(!std::is_void_v<T>)
		{
			return const_interator{data()};
		}
		/// \returns An iterator/pointer to first element
		[[nodiscard]] const_interator cbegin() const requires(!std::is_void_v<T>) { return begin(); }
		/// \returns An iterator/pointer to one past the last element
		[[nodiscard]] const_interator end() const requires(!std::is_void_v<T>) { return begin() + size(); }
		/// \returns An iterator/pointer to one past the last element
		[[nodiscard]] const_interator cend() const requires(!std::is_void_v<T>) { return end(); }

		/// \returns The element stored at `index`
		/// \throws Out_of_bounds_exception if the `index` is invalid
		[[nodiscard]] const_reference operator[](index_t index) const requires(!std::is_void_v<T>);

		/// \returns A pointer to first element
		[[nodiscard]] auto data() const requires(!std::is_void_v<T>)
		{
			if constexpr(detail::is_view_type<T>)
				return c_ptr((*this)[0]);
			else
				return &(*this)[0];
		}

		/**\returns A typed reference to this sequence
		 * \throws Type_mismatch_exception If the underlying type doesn't match the requested one
		 * \tparam U The type stored in the sequence, i.e. the equivalent C++ type for the enum value returned by `type()`. See `Type` for a list of valid types.
		 */
		template <typename U>
		constexpr Const_array_view<U> cast_to() const;

		/// Checks the underlying type and calls `visitor` with a correctly cast `Array` reference.
		/// \tparam Visitor A type that is invokable with (at least) a reference to any of the types listed in `Type`
		template <typename Visitor>
		void visit_value_types(Visitor&& visitor) const;
		template <typename... Visitors>
		void visit_value_types(Visitors&&... visitors) const requires(sizeof...(Visitors) > 1);

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
		 * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
		 */
		[[nodiscard]] constexpr friend const YGDL_Array* c_ptr(Const_array_view a) { return a.array_; }
		[[nodiscard]] constexpr friend const YGDL_Array* c_ptr(const Const_array_view* a)
		{
			return a ? a->array_ : nullptr;
		}

	  private:
		const YGDL_Array* array_;
	};

	/// A mutable non-owning view of a sequence container of elements of type T.
	/// \see `Const_array_view` for details about the general structure.
	template <typename T>
	class Array_view : public Const_array_view<T> {
	  public:
		using const_view = Const_array_view<T>; ///< \skip

		/// Type for references to stored elements. Either `T&` or a view-wrapper-type that behaves like one.
		using reference = std::conditional_t<detail::is_view_type<T>, T, std::add_lvalue_reference_t<T>>;
		/// Iterator type to elements. Either `T*` or an iterator that wraps elements into the corresponding view-type
		using iterator = std::conditional_t<detail::is_view_type<T>, Wrapping_iterator<T>, T*>;

		/// Converting constructor from a reference to the C-API struct
		constexpr /*implicit*/ Array_view(YGDL_Array& array) : Const_array_view<T>(array) {}
		Array_view(const YGDL_Array&&) = delete; ///< \skip forbid construction from temporary

		/// Converting constructor from array of derived type
		template <typename U>
		requires(!std::is_same_v<U, T> && std::is_base_of_v<T, U>)
		constexpr /*implicit*/ Array_view(Array_view<U> rhs) : Array_view(detail::c_ref(rhs))
		{
		}

		/// \returns An iterator/pointer to first element
		[[nodiscard]] iterator begin() requires(!std::is_void_v<T>) { return iterator{data()}; }
		using const_view::begin;
		/// \returns An iterator/pointer to one past the last element
		[[nodiscard]] iterator end() requires(!std::is_void_v<T>) { return begin() + this->size(); }
		using const_view::end;

		/// \returns The element stored at `index`
		/// \throws Out_of_bounds_exception if the `index` is invalid
		[[nodiscard]] reference operator[](index_t index) requires(!std::is_void_v<T>);
		using const_view::      operator[];

		/// \returns A pointer to first element
		[[nodiscard]] auto data() requires(!std::is_void_v<T>)
		{
			if constexpr(detail::is_view_type<T>)
				return c_ptr((*this)[0]);
			else
				return &(*this)[0];
		}
		using const_view::data;

		/// Erases all elements stored in the sequence
		void clear() { YGDL_array_clear(c_ptr(this)); }
		/// Reserves space, so that at least `minimum_capacity` elements can be stored without requiring a reallocation
		/// \note Invalidates all pointers to the elements
		void reserve(index_t minimum_capacity);
		/// Changes the size of the sequence by either erasing or appending value-initialized values at the end until `size()==size`
		/// \note Invalidates all pointers to the elements
		void resize(index_t size);

		/**Appends a new element, constructing it in-place
		 * \overload_specific
		 * \param value The element is constructed by calling `T(std::forward<U>(value))`
		 */
		template <typename U>
		reference push_back(U&& value) requires(!std::is_void_v<T>);
		reference push_back() requires(!std::is_void_v<T>);
		void      push_back() requires(std::is_void_v<T>);

		/**Removes the given element.
		 * \note Invalidates pointers to the last element. Pointers to elements after the erased one will reference different but valid objects afterwards
		 * \pre `type()` has to be a valid type other than `YGDL_TYPE_NONE`
		 * \overload_specific
		 * \pre The behaviour is undefined if `ptr` doesn't point to a valid element of this sequence
		 */
		void erase_ordered(const T* ptr) requires(!std::is_void_v<T>) { erase_ordered(find_index(ptr)); }
		/// \overload_specific
		/// \throws Out_of_bounds_exception If the index is not valid
		void erase_ordered(index_t index) { YGDL_array_erase_ordered(c_ptr(this), index, Error_throw{}); }
		/**Removes the given element.
		 * \note Invalidates pointers to the last element. Pointers to elements after the erased one will reference different but valid objects afterwards
		 * \pre `type()` has to be a valid type other than `YGDL_TYPE_NONE`
		 * \overload_specific
		 * \pre The behaviour is undefined if `ptr` doesn't point to a valid element of this sequence
		 */
		void erase_unordered(const T* ptr) requires(!std::is_void_v<T>) { erase_unordered(find_index(ptr)); }
		/// \overload_specific
		/// \throws Out_of_bounds_exception If the index is not valid
		void erase_unordered(index_t index) { YGDL_array_erase_unordered(c_ptr(this), index, Error_throw{}); }

		/**\returns A typed reference to this sequence
		 * \throws Type_mismatch_exception If the underlying type doesn't match the requested one
		 * \tparam U The type stored in the sequence, i.e. the equivalent C++ type for the enum value returned by `type()`. See `Type` for a list of valid types.
		 */
		template <typename U>
		constexpr Array_view<U> cast_to()
		{
			return {const_cast<YGDL_Array&>(detail::c_ref(Const_array_view<T>::template cast_to<T>()))};
		}
		using const_view::cast_to;

		/// Checks the underlying type and calls `visitor` with a correctly cast `Array` reference.
		/// \tparam Visitor A type that is invokable with (at least) a reference to any of the types listed in `Type`
		template <typename Visitor>
		void visit_value_types(Visitor&& visitor);
		template <typename... Visitors>
		void visit_value_types(Visitors&&... visitors) requires(sizeof...(Visitors) > 1);
		using const_view::visit_value_types;

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
		 * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
		 */
		[[nodiscard]] constexpr friend YGDL_Array* c_ptr(Array_view a)
		{
			// we can safely cast away const here, because (inside the Array_view class) we know that array_ was initialized from a pointer to non-const
			return const_cast<YGDL_Array*>(c_ptr(static_cast<const Const_array_view<T>&>(a)));
		}
		[[nodiscard]] constexpr friend YGDL_Array* c_ptr(Array_view* a)
		{
			return const_cast<YGDL_Array*>(c_ptr(static_cast<const Const_array_view<T>*>(a)));
		}

	  private:
		index_t find_index(const T* ptr);
	};

	/// A mutable non-owning reference to a sequence container of elements of type T.
	/// Same as `Array_view`, except assignments don't rebind (like pointers) but instead replace the value of the referenced object (like references).
	template <typename T>
	class Array_ref : public Array_view<T> {
	  public:
		using const_view = Const_array_view<T>; ///< \skip

		/// Converting constructor from a reference to the C-API struct
		constexpr /*implicit*/ Array_ref(YGDL_Array& array) : Array_view<T>(array) {}
		Array_ref(const YGDL_Array&&) = delete; ///< \skip forbid construction from temporary

		/// Converting constructor from array of derived type
		template <typename U>
		requires(!std::is_same_v<U, T> && std::is_base_of_v<T, U>)
		constexpr /*implicit*/ Array_ref(Array_ref<U> rhs) : Array_ref(detail::c_ref(rhs))
		{
		}

		/**\returns A typed reference to this sequence
		 * \throws Type_mismatch_exception If the underlying type doesn't match the requested one
		 * \tparam U The type stored in the sequence, i.e. the equivalent C++ type for the enum value returned by `type()`. See `Type` for a list of valid types.
		 */
		template <typename U>
		constexpr Array_ref<U> cast_to()
		{
			return {detail::c_ref(Array_view<T>::template cast_to<T>())};
		}
		using const_view::cast_to;

		/// Checks the underlying type and calls `visitor` with a correctly cast `Array` reference.
		/// \tparam Visitor A type that is invokable with (at least) a reference to any of the types listed in `Type`
		template <typename Visitor>
		void visit_value_types(Visitor&& visitor);
		using const_view::visit_value_types;

		/**Clears this sequence and copies all elements in the passed container
		 * \tparam U A container type, providing at least a const begin and end function, with elements of
		 * type `U::value_type`, that are assignable to `T`
		 */
		template <typename U>
		Array_ref<T>& operator=(const U& data) requires(!std::is_void_v<T>);
	};

} // namespace yggdrasill

#define YGDL_ARRAY_HPP_INCLUDED
#include <yggdrasill/array.inl>
