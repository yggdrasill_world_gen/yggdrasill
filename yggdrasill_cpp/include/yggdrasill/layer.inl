/// NOTE: This is the template implementation header for layer.hpp and should not be included directly!
#pragma once

#ifndef YGDL_LAYER_HPP_INCLUDED
#include <yggdrasill/layer.hpp>
#endif

namespace yggdrasill {

	namespace detail {
		constexpr inline bool is_edge(Ref_type type)
		{
			return (static_cast<YGDL_Layer_ref_type>(type) & 0b100) != 0;
		}
		constexpr inline bool is_primal_edge(Ref_type type)
		{
			return (static_cast<YGDL_Layer_ref_type>(type) & 0b110) == 0b100;
		}
		constexpr inline bool is_directed_edge(Ref_type type)
		{
			return (static_cast<YGDL_Layer_ref_type>(type) & 0b101) == 0b101;
		}
		constexpr inline bool is_undirected_edge(Ref_type type)
		{
			return is_edge(type) && !is_directed_edge(type);
		}

		template <Ref_type RefType>
		constexpr index_t to_index(typename Key_type<RefType>::type key) noexcept
		{
			auto index = key.index();

			if constexpr(is_edge(RefType) && !is_directed_edge(RefType)) {
				index = index >> 1;
			}

			return index;
		}

		template <typename T>
		struct base_type {
			using type = T;
		};
		template <typename T>
		requires std::is_enum_v<T>
		struct base_type<T> {
			using type = std::underlying_type_t<T>;
		};
		template <typename T>
		using base_type_t = typename base_type<T>::type;

		using layer_value_info = decltype(YGDL_Layer_definition::value_info);

		template <Type DataType>
		constexpr layer_value_info create_layer_value_info()
		{
			if constexpr(DataType == Type::bool_t) {
				return {.bool_value = {.initial = YGDL_FALSE}};
			} else if constexpr(DataType == Type::int8_t) {
				return {.int8_value = {.min     = std::numeric_limits<int8_t>::lowest(),
				                       .initial = 0,
				                       .max     = std::numeric_limits<int8_t>::max()}};

			} else if constexpr(DataType == Type::int32_t) {
				return {.int32_value = {.min     = std::numeric_limits<int32_t>::lowest(),
				                        .initial = 0,
				                        .max     = std::numeric_limits<int32_t>::max()}};

			} else if constexpr(DataType == Type::int64_t) {
				return {.int64_value = {.min     = std::numeric_limits<int64_t>::lowest(),
				                        .initial = 0,
				                        .max     = std::numeric_limits<int64_t>::max()}};

			} else if constexpr(DataType == Type::float_t) {
				return {.float_value = {.min       = std::numeric_limits<float>::lowest(),
				                        .initial   = 0.f,
				                        .max       = std::numeric_limits<float>::max(),
				                        .has_min   = false,
				                        .has_max   = false,
				                        .allow_nan = false,
				                        .allow_inf = false}};

			} else if constexpr(DataType == Type::double_t) {
				return {.double_value = {.min       = std::numeric_limits<double>::lowest(),
				                         .initial   = 0.0,
				                         .max       = std::numeric_limits<double>::max(),
				                         .has_min   = false,
				                         .has_max   = false,
				                         .allow_nan = false,
				                         .allow_inf = false}};

			} else if constexpr(DataType == Type::vec2_t) {
				return {.vec2_value = {.min            = YGDL_Vec2{std::numeric_limits<float>::lowest(),
                                                        std::numeric_limits<float>::lowest()},
				                       .initial        = YGDL_Vec2{0.f, 0.f},
				                       .max            = YGDL_Vec2{std::numeric_limits<float>::max(),
                                                        std::numeric_limits<float>::max()},
				                       .min_length     = 0.f,
				                       .max_length     = std::numeric_limits<float>::max(),
				                       .has_min        = false,
				                       .has_max        = false,
				                       .has_min_length = false,
				                       .has_max_length = false,
				                       .allow_nan      = false,
				                       .allow_inf      = false}};

			} else if constexpr(DataType == Type::vec3_t) {
				return {.vec3_value = {.min            = YGDL_Vec3{std::numeric_limits<float>::lowest(),
                                                        std::numeric_limits<float>::lowest(),
                                                        std::numeric_limits<float>::lowest()},
				                       .initial        = YGDL_Vec3{0.f, 0.f, 0.f},
				                       .max            = YGDL_Vec3{std::numeric_limits<float>::max(),
                                                        std::numeric_limits<float>::max(),
                                                        std::numeric_limits<float>::max()},
				                       .min_length     = 0.f,
				                       .max_length     = std::numeric_limits<float>::max(),
				                       .has_min        = false,
				                       .has_max        = false,
				                       .has_min_length = false,
				                       .has_max_length = false,
				                       .allow_nan      = false,
				                       .allow_inf      = false}};

			} else {
				static_assert(DataType == DataType,
				              "Invalid DataType passed to YGDL_Layer_definition_create()");
			}
		}

		// duplicated here from world.cpp because it needs to be constexpr
		template <Type DataType>
		constexpr YGDL_Layer_definition create_layer_definition(YGDL_String_view id, YGDL_Layer_ref_type_t ref_type)
		{
			constexpr auto vi = create_layer_value_info<DataType>();

			return YGDL_Layer_definition{.id            = id,
			                             .description   = detail::to_ygdl_string_view(""),
			                             .data_type     = static_cast<YGDL_Type_t>(DataType),
			                             .ref_type      = ref_type,
			                             .invalidation  = YGDL_LAYER_INVALIDATION_RESET_AFFECTED,
			                             .interpolation = YGDL_LAYER_INTERPOLATION_RESET_AFFECTED,
			                             .value_info    = vi};
		}

	} // namespace detail

	// IMPLEMENTATION: Const_layer_view
	template <typename T, Ref_type RefType>
	Layer_definition<T, RefType> Const_layer_view<T, RefType>::definition() const
	{
		return Layer_definition<T, RefType>{*detail::c_ref(this).definition};
	}

	template <typename T, Ref_type RefType>
	[[nodiscard]] constexpr auto Const_layer_view<T, RefType>::data() const -> value_type*
	{
		auto* ptr = static_cast<const T*>(detail::c_ref(this).data);
		assert((reinterpret_cast<std::uintptr_t>(ptr) % 64) == 0);
		return std::assume_aligned<64>(ptr);
	}

	template <typename T, Ref_type RefType>
	constexpr const T& Const_layer_view<T, RefType>::operator[](key_type e) const
	{
		const auto index = detail::to_index<RefType>(e);
		if(e == key_type{} || index >= size())
			throw Out_of_bounds_exception(detail::concat("The given key ", e, " is not valid."));

		return begin()[index];
	}
	template <typename T, Ref_type RefType>
	constexpr const T& Const_layer_view<T, RefType>::operator[](Unchecked<key_type> e) const noexcept
	{
		return begin()[detail::to_index<RefType>(e)];
	}


	// IMPLEMENTATION: Layer_view
	template <typename T, Ref_type RefType>
	constexpr auto Layer_view<T, RefType>::data() -> value_type*
	{
		return const_cast<T*>(const_view::data());
	}
	template <typename T, Ref_type RefType>
	constexpr auto Layer_view<T, RefType>::begin() -> value_type*
	{
		return const_cast<T*>(const_view::begin());
	}
	template <typename T, Ref_type RefType>
	constexpr auto Layer_view<T, RefType>::end() -> value_type*
	{
		return const_cast<T*>(const_view::end());
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_view<T, RefType>::operator[](key_type e) -> value_type&
	{
		return const_cast<T&>(const_view::operator[](e));
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_view<T, RefType>::operator[](Unchecked<key_type> e) noexcept -> value_type&
	{
		return const_cast<T&>(const_view::operator[](e));
	}


	// IMPLEMENTATION: Layer_definition
	template <typename T, Ref_type RefType>
	constexpr Layer_definition<T, RefType>::Layer_definition(std::string_view id)
	  : info_{detail::create_layer_definition<static_cast<Type>(detail::c_api_type<T>)>(
	          detail::to_ygdl_string_view(id), static_cast<YGDL_Layer_ref_type_t>(ref_type))}
	{
	}
	template <typename T, Ref_type RefType>
	constexpr Layer_definition<T, RefType>::Layer_definition(const YGDL_Layer_definition& c_type)
	  : info_(c_type)
	{
		assert(c_type.data_type == static_cast<YGDL_Type_t>(data_type()));
		assert(c_type.ref_type == static_cast<YGDL_Layer_ref_type_t>(ref_type));
	}

	template <typename T, Ref_type RefType>
	constexpr std::string_view Layer_definition<T, RefType>::id() const
	{
		return detail::from_ygdl_string_view(info_.id);
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::description(std::string_view description) -> Layer_definition&
	{
		info_.description = detail::to_ygdl_string_view(description);
		return *this;
	}
	template <typename T, Ref_type RefType>
	constexpr std::string_view Layer_definition<T, RefType>::description() const
	{
		return detail::from_ygdl_string_view(info_.description);
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::min(value_type min) -> Layer_definition&
	{
		using base_t = detail::base_type_t<T>;
		if constexpr(std::is_same_v<base_t, YGDL_Bool>) {
			assert(!"Layer_definition<bool, RefType>::min() is not a valid operation");
		} else if constexpr(std::is_same_v<base_t, std::int8_t>) {
			info_.value_info.int8_value.min = static_cast<base_t>(min);
		} else if constexpr(std::is_same_v<base_t, std::int32_t>) {
			info_.value_info.int32_value.min = static_cast<base_t>(min);
		} else if constexpr(std::is_same_v<base_t, std::int64_t>) {
			info_.value_info.int64_value.min = static_cast<base_t>(min);
		} else if constexpr(std::is_same_v<base_t, float>) {
			info_.value_info.float_value.has_min = true;
			info_.value_info.float_value.min     = min;
		} else if constexpr(std::is_same_v<base_t, double>) {
			info_.value_info.double_value.has_min = true;
			info_.value_info.double_value.min     = min;
		} else if constexpr(std::is_same_v<base_t, Vec2>) {
			info_.value_info.vec2_value.has_min = true;
			info_.value_info.vec2_value.min     = min;
		} else if constexpr(std::is_same_v<base_t, Vec3>) {
			info_.value_info.vec3_value.has_min = true;
			info_.value_info.vec3_value.min     = min;
		} else {
			assert(!"Unexpected type T in Layer_definition<T, RefType>");
		}
		return *this;
	}
	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::min() const -> std::optional<value_type>
	{
		using base_t = detail::base_type_t<T>;
		if constexpr(std::is_same_v<base_t, YGDL_Bool>) {
			return std::nullopt;
		} else if constexpr(std::is_same_v<base_t, std::int8_t>) {
			return static_cast<value_type>(info_.value_info.int8_value.min);
		} else if constexpr(std::is_same_v<base_t, std::int32_t>) {
			return static_cast<value_type>(info_.value_info.int32_value.min);
		} else if constexpr(std::is_same_v<base_t, std::int64_t>) {
			return static_cast<value_type>(info_.value_info.int64_value.min);
		} else if constexpr(std::is_same_v<base_t, float>) {
			return info_.value_info.float_value.has_min ? std::optional(info_.value_info.float_value.min)
			                                            : std::nullopt;
		} else if constexpr(std::is_same_v<base_t, double>) {
			return info_.value_info.double_value.has_min ? std::optional(info_.value_info.double_value.min)
			                                             : std::nullopt;
		} else if constexpr(std::is_same_v<base_t, Vec2>) {
			return info_.value_info.vec2_value.has_min ? std::optional(info_.value_info.vec2_value.min)
			                                           : std::nullopt;
		} else if constexpr(std::is_same_v<base_t, Vec3>) {
			return info_.value_info.vec3_value.has_min ? std::optional(info_.value_info.vec3_value.min)
			                                           : std::nullopt;
		} else {
			return std::nullopt;
		}
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::initial(value_type initial) -> Layer_definition&
	{
		using base_t = detail::base_type_t<T>;
		if constexpr(std::is_same_v<base_t, YGDL_Bool>) {
			info_.value_info.bool_value.initial = initial;
		} else if constexpr(std::is_same_v<base_t, std::int8_t>) {
			info_.value_info.int8_value.initial = static_cast<base_t>(initial);
		} else if constexpr(std::is_same_v<base_t, std::int32_t>) {
			info_.value_info.int32_value.initial = static_cast<base_t>(initial);
		} else if constexpr(std::is_same_v<base_t, std::int64_t>) {
			info_.value_info.int64_value.initial = static_cast<base_t>(initial);
		} else if constexpr(std::is_same_v<base_t, float>) {
			info_.value_info.float_value.initial = initial;
		} else if constexpr(std::is_same_v<base_t, double>) {
			info_.value_info.double_value.initial = initial;
		} else if constexpr(std::is_same_v<base_t, Vec2>) {
			info_.value_info.vec2_value.initial = initial;
		} else if constexpr(std::is_same_v<base_t, Vec3>) {
			info_.value_info.vec3_value.initial = initial;
		} else {
			assert(!"Unexpected type T in Layer_definition<T, RefType>");
		}
		return *this;
	}
	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::initial() const -> value_type
	{
		using base_t = detail::base_type_t<T>;
		if constexpr(std::is_same_v<base_t, YGDL_Bool>) {
			return info_.value_info.bool_value.initial;
		} else if constexpr(std::is_same_v<base_t, std::int8_t>) {
			return static_cast<value_type>(info_.value_info.int8_value.initial);
		} else if constexpr(std::is_same_v<base_t, std::int32_t>) {
			return static_cast<value_type>(info_.value_info.int32_value.initial);
		} else if constexpr(std::is_same_v<base_t, std::int64_t>) {
			return static_cast<value_type>(info_.value_info.int64_value.initial);
		} else if constexpr(std::is_same_v<base_t, float>) {
			return info_.value_info.float_value.initial;
		} else if constexpr(std::is_same_v<base_t, double>) {
			return info_.value_info.double_value.initial;
		} else if constexpr(std::is_same_v<base_t, Vec2>) {
			return info_.value_info.vec2_value.initial;
		} else if constexpr(std::is_same_v<base_t, Vec3>) {
			return info_.value_info.vec3_value.initial;
		} else {
			assert(!"Unexpected type T in Layer_definition<T, RefType>");
			return value_type{};
		};
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::max(value_type max) -> Layer_definition&
	{
		using base_t = detail::base_type_t<T>;
		if constexpr(std::is_same_v<base_t, YGDL_Bool>) {
			assert(!"Layer_definition<bool, RefType>::max() is not a valid operation");
		} else if constexpr(std::is_same_v<base_t, std::int8_t>) {
			info_.value_info.int8_value.max = static_cast<base_t>(max);
		} else if constexpr(std::is_same_v<base_t, std::int32_t>) {
			info_.value_info.int32_value.max = static_cast<base_t>(max);
		} else if constexpr(std::is_same_v<base_t, std::int64_t>) {
			info_.value_info.int64_value.max = static_cast<base_t>(max);
		} else if constexpr(std::is_same_v<base_t, float>) {
			info_.value_info.float_value.has_max = true;
			info_.value_info.float_value.max     = max;
		} else if constexpr(std::is_same_v<base_t, double>) {
			info_.value_info.double_value.has_max = true;
			info_.value_info.double_value.max     = max;
		} else if constexpr(std::is_same_v<base_t, Vec2>) {
			info_.value_info.vec2_value.has_max = true;
			info_.value_info.vec2_value.max     = max;
		} else if constexpr(std::is_same_v<base_t, Vec3>) {
			info_.value_info.vec3_value.has_max = true;
			info_.value_info.vec3_value.max     = max;
		} else {
			assert(!"Unexpected type T in Layer_definition<T, RefType>");
		}
		return *this;
	}
	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::max() const -> std::optional<value_type>
	{
		using base_t = detail::base_type_t<T>;
		if constexpr(std::is_same_v<base_t, YGDL_Bool>) {
			return std::nullopt;
		} else if constexpr(std::is_same_v<base_t, std::int8_t>) {
			return static_cast<value_type>(info_.value_info.int8_value.max);
		} else if constexpr(std::is_same_v<base_t, std::int32_t>) {
			return static_cast<value_type>(info_.value_info.int32_value.max);
		} else if constexpr(std::is_same_v<base_t, std::int64_t>) {
			return static_cast<value_type>(info_.value_info.int64_value.max);
		} else if constexpr(std::is_same_v<base_t, float>) {
			return info_.value_info.float_value.has_max ? std::optional(info_.value_info.float_value.max)
			                                            : std::nullopt;
		} else if constexpr(std::is_same_v<base_t, double>) {
			return info_.value_info.double_value.has_max ? std::optional(info_.value_info.double_value.max)
			                                             : std::nullopt;
		} else if constexpr(std::is_same_v<base_t, Vec2>) {
			return info_.value_info.vec2_value.has_max ? std::optional(info_.value_info.vec2_value.min)
			                                           : std::nullopt;
		} else if constexpr(std::is_same_v<base_t, Vec3>) {
			return info_.value_info.vec3_value.has_max ? std::optional(info_.value_info.vec3_value.max)
			                                           : std::nullopt;
		} else {
			return std::nullopt;
		}
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::min_length(float min) -> Layer_definition&
	{
		if constexpr(std::is_same_v<T, Vec2>) {
			info_.value_info.vec2_value.has_min_length = true;
			info_.value_info.vec2_value.min_length     = min;
		} else if constexpr(std::is_same_v<T, Vec3>) {
			info_.value_info.vec3_value.has_min_length = true;
			info_.value_info.vec3_value.min_length     = min;
		} else {
			assert(!"Layer_definition<bool, RefType>::min_length() is only supported for vector types");
		}
		return *this;
	}
	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::min_length() const -> std::optional<float>
	{
		if constexpr(std::is_same_v<T, Vec2>) {
			return info_.value_info.vec2_value.has_min_length
			               ? std::optional(info_.value_info.vec2_value.min_length)
			               : std::nullopt;
		} else if constexpr(std::is_same_v<T, Vec3>) {
			return info_.value_info.vec3_value.has_min_length
			               ? std::optional(info_.value_info.vec3_value.min_length)
			               : std::nullopt;
		} else {
			assert(!"Layer_definition<bool, RefType>::min_length() is only supported for vector types");
			return std::nullopt;
		}
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::max_length(float max) -> Layer_definition&
	{
		if constexpr(std::is_same_v<T, Vec2>) {
			info_.value_info.vec2_value.has_max_length = true;
			info_.value_info.vec2_value.max_length     = max;
		} else if constexpr(std::is_same_v<T, Vec3>) {
			info_.value_info.vec3_value.has_max_length = true;
			info_.value_info.vec3_value.max_length     = max;
		} else {
			assert(!"Layer_definition<bool, RefType>::max_length() is only supported for vector types");
		}
		return *this;
	}
	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::max_length() const -> std::optional<float>
	{
		if constexpr(std::is_same_v<T, Vec2>) {
			return info_.value_info.vec2_value.has_max_length
			               ? std::optional(info_.value_info.vec2_value.max_length)
			               : std::nullopt;
		} else if constexpr(std::is_same_v<T, Vec3>) {
			return info_.value_info.vec3_value.has_max_length
			               ? std::optional(info_.value_info.vec3_value.max_length)
			               : std::nullopt;
		} else {
			assert(!"Layer_definition<bool, RefType>::max_length() is only supported for vector types");
			return std::nullopt;
		}
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::allow_nan(bool allow) -> Layer_definition&
	{
		if constexpr(std::is_same_v<T, float>) {
			info_.value_info.float_value.allow_nan = allow;
		} else if constexpr(std::is_same_v<T, double>) {
			info_.value_info.double_value.allow_nan = allow;
		} else if constexpr(std::is_same_v<T, Vec2>) {
			info_.value_info.vec2_value.allow_nan = allow;
		} else if constexpr(std::is_same_v<T, Vec3>) {
			info_.value_info.vec3_value.allow_nan = allow;
		} else {
			assert(!"Layer_definition<bool, RefType>::allow_nan() is only supported for float types");
		}
		return *this;
	}
	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::allow_nan() const -> bool
	{
		if constexpr(std::is_same_v<T, float>) {
			return info_.value_info.float_value.allow_nan;
		} else if constexpr(std::is_same_v<T, double>) {
			return info_.value_info.double_value.allow_nan;
		} else if constexpr(std::is_same_v<T, Vec2>) {
			return info_.value_info.vec2_value.allow_nan;
		} else if constexpr(std::is_same_v<T, Vec3>) {
			return info_.value_info.vec3_value.allow_nan;
		} else {
			assert(!"Layer_definition<bool, RefType>::allow_nan() is only supported for float types");
			return false;
		}
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::allow_inf(bool allow) -> Layer_definition&
	{
		if constexpr(std::is_same_v<T, float>) {
			info_.value_info.float_value.allow_inf = allow;
		} else if constexpr(std::is_same_v<T, double>) {
			info_.value_info.double_value.allow_inf = allow;
		} else if constexpr(std::is_same_v<T, Vec2>) {
			info_.value_info.vec2_value.allow_inf = allow;
		} else if constexpr(std::is_same_v<T, Vec3>) {
			info_.value_info.vec3_value.allow_inf = allow;
		} else {
			assert(!"Layer_definition<bool, RefType>::allow_inf() is only supported for float types");
		}
		return *this;
	}
	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::allow_inf() const -> bool
	{
		if constexpr(std::is_same_v<T, float>) {
			return info_.value_info.float_value.allow_inf;
		} else if constexpr(std::is_same_v<T, double>) {
			return info_.value_info.double_value.allow_inf;
		} else if constexpr(std::is_same_v<T, Vec2>) {
			return info_.value_info.vec2_value.allow_inf;
		} else if constexpr(std::is_same_v<T, Vec3>) {
			return info_.value_info.vec3_value.allow_inf;
		} else {
			assert(!"Layer_definition<bool, RefType>::allow_inf() is only supported for float types");
			return false;
		}
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::data_type() const -> Type
	{
		return static_cast<Type>(detail::c_api_type<T>);
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::invalidation() const -> Invalidation
	{
		return static_cast<Invalidation>(info_.invalidation);
	}
	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::invalidation(Invalidation v) -> Layer_definition&
	{
		info_.invalidation = static_cast<YGDL_Layer_invalidation_t>(v);
		return *this;
	}

	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::interpolation() const -> Interpolation
	{
		return static_cast<Interpolation>(info_.interpolation);
	}
	template <typename T, Ref_type RefType>
	constexpr auto Layer_definition<T, RefType>::interpolation(Interpolation v) -> Layer_definition&
	{
		info_.interpolation = static_cast<YGDL_Layer_interpolation_t>(v);
		return *this;
	}


	// IMPLEMENTATION: Tmp_layer
	namespace detail {
		template <typename T, Ref_type RefType>
		const auto* create_dummy_layer_def()
		{
			if constexpr(detail::can_be_stored_in_layer<T>) {
				static const auto def =
				        detail::create_layer_definition<static_cast<Type>(detail::c_api_type<T>)>(
				                detail::to_ygdl_string_view("__temporary_layer__"),
				                static_cast<YGDL_Layer_ref_type_t>(RefType));
				return &def;
			} else {
				// Temporary layers are not managed by the system and don't touch the C-layer.
				// So they can support more value types, than the restricted set supported by normal layers.
				// But for these we have to provide a different dummy definition and Const_layer_view::info() may not be called!
				static const auto def = detail::create_layer_definition<Type::int8_t>(
				        detail::to_ygdl_string_view("__temporary_layer__"),
				        static_cast<YGDL_Layer_ref_type_t>(RefType));
				return &def;
			}
		}
	} // namespace detail
	template <typename T, Ref_type RefType>
	Temporary_layer<T, RefType>::Temporary_layer()
	  : Layer_view<T, RefType>(dummy_layer_)
	  , dummy_layer_{detail::create_dummy_layer_def<T, RefType>(), nullptr, 0}
	{
	}
	template <typename T, Ref_type RefType>
	void Temporary_layer<T, RefType>::reset(Const_mesh_view mesh, const T& v)
	{
		data_.clear();
		data_.resize(get_size(mesh), v);
		on_reset();
	}

	template <typename T, Ref_type RefType>
	template <std::invocable<Layer_key_type<RefType>> Factory>
	void Temporary_layer<T, RefType>::reset(Const_mesh_view mesh, Factory&& value_factory)
	{
		const auto size = get_size(mesh);

		data_.clear();
		data_.reserve(size);

		constexpr auto step = detail::is_undirected_edge(RefType) ? 2 : 1;

		for(auto i = index_t(0); i < size; i++) {
			if(auto e = typename const_view::key_type(i * step); e.valid(mesh))
				data_.emplace_back(std::invoke(value_factory, e));
			else
				data_.emplace_back();
		}

		on_reset();
	}

	template <typename T, Ref_type RefType>
	void Temporary_layer<T, RefType>::on_reset()
	{
		// update data in dummy YGDL_Layer object
		dummy_layer_.data   = data_.data();
		dummy_layer_.length = static_cast<index_t>(data_.size());

		// update pointer used by inherited view functions
		Layer_view<T, RefType>::operator=(Layer_view<T, RefType>(dummy_layer_));
	}

	template <typename T, Ref_type RefType>
	index_t Temporary_layer<T, RefType>::get_size(Const_mesh_view mesh)
	{
		if constexpr(RefType == Ref_type::vertex) {
			return mesh.vertex_max_index() + 1;

		} else if constexpr(RefType == Ref_type::face) {
			return mesh.face_max_index() + 1;

		} else if constexpr(detail::is_directed_edge(RefType)) {
			return mesh.edge_max_index() + 1;

		} else {
			return (mesh.edge_max_index() / 2) + 1;
		}
	}


	// IMPLEMENTATION: range / unchecked_range
	template <typename... Layers>
	requires(joinable_layers<Layers...>)
	[[nodiscard]] auto range(Const_mesh_view mesh, Layers... layers)
	{
		constexpr Ref_type ref_type = detail::first<Layers...>::ref_type;
		using Key                   = Layer_key_type<ref_type>;
		using RT                    = std::tuple<Key, typename Layers::value_type&...>;

		const auto size = [](const auto& first, const auto&...) { return first.size(); }(layers...);
		(void) size;
		assert(((size == layers.size()) && ...));

		auto body = [=](Key key) mutable {
			return [&](auto& __restrict... l) { return RT{key, l...}; }(layers[Unchecked{key}]...);
		};

		if constexpr(ref_type == Ref_type::vertex) {
			return map_range(mesh.vertices(), std::move(body));

		} else if constexpr(ref_type == Ref_type::face) {
			return map_range(mesh.faces(), std::move(body));

		} else if constexpr(ref_type == Ref_type::edge_primal) {
			return map_range(mesh.undirected_edges<Edge_type::primal>(), std::move(body));

		} else if constexpr(ref_type == Ref_type::edge_primal_directed) {
			return map_range(mesh.directed_edges<Edge_type::primal>(), std::move(body));

		} else if constexpr(ref_type == Ref_type::edge_dual) {
			return map_range(mesh.undirected_edges<Edge_type::dual>(), std::move(body));

		} else if constexpr(ref_type == Ref_type::edge_dual_directed) {
			return map_range(mesh.directed_edges<Edge_type::dual>(), std::move(body));
		}
	}


	namespace detail {
		template <typename IterTuple, size_t... Is>
		struct Join_iterator {
			IterTuple iters;

			constexpr Join_iterator(IterTuple iters, std::index_sequence<Is...>) : iters(std::move(iters)) {}

			constexpr bool operator==(const Join_iterator& rhs) const
			{
				return std::get<0>(iters) == std::get<0>(rhs.iters);
			}

			constexpr auto operator*() const
			{
				return [&](auto& __restrict... l) { return std::tie(l...); }(*std::get<Is>(iters)...);
			}
			constexpr auto operator++() { (++std::get<Is>(iters), ...); }
			constexpr auto operator++(int)
			{
				auto v = *this;
				++*this;
				return v;
			}
		};

		template <typename Tuple, size_t... Is>
		struct Join_range {
			Tuple layers;

			constexpr Join_range(Tuple layers, std::index_sequence<Is...>) : layers(std::move(layers)) {}

			constexpr auto begin() const noexcept
			{
				return Join_iterator{std::make_tuple(std::get<Is>(layers).begin()...),
				                     std::index_sequence<Is...>{}};
			}
			constexpr auto begin() noexcept
			{
				return Join_iterator{std::make_tuple(std::get<Is>(layers).begin()...),
				                     std::index_sequence<Is...>{}};
			}
			constexpr auto end() const noexcept
			{
				return Join_iterator{std::make_tuple(std::get<Is>(layers).end()...),
				                     std::index_sequence<Is...>{}};
			}
			constexpr auto end() noexcept
			{
				return Join_iterator{std::make_tuple(std::get<Is>(layers).end()...),
				                     std::index_sequence<Is...>{}};
			}
		};
	} // namespace detail

	template <typename... Layers>
	requires(joinable_layers<Layers...>)
	[[nodiscard]] constexpr auto unchecked_range(Layers&... layers)
	{
		const auto size = [](const auto& first, const auto&...) { return first.size(); }(layers...);
		(void) size;
		assert(((size == layers.size()) && ...));

		return detail::Join_range{std::forward_as_tuple(layers...),
		                          std::make_index_sequence<sizeof...(Layers)>{}};
	}

} // namespace yggdrasill
