/// NOTE: This is the template implementation header for serialization.hpp and should not be included directly!
#pragma once

#ifndef YGDL_SERIALIZATION_HPP_INCLUDED
#include <yggdrasill/serialization.hpp>
#endif

namespace yggdrasill {


	inline bool serialization_supported()
	{
		return YGDL_serialization_supported();
	}


	// IMPLEMENTATION: Deserializer
	inline void Deserializer::foreach_userdata(std::invocable<std::string_view> auto&& callback) const
	{
		YGDL_serializer_userdata_iterate(c_ptr(this), &callback, [](void* ctx, YGDL_String_view name) {
			auto& f = *static_cast<std::remove_reference_t<decltype(callback)>*>(ctx);
			f(detail::from_ygdl_string_view(name));
		});
	}
	inline void Deserializer::foreach_config(std::invocable<std::string_view> auto&& callback) const
	{
		YGDL_serializer_config_iterate(c_ptr(this), &callback, [](void* ctx, YGDL_String_view name) {
			auto& f = *static_cast<std::remove_reference_t<decltype(callback)>*>(ctx);
			f(detail::from_ygdl_string_view(name));
		});
	}
	inline void Deserializer::foreach_world(std::invocable<Serialized_world_handle> auto&& callback) const
	{
		YGDL_serializer_world_iterate(c_ptr(this), &callback, [](void* ctx, YGDL_Serializer_world_ref key) {
			auto& f = *static_cast<std::remove_reference_t<decltype(callback)>*>(ctx);
			f(static_cast<Serialized_world_handle>(key));
		});
	}

	inline auto Deserializer::read_userdata(std::string_view key) -> std::vector<std::byte>
	{
		auto bytes = std::vector<std::byte>();
		read_userdata(key, [&](Byte_reader& reader) {
			bytes.resize(reader.remaining_bytes());
			reader.read_into(bytes);
		});
		return bytes;
	}
	inline void Deserializer::read_userdata(std::string_view key, std::invocable<Byte_reader&> auto&& callback)
	{
		auto callback_wrapper = [](void* ctx, void* reader_ctx, YGDL_Serializer_reader_req* req) {
			auto  reader = Byte_reader{reader_ctx, req};
			auto& cc     = *static_cast<std::remove_reference_t<decltype(callback)>*>(ctx);
			cc(reader);
		};

		YGDL_serializer_userdata_read(
		        c_ptr(this), detail::to_ygdl_string_view(key), &callback, callback_wrapper, Error_throw{});
	}

	inline auto Deserializer::read_config(std::string_view key) -> Dictionary
	{
		auto dict = Dictionary{};
		read_config(key, dict);
		return dict;
	}
	inline void Deserializer::read_config(std::string_view key, Dictionary_view dict)
	{
		YGDL_serializer_config_read(c_ptr(this), detail::to_ygdl_string_view(key), c_ptr(dict), Error_throw{});
	}

	inline auto Deserializer::read_world(Serialized_world_handle key) -> World
	{
		auto world = World{};
		read_world(key, world);
		return world;
	}
	inline void Deserializer::read_world(Serialized_world_handle key, World_view world)
	{
		YGDL_serializer_world_read(
		        c_ptr(this), static_cast<YGDL_Serializer_world_ref>(key), c_ptr(world), Error_throw{});
	}

	inline void Deserializer::shrink_read_caches()
	{
		YGDL_serializer_shrink_read_caches(c_ptr(this));
	}

	inline Deserializer::Deserializer(const std::filesystem::path& path, bool writeable, bool overwrite)
	  : serializer_(YGDL_serializer_create(detail::to_ygdl_string_view(path.generic_string()),
	                                       writeable ? YGDL_TRUE : YGDL_FALSE,
	                                       overwrite ? YGDL_TRUE : YGDL_FALSE,
	                                       Error_throw{}),
	                &YGDL_serializer_destroy)
	{
	}


	inline std::span<std::byte> Byte_reader::read_into(std::span<std::byte> dst)
	{
		if(dst.empty())
			return dst;

		auto read = req_(reader_ctx_, dst.size(), dst.data());
		remaining_bytes_ -= read;
		return dst.subspan(0, read);
	}
	template <typename T>
	void Byte_reader::read_into(std::vector<T>& container)
	{
		auto read       = read_into(std::as_writable_bytes(std::span(container)));
		auto read_count = read.size() / sizeof(T);
		if(container.size() > read_count)
			container.erase(container.begin() + read_count, container.end());
	}



	// IMPLEMENTATION: Serializer
	inline void Serializer::flush()
	{
		YGDL_serializer_flush(c_ptr(*this), Error_throw{});
	}

	inline void Serializer::write_userdata(std::string_view key, std::span<std::byte> data)
	{
		YGDL_serializer_userdata_write(c_ptr(this),
		                               detail::to_ygdl_string_view(key),
		                               data.data(),
		                               static_cast<uint64_t>(data.size()),
		                               Error_throw{});
	}
	inline void Serializer::write_config(std::string_view key, Const_dictionary_view dict)
	{
		YGDL_serializer_config_write(c_ptr(this), detail::to_ygdl_string_view(key), c_ptr(dict), Error_throw{});
	}
	inline auto Serializer::write_world(Const_world_view world) -> Serialized_world_handle
	{
		const auto key = YGDL_serializer_world_write(c_ptr(this), c_ptr(world), Error_throw{});
		return static_cast<Serialized_world_handle>(key);
	}

} // namespace yggdrasill
