/// NOTE: This is the template implementation header for generator.hpp and should not be included directly!
#pragma once

#ifndef YGDL_GENERATOR_HPP_INCLUDED
#include <yggdrasill/generator.hpp>
#endif


namespace yggdrasill {

	inline Error Module_view::validate(Dictionary_view configuration) const
	{
		assert(module_);
		auto error = Error();
		YGDL_generator_module_validate(module_, c_ptr(configuration), c_ptr(error));
		if(error.code() == YGDL_ERROR_CODE_INVALID_PLUGIN)
			error.throw_as_exception();

		return error;
	}
	inline void Module_view::execute(World_view world, Const_dictionary_view configuration)
	{
		assert(module_);
		auto error = Error();
		YGDL_generator_module_execute(module_, c_ptr(world), c_ptr(configuration), c_ptr(error));
		if(!error.success()) {
			auto msg = std::string("Error executing module '");
			msg += id();
			msg += "': ";
			error.throw_as_exception(msg);
		}
	}

	inline std::string_view Module_view::id() const
	{
		return detail::from_ygdl_string_view(YGDL_generator_module_id(module_));
	}

	inline Data_version Module_view::version() const
	{
		assert(module_);
		return Data_version(YGDL_generator_module_version(module_));
	}
	inline bool Module_view::modified(Data_version& version) const
	{
		if(auto new_version = this->version(); new_version != version) {
			version = new_version;
			return true;
		} else {
			return false;
		}
	}


	// IMPLEMENTATION: Generator
	namespace detail {
		inline constexpr Module_range list_to_range(const Module_list& list)
		{
			return Module_range{Module_iterator(list.modules), Module_iterator(list.modules + list.count)};
		}
	} // namespace detail

	inline Generator::Generator() : impl_(YGDL_generator_create(), &YGDL_generator_destroy) {}

	inline Module_view Generator::add(const Module_info& module_info)
	{
		return YGDL_generator_module_add(impl_.get(), &module_info, Error_throw());
	}

#ifndef YGDL_STATIC
	inline Module_range Generator::load(std::string_view path)
	{
		YGDL_generator_module_load(impl_.get(), detail::to_ygdl_string_view(path), Error_throw());
		auto loaded = YGDL_generator_module_last(impl_.get());
		return detail::list_to_range(loaded);
	}
#endif

	inline std::optional<Module_view> Generator::get(std::string_view id) const
	{
		auto m = YGDL_generator_module_get(impl_.get(), detail::to_ygdl_string_view(id));
		return m ? std::make_optional<Module_view>(m) : std::nullopt;
	}

	inline Module_range Generator::reload()
	{
		auto loaded = YGDL_generator_module_reload(impl_.get(), Error_throw());
		return detail::list_to_range(loaded);
	}
	inline bool Generator::reload_available() const
	{
		return YGDL_generator_module_reload_available(impl_.get()) == YGDL_TRUE;
	}

	inline Module_range Generator::list() const
	{
		return detail::list_to_range(YGDL_generator_module_list(impl_.get()));
	}

} // namespace yggdrasill
