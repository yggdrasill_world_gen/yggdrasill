/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/generator.h>

#include <yggdrasill/commons.hpp>
#include <yggdrasill/dictionary.hpp>
#include <yggdrasill/error.hpp>
#include <yggdrasill/module.hpp>
#include <yggdrasill/string.hpp>
#include <yggdrasill/utils.hpp>
#include <yggdrasill/world.hpp>

#include <memory>
#include <optional>
#include <span>


namespace yggdrasill {

	/**A handle that refers to a module managed by a `Generator`.\n
	 * Modules represent generation algorithms, or parts of them and can be executed on a `World` definition to modify it.\n
	 * 
	 * The handle might reference no underlying object (nullptr), in which case the bool-conversion-operator
	 * returns false and none of the other methods may be called.
	 * 
	 * \note References to modules remain valid until they are removed from the `Generator` they were obtained from,
	 * either because it was destroyed or because the module was unloaded by `Generator::reload`.
	 */
	class Module_view {
	  public:
		/// Converting constructor from a reference to the C-API struct
		constexpr /*implicit*/ Module_view(YGDL_Module* m = nullptr) : module_(m) {}

		/// Conversion operator to bool
		/// \returns true if this view references an existing world
		constexpr explicit operator bool() const { return module_; }

		/**Executes the validation function of the referenced module.\n
		 * The passed `configuration` is checked against the modules requirements and may be modified to
		 * match them. Including but not limited to adding configuration options not currently contained.
		 * \returns An `Error` that is set to the last failure, if there was an error in the passed
		 * `configuration` that couldn't be automatically corrected.
		 * \throws Invalid_plugin_exception If the plugin that contained this module was unloaded
		 * \note No pointer to the passed `configuration` is retained after this function returns.
		 * Thus, there are no further lifetime requirements for the parameters.
		 */
		[[nodiscard]] Error validate(Dictionary_view configuration) const;

		/**Executes the module with the given `configuration` and initial `world` state.\n
		 * The state stored in the passed `world` object is modified according to the modules' definition.
		 * \throws Invalid_plugin_exception If the plugin that contained this module was unloaded
		 * \throws Exception If the `configuration` was invalid, or the module execution failed for other reasons.
		 * \note No pointer to the passed `configuration` is retained after this function returns.
		 * Thus, there are no further lifetime requirements for the parameters.
		 */
		void execute(World_view world, Const_dictionary_view configuration);

		/// \returns The unique id of the module this handle references
		[[nodiscard]] std::string_view id() const;

		/**\returns A operator== comparable object, that changes every time the underlying implementation is
		 * replaces by calling `Generator::reload`. So, the returned value can be used to check if the
		 * underlying implementation has changed, for example since it was executed.
		 */
		[[nodiscard]] Data_version version() const;
		/**Convenience helper that checks if the implementation changed since the last call to this function.
		 * \param version The object used to memorize the last version number, as returned by `Module_view::version()`,
		 * The object has to be stored and reused for each subsequent call on this module-handle.
		 * \returns true if the implementation changed since the last call to this function
		 */
		[[nodiscard]] bool modified(Data_version& version) const;

		/// Comparison operator \skip
		[[nodiscard]] constexpr friend bool operator==(const Module_view& lhs, const Module_view& rhs)
		{
			return lhs.module_ == rhs.module_;
		}

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
		 * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
		 */
		[[nodiscard]] constexpr friend const YGDL_Module* c_ptr(const Module_view& m) { return m.module_; }
		[[nodiscard]] constexpr friend YGDL_Module*       c_ptr(Module_view& m) { return m.module_; }
		[[nodiscard]] constexpr friend const YGDL_Module* c_ptr(const Module_view* m)
		{
			return m ? m->module_ : nullptr;
		}
		[[nodiscard]] constexpr friend YGDL_Module* c_ptr(Module_view* m) { return m ? m->module_ : nullptr; }

	  private:
		YGDL_Module* module_;
	};

	/// An iterator into a sequence of modules of type `Module_view`
	using Module_iterator = Wrapping_iterator<Module_view, YGDL_Module**>;
	// A range of modules of type `Module_view`
	using Module_range = Iterator_range<Module_iterator, Module_iterator>;

	/**A world generator.\n
	 * Objects of this class manage a collection of modules, that can be used to execute generation steps,
	 * as well as their underlying OS-resources (e.g. shared library handles) and internal persistent state.
	 */
	class Generator {
	  public:
		/// Default constructor
		Generator();

		/**Instantiate the given module and add it to this generator.
		 * \returns A handle to the instantiated module
		 * \param module_info The definition of the module. This can either be a reference to the global
		 * definition exported from a statically linked plugin, or a temporary object returned by calling
		 * `to_module_info`
		 * \note The passed `Module_info` only has to outlive this function call but all function pointers
		 * contained within it have to remain valid for at least the whole lifetime of the generator.
		 */
		Module_view add(const Module_info& module_info);

#ifndef YGDL_STATIC
		/**Load any shared libraries at the given `path` and adds them to the generator.\n
		 * \param path Either the path to a single shared library that should be loaded or to a directory
		 * that may contain multiple shared libraries. In The later case, each of the files must have the
		 * correct platform dependent suffix (.so or .dll for Windows).
		 * \returns A range of all modules loaded during this call
		 * \throws Invalid_plugin_exception If the shared library couldn't be loaded, didn't contain a
		 * ``YGDL_init_plugin`` function, or the modules initialisation failed.
		 * \attention If multiple plugins are loaded during this call only the first error is reported and
		 * one or more modules might still have been loaded successfully!
		 * \note Already loaded files are ignored but loading a plugin might fail if a module with the same
		 * id already exists. Use `reload` instead to reload modules after their shared library file was updated.
		 * \note Because plugins require access to the library API, this function is only available if the
		 * library itself was compiled as a shared library. If you are using a static build, you'll have to
		 * add modules manually using `add`
		 */
		Module_range load(std::string_view path);
#endif

		/// \returns A pointer to the module with the given id, if there is one in this generator
		[[nodiscard]] std::optional<Module_view> get(std::string_view id) const;

		/**Reloads any shared libraries that have been modified (filesystem last modified date) since they
		 * were loaded or have been added to loaded directories.
		 * \returns A range of all modules loaded during this call
		 * \throws Invalid_plugin_exception If one of the shared libraries couldn't be loaded, didn't contain a
		 * ``YGDL_init_plugin`` function, or the modules initialisation failed.
		 * \note Any state stored by the reloaded modules is reset in the process.
		 * \attention The behaviour is undefined if any of the reloaded modules is currently being used in
		 * a concurrent call to `Module_view::execute` or `Module_view::validate`.
		 * \attention This functions only provides the weak exception guarantee! Plugins may be missing
		 * after this call if their file was removed or loading them failed and if a failure occurred
		 * the previous state can't be restored.
		 * \attention This function is generally not usable if multiple generator instances loaded the same
		 * plugins. Because the support for true isolation of shared libraries is limited in OS APIs,
		 * multiple generators might end up sharing the same shared-library object. In this case unloading
		 * and reloading the shared library won't have any effect.
		 */
		Module_range reload();

		/**\returns true if a call to `reload` *might* reload modules, i.e. if there could be changes that require a reload.
		 * \note In contrast to `reload` this function can be safely called concurrently with module executions.
		 * So it can be used as a quick check if further expensive synchronization and a call `reload` is required
		 */
		[[nodiscard]] bool reload_available() const;

		/// \returns A range of all modules managed by this generator
		[[nodiscard]] Module_range list() const;

	  private:
		std::unique_ptr<YGDL_Generator, void (*)(YGDL_Generator*)> impl_;
	};

} // namespace yggdrasill

#define YGDL_GENERATOR_HPP_INCLUDED
#include <yggdrasill/generator.inl>
