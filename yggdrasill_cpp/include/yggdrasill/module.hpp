/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/module.h>

#include <yggdrasill/commons.hpp>
#include <yggdrasill/dictionary.hpp>
#include <yggdrasill/error.hpp>
#include <yggdrasill/string.hpp>
#include <yggdrasill/world.hpp>

#include <optional>

namespace yggdrasill {

	/**Structure that contains the metadata for a module, in the format expected by `YGDL_Generator`.\n
	 * A valid module has to consist of at least its unique ID and pointers to its ``validate`` and ``execute`` functions.\n
	 * Additionally, modules may use semi-persistent storage as a cache or utilize additional initialization routines. But it has to be ensured that the
	 * behaviour remains valid even if ``validate`` or ``execute`` are called multiple times, with different configurations or on different world states.
	 * \attention While the object itself can be short-lived, i.e. you may add a temporary `YGDL_Module_info`
	 * to a generator, the contained function pointers have to remain valid for the whole remaining lifetime of the generator it is added to.
	 */
	using Module_info = YGDL_Module_info;

	/**Creates a `Module_info` definition based on the passed unique ID and the module entry-point defined by the template parameter(s).
	 * \param id The unique ID of the module
	 * \overload_specific
	 * \tparam T A class type that contains the modules state and entry-points and will be instanced and
	 * destroyed automatically. The type `T` has to be default-constructable, destructible
	 * and has to have a public callable ``execute`` function, matching one of the signatures in `Module_signature`,
	 * as well as either a ``validate`` function, or an ``execute`` overload that can be used for validation, as described above.
	 * 
	 * \attention The `Generator` is explicitly allowed to instantiate multiple objects (e.g. one per thread)
	 * and makes no guarantees, whether the same object is used for subsequent calls or if there were other 
	 * intermittent calls with different configurations or world states.  
	 * Thus, the objects may be used for either constant or temporary data, but the initial state of any 
	 * non-const data is unspecified and usually has to be reset or otherwise overwritten.
	 */
	template <typename T>
	inline constexpr Module_info to_module_info(std::string_view id);

	/**This type contains aliases for the signatures that are supported for module entry points
	 * by the `to_module_info` factory functions.\n
	 * Any exceptions thrown by the functions (including those with an `Error` return type) are automatically
	 * caught and translated to the corresponding C-API error type.
	 */
	struct Module_signature {
		using Execute_throw   = void(World_view, Const_dictionary_view);
		using Execute_nothrow = Error(World_view, Const_dictionary_view);

		using Validate_a_throw   = void(Dictionary_view);
		using Validate_a_nothrow = Error(Dictionary_view);

		using Validate_b_throw   = void(World_view, Dictionary_view);
		using Validate_b_nothrow = Error(World_view, Dictionary_view);
	};

	template <Module_signature::Execute_nothrow* Execute, Module_signature::Validate_a_nothrow* Validate>
	inline constexpr Module_info to_module_info(std::string_view id);

	template <Module_signature::Execute_nothrow* Execute, Module_signature::Validate_b_nothrow* Validate>
	inline constexpr Module_info to_module_info(std::string_view id);

	template <Module_signature::Execute_throw* Execute, Module_signature::Validate_a_throw* Validate>
	inline constexpr Module_info to_module_info(std::string_view id);

	template <Module_signature::Execute_throw* Execute, Module_signature::Validate_b_throw* Validate>
	inline constexpr Module_info to_module_info(std::string_view id);

} // namespace yggdrasill

#define YGDL_MODULE_HPP_INCLUDED
#include <yggdrasill/module.inl>
