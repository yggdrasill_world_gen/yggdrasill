/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/commons.hpp>
#include <yggdrasill/layer.hpp>
#include <yggdrasill/mesh.hpp>
#include <yggdrasill/random.hpp>
#include <yggdrasill/utils.hpp>

#include <algorithm>
#include <unordered_map>
#include <variant>

namespace yggdrasill {

	/// \returns true if the triangle ABC would have the wrong winding order, i.e. clockwise instead of counter-clockwise.
	constexpr inline bool is_wrong_winding_order(const Vec3& a, const Vec3& b, const Vec3& c);

	/**\returns true if the given vertex could be moved to the new position without causing any face to
	 * fold, i.e. its winding-order to change.
	 * \pre All faces have to be in counter-clockwise winding order.
	 */
	inline bool can_move_vertex(
	        Const_mesh_view, Vertex, Const_layer_view<Vec3, Ref_type::vertex> positions, const Vec3&);

	/**Checks if the given edge could be collapsed using `join_point` without causing any face to fold,
	 * i.e. its winding-order to change.
	 * \param positions The position layer that should be used to check the winding order.
	 * \pre All faces have to be in counter-clockwise winding order.
	 * \see `Mesh_view::collapse()`
	 */
	inline bool try_collapse(
	        Mesh_view, Primal_edge, float join_point, Const_layer_view<Vec3, Ref_type::vertex> positions);

	/// \returns A random vertex from the mesh, for which the given predicate returns true,
	/// or `std::nullopt` if after `tries` samples no such vertex was found.
	inline std::optional<Valid<Vertex>> random_vertex(
	        Const_mesh_view, Random_view&, int tries, std::predicate<Vertex> auto&&);

	/// A callable object, which returns a floating point, used to rank objects.
	template <typename T, typename... Args>
	concept Scorer = std::is_floating_point_v<std::invoke_result_t<T, Args...>>;

	/// Uses hill-climbing, starting from the given vertex and using `max_steps`) to find the vertex for
	/// which the given function returns the smallest result.
	inline Vertex minimizing_hill_climb(
	        Const_mesh_view, Vertex start, int max_steps, Neighbor_type, Scorer<Vertex> auto&&);

	/// Predicate that can be used for `flood_fill()`, which takes an additional parameter and optionally
	/// returns a new value, which will be passed to any calls for vertices reached from that position.
	template <typename T, typename Payload>
	concept Flood_fill_payload_predicate =
	        std::predicate<T, Vertex, Payload>
	        || std::is_same_v<std::invoke_result_t<T, Vertex, Payload>, std::optional<Payload>>;

	/**Traverses the mesh, starting at the given `seed` vertices, calling the predicate for each reached
	 * vertex exactly once, until there are no more edges to explore.
	 * \overload_specific
	 */
	inline void flood_fill(
	        Const_mesh_view, std::vector<Vertex> seeds, Neighbor_type, std::predicate<Vertex> auto&&);

	/**\overload_specific
	 * An additional payload may be passed with the seed vertices, which is tracked and passed to the
	 * predicate. The predicate may also return a new value, which is memorized and used for every vertex
	 * reached from this one.
	 */
	template <typename Payload>
	inline void flood_fill(Const_mesh_view,
	                       std::vector<std::pair<Vertex, Payload>> seeds,
	                       Neighbor_type,
	                       Flood_fill_payload_predicate<Payload> auto&&);

	/// \returns The area of the voronoi region surrounding the given vertex, using the normalized vectors
	/// from the passed layer as a source for positional data.
	inline float normalized_voronoi_area(Const_mesh_view, Const_layer_view<Vec3, Ref_type::vertex>, Vertex);

	/// An algorithm to determine the shortest path from a start vertex to on implicitly defined target.
	namespace path_finding {
		/// Tag type returned from the cost function to indicate that the parameter is a valid target.
		struct Target_found {};
		/// Tag type returned from the cost function to indicate that the given path can't be taken.
		struct Unreachable {};

		/// Return type of cost functions, either a float ``> 0`` or one of the tags above.
		using Cost = std::variant<Target_found, Unreachable, float>;

		using Path = std::vector<Vertex>;

		/**Object that is called with a step and returns it's cost. The parameters are:\n
		 * - The edge e that is being checked.
		 * - The previous vertex on the path or `no_vertex` if this is the first step.
		 * - `e.origin()`
		 * - `e.dest()`
		 */
		template <typename T>
		concept Cost_function =
		        std::is_same_v<std::invoke_result_t<T, Valid<Primal_edge>, Vertex, Valid<Vertex>, Valid<Vertex>>, Cost>;

		/// \returns The shortest path from `start` to a target implicitly defined by the cost function,
		/// or an empty `std::vector` if no such path could be found.
		inline Path shortest_path(const Const_mesh_view, const Vertex start, const Cost_function auto&);
	} // namespace path_finding

} // namespace yggdrasill

#define YGDL_MESH_UTILS_HPP_INCLUDED
#include <yggdrasill/mesh_utils.inl>
