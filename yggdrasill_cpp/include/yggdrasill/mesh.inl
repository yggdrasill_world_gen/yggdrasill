/// NOTE: This is the template implementation header for mesh.hpp and should not be included directly!
#pragma once

#ifndef YGDL_MESH_HPP_INCLUDED
#include <yggdrasill/mesh.hpp>
#endif


namespace yggdrasill {

	// IMPLEMENTATION: Const_mesh_view
	[[nodiscard]] inline Range_of<const Valid<Face>> auto Const_mesh_view::faces() const
	{
		return detail::make_mesh_element_range<Face, false, &YGDL_Mesh::face_count, &YGDL_Mesh::unused_face_list, &YGDL_Mesh::unused_face_count>(
		        *mesh_);
	}
	[[nodiscard]] inline Range_of<const Valid<Face>> auto Const_mesh_view::faces(concurrent_modifications_t) const
	{
		return detail::make_mesh_element_range<Face, true, &YGDL_Mesh::face_count, &YGDL_Mesh::unused_face_list, &YGDL_Mesh::unused_face_count>(
		        *mesh_);
	}

	[[nodiscard]] inline Range_of<const Valid<Vertex>> auto Const_mesh_view::vertices() const
	{
		return detail::make_mesh_element_range<Vertex, false, &YGDL_Mesh::vertex_count, &YGDL_Mesh::unused_vertex_list, &YGDL_Mesh::unused_vertex_count>(
		        *mesh_);
	}
	[[nodiscard]] inline Range_of<const Valid<Vertex>> auto Const_mesh_view::vertices(concurrent_modifications_t) const
	{
		return detail::make_mesh_element_range<Vertex, true, &YGDL_Mesh::vertex_count, &YGDL_Mesh::unused_vertex_list, &YGDL_Mesh::unused_vertex_count>(
		        *mesh_);
	}



	// Const_mesh_view::find_edge definition at end of file

	inline constexpr index_t Const_mesh_view::vertex_used_count() const
	{
		return detail::c_ref(this).vertex_count - detail::c_ref(this).unused_vertex_count;
	}
	inline constexpr index_t Const_mesh_view::face_used_count() const
	{
		return detail::c_ref(this).face_count - detail::c_ref(this).unused_face_count;
	}
	inline constexpr index_t Const_mesh_view::edge_used_count() const
	{
		return detail::c_ref(this).edge_count - detail::c_ref(this).unused_edge_count * 2;
	}

	inline constexpr index_t Const_mesh_view::vertex_max_index() const
	{
		return detail::c_ref(this).vertex_count - 1;
	}
	inline constexpr index_t Const_mesh_view::face_max_index() const
	{
		return detail::c_ref(this).face_count - 1;
	}
	inline constexpr index_t Const_mesh_view::edge_max_index() const
	{
		return detail::c_ref(this).edge_count - 1;
	}


	// IMPLEMENTATION: Mesh_view
	inline void Mesh_view::add_vertices(index_t count)
	{
		YGDL_mesh_vertex_add(&detail::c_ref(this), count, nullptr, Error_throw());
	}
	inline void Mesh_view::add_vertices(std::span<Vertex> out)
	{
		if(out.empty())
			return;

		YGDL_mesh_vertex_add(&detail::c_ref(this), static_cast<index_t>(out.size()), out.data(), Error_throw());
	}
	inline bool Mesh_view::remove_unused_vertex(Vertex v)
	{
		return YGDL_mesh_vertex_remove(&detail::c_ref(this), v);
	}
	inline Valid<Face> Mesh_view::add_face(Vertex a, Vertex b, Vertex c)
	{
		auto r = YGDL_mesh_face_add(&detail::c_ref(this), a, b, c, Error_throw());
		return Valid<Face>{r};
	}
	inline Face Mesh_view::try_add_face(Vertex a, Vertex b, Vertex c)
	{
		return YGDL_mesh_face_add(&detail::c_ref(this), a, b, c, nullptr);
	}

	inline void Mesh_view::flip(Edge<> e)
	{
		YGDL_mesh_edge_flip(&detail::c_ref(this), e, Error_throw());
	}
	inline bool Mesh_view::try_flip(Edge<> e)
	{
		const auto r = YGDL_mesh_edge_flip(&detail::c_ref(this), e, nullptr);
		return r;
	}
	inline Valid<Primal_edge> Mesh_view::split(Primal_edge e, float split_point)
	{
		const auto r = YGDL_mesh_edge_split(&detail::c_ref(this), e, split_point, Error_throw());
		return Valid<Primal_edge>{r};
	}
	inline Primal_edge Mesh_view::try_split(Primal_edge e, float split_point)
	{
		return YGDL_mesh_edge_split(&detail::c_ref(this), e, split_point, nullptr);
	}

	inline Valid<Vertex> Mesh_view::collapse(Primal_edge e, float join_point)
	{
		const auto r = YGDL_mesh_edge_collapse(&detail::c_ref(this), e, join_point, Error_throw());
		return Valid<Vertex>{r};
	}
	inline Vertex Mesh_view::try_collapse(Primal_edge e, float join_point)
	{
		return YGDL_mesh_edge_collapse(&detail::c_ref(this), e, join_point, nullptr);
	}


	// IMPLEMENTATION: Edge
	template <Edge_type Type>
	constexpr Edge<Type>::Edge(Edge_type type, index_t index) requires(Type == Edge_type::any)
	  : Edge(YGDL_mesh_edge_from_index(type == Edge_type::dual, index))
	{
		assert(type != Edge_type::any);
	}
	template <Edge_type Type>
	constexpr Edge<Type>::Edge(index_t index) requires(Type != Edge_type::any)
	  : Edge(YGDL_mesh_edge_from_index(Type == Edge_type::dual, index))
	{
	}
	template <Edge_type Type>
	template <Edge_type T>
	constexpr Edge<Type>::Edge(const Edge<T>& e) requires(assignable_from(Type, T))
	  : YGDL_Edge{e.mask}
	{
		if constexpr(T == Edge_type::any && Type != Edge_type::any) {
			assert(e == no_edge || Type == e.type());
		}
	}

	template <Edge_type Type>
	template <Edge_type T>
	constexpr Edge<Type>& Edge<Type>::operator=(const Edge<T>& e) requires(assignable_from(Type, T))
	{
		if constexpr(T == Edge_type::any && Type != Edge_type::any) {
			assert(e == no_edge || Type == e.type());
		}
		mask = e.mask;
		return *this;
	}

	template <Edge_type Type>
	constexpr index_t Edge<Type>::index() const
	{
		if constexpr(Type == Edge_type::primal) {
			return static_cast<index_t>(mask);
		} else {
			return YGDL_mesh_edge_index(c_val());
		}
	}

	template <Edge_type Type>
	constexpr Edge_type Edge<Type>::type() const
	{
		if constexpr(Type == Edge_type::any)
			return YGDL_mesh_edge_is_dual(c_val()) ? Edge_type::dual : Edge_type::primal;
		else
			return Type;
	}

	template <Edge_type Type>
	constexpr auto Edge<Type>::sym() const -> Valid<Edge>
	{
		return Valid<Edge>{YGDL_mesh_edge_sym(c_val())};
	}
	template <Edge_type Type>
	constexpr Valid<Generic_edge> Edge<Type>::rot() const requires(Type == Edge_type::any)
	{
		return Valid<Generic_edge>{YGDL_mesh_edge_rot(c_val())};
	}
	template <Edge_type Type>
	constexpr Valid<Dual_edge> Edge<Type>::rot() const requires(Type == Edge_type::primal)
	{
		return Valid<Dual_edge>{YGDL_mesh_edge_rot(c_val())};
	}
	template <Edge_type Type>
	constexpr Valid<Primal_edge> Edge<Type>::rot() const requires(Type == Edge_type::dual)
	{
		return Valid<Primal_edge>{YGDL_mesh_edge_rot(c_val())};
	}
	template <Edge_type Type>
	constexpr Valid<Generic_edge> Edge<Type>::inv_rot() const requires(Type == Edge_type::any)
	{
		return Valid<Generic_edge>{YGDL_mesh_edge_inv_rot(c_val())};
	}
	template <Edge_type Type>
	constexpr Valid<Dual_edge> Edge<Type>::inv_rot() const requires(Type == Edge_type::primal)
	{
		return Valid<Dual_edge>{YGDL_mesh_edge_inv_rot(c_val())};
	}
	template <Edge_type Type>
	constexpr Valid<Primal_edge> Edge<Type>::inv_rot() const requires(Type == Edge_type::dual)
	{
		return Valid<Primal_edge>{YGDL_mesh_edge_inv_rot(c_val())};
	}
	template <Edge_type Type>
	constexpr auto Edge<Type>::base() const -> Valid<Primal_edge>
	{
		return Valid<Primal_edge>{YGDL_mesh_edge_base(c_val())};
	}

	template <Edge_type Type>
	constexpr auto Edge<Type>::origin_next(const Const_mesh_view mesh) const -> Valid<Edge>
	{
		return Valid<Edge>{YGDL_mesh_edge_origin_next(&detail::c_ref(mesh), c_val())};
	}
	template <Edge_type Type>
	constexpr auto Edge<Type>::origin_prev(const Const_mesh_view mesh) const -> Valid<Edge>
	{
		return Valid<Edge>{YGDL_mesh_edge_origin_prev(&detail::c_ref(mesh), c_val())};
	}

	template <Edge_type Type>
	constexpr auto Edge<Type>::dest_next(const Const_mesh_view mesh) const -> Valid<Edge>
	{
		return Valid<Edge>{YGDL_mesh_edge_destination_next(&detail::c_ref(mesh), c_val())};
	}
	template <Edge_type Type>
	constexpr auto Edge<Type>::dest_prev(const Const_mesh_view mesh) const -> Valid<Edge>
	{
		return Valid<Edge>{YGDL_mesh_edge_destination_prev(&detail::c_ref(mesh), c_val())};
	}

	template <Edge_type Type>
	constexpr auto Edge<Type>::left_next(const Const_mesh_view mesh) const -> Valid<Edge>
	{
		return Valid<Edge>{YGDL_mesh_edge_left_next(&detail::c_ref(mesh), c_val())};
	}
	template <Edge_type Type>
	constexpr auto Edge<Type>::left_prev(const Const_mesh_view mesh) const -> Valid<Edge>
	{
		return Valid<Edge>{YGDL_mesh_edge_left_prev(&detail::c_ref(mesh), c_val())};
	}

	template <Edge_type Type>
	constexpr auto Edge<Type>::right_next(const Const_mesh_view mesh) const -> Valid<Edge>
	{
		return Valid<Edge>{YGDL_mesh_edge_right_next(&detail::c_ref(mesh), c_val())};
	}
	template <Edge_type Type>
	constexpr auto Edge<Type>::right_prev(const Const_mesh_view mesh) const -> Valid<Edge>
	{
		return Valid<Edge>{YGDL_mesh_edge_right_prev(&detail::c_ref(mesh), c_val())};
	}

	template <Edge_type Type>
	constexpr Valid<Vertex> Edge<Type>::origin(const Const_mesh_view mesh) const
	        requires(Type == Edge_type::primal)
	{
		return Valid<Vertex>{YGDL_mesh_edge_primal_origin(&detail::c_ref(mesh), c_val())};
	}
	template <Edge_type Type>
	constexpr Valid<Vertex> Edge<Type>::dest(const Const_mesh_view mesh) const
	        requires(Type == Edge_type::primal)
	{
		return Valid<Vertex>{YGDL_mesh_edge_primal_destination(&detail::c_ref(mesh), c_val())};
	}
	template <Edge_type Type>
	constexpr Face Edge<Type>::left(const Const_mesh_view mesh) const requires(Type == Edge_type::primal)
	{
		return {YGDL_mesh_edge_primal_left(&detail::c_ref(mesh), c_val())};
	}
	template <Edge_type Type>
	constexpr Face Edge<Type>::right(const Const_mesh_view mesh) const requires(Type == Edge_type::primal)
	{
		return {YGDL_mesh_edge_primal_right(&detail::c_ref(mesh), c_val())};
	}

	template <Edge_type Type>
	constexpr Face Edge<Type>::origin(const Const_mesh_view mesh) const requires(Type == Edge_type::dual)
	{
		return {YGDL_mesh_edge_dual_origin(&detail::c_ref(mesh), c_val())};
	}
	template <Edge_type Type>
	constexpr Face Edge<Type>::dest(const Const_mesh_view mesh) const requires(Type == Edge_type::dual)
	{
		return {YGDL_mesh_edge_dual_destination(&detail::c_ref(mesh), c_val())};
	}
	template <Edge_type Type>
	constexpr Valid<Vertex> Edge<Type>::left(const Const_mesh_view mesh) const requires(Type == Edge_type::dual)
	{
		return Valid<Vertex>{YGDL_mesh_edge_dual_left(&detail::c_ref(mesh), c_val())};
	}
	template <Edge_type Type>
	constexpr Valid<Vertex> Edge<Type>::right(const Const_mesh_view mesh) const requires(Type == Edge_type::dual)
	{
		return Valid<Vertex>{YGDL_mesh_edge_dual_right(&detail::c_ref(mesh), c_val())};
	}

	template <Edge_type Type>
	constexpr std::array<Valid<Vertex>, 2> Edge<Type>::vertices(const Const_mesh_view mesh) const
	{
		if constexpr(Type == Edge_type::primal) {
			return {origin(mesh), dest(mesh)};
		} else if constexpr(Type == Edge_type::dual) {
			return {left(mesh), right(mesh)};
		} else {
			switch(type()) {
				case Edge_type::primal: return Primal_edge(*this).vertices(mesh);
				case Edge_type::dual: return Dual_edge(*this).vertices(mesh);
				case Edge_type::any: break;
			}
			assert(!"unhandled/unexpected Edge_type in Edge::vertices()");
			std::abort();
		}
	}
	template <Edge_type Type>
	constexpr std::array<Face, 2> Edge<Type>::faces(const Const_mesh_view mesh) const
	{
		if constexpr(Type == Edge_type::primal) {
			return {left(mesh), right(mesh)};
		} else if constexpr(Type == Edge_type::dual) {
			return {origin(mesh), dest(mesh)};
		} else {
			switch(type()) {
				case Edge_type::primal: return Primal_edge(*this).faces(mesh);
				case Edge_type::dual: return Dual_edge(*this).faces(mesh);
				case Edge_type::any: break;
			}
			assert(!"unhandled/unexpected Edge_type in Edge::faces()");
			std::abort();
		}
	}

	template <Edge_type Type>
	[[nodiscard]] inline bool Edge<Type>::valid(const Const_mesh_view mesh) const
	{
		return YGDL_mesh_edge_valid(&detail::c_ref(mesh), c_val()) == YGDL_TRUE;
	}

	template <Edge_type T1, Edge_type T2>
	constexpr inline bool operator==(const Edge<T1>& lhs, const Edge<T2>& rhs)
	{
		if constexpr(T1 != T2 && T1 != Edge_type::any && T2 != Edge_type::any)
			return false;
		else
			return lhs.mask == rhs.mask;
	}
	constexpr inline bool operator==(const Edge<>& lhs, const Edge<>& rhs)
	{
		return lhs.mask == rhs.mask;
	}
	template <Edge_type T1, Edge_type T2>
	requires(T1 != T2 || T1 == Edge_type::any || T2 == Edge_type::any)
	constexpr inline std::partial_ordering operator<=>(const Edge<T1>& lhs, const Edge<T2>& rhs)
	{
		if constexpr(T1 != T2 && T1 != Edge_type::any && T2 != Edge_type::any)
			return std::partial_ordering::unordered;
		else if(lhs.mask == YGDL_NO_EDGE.mask || rhs.mask == YGDL_NO_EDGE.mask)
			return lhs.mask <=> rhs.mask;
		else if(lhs.is_dual() != rhs.is_dual())
			return std::partial_ordering::unordered;
		else
			return lhs.mask <=> rhs.mask;
	}
	template <Edge_type T1, Edge_type T2>
	requires(T1 == T2 && T1 != Edge_type::any)
	constexpr inline std::strong_ordering operator<=>(const Edge<T1>& lhs, const Edge<T2>& rhs)

	{
		return lhs.mask <=> rhs.mask;
	}
	constexpr inline std::partial_ordering operator<=>(const Edge<>& lhs, const Edge<>& rhs)
	{
		if(lhs.mask == YGDL_NO_EDGE.mask || rhs.mask == YGDL_NO_EDGE.mask)
			return lhs.mask <=> rhs.mask;
		else if(lhs.is_dual() != rhs.is_dual())
			return std::partial_ordering::unordered;
		else
			return lhs.mask <=> rhs.mask;
	}


	// IMPLEMENTATION: Vertex
	[[nodiscard]] constexpr inline Primal_edge Vertex::edge(const Const_mesh_view mesh) const
	{
		assert(index() <= mesh.vertex_max_index());
		auto e = Primal_edge{detail::c_ref(mesh).vertex_edge[index()]};
		assert(e == no_edge || e.type() == Edge_type::primal);
		assert(e == no_edge || e.origin_next(mesh) != no_edge);
		return e;
	}

	[[nodiscard]] constexpr Range_of<Valid<Primal_edge>> auto Vertex::edges(const Const_mesh_view mesh) const
	{
		const auto start = edge(mesh);
		if(start == no_edge) {
			throw Precondition_violation_exception("Unconnected vertex passed to Vertex::edges()");
		}

		return start.origin_ccw(mesh);
	}

	[[nodiscard]] inline Range_of<Valid<Vertex>> auto Vertex::neighbors(const Const_mesh_view mesh,
	                                                                    Neighbor_type neighbor_type) const
	{
		const auto start = edge(mesh);
		if(start == no_edge) {
			throw Precondition_violation_exception("Unconnected vertex passed to Vertex::neighbors()");
		}

		// 0 : just primary
		// 1 : next is primary
		// 2 : next is dual of last primary
		auto mode = std::uint8_t(neighbor_type == Neighbor_type::just_primal_edges ? 0 : 1);

		return make_range([=, e = Primal_edge{}, i = 0](bool forward) mutable -> std::optional<Valid<Vertex>> {
			if(e == no_edge) {
				if(mode != 0)
					mode = 2;

				return (e = start).dest(mesh);
			}

			i += forward ? 1 : -1;

			if(mode == 2) {
				auto diagonal_edge = e.dest_next(mesh);
				// if the edge has no left or right face, there is no dual edge that connects => discard and return next primal connection.
				// this should never be the case on a closed mesh, but the mesh might currently have holes
				if(diagonal_edge.left(mesh) != no_face && diagonal_edge.right(mesh) != no_face) {
					mode = 1;
					return diagonal_edge.dest_next(mesh).origin(mesh);
				}
			} else if(mode == 1) {
				mode = 2;
			}

			e = forward ? e.origin_next(mesh) : e.origin_prev(mesh);

			if(e == start && forward && i > 0)
				return std::nullopt;
			else
				return e.dest(mesh);
		});
	}
	[[nodiscard]] inline bool Vertex::valid(const Const_mesh_view mesh) const
	{
		return YGDL_mesh_vertex_valid(&detail::c_ref(mesh), *this);
	}


	// IMPLEMENTATION: Face
	[[nodiscard]] constexpr inline Valid<Primal_edge> Face::edge(const Const_mesh_view mesh) const
	{
		assert(index() <= mesh.face_max_index());
		return Valid<Primal_edge>{detail::c_ref(mesh).face_edge[index()]};
	}

	[[nodiscard]] constexpr std::array<Valid<Vertex>, 3> Face::vertices(const Const_mesh_view mesh) const
	{
		const auto e  = edge(mesh);
		const auto en = e.left_next(mesh);
		return {e.origin(mesh), e.dest(mesh), en.dest(mesh)};
	}

	[[nodiscard]] constexpr Range_of<Valid<Primal_edge>> auto Face::edges(const Const_mesh_view mesh) const
	{
		return edge(mesh).left_ccw(mesh);
	}
	[[nodiscard]] inline bool Face::valid(const Const_mesh_view mesh) const
	{
		return YGDL_mesh_face_valid(&detail::c_ref(mesh), *this);
	}


	// has to be defined at the end, because it uses functionality from Edge and Vertex
	inline Primal_edge Const_mesh_view::find_edge(Vertex from, Vertex to) const
	{
		if(auto start_e = from.edge(*this); start_e != no_edge) {
			for(auto e : start_e.origin_ccw(*this)) {
				if(e.dest(*this) == to)
					return e;
			}
		}

		return Primal_edge{};
	}

} // namespace yggdrasill
