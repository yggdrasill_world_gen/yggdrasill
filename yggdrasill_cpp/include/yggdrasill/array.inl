/// NOTE: This is the template implementation header for array.hpp and should not be included directly!
#pragma once

#ifndef YGDL_ARRAY_HPP_INCLUDED
#include <yggdrasill/array.hpp>
#endif

namespace yggdrasill {

	// IMPLEMENTATION: Const_array_view
	template <typename T>
	[[nodiscard]] auto Const_array_view<T>::operator[](index_t index) const -> const_reference
	        requires(!std::is_void_v<T>)
	{
		auto* c_ptr = &detail::c_ref(this);

		if(c_api_value_type != static_cast<Type>(c_ptr->type)) {
			throw Type_mismatch_exception(detail::concat(
			        "Type of array elements doesn't match: ", c_api_value_type, "!=", static_cast<Type>(c_ptr->type)));
		}
		if(index < 0 || index >= c_ptr->size) {
			throw Out_of_bounds_exception(
			        detail::concat("Out of bounds access into yggdrasill::Array, with index ", index));
		}

		if constexpr(detail::is_dict<T>) {
			// YGDL_Dict is incomplete to enabled more compact storage without exposing C++ implementation details in the C-API
			return {*reinterpret_cast<const YGDL_Dict*>(reinterpret_cast<const char*>(c_ptr->data)
			                                            + index * detail::sizeof_dict_t)};

		} else if constexpr(detail::is_array_v<T>) {
			constexpr auto sub_type  = detail::c_api_type<detail::array_value_type<T>>;
			auto*          sub_array = reinterpret_cast<const YGDL_Array*>(c_ptr->data) + index;
			if(sub_type != Type::none && sub_type != static_cast<Type>(sub_array->type)) {
				throw Type_mismatch_exception(detail::concat(
				        "Type of sub-array doesn't match: ", sub_type, "!=", static_cast<Type>(sub_array->type)));
			}
			return {*sub_array};

		} else if constexpr(std::is_same_v<std::string_view, std::remove_cvref_t<T>>) {
			return *(reinterpret_cast<const String*>(c_ptr->data) + index);

		} else if constexpr(!detail::is_view_type<T>) {
			return *(reinterpret_cast<const T*>(c_ptr->data) + index);

		} else {
			static_assert(!std::is_same_v<T, T>, "Unsupported type");
		}
	}

	template <typename T>
	template <typename U>
	constexpr Const_array_view<U> Const_array_view<T>::cast_to() const
	{
		if(type() != detail::c_api_type<U>) {
			throw Type_mismatch_exception("Invalid target type in Untyped_array::cast_to()");
		}
		return detail::c_ref(this);
	}

	template <typename T>
	template <typename Visitor>
	void Const_array_view<T>::visit_value_types(Visitor&& visitor) const
	{
		switch(YGDL_array_type(c_ptr(this))) {
			case YGDL_TYPE_NONE: std::forward<Visitor>(visitor)(); break;
			case YGDL_TYPE_BOOL:
				std::forward<Visitor>(visitor)(Const_array_view<YGDL_Bool>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_INT8:
				std::forward<Visitor>(visitor)(Const_array_view<std::int8_t>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_INT32:
				std::forward<Visitor>(visitor)(Const_array_view<std::int32_t>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_INT64:
				std::forward<Visitor>(visitor)(Const_array_view<std::int64_t>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_FLOAT:
				std::forward<Visitor>(visitor)(Const_array_view<float>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_DOUBLE:
				std::forward<Visitor>(visitor)(Const_array_view<double>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_VEC2:
				std::forward<Visitor>(visitor)(Const_array_view<Vec2>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_VEC3:
				std::forward<Visitor>(visitor)(Const_array_view<Vec3>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_STRING:
				std::forward<Visitor>(visitor)(Const_array_view<String>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_ARRAY:
				std::forward<Visitor>(visitor)(Const_array_view<Const_array_view<void>>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_DICT:
				std::forward<Visitor>(visitor)(Const_array_view<Const_dictionary_view>(detail::c_ref(this)));
				break;
		}
	}
	template <typename T>
	template <typename... Visitors>
	void Const_array_view<T>::visit_value_types(Visitors&&... visitors) const requires(sizeof...(Visitors) > 1)
	{
		visit_value_types(overloaded{std::forward<Visitors>(visitors)...});
	}


	// IMPLEMENTATION: Array_view
	template <typename T>
	[[nodiscard]] auto Array_view<T>::operator[](index_t index) -> reference requires(!std::is_void_v<T>)
	{
		if constexpr(detail::is_dict<T> || detail::is_array_v<T>) {
			auto& c_ref = detail::c_ref(Const_array_view<T>::operator[](index));
			return const_cast<std::remove_cvref_t<decltype(c_ref)>&>(c_ref);
		} else {
			return const_cast<reference>(Const_array_view<T>::operator[](index));
		}
	}

	template <typename T>
	void Array_view<T>::reserve(index_t minimum_capacity)
	{
		YGDL_array_reserve(c_ptr(this), minimum_capacity, static_cast<YGDL_Type_t>(detail::c_api_type<T>));
	}

	template <typename T>
	void Array_view<T>::resize(index_t size)
	{
		YGDL_array_resize(c_ptr(this), size, static_cast<YGDL_Type_t>(detail::c_api_type<T>));
	}

	template <typename T>
	template <typename U>
	auto Array_view<T>::push_back(U&& value) -> reference requires(!std::is_void_v<T>)
	{
		reference v = push_back();
		v           = std::forward<U>(value);
		return v;
	}
	template <typename T>
	auto Array_view<T>::push_back() -> reference requires(!std::is_void_v<T>)
	{
		auto* c_ptr = &detail::c_ref(this);

		// Note: YGDL_array_get... calls and return are in different statements because destructing Error_throw,
		// and thus the error handling, needs to happen before the result is used!
		if constexpr(std::is_same_v<T, YGDL_Bool>) {
			auto* ptr = YGDL_array_push_bool(c_ptr, Error_throw{});
			return *ptr;
		} else if constexpr(std::is_same_v<T, std::int8_t>) {
			auto* ptr = YGDL_array_push_int8(c_ptr, Error_throw{});
			return *ptr;
		} else if constexpr(std::is_same_v<T, std::int32_t>) {
			auto* ptr = YGDL_array_push_int32(c_ptr, Error_throw{});
			return *ptr;
		} else if constexpr(std::is_same_v<T, std::int64_t>) {
			auto* ptr = YGDL_array_push_int64(c_ptr, Error_throw{});
			return *ptr;
		} else if constexpr(std::is_same_v<T, float>) {
			auto* ptr = YGDL_array_push_float(c_ptr, Error_throw{});
			return *ptr;
		} else if constexpr(std::is_same_v<T, double>) {
			auto* ptr = YGDL_array_push_double(c_ptr, Error_throw{});
			return *ptr;
		} else if constexpr(std::is_same_v<T, Vec2>) {
			auto* ptr = reinterpret_cast<Vec2*>(YGDL_array_push_vec2(c_ptr, Error_throw{}));
			return *ptr;
		} else if constexpr(std::is_same_v<T, Vec3>) {
			auto* ptr = reinterpret_cast<Vec3*>(YGDL_array_push_vec3(c_ptr, Error_throw{}));
			return *ptr;
		} else if constexpr(std::is_same_v<T, String>) {
			auto* ptr = reinterpret_cast<String*>(YGDL_array_push_string(c_ptr, Error_throw{}));
			return *ptr;
		} else if constexpr(detail::is_dict<T>) {
			auto* ptr = YGDL_array_push_dict(c_ptr, Error_throw{});
			return {*ptr};
		} else if constexpr(detail::is_array<T>::value) {
			constexpr auto sub_type = static_cast<YGDL_Type_t>(detail::c_api_type<detail::array_value_type<T>>);
			auto* ptr = YGDL_array_push_array(c_ptr, sub_type, Error_throw{});
			return {*ptr};
		} else {
			static_assert(!std::is_same_v<T, T>, "Unsupported type");
		}
	}
	template <typename T>
	void Array_view<T>::push_back() requires(std::is_void_v<T>)
	{
		switch(YGDL_array_type(c_ptr(this))) {
			case YGDL_TYPE_NONE: break;
			case YGDL_TYPE_BOOL: cast_to<YGDL_Bool>().push_back(); break;
			case YGDL_TYPE_INT8: cast_to<std::int8_t>().push_back(); break;
			case YGDL_TYPE_INT32: cast_to<std::int32_t>().push_back(); break;
			case YGDL_TYPE_INT64: cast_to<std::int64_t>().push_back(); break;
			case YGDL_TYPE_FLOAT: cast_to<float>().push_back(); break;
			case YGDL_TYPE_DOUBLE: cast_to<double>().push_back(); break;
			case YGDL_TYPE_VEC2: cast_to<Vec2>().push_back(); break;
			case YGDL_TYPE_VEC3: cast_to<Vec3>().push_back(); break;
			case YGDL_TYPE_STRING: cast_to<String>().push_back(); break;
			case YGDL_TYPE_ARRAY: cast_to<Array_ref<void>>().push_back(); break;
			case YGDL_TYPE_DICT: cast_to<Dictionary_ref>().push_back(); break;
		}
	}

	template <typename T>
	template <typename Visitor>
	void Array_view<T>::visit_value_types(Visitor&& visitor)
	{
		switch(YGDL_array_type(c_ptr(this))) {
			case YGDL_TYPE_NONE: std::forward<Visitor>(visitor)(); break;
			case YGDL_TYPE_BOOL:
				std::forward<Visitor>(visitor)(Array_view<YGDL_Bool>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_INT8:
				std::forward<Visitor>(visitor)(Array_view<std::int8_t>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_INT32:
				std::forward<Visitor>(visitor)(Array_view<std::int32_t>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_INT64:
				std::forward<Visitor>(visitor)(Array_view<std::int64_t>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_FLOAT:
				std::forward<Visitor>(visitor)(Array_view<float>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_DOUBLE:
				std::forward<Visitor>(visitor)(Array_view<double>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_VEC2: std::forward<Visitor>(visitor)(Array_view<Vec2>(detail::c_ref(this))); break;
			case YGDL_TYPE_VEC3: std::forward<Visitor>(visitor)(Array_view<Vec3>(detail::c_ref(this))); break;
			case YGDL_TYPE_STRING:
				std::forward<Visitor>(visitor)(Array_view<String>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_ARRAY:
				std::forward<Visitor>(visitor)(Array_view<Array_view<void>>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_DICT:
				std::forward<Visitor>(visitor)(Array_view<Dictionary_ref>(detail::c_ref(this)));
				break;
		}
	}
	template <typename T>
	template <typename... Visitors>
	void Array_view<T>::visit_value_types(Visitors&&... visitors) requires(sizeof...(Visitors) > 1)
	{
		visit_value_types(overloaded{std::forward<Visitors>(visitors)...});
	}

	template <typename T>
	index_t Array_view<T>::find_index(const T* ptr)
	{
		auto index = ptr - data();
		if(index < 0 || index >= this->size()) {
			throw Out_of_bounds_exception("The address passed to erase is not part of the Array_view range.");
		}

		return static_cast<index_t>(index);
	}


	// IMPLEMENTATION: Array_ref
	template <typename T>
	template <typename U>
	Array_ref<T>& Array_ref<T>::operator=(const U& data) requires(!std::is_void_v<T>)
	{
		static_assert(std::is_assignable_v<T&, const typename U::value_type&>,
		              "Array<T>::operator= requires that the right hand side is a container of type T");

		this->resize(static_cast<index_t>(data.size()));
		std::copy(data.begin(), data.end(), this->begin());
		return *this;
	}

	template <typename T>
	template <typename Visitor>
	void Array_ref<T>::visit_value_types(Visitor&& visitor)
	{
		switch(YGDL_array_type(c_ptr(this))) {
			case YGDL_TYPE_NONE: std::forward<Visitor>(visitor)(); break;
			case YGDL_TYPE_BOOL:
				std::forward<Visitor>(visitor)(Array_ref<YGDL_Bool>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_INT8:
				std::forward<Visitor>(visitor)(Array_ref<std::int8_t>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_INT32:
				std::forward<Visitor>(visitor)(Array_ref<std::int32_t>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_INT64:
				std::forward<Visitor>(visitor)(Array_ref<std::int64_t>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_FLOAT:
				std::forward<Visitor>(visitor)(Array_ref<float>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_DOUBLE:
				std::forward<Visitor>(visitor)(Array_ref<double>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_VEC2: std::forward<Visitor>(visitor)(Array_ref<Vec2>(detail::c_ref(this))); break;
			case YGDL_TYPE_VEC3: std::forward<Visitor>(visitor)(Array_ref<Vec3>(detail::c_ref(this))); break;
			case YGDL_TYPE_STRING:
				std::forward<Visitor>(visitor)(Array_ref<String>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_ARRAY:
				std::forward<Visitor>(visitor)(Array_ref<Array_ref<void>>(detail::c_ref(this)));
				break;
			case YGDL_TYPE_DICT:
				std::forward<Visitor>(visitor)(Array_ref<Dictionary_ref>(detail::c_ref(this)));
				break;
		}
	}

} // namespace yggdrasill
