/// NOTE: This is the template implementation header for error.hpp and should not be included directly!
#pragma once

#ifndef YGDL_ERROR_HPP_INCLUDED
#include <yggdrasill/error.hpp>
#endif

namespace yggdrasill {

	// IMPLEMENTATION: Error
	inline Error::Error(YGDL_Error_code_t code, std::string_view message)
	  : error_(YGDL_error_create(code, detail::to_ygdl_string_view(message)))
	{
	}
	inline Error::Error(YGDL_Error e) : error_(YGDL_error_copy(&e)) {}
	inline constexpr Error::Error(Error&& rhs) noexcept : error_(rhs.error_)
	{
		rhs.error_ = YGDL_error_create_success();
	}
	inline Error::Error(const Error& rhs) : error_(YGDL_error_copy(&rhs.error_)) {}
	inline Error::~Error()
	{
		YGDL_error_destroy(&error_);
	}

	inline Error& Error::operator=(Error&& rhs) noexcept
	{
		if(&rhs == this)
			return *this;

		YGDL_error_destroy(&error_);
		error_ = std::exchange(rhs.error_, YGDL_error_create_success());
		return *this;
	}
	inline Error& Error::operator=(const Error& rhs)
	{
		if(&rhs == this)
			return *this;

		YGDL_error_destroy(&error_);
		error_ = YGDL_error_copy(&rhs.error_);
		return *this;
	}

	inline void Error::set_c_error(YGDL_Error* out) &&
	{
		if(out) {
			YGDL_error_destroy(out);
			*out = std::exchange(error_, YGDL_error_create_success());
		}
	}
	inline void Error::set_c_error(YGDL_Error* out) const&
	{
		if(out) {
			*out = YGDL_error_copy(&error_);
		}
	}

	[[nodiscard]] inline std::string_view Error::message() const
	{
		return detail::from_ygdl_string_view(YGDL_string_get(&error_.message));
	}

	[[nodiscard]] inline std::string_view Error::stacktrace() const
	{
		return detail::from_ygdl_string_view(YGDL_string_get(&error_.stacktrace));
	}

	inline void Error::throw_as_exception(std::string_view prefix)
	{
		if(!prefix.empty()) {
			auto msg = String(prefix) + error_.message;
			YGDL_string_set(&error_.message, detail::to_ygdl_string_view(msg.view()));
		}
		switch(code()) {
			case YGDL_ERROR_CODE_SUCCESS: break;
			case YGDL_ERROR_CODE_TYPE_MISMATCH: throw Type_mismatch_exception(std::move(error_));
			case YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS: throw Out_of_bounds_exception(std::move(error_));
			case YGDL_ERROR_CODE_PRECONDITION_VIOLATION:
				throw Precondition_violation_exception(std::move(error_));
			case YGDL_ERROR_CODE_MESH_CORRUPTION: throw Mesh_corruption_exception(std::move(error_));
			case YGDL_ERROR_CODE_INVALID_PLUGIN: throw Invalid_plugin_exception(std::move(error_));
			default:
				if(error_.code >= YGDL_ERROR_CODE_MODULE_ERRORS)
					throw Module_exception(std::move(error_));
				else
					throw Exception(std::move(error_));
		}
	}

	// IMPLEMENTATION: Helper functions
	template <typename ErrorMsgWriter>
	void set_error(YGDL_Error* out, YGDL_Error_code_t code, ErrorMsgWriter&& writer)
	{
		if(out && out->code == YGDL_ERROR_CODE_SUCCESS) {
			YGDL_error_set(out, code, detail::to_ygdl_string_view(writer()));
		}
	}

	template <typename F>
	void catch_error(YGDL_Error* error, F&& f)
	{
		try {
			std::invoke(std::forward<F>(f));

		} catch(const Exception& e) {
			e.error().set_c_error(error);
		} catch(const std::exception& e) {
			if(error) {
				YGDL_error_set(error, YGDL_ERROR_CODE_MODULE_ERRORS, detail::to_ygdl_string_view(e.what()));
			}
		} catch(...) {
			if(error) {
				YGDL_error_set(error, YGDL_ERROR_CODE_MODULE_ERRORS, YGDL_string_view_create("Unknown exception"));
			}
		}
	}

	template <typename F>
	auto catch_error(YGDL_Error* error, std::invoke_result_t<F> error_return, F&& f) -> std::invoke_result_t<F>
	{
		try {
			return std::invoke(std::forward<F>(f));

		} catch(Exception& e) {
			e.error().set_c_error(error);
		} catch(std::exception& e) {
			if(error) {
				YGDL_error_set(error, YGDL_ERROR_CODE_MODULE_ERRORS, detail::to_ygdl_string_view(e.what()));
			}
		} catch(...) {
			if(error) {
				YGDL_error_set(error, YGDL_ERROR_CODE_MODULE_ERRORS, YGDL_string_view_create("Unknown exception"));
			}
		}

		return std::move(error_return);
	}

} // namespace yggdrasill
