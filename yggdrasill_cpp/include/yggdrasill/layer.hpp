/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/world.h>

#include <yggdrasill/commons.hpp>
#include <yggdrasill/mesh.hpp>
#include <yggdrasill/utils.hpp>

#include <vector>

namespace yggdrasill {

	/**Constants that specify, which part of the mesh a structures layer is based on.\n
	 * This defines the type used to index the layer, as well as its length and behaviour when the mesh is modified.
	 * 
	 * There are multiple ways to link data to edges. The first distinction is between
	 * primal and dual edges, that is already part of their definition in the
	 * mesh (see `Edge_type`).
	 * But as it's also common for data to be identical in both directions of an edge,
	 * layers can also use edges as either directed or undirected. Undirected edge-layers
	 * discard the least-significant-bit of the index are thus only half as large as
	 * those that use directed edges.
	 * 
	 * \see `Const_layer_view`, `Layer_view` and `Layer_definition`
	 */
	enum class Ref_type : YGDL_Layer_ref_type_t {
		// clang-format off
		vertex               = YGDL_LAYER_REF_TYPE_VERTEX,               ///< The layer indexed by `Vertex`
		face                 = YGDL_LAYER_REF_TYPE_FACE,                 ///< The layer indexed by `Face`
		/// The layer indexed by `Primal_edge`, but data is identical/shared between an edge and their `Edge::sym()`
		edge_primal          = YGDL_LAYER_REF_TYPE_EDGE_PRIMAL,
		edge_primal_directed = YGDL_LAYER_REF_TYPE_EDGE_PRIMAL_DIRECTED, ///< The layer indexed by `Primal_edge`
		/// The layer indexed by `Dual_edge`, but data is identical/shared between an `Edge` and their `Edge::sym()`
		edge_dual            = YGDL_LAYER_REF_TYPE_EDGE_DUAL,
		edge_dual_directed   = YGDL_LAYER_REF_TYPE_EDGE_DUAL_DIRECTED    ///< The layer indexed by `Dual_edge`
		// clang-format on
	};

	namespace detail {
		template <typename T>
		constexpr bool can_be_stored_in_layer =
		        !std::is_reference_v<T> && c_api_type<T> != Type::none && c_api_type<T> != Type::string_t
		        && c_api_type<T> != Type::dict_t && c_api_type<T> != Type::array_t;

		// clang-format off
		template <Ref_type T> struct Key_type { static_assert(T == T, "Unexpected Ref_type"); };
		template<> struct Key_type<Ref_type::vertex>               { using type = Vertex; };
		template<> struct Key_type<Ref_type::face>                 { using type = Face; };
		template<> struct Key_type<Ref_type::edge_dual>            { using type = Dual_edge; };
		template<> struct Key_type<Ref_type::edge_dual_directed>   { using type = Dual_edge; };
		template<> struct Key_type<Ref_type::edge_primal>          { using type = Primal_edge; };
		template<> struct Key_type<Ref_type::edge_primal_directed> { using type = Primal_edge; };
		// clang-format on
	} // namespace detail

	/// Evaluates to the corresponding type used to index a layer with the given `Ref_type` \n
	/// The type will be one of: `Vertex` `Face` `Primal_edge` `Dual_edge`
	template <Ref_type RefType>
	using Layer_key_type = typename detail::Key_type<RefType>::type;

	/**Constants that specify how a structured layer reacts to changes to the underlying mesh that invalidate
	 * parts connected to the data.
	 * 
	 * Vertices and face are invalidated when they are removed from the mesh by `Mesh_view::collapse()`.
	 * Edges, on the other hand, are not only invalidated when they are removed but also when their connectivity
	 * changes, i.e. when they connect different vertices or faces than before.
	 * See `Mesh_view::flip()`, `Mesh_view::split()` and `Mesh_view::collapse()` for details when a specific edge
	 * may be invalidated.
	 * 
	 * \note The default value will usually be `reset_affected`\n
	 * \note Independent of the value, layers are still resized to match the number of vertices/faces/edges,
	 * as specified by `Ref_type`, and values will be updated, according to the chosen `Interpolation`.
	 * \see `Mesh_view`, `Layer_view` and `Layer_definition`
	 */
	enum class Invalidation : YGDL_Layer_invalidation_t {
		// clang-format off
		keep           = YGDL_LAYER_INVALIDATION_KEEP,           ///< Any existing value will be left untouched
		reset_affected = YGDL_LAYER_INVALIDATION_RESET_AFFECTED, ///< The value will be reset to `Layer_definition::initial()`
		reset_all      = YGDL_LAYER_INVALIDATION_RESET_ALL,      ///< **All** values in the layer will be reset to `Layer_definition::initial()`
		remove_layer   = YGDL_LAYER_INVALIDATION_REMOVE_LAYER    ///< The layer will be removed from the `World` entirely
		// clang-format on
	};

	/**Constants that specify how values for a structured layer can be computed based on existing ones.
	 * 
	 * This information is primarily used by the `Mesh_view` implementation, to computed values for new
	 * vertices/faces/edges or to update existing ones, e.g. when an edge is split.
	 * For these cases this enum defines how a new value can be computed based on two existing ones (src,dest)
	 * and an interpolation factor (t).
	 * 
	 * \note The default value will usually be `reset_affected`\n
	 * \note While the mesh implementation itself only interpolates values in the specific well-defined
	 * cases, modules may explicitly invoke interpolation using `World_view::invalidate_layers()`.
	 * Modules may assume that all layers interpolate similar to positional data. Thus, if your data can't
	 * be interpolated in these cases or your use-case isn't covered by the options below, you should use
	 * `Interpolation::reset_affected` to override the data with a well-known value, that can later be
	 * replaced with the actual value, as re-calculated by your own module.
	 * \see `Mesh_view::flip()`, `Mesh_view::split()` and `Mesh_view::collapse()` for details when this
	 * operation takes place.
	 */
	enum class Interpolation : YGDL_Layer_interpolation_t {
		// clang-format off
		dont_care      = YGDL_LAYER_INTERPOLATION_DONT_CARE,      ///< The new value is unspecified
		reset_affected = YGDL_LAYER_INTERPOLATION_RESET_AFFECTED, ///< The new value is set to `Layer_definition::initial()`
		reset_all      = YGDL_LAYER_INTERPOLATION_RESET_ALL,      ///< **All** values are set to `Layer_definition::initial()`
		remove_layer   = YGDL_LAYER_INTERPOLATION_REMOVE_LAYER,   ///< The entire layer is removed from the `World`
		keep_origin    = YGDL_LAYER_INTERPOLATION_KEEP_ORIGIN,    ///< The new value is ``origin``
		keep_dest      = YGDL_LAYER_INTERPOLATION_KEEP_DEST,      ///< The new value is ``dest``
		lerp           = YGDL_LAYER_INTERPOLATION_LERP,           ///< The new value is ``lerp(origin, dest, t)``
		/// The new value is ``lerp(normalized(origin), normalized(dest), t) * lerp(length(origin), length(dest), t)``
		nlerp          = YGDL_LAYER_INTERPOLATION_NLERP,
		min_value      = YGDL_LAYER_INTERPOLATION_MIN_VALUE,      ///< The new value is ``min(origin, dest)``
		max_value      = YGDL_LAYER_INTERPOLATION_MAX_VALUE,      ///< The new value is ``max(origin, dest)``
		min_weight     = YGDL_LAYER_INTERPOLATION_MIN_WEIGHT,     ///< The new value is ``t>=0.5 ? origin : dest``
		max_weight     = YGDL_LAYER_INTERPOLATION_MAX_WEIGHT,     ///< The new value is ``t>=0.5 ? dest : origin``
		// clang-format on
	};

	// forward declaration
	template <typename T, Ref_type RefType>
	class Layer_definition;

	/// Wrapper type that can be used as a key to access layer values without checking if the given key is valid.
	template <typename Key>
	struct Unchecked {
		Key key;

		constexpr operator Key() const noexcept { return key; }
		bool operator==(const Unchecked&) const  = default;
		auto operator<=>(const Unchecked&) const = default;
	};
	/// \skip
	template <typename Key>
	Unchecked(Key) -> Unchecked<Key>;

	/**An immutable non-owning view of a structured data layer.
	 * 
	 * A structured layer behaves like a sequence container, except that it's tightly coupled to the
	 * `Mesh_view` that it's based on. It's not indexed by an integer, but by one of the mesh element types
	 * (depending on `RefType`), and its size is automatically managed by the system and adjusted to match
	 * the underlying mesh. So, it's not possible to manually add/remove elements to a layer or construct it,
	 * except through the `World_view` object.
	 * 
	 * The data of the layer is managed by the world (see `World_view`) it's a part of and follows the
	 * copy-on-write semantic, outlined there.
	 * 
	 * A layers data will most often be accessed in a ranged-based-for-loop over the corresponding mesh
	 * element. To simplify this common use-case the helper function `range()` is provided.
	 * It's also possible to access elements directly using the `operator[]` are iterate directly over a
	 * layer using a range-based-for loop.
	 * 
	 * \warning While the whole sequence of values represented by this layer can be accessed, some of it's
	 * values may not be currently in use by the mesh, e.g. if a vertex was removed. While it is allowed to
	 * read/write these values, there are no guarantees on the stored values and none of the validations
	 * specified in the `Layer_definition` are checked.
	 * \tparam T The type of the stored values, which has to be one of the following: `YGDL_Bool`, `int8_t`,
	 * `int32_t`, `int64`, `float`, `double`, `Vec2`, `Vec3`, any enum whose underlying type is listed here
	 * \tparam RefType The part of the mesh this layer is linked to, which also defined the `key_type`
	 */
	template <typename T, Ref_type RefType>
	class Const_layer_view {
	  public:
		using const_view               = Const_layer_view<T, RefType>; ///< \skip
		using value_type               = const T; ///< The type of value accessible via this view
		static constexpr auto ref_type = RefType; ///< The mesh elements the data is linked to
		using key_type                 = Layer_key_type<RefType>; ///< The type used to index the container

		/// Converting constructor from a reference to the C-API struct
		constexpr /*implicit*/ Const_layer_view(const YGDL_Layer& layer) : layer_(&layer) {}

		/// \returns A copy of this layer's definition, as defined when it was created
		Layer_definition<T, RefType> definition() const;

		/**\returns `true` if the layer has a size of there
		 * \note As the size matches the number of vertices/faces/edge in the mesh, this will return `true`
		 * in all cases, except initially before the mesh is populated.
		 */
		[[nodiscard]] constexpr auto empty() const { return size() == 0; }
		/// \returns The number of elements in this layer, which will always be the same as the number of
		/// vertices/faces/edge in the mesh (depending on `RefType`).
		[[nodiscard]] constexpr auto size() const { return detail::c_ref(this).length; }

		/// \returns A pointer to the first element stored in this layer. This is the beginning of a valid
		/// range of length `size()`.
		[[nodiscard]] constexpr value_type* data() const;

		/// \returns A pointer to the first element, i.e. the begin iterator of this layers element sequence
		[[nodiscard]] constexpr value_type* begin() const { return data(); }
		/// \returns A pointer to one-past the last element, i.e. the end iterator of this layers element sequence
		[[nodiscard]] constexpr value_type* end() const { return begin() + size(); }

		/**\returns The value currently stored for the given `key`.
		 * \see `Vertex`, `Face`, `Primal_edge` and `Dual_edge` for possible key types.
		 * \overload_specific
		 * \throws Out_of_bounds_exception If the given key is not valid.
		 */
		[[nodiscard]] constexpr value_type& operator[](key_type key) const;
		/// \attention The behaviour is undefined if `key` is not a valid index into this layer.
		[[nodiscard]] constexpr value_type& operator[](Unchecked<key_type> key) const noexcept;

		[[nodiscard]] constexpr const_view as_const() const noexcept { return *this; }

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
		 * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
		 */
		[[nodiscard]] constexpr friend const YGDL_Layer* c_ptr(const Const_layer_view& l) { return l.layer_; }
		[[nodiscard]] constexpr friend const YGDL_Layer* c_ptr(const Const_layer_view* l)
		{
			return l ? l->layer_ : nullptr;
		}

	  private:
		const YGDL_Layer* layer_;
	};

	/// A mutable non-owning view of a structured data layer.
	/// \see `Const_layer_view` for the read-access part of the API
	template <typename T, Ref_type RefType>
	class Layer_view : public Const_layer_view<T, RefType> {
	  public:
		using const_view = Const_layer_view<T, RefType>; ///< \skip
		using value_type = T;                            ///< The type of value accessible via this view
		using key_type = typename Const_layer_view<T, RefType>::key_type; ///< The type used to index the container

		/// Converting constructor from a reference to the C-API struct
		/*implicit*/ constexpr Layer_view(YGDL_Layer& layer) : Const_layer_view<T, RefType>(layer) {}

		/// \returns A pointer to the first element stored in this layer. This is the beginning of a valid
		/// range of length `size()`.
		constexpr value_type* data();
		using const_view::data;

		/// \returns A pointer to the first element, i.e. the begin iterator of this layers element sequence
		constexpr value_type* begin();
		using const_view::begin;

		/// \returns A pointer to one-past the last element, i.e. the end iterator of this layers element sequence
		constexpr value_type* end();
		using const_view::end;

		/**\returns The value currently stored for the given `key`.
		 * \see `Vertex`, `Face`, `Primal_edge` and `Dual_edge` for possible key types.
		 * \overload_specific
		 * \throws Out_of_bounds_exception If the given key is not valid.
		 */
		[[nodiscard]] constexpr value_type& operator[](key_type key);
		/// \overload_specific
		/// \attention The behaviour is undefined if `key` is not a valid index into this layer.
		[[nodiscard]] constexpr value_type& operator[](Unchecked<key_type> key) noexcept;
		using const_view::operator[];

		/// Sets all values in this layer to the passed value `v` or the default/initial value from the layer's definition.
		void reset(std::optional<T> v = {})
		{
			std::fill(begin(), end(), v.value_or(this->definition().initial()));
		}

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
		 * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
		 */
		[[nodiscard]] constexpr friend YGDL_Layer* c_ptr(Layer_view v)
		{
			// we can safely cast away const here, because (inside the Layer_view class) we know that layer_ was initialized from a pointer to non-const
			return const_cast<YGDL_Layer*>(c_ptr(static_cast<const const_view&>(v)));
		}
		[[nodiscard]] constexpr friend YGDL_Layer* c_ptr(Layer_view* v)
		{
			return const_cast<YGDL_Layer*>(c_ptr(static_cast<const const_view*>(v)));
		}
	};


	/**Definition of a layer, that specifies what data is stored in it, what values are valid and how it will
	 * be updated when the underlying mesh changes.
	 * 
	 * A layer is identified only by their given string ID. But all usages of a layer have to use the same or
	 * a compatible definition. For two definitions to be compatible, they have to have the same value for
	 * `data_type()` and `ref_type`. Other difference don't generate an error, but may produce a warning.
	 * Only the definition used to create a layer is used to define validation, interpolation and invalidation
	 * information.
	 * 
	 * An object of this type is used when constructing a new layer by calling `World_view::lock_layer`,
	 * stored internally in the world object and used on all later accesses to check if the stored layer
	 * matches the requested definition.
	 * 
	 * The setter methods all return a reference to `this` and are designed as a fluent API that can be used
	 * for construction:
	 * 
	 * \include_example{layer_definition_example}
	 * 
	 * \tparam T The type of the stored values, which has to be one of the following: `YGDL_Bool`, `int8_t`,
	 * `int32_t`, `int64`, `float`, `double`, `Vec2`, Vec3`, any enum whose underlying type is listed here
	 * \tparam RefType The part of the mesh this layer is linked to, which also defined the `key_type`
	 * 
	 * \note When a definition is used to create a new layer, a copy of it is made and retained inside the
	 * `World` object. This includes a deep-copy of its ID and description. So there are no requirements
	 * for the lifetime of these strings, i.e. they don't have to outlive the respective function call.
	 * \warning The min/max value and other validation options only apply to layer elements for
	 * vertices/faces/edges that are currently being used by the mesh. If that is not the case,
	 * i.e. when `Vertex::valid()`/`Face::valid()`/`Edge::valid()` returns ``false`` no  guarantee is made
	 * by the implementation concerning the value of the corresponding layer values. Specifically, it's
	 * allowed to read and write the values, but the read value may be outside the specified range or be
	 * NaN/Inf, even if that wouldn't be allowed normally.
	 */
	template <typename T, Ref_type RefType>
	class Layer_definition {
	  public:
		static_assert(detail::can_be_stored_in_layer<T>, "The given type T can't be stored in a layer.");

		using layer_type = Layer_view<T, RefType>;             ///< The type of the mutable view of this layer
		using const_layer_type = Const_layer_view<T, RefType>; ///< The type of the immutable view of this layer
		using value_type = T;                                  ///< The type of the value stored in this layer
		using key_type   = typename layer_type::key_type;      ///< The index type used to access elements
		static constexpr auto ref_type = RefType;              ///< The mesh elements the data is linked to

		/**Creates a new layer-definition with the given unique `id` and the following default values:\n
		 * - Elements are value-initialized, i.e. zero/false or equivalent
		 * - No validation of min/max values
		 * - NaN and INF are forbidden
		 * - Values are reset to the `initial()` value, when they are invalidated (see `Invalidation::reset_affected`)
		 * - Modified elements are reset to the `initial()` value and never automatically
		 *   computed/interpolated (see `Interpolation::reset_affected`)
		 */
		constexpr explicit Layer_definition(std::string_view id);

		/// Converting constructor from a reference to the C-API struct.
		constexpr Layer_definition(const YGDL_Layer_definition& c_type);

		/// \returns The unique ID, used to identify this layer
		[[nodiscard]] constexpr std::string_view id() const;

		/**Getter/Setter for the layer's description.
		 * \note The description can be arbitrary text and is only intended for usage in UIs and
		 * self-documenting data files.
		 */
		constexpr Layer_definition&              description(std::string_view description);
		[[nodiscard]] constexpr std::string_view description() const;

		/// Getter/Setter for the layer's initial value.\n
		/// This value is used for manually inserted vertices and faces and for elements that are explicitly reset.
		constexpr Layer_definition&        initial(value_type initial);
		[[nodiscard]] constexpr value_type initial() const;

		/// Getter/Setter for the layer's minimal value (inclusive).
		/// \note If this value is never set, no corresponding validation will take place.
		constexpr Layer_definition&                       min(value_type min);
		[[nodiscard]] constexpr std::optional<value_type> min() const;

		/// Getter/Setter for the layer's maximum value (inclusive).
		/// \note If this value is never set, no corresponding validation will take place.
		constexpr Layer_definition&                       max(value_type max);
		[[nodiscard]] constexpr std::optional<value_type> max() const;

		/// Getter/Setter for the layer's minimal length (inclusive), for vector types.
		/// \note If this value is never set, no corresponding validation will take place.
		constexpr Layer_definition&                  min_length(float min);
		[[nodiscard]] constexpr std::optional<float> min_length() const;

		/// Getter/Setter for the layer's maximum length (inclusive), for vector types.
		/// \note If this value is never set, no corresponding validation will take place.
		constexpr Layer_definition&                  max_length(float min);
		[[nodiscard]] constexpr std::optional<float> max_length() const;

		/// Getter/Setter for if floating point values (incl. vector types) are allowed to be NaN.
		constexpr Layer_definition&  allow_nan(bool allow);
		[[nodiscard]] constexpr bool allow_nan() const;

		/// Getter/Setter for if floating point values (incl. vector types) are allowed to be infinite.
		constexpr Layer_definition&  allow_inf(bool allow);
		[[nodiscard]] constexpr bool allow_inf() const;

		/// Getter/Setter for what should happen to the layers values when the part of the mesh they are
		/// based on is invalidated by modifications.
		[[nodiscard]] constexpr Invalidation invalidation() const;
		constexpr Layer_definition&          invalidation(Invalidation v);

		/// Getter/Setter for how new values for this layer should be automatically computed if a new
		/// vertex/face/edge (depending on `RefType`) is inserted into the mesh or the topology changes.
		[[nodiscard]] constexpr Interpolation interpolation() const;
		constexpr Layer_definition&           interpolation(Interpolation v);

		/// \returns The enum value corresponding to `T`, that specifies what the type of stored values is.
		[[nodiscard]] constexpr Type data_type() const;

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if the argument is `nullptr` (for the pointer overload)
		 */
		[[nodiscard]] constexpr friend const YGDL_Layer_definition* c_ptr(const Layer_definition& l)
		{
			return &l.info_;
		}
		[[nodiscard]] constexpr friend const YGDL_Layer_definition* c_ptr(const Layer_definition* l)
		{
			return l ? &l->info_ : nullptr;
		}

	  private:
		YGDL_Layer_definition info_;
	};

	namespace detail {
		template <typename, template <typename, auto> class Z>
		struct is_layer_definition : std::false_type {};

		template <typename T, Ref_type RefType, template <typename, auto> class Z>
		struct is_layer_definition<Z<T, RefType>, Z> : std::true_type {};
	} // namespace detail

	/// Concept that checks if `T` is a `Layer_definition`
	template <typename T>
	concept Any_layer_definition = detail::is_layer_definition<T, Layer_definition>::value;

	/// The type for mutable views of the given layer definition type.
	template <Any_layer_definition LayerInfo>
	using view_of = typename LayerInfo::layer_type;

	/// The type for immutable views of the given layer definition type.
	template <Any_layer_definition LayerInfo>
	using const_view_of = typename LayerInfo::const_layer_type;

	/// The type for mutable views of the given layer definition instance.
	template <auto& LayerInfo>
	using view_of_def = view_of<std::remove_cvref_t<decltype(LayerInfo)>>;

	/// The type for immutable views of the given layer definition instance.
	template <auto& LayerInfo>
	using const_view_of_def = const_view_of<std::remove_cvref_t<decltype(LayerInfo)>>;


	/**A structured data layer for temporary data, that isn't managed by the system but manually `reset` and
	 * owned by the object itself.
	 * 
	 * They main use-case for this class is storing temporary data that is linked to a specific element of the
	 * mesh (vertex/face/edge) but has a short lifetime and thus doesn't need to be automatically updated
	 * or resized when the topology changes.
	 * 
	 * \tparam T The type of the stored values. In addition to the types supported by normal layers, this class
	 * also allows other arbitrary copyable and default-constructable types. But for these types the method
	 * `Const_layer_view<T, RefType>::definition()` may not be instantiated.
	 * \tparam RefType The part of the mesh this layer is linked to, which also defined the `key_type`.
	 * 
	 * \attention You have to manually call `reset()` after the mesh was modified, as the layer is not
	 * managed by the system and not resized automatically. The behaviour is undefined if the layer is used
	 * after the mesh was modified and `reset()` wasn't called.
	 * \note It's usually a good idea to cache objects of these type in your module to avoid frequent re-allocations.
	 */
	template <typename T, Ref_type RefType>
	class Temporary_layer : public Layer_view<T, RefType> {
	  public:
		using const_view = Const_layer_view<T, RefType>; ///< \skip

		/// Constructs an empty layer. You have to call `reset()` before the layer can be used!
		Temporary_layer();
		/// Constructs a layer, setting the dimension to match the given `mesh` and initializing all values with `v`.
		explicit Temporary_layer(Const_mesh_view mesh, const T& v = {}) : Temporary_layer()
		{
			reset(mesh, v);
		}
		/**Constructs a layer, setting the dimension to match the given `mesh` and initializing all values
		 * with the result of invoking `value_factory` on every key.
		 * \param mesh The mesh that is used to define the size of the layer
		 * \param value_factory The function that is used to define the values. The function will be invoked.
		 * exactly once for every valid element in the layer (depending on `RefType`, determined by
		 * `Vertex::valid()`, `Face::valid()` or `Edge::valid()`).
		 */
		template <std::invocable<Layer_key_type<RefType>> Factory>
		Temporary_layer(Const_mesh_view mesh, Factory&& value_factory) : Temporary_layer()
		{
			reset(mesh, value_factory);
		}

		/**Resets the layer, which removes all elements, resizes the layer and inserts a matching number of
		 * new elements.
		 * \param mesh The mesh that is used to define the size of the layer
		 * \param value_factory The function that is used to define the values. The function will be invoked.
		 * exactly once for every valid element in the layer (depending on `RefType`, determined by
		 * `Vertex::valid()`, `Face::valid()` or `Edge::valid()`).
		 */
		template <std::invocable<Layer_key_type<RefType>> Factory>
		void reset(Const_mesh_view mesh, Factory&& value_factory);
		/// \param v The initial value, that will be copied into every element
		void reset(Const_mesh_view mesh, const T& v = {});

	  private:
		detail::Aligned_vector<T> data_;
		YGDL_Layer                dummy_layer_;

		void           on_reset();
		static index_t get_size(Const_mesh_view mesh);
	};

	/// Checks whether the given layers all share the same `Ref_type` and can thus can be iterated over together.
	template <typename Layer, typename... Layers>
	constexpr auto joinable_layers = ((Layer::ref_type == Layers::ref_type) && ...);

	/**
	 * Creates a range wrapper that can be used to iterate over the vertices/edges/faces as well as the
	 * corresponding values in the passed layers. The iteration will automatically skip elements that
	 * are not currently used by the `mesh`.
	 *
	 * The other parameters have to be one or more instances of either `Const_layer_view` or `Layer_view`.
	 * All passed layers have to use the same `Ref_type`.
	 *
	 * \returns A range over elements of type `std::tuple<Key_type, Layers::value_type&...>`, where `Key_type`
	 * is either `Vertex`, `Face`, `Primal_edge` or `Dual_edge`.
	 *
	 * \code{cpp}
	 * for(auto&& [vertex, position, velocity] : range(mesh, positions, velocities)) {
	 * 	// ...
	 * }
	 * \endcode
	 */
	template <typename... Layers>
	requires(joinable_layers<Layers...>)
	[[nodiscard]] auto range(Const_mesh_view mesh, Layers... layers);

	/**
	 * Creates a range wrapper that can be used to iterate over multiple layers at once, **without** checking
	 * the validity of elements.
	 *
	 * Contrary to `range()` this function will iterate over **all** values in the layers, including those
	 * that are currently unused by the mesh and may contain invalid data.
	 *
	 * \param layers One or more instances of either `Const_layer_view` or `Layer_view`.
	 * All passed layers have to use the same `Ref_type`.
	 *
	 * \returns A range over elements of type `std::tuple<Layers::value_type&...>`
	 *
	 * \code{cpp}
	 * for(auto&& [vertex, position, velocity] : range(mesh, positions, velocities)) {
	 * 	// ...
	 * }
	 * \endcode
	 */
	template <typename FirstLayer, typename... Layers>
	requires(joinable_layers<Layers...>)
	[[nodiscard]] constexpr auto unchecked_range(Layers&... layers);

} // namespace yggdrasill

#define YGDL_LAYER_HPP_INCLUDED
#include <yggdrasill/layer.inl>
