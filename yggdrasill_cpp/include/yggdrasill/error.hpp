/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/error.h>

#include <yggdrasill/string.hpp>

#include <functional>
#include <stdexcept>
#include <string>


namespace yggdrasill {

	/**Representation of any non-fatal error that occurs in the framework or its plugins.\n
	 * An error consists of an error-code that describes the general category of error and a
	 * human-readable message with additional details.\n
	 * This type functions as a bridge between C++-Exception based error handling and C-Style return codes.
	 */
	class Error {
	  public:
		/// Constructs an empty error, i.e. a success
		constexpr Error() : error_(YGDL_error_create_success()) {}
		/// Constructs an error with the given `code` and error `message`
		Error(YGDL_Error_code_t code, std::string_view message);
		Error(YGDL_Error e);                   ///< Converting constructor from the C-API error type
		constexpr Error(Error&& rhs) noexcept; ///< Move constructor \skip
		Error(const Error& rhs);               ///< Copy constructor \skip
		~Error();                              ///< Destructor \skip

		Error& operator=(Error&& rhs) noexcept; ///< Move assignment \skip
		Error& operator=(const Error& rhs);     ///< Copy assignment \skip

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if the argument is `nullptr` (for the pointer overload), otherwise
		 * the returned pointer is guaranteed to not be null
		 */
		[[nodiscard]] constexpr friend const YGDL_Error* c_ptr(const Error& e) { return &e.error_; }
		[[nodiscard]] constexpr friend YGDL_Error*       c_ptr(Error& e) { return &e.error_; }
		[[nodiscard]] constexpr friend const YGDL_Error* c_ptr(const Error* e)
		{
			return e ? &e->error_ : nullptr;
		}
		[[nodiscard]] constexpr friend YGDL_Error* c_ptr(Error* e) { return e ? &e->error_ : nullptr; }

		/**Sets the C-API error `out` to the same state currently contained in this object.
		 * \overload_specific
		 * If `this` is an r-value, the state is moved instead of copied and `this` will be empty/success afterwards
		 */
		void set_c_error(YGDL_Error* out) &&;
		void set_c_error(YGDL_Error* out) const&;

		/// \returns The stored error code
		[[nodiscard]] constexpr YGDL_Error_code_t code() const { return error_.code; }
		/// \returns The stored detail message
		[[nodiscard]] std::string_view message() const;
		/// \returns The stored stacktrace (if the library was build with support for std::stacktrace)
		[[nodiscard]] std::string_view stacktrace() const;
		/// \returns `true` if this error is currently not set, i.e. it represents a success
		[[nodiscard]] constexpr bool success() const { return code() == YGDL_ERROR_CODE_SUCCESS; }

		/// \returns `true` if this error is currently not set, i.e. it represents a success
		constexpr explicit operator bool() const { return success(); }

		/// \throws Exception If this error is set, the corresponding sub-type of `Exception` is thrown
		/// \attention This method resets this object to success, i.e. its moved-from-state.
		void throw_as_exception(std::string_view prefix = {});

	  private:
		YGDL_Error error_;
	};

	/// An empty error that represents a success
	inline constexpr auto success = YGDL_error_create_success();


	/// Base class for all Yggdrasill specific exceptions
	class Exception : public std::runtime_error {
	  public:
		explicit Exception(YGDL_Error_code_t code, const std::string& msg)
		  : std::runtime_error(msg), error_(code, msg)
		{
		}
		explicit Exception(Error error)
		  : std::runtime_error(std::string(error.message()) + "\nStacktrace:\n" + std::string(error.stacktrace()))
		  , error_(std::move(error))
		{
		}

		/// \returns The stored error code
		[[nodiscard]] YGDL_Error_code_t code() const { return error_.code(); }

		/// \returns The stored error code
		[[nodiscard]] std::string_view message() const { return error_.message(); }

		/// \returns The stored error code
		[[nodiscard]] std::string_view stacktrace() const { return error_.stacktrace(); }

		/// \returns An error object that contains both the code and the detail message of the exception
		[[nodiscard]] const Error& error() const { return error_; }

	  private:
		Error error_;
	};

	/// Thrown if the runtime type doesn't match the expected type or the one specified at compile time
	class Type_mismatch_exception : public Exception {
	  public:
		using Exception::Exception;
		explicit Type_mismatch_exception(const std::string& msg)
		  : Exception(YGDL_ERROR_CODE_TYPE_MISMATCH, msg)
		{
		}
	};
	/// Thrown if an array is accessed with an invalid index
	class Out_of_bounds_exception : public Exception {
	  public:
		using Exception::Exception;
		explicit Out_of_bounds_exception(const std::string& msg)
		  : Exception(YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS, msg)
		{
		}
	};
	/**Thrown if a precondition specified in the API wasn't satisfied.\n
	 * This usually indicates a programming error, either by the implementor or the client, for example if
	 * an invalid face is added to a `Mesh` or a `World` layer was requested as required but was missing
	 */
	class Precondition_violation_exception : public Exception {
	  public:
		using Exception::Exception;
		explicit Precondition_violation_exception(const std::string& msg)
		  : Exception(YGDL_ERROR_CODE_PRECONDITION_VIOLATION, msg)
		{
		}
	};
	/// Thrown if either a method is called to travers or modify an invalid mesh or if an operation would
	/// invalidate the mesh and was thus aborted
	class Mesh_corruption_exception : public Exception {
	  public:
		using Exception::Exception;
		explicit Mesh_corruption_exception(const std::string& msg)
		  : Exception(YGDL_ERROR_CODE_MESH_CORRUPTION, msg)
		{
		}
	};
	/// Thrown if a plugin couldn't be loaded, usually because it's either not a valid/compatible
	/// shared library or because the entry function wasn't found
	class Invalid_plugin_exception : public Exception {
	  public:
		using Exception::Exception;
		explicit Invalid_plugin_exception(const std::string& msg)
		  : Exception(YGDL_ERROR_CODE_INVALID_PLUGIN, msg)
		{
		}
	};
	/// Thrown if an error occurred reading/writing a file. Usually either because of an
	/// invalid/inaccessible path or an unexpected device error
	class Io_error_exception : public Exception {
	  public:
		using Exception::Exception;
		explicit Io_error_exception(const std::string& msg) : Exception(YGDL_ERROR_CODE_IO_ERROR, msg) {}
	};
	/// Thrown for any errors that occur inside a user-defined module
	class Module_exception : public Exception {
	  public:
		using Exception::Exception;
		explicit Module_exception(const std::string& msg) : Exception(YGDL_ERROR_CODE_MODULE_ERRORS, msg) {}
	};


	/**Helper type, that can be passed to C-API functions instead of an `YGDL_Error*` and throws any
	 * assigned error as an exception at destruction.
	 * \attention Special care must be taken, because this type might throw on destruction!
	 * \attention (1) There must not be any other potentially throwing calls between passing this object and its destruction.
	 * \attention (2) The value returned by the C-API function must not be used, before this object is
	 * destroyed or its value checked, because the C function might have returned an invalid or NULL value on error
	 */
	class Error_throw {
	  public:
		/// Constructs an empty error, i.e. a success
		Error_throw() : error_(YGDL_error_create_success()) {}
		/// \throws Exception If an error has been set through the C-API pointer
		~Error_throw() noexcept(false) { error_.throw_as_exception(); }

		/// Implicit conversion to a pointer to the C-API error type, to be passed to a C-API function
		operator YGDL_Error*() { return c_ptr(error_); }

	  private:
		Error error_;
	};

	/**Helper function to set an error. The callable passed in the last parameter is only evaluated
	 * if `out` isn't `nullptr` or already set to a different error.
	 * \code{cpp}
	 * set_error(c_error_ptr, YGDL_ERROR_CODE_MODULE_ERRORS, [&] {
	 *     return "Error in "s + some_large_string + ", caused by: " + some_expansive_operation();
	 * });
	 * \endcode
	 */
	template <typename ErrorMsgWriter>
	void set_error(YGDL_Error* out, YGDL_Error_code_t code, ErrorMsgWriter&& writer);

	/**Helper function to convert from an exception to the C-API error type.\n
	 * Invokes the callable `f`, catching any thrown exceptions and setting `error` to the corresponding code.\n
	 * For exception derived from either `Exception` or `std::exception` the message/what is conserved
	 * and returned as the error message.
	 * \overload_specific
	 */
	template <typename F>
	void catch_error(YGDL_Error* error, F&& f);
	/**\overload_specific
	 * \returns If no exception is thrown, the value returned by `f` is forwarded to the caller,
	 * otherwise `error_return` is returned
	 */
	template <typename F>
	auto catch_error(YGDL_Error* error, std::invoke_result_t<F> error_return, F&& f) -> std::invoke_result_t<F>;

} // namespace yggdrasill

#define YGDL_ERROR_HPP_INCLUDED
#include <yggdrasill/error.inl>
