/**********************************************************************************
* Copyright (c) 2023 Florian Oetke                                               *
* This file is part of the Yggdrasill project and licensed under the MIT License *
* See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/serialization.h>

#include <yggdrasill/dictionary.hpp>
#include <yggdrasill/error.hpp>
#include <yggdrasill/string.hpp>
#include <yggdrasill/world.hpp>

#include <filesystem>
#include <memory>
#include <span>
#include <string_view>

namespace yggdrasill {

	/// \returns true if the library was compiled with serialization support.
	/// Support is usually included, but can be explicitly disabled by setting the CMake variable ``YGGDRASILL_SERIALIZATION`` to ``OFF``.
	/// \attention If this function returns false, no other functions in this header may be called.
	inline bool serialization_supported();

	/// A handle used to identify a world stored in the `YGDL_Serializer` archive.
	/// \attention Handles from different `YGDL_Serializer` instances or archives are *not* comparable nor compatible!
	enum class Serialized_world_handle : YGDL_Serializer_world_ref {};

	class Byte_reader;

	/**An object that can be used to read data from a previously saved archive file.\n
	 * An archive may contain an arbitrary number of `World` and `Dictionary` (config) as well as user-defined
	 * objects. Each stored object has a unique key that is used to retrieve it.
	 * \note The domains of keys is disjoint between different archives/serializers and entry types. 
	 * Thus, comparing two keys from different archives or for different types (e.g. user-data and config) 
	 * or using one to access data in the other is not a meaningful operation.
	 */
	class Deserializer {
	  public:
		/// Opens a deserializer that reads from the file at the given path.
		/// \throws Io_error_exception If the file doesn't exist, can't be read, or isn't a valid archive file.
		explicit Deserializer(const std::filesystem::path& path) : Deserializer(path, false, false) {}

		/// Invokes the passed callable once for each user-data entry stored in the archive, with their respective keys as the parameter.
		void foreach_userdata(std::invocable<std::string_view> auto&&) const;
		/// Invokes the passed callable once for each configuration entry stored in the archive, with their respective keys as the parameter.
		void foreach_config(std::invocable<std::string_view> auto&&) const;
		/// Invokes the passed callable once for each world entry stored in the archive, with their respective keys as the parameter.
		void foreach_world(std::invocable<Serialized_world_handle> auto&&) const;

		/**Reads the user-data entry identified by the given `key`.
		 * \throws Io_error_exception If the entry couldn't be read.
		 * \throws Out_of_bounds_exception If no user-data entry with this key exists in the archive.
		 * \overload_specific
		 * \returns The entry's bytes.
		 */
		auto read_userdata(std::string_view key) -> std::vector<std::byte>;
		/// \overload_specific
		/// If the entry exists and can be opened, the callback will be invoked exactly once with an
		/// object that can be used to access the entries bytes.
		void read_userdata(std::string_view key, std::invocable<Byte_reader&> auto&&);

		/**Reads the configuration entry identified by the given `key`.
		 * \throws Io_error_exception If the entry couldn't be read.
		 * \throws Out_of_bounds_exception If no user-data entry with this key exists in the archive.
		 * \overload_specific
		 * \returns A new `Dictionary` object containing the read data.
		 */
		auto read_config(std::string_view key) -> Dictionary;
		/**\overload_specific
		 * \note The data is read into the passed object, which is cleared before reading. Also, some of its
		 * storage is potentially reused and all references to the object itself remain valid.
		 * \attention This function invalidates all pointers/references to the passed dictionary's values and sub-objects.
		 * \attention If the operation fails, `world` may have been partially updated and will be in an unspecified but valid state.
		 */
		void read_config(std::string_view key, Dictionary_view);

		/**Reads the world entry identified by the given `key`.
		 * \throws Io_error_exception If the entry couldn't be read.
		 * \throws Out_of_bounds_exception If no user-data entry with this key exists in the archive.
		 * \note The serializer keeps a cache of the individual parts (mesh, layers, ...) of previously loaded worlds and reuses them.
		 * \overload_specific
		 * \returns A new `World` object containing the read data.
		 */
		auto read_world(Serialized_world_handle key) -> World;
		/**\overload_specific
		 * \note The data is read into the passed object, which is cleared before reading. Also, some of its
		 * storage is potentially reused and all references to the object itself remain valid.
		 * \attention This function invalidates all pointers/references to the passed world's values and sub-objects.
		 * \attention If the operation fails, `world` may have been partially updated and will be in an unspecified but valid state.
		 */
		void read_world(Serialized_world_handle key, World_view);

		/// Removes entries from the internal read-caches, that aren't used by any external objects.\n
		/// The system keeps copy-on-write pointer for the individual parts of worlds read with
		/// `read_world()`, to reuse them for later read-requests.
		void shrink_read_caches();

		/// Closes this deserializer.
		/// \attention After this function was called, none of the other functions, except the destructor and assignment operators, may be invoked.
		void close() { serializer_.reset(); }

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
		 * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
		 */
		[[nodiscard]] friend YGDL_Serializer* c_ptr(const Deserializer& s) { return s.serializer_.get(); }
		[[nodiscard]] friend YGDL_Serializer* c_ptr(const Deserializer* s)
		{
			return s ? s->serializer_.get() : nullptr;
		}

	  protected:
		Deserializer(const std::filesystem::path& path, bool writeable, bool overwrite);

	  private:
		std::unique_ptr<YGDL_Serializer, void (*)(YGDL_Serializer*)> serializer_;
	};

	/// A reader instantiated by `Deserializer::read_userdata` that can be used to read bytes from an entry.
	class Byte_reader {
	  public:
		/// \skip Constructor from the C-API representation
		Byte_reader(void* reader_ctx, YGDL_Serializer_reader_req* req)
		  : reader_ctx_(reader_ctx), req_(req), remaining_bytes_(req(reader_ctx, 0, nullptr))
		{
		}

		/// \returns The number of bytes that haven't yet been read from the entry.
		std::size_t remaining_bytes() const { return remaining_bytes_; }

		/// Reads bytes into the given span until it is either full or there are no more bytes available.
		/// \returns A subspan of the parameter that represents the bytes that were actually read.
		std::span<std::byte> read_into(std::span<std::byte>);
		/// \overload_specific
		/// The passed container is converted to a byte-span using `std::as_writable_bytes()`, passed to
		/// overload [1] and resized afterwards to only contain elements that have been read to completion.
		/// \attention If the remaining number of bytes is not a multiple of `sizeof(T)` some bytes might be discarded.
		template <typename T>
		void read_into(std::vector<T>&);

	  private:
		void*                       reader_ctx_;
		YGDL_Serializer_reader_req* req_;
		std::size_t                 remaining_bytes_;
	};


	/**An object that can be used to both write data to and archive file and read from it.\n
	 * An archive may contain an arbitrary number of `World` and `Dictionary` (config) as well as user-defined
	 * objects. Each stored object has a unique key that is used to retrieve it.
	 * 
	 * \note The domains of keys is disjoint between different archives/serializers and entry types. 
	 * Thus, comparing two keys from different archives or for different types (e.g. user-data and config) 
	 * or using one to access data in the other is not a meaningful operation.
	 * 
	 * \attention Write new entries into an archive invalidates its internal index/directory/header, which is
	 * only rewritten when the `Serializer` ist destroyed or `flush()` is called. Thus, if the program is terminated 
	 * prematurely the archive will be in an invalid state, if any entries were written since the last flush. 
	 */
	class Serializer : public Deserializer {
	  public:
		/// Creates a Serializer that writes to the given path. If the file already exists, it will be deleted.
		/// \throws Io_error_exception If the file couldn't be created.
		static Serializer open_overwrite(const std::filesystem::path& path) { return {path, true}; }
		/// Creates a Serializer that writes to the given path. If the file already exists, it is read and its content is preserved.
		/// \throws Io_error_exception If the file couldn't be created or if it already exists and couldn't be read.
		static Serializer open_read_write(const std::filesystem::path& path) { return {path, false}; }

		/// Writes all buffered data and the archive directory to the underlying file.
		/// \throws Io_error_exception If the file couldn't be reopened, either because of an OS-level IO
		/// error or because the archive directory couldn't be written successfully.
		void flush();

		/// Creates a new entry for user-defined arbitrary data associated with the given `key` and writes the given `bytes`.
		/// \throws Io_error_exception If the entry couldn't be written because of on OS-level IO error.
		/// \throws Precondition_violation_exception If a user-data entry with the given `key` already exists.
		void write_userdata(std::string_view key, std::span<std::byte> bytes);
		/// Creates a new entry for a configuration, associated with the given `key`, and writes the given `dict`.
		/// \throws Io_error_exception If the entry couldn't be written because of on OS-level IO error.
		/// \throws Precondition_violation_exception If a configuration entry with the given `key` already exists.
		void write_config(std::string_view key, Const_dictionary_view dict);
		/// Creates a new entry for a simulation state and writes the given `world`.
		/// \returns A handle that can later be used to retrieve the stored `World`.
		/// \throws Io_error_exception If the entry couldn't be written because of on OS-level IO error.
		/// \throws Precondition_violation_exception If a world entry with the given `key` already exists.
		auto write_world(Const_world_view world) -> Serialized_world_handle;


		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
		 * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
		 */
		[[nodiscard]] friend YGDL_Serializer* c_ptr(const Serializer& s)
		{
			return c_ptr(static_cast<const Deserializer&>(s));
		}
		[[nodiscard]] friend YGDL_Serializer* c_ptr(const Serializer* s)
		{
			return c_ptr(static_cast<const Deserializer*>(s));
		}

	  private:
		Serializer(const std::filesystem::path& path, bool overwrite) : Deserializer(path, true, overwrite) {}
	};

} // namespace yggdrasill

#define YGDL_SERIALIZATION_HPP_INCLUDED
#include <yggdrasill/serialization.inl>
