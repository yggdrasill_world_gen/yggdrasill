/// NOTE: This is the template implementation header for module.hpp and should not be included directly!
#pragma once

#ifndef YGDL_MODULE_HPP_INCLUDED
#include <yggdrasill/module.hpp>
#endif


namespace yggdrasill {

	template <Module_signature::Execute_nothrow* Execute, Module_signature::Validate_a_nothrow* Validate>
	constexpr Module_info to_module_info(std::string_view id)
	{
		return {detail::to_ygdl_string_view(id),
		        nullptr,
		        nullptr,
		        +[](void*, YGDL_Dict* dict, YGDL_Error* error) {
			        catch_error(error, [&] { Validate(Dictionary_view{*dict}).set_c_error(error); });
		        },
		        +[](void*, YGDL_World* world, const YGDL_Dict* dict, YGDL_Error* error) {
			        catch_error(error, [&] {
				        Execute(World_view(world), Const_dictionary_view{*dict}).set_c_error(error);
			        });
		        }};
	}

	template <Module_signature::Execute_nothrow* Execute, Module_signature::Validate_b_nothrow* Validate>
	constexpr Module_info to_module_info(std::string_view id)
	{
		return {detail::to_ygdl_string_view(id),
		        nullptr,
		        nullptr,
		        +[](void*, YGDL_Dict* dict, YGDL_Error* error) {
			        catch_error(error,
			                    [&] { Validate(World_view(), Dictionary_view{*dict}).set_c_error(error); });
		        },
		        +[](void*, YGDL_World* world, const YGDL_Dict* dict, YGDL_Error* error) {
			        catch_error(error, [&] {
				        Execute(World_view(world), Const_dictionary_view{*dict}).set_c_error(error);
			        });
		        }};
	}

	template <Module_signature::Execute_throw* Execute, Module_signature::Validate_a_throw* Validate>
	constexpr Module_info to_module_info(std::string_view id)
	{
		return {detail::to_ygdl_string_view(id),
		        nullptr,
		        nullptr,
		        +[](void*, YGDL_Dict* dict, YGDL_Error* error) {
			        catch_error(error, [&] { Validate(Dictionary_view{*dict}); });
		        },
		        +[](void*, YGDL_World* world, const YGDL_Dict* dict, YGDL_Error* error) {
			        catch_error(error, [&] { Execute(World_view(world), Const_dictionary_view{*dict}); });
		        }};
	}
	template <Module_signature::Execute_throw* Execute, Module_signature::Validate_b_throw* Validate>
	constexpr Module_info to_module_info(std::string_view id)
	{
		return {detail::to_ygdl_string_view(id),
		        nullptr,
		        nullptr,
		        +[](void*, YGDL_Dict* dict, YGDL_Error* error) {
			        catch_error(error, [&] { Validate(World_view(), Dictionary_view{*dict}); });
		        },
		        +[](void*, YGDL_World* world, const YGDL_Dict* dict, YGDL_Error* error) {
			        catch_error(error, [&] { Execute(World_view(world), Const_dictionary_view{*dict}); });
		        }};
	}


#define YGGDRASILL_IF_VALID_EXECUTE(EXPR) \
	if constexpr(requires { EXPR; }) {    \
		EXPR;                             \
	}
#define YGGDRASILL_ELSE_IF_VALID_EXECUTE(EXPR) \
	else if constexpr(requires { EXPR; })      \
	{                                          \
		EXPR;                                  \
	}

	template <typename T>
	constexpr Module_info to_module_info(std::string_view id)
	{
		return {detail::to_ygdl_string_view(id),
		        +[](YGDL_Error* error) -> void* {
			        // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
			        return catch_error(error, nullptr, [&] { return new T(); });
		        },
		        // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
		        +[](void* ud) { delete static_cast<T*>(ud); },
		        +[](void* ud, YGDL_Dict* dict, YGDL_Error* error) {
			        catch_error(error, [&] {
				        auto* inst = static_cast<T*>(ud);
				        auto  cfg  = Dictionary_view{*dict};

				        YGGDRASILL_IF_VALID_EXECUTE(inst->validate(cfg).set_c_error(error))
				        YGGDRASILL_ELSE_IF_VALID_EXECUTE(inst->validate({}, cfg).set_c_error(error))
				        YGGDRASILL_ELSE_IF_VALID_EXECUTE(inst->execute({}, cfg).set_c_error(error))
				        YGGDRASILL_ELSE_IF_VALID_EXECUTE(inst->validate(cfg))
				        YGGDRASILL_ELSE_IF_VALID_EXECUTE(inst->validate({}, cfg))
				        YGGDRASILL_ELSE_IF_VALID_EXECUTE(inst->execute({}, cfg))
				        else static_assert(!std::is_same_v<T, T>, "No valid validate() method");
			        });
		        },
		        +[](void* ud, YGDL_World* world, const YGDL_Dict* dict, YGDL_Error* error) {
			        catch_error(error, [&] {
				        auto* inst  = static_cast<T*>(ud);
				        auto  state = World_view(world);
				        auto  cfg   = Const_dictionary_view{*dict};

				        YGGDRASILL_IF_VALID_EXECUTE(inst->execute(state, cfg).set_c_error(error))
				        YGGDRASILL_ELSE_IF_VALID_EXECUTE(inst->execute(state, cfg))
				        else static_assert(!std::is_same_v<T, T>, "No valid execute() method");
			        });
		        }};
	}

#undef YGGDRASILL_IF_VALID_EXECUTE
#undef YGGDRASILL_ELSE_IF_VALID_EXECUTE

} // namespace yggdrasill
