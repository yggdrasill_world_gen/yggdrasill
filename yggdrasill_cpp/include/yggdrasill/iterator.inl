/// NOTE: This is the template implementation header for iterator.hpp and should not be included directly!
#pragma once

#ifndef YGDL_ITERATOR_HPP_INCLUDED
#include <yggdrasill/iterator.hpp>
#endif

namespace yggdrasill {
	namespace detail {

		/// Sentinel type to mark the end of a range of `Function_iterator`.
		struct Iterator_end {};

		/// Implementation of the functionality specified in make_range(...) and map_range(...) from the HPP
		template <typename Next>
		class Function_iterator {
		  public:
			using function_type     = Next;
			using value_type        = typename std::invoke_result_t<Next, bool>::value_type;
			using difference_type   = std::ptrdiff_t;
			using reference         = value_type;
			using pointer           = const value_type*;
			using iterator_category = std::bidirectional_iterator_tag;

			constexpr Function_iterator() = default;
			constexpr explicit Function_iterator(Iterator_end) {}
			constexpr explicit Function_iterator(Next next)
			  : next_(std::move(next)), last_value_(std::invoke(*next_, true))
			{
			}
			constexpr Function_iterator(const Function_iterator&)     = default;
			constexpr Function_iterator(Function_iterator&&) noexcept = default;

			constexpr Function_iterator& operator=(const Function_iterator& rhs) noexcept
			{
				if(this != &rhs) {
					detail::safe_assign_optional(next_, std::optional{rhs.next_});
					detail::safe_assign_optional(last_value_, std::optional{rhs.last_value_});
				}

				return *this;
			}
			constexpr Function_iterator& operator=(Function_iterator&& rhs) noexcept
			{
				if(this != &rhs) {
					detail::safe_assign_optional(next_, std::move(rhs.next_));
					detail::safe_assign_optional(last_value_, std::move(rhs.last_value_));
					rhs.next_.reset();
					rhs.last_value_.reset();
				}

				return *this;
			}

			constexpr reference operator*() const { return *last_value_; }
			constexpr pointer   operator->() const { return &*last_value_; }
			constexpr auto&     operator++()
			{
				detail::safe_assign_optional(last_value_, std::invoke(*next_, true));
				return *this;
			}
			constexpr auto operator++(int)
			{
				auto r = *this;
				++*this;
				return r;
			}
			constexpr auto& operator--()
			{
				detail::safe_assign_optional(last_value_, std::invoke(*next_, false));
				return *this;
			}
			constexpr auto operator--(int)
			{
				auto r = *this;
				--*this;
				return r;
			}

			constexpr bool operator==(const Function_iterator& rhs) const
			{
				return last_value_ == rhs.last_value_;
			}

			// comparison to end-sentinel
			constexpr bool operator==(const Iterator_end&) const { return !last_value_; }

			constexpr friend bool operator==(const Iterator_end&, const Function_iterator& rhs)
			{
				return !rhs.last_value_;
			}

		  private:
			// Note: All assignments to the optional have to use detail::safe_assign_optional(), because a
			//         non-empty optional would call the values' operator=, that might not be well-behaved
			//         (e.g. for reference- or const types)
			std::optional<Next>       next_;
			std::optional<value_type> last_value_;

			static_assert(!std::is_reference_v<value_type>);
		};
	} // namespace detail

	template <typename F>
	constexpr auto make_range(F&& f)
	{
		return Iterator_range{detail::Function_iterator{std::forward<F>(f)}, detail::Iterator_end{}};
	}
	template <typename BeginIter, typename EndIter, typename Map>
	constexpr auto map_range(BeginIter begin, EndIter end, Map&& map)
	{
		return make_range([=, iter = begin, map = std::forward<Map>(map)](bool forward) mutable
		                  -> std::optional<std::invoke_result_t<Map, decltype(*begin)>> {
			if(iter == end)
				return std::nullopt;

			auto r = std::invoke(map, *iter);

			if(forward)
				++iter;
			else {
				assert(iter != begin);
				--iter;
			}

			return r;
		});
	}
	template <typename Range, typename Map>
	constexpr auto map_range(Range&& base_range, Map&& map)
	{
		return map_range(base_range.begin(), base_range.end(), std::forward<Map>(map));
	}

} // namespace yggdrasill
