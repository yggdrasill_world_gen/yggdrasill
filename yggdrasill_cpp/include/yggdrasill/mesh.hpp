/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/mesh.h>

#include <yggdrasill/commons.hpp>
#include <yggdrasill/error.hpp>
#include <yggdrasill/iterator.hpp>

#include <span>

#include <array>
#include <functional>
#include <iterator>


namespace yggdrasill {

	/**The type of an `Edge<>`, used in its template parameter.\n
	 * An edge can be either a `primal` edge, connecting two vertices, or a `dual` edge, connecting two faces.
	 * Additionally, the value `any` may be chosen to represent edges that could be both.
	 */
	enum class Edge_type { primal, dual, any };

	/// \returns true if `Edge<lhs>` could store a value of type `Edge<rhs>`
	constexpr bool assignable_from(Edge_type lhs, Edge_type rhs)
	{
		return lhs == rhs || lhs == Edge_type::any || rhs == Edge_type::any;
	}

	// forward declarations
	template <Edge_type Type = Edge_type::any>
	struct Edge;
	struct Vertex;
	struct Face;

	using Generic_edge = Edge<Edge_type::any>;
	using Primal_edge  = Edge<Edge_type::primal>;
	using Dual_edge    = Edge<Edge_type::dual>;

	template <typename T>
	struct Valid;

	/// Tag type for indicating iterations with concurrent modification operations on the mesh.
	/// \see `Const_mesh_view::faces(concurrent_modifications_t)`,
	/// `Const_mesh_view::vertices(concurrent_modifications_t)`,
	/// `Const_mesh_view::directed_edges(concurrent_modifications_t)` and
	/// `Const_mesh_view::undirected_edges(concurrent_modifications_t)`
	constexpr struct concurrent_modifications_t {
	} concurrent_modifications;

	namespace detail {
		template <typename T,
		          bool    ConcurrentModifications,
		          index_t YGDL_Mesh::*Size,
		          auto                FreeList,
		          index_t YGDL_Mesh::*FreeListSize,
		          index_t             Step           = 1,
		          index_t             IndexCheckMask = ~index_t(0)>
		[[nodiscard]] Range_of<const Valid<T>> auto make_mesh_element_range(const YGDL_Mesh& mesh)
		{
			if constexpr(ConcurrentModifications) {
				return make_range([i = index_t(0), &mesh](bool forward) mutable -> std::optional<const Valid<T>> {
					if(forward) {
						while(i < mesh.*Size) {
							const auto curr_idx = std::exchange(i, i + Step);
							const auto curr     = T{curr_idx};
							if(curr.valid(mesh))
								return Valid{curr};
						}
						return std::nullopt;
					} else {
						for(;;) {
							const auto first_element = i == 0;
							const auto curr_idx      = std::exchange(i, std::max(i - Step, 0));
							const auto curr          = T{curr_idx};
							if(curr.valid(mesh))
								return Valid{curr};

							if(first_element)
								return std::nullopt;
						}
					}
				});

			} else {
				const auto size = mesh.*Size;
				const uint32_t* __restrict next_unused =
				        reinterpret_cast<const uint32_t*>(mesh.*FreeList + mesh.*FreeListSize - 1);
				const auto first_unused = *next_unused;

				return make_range([i = index_t(0), size, next_unused, first_unused](
				                          bool forward) mutable -> std::optional<const Valid<T>> {
					if(forward) {
						while(i < size) {
							const auto curr_idx = std::exchange(i, i + Step);
							const auto curr     = T{curr_idx};
							// skip free-list slots < curr
							if(*reinterpret_cast<const uint32_t*>(next_unused)
							   < static_cast<std::uint32_t>(curr_idx & IndexCheckMask))
								--next_unused;

							if(static_cast<std::uint32_t>(curr_idx & IndexCheckMask)
							   != *reinterpret_cast<const uint32_t*>(next_unused)) {
								return Valid{curr};
							}
						}

						return std::nullopt;

					} else {
						for(;;) {
							const auto first_element = i == 0;
							const auto curr_idx      = std::exchange(i, std::max(i - Step, 0));
							const auto curr          = T{curr_idx};
							if(*next_unused == first_unused)
								return Valid{curr};

							// skip free-list slots > curr
							if(*reinterpret_cast<const uint32_t*>(next_unused)
							   > static_cast<std::uint32_t>(curr_idx & IndexCheckMask))
								++next_unused;

							if(static_cast<std::uint32_t>(curr_idx & IndexCheckMask)
							   != *reinterpret_cast<const uint32_t*>(next_unused)) {
								return Valid{curr};
							}

							if(first_element)
								return std::nullopt;
						}
					}
				});
			}
		}
	} // namespace detail

	/**An immutable non-owning view of the topological information of the `World`, i.e. which
	 * :cpp:any:`vertices <Vertex>`, :cpp:any:`faces <Face>` and :cpp:any:`edges <Edge>` are valid and how
	 * they are connected to each other.
	 * 
	 * The data of the layer is managed by the world (see `World_view`) it's a part of and follows the
	 * copy-on-write semantic, outlined there.
	 * 
	 * The underlying data structure is loosely based on
	 * `this paper <https://www.cs.cmu.edu/afs/andrew/scs/cs/15-463/2001/pub/src/a2/quadedge.html>`_,
	 * with some additional features and restricted to only support triangular faces.
	 * 
	 * \attention All faces in the mesh have to use counter-clockwise winding-order.
	 */
	class Const_mesh_view {
	  public:
		/// \skip
		using const_view = Const_mesh_view;

		/// Converting constructor from a reference to the C-API struct
		constexpr /*implicit*/ Const_mesh_view(const YGDL_Mesh& mesh) : mesh_(&mesh) {}

		/**\returns A range of bidirectional iterators over all valid faces in the mesh.
		 * \overload_specific
		 * \attention The behaviour is undefined if faces are created or destroyed during iteration, 
		 * unless `concurrent_modifications` is passed as a parameter.
		 */
		[[nodiscard]] Range_of<const Valid<Face>> auto faces() const;
		/// \overload_specific
		/// By passing `concurrent_modifications` as a parameter, concurrent modifications during iterations
		/// are allowed. But it is still unspecified if faces created during the iteration will be visited.
		[[nodiscard]] Range_of<const Valid<Face>> auto faces(concurrent_modifications_t) const;

		/**\returns A range of bidirectional iterators over all valid vertices in the mesh.
		 * \overload_specific
		 * \attention The behaviour is undefined if vertices are created or destroyed during iteration, 
		 * unless `concurrent_modifications` is passed as a parameter.
		 */
		[[nodiscard]] Range_of<const Valid<Vertex>> auto vertices() const;
		/// \overload_specific
		/// By passing `concurrent_modifications` as a parameter, concurrent modifications during iterations
		/// are allowed. But it is still unspecified if vertices created during the iteration will be visited.
		[[nodiscard]] Range_of<const Valid<Vertex>> auto vertices(concurrent_modifications_t) const;

		/**\returns A range of bidirectional iterators over all valid primal or dual edges in the mesh.
		 * \overload_specific
		 * \attention The behaviour is undefined if edges are created or destroyed during iteration, 
		 * unless `concurrent_modifications` is passed as a parameter.
		 */
		template <Edge_type Type>
		[[nodiscard]] Range_of<const Valid<Edge<Type>>> auto directed_edges() const
		{
			static_assert(Type != Edge_type::any,
			              "Generic_edge/Edge<> is not supported by Const_mesh_view::directed_edges()");
			return detail::make_mesh_element_range<Edge<Type>,
			                                       false,
			                                       &YGDL_Mesh::edge_count,
			                                       &YGDL_Mesh::unused_edge_list,
			                                       &YGDL_Mesh::unused_edge_count,
			                                       1,
			                                       ~index_t(1)>(*mesh_);
		}
		/// \overload_specific
		/// By passing `concurrent_modifications` as a parameter, concurrent modifications during iterations
		/// are allowed. But it is still unspecified if edges created during the iteration will be visited.
		template <Edge_type Type>
		[[nodiscard]] Range_of<const Valid<Edge<Type>>> auto directed_edges(concurrent_modifications_t) const
		{
			static_assert(Type != Edge_type::any,
			              "Generic_edge/Edge<> is not supported by Const_mesh_view::directed_edges()");
			return detail::make_mesh_element_range<Edge<Type>,
			                                       true,
			                                       &YGDL_Mesh::edge_count,
			                                       &YGDL_Mesh::unused_edge_list,
			                                       &YGDL_Mesh::unused_edge_count,
			                                       1,
			                                       ~index_t(1)>(*mesh_);
		}

		/**\returns A range of bidirectional iterators over all valid primal or dual edges in the mesh,
		 * skipping every second edge, so for each quad-edge only the first of its edges it visited.
		 * \overload_specific
		 * \attention The behaviour is undefined if edges are created or destroyed during iteration, 
		 * unless `concurrent_modifications` is passed as a parameter.
		 */
		template <Edge_type Type>
		[[nodiscard]] Range_of<const Valid<Edge<Type>>> auto undirected_edges() const
		{
			static_assert(Type != Edge_type::any,
			              "Generic_edge/Edge<> is not supported by Const_mesh_view::undirected_edges()");
			return detail::make_mesh_element_range<Edge<Type>,
			                                       false,
			                                       &YGDL_Mesh::edge_count,
			                                       &YGDL_Mesh::unused_edge_list,
			                                       &YGDL_Mesh::unused_edge_count,
			                                       2>(*mesh_);
		}
		/// \overload_specific
		/// By passing `concurrent_modifications` as a parameter, concurrent modifications during iterations
		/// are allowed. But it is still unspecified if edges created during the iteration will be visited.
		template <Edge_type Type>
		[[nodiscard]] Range_of<const Valid<Edge<Type>>> auto undirected_edges(concurrent_modifications_t) const
		{
			static_assert(Type != Edge_type::any,
			              "Generic_edge/Edge<> is not supported by Const_mesh_view::undirected_edges()");
			return detail::make_mesh_element_range<Edge<Type>, true, &YGDL_Mesh::edge_count, &YGDL_Mesh::unused_edge_list, &YGDL_Mesh::unused_edge_count, 2>(
			        *mesh_);
		}


		/// \returns The primal edge that connects `from` to `to`, or `no_edge` if no such connection exists.
		[[nodiscard]] Primal_edge find_edge(Vertex from, Vertex to) const;

		/**\returns The number of valid vertices, i.e. the number of vertices that are actively used.
		 * \attention Because this count doesn't include gaps left by removed elements, the actual largest
		 * vertex index may be larger. Thus, the returned value can't be used for iteration.
		 */
		[[nodiscard]] constexpr index_t vertex_used_count() const;
		/**\returns The number of valid faces, i.e. the number of faces that are actively used.
		 * \attention Because this count doesn't include gaps left by removed elements, the actual largest
		 * face index may be larger. Thus, the returned value can't be used for iteration.
		 */
		[[nodiscard]] constexpr index_t face_used_count() const;
		/**\returns The number of valid dual or primal edges, i.e. the number of edges that are actively
		 * used for each of the two sub-sets.
		 * \attention Because this count doesn't include gaps left by removed elements, the actual largest
		 * edge index may be larger. Thus, the returned value can't be used for iteration.
		 */
		[[nodiscard]] constexpr index_t edge_used_count() const;

		/// \returns The largest index currently used for vertices.
		/// \note This number can be used to iterate over all vertices, including unused/invalid ones.
		[[nodiscard]] constexpr index_t vertex_max_index() const;
		/// \returns The largest index currently used for faces.
		/// \note This number can be used to iterate over all faces, including unused/invalid ones.
		[[nodiscard]] constexpr index_t face_max_index() const;
		/// \returns The largest index currently used for edges.
		/// \note This number can be used to iterate over all edges, including unused/invalid ones.
		[[nodiscard]] constexpr index_t edge_max_index() const;


		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
		 * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
		 */
		[[nodiscard]] constexpr friend const YGDL_Mesh* c_ptr(const Const_mesh_view& m) { return m.mesh_; }
		[[nodiscard]] constexpr friend const YGDL_Mesh* c_ptr(const Const_mesh_view* m)
		{
			return m ? m->mesh_ : nullptr;
		}

	  private:
		const YGDL_Mesh* mesh_;
	};

	// TODO: make parameters of modify operations Valid<...>

	/**A mutable non-owning view of the topological information of the `World`.
	 * 
	 * \note Most of the modification operations in this class have preconditions, to keep the mesh in a
	 * valid state, and throw `Mesh_corruption_exception` if they are violated.
	 * Because these failures might be common in many cases and can be safely ignored, there are additional
	 * variants for each of these functions with a ``try_`` prefix, that don't throw that specific exception
	 * and instead indicates this kind of error through their return value. Note however, that these
	 * functions will still throw any other specified exceptions, like `Precondition_violation_exception`.
	 * 
	 * \attention All functions in this class have to :cpp:any:`acquire <World_view::lock_layer()>` mutable
	 * views of all associated data layers to update them, with all consequences stated there.
	 * 
	 * \note While it's safe to modify the mesh during traversal, iteration might skip elements and
	 * special care might have to be taken to accommodate the changing topology and safely handle elements
	 * that became invalid during iteration.
	 * 
	 * \see `Const_mesh_view` for the read-access part of the API
	 */
	class Mesh_view : public Const_mesh_view {
	  public:
		/// \skip
		using const_view = Const_mesh_view;

		/// Converting constructor from a reference to the C-API struct
		constexpr /*implicit*/ Mesh_view(YGDL_Mesh& mesh) : Const_mesh_view(mesh) {}

		/**Creates new vertices.
		 * \param count The number of vertices that should be created.
		 * \param out View of a container that is used both to determine the number of vertices that should
		 * be created and is used to return the handles for the new vertices.
		 */
		void add_vertices(index_t count);
		void add_vertices(std::span<Vertex> out);

		/// Removes the given vertex, if it's currently not used by any edges.
		/// \returns `true` if the vertex has been removed
		bool remove_unused_vertex(Vertex v);

		/**Creates a new face that connects the three given vertices.
		 * \returns The newly created face.
		 * \throws Mesh_corruption_exception If the mesh is in an invalid state or adding the face would
		 * corrupt it, usually because one of the following conditions is not met:\n
		 *   - There must be no pre-existing face connecting these three vertices.
		 *   - The winding-order of the passed vertices has to be consistent with all its neighbors.
		 *   - If one of the vertices already has two faces, which aren't connected to each other, the new face has to share an edge with at least one of them.
		 * 
		 * \throws Precondition_violation_exception If the system couldn't acquire mutable pointers to the
		 * data layers, to update them.
		 */
		Valid<Face> add_face(Vertex a, Vertex b, Vertex c);
		/**Creates a new face that connects the three given vertices.
		 * \returns The newly created face or `no_face` if it couldn't be created.
		 * \see `add_face()` which behaves identical, except that this function won't throw `Mesh_corruption_exception`.
		 */
		Face try_add_face(Vertex a, Vertex b, Vertex c);

		/**Flips the given edge `e`, i.e. rotating it inside the quad formed `e.left()` and `e.right()`.
		 * \throws Mesh_corruption_exception If the mesh is in an invalid state or `e` doesn't have two faces.
		 * \throws Precondition_violation_exception If the system couldn't acquire mutable pointers to the
		 * data layers, to update them.
		 * \note Invalidates all data layer values (according to their `Invalidation` setting)
		 * for: `e`, the other parts of the quad-edge, `e.left()`, `e.right()` and all dual-edges that
		 * are connected to these faces.
		 */
		void flip(Edge<> e);
		/**Flips the given edge `e`, i.e. rotating it inside the quad formed `e.left()` and `e.right()`
		 * \returns true if the operation was successful.
		 * \see `split()` which behaves identical, except that this function won't throw `Mesh_corruption_exception`.
		 */
		bool try_flip(Edge<> e);

		/**Splits the given edge, inserting a new vertex, up to two faces and up to three quad-edges.
		 * \param split_point The weight that is used to interpolate the data for the new vertex,
		 * where ``0.0`` stand for `e.origin()` and ``1.0`` for `e.dest()`.
		 * \returns The newly created edge, between the new vertex and `e.dest()`.
		 * \throws Mesh_corruption_exception If the mesh is in an invalid state or `e` doesn't have at
		 * least one faces.
		 * \throws Precondition_violation_exception If the system couldn't acquire mutable pointers to the
		 * data layers, to update them.
		 * \note Invalidates all data layer values (according to their `Invalidation` setting) for:
		 * the passed edge and all dual edges that had to be updated to insert the new faces.\n
		 * \note Evaluates the data layers' `Interpolation` for: the new vertex, the new faces
		 * (with both origin and dest identical to either the original left or right face) and the primal
		 * and dual edges that had to be newly inserted.
		 */
		Valid<Primal_edge> split(Primal_edge e, float split_point);
		/**Splits the given edge, inserting a new vertex, up to two faces and up to three quad-edges.
		 * \returns The newly created edge, between the new vertex and `e.dest()`, or `no_edge` on a failure.
		 * \see `split()` which behaves identical, except that this function won't throw `Mesh_corruption_exception`.
		 */
		Primal_edge try_split(Primal_edge e, float split_point);

		/**Collapses the given edge, removing its destination vertex and updating the data associated with
		 * its origin vertex to move it according to `join_point`.
		 * \param join_point The weight that is used to interpolate the data for the moved vertex `e.origin()`,
		 * where ``0.0`` stands for its old value and ``1.0`` for the one from `e.destination()`.
		 * \returns The remaining vertex (`e.origin()`).
		 * \throws Mesh_corruption_exception If the mesh is in an invalid state or `e` doesn't satisfy the link condition.
		 * \throws Precondition_violation_exception If the system couldn't acquire mutable pointers to the
		 * data layers, to update them.
		 * \attention This function removes `e`, `e.dest()`, `e.left()` and `e.right()`,
		 * which might have to be considered during traversal.
		 * \note Invalidates all data layer values (according to their `Invalidation` setting) for:
		 * `e`, `e.dest()`, `e.left()`, `e.right()` and all primal and dual edges that are either part of
		 * the deleted faces or of the edge-ring around `e.dest()`.
		 * \note Evaluates the data layers' `Interpolation` for the vertex that was kept and is returned,
		 * i.e. `e.origin()`.
		 */
		Valid<Vertex> collapse(Primal_edge e, float join_point);
		/**Collapses the given edge, removing its destination vertex and updating the data associated with
		 * its origin vertex to move it according to `join_point`.
		 * \returns The remaining vertex (`e.origin()`) or `no_vertex` on a failure.
		 * \see `collapse()` which behaves identical, except that this function won't throw `Mesh_corruption_exception`.
		 */
		Vertex try_collapse(Primal_edge e, float join_point);

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
		 * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
		 */
		[[nodiscard]] constexpr friend YGDL_Mesh* c_ptr(Mesh_view v)
		{
			// we can safely cast away const here, because (inside the Mesh_view class) we know that mesh_ was initialized from a pointer to non-const
			return const_cast<YGDL_Mesh*>(c_ptr(static_cast<const Const_mesh_view&>(v)));
		}
		[[nodiscard]] constexpr friend YGDL_Mesh* c_ptr(Mesh_view* v)
		{
			return const_cast<YGDL_Mesh*>(c_ptr(static_cast<const Const_mesh_view*>(v)));
		}
	};

	/**A wrapper around `Vertex`, `Face` or `Edge` that is used in the API to mark functions that always
	 * return valid elements.
	 * 
	 * The type provides the same interface as the one its wrapping, except that it isn't
	 * default-constructible and can never be constructed or assigned to the empty-state (`T{}`).
	 * 
	 * \tparam T The wrapped type, either `Vertex`, `Face` or `Edge`
	 * \note While API functions that return this type will never return invalid values, the elements can
	 * still become invalid later, which isn't checked in any way.
	 */
	template <typename T>
	struct Valid : T {
		Valid() = delete; /// \skip

		/// \skip
		template <typename... Args>
		requires std::constructible_from<T, Args...>
		constexpr explicit Valid(Args&&... args) noexcept : T(std::forward<Args>(args)...)
		{
			YGDL_ASSUME(*this != T{});
		}

		/// \skip
		template <typename Arg>
		requires std::assignable_from<T&, Arg>
		constexpr Valid& operator=(Arg v) noexcept
		{
			T::operator=(v);
			YGDL_ASSUME(*this != T{});
			return *this;
		}

		/// \returns a copy of the wrapped object
		[[nodiscard]] constexpr T decay() const noexcept { return *this; }
	};
	template <typename T>
	Valid(T) -> Valid<T>;                ///< \skip
	Valid(YGDL_Edge) -> Valid<Edge<>>;   ///< \skip
	Valid(YGDL_Face) -> Valid<Face>;     ///< \skip
	Valid(YGDL_Vertex) -> Valid<Vertex>; ///< \skip

	/// An alias for an `Edge` that can reference only primal edges
	using Primal_edge = Edge<Edge_type::primal>;
	/// An alias for an `Edge` that can reference only dual edges
	using Dual_edge = Edge<Edge_type::dual>;
	/// An alias for an `Edge` that can reference either primal or dual edges.
	using Generic_edge = Edge<Edge_type::any>;

#ifdef __clang__
#define YGDL_EDGE_ATTRIBUTES \
	[[clang::preferred_name(Generic_edge), clang::preferred_name(Primal_edge), clang::preferred_name(Dual_edge)]]
#else
#define YGDL_EDGE_ATTRIBUTES
#endif

	/**A handle to an edge, stored and managed by a `Mesh_view`.\n
	 * This type only contains the edges ID and provides traversal functions, but all actual data is either
	 * stored in the mesh or in the :doc:`data layers <layer>`.
	 * \attention Instances have an empty-state (`no_edge`), which is used to mark missing or invalid edges.
	 * Use `Valid<Edge<Type>>` to store handles that should always point to a valid element.
	 * \attention Instances can also be invalid because they reference an element that has since been
	 * removed from the mesh. Use `valid()` to identify these invalid handles before using them.
	 * \attention The behaviour is undefined if any function, other than `valid()`, is called on an invalid handle.
	 */
	template <Edge_type Type>
	struct YGDL_EDGE_ATTRIBUTES Edge : YGDL_Edge {
	  private:
		using self_t             = Edge<Type>;
		using Edge_ring_function = self_t (*)(self_t, Const_mesh_view, bool);

		constexpr YGDL_Edge c_val() const
		{
			const auto e = YGDL_Edge(*this);
			if constexpr(Type != Edge_type::any)
				YGDL_ASSUME(mask == YGDL_NO_EDGE.mask
				            || (YGDL_mesh_edge_is_dual(e) == YGDL_TRUE) == (Type == Edge_type::dual));
			return e;
		}

		/// private helper function that constructs a range for a given type of edge-ring
		constexpr Range_of<Valid<Edge>> auto edge_ring_range(const Const_mesh_view mesh, Edge_ring_function f) const
		{
			if(origin_next(mesh) == Edge{}) {
				throw Precondition_violation_exception("Can't form an iterator based on a deleted edge");
			}

			return make_range(
			        [=, *this, e = self_t{}, i = 0](bool forward) mutable -> std::optional<Valid<self_t>> {
				        if(e == Edge{})
					        return Valid<self_t>(e = *this);
				        else
					        e = f(e, mesh, forward);

				        i += forward ? 1 : -1;

				        if(e == *this && forward && i > 0)
					        return std::nullopt;
				        else
					        return Valid<self_t>(e);
			        });
		}

	  public:
		/// Creates an empty/invalid edge, i.e. `no_edge`.
		constexpr Edge() : Edge(YGDL_NO_EDGE) {}
		/// Converting constructor from the C-API type.
		constexpr /*implicit*/ Edge(const YGDL_Edge& e) : YGDL_Edge(e)
		{
			if constexpr(Type != Edge_type::any)
				YGDL_ASSUME(mask == YGDL_NO_EDGE.mask
				            || (YGDL_mesh_edge_is_dual(e) == YGDL_TRUE) == (Type == Edge_type::dual));
		}
		/// Constructs an edge from its `type` and `index`.
		/// \param type Either `Edge_type::primal` or `Edge_type::dual`
		constexpr Edge(Edge_type type, index_t index) requires(Type == Edge_type::any);
		/// Constructs an edge of type `Type` from an index.
		constexpr explicit Edge(index_t index) requires(Type != Edge_type::any);
		/// Converting constructor from another edge specialization.
		template <Edge_type T>
		constexpr /*implicit*/ Edge(const Edge<T>& e) requires(assignable_from(Type, T));

		/// Assignment operator, which accepts any compatible `Edge` specialization.
		template <Edge_type T>
		constexpr Edge& operator=(const Edge<T>& e) requires(assignable_from(Type, T));

		/// \returns The index that can be used to associate data with this edge.
		[[nodiscard]] constexpr index_t index() const;
		/// \returns Either `Edge_type::primal` or `Edge_type::dual` depending on the type of edge stored.
		[[nodiscard]] constexpr Edge_type type() const;

		[[nodiscard]] constexpr bool is_dual() const { return type() == Edge_type::dual; }
		[[nodiscard]] constexpr bool is_primal() const { return type() == Edge_type::primal; }

		/// \returns The symmetric edge, i.e. the edge that connect `dest()` to `origin()`.
		[[nodiscard]] constexpr Valid<Edge> sym() const;
		/// \returns The CCW rotation of this edge, i.e. the edge that connect `right()` to `left()`.
		[[nodiscard]] constexpr Valid<Generic_edge> rot() const requires(Type == Edge_type::any);
		[[nodiscard]] constexpr Valid<Dual_edge>    rot() const requires(Type == Edge_type::primal);
		[[nodiscard]] constexpr Valid<Primal_edge>  rot() const requires(Type == Edge_type::dual);
		/// \returns The CW rotation of this edge, i.e. the edge that connect `left()` to `right()`.
		[[nodiscard]] constexpr Valid<Generic_edge> inv_rot() const requires(Type == Edge_type::any);
		[[nodiscard]] constexpr Valid<Dual_edge>    inv_rot() const requires(Type == Edge_type::primal);
		[[nodiscard]] constexpr Valid<Primal_edge>  inv_rot() const requires(Type == Edge_type::dual);
		/**\returns The canonical/first edge of this edge's quad-edge, which will always be a primal edge with an even index.
		 * \note This is both usefully to identify the quad-edge itself and to implement functions that don't
		 * need to distinguish between different parts of the quad-edge, like `Mesh_view::flip()`.
		 */
		[[nodiscard]] constexpr Valid<Primal_edge> base() const;
		[[nodiscard]] constexpr bool               is_base() const { return base() == *this; }

		// clang-format off
		[[nodiscard]] constexpr Valid<Vertex> origin(const Const_mesh_view mesh) const requires(Type == Edge_type::primal);
		[[nodiscard]] constexpr Valid<Vertex> dest(const Const_mesh_view mesh)   const requires(Type == Edge_type::primal);
		[[nodiscard]] constexpr Face          left(const Const_mesh_view mesh)   const requires(Type == Edge_type::primal);
		[[nodiscard]] constexpr Face          right(const Const_mesh_view mesh)  const requires(Type == Edge_type::primal);

		[[nodiscard]] constexpr Face          origin(const Const_mesh_view mesh) const requires(Type == Edge_type::dual);
		[[nodiscard]] constexpr Face          dest(const Const_mesh_view mesh)   const requires(Type == Edge_type::dual);
		[[nodiscard]] constexpr Valid<Vertex> left(const Const_mesh_view mesh)   const requires(Type == Edge_type::dual);
		[[nodiscard]] constexpr Valid<Vertex> right(const Const_mesh_view mesh)  const requires(Type == Edge_type::dual);
		// clang-format on

		/// \returns The vertices around this edge, both for primal and dual edges.
		[[nodiscard]] constexpr std::array<Valid<Vertex>, 2> vertices(const Const_mesh_view mesh) const;
		/// \returns The faces around this edge, both for primal and dual edges, some of which may be `no_face`.
		[[nodiscard]] constexpr std::array<Face, 2> faces(const Const_mesh_view mesh) const;

		/**\returns A range of bidirectional iterators over all edges in the indicated edge-ring.
		 * \note The edge-ring contains always at least this edge.\n
		 * \note The edge-ring is cyclical, which means it can be decremented beyond begin and incremented beyond end.
		 */
		[[nodiscard]] constexpr Range_of<Valid<Edge>> auto origin_ccw(const Const_mesh_view mesh) const
		{
			return edge_ring_range(mesh, [](self_t e, const Const_mesh_view mesh, bool forward) -> self_t {
				return forward ? e.origin_next(mesh) : e.origin_prev(mesh);
			});
		}
		[[nodiscard]] constexpr Range_of<Valid<Edge>> auto origin_cw(const Const_mesh_view mesh) const
		{
			return edge_ring_range(mesh, [](self_t e, const Const_mesh_view mesh, bool forward) -> self_t {
				return forward ? e.origin_prev(mesh) : e.origin_next(mesh);
			});
		}
		[[nodiscard]] constexpr Range_of<Valid<Edge>> auto dest_ccw(const Const_mesh_view mesh) const
		{
			return edge_ring_range(mesh, [](self_t e, const Const_mesh_view mesh, bool forward) -> self_t {
				return forward ? e.dest_next(mesh) : e.dest_prev(mesh);
			});
		}
		[[nodiscard]] constexpr Range_of<Valid<Edge>> auto dest_cw(const Const_mesh_view mesh) const
		{
			return edge_ring_range(mesh, [](self_t e, const Const_mesh_view mesh, bool forward) -> self_t {
				return forward ? e.dest_prev(mesh) : e.dest_next(mesh);
			});
		}
		[[nodiscard]] constexpr Range_of<Valid<Edge>> auto left_ccw(const Const_mesh_view mesh) const
		{
			return edge_ring_range(mesh, [](self_t e, const Const_mesh_view mesh, bool forward) -> self_t {
				return forward ? e.left_next(mesh) : e.left_prev(mesh);
			});
		}
		[[nodiscard]] constexpr Range_of<Valid<Edge>> auto left_cw(const Const_mesh_view mesh) const
		{
			return edge_ring_range(mesh, [](self_t e, const Const_mesh_view mesh, bool forward) -> self_t {
				return forward ? e.left_prev(mesh) : e.left_next(mesh);
			});
		}
		[[nodiscard]] constexpr Range_of<Valid<Edge>> auto right_ccw(const Const_mesh_view mesh) const
		{
			return edge_ring_range(mesh, [](self_t e, const Const_mesh_view mesh, bool forward) -> self_t {
				return forward ? e.right_next(mesh) : e.right_prev(mesh);
			});
		}
		[[nodiscard]] constexpr Range_of<Valid<Edge>> auto right_cw(const Const_mesh_view mesh) const
		{
			return edge_ring_range(mesh, [](self_t e, const Const_mesh_view mesh, bool forward) -> self_t {
				return forward ? e.right_prev(mesh) : e.right_next(mesh);
			});
		}

		/// \returns true if this edge isn't `no_edge` and references a valid edge used by the `mesh`.
		[[nodiscard]] bool valid(const Const_mesh_view mesh) const;

		/// \returns The next (CCW) edge, rotating around `origin()`.
		[[nodiscard]] constexpr Valid<Edge> origin_next(const Const_mesh_view mesh) const;
		/// \returns The previous (CW) edge, rotating around `origin()`.
		[[nodiscard]] constexpr Valid<Edge> origin_prev(const Const_mesh_view mesh) const;

		/// \returns The next (CCW) edge, rotating around `dest()`.
		[[nodiscard]] constexpr Valid<Edge> dest_next(const Const_mesh_view mesh) const;
		/// \returns The previous (CW) edge, rotating around `dest()`.
		[[nodiscard]] constexpr Valid<Edge> dest_prev(const Const_mesh_view mesh) const;

		/// \returns The next (CCW) edge, rotating around `left()`.
		[[nodiscard]] constexpr Valid<Edge> left_next(const Const_mesh_view mesh) const;
		/// \returns The previous (CW) edge, rotating around `left()`.
		[[nodiscard]] constexpr Valid<Edge> left_prev(const Const_mesh_view mesh) const;

		/// \returns The next (CCW) edge, rotating around `right()`.
		[[nodiscard]] constexpr Valid<Edge> right_next(const Const_mesh_view mesh) const;
		/// \returns The previous (CW) edge, rotating around `right()`.
		[[nodiscard]] constexpr Valid<Edge> right_prev(const Const_mesh_view mesh) const;

		/// \skip
		friend std::ostream& operator<<(std::ostream& o, const self_t& e)
		{
			if(e == self_t{})
				return o << "no_edge";
			return o << (e.type() == Edge_type::dual ? "Dual_edge{" : "Primal_edge{") << e.index() << "}";
		}
	};
	/// \skip
	template <Edge_type T>
	Edge(const Edge<T>& e) -> Edge<T>;
#undef YGDL_EDGE_ATTRIBUTES

	/// \skip
	template <Edge_type T1, Edge_type T2>
	[[nodiscard]] constexpr inline bool operator==(const Edge<T1>& lhs, const Edge<T2>& rhs);
	/// \skip
	[[nodiscard]] constexpr inline bool operator==(const Edge<>& lhs, const Edge<>& rhs);
	/// \skip
	template <Edge_type T1, Edge_type T2>
	requires(T1 != T2 || T1 == Edge_type::any || T2 == Edge_type::any)
	[[nodiscard]] constexpr inline std::partial_ordering operator<=>(const Edge<T1>& lhs, const Edge<T2>& rhs);
	/// \skip
	template <Edge_type T1, Edge_type T2>
	requires(T1 == T2 && T1 != Edge_type::any)
	[[nodiscard]] constexpr inline std::strong_ordering operator<=>(const Edge<T1>& lhs, const Edge<T2>& rhs);
	[[nodiscard]] constexpr inline std::partial_ordering operator<=>(const Edge<>& lhs, const Edge<>& rhs);

	/// Constants used by `Vertex::neighbors()` and traversal functions based on it,
	/// to restrict, which kind of neighbors should be visited.
	enum class Neighbor_type {
		just_primal_edges, ///< Just visit vertices that share an edge with the original vertex.
		with_dual_edges    ///< Visit both vertices that share an edge and those that only share a face
	};

	/**A handle to a vertex, stored and managed by a `Mesh_view`.\n
	 * This type only contains the vertex ID and provides traversal functions, but all actual data is either
	 * stored in the mesh or in the :doc:`data layers <layer>`.
	 * \attention Instances have an empty-state (`no_vertex`), which is used to mark missing or invalid vertices.
	 * Use `Valid<Vertex>` to store handles that should always point to a valid element.
	 * \attention Instances can also be invalid because they reference an element that has since been
	 * removed from the mesh. Use `valid()` to identify these invalid handles before using them.
	 * \attention The behaviour is undefined if any function, other than `valid()`, is called on an invalid handle.
	 */
	struct Vertex : YGDL_Vertex {
		/// Creates an empty/invalid vertex, i.e. `no_vertex`.
		constexpr Vertex() : YGDL_Vertex{YGDL_NO_VERTEX} {}
		/// Converting constructor from the C-API type.
		constexpr /*implicit*/ Vertex(const YGDL_Vertex& e) : YGDL_Vertex(e) {}
		/// Constructs a vertex from an index.
		constexpr explicit Vertex(index_t index) : YGDL_Vertex{index} {}

		/// \returns The index that can be used to associate data with this vertex.
		[[nodiscard]] constexpr index_t index() const { return YGDL_Vertex::index; }

		/// \returns An arbitrary edge that has this vertex as it's `Edge::origin()`, or `no_edge` if this vertex
		/// isn't part of any edge or face.
		[[nodiscard]] constexpr Primal_edge edge(const Const_mesh_view mesh) const;

		/// \returns A range of bidirectional iterators over all edges that originate from this vertex.
		[[nodiscard]] constexpr Range_of<Valid<Primal_edge>> auto edges(const Const_mesh_view mesh) const;

		/// \returns A range of bidirectional iterators over all vertices that are connected to this vertex.
		[[nodiscard]] Range_of<Valid<Vertex>> auto neighbors(
		        const Const_mesh_view mesh, Neighbor_type = Neighbor_type::just_primal_edges) const;

		/// \returns true if this vertex isn't `no_vertex` and references a valid vertex used by the `mesh`.
		[[nodiscard]] bool valid(const Const_mesh_view mesh) const;


		/// \skip
		friend std::strong_ordering operator<=>(const Vertex& lhs, const Vertex& rhs)
		{
			return lhs.index() <=> rhs.index();
		}
		/// \skip
		[[nodiscard]] constexpr friend bool operator==(const Vertex& lhs, const Vertex& rhs)
		{
			return lhs.index() == rhs.index();
		}

		/// \skip
		friend std::ostream& operator<<(std::ostream& o, const Vertex& v)
		{
			return v == Vertex{} ? (o << "no_vertex") : (o << "Vertex{" << v.index() << "}");
		}
	};


	/**A handle to a face, stored and managed by a `Mesh_view`.\n
	 * This type only contains the face ID and provides traversal functions, but all actual data is either
	 * stored in the mesh or in the :doc:`data layers <layer>`.
	 * \attention Instances have an empty-state (`no_face`), which is used to mark missing or invalid faces.
	 * Use `Valid<Face>` to store handles that should always point to a valid element.
	 * \attention Instances can also be invalid because they reference an element that has since been
	 * removed from the mesh. Use `valid()` to identify these invalid handles before using them.
	 * \attention The behaviour is undefined if any function, other than `valid()`, is called on an invalid handle.
	 */
	struct Face : YGDL_Face {
		/// Creates an empty/invalid face, i.e. `no_face`.
		constexpr Face() : YGDL_Face{YGDL_NO_FACE} {}
		/// Converting constructor from the C-API type.
		constexpr /*implicit*/ Face(const YGDL_Face& e) : YGDL_Face(e) {}
		/// Constructs a face from an index.
		constexpr explicit Face(index_t index) : YGDL_Face{index} {}

		/// \returns The index that can be used to associate data with this face.
		[[nodiscard]] constexpr index_t index() const { return YGDL_Face::index; }

		/// \returns An arbitrary edge that has this face as it's `Edge::left()`.
		[[nodiscard]] constexpr Valid<Primal_edge> edge(const Const_mesh_view mesh) const;

		/// \returns A range of bidirectional iterators over all edges that have this face as their `Edge::left()`.
		[[nodiscard]] constexpr Range_of<Valid<Primal_edge>> auto edges(const Const_mesh_view mesh) const;

		/// \returns The three vertices that make up this face
		[[nodiscard]] constexpr std::array<Valid<Vertex>, 3> vertices(const Const_mesh_view mesh) const;

		/// \returns true if this face isn't `no_face` and references a valid face used by the `mesh`.
		[[nodiscard]] bool valid(const Const_mesh_view mesh) const;


		/// \skip
		friend std::strong_ordering operator<=>(const Face& lhs, const Face& rhs)
		{
			return lhs.index() <=> rhs.index();
		}
		/// \skip
		[[nodiscard]] constexpr friend bool operator==(const Face& lhs, const Face& rhs)
		{
			return lhs.index() == rhs.index();
		}

		/// \skip
		friend std::ostream& operator<<(std::ostream& o, const Face& v)
		{
			return v == Face{} ? (o << "no_face") : (o << "Face{" << v.index() << "}");
		}
	};

	/// Constant used to indicate missing or invalid vertices.
	constexpr auto no_vertex = Vertex{};
	/// Constant used to indicate missing or invalid faces.
	constexpr auto no_face = Face{};
	/// Constant used to indicate missing or invalid edges.
	constexpr auto no_edge = Generic_edge{};


} // namespace yggdrasill

namespace std {

	template <>
	struct hash<yggdrasill::Vertex> {
		size_t operator()(const yggdrasill::Vertex& v) const noexcept
		{
			return static_cast<std::size_t>(v.index());
		}
	};

	template <>
	struct hash<yggdrasill::Face> {
		size_t operator()(const yggdrasill::Face& v) const noexcept
		{
			return static_cast<std::size_t>(v.index());
		}
	};

	template <yggdrasill::Edge_type Type>
	struct hash<yggdrasill::Edge<Type>> {
		size_t operator()(const yggdrasill::Edge<Type>& v) const noexcept
		{
			return static_cast<std::size_t>(v.mask);
		}
	};
} // namespace std

#define YGDL_MESH_HPP_INCLUDED
#include <yggdrasill/mesh.inl>
