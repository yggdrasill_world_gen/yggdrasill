/// NOTE: This is the template implementation header for mesh_utils.hpp and should not be included directly!
#pragma once

#ifndef YGDL_MESH_UTILS_HPP_INCLUDED
#include <yggdrasill/mesh_utils.hpp>
#endif


namespace yggdrasill {

	constexpr inline bool is_wrong_winding_order(const Vec3& a, const Vec3& b, const Vec3& c)
	{
		// = dot(b, cross(b - a, c - b)) <= 0.f
		return b.x * a.z * c.y - b.y * a.z * c.x + b.y * a.x * c.z - b.z * a.x * c.y + b.z * a.y * c.x
		               - b.x * a.y * c.z
		       <= 0.f;
	}

	inline bool can_move_vertex(Const_mesh_view                          mesh,
	                            Vertex                                   v,
	                            Const_layer_view<Vec3, Ref_type::vertex> positions,
	                            const Vec3&                              new_position)
	{
		auto range = v.neighbors(mesh, Neighbor_type::just_primal_edges);
		auto iter  = range.begin();

		auto prev = positions[*iter];
		++iter;

		return std::none_of(iter, range.end_iterator(), [&](Vertex neighbor) {
			const auto curr    = positions[neighbor];
			const auto invalid = is_wrong_winding_order(new_position, prev, curr);
			prev               = curr;
			return invalid;
		});
	}

	inline bool try_flip(Mesh_view mesh, Primal_edge edge, Const_layer_view<Vec3, Ref_type::vertex> positions)
	{
		const auto origin = positions[edge.origin(mesh)];
		const auto dest   = positions[edge.dest(mesh)];

		const auto left  = positions[edge.origin_next(mesh).dest(mesh)];
		const auto right = positions[edge.origin_prev(mesh).dest(mesh)];

		if(is_wrong_winding_order(origin, right, left) || is_wrong_winding_order(right, dest, left))
			return false;

		return mesh.try_flip(edge);
	}

	inline bool try_collapse(
	        Mesh_view mesh, Primal_edge edge, float alpha, Const_layer_view<Vec3, Ref_type::vertex> positions)
	{
		const auto origin       = positions[edge.origin(mesh)];
		const auto dest         = positions[edge.dest(mesh)];
		const auto new_position = normalized(lerp(origin, dest, alpha));


		const auto is_any_face_wrong_winding_order = [&](Primal_edge edge) {
			auto range = edge.origin_ccw(mesh).skip(1);
			auto iter  = range.begin();

			auto prev = positions[iter->dest(mesh)];
			++iter;

			return std::any_of(iter, range.end_iterator(), [&](Primal_edge e) {
				const auto curr    = positions[e.dest(mesh)];
				const auto invalid = is_wrong_winding_order(new_position, prev, curr);
				prev               = curr;
				return invalid;
			});
		};

		if(is_any_face_wrong_winding_order(edge) || is_any_face_wrong_winding_order(edge.sym()))
			return false;

		return mesh.try_collapse(edge, alpha) != no_vertex;
	}

	inline std::optional<Valid<Vertex>> random_vertex(
	        Const_mesh_view mesh, Random_view& rand, int tries, std::predicate<Vertex> auto&& predicate)
	{
		auto tries_left = tries;
		do {
			tries_left--;
			auto vertex = Vertex(rand.uniform(0, mesh.vertex_max_index()));
			if(vertex.valid(mesh) && predicate(vertex))
				return Valid<Vertex>(vertex);
		} while(tries_left > 0);

		return std::nullopt;
	}

	inline Vertex minimizing_hill_climb(
	        Const_mesh_view mesh, Vertex start, int max_steps, Neighbor_type neighbor_type, Scorer<Vertex> auto&& scorer)
	{
		auto min_vertex = start;
		auto last_score = scorer(start);

		for(int i = 0; i < max_steps; i++) {
			auto done = true;
			for(const auto n : min_vertex.neighbors(mesh, neighbor_type)) {
				if(const auto s = scorer(n); s < last_score) {
					last_score = s;
					min_vertex = n;
					done       = false;
				}
			}

			if(done)
				break;
		}

		return min_vertex;
	}

	inline void flood_fill(Const_mesh_view               mesh,
	                       std::vector<Vertex>           seeds,
	                       Neighbor_type                 neighbor_type,
	                       std::predicate<Vertex> auto&& callback)
	{
		std::erase_if(seeds, [&](Vertex v) { return !callback(v); });

		auto openlists = std::array<std::vector<Vertex>, 2>{std::move(seeds)};
		auto current   = 0;

		while(!openlists[current].empty()) {
			const auto next = current == 0 ? 1 : 0;

			for(auto v : openlists[current]) {
				for(auto n : v.neighbors(mesh, neighbor_type)) {
					if(callback(n)) {
						openlists[next].push_back(n);
					}
				}
			}

			openlists[current].clear();
			current = next;
		}
	}

	template <typename Payload>
	inline void flood_fill(Const_mesh_view                              mesh,
	                       std::vector<std::pair<Vertex, Payload>>      seeds,
	                       Neighbor_type                                neighbor_type,
	                       Flood_fill_payload_predicate<Payload> auto&& callback)
	{
		std::erase_if(seeds, [&](auto& e) {
			auto&& [v, p] = e;
			return !callback(v, p);
		});

		auto openlists = std::array<std::vector<std::pair<Vertex, Payload>>, 2>{std::move(seeds)};
		auto current   = 0;

		while(!openlists[current].empty()) {
			const auto next = current == 0 ? 1 : 0;

			for(auto&& [v, p] : openlists[current]) {
				for(auto n : v.neighbors(mesh, neighbor_type)) {
					// the callback may return a new Payload as an optional, that will be used for all children of the current node
					if(const auto r = callback(n, p)) {
						if constexpr(std::is_same_v<std::optional<Payload>, std::remove_cvref_t<decltype(r)>>) {
							openlists[next].emplace_back(n, *r);
						} else {
							openlists[next].emplace_back(n, p);
						}
					}
				}
			}

			openlists[current].clear();
			current = next;
		}
	}

	inline float normalized_voronoi_area(
	        Const_mesh_view mesh, Const_layer_view<Vec3, Ref_type::vertex> positions, Vertex center_v)
	{
		const auto center = positions[center_v];

		const auto calc_triangle_area = [center](const Vec3& prev, const Vec3& curr, const Vec3& next) {
			const auto center_left  = circumcenter(center, prev, curr);
			const auto center_right = circumcenter(center, curr, next);

			return length(cross(center_left - center, center_right - center)) / 2.f;
		};

		auto area = 0.0f;

		const auto neighbors = center_v.neighbors(mesh, Neighbor_type::just_primal_edges);
		auto       iter      = neighbors.begin();
		auto       v_minus_1 = positions[*(--iter)];
		auto       v_minus_2 = positions[*(--iter)];

		for(const auto n : neighbors) {
			const auto v_0 = positions[n];
			area += calc_triangle_area(v_minus_2, v_minus_1, v_0);
			v_minus_2 = std::exchange(v_minus_1, v_0);
		}

		return area;
	}

	path_finding::Path path_finding::shortest_path(
	        const Const_mesh_view mesh, const Vertex start, const Cost_function auto& cost)
	{
		if(auto h = cost(no_edge, no_vertex, no_vertex, start); std::holds_alternative<Target_found>(h)) {
			return {start};
		} else if(std::holds_alternative<Unreachable>(h)) {
			return {};
		}

		auto prev      = std::unordered_map<Vertex, Vertex>{{start, no_vertex}};
		auto costs_to  = std::unordered_map<Vertex, float>{{start, 1.f}};
		auto open_list = std::vector<Valid<Vertex>>{Valid<Vertex>(start)};

		while(!open_list.empty()) {
			// remove the element with the smallest cost
			auto current_iter = std::min_element(open_list.begin(), open_list.end(), [&](Vertex lhs, Vertex rhs) {
				return costs_to[lhs] < costs_to[rhs];
			});
			const auto current = *current_iter;
			*current_iter      = open_list.back();
			open_list.pop_back();

			const auto prev_vertex = prev[current];
			const auto cost_so_far = costs_to[current];

			for(const auto e : current.edges(mesh)) {
				const auto dest = e.dest(mesh);

				const auto h = cost(e, prev_vertex, current, dest);

				if(auto step_cost = std::get_if<float>(&h)) {
					assert(*step_cost > 0.f);

					const auto new_cost     = cost_so_far + *step_cost;
					auto&      current_cost = costs_to[dest];
					if(current_cost == 0.f) {
						open_list.push_back(dest);
					}
					if(current_cost == 0.f || current_cost > new_cost) {
						current_cost = new_cost;
						prev[dest]   = current;
					}

				} else if(std::holds_alternative<Target_found>(h)) {
					// found a destination => build and return path
					auto path = std::vector<Vertex>();
					path.push_back(dest);
					for(auto v = current.decay(); v != no_vertex; v = prev[v]) {
						path.push_back(v);
					}
					std::reverse(path.begin(), path.end());
					return path;
				}
			}
		}

		// no path found
		return {};
	}

} // namespace yggdrasill
