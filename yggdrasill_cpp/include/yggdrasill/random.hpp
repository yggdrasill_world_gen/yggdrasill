/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/random.h>
#include <yggdrasill/commons.hpp>
#include <yggdrasill/error.hpp>

#include <array>
#include <charconv>
#include <iomanip>
#include <iostream>
#include <iterator>


namespace yggdrasill {

	template <typename R, typename T>
	concept Contiguous_iterator_of = std::contiguous_iterator<R> && std::output_iterator<R, T>;

	/// Concept that checks for the restricted set of types supported by `Random_view::normal`
	template <typename T>
	concept Random_range_supported_normal = std::is_same_v<T, float> || std::is_same_v<T, double>;

	/// Concept that checks for the restricted set of types supported by `Random_view::uniform`
	template <typename T>
	concept Random_range_supported =
	        std::is_same_v<T, std::int8_t> || std::is_same_v<T, std::int32_t>
	        || std::is_same_v<T, std::int64_t> || std::is_same_v<T, float> || std::is_same_v<T, double>;

	/// A 128-bit seed that can be used to initialize/re-seed the RNG by calling `Random_view::seed`
	class Seed {
	  public:
		/// \returns A seed created from 4 32-bit numbers
		static constexpr Seed from(const std::array<std::uint32_t, 4>& data);
		/**\returns A seed created from the given string.
		 * \param str A string consisting of 32 characters, where each character has to be a valid
		 * hexadecimal digit [0-9a-zA-Z]
		 * \throws Precondition_violation_exception Id the string doesn't match the specified format
		 */
		static Seed from_str(std::string_view str);
		/// \returns A seed created by sampling the random number pool provided by the operating system
		static Seed random();

		/// \returns A string representation of the state, compatible with `from_str()`, that consists of
		/// 32 hexadecimal digit
		std::string str() const;

		/// Stream output operator that writes the string representation, also returned by `str()`
		friend std::ostream& operator<<(std::ostream& os, const Seed& s) { return os << s.str(); }

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if the argument is `nullptr` (for the pointer overload)
		 */
		[[nodiscard]] constexpr friend const YGDL_Seed* c_ptr(const Seed& r) { return &r.seed_; }
		[[nodiscard]] constexpr friend const YGDL_Seed* c_ptr(const Seed* r)
		{
			return r ? &r->seed_ : nullptr;
		}

	  private:
		YGDL_Seed seed_;
		constexpr Seed(const YGDL_Seed& s) : seed_(s) {}
	};

	/**A non-owning view of the internally managed pseudo random number generator of a `World`.
	 * \note The generated random numbers are guaranteed to be portable, i.e. the same on all platforms.\n
	 * \note The RNG is usually automatically seeded when the `World` is created.
	 */
	class Random_view {
	  public:
		/// Converting constructor from a reference to the C-API struct
		constexpr /*implicit*/ Random_view(YGDL_Random& rng) : rand_(&rng) {}

		/// Re-seeds the RNG. The same seed will always produce the same random numbers
		void seed(const Seed& s);

		/**Generates pseudo random numbers of the given type, uniformly distributed in the range [`min`,`max`].
		 * 
		 * The function may be called with a target range that the numbers should be written to [1],
		 * with a template parameter to return a fixed count [2] or without additional parameters [3] to
		 * only generate a single number.
		 * \tparam T The type of the values that should be generated. The set of types that is accepted
		 * here is limited by the C-API to: signed integers with 8, 32 or 64 bytes, float and double
		 */
		template <Random_range_supported T, Contiguous_iterator_of<T> Iterator>
		void uniform(T min, T max, Iterator out_begin, Iterator out_end) noexcept;
		template <std::size_t Count, Random_range_supported T>
		std::array<T, Count> uniform(T min, T max) noexcept;
		template <Random_range_supported T>
		T uniform(T min, T max) noexcept;

		/**Generates pseudo random numbers of the given type, following a normal distribution with the
		 * given `mean` and `standard_deviation`
		 * 
		 * The function may be called with a target range that the numbers should be written to [1],
		 * with a template parameter to return a fixed count [2] or without additional parameters [3] to
		 * only generate a single number.
		 */
		template <Random_range_supported_normal T, Contiguous_iterator_of<T> Iterator>
		void normal(T mean, T standard_deviation, Iterator out_begin, Iterator out_end) noexcept;
		template <std::size_t Count, Random_range_supported_normal T>
		std::array<T, Count> normal(T min, T max) noexcept;
		template <Random_range_supported_normal T>
		T normal(T min, T max) noexcept;

		/**Generates pseudo random `YGDL_Bool` with the given `probability` to be `YGDL_TRUE`.
		 * 
		 * The function may be called with a target range that the booleans should be written to [1],
		 * with a template parameter to return a fixed count [2] or without additional parameters [3] to
		 * only generate a single booleans.
		 */
		template <Contiguous_iterator_of<YGDL_Bool> Iterator>
		void probability(float p, Iterator out_begin, Iterator out_end) noexcept;
		template <std::size_t Count>
		std::array<YGDL_Bool, Count> probability(float p) noexcept;
		bool                         probability(float p) noexcept;

		/// Rearranges the elements in the given range to be in a random order
		template <typename Range>
		void shuffle(Range& c);

		/**Generates pseudo random `Vec2` objects, where each component is uniformly distributed in the
		 * range [`min`,`max`).
		 * 
		 * The function may be called with a target range that the numbers should be written to [1],
		 * with a template parameter to return a fixed count [2] or without additional parameters [3] to
		 * only generate a single number.
		 */
		template <Contiguous_iterator_of<Vec2> Iterator>
		void uniform_vec2(float min, float max, Iterator out_begin, Iterator out_end) noexcept;
		template <std::size_t Count>
		std::array<Vec2, Count> uniform_vec2(float min, float max) noexcept;
		Vec2                    uniform_vec2(float min, float max) noexcept;

		/**Generates pseudo random `Vec3` objects, where each component is uniformly distributed in the
		 * range [`min`,`max`).
		 * 
		 * The function may be called with a target range that the numbers should be written to [1],
		 * with a template parameter to return a fixed count [2] or without additional parameters [3] to
		 * only generate a single number.
		 */
		template <Contiguous_iterator_of<Vec3> Iterator>
		void uniform_vec3(float min, float max, Iterator out_begin, Iterator out_end) noexcept;
		template <std::size_t Count>
		std::array<Vec3, Count> uniform_vec3(float min, float max) noexcept;
		Vec3                    uniform_vec3(float min, float max) noexcept;

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
		 * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
		 */
		[[nodiscard]] constexpr friend YGDL_Random* c_ptr(Random_view r) { return r.rand_; }
		[[nodiscard]] constexpr friend YGDL_Random* c_ptr(Random_view* r) { return r ? r->rand_ : nullptr; }

	  private:
		YGDL_Random* rand_;
	};

} // namespace yggdrasill

#define YGDL_RANDOM_HPP_INCLUDED
#include <yggdrasill/random.inl>