/// NOTE: This is the template implementation header for dictionary.hpp and should not be included directly!
#pragma once

#ifndef YGDL_DICTIONARY_HPP_INCLUDED
#include <yggdrasill/dictionary.hpp>
#endif

namespace yggdrasill {

	namespace detail {

		/// used to determine the type to pass to Dictionary::get, based on the right-hand-side of the assignment,
		/// i.e. the following is valid: dict.get<normalized_api_type_helper<T>::type>() = T{};
		template <typename T>
		struct normalized_api_type_helper {
			using type = void;
		};

		template <typename T>
		using normalized_api_type = typename normalized_api_type_helper<std::remove_cvref_t<T>>::type;

		template <typename T>
		using normalized_api_type_v = std::remove_cvref_t<normalized_api_type<T>>;


		template <typename T>
		requires(std::is_integral_v<T> && !std::is_same_v<T, bool>)
		struct normalized_api_type_helper<T> {
			using type =
			        std::conditional_t<sizeof(T) <= 1,
			                           std::int8_t&,
			                           std::conditional_t<sizeof(T) <= sizeof(std::int32_t), std::int32_t&, std::int64_t&>>;
		};

		template <typename T>
		requires(std::is_floating_point_v<T>)
		struct normalized_api_type_helper<T> {
			using type = std::conditional_t<sizeof(T) <= sizeof(float), float&, double&>;
		};

		template <typename T>
		requires std::is_enum_v<T>
		struct normalized_api_type_helper<T> : normalized_api_type_helper<std::underlying_type_t<T>> {};

		// clang-format off
		template<> struct normalized_api_type_helper<void>   {using type = void; };
		template<> struct normalized_api_type_helper<YGDL_Bool> {using type = YGDL_Bool&; };
		template<> struct normalized_api_type_helper<bool>      {using type = YGDL_Bool&; };
		template<> struct normalized_api_type_helper<Vec2>   {using type = Vec2&; };
		template<> struct normalized_api_type_helper<Vec3>   {using type = Vec3&; };
		template<> struct normalized_api_type_helper<String> {using type = String&; };
		template<> struct normalized_api_type_helper<std::string_view> {using type = String&; };
		template<> struct normalized_api_type_helper<std::string> {using type = String&; };
		template<> struct normalized_api_type_helper<const char*> {using type = String&; };
		template<> struct normalized_api_type_helper<const volatile char*> {using type = String&; };
		template<> struct normalized_api_type_helper<volatile char*> {using type = String&; };
		template<std::size_t N> struct normalized_api_type_helper<const char[N]> {using type = String&; };
		template<std::size_t N> struct normalized_api_type_helper<char[N]> {using type = String&; };
		template<> struct normalized_api_type_helper<Const_dictionary_view>   {using type = Dictionary_ref; };
		template<> struct normalized_api_type_helper<Dictionary_view>   {using type = Dictionary_ref; };
		template<> struct normalized_api_type_helper<Dictionary_ref>   {using type = Dictionary_ref; };
		template<> struct normalized_api_type_helper<Dictionary>   {using type = Dictionary_ref; };
		template<> struct normalized_api_type_helper<Array_ref<void>>  {using type = Array_ref<void>; };
		template<> struct normalized_api_type_helper<Array_view<void>>  {using type = Array_ref<void>; };
		template<> struct normalized_api_type_helper<Const_array_view<void>>  {using type = Array_ref<void>; };
		template<typename T> struct normalized_api_type_helper<Array_ref<T>>  {using type = Array_ref<normalized_api_type_v<T>>; };
		template<typename T> struct normalized_api_type_helper<Array_view<T>>  {using type = Array_ref<normalized_api_type_v<T>>; };
		template<typename T> struct normalized_api_type_helper<Const_array_view<T>>  {using type = Array_ref<normalized_api_type_v<T>>; };
		template<typename T> struct normalized_api_type_helper<std::vector<T>>  {using type = Array_ref<normalized_api_type_v<T>>; };
		template<typename T> struct normalized_api_type_helper<std::span<T>>  {using type = Array_ref<normalized_api_type_v<T>>; };
		// clang-format on

		template <typename T>
		constexpr auto is_normalized_api_type =
		        std::is_same_v<std::remove_cvref_t<T>, std::remove_cvref_t<normalized_api_type<T>>>;

	} // namespace detail

	// IMPLEMENTATIONS: Const_dictionary_view
	[[nodiscard]] inline Range_of<std::string_view> auto Const_dictionary_view::keys() const
	{
		auto key_range = YGDL_dict_keys(&detail::c_ref(this));
		return map_range(key_range.keys, key_range.keys + key_range.size, [](const YGDL_String_view& str) {
			return detail::from_ygdl_string_view(str);
		});
	}
	inline Const_dictionary_value_view Const_dictionary_view::operator[](std::string_view key) const
	{
		return Const_dictionary_value_view{
		        YGDL_dict_get_const(&detail::c_ref(this), detail::to_ygdl_string_view(key))};
	}

	// IMPLEMENTATIONS: Dictionary_view
	inline Dictionary_value_ref Dictionary_view::operator[](std::string_view key)
	{
		return {YGDL_dict_get(&detail::c_ref(this), detail::to_ygdl_string_view(key))};
	}

	// IMPLEMENTATIONS: Dictionary_ref
	inline Dictionary_ref& Dictionary_ref::operator=(Dictionary_ref&& rhs) noexcept
	{
		YGDL_dict_move_assign(&detail::c_ref(this), &detail::c_ref(rhs));
		return *this;
	}
	inline Dictionary_ref& Dictionary_ref::operator=(const Dictionary_ref& rhs)
	{
		YGDL_dict_copy_assign(&detail::c_ref(this), &detail::c_ref(rhs));
		return *this;
	}
	inline Dictionary_ref& Dictionary_ref::operator=(const Dictionary_view& rhs)
	{
		YGDL_dict_copy_assign(&detail::c_ref(this), &detail::c_ref(rhs));
		return *this;
	}
	inline Dictionary_ref& Dictionary_ref::operator=(const Const_dictionary_view& rhs)
	{
		YGDL_dict_copy_assign(&detail::c_ref(this), &detail::c_ref(rhs));
		return *this;
	}

	// IMPLEMENTATIONS: Const_dictionary_value_view
	template <typename Visitor>
	decltype(auto) Const_dictionary_value_view::visit(Visitor&& visitor) const
	{
		// Note: YGDL_dict_value... calls and calling the visitor are in different statements because destructing Error_throw,
		// and thus the error handling, needs to happen before the result is used!
		switch(type()) {
			case Type::none: return std::forward<Visitor>(visitor)(); break;
			case Type::bool_t: {
				auto* ptr = YGDL_dict_value_const_bool(&detail::c_ref(this), Error_throw{});
				return std::forward<Visitor>(visitor)(*ptr);
				break;
			}
			case Type::int8_t: {
				auto* ptr = YGDL_dict_value_const_int8(&detail::c_ref(this), Error_throw{});
				return std::forward<Visitor>(visitor)(*ptr);
				break;
			}
			case Type::int32_t: {
				auto* ptr = YGDL_dict_value_const_int32(&detail::c_ref(this), Error_throw{});
				return std::forward<Visitor>(visitor)(*ptr);
				break;
			}
			case Type::int64_t: {
				auto* ptr = YGDL_dict_value_const_int64(&detail::c_ref(this), Error_throw{});
				return std::forward<Visitor>(visitor)(*ptr);
				break;
			}
			case Type::float_t: {
				auto* ptr = YGDL_dict_value_const_float(&detail::c_ref(this), Error_throw{});
				return std::forward<Visitor>(visitor)(*ptr);
				break;
			}
			case Type::double_t: {
				auto* ptr = YGDL_dict_value_const_double(&detail::c_ref(this), Error_throw{});
				return std::forward<Visitor>(visitor)(*ptr);
				break;
			}
			case Type::vec2_t: {
				auto* ptr = YGDL_dict_value_const_vec2(&detail::c_ref(this), Error_throw{});
				return std::forward<Visitor>(visitor)(*reinterpret_cast<const Vec2*>(ptr));
				break;
			}
			case Type::vec3_t: {
				auto* ptr = YGDL_dict_value_const_vec3(&detail::c_ref(this), Error_throw{});
				return std::forward<Visitor>(visitor)(*reinterpret_cast<const Vec3*>(ptr));
				break;
			}
			case Type::string_t: {
				auto* ptr = YGDL_dict_value_const_string(&detail::c_ref(this), Error_throw{});
				return std::forward<Visitor>(visitor)(*reinterpret_cast<const String*>(ptr));
				break;
			}
			case Type::array_t: {
				auto* ptr = YGDL_dict_value_const_array(&detail::c_ref(this), YGDL_TYPE_NONE, Error_throw{});
				return std::forward<Visitor>(visitor)(Const_array_view<void>(*ptr));
				break;
			}
			case Type::dict_t: {
				auto* ptr = YGDL_dict_value_const_dict(&detail::c_ref(this), Error_throw{});
				return std::forward<Visitor>(visitor)(Const_dictionary_view(*ptr));
				break;
			}
		}

		std::cerr << "unhandled case in " << __FILE__ << " " << __LINE__ << '\n';
		std::abort();
	}
	template <typename... Visitors>
	decltype(auto) Const_dictionary_value_view::visit(Visitors&&... visitors) const
	        requires(sizeof...(Visitors) > 1)
	{
		return visit(overloaded{std::forward<Visitors>(visitors)...});
	}

	template <typename T>
	const T Const_dictionary_value_view::get() const
	{
		constexpr auto type = detail::c_api_type<T>;
		static_assert(type != Type::none,
		              "Conversion to Type::none is not supported. Call erase instead to remove elements "
		              "from Dicts");

		// arrays need special handling, because we need to determine the element type
		if constexpr(detail::is_array_v<T>) {
			constexpr auto sub_type = static_cast<YGDL_Type_t>(detail::c_api_type<detail::array_value_type<T>>);
			auto ptr = YGDL_dict_value_const_array(&detail::c_ref(this), sub_type, Error_throw{});
			return {*ptr};
		}

		// all other types can be handled generically
		return visit(overloaded{
		        [&](const auto& v) -> const T {
			        if constexpr(detail::Explicitly_convertible_to<decltype(v), const T>)
				        return detail::overflow_safe_cast<const T>(v);
			        else
				        throw Type_mismatch_exception("Unexpected or missing type in dictionary");
		        },
		        [&]() -> const T { throw Type_mismatch_exception("Unexpected or missing type in dictionary"); }});
	}
	template <typename T>
	T Const_dictionary_value_view::get(const T& default_value) const
	        requires(!std::is_arithmetic_v<std::remove_cvref_t<T>>)
	{
		static_assert(detail::is_normalized_api_type<T>, "The requested type can't be represented exactly.");

		return visit(overloaded{[&](const std::remove_cvref_t<T>& v) -> const T& { return v; },
		                        [&](const auto&) -> const T& { return default_value; },
		                        [&]() -> const T& { return default_value; }});
	}
	template <typename T>
	T Const_dictionary_value_view::get(T default_value, const Validation<T>& validation) const
	        requires(std::is_arithmetic_v<std::remove_cvref_t<T>>)
	{
		using T_plain = std::remove_cvref_t<T>;

		return visit(overloaded{
		        [&](const auto& v) -> T_plain {
			        if constexpr(!detail::Explicitly_convertible_to<decltype(v), T_plain>) {
				        if(validation.throw_on_error)
					        throw Type_mismatch_exception("Unexpected or missing entry in dictionary");
				        else
					        return default_value;
			        } else {
				        auto value = detail::overflow_safe_cast<T_plain>(v);

				        if constexpr(std::is_floating_point_v<T_plain>) {
					        if(!validation.nan_allowed && std::isnan(value)) {
						        if(validation.throw_on_error)
							        throw Type_mismatch_exception("Dictionary value is NaN");
						        else
							        value = default_value;
					        }
					        if(!validation.inf_allowed && std::isinf(value)) {
						        if(validation.throw_on_error)
							        throw Type_mismatch_exception("Dictionary value is +-inf");
						        else
							        value = default_value;
					        }
				        }

				        if(validation.min && value < *validation.min) {
					        if(validation.throw_on_error)
						        throw Type_mismatch_exception(
						                "Dictionary value is less than required minimum.");
					        else
						        value = *validation.min;

				        } else if(validation.max && value > *validation.max) {
					        if(validation.throw_on_error)
						        throw Type_mismatch_exception(
						                "Dictionary value is greater than required maximum.");
					        else
						        value = *validation.max;
				        }
				        return value;
			        }
		        },
		        [&]() -> T_plain { return default_value; }});
	}

	template <typename T>
	bool Const_dictionary_value_view::operator==(const T& rhs) const
	{
		return visit(overloaded{[&](const auto& v) -> bool {
			                        if constexpr(std::is_same_v<T, bool>) {
				                        if constexpr(std::is_same_v<std::remove_cvref_t<decltype(v)>, YGDL_Bool>)
					                        return (v == YGDL_TRUE) == rhs;
				                        else
					                        return false;
			                        } else if constexpr(std::convertible_to<decltype(v), std::remove_cvref_t<T>>) {
				                        return static_cast<std::remove_cvref_t<T>>(v) == rhs;
			                        } else if constexpr(detail::equality_comparable<decltype(v), T>) {
				                        return v == rhs;
			                        } else
				                        return false;
		                        },
		                        []() -> bool { return false; }});
	}
	template <typename T>
	std::partial_ordering Const_dictionary_value_view::operator<=>(const T& rhs) const
	{
		return visit(
		        overloaded{[&](const auto& v) -> std::partial_ordering {
			                   if constexpr(std::is_same_v<T, bool>) {
				                   if constexpr(std::is_same_v<std::remove_cvref_t<decltype(v)>, YGDL_Bool>)
					                   return (v == YGDL_TRUE) <=> rhs;
				                   else
					                   return std::partial_ordering::unordered;
			                   } else if constexpr(detail::three_way_comparable<decltype(v), decltype(rhs)>) {
				                   return v <=> rhs;
			                   } else if constexpr(std::convertible_to<decltype(v), std::remove_cvref_t<T>>) {
				                   return static_cast<std::remove_cvref_t<T>>(v) <=> rhs;
			                   } else
				                   return std::partial_ordering::unordered;
		                   },
		                   []() -> std::partial_ordering { return std::partial_ordering::unordered; }});
	}

	inline void Const_dictionary_value_view::print(std::ostream& s) const
	{
		visit(overloaded{[&](Const_array_view<void>) { s << "[ARRAY]"; },
		                 [&](Const_dictionary_view) { s << "[DICT]"; },
		                 [&](const int8_t& vv) { s << int(vv); },
		                 [&](auto& vv) { s << vv; },
		                 [&] { s << "[NONE]"; }});
	}

	inline Const_dictionary_value_view Const_dictionary_value_view::operator[](std::string_view key) const
	{
		return get<Const_dictionary_view>()[key];
	}


	// IMPLEMENTATION: Dictionary_value_view
	inline void Dictionary_value_view::type(Type new_type, Type array_value_type)
	{
		assert(array_value_type == Type::none || new_type == Type::array_t); // parameter is only supported for arrays

		auto* c_ptr = &detail::c_ref(this);

		switch(new_type) {
			case Type::none: assert(!"Conversion to Type::none are not supported!"); break;
			case Type::bool_t: YGDL_dict_value_bool(c_ptr); break;
			case Type::int8_t: YGDL_dict_value_int8(c_ptr); break;
			case Type::int32_t: YGDL_dict_value_int32(c_ptr); break;
			case Type::int64_t: YGDL_dict_value_int64(c_ptr); break;
			case Type::float_t: YGDL_dict_value_float(c_ptr); break;
			case Type::double_t: YGDL_dict_value_double(c_ptr); break;
			case Type::vec2_t: YGDL_dict_value_vec2(c_ptr); break;
			case Type::vec3_t: YGDL_dict_value_vec3(c_ptr); break;
			case Type::string_t: YGDL_dict_value_string(c_ptr); break;
			case Type::array_t:
				YGDL_dict_value_array(c_ptr, static_cast<YGDL_Type_t>(array_value_type));
				break;
			case Type::dict_t: YGDL_dict_value_dict(c_ptr); break;
		}
	}

	template <typename Visitor>
	decltype(auto) Dictionary_value_view::visit(Visitor&& visitor)
	{
		switch(type()) {
			case Type::none: return std::forward<Visitor>(visitor)(); break;
			case Type::bool_t:
				return std::forward<Visitor>(visitor)(*YGDL_dict_value_bool(c_ptr(this)));
				break;
			case Type::int8_t:
				return std::forward<Visitor>(visitor)(*YGDL_dict_value_int8(c_ptr(this)));
				break;
			case Type::int32_t:
				return std::forward<Visitor>(visitor)(*YGDL_dict_value_int32(c_ptr(this)));
				break;
			case Type::int64_t:
				return std::forward<Visitor>(visitor)(*YGDL_dict_value_int64(c_ptr(this)));
				break;
			case Type::float_t:
				return std::forward<Visitor>(visitor)(*YGDL_dict_value_float(c_ptr(this)));
				break;
			case Type::double_t:
				return std::forward<Visitor>(visitor)(*YGDL_dict_value_double(c_ptr(this)));
				break;
			case Type::vec2_t:
				return std::forward<Visitor>(visitor)(*reinterpret_cast<Vec2*>(YGDL_dict_value_vec2(c_ptr(this))));
				break;
			case Type::vec3_t:
				return std::forward<Visitor>(visitor)(*reinterpret_cast<Vec3*>(YGDL_dict_value_vec3(c_ptr(this))));
				break;
			case Type::string_t:
				return std::forward<Visitor>(visitor)(
				        *reinterpret_cast<String*>(YGDL_dict_value_string(c_ptr(this))));
				break;
			case Type::array_t:
				return std::forward<Visitor>(visitor)(
				        Array_ref<void>(*YGDL_dict_value_array(c_ptr(this), YGDL_TYPE_NONE)));
				break;
			case Type::dict_t:
				return std::forward<Visitor>(visitor)(Dictionary_ref(*YGDL_dict_value_dict(c_ptr(this))));
				break;
		}

		std::cerr << "unhandled case in " << __FILE__ << " " << __LINE__ << '\n';
		std::abort();
	}
	template <typename... Visitors>
	decltype(auto) Dictionary_value_view::visit(Visitors&&... visitors) requires(sizeof...(Visitors) > 1)
	{
		return visit(overloaded{std::forward<Visitors>(visitors)...});
	}

	template <typename T>
	T Dictionary_value_view::get()
	{
		constexpr auto type = detail::c_api_type<T>;
		static_assert(type != Type::none,
		              "Conversion to Type::none is not supported. Call erase instead to remove elements "
		              "from Dicts");

		if constexpr(std::is_reference_v<T>) {
			// caller requested a T& to be returned => the type has to match exactly, and we can't apply conversions
			static_assert(!detail::is_view_type<std::remove_cvref_t<T>>,
			              "Proxy types like *_view or *_ref can't be returned by reference.");
			static_assert(detail::is_normalized_api_type<T>,
			              "The requested type can't be represented exactly.");

			if constexpr(type == Type::bool_t)
				return *YGDL_dict_value_bool(&detail::c_ref(this));
			else if constexpr(type == Type::int8_t)
				return *YGDL_dict_value_int8(&detail::c_ref(this));
			else if constexpr(type == Type::int32_t)
				return *YGDL_dict_value_int32(&detail::c_ref(this));
			else if constexpr(type == Type::int64_t)
				return *YGDL_dict_value_int64(&detail::c_ref(this));
			else if constexpr(type == Type::float_t)
				return *YGDL_dict_value_float(&detail::c_ref(this));
			else if constexpr(type == Type::double_t)
				return *YGDL_dict_value_double(&detail::c_ref(this));
			else if constexpr(type == Type::vec2_t)
				return *reinterpret_cast<Vec2*>(YGDL_dict_value_vec2(&detail::c_ref(this)));
			else if constexpr(type == Type::vec3_t)
				return *reinterpret_cast<Vec3*>(YGDL_dict_value_vec3(&detail::c_ref(this)));
			else if constexpr(type == Type::string_t)
				return *reinterpret_cast<String*>(YGDL_dict_value_string(&detail::c_ref(this)));
			else {
				static_assert(type != type, "unreachable");
			}

		} else if constexpr(detail::is_array_v<T>) {
			constexpr auto sub_type = static_cast<YGDL_Type_t>(detail::c_api_type<detail::array_value_type<T>>);
			return T(*YGDL_dict_value_array(&detail::c_ref(this), sub_type));
		} else if constexpr(detail::is_dict<T>) {
			return T(*YGDL_dict_value_dict(&detail::c_ref(this)));
		}

		return detail::overflow_safe_cast<T>(get<detail::normalized_api_type<T>>());
	}
	template <typename T>
	auto& Dictionary_value_view::get(const T& default_value)
	        requires(!std::is_arithmetic_v<std::remove_cvref_t<T>>)
	{
		using Actual_type = detail::normalized_api_type<T>;
		if(!convertible(detail::c_api_type<Actual_type>)) {
			return get<Actual_type>() = default_value;
		} else {
			return get<Actual_type>();
		}
	}
	template <typename T>
	T Dictionary_value_view::get(const T& default_value, const Validation<T>& v)
	        requires(std::is_arithmetic_v<std::remove_cvref_t<T>>)
	{
		using Actual_type = detail::normalized_api_type<T>;

		if(!convertible(detail::c_api_type<Actual_type>)) {
			if(v.throw_on_error)
				throw Type_mismatch_exception("Unexpected or missing type in dictionary");
			else
				get<Actual_type>() = default_value;
		}

		auto&& value = get<Actual_type>();

		if constexpr(std::is_floating_point_v<std::remove_cvref_t<Actual_type>>) {
			if(!v.nan_allowed && std::isnan(value)) {
				if(v.throw_on_error)
					throw Type_mismatch_exception("Dictionary value is NaN");
				else
					value = default_value;
			}
			if(!v.inf_allowed && std::isinf(value)) {
				if(v.throw_on_error)
					throw Type_mismatch_exception("Dictionary value is +-inf");
				else
					value = default_value;
			}
		}

		if(v.min && value < *v.min) {
			if(v.throw_on_error)
				throw Type_mismatch_exception("Unexpected or missing type in dictionary");
			else
				value = *v.min;

		} else if(v.max && value > *v.max) {
			if(v.throw_on_error)
				throw Type_mismatch_exception("Unexpected or missing type in dictionary");
			else
				value = *v.max;
		}

		return detail::overflow_safe_cast<T>(value);
	}

	inline Dictionary_value_ref Dictionary_value_view::operator[](std::string_view key)
	{
		return get<Dictionary_ref>()[key];
	}


	// IMPLEMENTATION: Dictionary_value_ref
	template <typename T>
	Dictionary_value_ref& Dictionary_value_ref::assign(T&& v)
	{
		if constexpr(std::convertible_to<T&&, const Const_dictionary_value_view&>) {
			// rhs is a dict-value => directly copy-assign the underlying std::variant
			YGDL_dict_value_copy_assign(&detail::c_ref(this), &detail::c_ref(v));

		} else {
			// normal conversion + assignment
			detail::narrow_into(get<detail::normalized_api_type<T>>(), std::forward<T>(v));
		}

		return *this;
	}

} // namespace yggdrasill
