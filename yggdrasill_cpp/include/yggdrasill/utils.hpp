/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/dictionary.h>
#include <yggdrasill/string.h>

#include <yggdrasill/commons.hpp>
#include <yggdrasill/error.hpp>

#include <iomanip>
#include <locale>
#include <optional>
#include <span>
#include <sstream>
#include <string_view>
#include <type_traits>
#include <vector>

namespace yggdrasill {

	class String;
	class Const_dictionary_view;

	class Dictionary_view;
	class Dictionary_ref;
	class Dictionary;

	template <typename T>
	class Const_array_view;
	template <typename T>
	class Array_view;
	template <typename T>
	class Array_ref;
} // namespace yggdrasill

namespace yggdrasill::detail {

	/// \returns A reference to the C-API type wrapped by `v`
	/// \attention The behaviour is undefined if `v` wraps a `nullptr`, e.g. because it's in a moved from state.
	template <typename T>
	constexpr auto& c_ref(T&& v)
	{
		auto* ptr = c_ptr(v);
		assert(ptr != nullptr);
		return *ptr;
	}

	template <typename Constness, typename Type>
	using copy_constness = std::conditional_t<std::is_const_v<Constness>, std::add_const_t<Type>, Type>;


	template <typename T>
	struct is_array {
		static constexpr auto value = false;
		using value_type            = void;
	};
	template <typename T>
	struct is_array<Const_array_view<T>> {
		static constexpr auto value = true;
		using value_type            = T;
	};
	template <typename T>
	struct is_array<Array_view<T>> {
		static constexpr auto value = true;
		using value_type            = T;
	};
	template <typename T>
	struct is_array<Array_ref<T>> {
		static constexpr auto value = true;
		using value_type            = T;
	};
	template <>
	struct is_array<Const_array_view<void>> {
		static constexpr auto value = true;
		using value_type            = void;
	};
	template <>
	struct is_array<Array_view<void>> {
		static constexpr auto value = true;
		using value_type            = void;
	};
	template <>
	struct is_array<Array_ref<void>> {
		static constexpr auto value = true;
		using value_type            = void;
	};

	template <typename T>
	constexpr bool is_array_v = is_array<std::remove_cvref_t<T>>::value; // TODO: remove _v

	template <typename T>
	using array_value_type = typename is_array<std::remove_cvref_t<T>>::value_type;

	template <typename T>
	constexpr bool is_dict = std::is_base_of_v<Const_dictionary_view, std::remove_cvref_t<T>>;

	template <typename T>
	constexpr bool is_view_type =
	        is_array_v<T> || is_dict<T> || std::is_same_v<std::string_view, std::remove_cvref_t<T>>;

	template <typename T>
	struct make_const_helper {
		using type = const T;
	};
	template <typename T>
	requires(is_view_type<T>)
	struct make_const_helper<T> {
		using type = typename T::const_view;
	};
	template <typename T>
	requires(is_view_type<T> && std::is_same_v<std::string_view, std::remove_cvref_t<T>>)
	struct make_const_helper<T> {
		using type = T;
	};
	template <typename T>
	using make_const = typename make_const_helper<T>::type;


	// template lookup-up table from type to YGDL_Type
	template <typename T>
	struct C_api_type_helper {};

	// clang-format off
		template<> struct C_api_type_helper<void>                     {static constexpr auto value = Type::none;  };
		template<> struct C_api_type_helper<YGDL_Bool>                {static constexpr auto value = Type::bool_t;  };
		template<> struct C_api_type_helper<bool>                     {static constexpr auto value = Type::bool_t;  };
		template<> struct C_api_type_helper<std::int8_t>              {static constexpr auto value = Type::int8_t;  };
		template<> struct C_api_type_helper<std::int32_t>             {static constexpr auto value = Type::int32_t; };
		template<> struct C_api_type_helper<std::uint32_t>            {static constexpr auto value = Type::int32_t; };
		template<> struct C_api_type_helper<std::int64_t>             {static constexpr auto value = Type::int64_t; };
		template<> struct C_api_type_helper<std::uint64_t>            {static constexpr auto value = Type::int64_t; };
		template<> struct C_api_type_helper<float>                    {static constexpr auto value = Type::float_t; };
		template<> struct C_api_type_helper<double>                   {static constexpr auto value = Type::double_t; };
		template<> struct C_api_type_helper<Vec2>                     {static constexpr auto value = Type::vec2_t;  };
		template<> struct C_api_type_helper<Vec3>                     {static constexpr auto value = Type::vec3_t;  };
		template<> struct C_api_type_helper<String>                   {static constexpr auto value = Type::string_t;};
		template<> struct C_api_type_helper<std::string_view>         {static constexpr auto value = Type::string_t;};
		template<> struct C_api_type_helper<Const_dictionary_view>          {static constexpr auto value = Type::dict_t;  };
		template<> struct C_api_type_helper<Dictionary_view>                {static constexpr auto value = Type::dict_t;  };
		template<> struct C_api_type_helper<Dictionary_ref>                 {static constexpr auto value = Type::dict_t;  };
		template<> struct C_api_type_helper<Dictionary>                     {static constexpr auto value = Type::dict_t;  };
		template<> struct C_api_type_helper<Array_ref<void>>          {static constexpr auto value = Type::array_t; };
		template<> struct C_api_type_helper<Array_view<void>>         {static constexpr auto value = Type::array_t; };
		template<> struct C_api_type_helper<Const_array_view<void>>   {static constexpr auto value = Type::array_t; };
		template<typename T> struct C_api_type_helper<Array_ref<T>>   {static constexpr auto value = Type::array_t; };
		template<typename T> struct C_api_type_helper<Array_view<T>>  {static constexpr auto value = Type::array_t; };
		template<typename T> struct C_api_type_helper<Const_array_view<T>> {static constexpr auto value = Type::array_t; };
	// clang-format on

	template <typename T>
	requires std::is_enum_v<T>
	struct C_api_type_helper<T> : C_api_type_helper<std::underlying_type_t<T>> {};

	template <typename T>
	constexpr Type c_api_type = C_api_type_helper<std::remove_cvref_t<T>>::value;

	template <typename T>
	constexpr bool return_by_reference = std::is_reference_v<T> || is_array_v<T>
	                                     || std::is_same_v<T, Dictionary> || std::is_same_v<T, String>;


	template <typename... Ts>
	auto concat(const Ts&... vs) requires(std::is_convertible_v<const Ts&, std::string_view> && ...)
	{
		auto str = std::string();
		str.reserve((std::string_view(vs).size() + ...));
		(str.append(vs), ...);
		return str;
	}
	template <typename... Ts>
	auto concat(const Ts&... vs) requires(!(std::is_convertible_v<const Ts&, std::string_view> && ...))
	{
		struct facet : std::numpunct<char> {
			char        do_thousands_sep() const { return '\''; }
			std::string do_grouping() const { return "\03"; }
		};
		static const auto loc = std::locale(std::locale::classic(), new facet);

		auto ss = std::ostringstream();
		ss << std::boolalpha << std::setprecision(8);
		(ss << ... << vs);
		return std::move(ss).str();
	}

	template <typename Target, typename Source>
	Target overflow_safe_cast(Source&& src)
	{
		using Source_plain = std::remove_cvref_t<Source>;

		if constexpr(std::is_reference_v<Target>) {
			return src;

		} else if constexpr(std::is_integral_v<Target> && std::is_integral_v<Source_plain>) {
			constexpr auto same_signedness = std::is_signed_v<Target> == std::is_signed_v<Source_plain>;
			Target         result          = static_cast<Target>(src);

			if(static_cast<Source_plain>(result) != src
			   || (!same_signedness && ((result < Target{}) != (src < Source_plain{})))) {
				throw Type_mismatch_exception("Overflow/Underflow trying to cast '" + std::to_string(src)
				                              + "' to target type");
			}

			return result;

		} else {
			return static_cast<Target>(src);
		}
	}

	template <typename From, typename To>
	concept Explicitly_convertible_to =
	        (std::is_reference_v<To> && std::convertible_to<From, To>)
	        || (std::is_integral_v<std::remove_cvref_t<From>> && std::is_integral_v<std::remove_cvref_t<To>>)
	        || (!std::is_reference_v<To> && requires { static_cast<To>(std::declval<From>()); });

	template <typename T>
	constexpr auto is_arithmetic_like = std::is_arithmetic_v<T> || std::is_enum_v<T>;

	template <typename Target, typename Source>
	Target narrow(Source&& src)
	{
		using Source_plain = std::remove_cvref_t<Source>;

		if constexpr(is_arithmetic_like<Source_plain> && is_arithmetic_like<Target>) {
			const auto result = static_cast<Target>(src);
			if constexpr(sizeof(Target) >= sizeof(Source))
				return result; // target is larger

			// target is smaller => might truncate
			if(static_cast<Source_plain>(result) != src) {
				if constexpr(requires { std::stringstream{} << src; })
					throw Type_mismatch_exception(
					        concat("Truncation trying to narrow '", src, "' to target type"));
				else
					throw Type_mismatch_exception(concat("Truncation trying to narrow value to target type"));
			}

			return result;

		} else {
			return std::forward<Source>(src);
		}
	}
	template <typename Target, typename Source>
	void narrow_into(Target&& target, Source&& src)
	{
		if constexpr(is_arithmetic_like<std::remove_cvref_t<Source>>
		             && is_arithmetic_like<std::remove_cvref_t<Target>>) {
			target = narrow<std::remove_cvref_t<Target>>(std::forward<Source>(src));
		} else {
			target = std::forward<Source>(src);
		}
	}

#if __cpp_lib_three_way_comparison >= 201907
	// clang-format off
		template <class T, class U>
		concept equality_comparable = requires(const std::remove_reference_t<T>& t,
		                                       const std::remove_reference_t<U>& u)
		{
			{t == u} -> std::same_as<bool>;
		};
		template <class T, class U>
		concept three_way_comparable = requires(const std::remove_reference_t<T>& t,
		                                        const std::remove_reference_t<U>& u)
		{
			{t <=> u} -> std::convertible_to<std::partial_ordering>;
		};
	// clang-format on

#else
	template <class T, class U>
	concept equality_comparable = false;
	template <class T, class U>
	concept three_way_comparable = false;
#endif

	template <typename T, template <typename...> class Z>
	struct is_specialization_of : std::false_type {};

	template <typename... Args, template <typename...> class Z>
	struct is_specialization_of<Z<Args...>, Z> : std::true_type {};

	template <typename T, template <typename...> class Z>
	inline constexpr bool is_specialization_of_v = is_specialization_of<T, Z>::value;

	template <typename T>
	void safe_assign_optional(std::optional<T>& dst, std::optional<T>&& src)
	{
		if(src) {
			dst.emplace(std::move(*src));
		} else {
			dst.reset();
		}
	}

	template <typename T, typename... Ts>
	struct first_impl {
		using type = T;
	};

	template <typename... Ts>
	using first = typename first_impl<Ts...>::type;


	template <typename T, std::size_t Alignment = alignof(T)>
	class Aligned_allocator {
	  public:
		typedef T*        pointer;
		typedef const T*  const_pointer;
		typedef T&        reference;
		typedef const T&  const_reference;
		typedef T         value_type;
		typedef size_t    size_type;
		typedef ptrdiff_t difference_type;

		constexpr Aligned_allocator() = default;
		template <typename U, std::size_t A>
		constexpr Aligned_allocator(const Aligned_allocator<U, A>&) noexcept {}

		bool operator==(const Aligned_allocator& other) const = default;

		constexpr T*       address(T& r) const { return &r; }
		constexpr const T* address(const T& s) const { return &s; }

		constexpr size_t max_size() const
		{
			return (static_cast<size_t>(0) - static_cast<size_t>(1)) / sizeof(T);
		}

		template <typename U>
		struct rebind {
			typedef Aligned_allocator<U, Alignment> other;
		};

		constexpr void construct(T* const p, const T& t) const { new(static_cast<void*>(p)) T(t); }
		constexpr void destroy(T* const p) const { p->~T(); }

		T* allocate(const std::size_t n) const
		{
			if(n == 0) {
				return nullptr;
			}

			if(n > max_size()) {
				throw std::length_error("Alloc_aligned<T,Alignment>::allocate() - Integer overflow.");
			}

			const auto size         = n * sizeof(T);
			const auto aligned_size = ((size - 1) / Alignment + 1) * Alignment;
#ifdef _MSC_VER
			// special case for MSVC, which doesn't implement std::aligned_alloc
			auto* const pv = _aligned_malloc(aligned_size, Alignment);
#else
			auto* const pv = std::aligned_alloc(Alignment, aligned_size);
#endif
			assert((reinterpret_cast<std::uintptr_t>(pv) % Alignment) == 0);

			if(pv == nullptr) {
				throw std::bad_alloc();
			}

			return static_cast<T*>(pv);
		}
		template <typename U>
		T* allocate(const std::size_t n, const U*) const
		{
			return allocate(n);
		}

		void deallocate(T* const p, const std::size_t) const
		{
#ifdef _MSC_VER
			_aligned_free(p);
#else
			std::free(p);
#endif
		}
	};

	template <typename T>
	using Aligned_vector = std::vector<T, detail::Aligned_allocator<T, 64>>;

} // namespace yggdrasill::detail
