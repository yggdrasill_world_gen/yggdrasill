/// NOTE: This is the template implementation header for random.hpp and should not be included directly!
#pragma once

#ifndef YGDL_RANDOM_HPP_INCLUDED
#include <yggdrasill/random.hpp>
#endif

namespace yggdrasill {

	// IMPLEMENTATION: Seed
	inline constexpr Seed Seed::from(const std::array<std::uint32_t, 4>& data)
	{
		return {{data[0], data[1], data[2], data[3]}};
	}
	inline Seed Seed::from_str(std::string_view str)
	{
		if(str.size() != 8 * 4)
			throw Precondition_violation_exception("The string passed to Seed::from_str() is invalid.");

		auto seed = YGDL_Seed{};
		for(int i = 0; i < 4; ++i) {
			const auto num = str.substr(i * 8, 8);
			const auto end = num.data() + num.size();
			const auto r   = std::from_chars(num.data(), end, seed.data[i], 16);
			if(r.ptr != end) {
				throw Precondition_violation_exception("The string passed to Seed::from_str() is invalid.");
			}
		}
		return {seed};
	}
	inline Seed Seed::random()
	{
		return {YGDL_seed_create()};
	}

	inline std::string Seed::str() const
	{
		auto str = std::string(8 * 4, '0');
		for(int i = 0; i < 4; ++i) {
			// write 32bit number as hex, zero fill if shorter than max. length
			const auto begin = str.data() + (i * 8);
			const auto end   = str.data() + ((i + 1) * 8);

			auto end_written = std::to_chars(begin, end, seed_.data[i], 16).ptr;
			if(end_written < end) {
				auto b = std::move_backward(begin, end_written, end);
				std::fill(begin, b, '0');
			}
		}
		return str;
	}


	// IMPLEMENTATION: Random_view
	inline void Random_view::seed(const Seed& s)
	{
		YGDL_random_seed(rand_, c_ptr(s));
	}

	template <Random_range_supported T, Contiguous_iterator_of<T> Iterator>
	void Random_view::uniform(T min, T max, Iterator out_begin, Iterator out_end) noexcept
	{
		const auto count = static_cast<index_t>(std::distance(out_begin, out_end));
		if constexpr(std::is_same_v<T, std::int8_t>) {
			YGDL_random_uniform_int8(rand_, min, max, count, &*out_begin);
		} else if constexpr(std::is_same_v<T, std::int32_t>) {
			YGDL_random_uniform_int32(rand_, min, max, count, &*out_begin);
		} else if constexpr(std::is_same_v<T, std::int64_t>) {
			YGDL_random_uniform_int64(rand_, min, max, count, &*out_begin);
		} else if constexpr(std::is_same_v<T, float>) {
			YGDL_random_uniform_float(rand_, min, max, count, &*out_begin);
		} else if constexpr(std::is_same_v<T, double>) {
			YGDL_random_uniform_double(rand_, min, max, count, &*out_begin);
		} else {
			static_assert(std::is_same_v<T, T>, "Unsupported type in Random_view::uniform");
		}
	}
	template <std::size_t Count, Random_range_supported T>
	std::array<T, Count> Random_view::uniform(T min, T max) noexcept
	{
		std::array<T, Count> out;
		uniform(min, max, out.begin(), out.end());
		return out;
	}
	template <Random_range_supported T>
	inline T Random_view::uniform(T min, T max) noexcept
	{
		T v;
		uniform(min, max, &v, &v + 1);
		return v;
	}


	template <Random_range_supported_normal T, Contiguous_iterator_of<T> Iterator>
	void Random_view::normal(T mean, T standard_deviation, Iterator out_begin, Iterator out_end) noexcept
	{
		const auto count = static_cast<index_t>(std::distance(out_begin, out_end));
		if constexpr(std::is_same_v<T, float>) {
			YGDL_random_normal_float(rand_, mean, standard_deviation, count, &*out_begin);
		} else if constexpr(std::is_same_v<T, double>) {
			YGDL_random_normal_double(rand_, mean, standard_deviation, count, &*out_begin);
		} else {
			static_assert(std::is_same_v<T, T>, "Unsupported type in Random_view::normal");
		}
	}
	template <std::size_t Count, Random_range_supported_normal T>
	std::array<T, Count> Random_view::normal(T mean, T standard_deviation) noexcept
	{
		std::array<T, Count> out;
		normal(mean, standard_deviation, out.begin(), out.end());
		return out;
	}
	template <Random_range_supported_normal T>
	inline T Random_view::normal(T mean, T standard_deviation) noexcept
	{
		T v;
		normal(mean, standard_deviation, &v, &v + 1);
		return v;
	}


	template <Contiguous_iterator_of<Vec2> Iterator>
	void Random_view::uniform_vec2(float min, float max, Iterator out_begin, Iterator out_end) noexcept
	{
		const auto count = static_cast<index_t>(std::distance(out_begin, out_end)) * 2;
		YGDL_random_uniform_float(rand_, min, max, count, reinterpret_cast<float*>(&*out_begin));
	}
	template <std::size_t Count>
	std::array<Vec2, Count> Random_view::uniform_vec2(float min, float max) noexcept
	{
		std::array<Vec2, Count> out;
		uniform(min, max, out.begin(), out.end());
		return out;
	}
	inline Vec2 Random_view::uniform_vec2(float min, float max) noexcept
	{
		auto arr = uniform<2>(min, max);
		return {arr[0], arr[1]};
	}

	template <Contiguous_iterator_of<Vec3> Iterator>
	void Random_view::uniform_vec3(float min, float max, Iterator out_begin, Iterator out_end) noexcept
	{
		const auto count = static_cast<index_t>(std::distance(out_begin, out_end)) * 3;
		YGDL_random_uniform_float(rand_, min, max, count, reinterpret_cast<float*>(&*out_begin));
	}
	template <std::size_t Count>
	std::array<Vec3, Count> Random_view::uniform_vec3(float min, float max) noexcept
	{
		std::array<Vec3, Count> out;
		uniform(min, max, out.begin(), out.end());
		return out;
	}
	inline Vec3 Random_view::uniform_vec3(float min, float max) noexcept
	{
		auto arr = uniform<3>(min, max);
		return {arr[0], arr[1], arr[2]};
	}


	template <Contiguous_iterator_of<YGDL_Bool> Iterator>
	void Random_view::probability(float p, Iterator out_begin, Iterator out_end) noexcept
	{
		YGDL_random_bool(rand_, p, static_cast<index_t>(std::distance(out_begin, out_end)), &*out_begin);
	}
	template <std::size_t Count>
	std::array<YGDL_Bool, Count> Random_view::probability(float p) noexcept
	{
		std::array<YGDL_Bool, Count> out;
		probability(p, out.begin(), out.end());
		return out;
	}
	inline bool Random_view::probability(float p) noexcept
	{
		YGDL_Bool v;
		probability(p, &v, &v + 1);
		return static_cast<bool>(v);
	}

	template <typename Range>
	inline void Random_view::shuffle(Range& c)
	{
		auto begin = c.begin();

		auto n = c.end() - begin;
		for(auto i = n - 1; i > 0; --i) {
			using std::swap;
			swap(begin[i], begin[uniform(0, static_cast<std::int32_t>(i))]);
		}
	}

} // namespace yggdrasill
