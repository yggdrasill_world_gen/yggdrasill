/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/dictionary.h>

#include <yggdrasill/array.hpp>
#include <yggdrasill/commons.hpp>
#include <yggdrasill/error.hpp>
#include <yggdrasill/string.hpp>
#include <yggdrasill/utils.hpp>

#include <algorithm>
#include <concepts>
#include <iterator>
#include <optional>
#include <string_view>
#include <type_traits>

namespace yggdrasill {

	// forward declarations
	class Const_dictionary_value_view;
	class Dictionary_value_view;
	class Dictionary_value_ref;
	class Dictionary;
	template <typename T>
	struct Validation;

	/**\returns true if a (possibly lossy) conversion between the two types is possible.
     * \note If this function returns `true` the system will conserve some of the previous value if a
     * conversion is requested by changing the type or requesting a different type than the stored one.
     * Otherwise a value-initialized object will be used, instead.
     */
	inline bool is_convertible(Type from, Type to)
	{
		return YGDL_type_is_convertible(static_cast<YGDL_Type_t>(from), static_cast<YGDL_Type_t>(to));
	}

	/**An immutable non-owning view of a JSON-like key-value store, with string keys and values that
     * are either concrete values, sequences of values or nested key-value stores.
     *
     * The concrete instance is either owned by a `Dictionary` object or as part of the underlying `World` structure.
     */
	class Const_dictionary_view {
	  public:
		/// \skip
		using const_view = Const_dictionary_view;

		/// Converting constructor from a reference to the C-API struct
		constexpr /*implicit*/ Const_dictionary_view(const YGDL_Dict& dict) : dict_(&dict) {}
		Const_dictionary_view(const YGDL_Dict&&)  = delete; ///< \skip forbid construction from temporary
		Const_dictionary_view(const Dictionary&&) = delete; ///< \skip forbid construction from temporary

		/**\returns An immutable non-owning range over every key in the key-value store.
         * \note This only returns the keys directly contained in the current level, i.e. those
         * accessible through `operator[]`, and not those of any nested dictionaries.
         * \note The returned range is invalidated by any call to a non-const member-function
         */
		[[nodiscard]] Range_of<std::string_view> auto keys() const;

		/**\returns An immutable non-owning view of the value mapped to the given key.
         * \note If the key is not currently mapped, the returned view represents a missing value and has the type `Type::none`
         * \note The returned view remains valid until the key is erases. Either directly through `Dictionary_view::erase` or
         * indirectly by assigning to a `Dictionary_ref`, moving from the underlying `Dictionary` or otherwise destroying the underlying dictionary.
         */
		Const_dictionary_value_view operator[](std::string_view key) const;

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
         * and as an escape hatch for directly interaction with the C-API
         * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
         * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
         */
		[[nodiscard]] constexpr friend const YGDL_Dict* c_ptr(Const_dictionary_view d) { return d.dict_; }

		[[nodiscard]] constexpr friend const YGDL_Dict* c_ptr(const Const_dictionary_view* d)
		{
			return d ? d->dict_ : nullptr;
		}

	  protected:
		const YGDL_Dict* dict_; ///< \skip
	};

	/// A mutable non-owning view of a JSON-like key-value store.
	/// \see `Const_dictionary_view`
	class Dictionary_view : public Const_dictionary_view {
	  public:
		/// Converting constructor from a reference to the C-API struct
		constexpr /*implicit*/ Dictionary_view(YGDL_Dict& dict) : Const_dictionary_view(dict) {}
		Dictionary_view(const YGDL_Dict&&)  = delete; ///< \skip forbid construction from temporary
		Dictionary_view(const Dictionary&&) = delete; ///< \skip forbid construction from temporary

		/**\returns A non-owning view of the value mapped to the given key.
         * \note If the key is not currently mapped, a new empty entry is inserted into the key-value store.
         * \note The returned view remains valid until the key is erases. Either directly through `Dictionary_view::erase` or
         * indirectly by assigning to a `Dictionary_ref`, moving from the underlying `Dictionary` or otherwise destroying the underlying dictionary.
         */
		Dictionary_value_ref operator[](std::string_view key);

		using Const_dictionary_view::operator[];

		/// Removes any mapping of the given key from the dictionary, if there is any.
		/// \note Invalidates all pointers/references/views to the value and its underlying storage.
		void erase(std::string_view key) { YGDL_dict_erase(c_ptr(*this), detail::to_ygdl_string_view(key)); }

		/// Removes all mappings from the dictionary.
		/// \note Invalidates all pointers/references/views to all values in the dictionary.
		void clear() { YGDL_dict_clear(c_ptr(*this)); }

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
         * and as an escape hatch for directly interaction with the C-API
         * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
         * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
         */
		[[nodiscard]] constexpr friend YGDL_Dict* c_ptr(Dictionary_view d)
		{
			// we can safely cast away const here, because (inside the Dictionary_view class) we know that dict_ was initialized from a pointer to non-const
			return const_cast<YGDL_Dict*>(c_ptr(static_cast<const Const_dictionary_view&>(d)));
		}

		[[nodiscard]] constexpr friend YGDL_Dict* c_ptr(Dictionary_view* d)
		{
			return const_cast<YGDL_Dict*>(c_ptr(static_cast<const Const_dictionary_view*>(d)));
		}
	};

	/**A mutable non-owning view of a JSON-like key-value store with reference semantics.
     *
     * The only difference to its base class `Dictionary_view` is that assignments to this class have reference- instead
     * of pointer-semantics. So, assigning to this type modifies the underlying dictionary instead of rebinding it to point to an entirely different one.
     * \see `Const_dictionary_view`
     */
	class Dictionary_ref : public Dictionary_view {
	  public:
		/// Converting constructor from a reference to the C-API struct
		constexpr /*implicit*/ Dictionary_ref(YGDL_Dict& dict) : Dictionary_view(dict) {}
		Dictionary_ref(const YGDL_Dict&&)  = delete; ///< \skip forbid construction from temporary
		Dictionary_ref(const Dictionary&&) = delete; ///< \skip forbid construction from temporary

		constexpr Dictionary_ref(Dictionary_ref&& rhs) noexcept = default; ///< Move constructor \skip
		constexpr Dictionary_ref(const Dictionary_ref& rhs)     = default; ///< Copy constructor \skip
		constexpr ~Dictionary_ref()                             = default; ///< \skip

		Dictionary_ref& operator=(Dictionary_ref&& rhs) noexcept;    ///< Move assignment operator \skip
		Dictionary_ref& operator=(const Dictionary_ref& rhs);        ///< Copy assignment operator \skip
		Dictionary_ref& operator=(const Dictionary_view& rhs);       ///< Copy assignment operator \skip
		Dictionary_ref& operator=(const Const_dictionary_view& rhs); ///< Copy assignment operator \skip
	};

	/**A mutable JSON-like key-value store that owns the underlying storage and has value semantics.
     * \note You usually want to pass instances of this type as references to its base classes, to restrict
     * the allowed operations and be compatible with dictionaries acquired from other
     * sources (e.g. returned as unstructured layers from the `World` API).
     */
	class Dictionary : public Dictionary_ref {
	  public:
		/// Default constructor, that creates a new empty dictionary.
		Dictionary() : Dictionary_ref(*YGDL_dict_create()) {} ///< \skip
		constexpr Dictionary(Dictionary&& rhs) noexcept : Dictionary_ref(rhs)
		{
			rhs.dict_ = nullptr;
		}                                                                                  ///< \skip
		Dictionary(const Dictionary& rhs) : Dictionary_ref(*YGDL_dict_copy(c_ptr(rhs))) {} ///< \skip
		explicit Dictionary(const Const_dictionary_view& rhs)
		  : Dictionary_ref(*YGDL_dict_copy(c_ptr(rhs))) {} ///< \skip
		/// \skip
		~Dictionary()
		{
			if(auto ptr = c_ptr(this))
				YGDL_dict_destroy(ptr);
		}

		using Dictionary_ref::operator=;

		/// \skip
		Dictionary& operator=(const Dictionary& rhs)
		{
			Dictionary_ref::operator=(rhs);
			return *this;
		}

		/// \skip
		Dictionary& operator=(Dictionary&& rhs) noexcept
		{
			YGDL_dict_move_assign(c_ptr(this), c_ptr(rhs));
			return *this;
		}
	};

	/** A concept that accepts any dictionary type, ignoring CV-qualifies, value category or the type of view.
     *
     * This is mostly used by functions that can be called with both mutable and immutable dictionaries,
     * like the execute function of modules. For example:
     *
     * .. code-block:: cpp
     *      :linenos:
     *
     *      static void execute(World_view world, yggdrasill::Any_dictionary auto cfg) {
     *          const auto vertices = cfg["generate"]["vertices"].get(1024, {.min = 16});
     *
     *          if(!world) return;
     *      }
     *
     * The effect is, that such a function can be used for *both* validation and execution by varying the
     * passed parameter type. During validation a mutable `Dictionary_view` is passed to the function, which is than
     * validated and modified to match the specified requirements. While during execution the parameter type
     * will be `Const_dictionary_view`, which causes an exception to be raised if the requirements are not met.
    */
	template <typename T>
	concept Any_dictionary = std::is_base_of_v<Const_dictionary_view, std::remove_cvref_t<T>>;

	/**An immutable non-owning view of a mapped value inside a `Dictionary` (or one of its bases).
     * \note Instances of this type can't be constructed directly, except by copying existing instances,
     * instead they are returned by calls to `Const_dictionary_view::operator[]`.
     */
	class Const_dictionary_value_view {
	  public:
		/// \returns The type of the mapped value or `Type::none` if the entry is either missing or hasn't been assigned a value yet.
		[[nodiscard]] Type type() const noexcept { return static_cast<Type>(YGDL_dict_value_type(value_)); }

		/// \returns ``true`` if this view represents an existing entry with a value
		[[nodiscard]] bool exists() const noexcept { return type() != Type::none; }

		/// \returns ``true`` if the mapped value can be converted to `new_type`, conserving at least some of the previous value
		[[nodiscard]] bool convertible(Type new_type) const noexcept
		{
			return is_convertible(type(), new_type);
		}

		/** Generic visitor function. The given parameter `visitor` is called with a reference to the
         * currently stored value or without parameters for `Type::none`.
         * \tparam Visitor A callable that accepts zero or one parameter of any type listed in `Type`
         * \return The result as returned by the `Visitor`
         *
         * This is usually used in conjunction with either a generic lambda or an overload-set-container like
         * `overloaded` containing multiple functions. For example:
         *
         * .. cpp:ucpp_var_ctx:: yggdrasill::Const_dictionary_value_view dict_value
         *
          * \code{cpp}
         * dict_value.visit(
         *     [&] {
         *         // for `Type::none`
         *     },
         *     [&](YGDL_Bool v) {},
         *     [&](int8_t v) {},
         *     [&](Const_array_view<void> v) {},
         *     [&](const auto&) {} // catch all for anything not handled above
         * );
         * \endcode
         */
		template <typename Visitor>
		decltype(auto) visit(Visitor&& visitor) const;
		template <typename... Visitors>
		decltype(auto) visit(Visitors&&... visitors) const requires(sizeof...(Visitors) > 1);

		/**Retrieves the value currently stored in this mapping.
         * \tparam T The expected type. This doesn't have to be the exact type stored in the map
         * (as listed in `Type`), as long as the stored value can be losslessly converted to `T`.
         * `T` may also be a reference, which is recommended for large owning types like `String`, but
         * in this case conversions are not be possible and you have to use the exact type that is stored.
         * \overload_specific
         * Always throws an exception if no value can be returned.
         * \throws Type_mismatch_exception If there is no value mapped to the key or if a conversion to `T` is not possible
         */
		template <typename T>
		const T get() const;

		/**\overload_specific
         * This function only participates in overload resolution if `T` is not an arithmetic type or a reference to one (in that case function (3) is called)
         * Returns `default_value`, instead of throwing an exception, if the value is missing or not convertible to `T`.
         */
		template <typename T>
		T get(const T& default_value) const requires(!std::is_arithmetic_v<std::remove_cvref_t<T>>);

		/**\overload_specific
         * This function only participates in overload resolution if `T` is an arithmetic type or a reference to one.
         * In addition to a `default_value`, optional `validation` constraints may be specified, which are checked before a value is returned.
         * \throws Type_mismatch_exception If `Validation::throw_on_error` is `true` and the value is
         * missing, not convertible to `T` or doesn't match the specified requirements
         */
		template <typename T>
		T get(T default_value, const Validation<T>& validation = {}) const
		        requires(std::is_arithmetic_v<std::remove_cvref_t<T>>);

		/// \returns `true` if the stored value is not `Type::none`, it's either ``operator==``-comparable or
		/// convertible to `T` and the resulting comparison evaluates to `true`.
		template <typename T>
		bool operator==(const T& rhs) const;

		/**\returns The result of ``v <=> rhs``, for the stored value ``v``, if it's not `Type::none` and
         * either ``operator<=>``-comparable or convertible to `T`.
         * Otherwise `std::partial_ordering::unordered` is returned.
         */
		template <typename T>
		std::partial_ordering operator<=>(const T& rhs) const;

		/// Interprets the stored value as a nested dictionary and returns the element with the given `key`.
		/// \throws Type_mismatch_exception If `type() != Type::dict_t`
		Const_dictionary_value_view operator[](std::string_view key) const;

		/**Prints the value stored in `v` to the given stream `s`.
         * \note If the value is `Type::none`, an array or a dictionary, only a short type-marker is
         * printed instead of any nested values
         */
		friend std::ostream& operator<<(std::ostream& s, const Const_dictionary_value_view& v)
		{
			v.print(s);
			return s;
		}

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
         * and as an escape hatch for directly interaction with the C-API
         * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
         * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
         */
		[[nodiscard]] constexpr friend const YGDL_Dict_value* c_ptr(const Const_dictionary_value_view& v)
		{
			return v.value_;
		}

		[[nodiscard]] constexpr friend const YGDL_Dict_value* c_ptr(const Const_dictionary_value_view* v)
		{
			return v ? v->value_ : nullptr;
		}

	  private:
		friend class Const_dictionary_view;

		friend class Dictionary_value_view;

		const YGDL_Dict_value* value_;

		constexpr /*implicit*/ Const_dictionary_value_view(const YGDL_Dict_value* value) : value_(value) {}

		void print(std::ostream& s) const;
	};

	/// Additional constraints to validate arithmetic dictionary values against
	template <typename T>
	struct Validation {
		std::optional<T> min = {}; ///< If not `std::nullopt`, the stored value has to be ``>=`` this value
		std::optional<T> max = {}; ///< If not `std::nullopt`, the stored value has to be ``<=`` this value
		/// If `true` always throw an exception if the validation fails or the value can't be converted to the requested type.
		/// If `false` the specified default value will be returned, instead of throwing an exception.
		bool throw_on_error = false;
		/// If `false` a value of NaN will fail validation (only available for floating-point types)
		bool nan_allowed = false;
		/// If `false` a value of +-INF will fail validation (only available for floating-point types)
		bool inf_allowed = false;
	};
	template <typename T>
	requires(!std::is_floating_point_v<T>)
	struct Validation<T> {
		std::optional<T> min            = {};
		std::optional<T> max            = {};
		bool             throw_on_error = false;
	};


	/// A mutable non-owning view of a mapped value inside a `Dictionary` (or one of its bases).
	/// \see `Const_dictionary_value_view` for details about the general structure.
	class Dictionary_value_view : public Const_dictionary_value_view {
	  public:
		using Const_dictionary_value_view::type;

		/**Changes the type of the stored value to `new_type`, possibly forcing a conversion or value initializing it,
         * if no conversion is possible.
         * \param array_element_type If `new_type` is `Type::array_t`, the type of its elements may optionally
         * also be specified, so it's recreated with the new type if necessary.
         */
		void type(Type new_type, Type array_element_type = Type::none);

		using Const_dictionary_value_view::visit;

		/// Same as `Const_dictionary_value_view::visit`, except the visitor may accept mutable references
		/// and modify the stored value through them.
		template <typename Visitor>
		decltype(auto) visit(Visitor&& visitor);
		template <typename... Visitors>
		decltype(auto) visit(Visitors&&... visitors) requires(sizeof...(Visitors) > 1);

		using Const_dictionary_value_view::get;

		/**Retrieves the value currently stored in this mapping.
         *
         * Using a different type for `T` than the currently stored one causes a conversion or insertion of  a new value.
         *
         * \tparam T The expected type. This doesn't have to be the exact type stored in the map
         * (as listed in `Type`), as long as the stored value can be losslessly converted to `T`.
         * In this case the type of the value is changed to the representable type closest to `T` and further
         * minor conversions (e.g. signed/unsigned) are applied after retrieving the value.
         * `T` may also be a reference but in this case conversions are not be possible and you have to use the exact type as listed in `Type`.
         * \overload_specific
         * If the value is missing or a conversion is not possible, a the new mapping is value initialized.
         */
		template <typename T>
		T get();

		/**\overload_specific
         * This function only participates in overload resolution if `T` is not an arithmetic type or a reference to one (in that case function (3) is called)
         * If the value is missing or a conversion is not possible, the passed `default_value` is inserted,
         * instead of value initalizing the new object.
         */
		template <typename T>
		auto& get(const T& default_value) requires(!std::is_arithmetic_v<std::remove_cvref_t<T>>);

		/**\overload_specific
         * This function only participates in overload resolution if `T` is an arithmetic type or a reference to one.
         * In addition to a `default_value`, optional `validation` constraints may be specified, which are checked before a value is returned.
         * \throws Type_mismatch_exception If `Validation::throw_on_error` is `true` and the value,
         * missing, not convertible to `T` or doesn't match the specified requirements
         */
		template <typename T>
		T get(const T& default_value, const Validation<T>& v = {})
		        requires(std::is_arithmetic_v<std::remove_cvref_t<T>>);

		using Const_dictionary_value_view::operator[];

		/// Treads the stored value as a nested dictionary, converting it to one if necessary,
		/// and returns the element with the given `key`.
		Dictionary_value_ref operator[](std::string_view key);

		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
         * and as an escape hatch for directly interaction with the C-API
         * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
         * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
         */
		[[nodiscard]] constexpr friend YGDL_Dict_value* c_ptr(Dictionary_value_view& v)
		{
			// we can safely cast away const here, because (inside the Dictionary_value_view class) we know that value_ was initialized from a pointer to non-const
			return const_cast<YGDL_Dict_value*>(c_ptr(static_cast<const Const_dictionary_value_view&>(v)));
		}

		[[nodiscard]] constexpr friend YGDL_Dict_value* c_ptr(Dictionary_value_view* v)
		{
			return const_cast<YGDL_Dict_value*>(c_ptr(static_cast<const Const_dictionary_value_view*>(v)));
		}

	  private:
		friend class Dictionary_view;

		friend class Dictionary_value_ref;

		constexpr /*implicit*/ Dictionary_value_view(YGDL_Dict_value* value)
		  : Const_dictionary_value_view(value)
		{
		}
	};

	/// A mutable non-owning reference to a mapped value inside a `Dictionary` (or one of its bases).
	/// Same as `Dictionary_value_view`, except assignments don't rebind (like pointers) but instead replace the value of the referenced object (like references).
	class Dictionary_value_ref : public Dictionary_value_view {
	  public:
		Dictionary_value_ref(Dictionary_value_ref&&)               = default; ///< Move constructor \skip
		Dictionary_value_ref(const Dictionary_value_ref&) noexcept = default; ///< Copy constructor \skip
		~Dictionary_value_ref()                                    = default; ///< \skip

		/**Assignment operator that accepts an argument of any type `T`, as long as its assignable to at
         * least one of the types listed in `Types`.
         *
         * If a `Dictionary_value_ref`, any of its base classes, is passed this value is replaced by a deep-copy of the one represented by `rhs`.
         * Otherwise the value is replaced by the closest representable type to `T` and `rhs` is forwarded to its assignment operator.
         */
		template <typename T>
		Dictionary_value_ref& operator=(T&& rhs)
		{
			return assign(std::forward<T>(rhs));
		}

		Dictionary_value_ref& operator=(Dictionary_value_ref&& rhs) noexcept
		{
			return assign(std::move(rhs));
		}

		Dictionary_value_ref& operator=(const Dictionary_value_ref& rhs) { return assign(rhs); }

	  private:
		friend class Dictionary_view;

		constexpr /*implicit*/ Dictionary_value_ref(YGDL_Dict_value* value) : Dictionary_value_view(value) {}

		template <typename T>
		Dictionary_value_ref& assign(T&& v);
	};

} // namespace yggdrasill

#define YGDL_DICTIONARY_HPP_INCLUDED

#include <yggdrasill/dictionary.inl>
