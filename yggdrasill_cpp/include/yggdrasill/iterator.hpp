/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/utils.hpp>

#include <array>
#include <concepts>
#include <functional>
#include <iostream>
#include <iterator>
#include <memory>
#include <optional>
#include <type_traits>

namespace yggdrasill {

	namespace detail {
		inline const int32_t sizeof_dict_t = YGDL_dict_sizeof();

		template <typename T>
		struct Wrapper_base_type {
			using type = void;
		};
		template <typename T>
		concept has_c_ptr = requires(T v) { c_ptr(v); };
		template <has_c_ptr T>
		struct Wrapper_base_type<T> {
			using type = decltype(c_ptr(std::declval<T&>()));
		};
	} // namespace detail

	/**An iterator over a C-style array, that wraps each element in the passed C++ wrapper class.
     * \note While this type supports all arithmetic operators that a random access iterator provides, since
     *       `reference` is a pr-value of type Wrapper, instead of a reference, it technically only qualifies
     *       as a forward iterator.
     * \tparam Wrapper The type of the wrapper class. Has to be copyable, constructable from a pointer to the
     * C-API type and the ADL function c_ptr(cpp_type) has to return a pointer to the corresponding C-API type
     */
	template <typename Wrapper, typename BaseIterator = typename detail::Wrapper_base_type<Wrapper>::type>
	class Wrapping_iterator {
	  public:
		using value_type        = Wrapper;
		using difference_type   = std::ptrdiff_t;
		using reference         = Wrapper;
		using iterator_category = std::forward_iterator_tag;

		constexpr explicit Wrapping_iterator(BaseIterator iterator = nullptr) : base_iterator_(iterator) {}

		/// \skip
		constexpr reference operator*()
		{
			if constexpr(std::is_constructible_v<Wrapper, BaseIterator>)
				return Wrapper(base_iterator_);
			else
				return Wrapper(*base_iterator_);
		}

		/// \skip
		constexpr Wrapper* operator->()
		{
			last_value_.clear();
			last_value_.emplace(**this);
			return &*last_value_;
		}

		/// \skip
		reference operator[](difference_type off) const { return Wrapper(base_iterator_ + off); }

		/// \skip
		constexpr Wrapping_iterator& operator++()
		{
			base_iterator_ = add(base_iterator_, 1);
			return *this;
		}

		/// \skip
		constexpr Wrapping_iterator operator++(int)
		{
			auto r         = *this;
			base_iterator_ = add(base_iterator_, 1);
			return r;
		}

		/// \skip
		constexpr Wrapping_iterator& operator--()
		{
			base_iterator_ = add(base_iterator_, -1);
			return *this;
		}

		/// \skip
		constexpr Wrapping_iterator operator--(int)
		{
			auto r         = *this;
			base_iterator_ = add(base_iterator_, -1);
			return r;
		}

		/// \skip
		Wrapping_iterator& operator+=(difference_type off)
		{
			base_iterator_ = add(base_iterator_, off);
			return *this;
		}

		/// \skip
		Wrapping_iterator operator+(difference_type off) const
		{
			return Wrapping_iterator(add(base_iterator_, off));
		}

		/// \skip
		friend Wrapping_iterator operator+(difference_type off, const Wrapping_iterator& right)
		{
			return vector_const_iterator(add(right.base_iterator_, off));
		}

		/// \skip
		Wrapping_iterator& operator-=(difference_type off)
		{
			base_iterator_ = add(base_iterator_, -off);
			return *this;
		}

		/// \skip
		Wrapping_iterator operator-(difference_type off) const
		{
			return vector_const_iterator(add(base_iterator_, -off));
		}

		/// \skip
		difference_type operator-(const Wrapping_iterator& right) const
		{
			if constexpr(std::is_same_v<YGDL_Dict, std::remove_cvref_t<BaseIterator>>)
				return (reinterpret_cast<const char*>(base_iterator_)
				        - reinterpret_cast<const char*>(right.base_iterator_))
				       / detail::sizeof_dict_t;
			else
				return base_iterator_ - right.base_iterator_;
		}

		/// \skip
		constexpr bool operator==(const Wrapping_iterator& rhs) const
		{
			return base_iterator_ == rhs.base_iterator_;
		}

		/// \skip
		constexpr auto operator<=>(const Wrapping_iterator& rhs) const
		{
			return base_iterator_ <=> rhs.base_iterator_;
		}

	  private:
		BaseIterator           base_iterator_;
		std::optional<Wrapper> last_value_;

		static BaseIterator add(BaseIterator ptr, difference_type off)
		{
			if constexpr(std::is_same_v<YGDL_Dict, std::remove_cvref_t<std::remove_pointer_t<BaseIterator>>>)
				// YGDL_Dict is incomplete to enabled more compact storage without exposing C++ implementation details in the C-API
				return reinterpret_cast<BaseIterator>(
				        reinterpret_cast<detail::copy_constness<std::remove_pointer_t<BaseIterator>, char>*>(ptr)
				        + off * detail::sizeof_dict_t);
			else
				return ptr + off;
		}
	};


	/**Wrapper over a pair of iterators that define a range of elements, that also provides a number of additional utility functions.\n
     * \tparam Iterator The type used for iterators into the range, as well as to mark the beginning of it
     * \tparam EndSentinel The type used for the end iterator, which can either be the same as `Iterator`
     * or a sentinel type that just marks the end of the range.
     * \note While this wrapper generally requires at least bidirectional iterators to work, this restriction
     * is not checked explicitly using concepts, to still allow near-bidirectional iterators like
     * `Wrapping_iterator` that can only return pr-values instead of true references.
     */
	template <typename Iterator, std::sentinel_for<Iterator> EndSentinel = Iterator>
	class Iterator_range {
	  public:
		/// The type of elements stored in this range
		using value_type = typename Iterator::value_type;
		using reference  = typename Iterator::reference;

		constexpr explicit Iterator_range(Iterator begin = {}, EndSentinel end = {})
		  : begin_(begin), end_(end)
		{
		}

		/// \returns The first element in the range or `std::nullopt` if the range is empty
		constexpr std::optional<value_type> first() const
		{
			const auto iter = begin();
			return iter != end() ? std::optional(*iter) : std::nullopt;
		}

		/// \returns Begin iterator of this range
		constexpr Iterator begin() const { return begin_; }

		/// \returns End sentinel of this range
		constexpr EndSentinel end() const { return end_; }

		/// \returns An end iterator for this range constructed from the sentinel.
		///	\note Sometimes required to call functions that don't support sentinels, yet.
		constexpr Iterator end_iterator() const { return Iterator(end_); }

		/**\returns The `index`-th element in the range.
         * \attention The behaviour is undefined if the range is empty or has less than ``index+1`` elements.
         * \attention This operation might be slow, depending on the underlying iterator type.
         */
		reference operator[](index_t index)
		{
			auto iter = begin();
			if constexpr(requires { begin() += index; }) {
				iter += index;
			} else {
				std::advance(iter, index);
			}

			return *iter;
		}

		/// \returns A new range over the same elements but skipping the first `offset` many elements.
		/// \attention This operation might be slow, depending on the underlying iterator type.
		constexpr Iterator_range skip(int offset) const
		{
			auto r = *this;
			while(offset > 0 && r.begin_ != r.end_) {
				--offset;
				++r.begin_;
			}
			return r;
		}

		/// \returns An iterator to the smallest element in the range, where each element is compared using ``map(lhs) < map(rhs)``
		template <typename Mapping>
		constexpr Iterator min(Mapping&& map) const
		{
			auto min_iter = begin();
			if(min_iter == end())
				return min_iter;

			auto min_value = map(*min_iter);

			for(auto iter = min_iter; iter != end(); ++iter) {
				if(auto v = map(*iter); v < min_value) {
					min_iter  = iter;
					min_value = v;
				}
			}

			return min_iter;
		}

		/// \returns An iterator to the largest element in the range, where each element is compared using ``map(lhs) < map(rhs)``
		template <typename Mapping>
		constexpr Iterator max(Mapping&& map) const
		{
			return min([&](auto v) { return -map(v); });
		}

		/// \returns The first element in the range that matches the given predicate `p` or `std::nullopt`
		template <typename Predicate>
		constexpr std::optional<value_type> find_if(Predicate&& p)
		{
			for(auto&& v : *this)
				if(p(v))
					return v;

			return std::nullopt;
		}

		/// \returns `true` if the two ranges contain the same number of elements and each element compares
		/// equal to the corresponding element in the other range.
		template <typename Range>
		bool operator==(const Range& rhs) const
		{
			return std::equal(begin(), end_iterator(), rhs.begin(), static_cast<decltype(rhs.begin())>(rhs.end()));
		}

		/// Writes each element in `range` to the given output stream
		friend std::ostream& operator<<(std::ostream& os, const Iterator_range& range)
		{
			os << '[';
			std::copy(range.begin(), range.end_iterator(), std::ostream_iterator<value_type>(os, ", "));
			os << ']';
			return os;
		}

		/// \returns The number of elements in this range
		/// \attention This operation might be slow, depending on the underlying iterator type.
		[[nodiscard]] constexpr index_t size() const { return static_cast<index_t>(end() - begin()); }

	  private:
		Iterator    begin_;
		EndSentinel end_;
	};

	/// A range of bidirectional iterators over elements of type T.
	/// \see `Iterator_range` for a list of available operations on these kind of ranges.
	template <typename R, typename T>
	concept Range_of =
	        detail::is_specialization_of_v<R, Iterator_range> && std::is_same_v<T, typename R::value_type>;

	/**
     * \returns A `Range_of` of bidirectional iterators, generated on-demand by the mutable callable `f`
     *
     * \param f Callable that accepts a bool, indicating the iteration direction (true=forward, false=backward),
     * and returns an optional, where `std::nullopt` indicates that the end of the range has been reached.
     *
     * \note The passed lambda and all its captured state have to remain valid for the complete lifetime of the returned range.
     *
     * \note While the iterators used in the returned range support all arithmetic operators that a
     * bidirectional iterator provides, their reference type is a pr-value instead of a reference because
     * they generate new elements on-demand, which means they technically only qualify as a forward iterators.
     *
     * Example usage:
     * \code{cpp}
     * auto fibonacci() {
     * 	return make_range([a=0, b=1](bool forward)mutable -> std::optional<int> {
     * 		if(forward) {
     * 			return std::exchange(a, std::exchange(b, b+a));
     * 		} else {
     * 			b = std::exchange(a, b-a);
     * 			return a;
     * 		}
     * 	});
     * }
     * \endcode
     */
	template <typename F>
	constexpr auto make_range(F&& f);

	/**
     * \returns A `Range_of` of bidirectional iterators, generated on demand by applying the given mapping function
     * `map` on each element of the passed range, defined either by (1) a begin and end iterator or by (2) a
     * range object, which has at least the two callable members `begin()` and `end()`.
     *
     * \note The passed range, the lambda and all its captured state have to remain valid for the complete lifetime of the returned range.
     *
     * \note While the iterators used in the returned range support all arithmetic operators that a
     * bidirectional iterator provides, their reference type is a pr-value instead of a reference because
     * they generate new elements ondemand, which means they technically only qualify as a forward iterators.
     *
     * .. cpp:ucpp_var_ctx:: yggdrasill::Const_mesh_view mesh
     *
     * Example usage:
     * \code{cpp}
     * auto data_range = map_range(mesh.vertices(), [&](Vertex v) mutable {
     *     return some_layer[v];
     * });
     * \endcode
     */
	template <typename BeginIter, typename EndIter, typename Map>
	constexpr auto map_range(BeginIter begin, EndIter end, Map&& map);

	template <typename Range, typename Map>
	constexpr auto map_range(Range&& base_range, Map&& map);

	/**Simple non-owning range over a fixed number of elements.\n
     * Used as a utility construct to iterate over a small set of values, without incurring additional costs
     * from copies or heap allocations and without unnecessary syntax, like referencing pointers or
     * specifying the explicit type and/or size.\n
     * Example usage:
     * \code{cpp}
     * for(auto& e : Ref_range{large_obj_a, large_obj_b, large_obj_c}) {
     *     // ...
     * }
     * \endcode
     */
	template <typename T, std::size_t N>
	class Ref_range {
	  public:
		struct iterator {
			using iterator_category = std::forward_iterator_tag;
			using value_type        = T&;
			using difference_type   = std::ptrdiff_t;
			using reference         = value_type;
			using pointer           = T*;

			/// \skip
			typename std::array<T*, N>::iterator wrapped;

			/// \skip
			constexpr reference operator*() { return **wrapped; }

			/// \skip
			constexpr pointer operator->() const { return *wrapped; }

			/// \skip
			constexpr auto& operator++()
			{
				++wrapped;
				return *this;
			}

			/// \skip
			constexpr auto operator++(int)
			{
				auto r = *this;
				++*this;
				return r;
			}

			/// \skip
			constexpr bool operator==(const iterator& rhs) const { return wrapped == rhs.wrapped; }
		};

		/// \skip
		template <typename... Ts>
		constexpr explicit Ref_range(T& first, Ts&... rest)
		  : elements_{std::addressof(first), std::addressof(rest)...}
		{
		}

		constexpr iterator begin() { return {elements_.begin()}; }

		constexpr iterator end() { return {elements_.end()}; }

	  private:
		std::array<T*, N> elements_;
	};
	/// \skip (deduction guide)
	template <class First, typename... Rest>
	Ref_range(First& first, Rest&... rest) -> Ref_range<
	        std::remove_pointer_t<std::common_type_t<decltype(std::addressof(first)), decltype(std::addressof(rest))...>>,
	        1 + sizeof...(Rest)>;

} // namespace yggdrasill

#define YGDL_ITERATOR_HPP_INCLUDED

#include <yggdrasill/iterator.inl>
