/// NOTE: This is the template implementation header for world.hpp and should not be included directly!
#pragma once

#ifndef YGDL_WORLD_HPP_INCLUDED
#include <yggdrasill/world.hpp>
#endif

namespace yggdrasill {

	namespace detail {
		template <typename T, typename F>
		decltype(auto) convert_to_layer(const YGDL_Layer& layer, F& visit)
		{
			switch(layer.definition->ref_type) {
				case YGDL_LAYER_REF_TYPE_VERTEX: return visit(Const_layer_view<T, Ref_type::vertex>(layer));
				case YGDL_LAYER_REF_TYPE_FACE: return visit(Const_layer_view<T, Ref_type::face>(layer));
				case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL:
					return visit(Const_layer_view<T, Ref_type::edge_primal>(layer));
				case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL_DIRECTED:
					return visit(Const_layer_view<T, Ref_type::edge_primal_directed>(layer));
				case YGDL_LAYER_REF_TYPE_EDGE_DUAL:
					return visit(Const_layer_view<T, Ref_type::edge_dual>(layer));
				case YGDL_LAYER_REF_TYPE_EDGE_DUAL_DIRECTED:
					return visit(Const_layer_view<T, Ref_type::edge_dual_directed>(layer));
			}
			std::abort();
		}
	} // namespace detail


	// IMPLEMENTATION: Const_world_view
	inline void Const_world_view::validate() const
	{
		assert(world_);
		YGDL_world_valid(world_, Error_throw{});
	}
	inline Data_version Const_world_view::mesh_version() const
	{
		assert(world_);
		return Data_version(YGDL_world_mesh_version(world_));
	}
	inline bool Const_world_view::mesh_modified(Data_version& v) const
	{
		if(auto nv = mesh_version(); nv != v) {
			v = nv;
			return true;
		} else {
			return false;
		}
	}

	inline Const_mesh_view Const_world_view::mesh() const
	{
		assert(world_);
		auto ptr = YGDL_world_mesh(world_);
		assert(ptr);
		return *ptr;
	}

	template <typename Visitor>
	inline void Const_world_view::foreach_layer(Visitor&& visit) const
	{
		auto size = YGDL_Index(0);
		YGDL_world_layer_list(world_, &size, nullptr);
		if(size == 0)
			return;

		auto layers = std::vector<const YGDL_Layer*>(size);
		if(!YGDL_world_layer_list(world_, &size, layers.data()))
			return;

		if(layers.size() > std::size_t(size))
			layers.resize(size);

		for(auto layer : layers) {
			using namespace ::yggdrasill::detail;
			// determine the data and ref type, construct the appropriate type and call the visitor
			switch(layer->definition->data_type) {
				case YGDL_TYPE_STRING:
				case YGDL_TYPE_ARRAY:
				case YGDL_TYPE_DICT:
				case YGDL_TYPE_NONE: break;

				case YGDL_TYPE_BOOL: convert_to_layer<YGDL_Bool>(*layer, visit); break;
				case YGDL_TYPE_INT8: convert_to_layer<std::int8_t>(*layer, visit); break;
				case YGDL_TYPE_INT32: convert_to_layer<std::int32_t>(*layer, visit); break;
				case YGDL_TYPE_FLOAT: convert_to_layer<float>(*layer, visit); break;
				case YGDL_TYPE_VEC2: convert_to_layer<Vec2>(*layer, visit); break;
				case YGDL_TYPE_VEC3: convert_to_layer<Vec3>(*layer, visit); break;
			}
		}
	}

	template <Any_layer_definition LayerInfo>
	inline Data_version Const_world_view::layer_version(const LayerInfo& info) const
	{
		assert(world_);
		return Data_version(YGDL_world_layer_version(world_, detail::to_ygdl_string_view(info.id())));
	}
	template <Any_layer_definition LayerInfo>
	inline bool Const_world_view::layer_modified(const LayerInfo& info, Data_version& v) const
	{
		if(auto nv = layer_version(info); nv != v) {
			v = nv;
			return true;
		} else {
			return false;
		}
	}
	template <Any_layer_definition LayerInfo>
	inline std::optional<const_view_of<LayerInfo>> Const_world_view::layer(const LayerInfo& info) const
	{
		assert(world_);
		auto layer = YGDL_world_layer(&detail::c_ref(this), c_ptr(info));
		if(layer)
			return const_view_of<LayerInfo>{*layer};
		else
			return std::nullopt;
	}
	template <Any_layer_definition... LayerInfo>
	inline std::tuple<std::optional<const_view_of<LayerInfo>>...> Const_world_view::layer(
	        const LayerInfo&... infos) const
	{
		return std::tuple{layer(infos)...};
	}
	template <Any_layer_definition LayerInfo>
	inline const_view_of<LayerInfo> Const_world_view::required_layer(const LayerInfo& info) const
	{
		auto l = layer(info);
		if(!l)
			throw Precondition_violation_exception(detail::concat("missing required layer ", info.id()));

		return *l;
	}
	template <Any_layer_definition... LayerInfo>
	inline std::tuple<const_view_of<LayerInfo>...> Const_world_view::required_layer(const LayerInfo&... infos) const
	{
		return std::tuple{required_layer(infos)...};
	}

	inline Data_version Const_world_view::unstructured_layer_version(std::string_view id) const
	{
		assert(world_);
		return Data_version(YGDL_world_layer_version(world_, detail::to_ygdl_string_view(id)));
	}
	inline bool Const_world_view::unstructured_layer_modified(std::string_view id, Data_version& v) const
	{
		if(auto nv = unstructured_layer_version(id); nv != v) {
			v = nv;
			return true;
		} else {
			return false;
		}
	}

	inline std::optional<Const_dictionary_view> Const_world_view::unstructured_layer(std::string_view id) const
	{
		assert(world_);
		auto layer = YGDL_world_unstructured_layer(&detail::c_ref(this), detail::to_ygdl_string_view(id));
		if(layer)
			return {*layer};
		else
			return std::nullopt;
	}

	inline Const_dictionary_view Const_world_view::required_unstructured_layer(std::string_view id) const
	{
		auto l = unstructured_layer(id);
		if(!l)
			throw Precondition_violation_exception(detail::concat("missing required layer", id));

		return *l;
	}

	template <std::invocable<std::string_view> Visitor>
	void Const_world_view::foreach_unstructured_layer(Visitor&& visit) const
	{
		auto size = YGDL_Index(0);
		YGDL_world_unstructured_layer_list(world_, &size, nullptr);
		if(size == 0)
			return;

		auto layer_ids = std::vector<YGDL_String_view>(size);
		if(!YGDL_world_unstructured_layer_list(world_, &size, layer_ids.data()))
			return;

		if(layer_ids.size() > std::size_t(size))
			layer_ids.resize(size);

		for(auto layer_id : layer_ids) {
			visit(detail::from_ygdl_string_view(layer_id));
		}
	}
	inline std::vector<std::string_view> Const_world_view::unstructured_layers() const
	{
		auto size = YGDL_Index(0);
		YGDL_world_unstructured_layer_list(world_, &size, nullptr);

		auto layer_ids = std::vector<std::string_view>();
		layer_ids.reserve(size);
		foreach_unstructured_layer([&](auto id) { layer_ids.push_back(id); });
		return layer_ids;
	}


	// IMPLEMENTATION: World_view
	inline Random_view World_view::random()
	{
		return {*YGDL_world_random(&detail::c_ref(this))};
	}

	inline Mesh_lock World_view::lock_mesh()
	{
		auto& c_ref = detail::c_ref(this);
		return {c_ref, *YGDL_world_mesh_acquire(&c_ref, Error_throw())};
	}

	template <Any_layer_definition... LayerInfo>
	inline Layer_lock<view_of<LayerInfo>...> World_view::lock_layer(const LayerInfo&... info)
	{
		// Constructed here and destroyed on return, which then in-turn destroys the Layer_lock (with some nullptrs),
		//   which release the non-null layers.
		auto error = Error_throw();
		return Layer_lock<view_of<LayerInfo>...>{
		        detail::c_ref(this), (view_of<LayerInfo>{*acquire_layer(detail::c_ref(info), error)})...};
	}

	template <Any_layer_definition LayerInfo>
	inline void World_view::remove_layer(const LayerInfo& info)
	{
		YGDL_world_layer_remove(&detail::c_ref(this), detail::to_ygdl_string_view(info.id()));
	}

	inline void World_view::invalidate_layers(const Layer_mesh_changes& req)
	{
		auto invalidation_from_span = [](auto& s) {
			return YGDL_Layer_element_invalidations{s.data(), static_cast<index_t>(s.size())};
		};
		auto interpolation_from_span = [](auto& s) {
			return YGDL_Layer_element_interpolations{s.data(), static_cast<index_t>(s.size())};
		};

		const auto c_req = YGDL_Layer_mesh_changes{
		        .invalidated_vertices      = invalidation_from_span(req.invalidated_vertices),
		        .invalidated_faces         = invalidation_from_span(req.invalidated_faces),
		        .invalidated_primal_edges  = invalidation_from_span(req.invalidated_primal_edges),
		        .invalidated_dual_edges    = invalidation_from_span(req.invalidated_dual_edges),
		        .interpolated_vertices     = interpolation_from_span(req.interpolated_vertices),
		        .interpolated_faces        = interpolation_from_span(req.interpolated_faces),
		        .interpolated_primal_edges = interpolation_from_span(req.interpolated_primal_edges),
		        .interpolated_dual_edges   = interpolation_from_span(req.interpolated_dual_edges),
		        .interpolation_weight      = req.interpolation_weight};

		YGDL_world_layer_invalidate(&detail::c_ref(this), &c_req, Error_throw());
	}

	inline YGDL_Layer* World_view::acquire_layer(const YGDL_Layer_definition& layer_def, YGDL_Error* out_error)
	{
		return YGDL_world_layer_acquire(&detail::c_ref(this), &layer_def, out_error);
	}

	inline Unstructured_layer_lock World_view::lock_unstructured_layer(std::string_view id)
	{
		auto& c_ref = detail::c_ref(this);
		return {c_ref,
		        *YGDL_world_unstructured_layer_acquire(&c_ref, detail::to_ygdl_string_view(id), Error_throw())};
	}

	inline void World_view::remove_unstructured_layer(std::string_view id)
	{
		YGDL_world_unstructured_layer_remove(&detail::c_ref(this), detail::to_ygdl_string_view(id));
	}


	// IMPLEMENTATION: World_view
	inline World::World() : World_view(YGDL_world_create()) {}
	inline World::World(const Seed& seed) : World()
	{
		random().seed(seed);
	}
	inline World::World(World&& rhs) noexcept
	  : World_view(YGDL_world_move(const_cast<YGDL_World**>(&rhs.world_)))
	{
	}
	inline World::World(const World& rhs) : World_view(YGDL_world_copy(rhs.world_)) {}
	inline World::World(const Const_world_view& rhs) : World_view(YGDL_world_copy(c_ptr(rhs))) {}
	inline World::~World()
	{
		if(auto self = c_ptr(this))
			YGDL_world_destroy(self);
	}

	inline World& World::operator=(World&& rhs) noexcept
	{
		if(world_ == nullptr) {
			// empty (e.g. because we got moved-from previously) => directly steal the internal state
			world_ = std::exchange(rhs.world_, nullptr);
		} else {
			// non-empty (i.e this already contains state)
			YGDL_world_move_assign(c_ptr(this), const_cast<YGDL_World**>(&rhs.world_));
		}
		return *this;
	}
	inline World& World::operator=(const World& rhs)
	{
		if(world_ == nullptr) {
			// empty (e.g. because we got moved-from previously) => directly steal the internal state
			world_ = YGDL_world_copy(rhs.world_);
		} else {
			YGDL_world_copy_assign(c_ptr(this), rhs.world_);
		}
		return *this;
	}

} // namespace yggdrasill
