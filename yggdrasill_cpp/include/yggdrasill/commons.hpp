/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/commons.h>
#include <yggdrasill/dictionary.h>

#include <array>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <type_traits>
#include <variant>

namespace yggdrasill {

	/**Enum used to indicate the underlying type if there are multiple alternatives at runtime.
	 * Used for example in dictionaries, arrays and layers.
	 * Each value corresponds to the concrete C++ type listed below
	 */
	enum class Type : YGDL_Type_t {
		// clang-format off
		none     = YGDL_TYPE_NONE,   ///< `void`, used for unknown, unassigned or missing types
		bool_t   = YGDL_TYPE_BOOL,   ///< `YGDL_Bool`, with values `YGDL_TRUE` and `YGDL_FALSE`
		int8_t   = YGDL_TYPE_INT8,   ///< `std::int8_t`
		int32_t  = YGDL_TYPE_INT32,  ///< `std::int32_t`
		int64_t  = YGDL_TYPE_INT64,  ///< `std::int64_t`
		float_t  = YGDL_TYPE_FLOAT,  ///< `float`
		double_t = YGDL_TYPE_DOUBLE, ///< `double`
		vec2_t   = YGDL_TYPE_VEC2,   ///< `Vec2`
		vec3_t   = YGDL_TYPE_VEC3,   ///< `Vec3`
		string_t = YGDL_TYPE_STRING, ///< `String`
		array_t  = YGDL_TYPE_ARRAY,  ///< `Array_ref<T>`, `Array_view<T>` and `Const_array_view<T>` (with any type listed in `Type` as T)
		dict_t   = YGDL_TYPE_DICT    ///< `Dictionary_ref`, `Dictionary_view` or `Const_dictionary_view`
		// clang-format on
	};
	namespace detail {
		constexpr auto type_names = std::array<std::string_view, 12>{"none",
		                                                             "bool",
		                                                             "int8",
		                                                             "int32",
		                                                             "int64",
		                                                             "float",
		                                                             "double",
		                                                             "vec2",
		                                                             "vec3",
		                                                             "string",
		                                                             "array",
		                                                             "dict"};
	}

	/// \returns a human-readable name for the given `Type`
	constexpr auto type_name(Type t) -> std::string_view
	{
		return detail::type_names[static_cast<std::underlying_type_t<Type>>(t)];
	}
	inline std::ostream& operator<<(std::ostream& os, Type rhs)
	{
		return os << type_name(rhs);
	}

	/// \returns A const-view of the same view wrapped by `view`.
	template <typename T>
	constexpr typename T::const_view const_view(const T& view)
	{
		return view;
	}

	/// Type used for all indices and sizes
	using index_t = YGDL_Index;

	/// Type used to indicate version numbers, both for plugins after hot-reloading and to mark different revisions of copy-on-write data.\n
	/// Values of
	class Data_version {
	  public:
		constexpr Data_version() : version_{0} {}
		constexpr explicit Data_version(YGDL_Data_version v) : version_(v) {}

		friend constexpr bool operator==(const Data_version lhs, const Data_version rhs)
		{
			return lhs.version_ == rhs.version_;
		}

	  private:
		YGDL_Data_version version_;
	};

	/// A two-dimensional vector type with components `x` and `y`.\n
	/// Binary compatible to `YGDL_Vec2` to allow direct conversions.
	struct Vec2 : public YGDL_Vec2 {
		constexpr Vec2() noexcept : YGDL_Vec2{0.f, 0.f} {}
		constexpr Vec2(YGDL_Vec2 v) noexcept : YGDL_Vec2{v} {}
		constexpr Vec2(float x, float y) noexcept : YGDL_Vec2{x, y} {}

		friend constexpr Vec2 operator-(const Vec2& lhs) { return Vec2(-lhs.x, -lhs.y); }
		friend constexpr Vec2 operator+(const Vec2& lhs, const Vec2& rhs)
		{
			return Vec2(lhs.x + rhs.x, lhs.y + rhs.y);
		}
		friend constexpr Vec2 operator-(const Vec2& lhs, const Vec2& rhs)
		{
			return Vec2(lhs.x - rhs.x, lhs.y - rhs.y);
		}
		friend constexpr Vec2 operator*(const Vec2& lhs, const Vec2& rhs)
		{
			return Vec2(lhs.x * rhs.x, lhs.y * rhs.y);
		}
		friend constexpr Vec2 operator*(const float& lhs, const Vec2& rhs)
		{
			return Vec2(lhs * rhs.x, lhs * rhs.y);
		}
		friend constexpr Vec2 operator*(const Vec2& lhs, const float& rhs)
		{
			return Vec2(lhs.x * rhs, lhs.y * rhs);
		}
		friend constexpr Vec2 operator/(const Vec2& lhs, const Vec2& rhs)
		{
			return Vec2(lhs.x / rhs.x, lhs.y / rhs.y);
		}
		friend constexpr Vec2 operator/(const float& lhs, const Vec2& rhs)
		{
			return Vec2(lhs / rhs.x, lhs / rhs.y);
		}
		friend constexpr Vec2 operator/(const Vec2& lhs, const float& rhs)
		{
			return Vec2(lhs.x / rhs, lhs.y / rhs);
		}
		friend Vec2 operator%(const Vec2& lhs, const float& rhs)
		{
			return Vec2(std::fmod(lhs.x, rhs), std::fmod(lhs.y, rhs));
		}

		friend constexpr Vec2& operator+=(Vec2& lhs, const Vec2& rhs)
		{
			lhs.x += rhs.x;
			lhs.y += rhs.y;
			return lhs;
		}
		friend constexpr Vec2& operator+=(Vec2& lhs, const float& rhs)
		{
			lhs.x += rhs;
			lhs.y += rhs;
			return lhs;
		}
		friend constexpr Vec2& operator-=(Vec2& lhs, const Vec2& rhs)
		{
			lhs.x -= rhs.x;
			lhs.y -= rhs.y;
			return lhs;
		}
		friend constexpr Vec2& operator-=(Vec2& lhs, const float& rhs)
		{
			lhs.x -= rhs;
			lhs.y -= rhs;
			return lhs;
		}
		friend constexpr Vec2& operator*=(Vec2& lhs, const Vec2& rhs)
		{
			lhs.x *= rhs.x;
			lhs.y *= rhs.y;
			return lhs;
		}
		friend constexpr Vec2& operator*=(Vec2& lhs, const float& rhs)
		{
			lhs.x *= rhs;
			lhs.y *= rhs;
			return lhs;
		}
		friend constexpr Vec2& operator/=(Vec2& lhs, const Vec2& rhs)
		{
			lhs.x /= rhs.x;
			lhs.y /= rhs.y;
			return lhs;
		}
		friend constexpr Vec2& operator/=(Vec2& lhs, const float& rhs)
		{
			lhs.x /= rhs;
			lhs.y /= rhs;
			return lhs;
		}
		friend Vec2& operator%=(Vec2& lhs, const float& rhs)
		{
			lhs.x = std::fmod(lhs.x, rhs);
			lhs.y = std::fmod(lhs.y, rhs);
			return lhs;
		}

		friend constexpr bool operator==(const Vec2& lhs, const Vec2& rhs)
		{
			return length2(lhs - rhs) < 0.0000000001f;
		}
		friend Vec2 abs(const Vec2& lhs) { return Vec2(std::abs(lhs.x), std::abs(lhs.y)); }
		friend Vec2 sqrt(const Vec2& lhs) { return Vec2(std::sqrt(lhs.x), std::sqrt(lhs.y)); }
		friend Vec2 pow(const Vec2& lhs, float exp)
		{
			return Vec2(std::pow(lhs.x, exp), std::pow(lhs.y, exp));
		}
		friend constexpr float dot(const Vec2& lhs, const Vec2& rhs) { return lhs.x * rhs.x + lhs.y * rhs.y; }
		friend constexpr float length2(const Vec2& lhs) { return dot(lhs, lhs); }
		friend float           length(const Vec2& lhs) { return std::sqrt(length2(lhs)); }
		/// \returns The square of Euclidean distance between `lhs` and `rhs`
		friend constexpr float dist2(const Vec2& lhs, const Vec2& rhs) { return length2(lhs - rhs); }
		/// \returns The Euclidean distance between `lhs` and `rhs`
		friend float dist(const Vec2& lhs, const Vec2& rhs) { return std::sqrt(dist2(lhs, rhs)); }
		friend Vec2  normalized(const Vec2& lhs) { return lhs / length(lhs); }
		/// \returns Either the unchanged `lhs` if its length is shorter or equal to `mas_length` or
		/// a normalized vector with a length of `max_length`.
		friend Vec2 limit_length(const Vec2& lhs, float max_length)
		{
			if(auto len2 = length2(lhs); len2 > max_length * max_length)
				return lhs / std::sqrt(len2) * max_length;
			else
				return lhs;
		}
		/// \returns The linear interpolation between 'a' and 'b'.
		/// The parameter 't' is clamped to be inside [0, 1].
		friend Vec2 lerp(const Vec2& a, const Vec2& b, float t)
		{
			if(t <= 0.f)
				return a;
			else if(t >= 1.f)
				return b;

			return {std::lerp(a.x, b.x, t), std::lerp(a.y, b.y, t)};
		}
		/// \returns The circular linear interpolation between 'a' and 'b'.
		/// The parameter 't' is clamped to be inside [0, 1].
		friend Vec2 slerp(const Vec2& a, const Vec2& b, float t)
		{
			if(t <= 0.f)
				return a;
			else if(t >= 1.f)
				return b;

			// technically NLerp, but should be good enough for our use-case
			const auto a_len = length(a);
			const auto b_len = length(b);
			return normalized(lerp(a / a_len, b / b_len, t)) * std::lerp(a_len, b_len, t);
		}

		friend std::ostream& operator<<(std::ostream& o, const Vec2& v)
		{
			o << "Vec2(" << v.x << ", " << v.y << ")";
			return o;
		}
	};
	static_assert(std::is_standard_layout_v<Vec2>);
	static_assert(std::is_standard_layout_v<YGDL_Vec2>);
	static_assert(sizeof(Vec2) == sizeof(YGDL_Vec2));
	static_assert(alignof(Vec2) == alignof(YGDL_Vec2));


	/// A three-dimensional vector type with components `x`, `y` and `z`.\n
	/// Binary compatible to `YGDL_Vec3` to allow direct conversions.
	struct Vec3 : public YGDL_Vec3 {
		constexpr Vec3() noexcept : YGDL_Vec3{0.f, 0.f, 0.f} {}
		constexpr Vec3(YGDL_Vec3 v) noexcept : YGDL_Vec3{v} {}
		constexpr Vec3(float x, float y, float z) noexcept : YGDL_Vec3{x, y, z} {}
		constexpr Vec3(Vec2 v, float z) noexcept : YGDL_Vec3{v.x, v.y, z} {}

		friend constexpr Vec3 operator-(const Vec3& lhs) { return Vec3(-lhs.x, -lhs.y, -lhs.z); }
		friend constexpr Vec3 operator+(const Vec3& lhs, const Vec3& rhs)
		{
			return Vec3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
		}
		friend constexpr Vec3 operator-(const Vec3& lhs, const Vec3& rhs)
		{
			return Vec3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
		}
		friend constexpr Vec3 operator*(const Vec3& lhs, const Vec3& rhs)
		{
			return Vec3(lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z);
		}
		friend constexpr Vec3 operator*(const float& lhs, const Vec3& rhs)
		{
			return Vec3(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z);
		}
		friend constexpr Vec3 operator*(const Vec3& lhs, const float& rhs)
		{
			return Vec3(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs);
		}
		friend constexpr Vec3 operator/(const Vec3& lhs, const Vec3& rhs)
		{
			return Vec3(lhs.x / rhs.x, lhs.y / rhs.y, lhs.z / rhs.z);
		}
		friend constexpr Vec3 operator/(const float& lhs, const Vec3& rhs)
		{
			return Vec3(lhs / rhs.x, lhs / rhs.y, lhs / rhs.z);
		}
		friend constexpr Vec3 operator/(const Vec3& lhs, const float& rhs)
		{
			return Vec3(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs);
		}
		friend Vec3 operator%(const Vec3& lhs, const float& rhs)
		{
			return Vec3(std::fmod(lhs.x, rhs), std::fmod(lhs.y, rhs), std::fmod(lhs.z, rhs));
		}

		friend constexpr Vec3& operator+=(Vec3& lhs, const Vec3& rhs)
		{
			lhs.x += rhs.x;
			lhs.y += rhs.y;
			lhs.z += rhs.z;
			return lhs;
		}
		friend constexpr Vec3& operator+=(Vec3& lhs, const float& rhs)
		{
			lhs.x += rhs;
			lhs.y += rhs;
			lhs.z += rhs;
			return lhs;
		}
		friend constexpr Vec3& operator-=(Vec3& lhs, const Vec3& rhs)
		{
			lhs.x -= rhs.x;
			lhs.y -= rhs.y;
			lhs.z -= rhs.z;
			return lhs;
		}
		friend constexpr Vec3& operator-=(Vec3& lhs, const float& rhs)
		{
			lhs.x -= rhs;
			lhs.y -= rhs;
			lhs.z *= rhs;
			return lhs;
		}
		friend constexpr Vec3& operator*=(Vec3& lhs, const Vec3& rhs)
		{
			lhs.x *= rhs.x;
			lhs.y *= rhs.y;
			lhs.z *= rhs.z;
			return lhs;
		}
		friend constexpr Vec3& operator*=(Vec3& lhs, const float& rhs)
		{
			lhs.x *= rhs;
			lhs.y *= rhs;
			lhs.z *= rhs;
			return lhs;
		}
		friend constexpr Vec3& operator/=(Vec3& lhs, const Vec3& rhs)
		{
			lhs.x /= rhs.x;
			lhs.y /= rhs.y;
			lhs.z /= rhs.z;
			return lhs;
		}
		friend constexpr Vec3& operator/=(Vec3& lhs, const float& rhs)
		{
			lhs.x /= rhs;
			lhs.y /= rhs;
			lhs.z /= rhs;
			return lhs;
		}
		friend Vec3& operator%=(Vec3& lhs, const float& rhs)
		{
			lhs.x = std::fmod(lhs.x, rhs);
			lhs.y = std::fmod(lhs.y, rhs);
			lhs.z = std::fmod(lhs.z, rhs);
			return lhs;
		}

		friend constexpr bool operator==(const Vec3& lhs, const Vec3& rhs)
		{
			return length2(lhs - rhs) < 0.0000000001f;
		}

		friend Vec3 abs(const Vec3& lhs) { return Vec3(std::abs(lhs.x), std::abs(lhs.y), std::abs(lhs.z)); }
		friend Vec3 sqrt(const Vec3& lhs)
		{
			return Vec3(std::sqrt(lhs.x), std::sqrt(lhs.y), std::sqrt(lhs.z));
		}
		friend Vec3 pow(const Vec3& lhs, float exp)
		{
			return Vec3(std::pow(lhs.x, exp), std::pow(lhs.y, exp), std::pow(lhs.z, exp));
		}
		friend constexpr float dot(const Vec3& lhs, const Vec3& rhs)
		{
			return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
		}
		friend constexpr Vec3 cross(const Vec3& lhs, const Vec3& rhs)
		{
			return {lhs.y * rhs.z - lhs.z * rhs.y, lhs.z * rhs.x - lhs.x * rhs.z, lhs.x * rhs.y - lhs.y * rhs.x};
		}
		friend constexpr float length2(const Vec3& lhs) { return dot(lhs, lhs); }
		friend float           length(const Vec3& lhs) { return std::sqrt(length2(lhs)); }
		/// \returns The square of Euclidean distance between `lhs` and `rhs`
		friend constexpr float dist2(const Vec3& lhs, const Vec3& rhs) { return length2(lhs - rhs); }
		/// \returns The Euclidean distance between `lhs` and `rhs`
		friend float dist(const Vec3& lhs, const Vec3& rhs) { return std::sqrt(dist2(lhs, rhs)); }
		friend Vec3  normalized(const Vec3& lhs) { return lhs / length(lhs); }
		/// \returns Either the unchanged `lhs` if its length is shorter or equal to `mas_length` or
		/// a normalized vector with a length of `max_length`.
		friend Vec3 limit_length(const Vec3& lhs, float max_length)
		{
			if(auto len2 = length2(lhs); len2 > max_length * max_length)
				return lhs / std::sqrt(len2) * max_length;
			else
				return lhs;
		}
		/// \returns The linear interpolation between 'a' and 'b'.
		/// The parameter 't' is clamped to be inside [0, 1].
		friend Vec3 lerp(const Vec3& a, const Vec3& b, float t)
		{
			if(t <= 0.f)
				return a;
			else if(t >= 1.f)
				return b;

			return {std::lerp(a.x, b.x, t), std::lerp(a.y, b.y, t), std::lerp(a.z, b.z, t)};
		}
		/// \returns The spherical linear interpolation between 'a' and 'b'.
		/// The parameter 't' is clamped to be inside [0, 1].
		friend Vec3 slerp(const Vec3& a, const Vec3& b, float t)
		{
			if(t <= 0.f)
				return a;
			else if(t >= 1.f)
				return b;

			// technically NLerp, but should be good enough for our use-case
			const auto a_len   = length(a);
			const auto b_len   = length(b);
			const auto new_len = std::lerp(a_len, b_len, t);

			return normalized(lerp(a / a_len, b / b_len, t)) * new_len;
		}

		friend std::ostream& operator<<(std::ostream& o, const Vec3& v)
		{
			o << "Vec3(" << v.x << ", " << v.y << ", " << v.z << ")";
			return o;
		}
	};
	static_assert(std::is_standard_layout_v<Vec3>);
	static_assert(std::is_standard_layout_v<YGDL_Vec3>);
	static_assert(sizeof(Vec3) == sizeof(YGDL_Vec3));
	static_assert(alignof(Vec3) == alignof(YGDL_Vec3));

	/**Calculate the circumcenter of the triangle defined by the three given points.
	 * \attention All parameters have to be normalized!
	 * \note Based on https://fsu.digital.flvc.org/islandora/object/fsu:182663/datastream/PDF/view
	 */
	inline Vec3 circumcenter(const Vec3& a, const Vec3& b, const Vec3& c)
	{
		return normalized(cross(b - a, c - a));
	}
	/**Calculate the circumcenter of the triangle defined by the three given points.
	 * \note In contrast to `circumcenter`, this function works with non-unit vectors but is generally more costly because of this.
	 * \note Based on https://fsu.digital.flvc.org/islandora/object/fsu:182663/datastream/PDF/view
	 */
	inline Vec3 circumcenter_non_normalized(const Vec3& a, const Vec3& b, const Vec3& c)
	{
		const auto a_len = length(a);
		const auto b_len = length(b);
		const auto c_len = length(c);
		return normalized(cross(b / b_len - a / a_len, c / c_len - a / a_len)) * (a_len + b_len + c_len) / 3.f;
	}


	/// Small helper-type that can be used to pass overload-sets to generic visitors
	/// \see e.g. `Const_dictionary_value_view::visit`
	template <class... Ts>
	struct overloaded : Ts... {
		using Ts::operator()...;
	};
	/// \skip (deduction guide)
	template <class... Ts>
	overloaded(Ts...) -> overloaded<Ts...>;

	template <typename Variant, typename... Visitors>
	inline decltype(auto) visit(Variant&& v, Visitors&&... vs)
	{
		return std::visit(overloaded{std::forward<Visitors>(vs)...}, std::forward<Variant>(v));
	}

} // namespace yggdrasill
