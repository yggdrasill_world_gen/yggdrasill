/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/string.h>

#include <yggdrasill/commons.hpp>

#include <cassert>
#include <compare>
#include <cstring>
#include <iostream>
#include <string>
#include <string_view>
#include <utility>

namespace yggdrasill {

	namespace detail {
		constexpr YGDL_String_view to_ygdl_string_view(std::string_view str)
		{
			return YGDL_String_view{str.data(), static_cast<index_t>(str.size())};
		}
		constexpr std::string_view from_ygdl_string_view(YGDL_String_view str)
		{
			return std::string_view(str.chars, static_cast<std::size_t>(str.length));
		}
	} // namespace detail

	/**A sequence of characters, similar to `std::string` but binary compatible to `YGDL_String`.\n
	 * Characters are 8-Bit ASCII. UTF-8 characters are supported for storage, but access operators and traversal operate on bytes instead of code units.\n
	 * Small strings are stored inline, instead of incurring a heap allocation, if their length is <= `YGDL_SMALL_STRING_SIZE`.
	 * If the string is larger, like `std::string` the managed heap-buffer will reserve more space than necessary to avoid frequent reallocations.
	 * \note Constructible and assignable from the C-Type `YGDL_String`, `std::string` and `std::string_view`, as well as convertible to `std::string_view`
	 */
	class String {
	  public:
		/// Creates an empty string  \skip
		constexpr String() : string_(YGDL_string_create_empty()) {}
		/// Move constructor from C-Type  \skip
		/*implicit*/ String(YGDL_String&& rhs) noexcept : string_(rhs) { rhs = YGDL_string_create_empty(); }
		/// Move constructor from C-Type  \skip
		/*implicit*/ String(const YGDL_String& rhs) : string_(YGDL_string_copy(&rhs)) {}
		/// Move constructor \skip
		/*implicit*/ String(String&& rhs) noexcept : string_(rhs.string_)
		{
			rhs.string_ = YGDL_string_create_empty();
		}
		/// Copy constructor \skip
		/*implicit*/ String(const String& rhs) : string_(YGDL_string_copy(&rhs.string_)) {}
		/// Copy constructor from C string-view  \skip
		explicit String(YGDL_String_view str) : string_(YGDL_string_create(str)) {}
		/// Copy constructor from C++ string-view  \skip
		explicit String(std::string_view str) : String(detail::to_ygdl_string_view(str)) {}
		/// Destructor \skip
		~String() { YGDL_string_destroy(&string_); }

		/// Move assignment from C-Type  \skip
		String& operator=(YGDL_String&& string) noexcept
		{
			YGDL_string_destroy(&string_);
			string_ = std::exchange(string, YGDL_string_create_empty());
			return *this;
		}
		/// Copy assignment from C-Type  \skip
		String& operator=(const YGDL_String& rhs) { return *this = YGDL_string_get(&rhs); }
		/// Move assignment \skip
		String& operator=(String&& rhs) noexcept
		{
			if(&rhs == this)
				return *this;

			YGDL_string_destroy(&string_);
			string_ = std::exchange(rhs.string_, YGDL_string_create_empty());
			return *this;
		}
		/// Copy assignment \skip
		String& operator=(const String& rhs)
		{
			if(&rhs == this)
				return *this;

			YGDL_string_set(&string_, YGDL_string_get(&rhs.string_));
			return *this;
		}
		/// Copy assignment from C++ string-view \skip
		String& operator=(std::string_view data)
		{
			YGDL_string_set(&string_, detail::to_ygdl_string_view(data));
			return *this;
		}
		/// Copy assignment from C string-view \skip
		String& operator=(YGDL_String_view data)
		{
			YGDL_string_set(&string_, data);
			return *this;
		}

		/// \skip
		friend void swap(String& lhs, String& rhs) { std::swap(lhs.string_, rhs.string_); }

		/// Concatenates `lhs` and `rhs`
		friend String operator+(const String& lhs, const String& rhs) { return append(lhs, rhs); }
		friend String operator+(const String& lhs, std::string_view rhs) { return append(lhs, rhs); }
		friend String operator+(std::string_view lhs, const String& rhs) { return append(lhs, rhs); }
		friend String operator+(const String& lhs, const char* rhs) { return append(lhs, rhs); }

		/// Append `rhs` to this string
		/// \note Invalidates all pointers to stored characters
		String& operator+=(const String& rhs)
		{
			YGDL_string_append(&string_, YGDL_string_get(&rhs.string_));
			return *this;
		}
		String& operator+=(std::string_view rhs)
		{
			YGDL_string_append(&string_, detail::to_ygdl_string_view(rhs));
			return *this;
		}

		/// \returns `true` if both `lhs` and `rhs` have the same length and contain exactly the same characters
		friend bool operator==(const String& lhs, const String& rhs) { return lhs.view() == rhs.view(); }
		friend bool operator==(const String& lhs, const std::string_view& rhs) { return lhs.view() == rhs; }
		friend bool operator==(const String& lhs, const std::string& rhs) { return lhs.view() == rhs; }
		friend bool operator==(const String& lhs, const char* rhs) { return lhs.view() == rhs; }

#if __cpp_lib_three_way_comparison >= 201907
		friend std::strong_ordering operator<=>(const String& lhs, const String& rhs)
		{
			return lhs.view() <=> rhs.view();
		}
		friend std::strong_ordering operator<=>(const String& lhs, const std::string_view& rhs)
		{
			return lhs.view() <=> rhs;
		}
#endif

		/**Clears this string and returns a C-API string that contains the previous contents.
		 * \note Invalidates all pointers to stored characters
		 * \attention This serves as an escape hatch to pass ownership of strings to the C-API. The returned
		 * object **won't** automatically free any resources.
		 */
		[[nodiscard]] YGDL_String release() { return std::exchange(string_, YGDL_string_create_empty()); }

		/*implicit*/ operator std::string_view() const
		{
			return detail::from_ygdl_string_view(YGDL_string_get(&string_));
		}

		/// \returns The length of the string, i.e. the number of stored bytes
		[[nodiscard]] index_t size() const { return YGDL_string_size(&string_); }
		[[nodiscard]] bool    empty() const { return size() == 0; }
		/// \returns The number of reserved bytes, i.e. the maximum length we can store before we have to reallocate
		[[nodiscard]] index_t capacity() const { return YGDL_string_capacity(&string_); }

		/// \returns A pointer to the first byte
		[[nodiscard]] char*       data() { return YGDL_string_data(&string_); }
		[[nodiscard]] const char* data() const { return YGDL_string_const_data(&string_); }
		/// \returns A null-terminated C-String that references the contents of this string
		[[nodiscard]] const char* c_str() const { return data(); }
		/// \returns A `std::string` with a copy of this string
		[[nodiscard]] std::string std_str() const { return std::string(*this); }
		/// \returns A string-view that references the contents of this string
		[[nodiscard]] std::string_view view() const { return *this; }

		/// \note Invalidates all pointers to stored characters
		void clear() { YGDL_string_set(&string_, YGDL_String_view{"", 0}); }
		/// \note Invalidates all pointers to stored characters
		void reserve(index_t size) { YGDL_string_reserve(&string_, size); }
		/// \note Invalidates all pointers to stored characters
		void resize(index_t new_size, char c = '\0') { YGDL_string_resize(&string_, new_size, c); }

		/// \returns A random access iterator (i.e. a pointer) to the first byte
		[[nodiscard]] char*       begin() { return data(); }
		[[nodiscard]] const char* begin() const { return data(); }
		/// \returns A random access iterator (i.e. a pointer) to one-past the last byte
		[[nodiscard]] char*       end() { return data() + size(); }
		[[nodiscard]] const char* end() const { return data() + size(); }

		friend std::ostream& operator<<(std::ostream& s, const String& str)
		{
			s << std::string_view(str);
			return s;
		}
		friend std::istream& operator>>(std::istream& s, String& str)
		{
			auto buffer = std::string();
			s >> buffer;
			str = buffer;
			return s;
		}

	  private:
		YGDL_String string_;

		static String append(std::string_view lhs, std::string_view rhs)
		{
			auto str = YGDL_string_create_empty();
			YGDL_string_reserve(&str, index_t(lhs.size() + rhs.size()));
			YGDL_string_append(&str, detail::to_ygdl_string_view(lhs));
			YGDL_string_append(&str, detail::to_ygdl_string_view(rhs));
			return String(std::move(str));
		}
	};
	static_assert(std::is_standard_layout_v<String>);
	static_assert(std::is_standard_layout_v<YGDL_String>);
	static_assert(sizeof(String) == sizeof(YGDL_String));
	static_assert(alignof(String) == alignof(YGDL_String));

} // namespace yggdrasill

namespace std {

	template <>
	struct hash<yggdrasill::String> {
		size_t operator()(const yggdrasill::String& str) const noexcept
		{
			auto hash = size_t(0);
			for(auto c : str) {
				hash = hash * 31 + c;
			}
			return hash;
		}
	};
} // namespace std
