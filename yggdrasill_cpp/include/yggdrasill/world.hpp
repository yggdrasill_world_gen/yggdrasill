/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#pragma once

#include <yggdrasill/world.h>

#include <yggdrasill/commons.hpp>
#include <yggdrasill/dictionary.hpp>
#include <yggdrasill/mesh.hpp>
#include <yggdrasill/random.hpp>

#include <memory>
#include <utility>
#include "layer.hpp"

namespace yggdrasill {

	class World;
	class World_view;
	class Mesh_lock;
	template <typename LayerType, typename... RestLayerTypes>
	class Layer_lock;
	class Unstructured_layer_lock;
	template <typename T>
	struct Element_interpolation;
	struct Layer_mesh_changes;

	/**Changes if the release of a mutable lock on world objects triggers a mesh validation.\n
	 * While these validations incur some additional cost, they are valuable to detect programming errors.
	 * \note Validation is enabled by default.
	 * \note This setting is shared between all world objects and threads, but changing it might not take
	 * effect immediately for concurrent threads.
	 */
	inline void validate_mesh_on_release(bool validate)
	{
		YGDL_world_validate_mesh_on_release_set(validate ? YGDL_TRUE : YGDL_FALSE);
	}
	/// \returns If the worlds data are validated after modifications.
	/// \see `validate_worlds_on_release(bool)`
	inline bool validate_mesh_on_release()
	{
		return YGDL_world_validate_mesh_on_release_get() != YGDL_FALSE;
	}

	/**Changes if the release of a mutable lock on world objects triggers a layer validation.\n
	 * If set to true, when the guard objects returned by the ``lock_*`` methods in `World_view` are destroyed,
	 * the data that might have been modified is checked against the valid range specified in the `Layer_definition`,
	 * as well as other invariants.\n
	 * While these validations incur some additional cost, they are valuable to detect programming errors.
	 * \note Validation is enabled by default.
	 * \note This setting is shared between all world objects and threads, but changing it might not take
	 * effect immediately for concurrent threads.
	 */
	inline void validate_layers_on_release(bool validate)
	{
		YGDL_world_validate_layers_on_release_set(validate ? YGDL_TRUE : YGDL_FALSE);
	}
	/// \returns If the worlds data are validated after modifications.
	/// \see `validate_worlds_on_release(bool)`
	inline bool validate_layers_on_release()
	{
		return YGDL_world_validate_layers_on_release_get() != YGDL_FALSE;
	}

	/**An immutable non-owning view of a generated world.
	 * 
	 * The view might reference no underlying object (nullptr), in which case the bool-conversion-operator
	 * returns false and none of the other methods may be called.
	 * 
	 * The stored state consists of a `Mesh`, a `Random_view` and a number of data layers.
	 * 
	 * These individual parts (including the separate layers) are internally stored using copy-on-write
	 * semantics, to enable efficient deep copies of the owning `World` objects. Thus, parts of this object
	 * may be shared with other instances and their address may change if they are modified.
	 * 
	 * \attention Thread-Safety: Multiple threads may access different `World` objects (or views of them)
	 * even if they share some parts due to the copy-on-write system. But any calls on the same objects have
	 * to externally synchronized.
	 * 
	 * \see See the project documentation for details on the internal structure and how copies and modifications work:
	 * https://yggdrasill_world_gen.gitlab.io/yggdrasill/api_cpp/world
	 */
	class Const_world_view {
	  public:
		/// \skip
		using const_view = Const_world_view;

		/// Converting constructor from a pointer to the C-API struct
		constexpr /*implicit*/ Const_world_view(const YGDL_World* world = nullptr) : world_(world) {}
		Const_world_view(const World&&) = delete; ///< \skip forbid construction from temporary

		/// Conversion operator to bool
		/// \returns true if this view references an existing world
		constexpr explicit operator bool() const { return world_; }

		/// \throws Precondition_violation_exception if one of the world invariants has been breached, e.g.
		/// if a layer contains a value that is beyond its specified range
		void validate() const;

		/// \returns An immutable view of the stored mesh
		[[nodiscard]] Const_mesh_view mesh() const;
		/// \returns A operator== comparable object, that changes every time a mutable view of the mesh,
		/// acquired from `World_view::lock_mesh`, is released again.
		[[nodiscard]] Data_version mesh_version() const;
		/**Convenience helper that checks if the mesh changed since the last call to this function.
		 * \param version The object used to memorize the last version number, as returned by `mesh_version()`,
		 * The object has to be stored and reused for each subsequent call on this module-handle.
		 * \returns true if a mutable view of the mesh was acquired since the last call.
		 * \note A return value of false usually means that the stored data hasn't changed since the last call,
		 * but that may not be guaranteed if `mesh_modified` is called while the mesh is locked and actively
		 * modified.
		 */
		[[nodiscard]] bool mesh_modified(Data_version& version) const;


		/**Retrieves a read-only view of one or more structured layers.
		 * The function may be called with multiple `Layer_definition` objects, in which case a `std::tuple` is returned.
		 * 
		 * \returns An immutable view of the requested layer or `std::nullopt` if no such layer exists.
		 * Or a tuple of optionals if multiple layers were requested
		 * \attention The returned views may be invalidated by locking the layers to acquire a mutable references
		 * or by modifying the mesh in a way that affects these layers.
		 */
		template <Any_layer_definition LayerInfo>
		[[nodiscard]] std::optional<const_view_of<LayerInfo>> layer(const LayerInfo& def) const;
		template <Any_layer_definition... LayerInfo>
		[[nodiscard]] std::tuple<std::optional<const_view_of<LayerInfo>>...> layer(const LayerInfo&... defs) const;

		/**Retrieves a read-only view of one or more non-optional structured layers.
		 * The function may be called with multiple `Layer_definition` objects, in which case a `std::tuple` is returned.
		 * 
		 * \returns An immutable view of the requested layer. The type of the layer depends on the passed
		 * arguments. If multiple layers are requested, a tuple of views is returned.
		 * \throws Precondition_violation_exception If one of the requested layers doesn't exist
		 * \attention The returned views may be invalidated by locking the layers to acquire a mutable references
		 * or by modifying the mesh in a way that affects these layers.
		 */
		template <Any_layer_definition LayerInfo>
		[[nodiscard]] const_view_of<LayerInfo> required_layer(const LayerInfo& def) const;
		template <Any_layer_definition... LayerInfo>
		[[nodiscard]] std::tuple<const_view_of<LayerInfo>...> required_layer(const LayerInfo&... defs) const;

		/// \returns A operator== comparable object, that changes every time a mutable view of the given layer,
		/// acquired from `World_view::lock_layer`, is released again.
		template <Any_layer_definition LayerInfo>
		[[nodiscard]] Data_version layer_version(const LayerInfo& def) const;

		/**Convenience helper that checks if the given layer changed since the last call to this function.
		 * \param version The object used to memorize the last version number, as returned by `layer_version()`,
		 * The object has to be stored and reused for each subsequent call with the same layer.
		 * \returns true if a mutable view of the given layer was acquired since the last call.
		 * \note A return value of false usually means that the stored data hasn't changed since the last call,
		 * but that may not be guaranteed if `layer_modified` is called while the layer is locked and actively
		 * modified.
		 */
		template <Any_layer_definition LayerInfo>
		[[nodiscard]] bool layer_modified(const LayerInfo& def, Data_version& version) const;

		/// Calls the given `visit` function for each structured, i.e. mesh-based, layer.
		/// \tparam Visitor A type that is callable with a rvalue of an arbitrary `Const_layer_view` specialization
		template <typename Visitor>
		void foreach_layer(Visitor&& visit) const;


		/**Retrieves a read-only view of the unstructured layer with the given id.
		 * \returns An immutable view of the requested layer or `std::nullopt` if no such layer exists.
		 * \attention The returned view may be invalidated by locking the layer to acquire a mutable reference.
		 */
		[[nodiscard]] std::optional<Const_dictionary_view> unstructured_layer(std::string_view id) const;

		/**Retrieves a read-only view of a non-optional unstructured layers.
		 * \returns An immutable view of the requested layer.
		 * \throws Precondition_violation_exception If the requested layer doesn't exist
		 * \attention The returned view may be invalidated by locking the layer to acquire a mutable reference.
		 */
		[[nodiscard]] Const_dictionary_view required_unstructured_layer(std::string_view id) const;

		/// \returns A operator== comparable object, that changes every time a mutable view of the given layer,
		/// acquired from `World_view::lock_unstructured_layer`, is released again.
		[[nodiscard]] Data_version unstructured_layer_version(std::string_view id) const;

		/**Convenience helper that checks if the given unstructured layer changed since the last call to this function.
		 * \param version The object used to memorize the last version number, as returned by `unstructured_layer_version()`,
		 * The object has to be stored and reused for each subsequent call with the same layer.
		 * \returns true if a mutable view of the given layer was acquired since the last call.
		 * \note A return value of false usually means that the stored data hasn't changed since the last call,
		 * but that may not be guaranteed if `unstructured_layer_modified` is called while the layer is locked
		 * and actively modified.
		 */
		[[nodiscard]] bool unstructured_layer_modified(std::string_view id, Data_version& version) const;

		/// Calls the given `visit` function for each unstructured structured layer and passes its ID.
		template <std::invocable<std::string_view> Visitor>
		void foreach_unstructured_layer(Visitor&& visit) const;

		/// \returns A range of all unstructured layer IDs currently stored in this world
		[[nodiscard]] std::vector<std::string_view> unstructured_layers() const;


		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
		 * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
		 */
		[[nodiscard]] constexpr friend const YGDL_World* c_ptr(Const_world_view w) { return w.world_; }
		[[nodiscard]] constexpr friend const YGDL_World* c_ptr(const Const_world_view* w)
		{
			return w ? w->world_ : nullptr;
		}

		/// \returns true if the two views represent the same underlying object.
		/// \note This means this function returns false if the two views represent distinct but otherwise identical objects.
		friend bool operator==(const Const_world_view& lhs, const Const_world_view& rhs)
		{
			return lhs.world_ == rhs.world_;
		}

	  protected:
		const YGDL_World* world_; ///< \skip
	};

	/**A mutable non-owning view of a generated world.
	 * 
	 * The view might reference no underlying object (nullptr), in which case the bool-conversion-operator
	 * returns false and none of the other methods may be called.
	 * 
	 * \see See the project documentation for details on the internal structure and how copies and modifications work:
	 * https://yggdrasill_world_gen.gitlab.io/yggdrasill/api_cpp/world
	 */
	class World_view : public Const_world_view {
	  public:
		/// \skip
		using const_view = Const_world_view;

		/// Converting constructor from a pointer to the C-API struct
		constexpr /*implicit*/ World_view(YGDL_World* world = nullptr) : Const_world_view(world) {}
		World_view(const World&&) = delete; ///< \skip forbid construction from temporary

		/// \returns A view object thant can be used to generate pseudo random numbers
		[[nodiscard]] Random_view random();

		/**Acquires a mutable view of the stored mesh.
		 * \returns A RAII wrapper that automatically release the lock on destruction and can be deconstructed
		 * using structured bindings to yield the contained `Mesh_view`.
		 * \throws Precondition_violation_exception If the lock couldn't be acquired, for example if a data
		 * race has been detected.
		 * \note See `Const_world_view` for usage examples.
		 * \attention The returned object can't be copied or moved and its lifetime should generally be
		 * kept as short as possible.\n
		 * \attention The destructor of the returned wrapper may throw, if the lock can't be released or
		 * if the mesh has been left in an invalid state.
		 */
		[[nodiscard]] Mesh_lock lock_mesh();

		/**Acquires a mutable view of the requested layers, creating them if they don't already exist.
		 * \throws Precondition_violation_exception If the lock couldn't be acquired, for example if a data
		 * race has been detected.
		 * \throws Type_mismatch_exception If the passed `LayerInfo` is incompatible with the existing layer.
		 * \note See `Const_world_view` for usage examples.
		 * \attention The returned object can't be copied or moved and its lifetime should generally be
		 * kept as short as possible.\n
		 * \attention The destructor of the returned wrapper may throw, if the lock can't be released or
		 * if the layer has been left in an invalid state.
		 * \returns A RAII wrapper that automatically release the lock on destruction and can be deconstructed
		 * using structured bindings to yield the contained `Layer_view` objects.
		 */
		template <Any_layer_definition... LayerInfo>
		[[nodiscard]] Layer_lock<view_of<LayerInfo>...> lock_layer(const LayerInfo&... def);

		/**Removes the given layer from this world.
		 * \attention This invalidates all views to this layer. The behaviour is undefined if there are any
		 * active locks for this layer.
		 */
		template <Any_layer_definition LayerInfo>
		void remove_layer(const LayerInfo& def);

		/**Invalidates part of the data stored in the mesh-based layers, as described in the parameter.\n
		 * How each layer is modified by this operation depends on its configuration. Specifically their
		 * settings for `On_mesh_change` and `Interpolation`.
		 * \attention This may acquire locks for any mesh-based layer and thus invalidate any const-views
		 * that haven't been locked so far.
		 * \note Invalidation of data layers normally happens automatically when the mesh is modified. This
		 * function is provided as an escape hatch for the rare use case that part of the data becomes
		 * outdated because of other reasons than changes in topology. For example, if the vertex positions
		 * are modified and other layers may depend on them and needs to be re-interpolated or invalidated.
		 */
		void invalidate_layers(const Layer_mesh_changes& req);


		/**Acquires a mutable view of the requested layer, creating it if it doesn't already exist.
		 * \throws Precondition_violation_exception If the lock couldn't be acquired, for example if a data
		 * race has been detected.
		 * \attention The returned object can't be copied or moved and its lifetime should generally be
		 * kept as short as possible.\n
		 * \attention The destructor of the returned wrapper may throw, if the lock can't be released or
		 * if the layer has been left in an invalid state.
		 * \returns A RAII wrapper that automatically release the lock on destruction and can be deconstructed
		 * using structured bindings to yield the contained `Dictionary_view`.
		 */
		[[nodiscard]] Unstructured_layer_lock lock_unstructured_layer(std::string_view id);

		/**Removes the given unstructured layer from this world.
		 * \attention This invalidates all views to this layer. The behaviour is undefined if there are any
		 * active locks for this layer.
		 */
		void remove_unstructured_layer(std::string_view id);


		/**Returns a pointer to the underlying C-API type, for use in the internal implementation
		 * and as an escape hatch for directly interaction with the C-API
		 * \note Returns `nullptr` if either the argument is `nullptr` (for the pointer overload) or the
		 * wrapped C-API type is `nullptr` (e.g. for moved-from-state)
		 */
		[[nodiscard]] constexpr friend YGDL_World* c_ptr(World_view w)
		{
			// we can safely cast away const here, because (inside the World_view class) we know that world_ was initialized from a pointer to non-const
			return const_cast<YGDL_World*>(c_ptr(static_cast<const Const_world_view&>(w)));
		}
		[[nodiscard]] constexpr friend YGDL_World* c_ptr(World_view* w)
		{
			return const_cast<YGDL_World*>(c_ptr(static_cast<const Const_world_view*>(w)));
		}

	  private:
		YGDL_Layer* acquire_layer(const YGDL_Layer_definition& layer_def, YGDL_Error* out_error);
	};

	/**The complete state of a generated world.
	 * 
	 * Contrary to `World_view` and `Const_world_view` objects of this type have value semantics. A
	 * copy-on-write system is used to implement the special member functions, which means that copies without
	 * or with only minor modifications are generally both cheap and memory efficient.
	 * Thus, `World` objects may be copied to create a snapshot of the current simulation state, that can later
	 * be restored, e.g. to implement an undo/redo-system.
	 * 
	 * \see See the project documentation for details on the internal structure and how copies and modifications work:
	 * https://yggdrasill_world_gen.gitlab.io/yggdrasill/api_cpp/world
	 * 
	 * \note The actual state is stored in an internal heap allocated object, that is only referenced by the
	 * `World` instances. Thus, the `World` object itself is small (usually ``sizeof(void*)``) and cheap to move.
	 * Additionally, `World_view` and `Const_world_view` object are not invalidated if the object they reference
	 * is moved from.
	 * \attention The behaviour is undefined if a `World` object is copied, assigned to or destroyed while
	 * there are still un-released locks on its RNG, mesh or layers.
	 */
	class World final : public World_view {
	  public:
		explicit World();
		explicit World(const Seed& seed);
		World(World&& rhs) noexcept;
		World(const World& rhs);
		explicit World(const Const_world_view& rhs);
		~World();

		World& operator=(World&& rhs) noexcept;
		World& operator=(const World& rhs);
	};


	/**Describes, which parts of the mesh-based layers are affected by the manuel invalidation triggered by
	 * `World_view::invalidate_layers(...)`.\n
	 * This consists of vertices/faces/edges that have become invalid (as if the element was removed from
	 * the mesh), and should be handled according to the layers `Invalidation`, and
	 * vertices/faces/edges whose data should be re-interpolated (as if e.g. a new vertex were inserted)
	 * according to the layers `Interpolation`.
	 */
	struct Layer_mesh_changes {
		std::span<const index_t> invalidated_vertices     = {};
		std::span<const index_t> invalidated_faces        = {};
		std::span<const index_t> invalidated_primal_edges = {};
		std::span<const index_t> invalidated_dual_edges   = {};

		std::span<const Element_interpolation<Vertex>>      interpolated_vertices     = {};
		std::span<const Element_interpolation<Face>>        interpolated_faces        = {};
		std::span<const Element_interpolation<Primal_edge>> interpolated_primal_edges = {};
		std::span<const Element_interpolation<Dual_edge>>   interpolated_dual_edges   = {};
		float                                               interpolation_weight      = 0.f;
	};
	/// \tparam T A mesh element type, so either `Vertex`, `Face`, `Generic_edge`, `Primal_edge` or
	/// `Dual_edge`, depending on the list in `Layer_invalidation` it's used in.
	template <typename T>
	struct Element_interpolation : public YGDL_Layer_element_interpolation {
		/**\param target The mesh element that is going to be modified
		 * \param origin The first mesh element used for interpolation
		 * \param dest The second mesh element used for interpolation
		 */
		Element_interpolation(T target, T origin, T dest)
		  : YGDL_Layer_element_interpolation{target.index(), origin.index(), dest.index()}
		{
		}
	};


	// region LockGuards
	/// Simple RAII lock-guard. Use structured-bindings to access the contained `Mesh_view`
	class Mesh_lock {
	  public:
		Mesh_lock(Mesh_lock&&)                 = delete; ///< \skip
		Mesh_lock(const Mesh_lock&)            = delete; ///< \skip
		Mesh_lock& operator=(Mesh_lock&&)      = delete; ///< \skip
		Mesh_lock& operator=(const Mesh_lock&) = delete; ///< \skip
		~Mesh_lock() noexcept(false) { YGDL_world_mesh_release(&world_, c_ptr(mesh_), Error_throw{}); }

		/// \skip
		template <std::size_t Index>
		Mesh_view& get()
		{
			static_assert(Index == 0);
			return mesh_;
		}

	  private:
		friend class World_view;

		YGDL_World& world_;
		Mesh_view   mesh_;

		Mesh_lock(YGDL_World& world, YGDL_Mesh& mesh) : world_(world), mesh_(mesh) {}
	};

	/// Simple RAII lock-guard. Use structured-bindings to access the contained `Layer_view`
	template <typename LayerType, typename... RestLayerTypes>
	class Layer_lock {
	  public:
		Layer_lock(Layer_lock&&)                 = delete; ///< \skip
		Layer_lock(const Layer_lock&)            = delete; ///< \skip
		Layer_lock& operator=(Layer_lock&&)      = delete; ///< \skip
		Layer_lock& operator=(const Layer_lock&) = delete; ///< \skip
		~Layer_lock() noexcept(false)
		{
			if(!world_)
				return;

			auto error = Error_throw();
			std::apply([&](auto... layers) { (..., YGDL_world_layer_release(world_, c_ptr(layers), error)); },
			           layers_);

			world_ = nullptr;
		}

		/// \skip
		template <std::size_t Index>
		auto& get()
		{
			return std::get<Index>(layers_);
		}

	  private:
		friend class World_view;

		YGDL_World*                              world_;
		std::tuple<LayerType, RestLayerTypes...> layers_;

		Layer_lock(YGDL_World& world, LayerType layer, RestLayerTypes... rest_layers)
		  : world_(&world), layers_{std::move(layer), std::move(rest_layers)...}
		{
		}
	};

	/// Simple RAII lock-guard. Use structured-bindings to access the contained `Dictionary_view`
	class Unstructured_layer_lock {
	  public:
		Unstructured_layer_lock(Unstructured_layer_lock&&)                 = delete; ///< \skip
		Unstructured_layer_lock(const Unstructured_layer_lock&)            = delete; ///< \skip
		Unstructured_layer_lock& operator=(Unstructured_layer_lock&&)      = delete; ///< \skip
		Unstructured_layer_lock& operator=(const Unstructured_layer_lock&) = delete; ///< \skip
		~Unstructured_layer_lock() { YGDL_world_unstructured_layer_release(&world_, c_ptr(layer_)); }

		/// \skip
		template <std::size_t Index>
		Dictionary_view& get()
		{
			static_assert(Index == 0);
			return layer_;
		}

	  private:
		friend class World_view;

		YGDL_World&     world_;
		Dictionary_view layer_;

		Unstructured_layer_lock(YGDL_World& world, YGDL_Dict& layer) : world_(world), layer_(layer) {}
	};
	// endregion
} // namespace yggdrasill

// region structured_bindings_for_lock_guards
namespace std {
	template <>
	struct tuple_size<::yggdrasill::Mesh_lock> : integral_constant<size_t, 1> {};
	template <size_t Index>
	struct tuple_element<Index, ::yggdrasill::Mesh_lock> {
		using type = ::yggdrasill::Mesh_view;
	};

	template <typename LayerType, typename... RestLayerTypes>
	struct tuple_size<::yggdrasill::Layer_lock<LayerType, RestLayerTypes...>>
	  : integral_constant<size_t, sizeof...(RestLayerTypes) + 1> {};
	template <size_t Index, typename LayerType, typename... RestLayerTypes>
	struct tuple_element<Index, ::yggdrasill::Layer_lock<LayerType, RestLayerTypes...>> {
		using type = typename tuple_element<Index, std::tuple<LayerType, RestLayerTypes...>>::type;
	};

	template <>
	struct tuple_size<::yggdrasill::Unstructured_layer_lock> : integral_constant<size_t, 1> {};
	template <size_t Index>
	struct tuple_element<Index, ::yggdrasill::Unstructured_layer_lock> {
		using type = ::yggdrasill::Dictionary_view;
	};
} // namespace std
// endregion

#define YGDL_WORLD_HPP_INCLUDED
#include <yggdrasill/world.inl>
