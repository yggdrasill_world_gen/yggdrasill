#include <catch2/catch.hpp>

#include "mesh_samples.hpp"

#include <yggdrasill/mesh.hpp>
#include <yggdrasill/world.hpp>

#include <array>
#include <cstring>
#include <set>
#include <unordered_set>

using namespace yggdrasill;

template <typename T>
static bool empty_state_element(T e)
{
	return e == T{};
}
template <typename T>
constexpr bool empty_state_element(Valid<T>)
{
	return false;
}

static int flood_fill_mesh(Const_mesh_view mesh, Vertex start)
{
	auto open_list = std::vector<Vertex>{start};
	auto new_list  = std::vector<Vertex>{};
	auto seen      = std::unordered_set<Vertex>{start};

	while(!open_list.empty()) {
		for(auto vertex : open_list) {
			auto initial_edge = vertex.edge(mesh);
			auto edge         = initial_edge;
			do {
				auto nv = edge.dest(mesh);
				if(seen.emplace(nv).second) {
					new_list.emplace_back(nv);
				}

				edge = edge.origin_next(mesh);
			} while(edge != initial_edge);
		}

		std::swap(open_list, new_list);
		new_list.clear();
	}

	return static_cast<int>(seen.size());
}

[[maybe_unused]] static void print_edges(Const_mesh_view mesh)
{
	std::cout << "Edges:\n";
	for(index_t i = 0; i <= mesh.edge_max_index(); i++) {
		auto e = Primal_edge(i);
		std::cout << e << ": " << e.origin(mesh) << " => " << e.dest(mesh) << "; FACES: " << e.left(mesh)
		          << " | " << e.right(mesh) << ";  origin_next()=" << e.origin_next(mesh) << "\n";
	}

	std::cout << "----------\n" << std::flush;

	for(index_t i = 0; i <= mesh.edge_max_index(); i++) {
		auto e = Dual_edge(i);
		std::cout << e << ": " << e.inv_rot().right(mesh) << " => " << e.inv_rot().left(mesh)
		          << ";  inv_rot()=" << e.inv_rot() << ";  origin_next()=" << e.origin_next(mesh) << "\n";
	}
	std::cout << "\n" << std::flush;
}
template <typename T>
void validate_edge_invariants(Const_mesh_view mesh, T&& face_context)
{
	auto count = 0;

	auto validate_edge = [&](auto e) {
		count++;

		INFO("Validating " << e);
		REQUIRE(e == e.rot().rot().rot().rot());
		REQUIRE(e.sym() == e.rot().rot());
		REQUIRE(e == e.sym().sym());

		REQUIRE(e.origin(mesh) == e.origin_next(mesh).origin(mesh));
		REQUIRE(e.origin(mesh) == e.origin_prev(mesh).origin(mesh));

		REQUIRE(e == e.origin_next(mesh).origin_prev(mesh));
		REQUIRE(e == e.dest_next(mesh).dest_prev(mesh));
		REQUIRE(e == e.left_next(mesh).left_prev(mesh));
		REQUIRE(e == e.right_next(mesh).right_prev(mesh));

		CHECK(e.origin_next(mesh) != e);

		if(e.type() == Edge_type::primal) {
			if(!empty_state_element(e.left(mesh)) && !empty_state_element(e.right(mesh))) {
				REQUIRE(e.origin_next(mesh) != e.origin_prev(mesh));
			}
		}
	};

	INFO("After " << face_context);
	for(auto e : mesh.directed_edges<Edge_type::primal>())
		validate_edge(e);
	REQUIRE(count == mesh.edge_used_count());
	count = 0;

	for(auto e : mesh.directed_edges<Edge_type::dual>())
		validate_edge(e);
	REQUIRE(count == mesh.edge_used_count());
	count = 0;

	INFO("validate iterators");
	const auto range = mesh.directed_edges<Edge_type::primal>();
	auto       iter  = range.begin();
	while(iter != range.end()) {
		++iter;
	}
	REQUIRE(iter == range.end());
	while(iter != range.begin()) {
		--iter;
	}
	REQUIRE(iter == range.begin());

	{
		INFO("Validate edge iterator integrity");
		for(auto e : mesh.undirected_edges<Edge_type::primal>()) {
			validate_edge(e);
			REQUIRE(count * 2 <= mesh.edge_used_count());
		}
		REQUIRE(count * 2 == mesh.edge_used_count());
	}

	{
		INFO("Validate face integrity");
		for(auto f : mesh.faces()) {
			auto e = f.edge(mesh);
			INFO("With " << e);
			REQUIRE(e.left(mesh) == f);
			validate_edge(e);
		}
	}

	{
		INFO("Validate vertex integrity");
		for(auto v : mesh.vertices()) {
			if(auto e = v.edge(mesh); e != no_edge) {
				INFO("With " << e);
				REQUIRE(e.origin(mesh) == v);
				validate_edge(e);
			}
		}
	}
}

TEST_CASE("yggdrasill::Mesh_view: creation and traversal")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 * 0-----1-----2-----3
	 * |    / \ F2 |\F3 /
	 * |F0 /   \   | \ /
	 * |  /     \  |  4----|
	 * | /  F1   \ |    F4 |
	 * |/         \|       |
	 * 5-----------6-------|
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(4);
	REQUIRE(mesh.vertex_max_index() == 3);
	for(auto v : std::span{c_ptr(mesh)->vertex_edge, std::size_t(c_ptr(mesh)->vertex_count)}) {
		REQUIRE(v == no_edge);
	}

	auto created_vertices = std::array<Vertex, 3>();
	mesh.add_vertices(created_vertices);
	CHECK(created_vertices[0] == Vertex{4});
	CHECK(created_vertices[1] == Vertex{5});
	CHECK(created_vertices[2] == Vertex{6});
	REQUIRE(mesh.vertex_max_index() == 6);
	for(auto v : std::span{c_ptr(mesh)->vertex_edge, std::size_t(c_ptr(mesh)->vertex_count)}) {
		REQUIRE(v == no_edge);
	}

	auto validate_edge_invariants = [&](auto last_face) { ::validate_edge_invariants(mesh, last_face); };

	auto validate_edges = [&](const Primal_edge edge_1_0,
	                          const Primal_edge edge_6_2,
	                          const Dual_edge   dual_edge_1_0,
	                          int               face0 = 0,
	                          int               face2 = 2) {
		REQUIRE(mesh.face_max_index() == 4);
		REQUIRE(mesh.edge_max_index() == 21);

		// check if edges are valid
		auto seen_edges = std::set<std::pair<Vertex, Vertex>>();
		for(index_t i = 0; i <= mesh.edge_max_index(); i++) {
			auto e         = Primal_edge(i);
			auto key       = std::make_pair(e.origin(mesh), e.dest(mesh));
			auto duplicate = !seen_edges.emplace(key).second;

			if(duplicate) {
				FAIL("Duplicate edge " << key.first << " -> " << key.second);
			}

			REQUIRE(!duplicate);
		}
		auto i = index_t(0);
		for(Primal_edge e : std::span{c_ptr(mesh)->vertex_edge, std::size_t(c_ptr(mesh)->vertex_count)}) {
			REQUIRE(e != no_edge);
			REQUIRE(e.type() == Edge_type::primal);
			REQUIRE(e.origin(mesh).index() == i);
			REQUIRE(e.origin_next(mesh).origin(mesh).index() == i);
			i++;
		}

		i = index_t(0);
		for(Primal_edge e : std::span{c_ptr(mesh)->face_edge, std::size_t(c_ptr(mesh)->face_count)}) {
			REQUIRE(e != no_edge);
			REQUIRE(e.type() == Edge_type::primal);
			REQUIRE(e.left_next(mesh).type() == Edge_type::primal);
			REQUIRE(e.left(mesh).index() == i);
			REQUIRE(e.left_next(mesh).left(mesh).index() == i);
			i++;
		}

		// check origin_next
		CHECK(edge_1_0.dest(mesh) == Vertex(0));
		CHECK(edge_1_0.origin_next(mesh).dest(mesh) == Vertex(5));
		CHECK(edge_1_0.origin_next(mesh).origin_next(mesh).dest(mesh) == Vertex(6));
		CHECK(edge_1_0.origin_next(mesh).origin_next(mesh).origin_next(mesh).dest(mesh) == Vertex(2));
		CHECK(edge_1_0.origin_next(mesh).origin_next(mesh).origin_next(mesh).origin_next(mesh).dest(mesh)
		      == Vertex(0));

		CHECK(edge_6_2.dest(mesh) == Vertex(2));
		CHECK(edge_6_2.origin_next(mesh).dest(mesh) == Vertex(1));
		CHECK(edge_6_2.origin_next(mesh).origin_next(mesh).dest(mesh) == Vertex(5));
		CHECK(edge_6_2.origin_next(mesh).origin_next(mesh).origin_next(mesh).dest(mesh) == Vertex(4));
		CHECK(edge_6_2.origin_next(mesh).origin_next(mesh).origin_next(mesh).origin_next(mesh).dest(mesh)
		      == Vertex(2));

		// check origin_next for faces
		CHECK(dual_edge_1_0.dest(mesh) == Face(face0));
		CHECK(dual_edge_1_0.origin_next(mesh).dest(mesh) == no_face);
		CHECK(dual_edge_1_0.origin_next(mesh).origin_next(mesh).dest(mesh) == Face(face2));
		CHECK(dual_edge_1_0.origin_next(mesh).origin_next(mesh).origin_next(mesh).dest(mesh) == Face(face0));

		// check origin_prev
		CHECK(edge_1_0.origin_prev(mesh).origin(mesh) == Vertex(1));
		CHECK(edge_1_0.origin_prev(mesh).dest(mesh) == Vertex(2));
		CHECK(edge_1_0.origin_prev(mesh).origin_prev(mesh).dest(mesh) == Vertex(6));
		CHECK(edge_1_0.origin_prev(mesh).origin_prev(mesh).origin_prev(mesh).dest(mesh) == Vertex(5));
		CHECK(edge_1_0.origin_prev(mesh).origin_prev(mesh).origin_prev(mesh).origin_prev(mesh).dest(mesh)
		      == Vertex(0));

		// check if all vertices are reachable
		REQUIRE(flood_fill_mesh(mesh, Vertex(0)) == 7);
	};

	// add faces and check if the edges are valid
	SECTION("Faces: 0, 1, 2, 3, 4")
	{
		validate_edge_invariants("none");

		REQUIRE(mesh.add_face(Vertex{0}, Vertex{5}, Vertex{1}).index() == 0);
		validate_edge_invariants(0);

		REQUIRE(mesh.add_face(Vertex{1}, Vertex{5}, Vertex{6}).index() == 1);
		validate_edge_invariants(1);

		REQUIRE(mesh.add_face(Vertex{1}, Vertex{6}, Vertex{2}).index() == 2);
		validate_edge_invariants(2);

		REQUIRE(mesh.add_face(Vertex{4}, Vertex{3}, Vertex{2}).index() == 3);
		validate_edge_invariants(3);

		// try to add invalid face (3-2-4 and 2-6-3 have different winding order, when laid out flat)
		REQUIRE_THROWS_AS(mesh.add_face(Vertex{2}, Vertex{3}, Vertex{6}), Mesh_corruption_exception);

		validate_edge_invariants("2-3-6 (invalid face)");

		REQUIRE(mesh.add_face(Vertex{4}, Vertex{2}, Vertex{6}).index() == 4);
		validate_edge_invariants(4);

		validate_edges(Edge(Edge_type::primal, 4), Edge(Edge_type::primal, 10), Edge(Edge_type::dual, 2));
	}
	SECTION("Faces: 0, 1, 2, 4, 3")
	{
		validate_edge_invariants("none");

		REQUIRE(mesh.add_face(Vertex{0}, Vertex{5}, Vertex{1}).index() == 0);
		validate_edge_invariants(0);

		REQUIRE(mesh.add_face(Vertex{1}, Vertex{5}, Vertex{6}).index() == 1);
		validate_edge_invariants(1);

		REQUIRE(mesh.add_face(Vertex{1}, Vertex{6}, Vertex{2}).index() == 2);
		validate_edge_invariants(2);

		REQUIRE(mesh.add_face(Vertex{4}, Vertex{2}, Vertex{6}).index() == 3);
		validate_edge_invariants(3);
		REQUIRE(mesh.add_face(Vertex{4}, Vertex{3}, Vertex{2}).index() == 4);
		validate_edge_invariants(4);

		validate_edges(Edge(Edge_type::primal, 4), Edge(Edge_type::primal, 10), Edge(Edge_type::dual, 2));
	}

	SECTION("Faces: 3, 0, 4, 1, 2")
	{
		validate_edge_invariants("none");

		REQUIRE(mesh.add_face(Vertex{4}, Vertex{3}, Vertex{2}).index() == 0);
		validate_edge_invariants(3);

		REQUIRE(mesh.add_face(Vertex{0}, Vertex{5}, Vertex{1}).index() == 1);
		validate_edge_invariants(0);

		REQUIRE(mesh.add_face(Vertex{4}, Vertex{2}, Vertex{6}).index() == 2);
		validate_edge_invariants(4);

		REQUIRE(mesh.add_face(Vertex{1}, Vertex{5}, Vertex{6}).index() == 3);
		validate_edge_invariants(1);

		REQUIRE(mesh.add_face(Vertex{1}, Vertex{6}, Vertex{2}).index() == 4);
		validate_edge_invariants(2);

		validate_edges(Edge(Edge_type::primal, 10), Edge(Edge_type::primal, 13), Edge(Edge_type::dual, 8), 1, 4);
	}

	SECTION("Mesh with holes")
	{
		mesh.add_vertices(2);
		REQUIRE(mesh.vertex_max_index() == 8);

		/*
		 * 0---1---2---3
		 * |\F1|\F3|\F6|
		 * |F0\|F7\|F5\|
		 * 4---5---6---7
		 *  \F4\ F8/F2/
		 *   \  |  | /
		 *    \  \/ /
		 *     \ | /
		 *      \ /
		 *       8
		 */

		mesh.add_face(Vertex{4}, Vertex{5}, Vertex{0});
		validate_edge_invariants(0);

		mesh.add_face(Vertex{5}, Vertex{1}, Vertex{0});
		validate_edge_invariants(1);

		mesh.add_face(Vertex{6}, Vertex{8}, Vertex{7});
		validate_edge_invariants(2);

		mesh.add_face(Vertex{1}, Vertex{6}, Vertex{2});
		validate_edge_invariants(3);

		mesh.add_face(Vertex{5}, Vertex{4}, Vertex{8});
		validate_edge_invariants(4);

		mesh.add_face(Vertex{7}, Vertex{2}, Vertex{6});
		validate_edge_invariants(5);

		mesh.add_face(Vertex{3}, Vertex{2}, Vertex{7});
		validate_edge_invariants(6);

		mesh.add_face(Vertex{1}, Vertex{5}, Vertex{6});
		validate_edge_invariants(7);

		mesh.add_face(Vertex{8}, Vertex{6}, Vertex{5});
		validate_edge_invariants(8);
	}
}

TEST_CASE("yggdrasill::Mesh_view: Creating duplicate faces")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	mesh.add_vertices(3);

	mesh.add_face(Vertex{0}, Vertex{1}, Vertex{2});

	REQUIRE_THROWS_AS(mesh.add_face(Vertex{0}, Vertex{1}, Vertex{2}), Mesh_corruption_exception);
}

TEST_CASE("yggdrasill::Mesh_view: flip")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 * 0-----1-----2-----3
	 * |    / \ F2 |\F3 /
	 * |F0 /   \   | \ /
	 * |  /     \  |  4----|
	 * | /  F1   \ |    F4 |
	 * |/         \|       |
	 * 5-----------6-------|
	 *
	 * =>
	 *
	 * 0-----1-----2-----3
	 * |    /    / |\F3 /
	 * |F0 / F2/   | \ /
	 * |  /  /     |  4----|
	 * | /  /  F1  |    F4 |
	 * |/ /        |       |
	 * 5-----------6-------|
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(7);
	mesh.add_face(Vertex{1}, Vertex{0}, Vertex{5});
	mesh.add_face(Vertex{5}, Vertex{6}, Vertex{1});
	mesh.add_face(Vertex{6}, Vertex{2}, Vertex{1});
	mesh.add_face(Vertex{4}, Vertex{3}, Vertex{2});
	mesh.add_face(Vertex{6}, Vertex{4}, Vertex{2});

	validate_edge_invariants(mesh, "pre flip");

	// Edge 2->1 can't be flip because it's at the border
	REQUIRE_THROWS_AS(mesh.flip(Primal_edge(12)), Mesh_corruption_exception);

	validate_edge_invariants(mesh, "post invalid flip");

	const auto edge = Primal_edge(8); // 6 -> 1

	mesh.flip(edge);

	validate_edge_invariants(mesh, "post flip");

	CHECK(edge.origin(mesh) == Vertex{2});
	CHECK(edge.dest(mesh) == Vertex{5});
	CHECK(edge.left(mesh) == Face(1));
	CHECK(edge.right(mesh) == Face(2));
	CHECK(edge.left_next(mesh).origin(mesh) == Vertex(5));
	CHECK(edge.left_next(mesh).left_next(mesh).origin(mesh) == Vertex(6));
	CHECK(edge.left_next(mesh).left_next(mesh).left_next(mesh).origin(mesh) == Vertex(2));

	const auto edge_sym = edge.sym();
	CHECK(edge_sym.origin(mesh) == Vertex{5});
	CHECK(edge_sym.dest(mesh) == Vertex{2});
	CHECK(edge_sym.left(mesh) == Face(2));
	CHECK(edge_sym.right(mesh) == Face(1));
	CHECK(edge_sym.left_next(mesh).origin(mesh) == Vertex(2));
	CHECK(edge_sym.left_next(mesh).left_next(mesh).origin(mesh) == Vertex(1));
	CHECK(edge_sym.left_next(mesh).left_next(mesh).left_next(mesh).origin(mesh) == Vertex(5));

	mesh.flip(edge);
	mesh.flip(edge);
	mesh.flip(edge);
	validate_edge_invariants(mesh, "post flip back to original");
	CHECK(edge.origin(mesh) == Vertex{6});
	CHECK(edge.dest(mesh) == Vertex{1});
	CHECK(edge.left(mesh) == Face{1});
	CHECK(edge.right(mesh) == Face{2});

	mesh.flip(edge.rot());
	validate_edge_invariants(mesh, "post flip dual edge");
	CHECK(edge.origin(mesh) == Vertex{2});
	CHECK(edge.dest(mesh) == Vertex{5});
	CHECK(edge.left(mesh) == Face(1));
	CHECK(edge.right(mesh) == Face(2));
}

TEST_CASE("yggdrasill::Mesh_view: vertex/face iterators")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 * 0-----1-----2-----3
	 * |    / \ F2 |\F3 /
	 * |F0 /   \   | \ /
	 * |  /     \  |  4----|
	 * | /  F1   \ |    F4 |
	 * |/         \|       |
	 * 5-----------6-------|
	 *
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(7);
	mesh.add_face(Vertex{1}, Vertex{0}, Vertex{5});
	mesh.add_face(Vertex{5}, Vertex{6}, Vertex{1});
	mesh.add_face(Vertex{6}, Vertex{2}, Vertex{1});
	mesh.add_face(Vertex{4}, Vertex{3}, Vertex{2});
	mesh.add_face(Vertex{6}, Vertex{4}, Vertex{2});

	validate_edge_invariants(mesh, "pre flip");

	using namespace yggdrasill;
	CHECK(mesh.vertices()
	      == std::initializer_list<Vertex>{
	              Vertex(0), Vertex(1), Vertex(2), Vertex(3), Vertex(4), Vertex(5), Vertex(6)});

	CHECK(Edge(Edge_type::primal, 11).origin_ccw(mesh)
	      == std::initializer_list<Primal_edge>{
	              Primal_edge(11), Primal_edge(18), Primal_edge(17), Primal_edge(12)});


	CHECK(Vertex(2).neighbors(mesh, Neighbor_type::just_primal_edges)
	      == std::initializer_list<Vertex>{Vertex(6), Vertex(4), Vertex(3), Vertex(1)});

	CHECK(Vertex(2).neighbors(mesh, Neighbor_type::with_dual_edges)
	      == std::initializer_list<Vertex>{Vertex(6), Vertex(5), Vertex(4), Vertex(3), Vertex(1)});

	CHECK(mesh.faces() == std::initializer_list<Face>{Face(0), Face(1), Face(2), Face(3), Face(4)});

	{
		auto range = Edge(Edge_type::primal, 11).origin_ccw(mesh);
		auto iter  = range.begin();
		REQUIRE(*iter == Edge(Edge_type::primal, 11));
		++iter;
		REQUIRE(*iter == Edge(Edge_type::primal, 18));
		--iter;
		REQUIRE(*iter == Edge(Edge_type::primal, 11));
		--iter;
		REQUIRE(*iter == Edge(Edge_type::primal, 12));
		++iter;
		REQUIRE(*iter == Edge(Edge_type::primal, 11));
		++iter;
		REQUIRE(*iter == Edge(Edge_type::primal, 18));
	}

	{
		auto range = Vertex(2).neighbors(mesh, Neighbor_type::just_primal_edges);
		auto iter  = range.begin();
		REQUIRE(*iter == Vertex(6));
		++iter;
		REQUIRE(*iter == Vertex(4));
		--iter;
		REQUIRE(*iter == Vertex(6));
		--iter;
		REQUIRE(*iter == Vertex(1));
		++iter;
		REQUIRE(*iter == Vertex(6));
		++iter;
		REQUIRE(*iter == Vertex(4));
	}
}

TEST_CASE("yggdrasill::Mesh_view: collapse (inner edge)")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F2 | F3  /|
	 *  |  \   |   /  |
	 *  |    \ | /    |
	 *  |     \|/     |
	 *  | F4   4  F5  |
	 *  |     / \     |
	 *  |    /   \    |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 *
	 * (2->4) =>
	 *
	 * // Faces removed: F2 & F3
	 * // Vertex removed: 4
	 * // Edges removed: 1->4, 2->4, 3->4
	 *
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  | F4  / \ F5  |
	 *  |    /   \    |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(7);
	mesh.add_face(Vertex{0}, Vertex{1}, Vertex{2});
	mesh.add_face(Vertex{0}, Vertex{2}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{4}, Vertex{2});
	mesh.add_face(Vertex{2}, Vertex{4}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{5}, Vertex{4});
	mesh.add_face(Vertex{4}, Vertex{6}, Vertex{3});
	mesh.add_face(Vertex{4}, Vertex{5}, Vertex{6});

	//print_edges(*mesh);

	validate_edge_invariants(mesh, "pre collapse");

	auto edge   = Edge(Edge_type::primal, 13); // 2 -> 4
	auto result = mesh.collapse(edge, 0.5f);
	//print_edges(*mesh);

	CHECK(result == Vertex(2));
	CHECK(mesh.vertex_used_count() == 6);    // V4 was removed
	CHECK(mesh.face_used_count() == 5);      // F2 and F3 got removed
	CHECK(mesh.edge_used_count() == 26 - 6); // 3 edges got removed

	validate_edge_invariants(mesh, "post collapse");
}

TEST_CASE("yggdrasill::Mesh_view: collapse (border edge A)")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F2 | F3  /|
	 *  |  \   |   /  |
	 *  |    \ | /    |
	 *  |     \|/     |
	 *  | F4   4  F5  |
	 *  |     / \     |
	 *  |    /   \    |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 *
	 * (6->5) =>
	 *
	 * // Faces removed: F6
	 * // Vertex removed: 6
	 * // Edges removed: 5->6, 4->6
	 *
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F2 | F3  /|
	 *  |  \   |   /  |
	 *  |    \ | /    |
	 *   \    \|/    /
	 *    \F4  4  F5/
	 *     \   |   /
	 *      \  |  /
	 *       \ | /
	 *        \|/
	 *         6
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(7);
	mesh.add_face(Vertex{0}, Vertex{1}, Vertex{2});
	mesh.add_face(Vertex{0}, Vertex{2}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{4}, Vertex{2});
	mesh.add_face(Vertex{2}, Vertex{4}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{5}, Vertex{4});
	mesh.add_face(Vertex{4}, Vertex{6}, Vertex{3});
	mesh.add_face(Vertex{4}, Vertex{5}, Vertex{6});

	//print_edges(*mesh);

	validate_edge_invariants(mesh, "pre collapse");

	auto edge   = Edge(Edge_type::primal, 25); // 6->5
	auto result = mesh.collapse(edge, 0.5f);
	//print_edges(*mesh);

	CHECK(result == Vertex(6));
	CHECK(mesh.vertex_used_count() == 6);    // V6 was removed
	CHECK(mesh.face_used_count() == 6);      // F6 got removed
	CHECK(mesh.edge_used_count() == 26 - 4); // 2 edges got removed

	validate_edge_invariants(mesh, "post collapse");
}

TEST_CASE("yggdrasill::Mesh_view: collapse (border edge B)")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F2 | F3  /|
	 *  |  \   |   /  |
	 *  |    \ | /    |
	 *  |     \|/     |
	 *  | F4   4  F5  |
	 *  |     / \     |
	 *  |    /   \    |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 *
	 * (5->6) =>
	 *
	 * // Faces removed: F6
	 * // Vertex removed: 6
	 * // Edges removed: 5->6, 4->6
	 *
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F2 | F3  /|
	 *  |  \   |   /  |
	 *  |    \ | /    |
	 *   \    \|/    /
	 *    \F4  4  F5/
	 *     \   |   /
	 *      \  |  /
	 *       \ | /
	 *        \|/
	 *         5
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(7);
	mesh.add_face(Vertex{0}, Vertex{1}, Vertex{2});
	mesh.add_face(Vertex{0}, Vertex{2}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{4}, Vertex{2});
	mesh.add_face(Vertex{2}, Vertex{4}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{5}, Vertex{4});
	mesh.add_face(Vertex{4}, Vertex{6}, Vertex{3});
	mesh.add_face(Vertex{4}, Vertex{5}, Vertex{6});

	//print_edges(*mesh);

	validate_edge_invariants(mesh, "pre collapse");

	auto edge   = Edge(Edge_type::primal, 24); // 5->6
	auto result = mesh.collapse(edge, 0.5f);
	//print_edges(*mesh);

	CHECK(result == Vertex(5));
	CHECK(mesh.vertex_used_count() == 6);    // V6 was removed
	CHECK(mesh.face_used_count() == 6);      // F6 got removed
	CHECK(mesh.edge_used_count() == 26 - 4); // 2 edges got removed

	validate_edge_invariants(mesh, "post collapse");
}


TEST_CASE("yggdrasill::Mesh_view: collapse (complex case A)")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 *             7
	 *            /|\
	 *           / | \
	 *          /  |  \
	 *         /F5 |F6 \
	 *        /    |    \
	 * 0-----1-----2-----3
	 * |    / \ F2 |\F3 /
	 * |F0 /   \   | \ /
	 * |  /     \  |  4----|
	 * | /  F1   \ |    F4 |
	 * |/         \|       |
	 * 5-----------6-------|
	 *
	 * (2->6) =>
	 *
	 * // Faces removed: F2 & F4
	 * // Vertex removed: 6
	 * // Edges removed: 6->4, 1->6, 2->6
	 *
	 *             7
	 *            /|\
	 *           / | \
	 *          /  |  \
	 *         /F5 |F6 \
	 *        /    |    \
	 * 0-----1-----2-----3
	 * |    /      |\ F3/
	 * |F0 /       | \ /
	 * |  /        |  4
	 * | /  F1    /
	 * |/        /
	 * 5--------/
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(8);
	mesh.add_face(Vertex{1}, Vertex{0}, Vertex{5});
	mesh.add_face(Vertex{5}, Vertex{6}, Vertex{1});
	mesh.add_face(Vertex{6}, Vertex{2}, Vertex{1});
	mesh.add_face(Vertex{4}, Vertex{3}, Vertex{2});
	mesh.add_face(Vertex{6}, Vertex{4}, Vertex{2});
	mesh.add_face(Vertex{1}, Vertex{2}, Vertex{7});
	mesh.add_face(Vertex{2}, Vertex{3}, Vertex{7});

	print_edges(mesh);

	validate_edge_invariants(mesh, "pre collapse");

	auto edge   = Edge(Edge_type::primal, 11); // 2 -> 6
	auto result = mesh.collapse(edge, 0.5f);
	// print_edges(*mesh);

	CHECK(result == Vertex(2));
	CHECK(mesh.vertex_used_count() == 7);    // V6 was removed
	CHECK(mesh.face_used_count() == 5);      // F2 and F4 got removed
	CHECK(mesh.edge_used_count() == 28 - 6); // 3 edges got removed

	validate_edge_invariants(mesh, "post collapse");
}

TEST_CASE("yggdrasill::Mesh_view: collapse (error case A)")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 * 0-----1-----2-----3
	 * |    / \ F2 |\F3 /
	 * |F0 /   \   | \ /
	 * |  /     \  |  4----|
	 * | /  F1   \ |    F4 |
	 * |/         \|       |
	 * 5-----------6-------|
	 *
	 * (2->6) =>
	 *
	 * // Faces removed: F2 & F4
	 * // Vertex removed: 6
	 * // Edges removed: 6->4, 1->6, 2->6
	 *
	 * 0-----1-----2-----3
	 * |    /      |\ F3/
	 * |F0 /       | \ /
	 * |  /        |  4
	 * | /  F1    /
	 * |/        /
	 * 5--------/
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(7);
	mesh.add_face(Vertex{1}, Vertex{0}, Vertex{5});
	mesh.add_face(Vertex{5}, Vertex{6}, Vertex{1});
	mesh.add_face(Vertex{6}, Vertex{2}, Vertex{1});
	mesh.add_face(Vertex{4}, Vertex{3}, Vertex{2});
	mesh.add_face(Vertex{6}, Vertex{4}, Vertex{2});

	//print_edges(*mesh);

	validate_edge_invariants(mesh, "pre collapse");

	auto edge = Edge(Edge_type::primal, 9); // 2 -> 6

	CHECK_THROWS_AS(mesh.collapse(edge, 0.5f), Mesh_corruption_exception);
	validate_edge_invariants(mesh, "post exception");
}

TEST_CASE("yggdrasill::Mesh_view: collapse (error case B)")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 * 0-----1-----2-----3
	 * |    / \ F2 |\F3 / \
	 * |F0 /   \   | \ / F5\
	 * |  /     \  |  4-----7
	 * | /  F1   \ |F4|
	 * |/         \|  /
	 * 5-----------6-/
	 *
	 * (2->4) =>
	 *
	 * // Faces removed: F3 & F4
	 * // Vertex removed: 4
	 * // Edges removed: 2->4, 6->4, 3->4, 2->3
	 *
	 * 0-----1-----2-----3
	 * |    / \ F2 | \F5 /
	 * |F0 /   \   |  \ /
	 * |  /     \  |   7
	 * | /  F1   \ |
	 * |/         \|
	 * 5-----------6
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(7);
	mesh.add_face(Vertex{1}, Vertex{0}, Vertex{5});
	mesh.add_face(Vertex{5}, Vertex{6}, Vertex{1});
	mesh.add_face(Vertex{6}, Vertex{2}, Vertex{1});
	mesh.add_face(Vertex{4}, Vertex{3}, Vertex{2});
	mesh.add_face(Vertex{6}, Vertex{4}, Vertex{2});

	validate_edge_invariants(mesh, "pre collapse");

	auto edge = Edge(Edge_type::primal, 18); // 2 -> 4

	CHECK_THROWS_AS(mesh.collapse(edge, 0.5f), Mesh_corruption_exception);
	validate_edge_invariants(mesh, "post exception");
}

TEST_CASE("yggdrasill::Mesh_view: collapse (error case C)")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 * 0-----1-----2-----3
	 * |    / \ F2 |\F3 /
	 * |F0 /   \   | \ /
	 * |  /     \  |  4----|
	 * | /  F1   \ |    F4 |
	 * |/         \|       |
	 * 5-----------6-------|
	 *
	 * (2->4) =>
	 *
	 * // Faces removed: F3 & F4
	 * // Vertex removed: 4
	 * // Edges removed: 2->4, 6->4, 3->4, 2->3
	 *
	 * 0-----1-----2     3
	 * |    / \ F2 |
	 * |F0 /   \   |
	 * |  /     \  |
	 * | /  F1   \ |
	 * |/         \|
	 * 5-----------6
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(7);
	mesh.add_face(Vertex{1}, Vertex{0}, Vertex{5});
	mesh.add_face(Vertex{5}, Vertex{6}, Vertex{1});
	mesh.add_face(Vertex{6}, Vertex{2}, Vertex{1});
	mesh.add_face(Vertex{4}, Vertex{3}, Vertex{2});
	mesh.add_face(Vertex{6}, Vertex{4}, Vertex{2});

	validate_edge_invariants(mesh, "pre collapse");

	auto edge = Edge(Edge_type::primal, 18); // 2 -> 4

	CHECK_THROWS_AS(mesh.collapse(edge, 0.5f), Mesh_corruption_exception);
	validate_edge_invariants(mesh, "post exception");
}

TEST_CASE("yggdrasill::Mesh_view: collapse (error case D)")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 * 0-----1-----2-----3
	 * |    / \ F2 |\F3 /
	 * |F0 /   \   | \ /
	 * |  /     \  |  4--\
	 * | /  F1   \ |F4|   \
	 * |/         \|  / F5 |
	 * 5-----------6-/-----7
	 *
	 * (2->4) =>
	 *
	 * // Faces removed: F3 & F4
	 * // Vertex removed: 4
	 * // Edges removed: 2->4, 6->4, 3->4, 2->3
	 *
	 * 0-----1-----2     3
	 * |    / \ F2 |\
	 * |F0 /   \   | \
	 * |  /     \  |  \
	 * | /  F1   \ | F5\
	 * |/         \|    \
	 * 5-----------6-----7
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(8);
	mesh.add_face(Vertex{1}, Vertex{0}, Vertex{5});
	mesh.add_face(Vertex{5}, Vertex{6}, Vertex{1});
	mesh.add_face(Vertex{6}, Vertex{2}, Vertex{1});
	mesh.add_face(Vertex{4}, Vertex{3}, Vertex{2});
	mesh.add_face(Vertex{6}, Vertex{4}, Vertex{2});
	mesh.add_face(Vertex{4}, Vertex{6}, Vertex{7});

	validate_edge_invariants(mesh, "pre collapse");

	auto edge = Edge(Edge_type::primal, 18); // 2 -> 4

	CHECK_THROWS_AS(mesh.collapse(edge, 0.5f), Mesh_corruption_exception);
	validate_edge_invariants(mesh, "post exception");
}


TEST_CASE("yggdrasill::Mesh_view: split (inner edge)")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F2 | F3  /|
	 *  |  \   |   /  |
	 *  |    \ | /    |
	 *  |     \|/     |
	 *  | F4   4  F5  |
	 *  |     / \     |
	 *  |    /   \    |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 *
	 * (2->4) =>
	 *
	 * // Faces added: F7 & F8
	 * // Vertex added: 7
	 * // Edges added: 2-7, 1-7, 3-7, 4-7
	 * // Edges removed: 2-4
	 *
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F7 | F8  /|
	 *  |\ \-\ | /--//|
	 *  | \    7    / |
	 *  |  \ F2|F3 /  |
	 *  |   \  |  /   |
	 *  |    \ | /    |
	 *  |     \|/     |
	 *  | F4   4  F5  |
	 *  |     / \     |
	 *  |    /   \    |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(7);
	mesh.add_face(Vertex{0}, Vertex{1}, Vertex{2});
	mesh.add_face(Vertex{0}, Vertex{2}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{4}, Vertex{2});
	mesh.add_face(Vertex{2}, Vertex{4}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{5}, Vertex{4});
	mesh.add_face(Vertex{4}, Vertex{6}, Vertex{3});
	mesh.add_face(Vertex{4}, Vertex{5}, Vertex{6});

	//print_edges(*mesh);

	validate_edge_invariants(mesh, "pre split");

	auto edge   = Edge(Edge_type::primal, 13); // 2 -> 4
	auto result = mesh.split(edge, 0.5f);
	//print_edges(*mesh);

	CHECK(result.origin(mesh) == Vertex(7));
	CHECK(result.dest(mesh) == Vertex(4));
	CHECK(mesh.vertex_used_count() == 8);
	CHECK(mesh.face_used_count() == 9);
	CHECK(mesh.edge_used_count() == 26 + 6); // 3 edges got added

	validate_edge_invariants(mesh, "post split");
}

TEST_CASE("yggdrasill::Mesh_view: split (inner edge inverse)")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F2 | F3  /|
	 *  |  \   |   /  |
	 *  |    \ | /    |
	 *  |     \|/     |
	 *  | F4   4  F5  |
	 *  |     / \     |
	 *  |    /   \    |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 *
	 * (4->2) =>
	 *
	 * // Faces added: F7 & F8
	 * // Vertex added: 7
	 * // Edges added: 2-7, 1-7, 3-7, 4-7
	 * // Edges removed: 2-4
	 *
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F7 | F8  /|
	 *  |\ \-\ | /--//|
	 *  | \    7    / |
	 *  |  \ F2|F3 /  |
	 *  |   \  |  /   |
	 *  |    \ | /    |
	 *  |     \|/     |
	 *  | F4   4  F5  |
	 *  |     / \     |
	 *  |    /   \    |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(7);
	mesh.add_face(Vertex{0}, Vertex{1}, Vertex{2});
	mesh.add_face(Vertex{0}, Vertex{2}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{4}, Vertex{2});
	mesh.add_face(Vertex{2}, Vertex{4}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{5}, Vertex{4});
	mesh.add_face(Vertex{4}, Vertex{6}, Vertex{3});
	mesh.add_face(Vertex{4}, Vertex{5}, Vertex{6});

	//print_edges(*mesh);

	validate_edge_invariants(mesh, "pre split");

	auto edge   = Edge(Edge_type::primal, 12); // 4 -> 2
	auto result = mesh.split(edge, 0.5f);
	//print_edges(*mesh);

	CHECK(result.origin(mesh) == Vertex(7));
	CHECK(result.dest(mesh) == Vertex(2));
	CHECK(mesh.vertex_used_count() == 8);
	CHECK(mesh.face_used_count() == 9);
	CHECK(mesh.edge_used_count() == 26 + 6); // 3 edges got added

	validate_edge_invariants(mesh, "post split");
}
TEST_CASE("yggdrasill::Mesh_view: split (no left face)")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F2 | F3  /|
	 *  |  \   |   /  |
	 *  |    \ | /    |
	 *  |     \|/     |
	 *  | F4   4  F5  |
	 *  |     / \     |
	 *  |    /   \    |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 *
	 * (3->6) =>
	 *
	 * // Faces added: F7
	 * // Vertex added: 7
	 * // Edges added: 4-7, 3-7, 6-7
	 * // Edges removed: 3-6
	 *
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F2 | F3  /|
	 *  |  \   |   /  |
	 *  |    \ | / F7 |
	 *  |     \|/     |
	 *  | F4   4------7
	 *  |     / \     |
	 *  |    /   \ F5 |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(7);
	mesh.add_face(Vertex{0}, Vertex{1}, Vertex{2});
	mesh.add_face(Vertex{0}, Vertex{2}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{4}, Vertex{2});
	mesh.add_face(Vertex{2}, Vertex{4}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{5}, Vertex{4});
	mesh.add_face(Vertex{4}, Vertex{6}, Vertex{3});
	mesh.add_face(Vertex{4}, Vertex{5}, Vertex{6});

	//print_edges(*mesh);

	validate_edge_invariants(mesh, "pre split");

	auto edge   = Edge(Edge_type::primal, 23); // 3 -> 6
	auto result = mesh.split(edge, 0.5f);
	//print_edges(*mesh);

	CHECK(result.origin(mesh) == Vertex(7));
	CHECK(result.dest(mesh) == Vertex(6));
	CHECK(mesh.vertex_used_count() == 8);
	CHECK(mesh.face_used_count() == 8);
	CHECK(mesh.edge_used_count() == 26 + 4); // 2 edges got added

	validate_edge_invariants(mesh, "post split");
}

TEST_CASE("yggdrasill::Mesh_view: split (no right face)")
{
	auto world  = World(Seed::from({42}));
	auto [mesh] = world.lock_mesh();

	/*
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F2 | F3  /|
	 *  |  \   |   /  |
	 *  |    \ | /    |
	 *  |     \|/     |
	 *  | F4   4  F5  |
	 *  |     / \     |
	 *  |    /   \    |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 *
	 * (6->3) =>
	 *
	 * // Faces added: F7
	 * // Vertex added: 7
	 * // Edges added: 4-7, 3-7, 6-7
	 * // Edges removed: 3-6
	 *
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F2 | F3  /|
	 *  |  \   |   /  |
	 *  |    \ | / F7 |
	 *  |     \|/     |
	 *  | F4   4------7
	 *  |     / \     |
	 *  |    /   \ F5 |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 */

	// add vertices and check if they are valid
	mesh.add_vertices(7);
	mesh.add_face(Vertex{0}, Vertex{1}, Vertex{2});
	mesh.add_face(Vertex{0}, Vertex{2}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{4}, Vertex{2});
	mesh.add_face(Vertex{2}, Vertex{4}, Vertex{3});
	mesh.add_face(Vertex{1}, Vertex{5}, Vertex{4});
	mesh.add_face(Vertex{4}, Vertex{6}, Vertex{3});
	mesh.add_face(Vertex{4}, Vertex{5}, Vertex{6});

	//print_edges(*mesh);

	validate_edge_invariants(mesh, "pre split");

	auto edge   = Edge(Edge_type::primal, 22); // 3 -> 6
	auto result = mesh.split(edge, 0.5f);
	//print_edges(*mesh);

	CHECK(result.origin(mesh) == Vertex(7));
	CHECK(result.dest(mesh) == Vertex(3));
	CHECK(mesh.vertex_used_count() == 8);
	CHECK(mesh.face_used_count() == 8);
	CHECK(mesh.edge_used_count() == 26 + 4); // 2 edges got added

	validate_edge_invariants(mesh, "post split");
}
