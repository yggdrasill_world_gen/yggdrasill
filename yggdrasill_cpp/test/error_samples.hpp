#pragma once

#include <yggdrasill/error.hpp>

[[maybe_unused]] static int YGDL_example_function_that_might_fail(yggdrasill::Error* error)
{
	return 0;
}

/// \def_example{error_handling_cpp}
[[maybe_unused]] static void error_handling_cpp()
{
	auto error = yggdrasill::Error{};

	int result = YGDL_example_function_that_might_fail(&error);
	if(!error) {
		std::cout << "Operation failed: " << error.message() << '\n';
		// ... handle the error ...
		return;
	}

	// ... use result ...
	std::cout << result << '\n';
}