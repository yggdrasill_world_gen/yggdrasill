#include <catch2/catch.hpp>

#include "dictionary_samples.hpp"

#include <yggdrasill/array.hpp>
#include <yggdrasill/dictionary.hpp>

#include <sstream>


TEST_CASE("yggdrasill::Dictionary: operator[]")
{
	auto dict = yggdrasill::Dictionary();

	{
		const auto& const_dict = dict;
		auto        radius     = const_dict["domain.radius"];
		REQUIRE(!radius.exists());
		CHECK_THROWS_AS(radius.get<float>(), yggdrasill::Type_mismatch_exception);
		CHECK_THROWS_AS(radius["test"], yggdrasill::Type_mismatch_exception);
	}

	{
		auto radius = dict["domain.radius"];
		REQUIRE(!radius.exists());
		REQUIRE((radius != 0));
		REQUIRE(!radius.exists());
		radius = 42;
		REQUIRE(radius.exists());
		REQUIRE((radius == 42));
	}

	{
		auto radius = dict["domain.radius"];
		REQUIRE(radius.exists());
		CHECK_THROWS_AS(std::as_const(radius).get<yggdrasill::Dictionary_ref>(),
		                yggdrasill::Type_mismatch_exception);
		radius = false;
		REQUIRE(std::as_const(radius).get<bool>() == false);
	}
}


TEST_CASE("yggdrasill::Dictionary: int8")
{
	auto dict = yggdrasill::Dictionary();

	dict["test"] = std::int8_t(42);
	CHECK(dict["test"] == 42);
	CHECK(dict["test"] != 0);
	CHECK(dict["test"] < 43);

	CHECK(dict["test2"].get<std::int8_t>(2) == 2);
	CHECK(dict["test2"] == 2);

	dict["test"] = false;
	CHECK(dict["test"] == false);

	dict["test2"].get<std::int8_t>(50, {.min = std::int8_t(10), .max = std::int8_t(20)});
	CHECK(dict["test2"].get<int>() == 10);
	CHECK(dict["test2"].get<float>() == 10);
}

TEST_CASE("yggdrasill::Dictionary: int32")
{
	auto dict = yggdrasill::Dictionary();

	dict["test"] = 42;
	CHECK(dict["test"] == 42);
	CHECK(dict["test"] != 0);
	CHECK(dict["test"] < 43);

	dict["test"].get(50, {.min = 10, .max = 20});
	CHECK(dict["test"].get<int>() == 20);
	CHECK(dict["test"].get<float>() == 20);
}

TEST_CASE("yggdrasill::Dictionary: int64")
{
	auto dict = yggdrasill::Dictionary();

	dict["test"] = std::int64_t(42);
	CHECK(dict["test"] == std::int64_t(42));
	CHECK(dict["test"] != 0);
	CHECK(dict["test"] < 43);

	dict["test"].get(50, {.min = 10, .max = 20});
	CHECK(dict["test"].get<int>() == 20);
	CHECK(dict["test"].get<float>() == 20);
}

TEST_CASE("yggdrasill::Dictionary: float")
{
	auto dict = yggdrasill::Dictionary();

	dict["test"] = 2.f;
	CHECK(dict["test"] == 2.f);
	CHECK(dict["test"] != 0);
	CHECK(dict["test"] < 4.0);

	dict["test"].get(1.f, {.min = -2.f, .max = 0.5f});
	CHECK(dict["test"].get<float>() == 0.5);
	CHECK(dict["test"].get<int>() == 0);
	CHECK(dict["test"].get<float>() == 0.0);
}

TEST_CASE("yggdrasill::Dictionary: double")
{
	auto dict = yggdrasill::Dictionary();

	dict["test"] = 2.0;
	CHECK(dict["test"] == 2.f);
	CHECK(dict["test"] != 0);
	CHECK(dict["test"] < 4.0);

	dict["test"].get(1.0, {.min = -2.f, .max = 0.5f});
	CHECK(dict["test"].get<float>() == 0.5);
	CHECK(dict["test"].get<int>() == 0);
	CHECK(dict["test"].get<float>() == 0);
}

TEST_CASE("yggdrasill::Dictionary: vec2")
{
	auto dict = yggdrasill::Dictionary();

	dict["test"] = yggdrasill::Vec2(1, 2);
	CHECK(dict["test"] == yggdrasill::Vec2(1, 2));

	dict.erase("test");

	dict["test"].get(yggdrasill::Vec2(3, 4));
	CHECK(dict["test"] == yggdrasill::Vec2(3, 4));
}

TEST_CASE("yggdrasill::Dictionary: vec3")
{
	auto dict = yggdrasill::Dictionary();

	dict["test"] = yggdrasill::Vec3(1, 2, 3);
	CHECK(dict["test"] == yggdrasill::Vec3(1, 2, 3));

	dict.erase("test");

	dict["test"].get(yggdrasill::Vec3(3, 4, 5));
	CHECK(dict["test"] == yggdrasill::Vec3(3, 4, 5));
}

TEST_CASE("yggdrasill::Dictionary: String")
{
	auto dict = yggdrasill::Dictionary();

	dict["test"] = "value";
	CHECK(dict["test"] == yggdrasill::String("value"));

	dict["test"].get("new_value");
	CHECK(dict["test"] == yggdrasill::String("value"));

	dict.erase("test");

	dict["test"].get("new_value");
	CHECK(dict["test"] == yggdrasill::String("new_value"));
}

TEST_CASE("yggdrasill::Dictionary: nested")
{
	using namespace yggdrasill;

	auto points = std::vector<Vec2>{Vec2{1, 2}, Vec2{3, 4}, Vec2{5, 6}};

	auto inner_dict1   = Dictionary();
	inner_dict1["foo"] = "bar";

	auto inner_dict2   = Dictionary();
	inner_dict2["foo"] = "baz";

	auto  array_dicts = std::vector<Dictionary>();
	auto& e1          = array_dicts.emplace_back();
	e1["v1"]          = 1.f;
	e1["v2"]          = 2.f;
	auto& e2          = array_dicts.emplace_back();
	e2["v1"]          = 21.f;
	e2["v2"]          = 22.f;

	REQUIRE(array_dicts[0].keys() == std::initializer_list<std::string>{"v1", "v2"});
	REQUIRE(array_dicts[1].keys() == std::initializer_list<std::string>{"v1", "v2"});

	auto   dict          = Dictionary();
	auto&& inner         = dict["test"].get<Dictionary_ref>();
	inner["float"]       = 1.f;
	inner["bool"]        = true;
	inner["points"]      = points;
	inner["inner_dict1"] = inner_dict1;
	inner["inner_dict2"] = std::move(inner_dict2);
	inner["dicts"]       = array_dicts;

	REQUIRE(c_ptr(inner["inner_dict1"].get<Dictionary_view>()) != c_ptr(inner_dict1));
	REQUIRE(c_ptr(inner["inner_dict2"].get<Dictionary_view>()) != c_ptr(inner_dict2));
	auto array_dicts_inner = inner["dicts"].get<Array_view<Dictionary_view>>();
	for(auto i = 0; i < array_dicts_inner.size(); ++i) {
		REQUIRE(c_ptr(array_dicts_inner[i]) != c_ptr(array_dicts[i]));
	}

	REQUIRE(!inner_dict2["foo"].exists()); // should have been moved from NOLINT(bugprone-use-after-move)
	REQUIRE(inner_dict1["foo"] == "bar");
	REQUIRE(array_dicts[0]["v1"] == 1.f);

	// validate that data has been written correctly
	REQUIRE(dict["test"]["float"] == 1.f);
	REQUIRE(dict["test"]["bool"].get<bool>() == true);
	REQUIRE(dict["test"]["points"].get<Array_ref<Vec2>>().size() == 3);
	REQUIRE(dict["test"]["points"].get<Array_ref<Vec2>>()[0] == Vec2{1, 2});
	REQUIRE(dict["test"]["points"].get<Array_ref<Vec2>>()[1] == Vec2{3, 4});
	REQUIRE(dict["test"]["points"].get<Array_ref<Vec2>>()[2] == Vec2{5, 6});

	REQUIRE(dict["test"]["inner_dict1"]["foo"] == "bar");
	REQUIRE(dict["test"]["inner_dict2"]["foo"] == "baz");

	REQUIRE(dict["test"]["dicts"].get<Array_ref<Const_dictionary_view>>().size() == 2);
	REQUIRE(dict["test"]["dicts"].get<Array_ref<Dictionary_view>>()[0]["v1"] == 1.f);
	REQUIRE(dict["test"]["dicts"].get<Array_ref<Dictionary_ref>>()[0]["v2"] == 2.f);
	REQUIRE(dict["test"]["dicts"].get<Array_ref<Dictionary_ref>>()[1]["v1"] == 21.f);
	REQUIRE(dict["test"]["dicts"].get<Array_ref<Dictionary_ref>>()[1]["v2"] == 22.f);
}

TEST_CASE("yggdrasill::Dictionary: keys")
{
	auto dict1               = yggdrasill::Dictionary();
	dict1["value1"]          = true;
	dict1["value2"]          = false;
	dict1["inner"]["value1"] = 1;
	dict1["inner"]["value2"] = 2;

	CHECK(dict1.keys() == std::initializer_list<std::string>{"value1", "value2", "inner"});
}
