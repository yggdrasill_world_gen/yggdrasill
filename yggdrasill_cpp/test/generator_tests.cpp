#include <catch2/catch.hpp>

#include "generator_samples.hpp"

#include <yggdrasill/generator.hpp>


namespace {
	void execute(yggdrasill::World_view, yggdrasill::Any_dictionary auto) {}
} // namespace

TEST_CASE("yggdrasill::Generator")
{
	auto gen    = yggdrasill::Generator();
	auto module = gen.add(yggdrasill::to_module_info<execute, execute>("my_module"));

	CHECK(gen.get("my_module"));
	CHECK(!gen.get("not a module"));

	auto world = yggdrasill::World(yggdrasill::Seed::from({42}));
	auto dict  = yggdrasill::Dictionary{};
	module.execute(world, dict);
}

TEST_CASE("yggdrasill::Generator::load")
{
	try {
		auto gen    = yggdrasill::Generator();
		auto loaded = gen.load("test_modules");
		REQUIRE(loaded.size() == 1);
		auto module = loaded[0];

		CHECK(gen.get("test_module"));
		CHECK(gen.get("test_module")->id() == "test_module");

		auto list = gen.list();
		CHECK(list.size() == 1);
		CHECK(list[0].id() == "test_module");

		auto world = yggdrasill::World(yggdrasill::Seed::from({42}));
		auto dict  = yggdrasill::Dictionary{};
		CHECK(module.validate(dict).success());
		module.execute(world, dict);

	} catch(yggdrasill::Exception& e) {
		std::cerr << "Exception [" << e.code() << "]: " << e.what();
		throw;
	}
}

TEST_CASE("yggdrasill::Generator::reload")
{
	try {
		auto gen = yggdrasill::Generator();
		gen.load("test_modules");
		gen.reload();

	} catch(yggdrasill::Exception& e) {
		std::cerr << "Exception [" << e.code() << "]: " << e.what();
		throw;
	}
}
