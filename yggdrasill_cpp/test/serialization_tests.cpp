#include <catch2/catch.hpp>

#include "serialization_samples.hpp"

#include <yggdrasill/serialization.hpp>


using namespace yggdrasill;

static bool equal(std::span<std::byte> lhs, std::span<std::byte> rhs)
{
	return std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

TEST_CASE("yggdrasill::Serializer/Deserializer: userdata")
{
	if(!yggdrasill::serialization_supported())
		return; // skip test if we are building without serialization support

	const auto path = std::filesystem::temp_directory_path() / "yggdrasill_serializer_cpp_userdata.zip";
	std::filesystem::remove(path);

	auto serializer = Serializer::open_overwrite(path);

	auto userdata = std::vector<std::byte>();
	userdata.resize(1024);
	std::generate(userdata.begin(), userdata.end(), [i = std::byte(0)]() mutable {
		return i = static_cast<std::byte>(std::to_integer<uint8_t>(i) + 1u);
	});

	serializer.write_userdata("test_1", userdata);
	serializer.write_userdata("test_2", std::span(userdata).subspan(512));

	const auto read_test_1 = serializer.read_userdata("test_1");
	CHECK(read_test_1 == userdata);

	auto read_test_2 = std::vector<std::byte>();
	serializer.read_userdata("test_2", [&](Byte_reader& reader) {
		CHECK(reader.remaining_bytes() == 512);
		read_test_2.resize(512 + 8);
		auto read_span = std::span(read_test_2).subspan(0, 16);
		CHECK(equal(reader.read_into(read_span), read_span));

		CHECK(reader.remaining_bytes() == 512 - 16);
		auto remainder      = std::span(read_test_2).subspan(16);
		auto read_remainder = reader.read_into(remainder);
		CHECK(read_remainder.size() == 512 - 16);
		read_test_2.resize(512);
	});
	CHECK(equal(std::span(read_test_2), std::span(userdata).subspan(512)));

	auto keys = std::vector<std::string>();
	serializer.foreach_userdata([&](std::string_view key) { keys.emplace_back(key); });
	CHECK(keys.size() == 2);
	CHECK(std::count(keys.begin(), keys.end(), std::string_view("test_1")) == 1);
	CHECK(std::count(keys.begin(), keys.end(), std::string_view("test_2")) == 1);

	serializer.close();
	CHECK(std::filesystem::exists(path));
	std::filesystem::remove(path);
}

static Dictionary build_test_dict()
{
	auto  array_dicts = std::vector<Dictionary>();
	auto& e1          = array_dicts.emplace_back();
	e1["v1"]          = 1.f;
	e1["v2"]          = 2.f;
	auto& e2          = array_dicts.emplace_back();
	e2["v1"]          = 21.f;
	e2["v2"]          = 22.f;

	REQUIRE(array_dicts[0].keys() == std::initializer_list<std::string>{"v1", "v2"});
	REQUIRE(array_dicts[1].keys() == std::initializer_list<std::string>{"v1", "v2"});

	auto dict           = Dictionary();
	dict["int8"]        = int8_t(42);
	dict["int32"]       = int32_t(123);
	dict["float"]       = 42.1f;
	dict["vec3"]        = Vec3(1, 2, 3);
	auto&& inner        = dict["dict"].get<Dictionary_ref>();
	inner["bool"]       = true;
	inner["dict_array"] = std::move(array_dicts);

	auto inner_array = inner["dict_array"].get<Array_view<Dictionary_view>>();
	REQUIRE(inner_array[0].keys() == std::initializer_list<std::string>{"v1", "v2"});
	REQUIRE(inner_array[1].keys() == std::initializer_list<std::string>{"v1", "v2"});

	return dict;
}

TEST_CASE("yggdrasill::Serializer/Deserializer: config")
{
	if(!yggdrasill::serialization_supported())
		return; // skip test if we are building without serialization support

	const auto path = std::filesystem::temp_directory_path() / "yggdrasill_serializer_cpp_config.zip";
	std::filesystem::remove(path);

	auto serializer = Serializer::open_overwrite(path);

	const auto dict = build_test_dict();

	serializer.write_config("test_1", dict);

	const auto dict_read = serializer.read_config("test_1");

	// validate that the read data is identical
	CHECK(dict_read.keys() == std::initializer_list<std::string>{"int8", "int32", "float", "vec3", "dict"});

	CHECK(dict_read["int8"].type() == Type::int8_t);
	CHECK(dict_read["int8"] == 42);

	CHECK(dict_read["int32"].type() == Type::int32_t);
	CHECK(dict_read["int32"] == 123);

	CHECK(dict_read["float"].type() == Type::float_t);
	CHECK(dict_read["float"] == dict["float"].get<float>());

	CHECK(dict_read["vec3"].type() == Type::vec3_t);
	CHECK(dict_read["vec3"] == dict["vec3"].get<Vec3>());

	CHECK(dict_read["dict"].type() == Type::dict_t);
	CHECK(dict_read["dict"].get<Const_dictionary_view>().keys()
	      == std::initializer_list<std::string>{"bool", "dict_array"});

	CHECK(dict_read["dict"]["bool"].type() == Type::bool_t);
	CHECK(dict_read["dict"]["bool"] == true);

	CHECK(dict_read["dict"]["dict_array"].type() == Type::array_t);
	auto org_array = dict["dict"]["dict_array"].get<Const_array_view<Const_dictionary_view>>();
	auto array     = dict_read["dict"]["dict_array"].get<Const_array_view<Const_dictionary_view>>();
	CHECK(array.size() == 2);
	CHECK(array[0].keys() == std::initializer_list<std::string>{"v1", "v2"});

	CHECK(array[0]["v1"].type() == Type::float_t);
	CHECK(array[0]["v1"] == org_array[0]["v1"].get<float>());

	CHECK(array[0]["v2"].type() == Type::float_t);
	CHECK(array[0]["v2"] == org_array[0]["v2"].get<float>());

	CHECK(array[1]["v1"].type() == Type::float_t);
	CHECK(array[1]["v1"] == org_array[1]["v1"].get<float>());

	CHECK(array[1]["v2"].type() == Type::float_t);
	CHECK(array[1]["v2"] == org_array[1]["v2"].get<float>());

	serializer.close();
	CHECK(std::filesystem::exists(path));
	std::filesystem::remove(path);
}


static constexpr auto layer_a_def =
        yggdrasill::Layer_definition<std::int32_t, yggdrasill::Ref_type::vertex>("example_layer_a")
                .description("Description of my example layer")
                .min(-128)
                .initial(2)
                .max(128);

static constexpr auto layer_b_def =
        yggdrasill::Layer_definition<float, yggdrasill::Ref_type::edge_primal>("example_layer_b")
                .initial(2)
                .max(std::numeric_limits<float>::infinity())
                .allow_inf(true);

static yggdrasill::World create_world()
{

	constexpr auto seed = yggdrasill::Seed::from({
	        2148334203,
	        346516426,
	        1904038130,
	        3887960964,
	});

	auto world = yggdrasill::World(seed);

	auto rand = world.random();
	(void) rand.uniform<100>(0, 10);

	auto [mesh] = world.lock_mesh();
	mesh.add_vertices(4);
	mesh.add_face(yggdrasill::Vertex{0}, yggdrasill::Vertex{1}, yggdrasill::Vertex{2});
	mesh.add_face(yggdrasill::Vertex{2}, yggdrasill::Vertex{3}, yggdrasill::Vertex{0});

	auto [layer] = world.lock_unstructured_layer("test");
	layer["val"] = 42;

	auto [layer_a, layer_b] = world.lock_layer(layer_a_def, layer_b_def);
	layer_a[Vertex{0}]      = 0;
	layer_a[Vertex{1}]      = 1;
	layer_a[Vertex{2}]      = 2;

	layer_b[mesh.find_edge(Vertex{0}, Vertex{1})] = 1.123f;
	layer_b[mesh.find_edge(Vertex{1}, Vertex{2})] = std::numeric_limits<float>::infinity();
	layer_b[mesh.find_edge(Vertex{2}, Vertex{0})] = -0.01f;

	return world;
}
static void compare_worlds(yggdrasill::World& world, yggdrasill::World& read_world)
{
	CHECK(world.random().uniform(0, 100) == read_world.random().uniform(0, 100));
	CHECK((*world.unstructured_layer("test"))["val"].get<int>() == (*world.unstructured_layer("test"))["val"]);

	CHECK(world.mesh().vertex_max_index() == read_world.mesh().vertex_max_index());
	CHECK(world.mesh().vertex_used_count() == read_world.mesh().vertex_used_count());
	CHECK(world.mesh().face_max_index() == read_world.mesh().face_max_index());
	CHECK(world.mesh().face_used_count() == read_world.mesh().face_used_count());
	CHECK(world.mesh().edge_max_index() == read_world.mesh().edge_max_index());
	CHECK(world.mesh().edge_used_count() == read_world.mesh().edge_used_count());

	CHECK(world.mesh().vertices() == read_world.mesh().vertices());
	CHECK(world.mesh().faces() == read_world.mesh().faces());
	CHECK(world.mesh().directed_edges<Edge_type::primal>()
	      == read_world.mesh().directed_edges<Edge_type::primal>());

	auto [layer_a, layer_b]           = world.required_layer(layer_a_def, layer_b_def);
	auto [read_layer_a, read_layer_b] = read_world.required_layer(layer_a_def, layer_b_def);

	for(auto v : world.mesh().vertices()) {
		CHECK(v.edge(world.mesh()) == v.edge(read_world.mesh()));
		CHECK(v.edge(world.mesh()).origin_next(world.mesh())
		      == v.edge(read_world.mesh()).origin_next(read_world.mesh()));

		CHECK(layer_a[v] == read_layer_a[v]);
	}

	for(auto e : world.mesh().directed_edges<Edge_type::primal>()) {
		CHECK(layer_b[e] == read_layer_b[e]);
	}
}

TEST_CASE("yggdrasill::Serializer/Deserializer: single world")
{
	if(!YGDL_serialization_supported())
		return; // skip test if we are building without serialization support

	const auto path = std::filesystem::temp_directory_path() / "yggdrasill_serializer_cpp_world.zip";
	std::filesystem::remove(path);

	auto serializer = Serializer::open_overwrite(path);

	auto world = create_world();

	auto handle = serializer.write_world(world);

	serializer.flush();

	auto read_world = serializer.read_world(handle);
	compare_worlds(world, read_world);

	serializer.close();
	CHECK(std::filesystem::exists(path));
	std::filesystem::remove(path);
}

TEST_CASE("yggdrasill::Serializer/Deserializer: multiple worlds")
{
	if(!YGDL_serialization_supported())
		return; // skip test if we are building without serialization support

	const auto path = std::filesystem::temp_directory_path() / "yggdrasill_serializer_cpp_world.zip";
	std::filesystem::remove(path);

	auto serializer = Serializer::open_overwrite(path);

	auto world_1  = create_world();
	auto handle_1 = serializer.write_world(world_1);

	auto world_2 = world_1;
	{
		auto [mesh] = world_2.lock_mesh();
		mesh.flip(mesh.find_edge(Vertex{2}, Vertex{0}));

		auto [layer_a]     = world_2.lock_layer(layer_a_def);
		layer_a[Vertex{0}] = 123;
	}

	auto handle_2 = serializer.write_world(world_2);

	serializer.flush();

	auto read_world_1 = serializer.read_world(handle_1);
	compare_worlds(world_1, read_world_1);

	auto read_world_2 = serializer.read_world(handle_2);
	compare_worlds(world_2, read_world_2);

	// unchanged layers should deserialize to the same object
	CHECK(c_ptr(*read_world_1.unstructured_layer("test")) == c_ptr(*read_world_2.unstructured_layer("test")));

	serializer.close();

	CHECK(std::filesystem::exists(path));
	std::filesystem::remove(path);
}
