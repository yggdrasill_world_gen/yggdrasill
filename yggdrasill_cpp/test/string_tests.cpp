#include <catch2/catch.hpp>

#include <yggdrasill/string.hpp>

#include <sstream>

using namespace std::literals;

TEST_CASE("yggdrasill::String: create, access, destroy")
{
	constexpr const auto str256_content =
	        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt "
	        "ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo "
	        "dolores et ea rebum. Stet clita kasd gubergren, no sea takimata ";

	auto null_str      = yggdrasill::String();
	auto empty_str     = yggdrasill::String(""sv);
	auto small_str_1   = yggdrasill::String("a"sv);
	auto small_str_15  = yggdrasill::String("abcdefghijklmno");
	auto large_str_16  = yggdrasill::String("abcdefghijklmnop"sv);
	auto large_str_256 = yggdrasill::String(str256_content);
	auto zero_str      = yggdrasill::String("abc\0de"sv);
	auto sub_str       = yggdrasill::String(("abcdefghijklmnop"sv).substr(0, 6));

	REQUIRE(null_str.size() == 0);
	REQUIRE(empty_str.size() == 0);
	REQUIRE(small_str_1.size() == 1);
	REQUIRE(small_str_15.size() == 15);
	REQUIRE(large_str_16.size() == 16);
	REQUIRE(large_str_256.size() == 256);
	REQUIRE(zero_str.size() == 6);
	REQUIRE(sub_str.size() == 6);

	REQUIRE(null_str == "");
	REQUIRE(empty_str == "");
	REQUIRE(small_str_1 == "a");
	REQUIRE(small_str_15 == "abcdefghijklmno");
	REQUIRE(large_str_16 == "abcdefghijklmnop");
	REQUIRE(large_str_256 == std::string_view(str256_content));
	REQUIRE(zero_str == "abc\0de"sv);
	REQUIRE(sub_str == "abcdef");

	REQUIRE(std::strcmp(null_str.data(), "") == 0);
	REQUIRE(std::strcmp(empty_str.data(), "") == 0);
	REQUIRE(std::strcmp(small_str_1.data(), "a") == 0);
	REQUIRE(std::strcmp(small_str_15.data(), "abcdefghijklmno") == 0);
	REQUIRE(std::strcmp(large_str_16.data(), "abcdefghijklmnop") == 0);
	REQUIRE(std::strcmp(large_str_256.data(), str256_content) == 0);
	REQUIRE(std::memcmp(zero_str.data(), "abc\0de", 6) == 0);
	REQUIRE(std::strcmp(sub_str.data(), "abcdef") == 0);
}

TEST_CASE("yggdrasill::String: assingment")
{
	auto str = yggdrasill::String();

	str = "a";
	REQUIRE(str.size() == 1);
	REQUIRE(str == "a");

	str = YGDL_string_view_create("abcdefghijklmno");
	REQUIRE(str.size() == 15);
	REQUIRE(str == "abcdefghijklmno");

	str = YGDL_string_create(YGDL_string_view_create("abcdefghijklmnop"));
	REQUIRE(str.size() == 16);
	REQUIRE(str == "abcdefghijklmnop");

	auto& tmp = str;
	str       = tmp;
	REQUIRE(str.size() == 16);
	REQUIRE(str == "abcdefghijklmnop");

	auto& tmp2 = str;
	str        = std::move(tmp2);
	REQUIRE(str.size() == 16);
	REQUIRE(str == "abcdefghijklmnop");

	auto str2 = yggdrasill::String("abc");
	str       = str2;
	REQUIRE(str.size() == 3);
	REQUIRE(str == "abc");

	str = std::string_view("a");
	REQUIRE(str.size() == 1);
	REQUIRE(str == "a");

	str = "abc\0de"sv;
	REQUIRE(str.size() == 6);
	REQUIRE(str == "abc\0de"sv);

	str = YGDL_String_view{nullptr, -1};
	REQUIRE(str.size() == 0);
	REQUIRE(str == "");
}

TEST_CASE("yggdrasill::String: stream")
{
	auto str      = yggdrasill::String("abcdefgh");
	auto str_view = "abcdefgh"sv;

	auto stream = std::stringstream();
	stream << str;
	CHECK(stream.str() == str);

	stream = std::stringstream();
	stream << str_view;
	CHECK(stream.str() == str_view);

	auto str_out = yggdrasill::String();
	stream       = std::stringstream(str.std_str());
	stream >> str_out;
	CHECK(str_out == str);
}

TEST_CASE("yggdrasill::String: append")
{
	auto str_a = yggdrasill::String("abc");

	auto str = str_a + "def";
	REQUIRE(str == "abcdef");

	str = "def" + str_a;
	REQUIRE(str == "defabc");

	str = str_a + "def"sv;
	REQUIRE(str == "abcdef");

	str_a += "def";
	REQUIRE(str_a == "abcdef");
}
