#include <catch2/catch.hpp>

#include "error_samples.hpp" // include to ensure that it compiles

#include <yggdrasill/error.hpp>


TEST_CASE("yggdrasill::Error and yggdrasill::Error_throw")
{
	const auto success = [](YGDL_Error*) {};
	const auto failure = [](YGDL_Error* error) {
		yggdrasill::set_error(error, 42, [] { return yggdrasill::String("My test error message"); });
	};

	auto error = yggdrasill::Error();
	success(c_ptr(error));
	CHECK(error.success());

	failure(c_ptr(error));
	CHECK(!error.success());
	CHECK(error.code() == 42);
	CHECK(error.message() == "My test error message");


	success(yggdrasill::Error_throw());
	CHECK_THROWS_AS(failure(yggdrasill::Error_throw()), yggdrasill::Exception);
}
