#include <catch2/catch.hpp>

#include "world_samples.hpp"

#include <yggdrasill/iterator.hpp>
#include <yggdrasill/layer.hpp>
#include <yggdrasill/world.hpp>

TEST_CASE("yggdrasill::World")
{
	YGDL_set_warning_log_handler(+[](const char* msg) { FAIL("Unexpected warning: " << msg); });

	auto world = yggdrasill::World(yggdrasill::Seed::from({42}));

	{
		auto [mesh] = world.lock_mesh();
		REQUIRE(mesh.vertex_max_index() == -1);
		mesh.add_vertices(1);
		REQUIRE(mesh.vertex_max_index() == 0);

		static auto warning = false;
		warning             = false;
		YGDL_set_warning_log_handler(+[](const char* msg) { warning = true; });

		auto world_copy  = world;
		auto [copy_mesh] = world_copy.lock_mesh();
		REQUIRE(copy_mesh.vertex_max_index() == 0);

		YGDL_set_warning_log_handler(nullptr);
		CHECK(warning);

		mesh.add_vertices(1);
		REQUIRE(mesh.vertex_max_index() == 1);
		REQUIRE(copy_mesh.vertex_max_index() == 0);
	}

	REQUIRE(world.mesh().vertex_max_index() == 1);

	auto world_copy  = world;
	auto [copy_mesh] = world.lock_mesh();
	REQUIRE(copy_mesh.vertex_max_index() == 1);
	copy_mesh.add_vertices(1);
	REQUIRE(copy_mesh.vertex_max_index() == 2);
}

TEST_CASE("yggdrasill::World random API")
{
	YGDL_set_warning_log_handler(+[](const char* msg) { FAIL("Unexpected warning: " << msg); });

	const auto seed  = yggdrasill::Seed::from({
            2148334203,
            346516426,
            1904038130,
            3887960964,
    });
	auto       world = yggdrasill::World(seed);

	SECTION("RNG is portable")
	{
		auto rand = world.random();
		CHECK(rand.uniform(0.f, 1.f) == Approx(0x1.883498p-3));
		CHECK(rand.uniform(0, 10) == 1);
		CHECK(rand.probability(0.5f) == true);
	}

	SECTION("Same seed generates same results")
	{
		auto rand = world.random();
		rand.seed(seed);
		const auto v1 = rand.uniform(0.f, 100.f);
		const auto v2 = rand.uniform(0, 100);
		const auto v3 = rand.probability(0.5f);

		rand.seed(seed);
		CHECK(rand.uniform(0.f, 100.f) == Approx(v1));
		CHECK(rand.uniform(0, 100) == v2);
		CHECK(rand.probability(0.5f) == v3);
	}

	SECTION("RNG state is copied correctly")
	{
		// prepare RNG
		{
			auto rand = world.random();
			rand.seed(seed);
			(void) rand.uniform<100>(0, 10);
		}

		auto world_copy = world;
		auto rand_1     = world.random();
		auto rand_2     = world_copy.random();

		const auto v1 = rand_1.uniform<10>(0, 10);
		const auto v2 = rand_2.uniform<10>(0, 10);

		CHECK(v1 == v2);
	}

	SECTION("Generate ints")
	{
		auto rand = world.random();
		rand.seed(seed);

		auto ints = std::vector<int32_t>();
		ints.resize(3);
		rand.uniform(-10, 10, ints.begin(), ints.end());
		CHECK(ints[0] == -6);
		CHECK(ints[1] == -8);
		CHECK(ints[2] == -7);

		auto&& [i1, i2, i3] = rand.uniform<3>(0, 10);
		CHECK(i1 == 10);
		CHECK(i2 == 1);
		CHECK(i3 == 2);

		auto&& [f1, f2] = rand.uniform<2>(0.f, 1.f);
		CHECK(f1 == Approx(0x1.24cebp-1));
		CHECK(f2 == Approx(0x1.732304p-3));
	}

	SECTION("Generate bools")
	{
		auto rand = world.random();
		rand.seed(seed);

		auto&& [b1, b2, b3, b4, b5] = rand.probability<5>(0.5f);
		CHECK(b1 == YGDL_TRUE);
		CHECK(b2 == YGDL_TRUE);
		CHECK(b3 == YGDL_TRUE);
		CHECK(b4 == YGDL_FALSE);
		CHECK(b5 == YGDL_TRUE);
	}

	SECTION("Normal distribution")
	{
		auto rand = world.random();
		rand.seed(seed);

		CHECK(rand.normal(0.f, 10.f) == Approx(-0x1.87451cp+0));
		CHECK(rand.normal(0.f, 10.f) == Approx(-0x1.46fde8p+0));
		CHECK(rand.normal(0.f, 10.f) == Approx(0x1.c599dcp+1));
		CHECK(rand.normal(0.f, 10.f) == Approx(-0x1.455824p+3));
	}
}

TEST_CASE("yggdrasill::Layer")
{
	constexpr auto layer_def =
	        yggdrasill::Layer_definition<std::int32_t, yggdrasill::Ref_type::vertex>("example_layer")
	                .description("Description of my example layer")
	                .min(-128)
	                .initial(2)
	                .max(128);

	auto world = yggdrasill::World(yggdrasill::Seed::from({42}));

	CHECK(!world.layer(layer_def));

	{
		auto [layer] = world.lock_layer(layer_def);
		CHECK(layer.size() == 0);
	}

	{
		auto [mesh] = world.lock_mesh();
		mesh.add_vertices(3);
		mesh.add_face(yggdrasill::Vertex{0}, yggdrasill::Vertex{1}, yggdrasill::Vertex{2});
	}

	REQUIRE(world.layer(layer_def));
	CHECK(world.layer(layer_def)->size() == 3);
	CHECK(world.required_layer(layer_def)[yggdrasill::Vertex{0}] == 2);

	REQUIRE_THROWS_AS(
	        [&] {
		        auto [sb_layer]                   = world.lock_layer(layer_def);
		        sb_layer[yggdrasill::Vertex{999}] = 2;
	        }(),
	        yggdrasill::Out_of_bounds_exception);

	REQUIRE_THROWS_AS(
	        [&] {
		        auto [sb_layer]                 = world.lock_layer(layer_def);
		        sb_layer[yggdrasill::Vertex{0}] = 200;
	        }(),
	        yggdrasill::Precondition_violation_exception);

	auto layer_count = 0;
	world.foreach_layer([&](auto layer) {
		layer_count++;
		CHECK(layer.size() == 3);
		CHECK(layer.definition().id() == "example_layer");
		CHECK(layer.definition().data_type() == yggdrasill::Type::int32_t);
		CHECK(layer.definition().ref_type == yggdrasill::Ref_type::vertex);
	});
	CHECK(layer_count == 1);

	world.remove_layer(layer_def);
	CHECK(!world.layer(layer_def));
}

TEST_CASE("yggdrasill::Layer Invalidation")
{
	using namespace yggdrasill;

	constexpr auto keep_layer =
	        Layer_definition<std::int32_t, Ref_type::face>("keep").initial(0).invalidation(Invalidation::keep);

	constexpr auto remove_layer =
	        Layer_definition<std::int32_t, Ref_type::edge_dual>("remove").initial(0).invalidation(
	                Invalidation::remove_layer);

	constexpr auto reset_layer =
	        Layer_definition<std::int32_t, Ref_type::edge_primal_directed>("reset").initial(0).invalidation(
	                Invalidation::reset_affected);

	constexpr auto reset_all_layer =
	        Layer_definition<std::int32_t, Ref_type::edge_primal_directed>("reset_all")
	                .initial(0)
	                .invalidation(Invalidation::reset_all);


	auto world = World(Seed::from({42}));

	/*
	 * 0------1
	 * | F0  /|
	 * |   /  |
	 * | / F1 |
	 * 2------3
	 *
	 */
	{
		auto [mesh] = world.lock_mesh();
		mesh.add_vertices(4);
		mesh.add_face(Vertex{0}, Vertex{2}, Vertex{1});
		mesh.add_face(Vertex{2}, Vertex{3}, Vertex{1});
	}

	{
		auto [layer] = world.lock_layer(keep_layer);
		CHECK(layer.size() == 2);
		layer[Face{0}] = 42;
	}

	{
		auto [layer] = world.lock_layer(remove_layer);
		CHECK(layer.size() == 5);
		layer[Edge{Edge_type::dual, 1}] = 42;
	}

	{
		auto [layer] = world.lock_layer(reset_layer);
		CHECK(layer.size() == 10);
		layer[Edge{Edge_type::primal, 0}] = 23;
		layer[Edge{Edge_type::primal, 2}] = 42;
	}

	{
		auto [layer] = world.lock_layer(reset_all_layer);
		CHECK(layer.size() == 10);
		layer[Edge{Edge_type::primal, 0}] = 23;
		layer[Edge{Edge_type::primal, 2}] = 42;
	}

	{
		auto [mesh] = world.lock_mesh();
		mesh.flip(Edge{Edge_type::primal, 2});
	}

	CHECK(!world.layer(remove_layer));
	CHECK(world.required_layer(keep_layer)[Face{0}] == 42);
	CHECK(world.required_layer(reset_layer)[Edge{Edge_type::primal, 0}] == 23);
	CHECK(world.required_layer(reset_layer)[Edge{Edge_type::primal, 2}] == 0);
	CHECK(world.required_layer(reset_all_layer)[Edge{Edge_type::primal, 0}] == 0);
	CHECK(world.required_layer(reset_all_layer)[Edge{Edge_type::primal, 2}] == 0);
}


TEST_CASE("yggdrasill::Layer interpolation (collapse)")
{
	using namespace yggdrasill;

	auto world = World(Seed::from({42}));

	// construct mesh
	{
		auto [mesh] = world.lock_mesh();

		/*
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  |\  F2 | F3  /|
	 *  |  \   |   /  |
	 *  |    \ | /    |
	 *  |     \|/     |
	 *  | F4   4  F5  |
	 *  |     / \     |
	 *  |    /   \    |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 *
	 * (2->4) =>
	 *
	 *         0
	 *       / | \
	 *     /   |   \
	 *   /  F0 | F1  \
	 *  1------2------3
	 *  | F4  / \ F5  |
	 *  |    /   \    |
	 *  |  /  F6   \  |
	 *  |/           \|
	 *  5-------------6
	 */

		// add vertices and check if they are valid
		mesh.add_vertices(7);
		mesh.add_face(Vertex{0}, Vertex{1}, Vertex{2});
		mesh.add_face(Vertex{0}, Vertex{2}, Vertex{3});
		mesh.add_face(Vertex{1}, Vertex{4}, Vertex{2});
		mesh.add_face(Vertex{2}, Vertex{4}, Vertex{3});
		mesh.add_face(Vertex{1}, Vertex{5}, Vertex{4});
		mesh.add_face(Vertex{4}, Vertex{6}, Vertex{3});
		mesh.add_face(Vertex{4}, Vertex{5}, Vertex{6});
	}

	// clang-format off
	constexpr auto keep_src_info   = Layer_definition<std::int32_t, Ref_type::vertex>("keep_src")
	                                 .interpolation(Interpolation::keep_origin);
	constexpr auto keep_dest_info  = Layer_definition<std::int32_t, Ref_type::vertex>("keep_dest")
	                                 .interpolation(Interpolation::keep_dest);
	constexpr auto reset_aff_info  = Layer_definition<std::int32_t, Ref_type::vertex>("reset_affected")
	                                 .interpolation(Interpolation::reset_affected);
	constexpr auto reset_all_info  = Layer_definition<std::int32_t, Ref_type::vertex>("reset_all")
	                                 .interpolation(Interpolation::reset_all);
	constexpr auto remove_info     = Layer_definition<std::int32_t, Ref_type::vertex>("remove_layer")
	                                 .interpolation(Interpolation::remove_layer);
	constexpr auto lerp_info       = Layer_definition<std::int32_t, Ref_type::vertex>("lerp")
	                                 .interpolation(Interpolation::lerp);
	constexpr auto slerp_info      = Layer_definition<Vec3, Ref_type::vertex>("slerp")
	                                 .interpolation(Interpolation::nlerp);
	constexpr auto min_value_info  = Layer_definition<std::int32_t, Ref_type::vertex>("min_value")
	                                 .interpolation(Interpolation::min_value);
	constexpr auto max_value_info  = Layer_definition<std::int32_t, Ref_type::vertex>("max_value")
	                                 .interpolation(Interpolation::max_value);
	constexpr auto min_weight_info = Layer_definition<std::int32_t, Ref_type::vertex>("min_weight")
	                                 .interpolation(Interpolation::min_weight);
	constexpr auto max_weight_info = Layer_definition<std::int32_t, Ref_type::vertex>("max_weight")
	                                 .interpolation(Interpolation::max_weight);
	// clang-format on

	// add layers
	for(auto&& info : Ref_range{keep_src_info,
	                            keep_dest_info,
	                            reset_aff_info,
	                            reset_all_info,
	                            remove_info,
	                            lerp_info,
	                            min_value_info,
	                            max_value_info,
	                            min_weight_info,
	                            max_weight_info}) {

		auto [layer] = world.lock_layer(info);
		for(int i = 0; i < 7; i++)
			layer[Vertex(i)] = i;
	}

	// slerp data
	{
		auto [layer]     = world.lock_layer(slerp_info);
		layer[Vertex(2)] = Vec3(10, 0, 0);
		layer[Vertex(4)] = Vec3(0, 10, 0);
	}

	{
		auto [mesh] = world.lock_mesh();
		CHECK(mesh.collapse(Edge(Edge_type::primal, 13), 0.5f) == Vertex(2));
	}

	// check layer values
	CHECK(!world.layer(remove_info));

	auto [keep_src, keep_dst, reset_aff, reset_all, lerp, slerp, min_val, max_val, min_weight, max_weight] =
	        world.lock_layer(keep_src_info,
	                         keep_dest_info,
	                         reset_aff_info,
	                         reset_all_info,
	                         lerp_info,
	                         slerp_info,
	                         min_value_info,
	                         max_value_info,
	                         min_weight_info,
	                         max_weight_info);

	CHECK(keep_src[Vertex(2)] == 2);
	CHECK(keep_dst[Vertex(2)] == 4);
	CHECK(reset_aff[Vertex(2)] == 0);
	CHECK(reset_all[Vertex(2)] == 0);
	CHECK(reset_all[Vertex(0)] == 0);
	CHECK(reset_all[Vertex(5)] == 0);
	CHECK(lerp[Vertex(2)] == 3);
	CHECK(slerp[Vertex(2)].x == Approx(7.071067812f));
	CHECK(slerp[Vertex(2)].y == Approx(7.071067812f));
	CHECK(slerp[Vertex(2)].z == Approx(0.f));
	CHECK(min_val[Vertex(2)] == 2);
	CHECK(max_val[Vertex(2)] == 4);
	CHECK(min_weight[Vertex(2)] == 2);
	CHECK(max_weight[Vertex(2)] == 4);
}

TEST_CASE("yggdrasill::Layer interpolation (split)")
{
	using namespace yggdrasill;

	auto world = World(Seed::from({42}));

	// construct mesh
	{
		auto [mesh] = world.lock_mesh();

		/*
		 *         0
		 *       / | \
		 *     /   |   \
		 *   /  F0 | F1  \
		 *  1------2------3
		 *  |\  F2 | F3  /|
		 *  |  \   |   /  |
		 *  |    \ | /    |
		 *  |     \|/     |
		 *  | F4   4  F5  |
		 *  |     / \     |
		 *  |    /   \    |
		 *  |  /  F6   \  |
		 *  |/           \|
		 *  5-------------6
		 *
		 * (2->4) =>
		 *
		 * // Faces added: F7 & F8
		 * // Vertex added: 7
		 * // Edges added: 2-7, 1-7, 3-7, 4-7
		 * // Edges removed: 2-4
		 *
		 *         0
		 *       / | \
		 *     /   |   \
		 *   /  F0 | F1  \
		 *  1------2------3
		 *  |\  F7 | F8  /|
		 *  |\ \-\ | /--//|
		 *  | \    7    / |
		 *  |  \ F2|F3 /  |
		 *  |   \  |  /   |
		 *  |    \ | /    |
		 *  |     \|/     |
		 *  | F4   4  F5  |
		 *  |     / \     |
		 *  |    /   \    |
		 *  |  /  F6   \  |
		 *  |/           \|
		 *  5-------------6
		 */

		// add vertices and check if they are valid
		mesh.add_vertices(7);
		mesh.add_face(yggdrasill::Vertex{0}, yggdrasill::Vertex{1}, yggdrasill::Vertex{2});
		mesh.add_face(yggdrasill::Vertex{0}, yggdrasill::Vertex{2}, yggdrasill::Vertex{3});
		mesh.add_face(yggdrasill::Vertex{1}, yggdrasill::Vertex{4}, yggdrasill::Vertex{2});
		mesh.add_face(yggdrasill::Vertex{2}, yggdrasill::Vertex{4}, yggdrasill::Vertex{3});
		mesh.add_face(yggdrasill::Vertex{1}, yggdrasill::Vertex{5}, yggdrasill::Vertex{4});
		mesh.add_face(yggdrasill::Vertex{4}, yggdrasill::Vertex{6}, yggdrasill::Vertex{3});
		mesh.add_face(yggdrasill::Vertex{4}, yggdrasill::Vertex{5}, yggdrasill::Vertex{6});
	}

	// clang-format off
	constexpr auto keep_src_info   = Layer_definition<std::int32_t, Ref_type::vertex>("keep_src")
	                                 .interpolation(Interpolation::keep_origin);
	constexpr auto keep_dest_info  = Layer_definition<std::int32_t, Ref_type::vertex>("keep_dest")
	                                 .interpolation(Interpolation::keep_dest);
	constexpr auto reset_aff_info  = Layer_definition<std::int32_t, Ref_type::vertex>("reset_affected")
	                                 .interpolation(Interpolation::reset_affected);
	constexpr auto reset_all_info  = Layer_definition<std::int32_t, Ref_type::vertex>("reset_all")
	                                 .interpolation(Interpolation::reset_all);
	constexpr auto remove_info     = Layer_definition<std::int32_t, Ref_type::vertex>("remove_layer")
	                                 .interpolation(Interpolation::remove_layer);
	constexpr auto lerp_info       = Layer_definition<std::int32_t, Ref_type::vertex>("lerp")
	                                 .interpolation(Interpolation::lerp);
	constexpr auto slerp_info      = Layer_definition<Vec2, Ref_type::vertex>("slerp")
	                                 .interpolation(Interpolation::nlerp);
	constexpr auto min_value_info  = Layer_definition<std::int32_t, Ref_type::vertex>("min_value")
	                                 .interpolation(Interpolation::min_value);
	constexpr auto max_value_info  = Layer_definition<std::int32_t, Ref_type::vertex>("max_value")
	                                 .interpolation(Interpolation::max_value);
	constexpr auto min_weight_info = Layer_definition<std::int32_t, Ref_type::vertex>("min_weight")
	                                 .interpolation(Interpolation::min_weight);
	constexpr auto max_weight_info = Layer_definition<std::int32_t, Ref_type::vertex>("max_weight")
	                                 .interpolation(Interpolation::max_weight);
	// clang-format on

	// add layers
	for(auto&& info : Ref_range{keep_src_info,
	                            keep_dest_info,
	                            reset_aff_info,
	                            reset_all_info,
	                            remove_info,
	                            lerp_info,
	                            min_value_info,
	                            max_value_info,
	                            min_weight_info,
	                            max_weight_info}) {

		auto [layer] = world.lock_layer(info);
		for(int i = 0; i < 7; i++)
			layer[Vertex(i)] = i;
	}

	// slerp data
	{
		auto [layer]     = world.lock_layer(slerp_info);
		layer[Vertex(2)] = Vec2(10, 0);
		layer[Vertex(4)] = Vec2(0, 10);
	}

	{
		auto [mesh] = world.lock_mesh();
		auto result = mesh.split(Edge(Edge_type::primal, 13), 0.5f);
		CHECK(result.origin(mesh) == yggdrasill::Vertex(7));
		CHECK(result.dest(mesh) == yggdrasill::Vertex(4));
	}

	// check layer values
	CHECK(!world.layer(remove_info));

	auto [keep_src, keep_dst, reset_aff, reset_all, lerp, slerp, min_val, max_val, min_weight, max_weight] =
	        world.lock_layer(keep_src_info,
	                         keep_dest_info,
	                         reset_aff_info,
	                         reset_all_info,
	                         lerp_info,
	                         slerp_info,
	                         min_value_info,
	                         max_value_info,
	                         min_weight_info,
	                         max_weight_info);

	CHECK(keep_src[Vertex(7)] == 2);
	CHECK(keep_dst[Vertex(7)] == 4);
	CHECK(reset_aff[Vertex(7)] == 0);
	CHECK(reset_all[Vertex(7)] == 0);
	CHECK(reset_all[Vertex(2)] == 0);
	CHECK(reset_all[Vertex(0)] == 0);
	CHECK(reset_all[Vertex(4)] == 0);
	CHECK(lerp[Vertex(7)] == 3);
	CHECK(slerp[Vertex(7)].x == Approx(7.071067812f));
	CHECK(slerp[Vertex(7)].y == Approx(7.071067812f));
	CHECK(min_val[Vertex(7)] == 2);
	CHECK(max_val[Vertex(7)] == 4);
	CHECK(min_weight[Vertex(7)] == 2);
	CHECK(max_weight[Vertex(7)] == 4);
}

SCENARIO("yggdrasill::Layer invalidate_layers (interpolation)")
{
	using namespace yggdrasill;

	auto world = World(Seed::from({42}));

	// construct mesh
	{
		auto [mesh] = world.lock_mesh();

		/*
		 *         0
		 *       / | \
		 *     /   |   \
		 *   /  F0 | F1  \
		 *  1------2------3
		 *  |\  F2 | F3  /|
		 *  |  \   |   /  |
		 *  |    \ | /    |
		 *  |     \|/     |
		 *  | F4   4  F5  |
		 *  |     / \     |
		 *  |    /   \    |
		 *  |  /  F6   \  |
		 *  |/           \|
		 *  5-------------6
		 *
		 * (2->4) =>
		 *
		 * // Faces added: F7 & F8
		 * // Vertex added: 7
		 * // Edges added: 2-7, 1-7, 3-7, 4-7
		 * // Edges removed: 2-4
		 *
		 *         0
		 *       / | \
		 *     /   |   \
		 *   /  F0 | F1  \
		 *  1------2------3
		 *  |\  F7 | F8  /|
		 *  |\ \-\ | /--//|
		 *  | \    7    / |
		 *  |  \ F2|F3 /  |
		 *  |   \  |  /   |
		 *  |    \ | /    |
		 *  |     \|/     |
		 *  | F4   4  F5  |
		 *  |     / \     |
		 *  |    /   \    |
		 *  |  /  F6   \  |
		 *  |/           \|
		 *  5-------------6
		 */

		// add vertices and check if they are valid
		mesh.add_vertices(7);
		mesh.add_face(yggdrasill::Vertex{0}, yggdrasill::Vertex{1}, yggdrasill::Vertex{2});
		mesh.add_face(yggdrasill::Vertex{0}, yggdrasill::Vertex{2}, yggdrasill::Vertex{3});
		mesh.add_face(yggdrasill::Vertex{1}, yggdrasill::Vertex{4}, yggdrasill::Vertex{2});
		mesh.add_face(yggdrasill::Vertex{2}, yggdrasill::Vertex{4}, yggdrasill::Vertex{3});
		mesh.add_face(yggdrasill::Vertex{1}, yggdrasill::Vertex{5}, yggdrasill::Vertex{4});
		mesh.add_face(yggdrasill::Vertex{4}, yggdrasill::Vertex{6}, yggdrasill::Vertex{3});
		mesh.add_face(yggdrasill::Vertex{4}, yggdrasill::Vertex{5}, yggdrasill::Vertex{6});
	}

	// clang-format off
	constexpr auto keep_src_info   = Layer_definition<std::int32_t, Ref_type::vertex>("keep_src")
	                                 .interpolation(Interpolation::keep_origin);
	constexpr auto keep_dest_info  = Layer_definition<std::int32_t, Ref_type::vertex>("keep_dest")
	                                 .interpolation(Interpolation::keep_dest);
	constexpr auto reset_aff_info  = Layer_definition<std::int32_t, Ref_type::vertex>("reset_affected")
	                                 .interpolation(Interpolation::reset_affected);
	constexpr auto reset_all_info  = Layer_definition<std::int32_t, Ref_type::vertex>("reset_all")
	                                 .interpolation(Interpolation::reset_all);
	constexpr auto remove_info     = Layer_definition<std::int32_t, Ref_type::vertex>("remove_layer")
	                                 .interpolation(Interpolation::remove_layer);
	constexpr auto lerp_info       = Layer_definition<std::int32_t, Ref_type::vertex>("lerp")
	                                 .interpolation(Interpolation::lerp);
	constexpr auto slerp_info      = Layer_definition<Vec2, Ref_type::vertex>("slerp")
	                                 .interpolation(Interpolation::nlerp);
	constexpr auto min_value_info  = Layer_definition<std::int32_t, Ref_type::vertex>("min_value")
	                                 .interpolation(Interpolation::min_value);
	constexpr auto max_value_info  = Layer_definition<std::int32_t, Ref_type::vertex>("max_value")
	                                 .interpolation(Interpolation::max_value);
	constexpr auto min_weight_info = Layer_definition<std::int32_t, Ref_type::vertex>("min_weight")
	                                 .interpolation(Interpolation::min_weight);
	constexpr auto max_weight_info = Layer_definition<std::int32_t, Ref_type::vertex>("max_weight")
	                                 .interpolation(Interpolation::max_weight);
	// clang-format on

	// add layers
	for(auto&& info : Ref_range{keep_src_info,
	                            keep_dest_info,
	                            reset_aff_info,
	                            reset_all_info,
	                            remove_info,
	                            lerp_info,
	                            min_value_info,
	                            max_value_info,
	                            min_weight_info,
	                            max_weight_info}) {

		auto [layer] = world.lock_layer(info);
		for(int i = 0; i < 7; i++)
			layer[Vertex(i)] = i;
	}

	// slerp data
	{
		auto [layer]     = world.lock_layer(slerp_info);
		layer[Vertex(2)] = Vec2(10, 0);
		layer[Vertex(4)] = Vec2(0, 10);
	}

	world.invalidate_layers(
	        {.interpolated_vertices = std::array{Element_interpolation{Vertex(1), Vertex(2), Vertex(4)}},
	         .interpolation_weight  = 0.5f});

	// check layer values
	CHECK(!world.layer(remove_info));

	auto [keep_src, keep_dst, reset_aff, reset_all, lerp, slerp, min_val, max_val, min_weight, max_weight] =
	        world.lock_layer(keep_src_info,
	                         keep_dest_info,
	                         reset_aff_info,
	                         reset_all_info,
	                         lerp_info,
	                         slerp_info,
	                         min_value_info,
	                         max_value_info,
	                         min_weight_info,
	                         max_weight_info);

	CHECK(keep_src[Vertex(1)] == 2);
	CHECK(keep_dst[Vertex(1)] == 4);
	CHECK(reset_aff[Vertex(1)] == 0);
	for(int i = 0; i < 7; i++)
		CHECK(reset_all[Vertex(i)] == 0);

	CHECK(lerp[Vertex(1)] == 3);
	CHECK(slerp[Vertex(1)].x == Approx(7.071067812f));
	CHECK(slerp[Vertex(1)].y == Approx(7.071067812f));
	CHECK(min_val[Vertex(1)] == 2);
	CHECK(max_val[Vertex(1)] == 4);
	CHECK(min_weight[Vertex(1)] == 2);
	CHECK(max_weight[Vertex(1)] == 4);

	for(auto& layer : Ref_range{keep_src, keep_dst, reset_aff, lerp, min_val, max_val, min_weight, max_weight}) {

		for(int i = 0; i < 7; i++)
			if(i != 1)
				CHECK(layer[Vertex(i)] == i);
	}
}

TEST_CASE("yggdrasill::Layer invalidate_layers (invalidation)")
{
	using namespace yggdrasill;

	auto world = World(Seed::from({42}));

	// construct mesh
	{
		auto [mesh] = world.lock_mesh();

		/*
		 *         0
		 *       / | \
		 *     /   |   \
		 *   /  F0 | F1  \
		 *  1------2------3
		 *  |\  F2 | F3  /|
		 *  |  \   |   /  |
		 *  |    \ | /    |
		 *  |     \|/     |
		 *  | F4   4  F5  |
		 *  |     / \     |
		 *  |    /   \    |
		 *  |  /  F6   \  |
		 *  |/           \|
		 *  5-------------6
		 *
		 * (2->4) =>
		 *
		 * // Faces added: F7 & F8
		 * // Vertex added: 7
		 * // Edges added: 2-7, 1-7, 3-7, 4-7
		 * // Edges removed: 2-4
		 *
		 *         0
		 *       / | \
		 *     /   |   \
		 *   /  F0 | F1  \
		 *  1------2------3
		 *  |\  F7 | F8  /|
		 *  |\ \-\ | /--//|
		 *  | \    7    / |
		 *  |  \ F2|F3 /  |
		 *  |   \  |  /   |
		 *  |    \ | /    |
		 *  |     \|/     |
		 *  | F4   4  F5  |
		 *  |     / \     |
		 *  |    /   \    |
		 *  |  /  F6   \  |
		 *  |/           \|
		 *  5-------------6
		 */

		// add vertices and check if they are valid
		mesh.add_vertices(7);
		mesh.add_face(yggdrasill::Vertex{0}, yggdrasill::Vertex{1}, yggdrasill::Vertex{2});
		mesh.add_face(yggdrasill::Vertex{0}, yggdrasill::Vertex{2}, yggdrasill::Vertex{3});
		mesh.add_face(yggdrasill::Vertex{1}, yggdrasill::Vertex{4}, yggdrasill::Vertex{2});
		mesh.add_face(yggdrasill::Vertex{2}, yggdrasill::Vertex{4}, yggdrasill::Vertex{3});
		mesh.add_face(yggdrasill::Vertex{1}, yggdrasill::Vertex{5}, yggdrasill::Vertex{4});
		mesh.add_face(yggdrasill::Vertex{4}, yggdrasill::Vertex{6}, yggdrasill::Vertex{3});
		mesh.add_face(yggdrasill::Vertex{4}, yggdrasill::Vertex{5}, yggdrasill::Vertex{6});
	}

	// clang-format off
	constexpr auto keep_info       = Layer_definition<std::int32_t, Ref_type::vertex>("keep")
	                                 .invalidation(Invalidation::keep);
	constexpr auto remove_info     = Layer_definition<std::int32_t, Ref_type::vertex>("remove_layer")
	                                 .invalidation(Invalidation::remove_layer);
	constexpr auto reset_aff_info  = Layer_definition<std::int32_t, Ref_type::vertex>("reset_affected")
	                                 .invalidation(Invalidation::reset_affected);
	constexpr auto reset_all_info  = Layer_definition<std::int32_t, Ref_type::vertex>("reset_all")
	                                 .invalidation(Invalidation::reset_all);
	// clang-format on

	// add layers
	for(auto&& info : Ref_range{keep_info, remove_info, reset_aff_info, reset_all_info}) {

		auto [layer] = world.lock_layer(info);
		for(int i = 0; i < 7; i++)
			layer[Vertex(i)] = i;
	}


	world.invalidate_layers({.invalidated_vertices = std::array<index_t, 1>{1}});

	// check layer values
	CHECK(!world.layer(remove_info));

	auto [keep, reset_aff, reset_all] = world.lock_layer(keep_info, reset_aff_info, reset_all_info);

	for(int i = 0; i < 7; i++)
		CHECK(keep[Vertex(i)] == i);

	CHECK(reset_aff[Vertex(1)] == 0);
	for(int i = 0; i < 7; i++)
		if(i != 1)
			CHECK(reset_aff[Vertex(i)] == i);

	for(int i = 0; i < 7; i++)
		CHECK(reset_all[Vertex(i)] == 0);
}

TEST_CASE("yggdrasill::Layer (unstructured)")
{
	auto world = yggdrasill::World(yggdrasill::Seed::from({42}));

	CHECK(!world.unstructured_layer("test"));

	{
		auto [layer] = world.lock_unstructured_layer("test");
		layer["val"] = 42;
	}

	REQUIRE(world.unstructured_layer("test"));

	auto&& d = world.required_unstructured_layer("test");
	CHECK(d["val"] == 42);

	auto layers = world.unstructured_layers();
	REQUIRE(layers.size() == 1);
	CHECK(layers[0] == "test");

	world.remove_unstructured_layer("test");
	CHECK(!world.unstructured_layer("test"));
}
