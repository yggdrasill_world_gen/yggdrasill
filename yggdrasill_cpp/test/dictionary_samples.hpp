#pragma once

#include <yggdrasill/dictionary.hpp>

namespace yggdrasill {

	/// \def_example{dictionary_sample_create}
	void dictionary_sample_create()
	{
		auto cfg = yggdrasill::Dictionary();

		cfg["seed"]                    = 42;
		cfg["plate_sim"]["delta_time"] = 1234.f;
		cfg["plate_sim"]["sub_steps"]  = 16.f;
		cfg["city_gen"]["position"]    = Vec3(1, 2, 3);
		cfg["city_gen"]["name"]        = "Atlantis";
	}

	/// \def_example{dictionary_sample_array}
	void dictionary_sample_array()
	{
		auto cfg = yggdrasill::Dictionary();

		// either construct it explicitly
		auto&& array = cfg["sequence"].get<yggdrasill::Array_ref<int32_t>>();
		array.push_back(1);
		array.push_back(2);
		array.push_back(3);

		// ... or assign an existing sequence (std::vector, std::span or yggdrasill::Array)
		cfg["other_sequence"] = std::vector{1.f, 2.f, 3.f};
	}

	/// \def_example{dictionary_sample_read}
	float dictionary_sample_read(yggdrasill::Dictionary_view cfg)
	{
		// either retrieves the value or inserts the given default value
		const auto a = cfg["a"].get(0.05f);

		// same as above, but also checks if the value is >=min and <=max
		const auto b = cfg["b"].get(0.05f, {.min = 0.f, .max = 0.99f});

		// same as above, but always throws on missing/invalid values
		const auto c = cfg["c"].get(0.05f, {.min = 0.f, .max = 0.99f, .throw_on_error = true});

		// same as above, but the default value is 0.f (0 for ints, false for bool, ...)
		const auto d = cfg["d"].get<float>();

		// values can also be compared directly, which evaluate to false or
		//   std::partial_ordering::unordered, if the value is unset or
		//   if it's neither comparable nor convertible to the requested type
		if(cfg["e"] > 0.5f) {
			// ...
		}

		return a + b + c + d;
	}

} // namespace yggdrasill
