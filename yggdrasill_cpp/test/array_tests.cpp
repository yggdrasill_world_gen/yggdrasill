#include <catch2/catch.hpp>

#include <yggdrasill/array.hpp>
#include <yggdrasill/dictionary.hpp>

#include <sstream>

TEST_CASE("yggdrasill::Array: String")
{
	auto dict  = yggdrasill::Dictionary();
	auto array = dict["array"].get<yggdrasill::Array_ref<yggdrasill::String>>();
	REQUIRE(array.empty());

	array.push_back("Test");
	array.push_back("long string that doesn't fit into the small-buffer string");
	array.push_back("Test 2");

	REQUIRE(array.size() == 3);
	REQUIRE(array[0] == "Test");

	auto iter = array.begin();
	REQUIRE(*iter == "Test");
	iter++;
	REQUIRE(*iter == "long string that doesn't fit into the small-buffer string");
	iter++;
	REQUIRE(*iter == "Test 2");
	iter++;
	REQUIRE(iter == array.end());

	REQUIRE(array[2] == "Test 2");

	const auto& carray = array;
	REQUIRE(carray[2] == "Test 2");

	array.reserve(6);
	REQUIRE(array.size() == 3);

	array.resize(2);
	REQUIRE(array.size() == 2);
	REQUIRE(array[0] == "Test");
	REQUIRE(array[1] == "long string that doesn't fit into the small-buffer string");

	array.push_back("Test 4");
	array.erase_unordered(0);
	REQUIRE(array.size() == 2);
	REQUIRE(array[0] == "Test 4");

	array.push_back("Test 5");
	array.erase_ordered(0);
	REQUIRE(array.size() == 2);
	REQUIRE(array[0] == "long string that doesn't fit into the small-buffer string");
	REQUIRE(array[1] == "Test 5");

	array = std::initializer_list<yggdrasill::String>{yggdrasill::String("abc"), yggdrasill::String("def")};
	REQUIRE(array.size() == 2);
	REQUIRE(array[0] == "abc");
	REQUIRE(array[1] == "def");
}

TEST_CASE("yggdrasill::Array: bool")
{
	auto        dict   = yggdrasill::Dictionary();
	auto        array  = dict["array"].get<yggdrasill::Array_ref<YGDL_Bool>>();
	const auto& carray = array;

	array.push_back(true);
	REQUIRE(array.size() == 1);
	REQUIRE(static_cast<bool>(array[0]) == true);
	REQUIRE(static_cast<bool>(carray[0]) == true);
	CHECK_THROWS_AS(array[1], yggdrasill::Out_of_bounds_exception);
}

TEST_CASE("yggdrasill::Array: int8")
{
	auto        dict   = yggdrasill::Dictionary();
	auto        array  = dict["array"].get<yggdrasill::Array_ref<std::int8_t>>();
	const auto& carray = array;

	array.push_back(int8_t(42));
	REQUIRE(array.size() == 1);
	REQUIRE(array[0] == 42);
	REQUIRE(carray[0] == 42);
}

TEST_CASE("yggdrasill::Array: int32")
{
	auto        dict   = yggdrasill::Dictionary();
	auto        array  = dict["array"].get<yggdrasill::Array_ref<std::int32_t>>();
	const auto& carray = array;

	array.push_back(42);
	REQUIRE(array.size() == 1);
	REQUIRE(array[0] == 42);
	REQUIRE(carray[0] == 42);
}

TEST_CASE("yggdrasill::Array: int64")
{
	auto        dict   = yggdrasill::Dictionary();
	auto        array  = dict["array"].get<yggdrasill::Array_ref<std::int64_t>>();
	const auto& carray = array;

	array.push_back(42);
	REQUIRE(array.size() == 1);
	REQUIRE(array[0] == 42);
	REQUIRE(carray[0] == 42);
}

TEST_CASE("yggdrasill::Array: float")
{
	auto        dict   = yggdrasill::Dictionary();
	auto        array  = dict["array"].get<yggdrasill::Array_ref<float>>();
	const auto& carray = array;

	array.push_back(2.3f);
	REQUIRE(array.size() == 1);
	REQUIRE(array[0] == 2.3f);
	REQUIRE(carray[0] == 2.3f);
}

TEST_CASE("yggdrasill::Array: double")
{
	auto        dict   = yggdrasill::Dictionary();
	auto        array  = dict["array"].get<yggdrasill::Array_ref<double>>();
	const auto& carray = array;

	array.push_back(2.3f);
	REQUIRE(array.size() == 1);
	REQUIRE(array[0] == 2.3f);
	REQUIRE(carray[0] == 2.3f);
}

TEST_CASE("yggdrasill::Array: vec2")
{
	auto        dict   = yggdrasill::Dictionary();
	auto        array  = dict["array"].get<yggdrasill::Array_ref<yggdrasill::Vec2>>();
	const auto& carray = array;

	array.push_back(yggdrasill::Vec2(1, 2));
	REQUIRE(array.size() == 1);
	REQUIRE(array[0] == yggdrasill::Vec2(1, 2));
	REQUIRE(carray[0] == yggdrasill::Vec2(1, 2));
}

TEST_CASE("yggdrasill::Array: vec3")
{
	auto        dict   = yggdrasill::Dictionary();
	auto        array  = dict["array"].get<yggdrasill::Array_ref<yggdrasill::Vec3>>();
	const auto& carray = array;

	array.push_back(yggdrasill::Vec3(1, 2, 3));
	REQUIRE(array.size() == 1);
	REQUIRE(array[0] == yggdrasill::Vec3(1, 2, 3));
	REQUIRE(carray[0] == yggdrasill::Vec3(1, 2, 3));
}

TEST_CASE("yggdrasill::Array: dict")
{
	auto        dict   = yggdrasill::Dictionary();
	auto        array  = dict["array"].get<yggdrasill::Array_ref<yggdrasill::Dictionary_ref>>();
	const auto& carray = array;

	array.push_back()["foo"] = "bar";
	REQUIRE(array.size() == 1);
	REQUIRE(array[0]["foo"] == "bar");
	REQUIRE(carray[0]["foo"] == "bar");
}

TEST_CASE("yggdrasill::Array: array")
{
	auto        dict   = yggdrasill::Dictionary();
	auto        array  = dict["array"].get<yggdrasill::Array_ref<yggdrasill::Array_ref<std::int32_t>>>();
	const auto& carray = array;

	auto&& inner_array = array.push_back();
	inner_array.push_back(1);
	inner_array.push_back(2);

	REQUIRE(array.size() == 1);
	REQUIRE(array[0].size() == 2);
	REQUIRE(carray[0].size() == 2);
	REQUIRE(array[0][0] == 1);
	REQUIRE(carray[0][0] == 1);
	REQUIRE(array[0][1] == 2);
	REQUIRE(carray[0][1] == 2);

	array.resize(1024);
	REQUIRE(array.size() == 1024);
	REQUIRE(array[0].size() == 2);

	CHECK_THROWS_AS(dict["array"].get<const yggdrasill::Array_ref<yggdrasill::Array_ref<YGDL_Bool>>>()[0][0],
	                yggdrasill::Type_mismatch_exception);
}
