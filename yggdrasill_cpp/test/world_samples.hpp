#pragma once

#include <yggdrasill/mesh_utils.hpp>
#include <yggdrasill/world.hpp>

namespace yggdrasill {

	/// \def_example{temporary_layer_example}
	[[maybe_unused]] void temporary_layer_example(Const_mesh_view mesh)
	{
		// Creates a layer of the correct size for the given mesh with value-initialized elements.
		// Optionally the initial value can also be provided as a second parameter.
		auto forces = Temporary_layer<Vec3, Ref_type::vertex>(mesh);

		// If a more complex initialization is required, you can also provide a lambda expression
		//   that will be executed once for each valid vertex/face/edge.
		// Elements that aren't used in the mesh will be value-initialized.
		auto ids = Temporary_layer<std::int8_t, Ref_type::face>(mesh, [](Face f) {
			// ...
			return static_cast<std::int8_t>(f.index() % 10);
		});

		// ...

		// It's important to reset the layer before using it, if the mesh might have been modified since
		//   because, contrary to normal layers, mesh modifications aren't automatically
		//   propagated to temporary layers.
		forces.reset(mesh);
		ids.reset(mesh, [](Face f) { return static_cast<std::int8_t>(f.index() % 10); });
	}

	/// \def_example{layer_definition_example}
	[[maybe_unused]] void layer_definition_example(float radius)
	{
		const auto position_layer = Layer_definition<Vec3, Ref_type::vertex>("position")
		                                    .interpolation(Interpolation::nlerp)
		                                    .invalidation(Invalidation::reset_affected)
		                                    .min_length(radius * 0.9f)
		                                    .max_length(radius * 1.1f);
	}

	/// \def_example{layer_example}
	[[maybe_unused]] void layer_example(World& world, float delta_time)
	{
		static constexpr float radius = 6'371'000.f;

		// Layers are usually defined once.
		// Their definitions are build using a fluent API, which can be used to set various properties
		static constexpr auto position_layer = Layer_definition<Vec3, Ref_type::vertex>("position")
		                                               .description("Position of vertices on the sphere")
		                                               .initial(radius * Vec3{1, 0, 0})
		                                               .min_length(radius * 0.9f)
		                                               .max_length(radius * 1.1f)
		                                               .interpolation(Interpolation::nlerp)
		                                               .invalidation(Invalidation::reset_affected);

		// ... but they also have default values, which are adequate for most use cases.
		static constexpr auto velocity_layer = Layer_definition<Vec3, Ref_type::vertex>("velocity");

		// ...

		// A read-only Const_layer_view can be acquired from the world object using
		std::optional<const_view_of_def<position_layer>> opt_ro_positions = world.layer(position_layer);

		// ... or if the layer is required to exist
		const_view_of_def<position_layer> ro_positions = world.required_layer(position_layer);


		// To modify a layer's content, a mutable Layer_view has to be acquired/locked
		auto [positions, velocities] = world.lock_layer(position_layer, velocity_layer);

		auto mesh = world.mesh();

		// Afterwards it can be used to directly access and modify elements.
		positions[Vertex(42)] = Vec3{1, 2, 3};

		// But you'll usually want to iterate over it and other layers in tandem,
		//   which also takes care of only accessing valid elements that are actually used by the mesh.
		for(auto&& [vertex, position, velocity] : range(mesh, positions, velocities)) {
			position = normalized(position + velocity * delta_time) * radius;
		}

		// It's also possible to iterate directly over the layer but this will not check if the element
		//   is currently used by the mesh, which might need consideration as unused elements can contain
		//   invalid data.
		// But this might be a worthwhile trade-off if performance is of a high priority.
		for(auto& position : positions) {
			// ...
		}
	}

	/// \def_example{world_lock_sample}
	[[maybe_unused]] void world_lock_sample(World&                                          world,
	                                        const Layer_definition<Vec3, Ref_type::vertex>& position_layer)
	{
		// Using the accessor methods (mesh(), layer() and required_layer()) returns an immutable view,
		// which might be shared between multiple worlds.
		std::optional opt_positions = world.layer(position_layer);

		// layer(...) returns an optional because the layer might be missing.
		// If the layer is required or known to exist, you can use required_layer(...) instead, which
		// throws if the layer is missing
		Const_layer_view<Vec3, Ref_type::vertex> positions = world.required_layer(position_layer);

		// These can be used to traverse the mesh topology and access the data stored in layers
		std::cout << positions[Vertex(42)] << '\n';

		// To modify the mesh or the data stored in the layers, you need a mutable copy.
		// These can be acquired using the lock_... methods, which return RAII proxy objects,
		// that automatically unlock the mutable reference on destruction and that should immediately be
		// unpacked using structured bindings to retrieve the actual mutable view.
		auto [mut_positions]      = world.lock_layer(position_layer);
		mut_positions[Vertex(42)] = Vec3(1, 2, 3);

		// It's also possible to lock multiple layers in a single call
		//  auto [positions, velocities] = world.lock_layer(position_layer_info, velocity_layer_info);
	}

	/// \def_example{random_example}
	[[maybe_unused]] void random_example(World& world)
	{
		// Since generating random numbers changes its internal state, it has to be locked first.
		auto rng = world.random();

		// It can than be used to generate numbers following a uniform distribution in the range [min, max]
		std::cout << rng.uniform(0.f, 1.f) << '\n';
		std::cout << rng.uniform<int32_t>(-10, 100) << '\n';

		// If more than a single random number is needed, a compile-time constant count can be passed...
		std::array<int64_t, 10> numbers = rng.uniform<10, int64_t>(-100, 100);

		// ... or a range can be passed to directly populate a continues container, like std::vector
		auto vec = std::vector<float>();
		vec.resize(100);
		rng.uniform(0.f, 100.f, vec.begin(), vec.end());


		// The RNG can also be used to generate normal distributions with a given mean and standard-deviation
		std::cout << rng.normal(0.f, 1.f) << '\n';

		// ... booleans with a given probability to be true
		std::cout << rng.probability(0.5f) << '\n';

		// ... vectors where each component follows a uniform distribution in the range [min, max]
		std::cout << rng.uniform_vec3(0.f, 1.f) << '\n';

		// ... or randomly shuffle a container
		rng.shuffle(vec);
	}

} // namespace yggdrasill