#include <yggdrasill/module.hpp>

namespace yggdrasill::modules {

	static int execute_called = 0;

	static void execute(World_view world, yggdrasill::Any_dictionary auto)
	{
		execute_called++;
		if(!world && execute_called != 1)
			std::abort();
		else if(world && execute_called != 2)
			std::abort();

		std::cout << "Test_module::execute()\n";
	}

	extern const auto test_module = to_module_info<execute, execute>("test_module");

} // namespace yggdrasill::modules

YGDL_REGISTER_PLUGIN_MODULES(&yggdrasill::modules::test_module)
