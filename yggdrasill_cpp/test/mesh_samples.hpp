#pragma once

#include <yggdrasill/mesh.hpp>
#include <yggdrasill/mesh_utils.hpp>
#include <yggdrasill/world.hpp>

#include <numeric>

namespace yggdrasill {

	static float compute_area(Face)
	{
		return 0.f;
	}

	/// \def_example{mesh_base_iterators_example}
	[[maybe_unused]] inline void mesh_base_iterators_example(Const_mesh_view mesh)
	{
		// The vertices()/faces() method returns a range that allows iterating over all valid elements.
		for(Valid<Vertex> v : mesh.vertices()) {
			// ...
		}

		// But, like all iterator ranges provided by the mesh API, they can also be used for more complex
		//   operations or passed into existing algorithms that accept bidirectional input iterator.
		auto smallest_area_iter = mesh.faces().min([](Face f) { return compute_area(f); });
		Face smallest_area_face = *smallest_area_iter;

		// A similar method also exists for edges, except that there are two, one for directed and one
		//   undirected edges, and that they require a template parameter to differentiate between
		//   primal and dual edges.
		for(Primal_edge e : mesh.directed_edges<Edge_type::primal>()) {
			// ...
		}
		for(Dual_edge e : mesh.undirected_edges<Edge_type::dual>()) {
			// ...
		}


		// All the ranges above automatically skip elements that are not currently used by the mesh
		//   because they were removed and are currently waiting to be reused.
		// But it may sometimes be necessary to iterate over *all* elements of the mesh, including the
		//   invalid/unused ones.
		// For this use-case, the mesh also exposes three methods to determine the largest index that could
		//   potentially be in use for vertices, faces or edges.
		for(yggdrasill::index_t i = 0; i <= mesh.vertex_max_index(); i++) {
			// ...
		}


		// Another common use-case is to find the primal edge (if any) that connects two vertices
		if(auto e = mesh.find_edge(Vertex{42}, Vertex{23}); e != no_edge) {
			// ...
		}
	}


	/// \def_example{mesh_edge_iterators_example}
	[[maybe_unused]] inline void mesh_edge_iterators_example(Const_mesh_view mesh, Primal_edge e)
	{
		// origin_ccw() returns a range that iterates over all edges of the ring around e.origin(),
		//   starting with e itself.
		for(Primal_edge other_edge : e.origin_ccw(mesh)) {
			// ...
		}

		// This is identical to writing the following (also a fair bit simpler).
		Primal_edge other_edge = e;
		do {
			// ...
		} while((other_edge = other_edge.origin_next(mesh)) != e);


		// And like mesh.vertices(), the returned range can be used for much more than simple iteration,
		//   like passing its iterators into other algorithms.
		// Note: origin_ring.end() returns an end-sentinel. For functions that don't support this, you can
		//   call end_iterator() instead, which returns the same type as begin().
		auto left_ring = e.left_ccw(mesh);
		auto sum       = std::accumulate(
                left_ring.begin(), left_ring.end_iterator(), 0.f, [](float value, Primal_edge ee) {
                    // ...
                    return value;
                });


		// One important difference to normal iterators, is that since edge-rings are circular,
		//   their ranges are too. Which has two effects:
		auto origin_ring = e.origin_ccw(mesh);
		auto iter        = origin_ring.begin();
		for(; iter != origin_ring.end(); ++iter) {
			// ...
		}

		// (1) Incrementing an iterator past the end brings us to the beginning.
		//     However, the end-iterator itself may still not be dereferenced, as usual.
		//     Also note that the iterator returned by end_iterator() is just a sentinel, that may be
		//       neither incremented, not decremented.
		++iter;
		assert(iter == origin_ring.begin());

		// (2) Decrementing the begin-iterator is allowed and brings us to the last element.
		//     Decrementing will never reach the end sentinel and instead loop around again.
		iter = origin_ring.begin();
		--iter;
	}

	/// \def_example{mesh_construct_example}
	[[maybe_unused]] inline void mesh_construct_example(Mesh_view mesh)
	{
		// The first operation required to construct a new mesh is creating vertices.
		// add_vertices() can be called to just insert a fixed number of vertices
		mesh.add_vertices(5);

		// ... or passed a std::span instead to insert vertices and return the newly created ones in it.
		auto new_vertices = std::vector<Vertex>();
		new_vertices.resize(5);
		mesh.add_vertices(new_vertices);

		// Once the vertices are created, triangle faces can be created by connecting three vertices
		try {
			Valid<Face> new_face = mesh.add_face(Vertex{3}, Vertex{2}, Vertex{1});

		} catch(const Mesh_corruption_exception&) {
			// ...
		}
	}

	/// \def_example{mesh_modify_example}
	[[maybe_unused]] inline void mesh_modify_example(Mesh_view mesh, Primal_edge e)
	{
		try {
			// flip the edge e
			mesh.flip(e);
		} catch(const Mesh_corruption_exception&) {
			// ...
		}

		// If the operation is likely to fail, it may be a good idea to use the try_flip() variant, which
		//   doesn't throw Mesh_corruption_exception but just returns false.
		if(!mesh.try_flip(e)) {
			// ...
		}

		// Collapse the edge e, removing e.dest() and moving e.origin() to a position 25% along the edge.
		// The function also returns the remaining vertex, i.e. e.origin().
		Vertex remaining_vertex = mesh.collapse(e, 0.25f);

		// Splits e, inserting a new vertex at a position 50% along the edge, as well as multiple faces/edges.
		// The function returns the newly created edge along the path between the old origin and destination.
		Primal_edge new_edge = mesh.split(e, 0.5f);
	}

} // namespace yggdrasill