#pragma once

#include <yggdrasill/serialization.hpp>

namespace yggdrasill {

	/// \def_example{serialization_sample_write}
	void serialization_sample_write(std::string_view archive_file_path, std::span<std::byte> userdata)
	{
		// Especially if you are using the shared library and might not have control over the flags used
		//   to build it, you should check if Yggdrasill was compiled with serialization support enabled.
		// If that isn't the case, neither Serializer nor Deserializer may be instantiated.
		assert(yggdrasill::serialization_supported());

		try {
			// To write an archive file, a Serializer has to be created using one of its static functions.
			// The object can be used both to write new entries and to read previously written once.
			// Depending on which one is used, a potentially pre-existing file is either discarded...
			auto serializer = Serializer::open_overwrite(archive_file_path);
			// ... or preserved and read in first.
			// auto serializer = Serializer::open_read_write(archive_file_path);


			// An archive can contain 0-N World objects, as well as configurations and additional
			//   user-defined data.
			// Each of these can be written with a corresponding write_...() function.

			// Writing a world always creates a new entry, even if the same world has already been written
			//   previously, and isn't named by the caller but returns an integer-handle to later load it.
			auto world = World{};
			// ... simulation steps ...
			auto world_handle = serializer.write_world(world);

			// Configurations and user-data, on the other hand, are identified by a user-provided string.
			// Since overwriting/updating entries is currently not supported, the passed string has to be
			//   unique and no such entry may already exist.
			// Otherwise, a Precondition_violation_exception is thrown.
			auto config       = Dictionary{};
			config["example"] = 42;
			config["world"]   = world_handle;
			serializer.write_config("config", config);

			// This also applies to user-data, that is arbitrary bytes, used for application-specific data.
			serializer.write_userdata("user specific data", userdata);

		} catch(const Io_error_exception& e) {
			// Either the archive couldn't be opened or one of the write operations failed.
		}
	}

	/// \def_example{serialization_sample_read}
	void serialization_sample_read(std::string_view archive_file_path)
	{
		try {
			// To read an existing archive file, you have to create a Serializer instance.
			auto deserializer = Deserializer{archive_file_path};

			// Since an archive may contain 0-N entries of each type, the Deserializer provides functions
			//   to iterate over all existing entries. The passed callback will be invoked with every key.
			deserializer.foreach_world([&](Serialized_world_handle world_key) {
				// These keys can then be used to load the actual data.
				World read_world = deserializer.read_world(world_key);
				// ...
			});
			deserializer.foreach_config([&](std::string_view config_key) {
				Dictionary read_config = deserializer.read_config(config_key);
				// ...
			});
			deserializer.foreach_userdata([&](std::string_view userdata_key) {
				std::vector<std::byte> read_userdata = deserializer.read_userdata(userdata_key);
				// ...
			});

			// But a more common use-case is to have a central configuration entry with a well-known key,
			//   that then contains references to all other relevant entries.
			try {
				// To check if the configuration exists, read it and catch the potential exception.
				auto config = deserializer.read_config("config");

				// The configuration can then, for example, contain the key of a world that should be loaded.
				auto world_handle = config["world"].get<Serialized_world_handle>();

				auto world = deserializer.read_world(world_handle);
				// ...

			} catch(const Out_of_bounds_exception& e) {
				// ...
			}

			// While user data is written by passing a continuous byte sequence, reading them as such might
			//   not be optimal or feasible.
			// For this use-case, read_userdata() has an overload that is passed a read-callback.
			deserializer.read_userdata("user specific data", [&](Byte_reader& reader) {
				// If the entry exists and could be opened, the callback is invoked immediately with an
				//   object that can be used to read the entry's bytes and determine how many are left unread.
				assert(reader.remaining_bytes() > 6);

				// Bytes can only be read in-order, i.e. without seeking or skipping, and either into a span
				auto data = std::vector<std::byte>();
				data.resize(6);
				// Note that the number of actually read bytes may be fewer than requested.
				auto actually_read_data_span = reader.read_into(std::span(data));

				if(reader.remaining_bytes() > 0) {
					// ... or directly into a vector.
					auto position = std::vector<Vec3>();
					position.resize(2);
					reader.read_into(position);
				}
			});

		} catch(const Io_error_exception& e) {
			// Either the archive couldn't be opened or one of the write operations failed.
		}
	}

} // namespace yggdrasill
