#pragma once

#include <yggdrasill/generator.hpp>

namespace yggdrasill {

	/// \def_example{generator_sample}
	void generator_sample(const Module_info& static_module_info)
	{
		auto gen = yggdrasill::Generator();

		// You can load modules from a directory
		gen.load("path/to/plugin/directory/");

		// ... or from a specific file
		gen.load("specific_plugin.so");

		// ... and you can also directly add a Module_info that is part of the same application,
		//     which returns the corresponding Module_view
		auto static_module = gen.add(static_module_info);

		// To use a module, you have to retrieve it using its ID
		auto generate_sphere_module = gen.get("generate_sphere");
		if(!generate_sphere_module) {
			// ... which might fail if no plugin contained a module with the ID
			std::cerr << "Missing module 'generate_sphere'\n";
			return;
		}

		auto config = yggdrasill::Dictionary{};

		// It's usually a good idea to validate the configuration first
		if(auto status = generate_sphere_module->validate(config); !status) {
			// ... and handle potential failures
		}

		// And then we can create a World object and execute the module on it
		const auto seed  = yggdrasill::Seed::random();
		auto       world = yggdrasill::World(seed);
		generate_sphere_module->execute(world, config);
	}

} // namespace yggdrasill
