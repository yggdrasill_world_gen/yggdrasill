#include <catch2/catch.hpp>

#include <yggdrasill/array.h>
#include <yggdrasill/dictionary.h>

#include <array>
#include <cstring>

TEST_CASE("YGDL_type_is_convertible")
{
	// all types are convertible to themselves
	for(auto i = int(YGDL_TYPE_NONE); i <= int(YGDL_TYPE_DICT); i++) {
		REQUIRE(YGDL_type_is_convertible(static_cast<YGDL_Type_t>(i), static_cast<YGDL_Type_t>(i)));
	}

	for(auto i = int(YGDL_TYPE_NONE) + 1; i <= int(YGDL_TYPE_DICT); i++) {
		// NONE can be converted to no other type
		REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_NONE, static_cast<YGDL_Type_t>(i)));
		// nothing can be converted to NONE
		REQUIRE(!YGDL_type_is_convertible(static_cast<YGDL_Type_t>(i), YGDL_TYPE_NONE));
	}

	// bool is convertible to int8, int32, int64, float, double and string
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_BOOL, YGDL_TYPE_INT8));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_BOOL, YGDL_TYPE_INT32));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_BOOL, YGDL_TYPE_INT64));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_BOOL, YGDL_TYPE_FLOAT));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_BOOL, YGDL_TYPE_DOUBLE));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_BOOL, YGDL_TYPE_STRING));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_BOOL, YGDL_TYPE_VEC2));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_BOOL, YGDL_TYPE_VEC3));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_BOOL, YGDL_TYPE_ARRAY));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_BOOL, YGDL_TYPE_DICT));

	// int8 is convertible to bool, int32, int32, float, double and string
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT8, YGDL_TYPE_BOOL));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT8, YGDL_TYPE_INT32));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT8, YGDL_TYPE_INT64));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT8, YGDL_TYPE_FLOAT));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT8, YGDL_TYPE_DOUBLE));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT8, YGDL_TYPE_STRING));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_INT8, YGDL_TYPE_VEC2));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_INT8, YGDL_TYPE_VEC3));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_INT8, YGDL_TYPE_ARRAY));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_INT8, YGDL_TYPE_DICT));

	// int32 is convertible to bool, int8, int64, float, double and string
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT32, YGDL_TYPE_BOOL));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT32, YGDL_TYPE_INT8));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT32, YGDL_TYPE_INT64));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT32, YGDL_TYPE_FLOAT));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT32, YGDL_TYPE_DOUBLE));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT32, YGDL_TYPE_STRING));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_INT32, YGDL_TYPE_VEC2));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_INT32, YGDL_TYPE_VEC3));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_INT32, YGDL_TYPE_ARRAY));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_INT32, YGDL_TYPE_DICT));

	// int64 is convertible to bool, int8, int32, float, double and string
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT64, YGDL_TYPE_BOOL));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT64, YGDL_TYPE_INT8));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT64, YGDL_TYPE_INT32));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT64, YGDL_TYPE_FLOAT));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT64, YGDL_TYPE_DOUBLE));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_INT64, YGDL_TYPE_STRING));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_INT64, YGDL_TYPE_VEC2));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_INT64, YGDL_TYPE_VEC3));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_INT64, YGDL_TYPE_ARRAY));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_INT64, YGDL_TYPE_DICT));

	// float is convertible to int8, int32, int64, double, string, vec2 and vec3
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_FLOAT, YGDL_TYPE_BOOL));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_FLOAT, YGDL_TYPE_INT8));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_FLOAT, YGDL_TYPE_INT32));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_FLOAT, YGDL_TYPE_INT64));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_FLOAT, YGDL_TYPE_DOUBLE));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_FLOAT, YGDL_TYPE_STRING));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_FLOAT, YGDL_TYPE_VEC2));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_FLOAT, YGDL_TYPE_VEC3));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_FLOAT, YGDL_TYPE_ARRAY));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_FLOAT, YGDL_TYPE_DICT));

	// double is convertible to int8, int32, int64, flat, string, vec2 and vec3
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_DOUBLE, YGDL_TYPE_BOOL));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_DOUBLE, YGDL_TYPE_INT8));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_DOUBLE, YGDL_TYPE_INT32));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_DOUBLE, YGDL_TYPE_INT64));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_DOUBLE, YGDL_TYPE_FLOAT));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_DOUBLE, YGDL_TYPE_STRING));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_DOUBLE, YGDL_TYPE_VEC2));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_DOUBLE, YGDL_TYPE_VEC3));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_DOUBLE, YGDL_TYPE_ARRAY));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_DOUBLE, YGDL_TYPE_DICT));

	// vec2 is convertible to vec3
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_VEC2, YGDL_TYPE_BOOL));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_VEC2, YGDL_TYPE_INT8));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_VEC2, YGDL_TYPE_INT32));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_VEC2, YGDL_TYPE_INT64));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_VEC2, YGDL_TYPE_FLOAT));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_VEC2, YGDL_TYPE_DOUBLE));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_VEC2, YGDL_TYPE_STRING));
	REQUIRE(YGDL_type_is_convertible(YGDL_TYPE_VEC2, YGDL_TYPE_VEC3));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_VEC2, YGDL_TYPE_ARRAY));
	REQUIRE(!YGDL_type_is_convertible(YGDL_TYPE_VEC2, YGDL_TYPE_DICT));

	// string, vec3, dict and array are not convertible to anything
	for(auto i = int(YGDL_TYPE_VEC3); i <= int(YGDL_TYPE_DICT); i++) {
		for(auto j = int(YGDL_TYPE_NONE) + 1; j <= int(YGDL_TYPE_DICT); j++) {
			if(i != j) {
				REQUIRE(!YGDL_type_is_convertible(static_cast<YGDL_Type_t>(i), static_cast<YGDL_Type_t>(j)));
			}
		}
	}
}

TEST_CASE("YGDL_dict: create, destroy")
{
	YGDL_Dict* dict = YGDL_dict_create();
	REQUIRE(dict != nullptr);
	YGDL_dict_destroy(dict);
}

TEST_CASE("YGDL_dict: get (non-const)")
{
	YGDL_Dict* dict = YGDL_dict_create();

	// dict and array in separate test case

	// write bool
	{
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("bool"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_NONE);

		auto v = YGDL_dict_value_bool(value);
		REQUIRE(v != nullptr);
		REQUIRE(static_cast<bool>(*v) == false);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_BOOL);
		*v = true;
		REQUIRE(static_cast<bool>(*v) == true);
	}

	// read bool
	{
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("bool"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_BOOL);

		auto v = YGDL_dict_value_bool(value);
		REQUIRE(v != nullptr);
		REQUIRE(static_cast<bool>(*v) == true);
	}

	// write/read int
	{
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("int"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_NONE);

		auto v = YGDL_dict_value_int32(value);
		REQUIRE(v != nullptr);
		REQUIRE(*v == 0);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_INT32);
		*v = 5;
		REQUIRE(*v == 5);

		REQUIRE(5 == *YGDL_dict_value_int32(YGDL_dict_get(dict, YGDL_string_view_create("int"))));
	}

	// write/read int64
	{
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("int64"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_NONE);

		auto v = YGDL_dict_value_int64(value);
		REQUIRE(v != nullptr);
		REQUIRE(*v == 0);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_INT64);
		*v = 5;
		REQUIRE(*v == 5);

		REQUIRE(5 == *YGDL_dict_value_int64(YGDL_dict_get(dict, YGDL_string_view_create("int64"))));
	}

	// write/read float
	{
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("float"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_NONE);

		auto v = YGDL_dict_value_float(value);
		REQUIRE(v != nullptr);
		REQUIRE(*v == 0.f);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_FLOAT);
		*v = 4.2f;
		REQUIRE(*v == 4.2f);

		REQUIRE(4.2f == *YGDL_dict_value_float(YGDL_dict_get(dict, YGDL_string_view_create("float"))));
	}

	// write/read double
	{
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("double"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_NONE);

		auto v = YGDL_dict_value_double(value);
		REQUIRE(v != nullptr);
		REQUIRE(*v == 0.f);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_DOUBLE);
		*v = 4.2f;
		REQUIRE(*v == 4.2f);

		REQUIRE(4.2f == *YGDL_dict_value_double(YGDL_dict_get(dict, YGDL_string_view_create("double"))));
	}

	// write/read vec2
	{
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("vec2"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_NONE);

		auto v = YGDL_dict_value_vec2(value);
		REQUIRE(v != nullptr);
		REQUIRE(v->x == 0);
		REQUIRE(v->y == 0);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_VEC2);
		v->x = 1.f;
		v->y = 2.f;
		REQUIRE(v->x == 1);
		REQUIRE(v->y == 2);

		auto vv = YGDL_dict_value_vec2(YGDL_dict_get(dict, YGDL_string_view_create("vec2")));
		REQUIRE(vv->x == 1);
		REQUIRE(vv->y == 2);
	}

	// write/read vec3
	{
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("vec3"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_NONE);

		auto v = YGDL_dict_value_vec3(value);
		REQUIRE(v != nullptr);
		REQUIRE(v->x == 0);
		REQUIRE(v->y == 0);
		REQUIRE(v->z == 0);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_VEC3);
		v->x = 1.f;
		v->y = 2.f;
		v->z = 3.f;
		REQUIRE(v->x == 1);
		REQUIRE(v->y == 2);
		REQUIRE(v->z == 3);

		auto vv = YGDL_dict_value_vec3(YGDL_dict_get(dict, YGDL_string_view_create("vec3")));
		REQUIRE(vv->x == 1);
		REQUIRE(vv->y == 2);
		REQUIRE(vv->z == 3);
	}

	// write/read string
	{
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("string"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_NONE);

		auto v = YGDL_dict_value_string(value);
		REQUIRE(v != nullptr);
		REQUIRE(YGDL_string_size(v) == 0);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_STRING);
		YGDL_string_set(v, YGDL_string_view_create("value"));
		REQUIRE(std::strcmp(YGDL_string_data(v), "value") == 0);

		REQUIRE(std::strcmp(YGDL_string_data(YGDL_dict_value_string(
		                            YGDL_dict_get(dict, YGDL_string_view_create("string")))),
		                    "value")
		        == 0);
	}


	// read first bool (data has not been overwritten/corrupted)
	{
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("bool"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_BOOL);
		REQUIRE(static_cast<bool>(*YGDL_dict_value_bool(value)) == true);
	}

	// conversion
	{ // bool (true) -> int (1)
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("bool"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_BOOL);
		REQUIRE(*YGDL_dict_value_int32(value) == 1);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_INT32);
	}

	{ // int (1) -> bool (true)
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("bool"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_INT32);
		REQUIRE(static_cast<bool>(*YGDL_dict_value_bool(value)) == true);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_BOOL);
	}

	{ // bool (true) -> float (1.0)
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("bool"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_BOOL);
		REQUIRE(*YGDL_dict_value_float(value) == 1.f);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_FLOAT);
	}

	{ // float (1.0) -> int (1)
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("bool"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_FLOAT);
		REQUIRE(*YGDL_dict_value_int32(value) == 1);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_INT32);
	}

	{ // int (1) -> float (1.0)
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("bool"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_INT32);
		REQUIRE(*YGDL_dict_value_float(value) == 1.f);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_FLOAT);
	}

	{ // float (1.0) -> vec2 (1.f, 1.f)
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("bool"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_FLOAT);
		auto& v = *YGDL_dict_value_vec2(value);
		REQUIRE(v.x == 1.f);
		REQUIRE(v.y == 1.f);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_VEC2);
	}

	{ // float (4.2) -> vec3 (4.2, 4.2, 4.2)
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("float"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_FLOAT);
		auto& v = *YGDL_dict_value_vec3(value);
		REQUIRE(v.x == 4.2f);
		REQUIRE(v.y == 4.2f);
		REQUIRE(v.z == 4.2f);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_VEC3);
	}

	{ // vec2 (1.0, 2.0) -> vec3 (1.0, 2.0, 0.0)
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("bool"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_VEC2);
		YGDL_dict_value_vec2(value)->y = 2.f;
		auto& v                        = *YGDL_dict_value_vec3(value);
		REQUIRE(v.x == 1.f);
		REQUIRE(v.y == 2.f);
		REQUIRE(v.z == 0.f);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_VEC3);
	}

	{ // vec3 -> bool (false) -> string ("false")
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("bool"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_VEC3);
		REQUIRE(static_cast<bool>(*YGDL_dict_value_bool(value)) == false);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_BOOL);

		REQUIRE(std::string(YGDL_string_data(
		                YGDL_dict_value_string(YGDL_dict_get(dict, YGDL_string_view_create("bool")))))
		        == "false");
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_STRING);
	}

	{ // int (5) -> string ("false")
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("int"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_INT32);

		REQUIRE(std::string(YGDL_string_data(
		                YGDL_dict_value_string(YGDL_dict_get(dict, YGDL_string_view_create("int")))))
		        == "5");
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_STRING);
	}

	{ // float (4.2) -> string ("4.2")
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("float"));
		REQUIRE(value != nullptr);
		*YGDL_dict_value_float(value) = 4.2f;
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_FLOAT);

		REQUIRE(std::string(YGDL_string_data(
		                YGDL_dict_value_string(YGDL_dict_get(dict, YGDL_string_view_create("float")))))
		        == "4.2");
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_STRING);
	}

	YGDL_dict_destroy(dict);
}

TEST_CASE("YGDL_dict: get (const)")
{
	YGDL_Error       error = YGDL_error_create_success();
	YGDL_Dict*       dict  = YGDL_dict_create();
	const YGDL_Dict* cdict = dict;

	// init values for each type
	*YGDL_dict_value_bool(YGDL_dict_get(dict, YGDL_string_view_create("bool")))   = true;
	*YGDL_dict_value_int32(YGDL_dict_get(dict, YGDL_string_view_create("int")))   = 2;
	*YGDL_dict_value_float(YGDL_dict_get(dict, YGDL_string_view_create("float"))) = 5.f;
	*YGDL_dict_value_vec2(YGDL_dict_get(dict, YGDL_string_view_create("vec2")))   = YGDL_Vec2{1.f, 2.f};
	*YGDL_dict_value_vec3(YGDL_dict_get(dict, YGDL_string_view_create("vec3")))   = YGDL_Vec3{1.f, 2.f, 3.f};
	YGDL_string_set(YGDL_dict_value_string(YGDL_dict_get(dict, YGDL_string_view_create("string"))),
	                YGDL_string_view_create("value"));
	*YGDL_array_push_int32(
	        YGDL_dict_value_array(YGDL_dict_get(dict, YGDL_string_view_create("array")), YGDL_TYPE_INT32),
	        &error) = 1;
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	*YGDL_dict_value_int32(
	        YGDL_dict_get(YGDL_dict_value_dict(YGDL_dict_get(dict, YGDL_string_view_create("dict"))),
	                      YGDL_string_view_create("v"))) = 3;

	// bool: valid and invalid const read
	YGDL_error_reset(&error);
	REQUIRE(static_cast<bool>(*YGDL_dict_value_const_bool(
	                YGDL_dict_get_const(cdict, YGDL_string_view_create("bool")), &error))
	        == true);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(YGDL_dict_value_const_int32(YGDL_dict_get_const(cdict, YGDL_string_view_create("bool")), &error)
	        == nullptr);
	REQUIRE(error.code == YGDL_ERROR_CODE_TYPE_MISMATCH);

	// int32: valid and invalid const read
	YGDL_error_reset(&error);
	REQUIRE(*YGDL_dict_value_const_int32(YGDL_dict_get_const(cdict, YGDL_string_view_create("int")), &error) == 2);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(YGDL_dict_value_const_bool(YGDL_dict_get_const(cdict, YGDL_string_view_create("int")), &error)
	        == nullptr);
	REQUIRE(error.code == YGDL_ERROR_CODE_TYPE_MISMATCH);

	// float: valid and invalid const read
	YGDL_error_reset(&error);
	REQUIRE(*YGDL_dict_value_const_float(YGDL_dict_get_const(cdict, YGDL_string_view_create("float")), &error)
	        == 5.f);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(YGDL_dict_value_const_bool(YGDL_dict_get_const(cdict, YGDL_string_view_create("float")), &error)
	        == nullptr);
	REQUIRE(error.code == YGDL_ERROR_CODE_TYPE_MISMATCH);

	// vec2: valid and invalid const read
	YGDL_error_reset(&error);
	auto& v_vec2 =
	        *YGDL_dict_value_const_vec2(YGDL_dict_get_const(cdict, YGDL_string_view_create("vec2")), &error);
	REQUIRE(v_vec2.x == 1.f);
	REQUIRE(v_vec2.y == 2.f);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(YGDL_dict_value_const_vec3(YGDL_dict_get_const(cdict, YGDL_string_view_create("vec2")), &error)
	        == nullptr);
	REQUIRE(error.code == YGDL_ERROR_CODE_TYPE_MISMATCH);

	// vec3: valid and invalid const read
	YGDL_error_reset(&error);
	auto& v_vec3 =
	        *YGDL_dict_value_const_vec3(YGDL_dict_get_const(cdict, YGDL_string_view_create("vec3")), &error);
	REQUIRE(v_vec3.x == 1.f);
	REQUIRE(v_vec3.y == 2.f);
	REQUIRE(v_vec3.z == 3.f);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(YGDL_dict_value_const_vec2(YGDL_dict_get_const(cdict, YGDL_string_view_create("vec3")), &error)
	        == nullptr);
	REQUIRE(error.code == YGDL_ERROR_CODE_TYPE_MISMATCH);

	// string: valid and invalid const read
	YGDL_error_reset(&error);
	REQUIRE(std::strcmp(YGDL_string_const_data(YGDL_dict_value_const_string(
	                            YGDL_dict_get_const(cdict, YGDL_string_view_create("string")), &error)),
	                    "value")
	        == 0);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(YGDL_dict_value_const_bool(YGDL_dict_get_const(cdict, YGDL_string_view_create("string")), &error)
	        == nullptr);
	REQUIRE(error.code == YGDL_ERROR_CODE_TYPE_MISMATCH);

	// array: valid and invalid const read
	YGDL_error_reset(&error);
	auto array_error = YGDL_error_create_success();
	REQUIRE(*YGDL_array_get_const_int32(
	                YGDL_dict_value_const_array(
	                        YGDL_dict_get_const(cdict, YGDL_string_view_create("array")), YGDL_TYPE_INT32, &error),
	                0,
	                &array_error)
	        == 1);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(array_error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(YGDL_dict_value_const_bool(YGDL_dict_get_const(cdict, YGDL_string_view_create("array")), &error)
	        == nullptr);
	REQUIRE(error.code == YGDL_ERROR_CODE_TYPE_MISMATCH);

	// dict: valid and invalid const read
	YGDL_error_reset(&error);
	auto* inner_dict =
	        YGDL_dict_value_const_dict(YGDL_dict_get_const(cdict, YGDL_string_view_create("dict")), &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(*YGDL_dict_value_const_int32(YGDL_dict_get_const(inner_dict, YGDL_string_view_create("v")), &error)
	        == 3);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(YGDL_dict_value_const_bool(YGDL_dict_get_const(cdict, YGDL_string_view_create("dict")), &error)
	        == nullptr);
	REQUIRE(error.code == YGDL_ERROR_CODE_TYPE_MISMATCH);

	YGDL_dict_destroy(dict);
	YGDL_error_destroy(&error);
}

TEST_CASE("YGDL_dict: nested")
{
	YGDL_Error       error = YGDL_error_create_success();
	YGDL_Dict*       dict  = YGDL_dict_create();
	const YGDL_Dict* cdict = dict;

	// write
	{
		YGDL_Dict_value* value = YGDL_dict_get(dict, YGDL_string_view_create("inner"));
		REQUIRE(value != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_NONE);

		auto inner = YGDL_dict_value_dict(value);
		REQUIRE(inner != nullptr);
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_DICT);

		*YGDL_dict_value_bool(YGDL_dict_get(inner, YGDL_string_view_create("v1")))  = true;
		*YGDL_dict_value_int32(YGDL_dict_get(inner, YGDL_string_view_create("v2"))) = 2;
	}

	// read
	{
		const YGDL_Dict_value* value = YGDL_dict_get_const(cdict, YGDL_string_view_create("inner"));
		REQUIRE(YGDL_dict_value_type(value) == YGDL_TYPE_DICT);
		auto inner = YGDL_dict_value_const_dict(value, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

		auto v1 = YGDL_dict_value_const_bool(YGDL_dict_get_const(inner, YGDL_string_view_create("v1")), &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
		REQUIRE(static_cast<bool>(*v1) == true);

		auto v2 = YGDL_dict_value_const_int32(YGDL_dict_get_const(inner, YGDL_string_view_create("v2")), &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
		REQUIRE(*v2 == 2);
	}

	YGDL_dict_destroy(dict);
	YGDL_error_destroy(&error);
}

TEST_CASE("YGDL_dict: copy")
{
	YGDL_Error error = YGDL_error_create_success();
	YGDL_Dict* dict  = YGDL_dict_create();

	// set values
	*YGDL_dict_value_int32(YGDL_dict_get(dict, YGDL_string_view_create("v1"))) = 2;
	*YGDL_dict_value_float(YGDL_dict_get(dict, YGDL_string_view_create("v2"))) = 5.f;

	YGDL_Dict* copy = nullptr;

	SECTION("YGDL_dict_copy")
	{
		copy = YGDL_dict_copy(dict);
	}

	SECTION("YGDL_dict_copy_assign")
	{
		copy = YGDL_dict_create();

		*YGDL_dict_value_int32(YGDL_dict_get(copy, YGDL_string_view_create("v3"))) = 3;

		YGDL_dict_copy_assign(copy, dict);

		REQUIRE(YGDL_dict_value_type(YGDL_dict_get_const(copy, YGDL_string_view_create("v3"))) == YGDL_TYPE_NONE);
	}

	// validate copy
	auto v1 = YGDL_dict_value_const_int32(YGDL_dict_get_const(copy, YGDL_string_view_create("v1")), &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(*v1 == 2);
	auto v2 = YGDL_dict_value_const_float(YGDL_dict_get_const(copy, YGDL_string_view_create("v2")), &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(*v2 == 5.f);

	*YGDL_dict_value_float(YGDL_dict_get(copy, YGDL_string_view_create("v2"))) = 10.f;

	v1 = YGDL_dict_value_const_int32(YGDL_dict_get_const(copy, YGDL_string_view_create("v1")), &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(*v1 == 2);
	v2 = YGDL_dict_value_const_float(YGDL_dict_get_const(copy, YGDL_string_view_create("v2")), &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(*v2 == 10.f);

	v1 = YGDL_dict_value_const_int32(YGDL_dict_get_const(dict, YGDL_string_view_create("v1")), &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(*v1 == 2);
	v2 = YGDL_dict_value_const_float(YGDL_dict_get_const(dict, YGDL_string_view_create("v2")), &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(*v2 == 5.f);

	YGDL_dict_destroy(copy);
	YGDL_dict_destroy(dict);
	YGDL_error_destroy(&error);
}
