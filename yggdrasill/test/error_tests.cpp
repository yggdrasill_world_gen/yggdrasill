#include <catch2/catch.hpp>

#include "error_samples.hpp" // include to ensure that it compiles

#include <yggdrasill/error.h>

#include "helper.hpp"

#include <cstring>


TEST_CASE("YGDL_Error: create/destroy")
{
	YGDL_Error error = YGDL_error_create(42, YGDL_string_view_create("abcdefghijklmnop"));

	REQUIRE(error.code == 42);
	REQUIRE(YGDL_string_get(&error.message) == "abcdefghijklmnop");

	YGDL_error_destroy(&error);
}
