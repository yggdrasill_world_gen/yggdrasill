#include <catch2/catch.hpp>

#include <yggdrasill/string.h>

#include "helper.hpp"

#include <cstring>


TEST_CASE("YGDL_String: invalid argument")
{
	YGDL_String str = YGDL_string_copy(nullptr);
	REQUIRE(YGDL_string_get(&str) == "");

	REQUIRE(YGDL_string_get(nullptr) == "");
	REQUIRE(YGDL_string_size(nullptr) == 0);
	REQUIRE(YGDL_string_capacity(nullptr) == 0);

	REQUIRE(std::strcmp(YGDL_string_const_data(nullptr), "") == 0);
	REQUIRE(YGDL_string_data(nullptr) == nullptr);

	YGDL_string_reserve(nullptr, 0);
	YGDL_string_resize(nullptr, 0, 0);
	YGDL_string_set(nullptr, YGDL_String_view{});
	YGDL_string_append(nullptr, YGDL_String_view{});
}

TEST_CASE("YGDL_String: create, access, destroy")
{
	constexpr const auto str256_content =
	        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt "
	        "ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo "
	        "dolores et ea rebum. Stet clita kasd gubergren, no sea takimata ";

	YGDL_String negative_str  = YGDL_string_create(YGDL_String_view{nullptr, -1});
	YGDL_String null_str      = YGDL_string_create(YGDL_String_view{nullptr, 0});
	YGDL_String empty_str     = YGDL_string_create(YGDL_string_view_create(""));
	YGDL_String small_str_1   = YGDL_string_create(YGDL_string_view_create("a"));
	YGDL_String small_str_15  = YGDL_string_create(YGDL_string_view_create("abcdefghijklmno"));
	YGDL_String large_str_16  = YGDL_string_create(YGDL_string_view_create("abcdefghijklmnop"));
	YGDL_String large_str_256 = YGDL_string_create(YGDL_string_view_create(str256_content));
	YGDL_String zero_str      = YGDL_string_create(YGDL_String_view{"abc\0de", 6});
	YGDL_String sub_str       = YGDL_string_create(YGDL_String_view{"abcdefghijklmnop", 6});

	const YGDL_String* const_negative_str  = &negative_str;
	const YGDL_String* const_null_str      = &null_str;
	const YGDL_String* const_empty_str     = &empty_str;
	const YGDL_String* const_small_str_1   = &small_str_1;
	const YGDL_String* const_small_str_15  = &small_str_15;
	const YGDL_String* const_large_str_16  = &large_str_16;
	const YGDL_String* const_large_str_256 = &large_str_256;
	const YGDL_String* const_zero_str      = &zero_str;
	const YGDL_String* const_sub_str       = &sub_str;

	REQUIRE(YGDL_string_size(const_negative_str) == 0);
	REQUIRE(YGDL_string_size(const_null_str) == 0);
	REQUIRE(YGDL_string_size(const_empty_str) == 0);
	REQUIRE(YGDL_string_size(const_small_str_1) == 1);
	REQUIRE(YGDL_string_size(const_small_str_15) == 15);
	REQUIRE(YGDL_string_size(const_large_str_16) == 16);
	REQUIRE(YGDL_string_size(const_large_str_256) == 256);
	REQUIRE(YGDL_string_size(const_zero_str) == 6);
	REQUIRE(YGDL_string_size(const_sub_str) == 6);

	REQUIRE(YGDL_string_get(const_negative_str) == "");
	REQUIRE(YGDL_string_get(const_null_str) == "");
	REQUIRE(YGDL_string_get(const_empty_str) == "");
	REQUIRE(YGDL_string_get(const_small_str_1) == "a");
	REQUIRE(YGDL_string_get(const_small_str_15) == "abcdefghijklmno");
	REQUIRE(YGDL_string_get(const_large_str_16) == "abcdefghijklmnop");
	REQUIRE(YGDL_string_get(const_large_str_256) == str256_content);
	REQUIRE(std::memcmp(YGDL_string_get(const_zero_str).chars, "abc\0de", 6) == 0);
	REQUIRE(YGDL_string_get(const_sub_str) == "abcdef");

	REQUIRE(std::strcmp(YGDL_string_const_data(const_negative_str), "") == 0);
	REQUIRE(std::strcmp(YGDL_string_const_data(const_null_str), "") == 0);
	REQUIRE(std::strcmp(YGDL_string_const_data(const_empty_str), "") == 0);
	REQUIRE(std::strcmp(YGDL_string_const_data(const_small_str_1), "a") == 0);
	REQUIRE(std::strcmp(YGDL_string_const_data(const_small_str_15), "abcdefghijklmno") == 0);
	REQUIRE(std::strcmp(YGDL_string_const_data(const_large_str_16), "abcdefghijklmnop") == 0);
	REQUIRE(std::strcmp(YGDL_string_const_data(const_large_str_256), str256_content) == 0);
	REQUIRE(std::memcmp(YGDL_string_const_data(const_zero_str), "abc\0de", 6) == 0);
	REQUIRE(std::strcmp(YGDL_string_const_data(const_sub_str), "abcdef") == 0);

	YGDL_String large_str_16_copy = YGDL_string_copy(const_large_str_16);
	REQUIRE(YGDL_string_size(&large_str_16_copy) == 16);
	REQUIRE(YGDL_string_get(&large_str_16_copy) == "abcdefghijklmnop");
	YGDL_string_destroy(&large_str_16_copy);

	YGDL_string_destroy(&negative_str);
	YGDL_string_destroy(&null_str);
	YGDL_string_destroy(&empty_str);
	YGDL_string_destroy(&small_str_1);
	YGDL_string_destroy(&small_str_15);
	YGDL_string_destroy(&large_str_16);
	YGDL_string_destroy(&large_str_256);
}

TEST_CASE("YGDL_String: reserve")
{
	YGDL_String empty_str     = YGDL_string_create(YGDL_String_view{nullptr, 0});
	YGDL_String non_empty_str = YGDL_string_create(YGDL_string_view_create("abc"));

	REQUIRE(YGDL_string_capacity(&empty_str) == 15);
	REQUIRE(YGDL_string_get(&empty_str) == "");
	REQUIRE(YGDL_string_const_data(&empty_str)[0] == '\0');

	REQUIRE(YGDL_string_capacity(&non_empty_str) == 15);
	REQUIRE(YGDL_string_get(&non_empty_str) == "abc");
	REQUIRE(YGDL_string_const_data(&non_empty_str)[3] == '\0');


	YGDL_string_reserve(&empty_str, 15);
	REQUIRE(YGDL_string_capacity(&empty_str) == 15);
	REQUIRE(YGDL_string_get(&empty_str) == "");
	REQUIRE(YGDL_string_const_data(&empty_str)[0] == '\0');

	YGDL_string_reserve(&non_empty_str, 15);
	REQUIRE(YGDL_string_capacity(&non_empty_str) == 15);
	REQUIRE(YGDL_string_get(&non_empty_str) == "abc");
	REQUIRE(YGDL_string_const_data(&non_empty_str)[3] == '\0');


	YGDL_string_reserve(&empty_str, 16);
	REQUIRE(YGDL_string_capacity(&empty_str) == 17);
	REQUIRE(YGDL_string_get(&empty_str) == "");
	REQUIRE(YGDL_string_const_data(&empty_str)[0] == '\0');

	YGDL_string_reserve(&non_empty_str, 16);
	REQUIRE(YGDL_string_capacity(&non_empty_str) == 17);
	REQUIRE(YGDL_string_get(&non_empty_str) == "abc");
	REQUIRE(YGDL_string_const_data(&non_empty_str)[3] == '\0');


	YGDL_string_reserve(&empty_str, 256);
	REQUIRE(YGDL_string_capacity(&empty_str) == 257);
	REQUIRE(YGDL_string_get(&empty_str) == "");
	REQUIRE(YGDL_string_const_data(&empty_str)[0] == '\0');

	YGDL_string_reserve(&non_empty_str, 256);
	REQUIRE(YGDL_string_capacity(&non_empty_str) == 257);
	REQUIRE(YGDL_string_get(&non_empty_str) == "abc");
	REQUIRE(YGDL_string_const_data(&non_empty_str)[3] == '\0');

	YGDL_string_destroy(&non_empty_str);
	YGDL_string_destroy(&empty_str);
}

TEST_CASE("YGDL_String: resize")
{
	YGDL_String str = YGDL_string_create(YGDL_String_view{nullptr, 0});
	REQUIRE(YGDL_string_size(&str) == 0);
	REQUIRE(YGDL_string_get(&str) == "");

	YGDL_string_resize(&str, 15, 0);
	REQUIRE(YGDL_string_size(&str) == 15);
	REQUIRE(std::strcmp(YGDL_string_const_data(&str), "") == 0);

	YGDL_string_resize(&str, YGDL_SMALL_STRING_SIZE + 1, 0);
	REQUIRE(YGDL_string_size(&str) == (YGDL_SMALL_STRING_SIZE + 1));
	REQUIRE(std::strcmp(YGDL_string_const_data(&str), "") == 0);

	YGDL_string_resize(&str, 256, 0);
	REQUIRE(YGDL_string_size(&str) == 256);
	REQUIRE(std::strcmp(YGDL_string_const_data(&str), "") == 0);

	YGDL_string_destroy(&str);
}

TEST_CASE("YGDL_String: set")
{
	YGDL_String str = YGDL_string_create(YGDL_String_view{nullptr, 0});

	YGDL_string_set(&str, YGDL_string_view_create("a"));
	REQUIRE(YGDL_string_size(&str) == 1);
	REQUIRE(YGDL_string_get(&str) == "a");
	REQUIRE(YGDL_string_const_data(&str)[1] == '\0');

	YGDL_string_set(&str, YGDL_string_view_create("abcdefghijklmno"));
	REQUIRE(YGDL_string_size(&str) == 15);
	REQUIRE(YGDL_string_get(&str) == "abcdefghijklmno");
	REQUIRE(YGDL_string_const_data(&str)[15] == '\0');

	YGDL_string_set(&str, YGDL_string_view_create("abcdefghijklmnop"));
	REQUIRE(YGDL_string_size(&str) == 16);
	REQUIRE(YGDL_string_get(&str) == "abcdefghijklmnop");
	REQUIRE(YGDL_string_const_data(&str)[16] == '\0');

	YGDL_string_set(&str, YGDL_string_view_create("abcdefghijklmnopqrstu"));
	REQUIRE(YGDL_string_size(&str) == 21);
	REQUIRE(YGDL_string_get(&str) == "abcdefghijklmnopqrstu");
	REQUIRE(YGDL_string_const_data(&str)[21] == '\0');

	YGDL_string_set(&str, YGDL_string_view_create("a"));
	REQUIRE(YGDL_string_size(&str) == 1);
	REQUIRE(YGDL_string_get(&str) == "a");
	REQUIRE(YGDL_string_const_data(&str)[1] == '\0');

	YGDL_string_set(&str, YGDL_String_view{"abc\0de", 6});
	REQUIRE(YGDL_string_size(&str) == 6);
	REQUIRE(std::memcmp(YGDL_string_get(&str).chars, "abc\0de", 6) == 0);
	REQUIRE(YGDL_string_const_data(&str)[6] == '\0');

	YGDL_string_set(&str, YGDL_String_view{nullptr, -1});
	REQUIRE(YGDL_string_size(&str) == 0);
	REQUIRE(YGDL_string_get(&str) == "");
	REQUIRE(YGDL_string_const_data(&str)[0] == '\0');

	YGDL_string_destroy(&str);
}

TEST_CASE("YGDL_String: append")
{
	YGDL_String str = YGDL_string_create(YGDL_String_view{nullptr, 0});

	YGDL_string_append(&str, YGDL_string_view_create("a"));
	REQUIRE(YGDL_string_size(&str) == 1);
	REQUIRE(YGDL_string_get(&str) == "a");
	REQUIRE(YGDL_string_const_data(&str)[1] == '\0');

	YGDL_string_append(&str, YGDL_string_view_create("bc"));
	REQUIRE(YGDL_string_size(&str) == 3);
	REQUIRE(YGDL_string_get(&str) == "abc");
	REQUIRE(YGDL_string_const_data(&str)[3] == '\0');

	YGDL_string_append(&str, YGDL_string_view_create(""));
	REQUIRE(YGDL_string_size(&str) == 3);
	REQUIRE(YGDL_string_get(&str) == "abc");
	REQUIRE(YGDL_string_const_data(&str)[3] == '\0');

	YGDL_string_append(&str, YGDL_String_view{nullptr, -1});
	REQUIRE(YGDL_string_size(&str) == 3);
	REQUIRE(YGDL_string_get(&str) == "abc");
	REQUIRE(YGDL_string_const_data(&str)[3] == '\0');

	YGDL_string_append(&str, YGDL_String_view{"abc\0de", 6});
	REQUIRE(YGDL_string_size(&str) == 9);
	REQUIRE(std::memcmp(YGDL_string_get(&str).chars, "abcabc\0de", 9) == 0);
	REQUIRE(YGDL_string_const_data(&str)[9] == '\0');

	YGDL_string_append(&str, YGDL_string_view_create("fghijklmn"));
	REQUIRE(YGDL_string_size(&str) == 18);
	REQUIRE(std::memcmp(YGDL_string_get(&str).chars, "abcabc\0defghijklmn", 18) == 0);
	REQUIRE(YGDL_string_const_data(&str)[18] == '\0');

	YGDL_string_destroy(&str);
}

TEST_CASE("YGDL_String: Small-String-Optimization")
{
	YGDL_String str = YGDL_string_create(YGDL_string_view_create("aaaaaaaaaaaaaab"));
	CHECK(YGDL_string_size(&str) == 15);
	CHECK(YGDL_string_capacity(&str) == 15);
	CHECK(YGDL_string_get(&str) == "aaaaaaaaaaaaaab");
	CHECK(YGDL_string_const_data(&str)[15] == '\0');
}
