#pragma once

#include <yggdrasill/string.h>

#include <cstdint>
#include <cstring>

inline bool operator==(YGDL_String_view view, const char* str)
{
	const auto len = int32_t(strlen(str));
	if(view.length != len)
		return false;

	return std::strcmp(view.chars, str) == 0;
}

template <std::size_t S>
inline bool operator==(YGDL_String_view view, const char (&str)[S])
{
	const auto len = int32_t(S);
	if(view.length != len)
		return false;

	return std::strcmp(view.chars, str) == 0;
}
