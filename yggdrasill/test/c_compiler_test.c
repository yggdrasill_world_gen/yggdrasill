#include <yggdrasill/array.h>
#include <yggdrasill/commons.h>
#include <yggdrasill/dictionary.h>
#include <yggdrasill/error.h>
#include <yggdrasill/generator.h>
#include <yggdrasill/mesh.h>
#include <yggdrasill/string.h>
#include <yggdrasill/world.h>

static const YGDL_Module_info c_comp_test_module = {{"c_comp_test_module", 18}, NULL, NULL, NULL, NULL};

YGDL_REGISTER_PLUGIN_MODULES(&c_comp_test_module)
