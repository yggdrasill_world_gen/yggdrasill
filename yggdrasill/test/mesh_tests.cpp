#include <catch2/catch.hpp>

#include <yggdrasill/mesh.h>
#include <yggdrasill/world.h>

SCENARIO("YGDL_Mesh: creation")
{
	auto error = YGDL_error_create_success();
	auto world = YGDL_world_create();

	auto mesh = YGDL_world_mesh_acquire(world, nullptr);
	REQUIRE(mesh != nullptr);
	REQUIRE(mesh->vertex_count == 0);
	REQUIRE(mesh->face_count == 0);
	REQUIRE(mesh->edge_count == 0);
	REQUIRE(mesh->vertex_edge == nullptr);
	REQUIRE(mesh->face_edge == nullptr);
	REQUIRE(mesh->primal_edge_origin == nullptr);
	REQUIRE(mesh->primal_edge_next == nullptr);
	REQUIRE(mesh->dual_edge_origin == nullptr);
	REQUIRE(mesh->dual_edge_next == nullptr);

	auto vertex = YGDL_Vertex{};
	auto count  = YGDL_mesh_vertex_add(mesh, 1, &vertex, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(count == 1);
	CHECK(vertex.index == 0);

	YGDL_world_mesh_release(world, mesh, nullptr);
	YGDL_world_destroy(world);
	YGDL_error_destroy(&error);
}
