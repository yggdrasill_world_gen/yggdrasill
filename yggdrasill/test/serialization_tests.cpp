#include <yggdrasill/serialization.h>
#include <yggdrasill/string.hpp>

#include <catch2/catch.hpp>

#include <filesystem>
#include <span>


TEST_CASE("YGDL_Serializer: create")
{
	if(!YGDL_serialization_supported())
		return; // skip test if we are building without serialization support

	const auto path = std::filesystem::temp_directory_path() / "yggdrasill_serializer_a.zip";
	std::filesystem::remove(path);

	YGDL_Error error = YGDL_error_create_success();
	auto       s     = YGDL_serializer_create(
            YGDL_string_view_create(path.generic_string().c_str()), YGDL_TRUE, YGDL_TRUE, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(s != nullptr);

	YGDL_serializer_destroy(s);
	YGDL_error_destroy(&error);

	CHECK(std::filesystem::exists(path));
	std::filesystem::remove(path);
}


TEST_CASE("YGDL_Serializer: userdata")
{
	if(!YGDL_serialization_supported())
		return; // skip test if we are building without serialization support

	const auto path = std::filesystem::temp_directory_path() / "yggdrasill_serializer_b.zip";
	std::filesystem::remove(path);

	YGDL_Error error = YGDL_error_create_success();

	auto s = YGDL_serializer_create(
	        YGDL_string_view_create(path.generic_string().c_str()), YGDL_TRUE, YGDL_TRUE, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(s != nullptr);

	auto test_data_data_a = std::vector<std::byte>();
	for(int i = 0; i < 512; ++i) {
		test_data_data_a.push_back(std::byte(0xde));
		test_data_data_a.push_back(std::byte(0xad));
	}


	YGDL_serializer_userdata_write(
	        s, YGDL_string_view_create("test_data_a"), test_data_data_a.data(), test_data_data_a.size(), &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

	auto read_function = [](void*, void* reader_ctx, YGDL_Serializer_reader_req* read) {
		auto buffer = std::array<std::byte, 3>();
		auto c      = read(reader_ctx, 2, buffer.data());
		CHECK(c == 2);
		CHECK(buffer[0] == std::byte(0xde));
		CHECK(buffer[1] == std::byte(0xad));

		c = read(reader_ctx, 1, &buffer[2]);
		CHECK(c == 1);
		CHECK(buffer[2] == std::byte(0xde));

		auto remaining = read(reader_ctx, 0, nullptr);
		REQUIRE(remaining == 1024 - 3);
		auto data = std::vector<std::byte>();
		data.resize(remaining);
		c = read(reader_ctx, remaining, data.data());
		REQUIRE(c == remaining);
		for(std::size_t i = 1; i < data.size(); i += 2) {
			CHECK(data[i - 1] == std::byte(0xad));
			CHECK(data[i] == std::byte(0xde));
		}
	};
	YGDL_serializer_userdata_read(s, YGDL_string_view_create("test_data_a"), nullptr, read_function, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

	YGDL_serializer_flush(s, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

	YGDL_serializer_userdata_write(
	        s, YGDL_string_view_create("test_data_b"), test_data_data_a.data(), test_data_data_a.size(), &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

	int found_mask = 0;
	YGDL_serializer_userdata_iterate(s, &found_mask, [](void* ctx, YGDL_String_view name) {
		auto& m        = *static_cast<int*>(ctx);
		auto  name_str = yggdrasill::detail::from_ygdl_string_view(name);
		CHECK((name_str == "test_data_a" || name_str == "test_data_b"));

		if(yggdrasill::detail::from_ygdl_string_view(name) == "test_data_a")
			m |= 0b01;
		else if(yggdrasill::detail::from_ygdl_string_view(name) == "test_data_b")
			m |= 0b10;
	});
	CHECK(found_mask == 0b11);

	YGDL_serializer_destroy(s);
	YGDL_error_destroy(&error);

	CHECK(std::filesystem::exists(path));
	std::filesystem::remove(path);
}
