#pragma once

#include <yggdrasill/error.h>

#include "helper.hpp"

#include <cstdio>
#include <cstring>


/// \def_example{error_handling_set_c}
static int YGDL_example_function_that_might_fail(YGDL_Error* error)
{
	YGDL_error_set(error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, YGDL_string_view_create("Error message"));
	return 0;
}

/// \def_example{error_handling_c}
static void error_handling_c()
{
	YGDL_Error error = YGDL_error_create_success();

	int result = YGDL_example_function_that_might_fail(&error);
	if(error.code != YGDL_ERROR_CODE_SUCCESS) {
		printf("Operation failed: %s", YGDL_string_const_data(&error.message));
		// ... handle the error ...
		return;
	}

	// ... use result ...
	printf("%i", result);
}