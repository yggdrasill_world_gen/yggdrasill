#include <catch2/catch.hpp>

#include <yggdrasill/array.h>
#include <yggdrasill/dictionary.h>

#include <array>
#include <cstring>

TEST_CASE("YGDL_Array: allocation")
{
	YGDL_Dict*  dict = YGDL_dict_create();
	YGDL_Array* array =
	        YGDL_dict_value_array(YGDL_dict_get(dict, YGDL_string_view_create("array")), YGDL_TYPE_INT32);
	REQUIRE(array != nullptr);

	YGDL_Error error = YGDL_error_create_success();
	(void) YGDL_dict_value_const_array(
	        YGDL_dict_get(dict, YGDL_string_view_create("array")), YGDL_TYPE_BOOL, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_TYPE_MISMATCH);
	YGDL_error_destroy(&error);

	YGDL_dict_destroy(dict);
}

namespace {
	struct array_bool {
		static constexpr auto type      = YGDL_TYPE_BOOL;
		static constexpr auto push      = &YGDL_array_push_bool;
		static constexpr auto get       = &YGDL_array_get_bool;
		static constexpr auto get_const = &YGDL_array_get_const_bool;

		std::array<bool, 5> values = {true, false, true, true, false};

		void init(int i, YGDL_Bool& val) { val = values[i]; }

		bool validate(int i, const YGDL_Bool& val) { return static_cast<bool>(val) == values[i]; }
	};

	struct array_int8 {
		static constexpr auto type      = YGDL_TYPE_INT8;
		static constexpr auto push      = &YGDL_array_push_int8;
		static constexpr auto get       = &YGDL_array_get_int8;
		static constexpr auto get_const = &YGDL_array_get_const_int8;

		std::array<std::int8_t, 5> values = {5, 2, 1, -5, 80};

		void init(int i, std::int8_t& val) { val = values[i]; }

		bool validate(int i, const std::int8_t& val) { return val == values[i]; }
	};

	struct array_int32 {
		static constexpr auto type      = YGDL_TYPE_INT32;
		static constexpr auto push      = &YGDL_array_push_int32;
		static constexpr auto get       = &YGDL_array_get_int32;
		static constexpr auto get_const = &YGDL_array_get_const_int32;

		std::array<std::int32_t, 5> values = {5, 2, 1, -5, 80};

		void init(int i, std::int32_t& val) { val = values[i]; }

		bool validate(int i, const std::int32_t& val) { return val == values[i]; }
	};

	struct array_int64 {
		static constexpr auto type      = YGDL_TYPE_INT64;
		static constexpr auto push      = &YGDL_array_push_int64;
		static constexpr auto get       = &YGDL_array_get_int64;
		static constexpr auto get_const = &YGDL_array_get_const_int64;

		std::array<std::int64_t, 5> values = {5, 2, 1, -5, 80};

		void init(int i, std::int64_t& val) { val = values[i]; }

		bool validate(int i, const std::int64_t& val) { return val == values[i]; }
	};

	struct array_float {
		static constexpr auto type      = YGDL_TYPE_FLOAT;
		static constexpr auto push      = &YGDL_array_push_float;
		static constexpr auto get       = &YGDL_array_get_float;
		static constexpr auto get_const = &YGDL_array_get_const_float;

		std::array<float, 5> values = {5.2f, 2.6f, 1.f, -5.f, 80.f};

		void init(int i, float& val) { val = values[i]; }

		bool validate(int i, const float& val) { return val == values[i]; }
	};

	struct array_double {
		static constexpr auto type      = YGDL_TYPE_DOUBLE;
		static constexpr auto push      = &YGDL_array_push_double;
		static constexpr auto get       = &YGDL_array_get_double;
		static constexpr auto get_const = &YGDL_array_get_const_double;

		std::array<double, 5> values = {5.2f, 2.6f, 1.f, -5.f, 80.f};

		void init(int i, double& val) { val = values[i]; }

		bool validate(int i, const double& val) { return val == values[i]; }
	};

	struct array_vec2 {
		static constexpr auto type      = YGDL_TYPE_VEC2;
		static constexpr auto push      = &YGDL_array_push_vec2;
		static constexpr auto get       = &YGDL_array_get_vec2;
		static constexpr auto get_const = &YGDL_array_get_const_vec2;

		std::array<YGDL_Vec2, 5> values = {YGDL_Vec2{1.f, 2.f},
		                                   YGDL_Vec2{3.f, 4.f},
		                                   YGDL_Vec2{5.f, 6.f},
		                                   YGDL_Vec2{7.f, 8.f},
		                                   YGDL_Vec2{9.f, 10.f}};

		void init(int i, YGDL_Vec2& val) { val = values[i]; }

		bool validate(int i, const YGDL_Vec2& val) { return val.x == values[i].x && val.y == values[i].y; }
	};

	struct array_vec3 {
		static constexpr auto type      = YGDL_TYPE_VEC3;
		static constexpr auto push      = &YGDL_array_push_vec3;
		static constexpr auto get       = &YGDL_array_get_vec3;
		static constexpr auto get_const = &YGDL_array_get_const_vec3;

		std::array<YGDL_Vec3, 5> values = {YGDL_Vec3{1.f, 2.f, -1.f},
		                                   YGDL_Vec3{3.f, 4.f, -2.f},
		                                   YGDL_Vec3{5.f, 6.f, -3.f},
		                                   YGDL_Vec3{7.f, 8.f, -4.f},
		                                   YGDL_Vec3{9.f, 10.f, -5.f}};

		void init(int i, YGDL_Vec3& val) { val = values[i]; }

		bool validate(int i, const YGDL_Vec3& val)
		{
			return val.x == values[i].x && val.y == values[i].y && val.z == values[i].z;
		}
	};

	struct array_string {
		static constexpr auto type      = YGDL_TYPE_STRING;
		static constexpr auto push      = &YGDL_array_push_string;
		static constexpr auto get       = &YGDL_array_get_string;
		static constexpr auto get_const = &YGDL_array_get_const_string;

		std::array<std::string, 5> values = {
		        "test1", "long test string that longer than 16 characters", "test2", "4534", ""};

		void init(int i, YGDL_String& val)
		{
			YGDL_string_set(&val,
			                YGDL_String_view{values[i].data(), static_cast<std::int32_t>(values[i].size())});
		}

		bool validate(int i, const YGDL_String& val)
		{
			return std::strcmp(YGDL_string_const_data(&val), values[i].data()) == 0;
		}
	};
} // namespace

TEMPLATE_TEST_CASE("YGDL_Array: push and get",
                   "",
                   array_bool,
                   array_int8,
                   array_int32,
                   array_int64,
                   array_float,
                   array_double,
                   array_vec2,
                   array_vec3,
                   array_string)
{
	auto current_type = TestType{};

	YGDL_Error error = YGDL_error_create_success();
	YGDL_Dict* dict  = YGDL_dict_create();

	YGDL_Array* array =
	        YGDL_dict_value_array(YGDL_dict_get(dict, YGDL_string_view_create("array")), TestType::type);

	REQUIRE(YGDL_array_type(array) == TestType::type);

	// write values
	auto v = TestType::push(array, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	current_type.init(0, *v);

	YGDL_array_reserve(array, 3, YGDL_TYPE_NONE);
	REQUIRE(array->capacity >= 3);

	v = TestType::push(array, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	current_type.init(1, *v);

	v = TestType::push(array, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	current_type.init(2, *v);

	v = TestType::push(array, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	current_type.init(3, *v);

	v = TestType::push(array, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	current_type.init(4, *v);

	// invalid push
	YGDL_array_push_array(array, YGDL_TYPE_STRING, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_TYPE_MISMATCH);
	YGDL_error_reset(&error);

	// validate size
	auto size = YGDL_array_size(array);
	REQUIRE(size == 5);

	// validate values (non-const)
	auto* data = TestType::get(array, 0, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	for(auto i = 0; i < size; i++) {
		REQUIRE(current_type.validate(i, data[i]));
	}

	// validate values (const)
	auto* const_data = TestType::get_const(array, 0, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	for(auto i = 0; i < size; i++) {
		REQUIRE(current_type.validate(i, const_data[i]));
	}

	// invalid get
	(void) YGDL_array_get_array(array, 0, YGDL_TYPE_ARRAY, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_TYPE_MISMATCH);
	YGDL_error_reset(&error);

	// copy
	auto        copy = YGDL_dict_copy(dict);
	YGDL_Array* array_copy =
	        YGDL_dict_value_array(YGDL_dict_get(copy, YGDL_string_view_create("array")), TestType::type);
	const_data = TestType::get_const(array_copy, 0, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	for(auto i = 0; i < size; i++) {
		REQUIRE(current_type.validate(i, const_data[i]));
	}
	YGDL_dict_destroy(copy);

	// out-of-bounds access
	data = TestType::get(array, size, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS);

	YGDL_dict_destroy(dict);
	YGDL_error_destroy(&error);
}

TEST_CASE("YGDL_Array: resize")
{
	YGDL_Error  error = YGDL_error_create_success();
	YGDL_Dict*  dict  = YGDL_dict_create();
	YGDL_Array* array =
	        YGDL_dict_value_array(YGDL_dict_get(dict, YGDL_string_view_create("array")), YGDL_TYPE_INT32);

	*YGDL_array_push_int32(array, &error) = 5;
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

	YGDL_array_resize(array, 17, YGDL_TYPE_NONE);
	REQUIRE(YGDL_array_size(array) == 17);
	REQUIRE(*YGDL_array_get_int32(array, 0, &error) == 5);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

	REQUIRE(*YGDL_array_get_int32(array, 16, &error) == 0);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

	YGDL_array_get_int32(array, 17, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS);
	YGDL_error_reset(&error);

	YGDL_array_resize(array, 1, YGDL_TYPE_NONE);
	REQUIRE(YGDL_array_size(array) == 1);
	REQUIRE(*YGDL_array_get_int32(array, 0, &error) == 5);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

	YGDL_dict_destroy(dict);
	YGDL_error_destroy(&error);
}

TEST_CASE("YGDL_Array: erase + clear")
{
	YGDL_Error  error = YGDL_error_create_success();
	YGDL_Dict*  dict  = YGDL_dict_create();
	YGDL_Array* array =
	        YGDL_dict_value_array(YGDL_dict_get(dict, YGDL_string_view_create("array")), YGDL_TYPE_INT32);

	for(int i = 0; i < 21; i++) {
		*YGDL_array_push_int32(array, &error) = i;
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	}

	YGDL_array_erase_ordered(array, 21, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS);
	YGDL_error_reset(&error);

	YGDL_array_erase_ordered(array, 15, &error);
	REQUIRE(YGDL_array_size(array) == 20);

	REQUIRE(*YGDL_array_get_int32(array, 15, &error) == 16);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

	auto data = YGDL_array_get_int32(array, 0, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(std::is_sorted(data, data + 20));

	YGDL_array_erase_unordered(array, 7, &error);
	REQUIRE(YGDL_array_size(array) == 19);

	std::sort(data, data + 19);
	for(int index = 0, value = 0; index < 19; value++) {
		if(value != 15 && value != 7) {
			REQUIRE(data[index] == value);
			index++;
		}
	}

	YGDL_array_clear(array);
	REQUIRE(YGDL_array_size(array) == 0);

	YGDL_array_clear(array);

	*YGDL_array_push_int32(array, &error) = 42;
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	REQUIRE(YGDL_array_size(array) == 1);

	YGDL_dict_destroy(dict);
	YGDL_error_destroy(&error);
}
