#include <catch2/catch.hpp>

#include <yggdrasill/mesh.h>
#include <yggdrasill/world.h>

#include "helper.hpp"

#include <array>
#include <cstring>


TEST_CASE("YGDL_World")
{
	YGDL_Error  error = YGDL_error_create_success();
	YGDL_World* world = YGDL_world_create();

	REQUIRE(YGDL_world_mesh(world) != nullptr);
	REQUIRE(YGDL_world_mesh(world)->vertex_count == 0);

	auto version = YGDL_world_mesh_version(world);

	{
		YGDL_Mesh* mesh = YGDL_world_mesh_acquire(world, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
		YGDL_mesh_vertex_add(mesh, 2, nullptr, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
		REQUIRE(mesh->vertex_count == 2);
		YGDL_world_mesh_release(world, mesh, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	}
	CHECK(YGDL_world_mesh_version(world) != version);
	version = YGDL_world_mesh_version(world);

	YGDL_World* copy      = YGDL_world_copy(world);
	auto        copy_mesh = YGDL_world_mesh(copy);

	REQUIRE(copy_mesh->vertex_count == 2);

	{
		YGDL_Mesh* mesh = YGDL_world_mesh_acquire(world, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
		YGDL_mesh_vertex_add(mesh, 2, nullptr, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
		REQUIRE(mesh->vertex_count == 4);
		YGDL_world_mesh_release(world, mesh, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	}
	CHECK(YGDL_world_mesh_version(world) != version);
	version = YGDL_world_mesh_version(world);

	REQUIRE(copy_mesh == YGDL_world_mesh(copy));
	REQUIRE(copy_mesh->vertex_count == 2);

	YGDL_world_destroy(world);

	REQUIRE(copy_mesh == YGDL_world_mesh(copy));
	REQUIRE(copy_mesh->vertex_count == 2);

	{
		YGDL_Mesh* mesh = YGDL_world_mesh_acquire(copy, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
		YGDL_mesh_vertex_add(mesh, 1, nullptr, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
		REQUIRE(YGDL_world_mesh(copy)->vertex_count == 3);
		YGDL_world_mesh_release(copy, mesh, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	}

	YGDL_world_destroy(copy);
	YGDL_error_destroy(&error);
}

TEST_CASE("YGDL_Random")
{
	YGDL_World* world = YGDL_world_create();

	YGDL_Random* rand = YGDL_world_random(world);

	YGDL_Seed seed{{2148334203, 346516426, 1904038130, 3887960964}};
	YGDL_random_seed(rand, &seed);

	float fv;
	YGDL_random_uniform_float(rand, 0.f, 1.f, 1, &fv);
	CHECK(fv == Approx(0x1.883498p-3));

	int32_t iv[3];
	YGDL_random_uniform_int32(rand, -10, 10, 3, iv);
	CHECK(iv[0] == -8);
	CHECK(iv[1] == -7);
	CHECK(iv[2] == 10);

	int64_t iv64[3];
	YGDL_random_uniform_int64(
	        rand, std::numeric_limits<int64_t>::min(), std::numeric_limits<int64_t>::max(), 3, iv64);
	CHECK(iv64[0] == 5020232752677908944);
	CHECK(iv64[1] == 6346357656833618384);
	CHECK(iv64[2] == -3507605163145404295);

	YGDL_Bool bv[3];
	YGDL_random_bool(rand, 0.5f, 3, bv);
	CHECK(bv[0] == YGDL_FALSE);
	CHECK(bv[1] == YGDL_FALSE);
	CHECK(bv[2] == YGDL_TRUE);

	YGDL_world_destroy(world);
}

TEST_CASE("YGDL_Layer")
{
	YGDL_Error  error = YGDL_error_create_success();
	YGDL_World* world = YGDL_world_create();

	// add 2 vertices
	{
		YGDL_Mesh* mesh = YGDL_world_mesh_acquire(world, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

		YGDL_mesh_vertex_add(mesh, 2, nullptr, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

		YGDL_world_mesh_release(world, mesh, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	}

	auto layer_def = YGDL_Layer_definition_create(
	        YGDL_string_view_create("test_layer"), YGDL_TYPE_FLOAT, YGDL_LAYER_REF_TYPE_VERTEX);
	layer_def.description            = YGDL_string_view_create("my test layer");
	layer_def.value_info.float_value = {.min       = -1.f,
	                                    .initial   = 0,
	                                    .max       = 1,
	                                    .has_min   = YGDL_TRUE,
	                                    .has_max   = YGDL_TRUE,
	                                    .allow_nan = YGDL_FALSE,
	                                    .allow_inf = YGDL_FALSE};

	CHECK(YGDL_world_layer(world, &layer_def) == nullptr);

	{
		auto layer = YGDL_world_layer_acquire(world, &layer_def, &error);
		CHECK(layer != nullptr);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);

		CHECK(layer->length == 2);
		CHECK(YGDL_world_layer_values_int32(layer) == nullptr);

		auto data = YGDL_world_layer_values_float(layer);
		CHECK(data != nullptr);
		CHECK(data[0] == 0);
		CHECK(data[1] == 0);
		data[1] = 1.f;

		auto layer_const = YGDL_world_layer(world, &layer_def);
		CHECK(layer_const != nullptr);
		CHECK(layer_const->length == 2);
		CHECK(YGDL_world_layer_values_const_float(layer_const)[1] == 1.f);

		YGDL_world_layer_release(world, layer, &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	}

	{
		auto size = YGDL_Index(0);
		CHECK(!YGDL_world_layer_list(world, &size, nullptr));
		REQUIRE(size == 1);

		auto data = std::vector<const YGDL_Layer*>(size);
		CHECK(YGDL_world_layer_list(world, &size, data.data()));
		REQUIRE(size == 1);

		for(auto& layer : data) {
			CHECK(layer != nullptr);
			CHECK(layer->length == 2);
			CHECK(YGDL_world_layer_values_const_float(layer)[1] == 1.f);
		}
	}

	YGDL_World* copy = YGDL_world_copy(world);

	YGDL_world_destroy(world);

	auto layer_const = YGDL_world_layer(copy, &layer_def);
	CHECK(layer_const != nullptr);
	CHECK(layer_const->length == 2);
	CHECK(YGDL_world_layer_values_const_float(layer_const)[1] == 1.f);

	auto wrong_layer_def      = layer_def;
	wrong_layer_def.data_type = YGDL_TYPE_INT32;
	CHECK(YGDL_world_layer(copy, &wrong_layer_def) == nullptr);

	wrong_layer_def.data_type = YGDL_TYPE_FLOAT;
	wrong_layer_def.ref_type  = YGDL_LAYER_REF_TYPE_FACE;
	CHECK(YGDL_world_layer(copy, &wrong_layer_def) == nullptr);

	YGDL_world_layer_remove(copy, YGDL_string_view_create("test_layer"));
	CHECK(YGDL_world_layer(copy, &layer_def) == nullptr);

	YGDL_world_destroy(copy);
	YGDL_error_destroy(&error);
}

TEST_CASE("YGDL_Layer (unstructured)")
{
	YGDL_Error  error = YGDL_error_create_success();
	YGDL_World* world = YGDL_world_create();

	auto version = YGDL_world_unstructured_layer_version(world, YGDL_string_view_create("test_layer"));
	{
		auto layer = YGDL_world_unstructured_layer_acquire(world, YGDL_string_view_create("test_layer"), &error);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
		CHECK(layer != nullptr);

		auto layer_const = YGDL_world_unstructured_layer(world, YGDL_string_view_create("test_layer"));
		CHECK(layer_const == layer);

		YGDL_world_unstructured_layer_release(world, layer);
		REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	}

	CHECK(version != YGDL_world_unstructured_layer_version(world, YGDL_string_view_create("test_layer")));
	version = YGDL_world_unstructured_layer_version(world, YGDL_string_view_create("test_layer"));

	{
		auto size = YGDL_Index(0);
		CHECK(!YGDL_world_unstructured_layer_list(world, &size, nullptr));
		REQUIRE(size == 1);

		auto data = std::vector<YGDL_String_view>(size);
		CHECK(YGDL_world_unstructured_layer_list(world, &size, data.data()));
		REQUIRE(size == 1);
		CHECK(data[0] == "test_layer");
	}

	CHECK(YGDL_world_unstructured_layer(world, YGDL_string_view_create("test_layer")) != nullptr);
	YGDL_world_unstructured_layer_remove(world, YGDL_string_view_create("test_layer"));
	CHECK(YGDL_world_unstructured_layer(world, YGDL_string_view_create("test_layer")) == nullptr);

	CHECK(version != YGDL_world_unstructured_layer_version(world, YGDL_string_view_create("test_layer")));

	{
		auto size = YGDL_Index(0);
		CHECK(!YGDL_world_unstructured_layer_list(world, &size, nullptr));
		REQUIRE(size == 0);

		auto data = std::vector<YGDL_String_view>(size);
		YGDL_world_unstructured_layer_list(world, &size, data.data());
		REQUIRE(size == 0);
	}

	YGDL_world_destroy(world);
	YGDL_error_destroy(&error);
}
