#include <catch2/catch.hpp>

#include <yggdrasill/generator.h>

#include "helper.hpp"


TEST_CASE("YGDL_Generator: creation")
{
	YGDL_Generator* gen = YGDL_generator_create();
	YGDL_generator_destroy(gen);
}

TEST_CASE("YGDL_Generator: module")
{
	YGDL_Error error = YGDL_error_create_success();

	YGDL_Generator* gen = YGDL_generator_create();

	// clang-format off
	auto mod_info = YGDL_Module_info{
		YGDL_string_view_create("my_module"),
		+[](YGDL_Error*) {return reinterpret_cast<void*>(42);},
		+[](void* userdata) {CHECK(reinterpret_cast<std::intptr_t>(userdata)==42);},
		+[](void* userdata, YGDL_Dict* dict, YGDL_Error* error) {
			CHECK(reinterpret_cast<std::intptr_t>(userdata)==42);
			REQUIRE(dict!=nullptr);
			CHECK(error!=nullptr);

				auto& dict_val = *YGDL_dict_value_int32(YGDL_dict_get(dict, YGDL_string_view_create("val")));
				CHECK(dict_val==42);
				dict_val = 23;
		},
		+[](void* userdata, YGDL_World* world, const YGDL_Dict* dict, YGDL_Error* error) {
			CHECK(reinterpret_cast<std::intptr_t>(userdata)==42);
			REQUIRE(dict!=nullptr);
			CHECK(error!=nullptr);
			
			YGDL_Error dict_error = YGDL_error_create_success();
			auto& dict_val = *YGDL_dict_value_const_int32(YGDL_dict_get_const(dict, YGDL_string_view_create("val")), &dict_error);
			REQUIRE(dict_error.code == YGDL_ERROR_CODE_SUCCESS);
			CHECK(dict_val==23);
			
			REQUIRE(world != nullptr);
			const YGDL_Mesh* mesh = YGDL_world_mesh(world);
			REQUIRE(mesh != nullptr);
			
			YGDL_error_set(error, YGDL_ERROR_CODE_MODULE_ERRORS, YGDL_string_view_create(""));
		}
	};
	
	auto mod_info2 = YGDL_Module_info{
		YGDL_string_view_create("my_module"),
		nullptr,nullptr,
		+[](void*, YGDL_Dict* dict, YGDL_Error* error) {
		},
		+[](void*, YGDL_World* world, const YGDL_Dict* dict, YGDL_Error* error) {
		}
	};
	// clang-format on

	// construction
	auto module = YGDL_generator_module_add(gen, &mod_info, &error);
	REQUIRE(module != nullptr);
	CHECK(module == YGDL_generator_module_add(gen, &mod_info, &error));
	REQUIRE(error.code == YGDL_ERROR_CODE_INVALID_PLUGIN);
	YGDL_error_reset(&error);

	CHECK(module == YGDL_generator_module_get(gen, YGDL_string_view_create("my_module")));
	CHECK(nullptr == YGDL_generator_module_get(gen, YGDL_string_view_create("not a module")));
	CHECK(YGDL_generator_module_id(module) == "my_module");

	auto list = YGDL_generator_module_list(gen);
	CHECK(list.count == 1);
	CHECK(YGDL_generator_module_id(list.modules[0]) == "my_module");

	auto reloaded = YGDL_generator_module_reload(gen, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	CHECK(reloaded.count == 0);

	YGDL_generator_module_add(gen, &mod_info2, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_INVALID_PLUGIN);
	YGDL_error_reset(&error);

	// validation and execution
	auto dict                        = YGDL_dict_create();
	auto dict_val                    = YGDL_dict_get(dict, YGDL_string_view_create("val"));
	*YGDL_dict_value_int32(dict_val) = 42;

	YGDL_World* world = YGDL_world_create();

	YGDL_generator_module_validate(module, dict, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_SUCCESS);
	YGDL_generator_module_execute(module, world, dict, &error);
	REQUIRE(error.code == YGDL_ERROR_CODE_MODULE_ERRORS);

	// destruction
	YGDL_dict_destroy(dict);
	YGDL_world_destroy(world);
	YGDL_generator_destroy(gen);
	YGDL_error_destroy(&error);
}
