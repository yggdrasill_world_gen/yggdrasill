#pragma once

#include <yggdrasill/random.h>

#include <cstdint>

/// 64 bit PCG implementation based on
/// - https://www.pcg-random.org/download.html
/// - https://gist.github.com/Leandros/6dc334c22db135b033b57e9ee0311553
struct YGDL_Random {
	using result_type = std::uint32_t;

	static constexpr result_type min() { return 0; }
	static constexpr result_type max() { return UINT32_MAX; }

	std::uint64_t state;
	std::uint64_t inc;

	constexpr YGDL_Random() : state(0x853c49e6748fea9bULL), inc(0xda3e39cb94b95bdbULL) {}
	constexpr explicit YGDL_Random(const YGDL_Seed& s) { seed(s); }

	constexpr void seed(const YGDL_Seed& s)
	{
		const auto s_0 = std::uint64_t(s.data[0]) << 31 | std::uint64_t(s.data[1]);
		const auto s_1 = std::uint64_t(s.data[2]) << 31 | std::uint64_t(s.data[3]);

		state = 0;
		inc   = (s_1 << 1) | 1;
		(void) operator()();
		state += s_0;
		(void) operator()();
	}

	constexpr result_type operator()()
	{
		const auto oldstate   = state;
		state                 = oldstate * 6364136223846793005ULL + inc;
		const auto xorshifted = static_cast<std::uint32_t>(((oldstate >> 18u) ^ oldstate) >> 27u);
		int        rot        = oldstate >> 59u;
		return (xorshifted >> rot) | (xorshifted << ((-rot) & 31u));
	}

	constexpr void discard(unsigned long long n)
	{
		for(unsigned long long i = 0; i < n; ++i)
			operator()();
	}

	constexpr friend bool operator==(YGDL_Random const& lhs, YGDL_Random const& rhs)
	{
		return lhs.state == rhs.state && lhs.inc == rhs.inc;
	}
	constexpr friend bool operator!=(YGDL_Random const& lhs, YGDL_Random const& rhs)
	{
		return lhs.state != rhs.state || lhs.inc != rhs.inc;
	}
};
