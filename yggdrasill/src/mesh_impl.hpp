#pragma once

#include <yggdrasill/mesh.h>

#include <yggdrasill/commons.hpp>
#include <yggdrasill/mesh.hpp>

#include <unordered_map>
#include <vector>

namespace yggdrasill::detail {

	class Layer_container;
	class Mesh_impl;

	/// implemented in serializer.cpp; used to directly access member vectors
	void read_mesh_data(Mesh_impl&, std::string_view key, auto& reader);

	class Mesh_impl : public YGDL_Mesh {
	  public:
		Mesh_impl();
		Mesh_impl(Mesh_impl&&) = default;
		Mesh_impl(const Mesh_impl&);

		Mesh_impl& operator=(Mesh_impl&&) = default;
		Mesh_impl& operator=(const Mesh_impl&);

		bool      add_vertices(YGDL_Index count, YGDL_Vertex*, YGDL_Error* out_error);
		bool      remove_vertex(YGDL_Vertex v);
		YGDL_Face add_face(Vertex v_a, Vertex v_b, Vertex v_c, YGDL_Error* out_error);

		bool        flip(Edge<>, YGDL_Error* out_error);
		Primal_edge split(Primal_edge, float split_point, YGDL_Error* out_error);
		Vertex      collapse(Primal_edge, float join_point, YGDL_Error* out_error);

		bool validation(YGDL_Error* out_error) const;

		void on_acquire(Layer_container& layers) { layers_ = &layers; }
		void on_release(YGDL_Bool validate, YGDL_Error* out_error)
		{
			if(validate)
				validation(out_error);

			layers_ = nullptr;
		}

		void update_all_data_pointers();

	  private:
		friend void read_mesh_data(Mesh_impl&, std::string_view key, auto& reader);
		friend void restore_free_lists(Mesh_impl&);

		class Lonely_edge_case_handler;

		Layer_container* layers_ = nullptr;

		std::vector<Primal_edge> vertex_edges_;
		std::vector<Primal_edge> face_edges_;
		std::vector<Vertex>      primal_edge_origin_;
		std::vector<Primal_edge> primal_edge_next_;
		std::vector<Face>        dual_edge_origin_;
		std::vector<Dual_edge>   dual_edge_next_;

		std::vector<Primal_edge> free_edges_{no_edge};
		std::vector<Face>        free_faces_{no_face};
		std::vector<Vertex>      free_vertices_{no_vertex};

		/// \returns The base of the new quad edge
		auto add_quad_edge(YGDL_Vertex origin, YGDL_Vertex dest, YGDL_Face left = no_face, YGDL_Face right = no_face)
		        -> Primal_edge;

		void set_edge_origin(YGDL_Edge e, YGDL_Vertex v);

		Primal_edge& next_primal(Primal_edge);
		Dual_edge&   next_dual(Dual_edge);

		void mark_free(Edge<>);
		void mark_free(Vertex);
		void mark_free(Face);

		bool create_faces(int32_t count, Face*, YGDL_Error* out_error);
	};

} // namespace yggdrasill::detail
