#pragma once

#include <yggdrasill/array.h>
#include <yggdrasill/dictionary.h>

#include <yggdrasill/commons.hpp>
#include <yggdrasill/string.hpp>

#include <memory>
#include <unordered_map>
#include <utility>
#include <variant>
#include <vector>

namespace yggdrasill::detail {

	struct string_hash {
		using is_transparent = void;
		[[nodiscard]] size_t operator()(std::string_view txt) const
		{
			return std::hash<std::string_view>{}(txt);
		}
		[[nodiscard]] size_t operator()(const std::string& txt) const
		{
			return std::hash<std::string_view>{}(txt);
		}
	};

	template <typename T>
	using String_map = std::unordered_map<std::string, T, string_hash, std::equal_to<>>;


	/// required to store YGDL_Dict inside its own values (Value_variant)
	template <typename T>
	class Recursive_wrapper {
	  public:
		Recursive_wrapper() : value_(std::make_unique<T>()) {}
		Recursive_wrapper(const Recursive_wrapper& rhs) : value_(std::make_unique<T>(*rhs.value_)) {}
		Recursive_wrapper(Recursive_wrapper&& rhs) = default;
		~Recursive_wrapper()                       = default;

		Recursive_wrapper& operator=(const Recursive_wrapper& rhs)
		{
			value_ = std::make_unique<T>(*rhs.value_);
			return *this;
		}
		Recursive_wrapper& operator=(Recursive_wrapper&& rhs) = default;

		T*       operator->() { return value_.get(); }
		const T* operator->() const { return value_.get(); }

		T&       operator*() { return *value_; }
		const T& operator*() const { return *value_; }

	  private:
		std::unique_ptr<T> value_;
	};


	struct Array_impl : public YGDL_Array {
		explicit Array_impl(YGDL_Type_t type = YGDL_TYPE_NONE);
		Array_impl(Array_impl&&) noexcept;
		Array_impl(const Array_impl&);
		~Array_impl();

		Array_impl& operator=(Array_impl&&) noexcept;
		Array_impl& operator=(const Array_impl&);

		void free();
	};
	static_assert(std::is_standard_layout_v<Array_impl>);
	static_assert(std::is_standard_layout_v<YGDL_Array>);
	static_assert(sizeof(Array_impl) == sizeof(YGDL_Array));
	static_assert(alignof(Array_impl) == alignof(YGDL_Array));

	// needs to be synchronized with YGDL_Type
	using Value_variant = std::variant<std::monostate,
	                                   YGDL_Bool,
	                                   std::int8_t,
	                                   std::int32_t,
	                                   std::int64_t,
	                                   float,
	                                   double,
	                                   yggdrasill::Vec2,
	                                   yggdrasill::Vec3,
	                                   yggdrasill::String,
	                                   Array_impl,
	                                   Recursive_wrapper<YGDL_Dict>>;
} // namespace yggdrasill::detail

struct YGDL_Dict {
  public:
	YGDL_Dict();
	explicit YGDL_Dict(bool user_allocated);
	YGDL_Dict(const YGDL_Dict& rhs, bool user_allocated = false);
	YGDL_Dict(YGDL_Dict&&) noexcept;
	~YGDL_Dict();

	YGDL_Dict& operator=(YGDL_Dict&& rhs) noexcept;
	YGDL_Dict& operator=(const YGDL_Dict& rhs);

	[[nodiscard]] bool user_allocated() const noexcept { return user_allocated_; }
	YGDL_Dict*         user_allocated(bool b) noexcept
	{
		user_allocated_ = b;
		return this;
	}

	[[nodiscard]] yggdrasill::detail::Value_variant*       get(std::string_view key);
	[[nodiscard]] const yggdrasill::detail::Value_variant* get(std::string_view key) const;

	void erase(std::string_view key);

	[[nodiscard]] YGDL_Dict_key_list keys() const;

	void clear();

  private:
	yggdrasill::detail::String_map<yggdrasill::detail::Value_variant> entries_;
	/// vector of keys; views directly reference the strings used as keys in entries_!
	std::vector<YGDL_String_view> keys_;
	bool                          user_allocated_ = false;
};

namespace yggdrasill::detail {
	// needs to be synchronized with YGDL_Type and Value_variant!
	template <YGDL_Type T>
	using id_to_type = std::variant_alternative_t<
	        static_cast<std::size_t>(T),
	        std::variant<std::monostate, YGDL_Bool, std::int8_t, std::int32_t, std::int64_t, float, double, yggdrasill::Vec2, yggdrasill::Vec3, yggdrasill::String, Array_impl, YGDL_Dict>>;

	template <YGDL_Type T>
	auto* cast_to(void* ptr)
	{
		return std::launder(reinterpret_cast<id_to_type<T>*>(ptr));
	}
	template <YGDL_Type T>
	auto* cast_to(const void* ptr)
	{
		return std::launder(reinterpret_cast<const id_to_type<T>*>(ptr));
	}

} // namespace yggdrasill::detail
