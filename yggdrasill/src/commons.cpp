#include <yggdrasill/commons.h>

#include <iostream>

using Log_handler = void (*)(const char*);

static Log_handler default_log_handler = +[](const char* str) { std::cerr << "YGDL: " << str; };
static Log_handler log_handler         = default_log_handler;


void YGDL_warning_log(const char* str)
{
	if(log_handler) {
		log_handler(str);
	}
}

void YGDL_set_warning_log_handler(void (*handler)(const char*))
{
	log_handler = handler ? handler : default_log_handler;
}
