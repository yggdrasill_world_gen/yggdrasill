#pragma once

#include <yggdrasill/commons.hpp>
#include <yggdrasill/string.hpp>
#include <yggdrasill/utils.hpp>

#include <bit>
#include <cstdint>
#include <limits>
#include <span>
#include <variant>
#include <vector>

namespace yggdrasill::ubjson {

	constexpr auto little_endian =
#ifdef YGDL_BIG_ENDIAN
	        false;
#else
	        true;
#endif

#if __cpp_lib_bit_cast >= 201806L
	using std::bit_cast;
#else
	template <class To, class From>
	std::enable_if_t<sizeof(To) == sizeof(From) && std::is_trivially_copyable_v<From> && std::is_trivially_copyable_v<To>, To>
	        // constexpr support needs compiler magic
	        bit_cast(const From& src) noexcept
	{
		static_assert(std::is_trivially_constructible_v<To>,
		              "This implementation additionally requires "
		              "destination type to be trivially constructible");
		To dst;
		std::memcpy(&dst, &src, sizeof(To));
		return dst;
	}

#endif

#if __cpp_lib_byteswap >= 202110L
	using std::byteswap;
#else
	template <typename T>
	T byteswap(T v)
	{
		auto value_representation = bit_cast<std::array<std::byte, sizeof(T)>>(v);
		std::reverse(value_representation.begin(), value_representation.end());
		return bit_cast<T>(value_representation);
	}
#endif

	// clang-format off
	template <class T>
	concept Container = requires(T a) {
		// T has to be convertible to a std::span, either through conversion or one of span's constructors
		requires std::convertible_to<T, std::span<typename std::remove_cvref_t<T>::value_type>>
		      || std::constructible_from<std::span<typename std::remove_cvref_t<T>::value_type>, T&>;
		{ a.clear() };
		{ a.reserve(std::declval<std::size_t>()) };
		{ a.resize(std::declval<std::size_t>()) };
		{ a.push_back(std::declval<typename std::remove_cvref_t<T>::value_type>()) };
	};
	// clang-format on

	template <typename T>
	concept Readable = detail::is_arithmetic_like<T> || std::assignable_from<T&, String>
	                   || std::assignable_from<T&, Vec2> || std::assignable_from<T&, Vec3>;

	class Writer {
	  public:
		class Obj_guard {
		  public:
			Obj_guard(Obj_guard&&)                 = delete;
			Obj_guard(const Obj_guard&)            = delete;
			Obj_guard& operator=(Obj_guard&&)      = delete;
			Obj_guard& operator=(const Obj_guard&) = delete;
			~Obj_guard() { writer_.put('}'); }

		  private:
			friend class Writer;
			Writer& writer_;
			Obj_guard(Writer& w) : writer_(w) {}
		};

		Writer(std::vector<std::byte>& scratch_buffer) : scratch_buffer_(scratch_buffer)
		{
			scratch_buffer_.clear();
		}

		auto bytes() const -> std::span<const std::byte> { return std::span(scratch_buffer_); }

		void write_null() { put('Z'); }
		void write(bool v) { put(v ? 'T' : 'F'); }
		void write(YGDL_Bool v) { put(v != YGDL_FALSE ? 'T' : 'F'); }
		void write(std::int8_t v) { put('i', v); }
		void write(std::int16_t v) { put('I', v); }
		void write(std::int32_t v) { put('l', v); }
		void write(std::int64_t v) { put('L', v); }
		void write(float v) { put('d', v); }
		void write(double v) { put('D', v); }
		void write(Vec2 v)
		{
			put('{', '$', 'd', '#', length{2});
			write_key(std::string_view("x"));
			put(v.x);
			write_key(std::string_view("y"));
			put(v.y);
		}
		void write(Vec3 v)
		{
			put('{', '$', 'd', '#', length{3});
			write_key(std::string_view("x"));
			put(v.x);
			write_key(std::string_view("y"));
			put(v.y);
			write_key(std::string_view("z"));
			put(v.z);
		}
		void write(std::string_view v) { put('S', length{v.size()}, std::span<const char>(v)); }
		void write(const String& v) { put('S', length{std::size_t(v.size())}, std::span<const char>(v)); }

		void write(std::span<const std::int8_t> v) { put('[', '$', 'i', '#', length{v.size()}, v); }
		void write(std::span<const std::int16_t> v) { put('[', '$', 'I', '#', length{v.size()}, v); }
		void write(std::span<const std::int32_t> v) { put('[', '$', 'l', '#', length{v.size()}, v); }
		void write(std::span<const std::int64_t> v) { put('[', '$', 'L', '#', length{v.size()}, v); }
		void write(std::span<const float> v) { put('[', '$', 'd', '#', length{v.size()}, v); }
		void write(std::span<const double> v) { put('[', '$', 'D', '#', length{v.size()}, v); }
		template <typename T>
		void write(std::span<const T> v)
		{
			put('[', '#', length{v.size()});
			for(auto e : v)
				write(e);
		}

		void write_key(std::string_view v) { put(length{v.size()}, std::span<const char>(v)); }
		void write_object_value(std::string_view key, const auto& v)
		{
			write_key(key);
			write(v);
		}

		Obj_guard begin_object()
		{
			put('{');
			return {*this};
		}
		void write_object(std::invocable<> auto&& callback)
		{
			auto _ = begin_object();
			callback();
		}
		void begin_array(std::size_t count) { put('[', '#', length{count}); }

		void write_array(std::size_t count, std::invocable<std::size_t> auto&& callback)
		{
			begin_array(count);
			for(std::size_t i = 0; i < count; ++i)
				callback(i);
		}

	  private:
		friend class Obj_guard;

		std::vector<std::byte>& scratch_buffer_;

		struct length {
			std::size_t value;
		};

		void put(char c) { scratch_buffer_.push_back(static_cast<std::byte>(c)); }
		void put(std::byte c) { scratch_buffer_.push_back(c); }
		void put(length l)
		{
			if(l.value <= std::numeric_limits<std::int8_t>::max())
				write(std::int8_t(l.value));
			else if(l.value <= std::numeric_limits<std::int16_t>::max())
				write(std::int16_t(l.value));
			else if(l.value <= std::numeric_limits<std::int32_t>::max())
				write(std::int32_t(l.value));
			else
				write(std::int64_t(l.value));
		}
		template <typename T>
		void put(std::span<const T> s)
		{
			const auto begin = scratch_buffer_.size();
			scratch_buffer_.resize(scratch_buffer_.size() + s.size_bytes());

			if constexpr(std::is_integral_v<T> && sizeof(T) > 1 && little_endian) {
				// requires endian conversion
				auto start = reinterpret_cast<T*>(scratch_buffer_.data() + begin);
				for(std::size_t i = 0; i < s.size(); i++) {
					const auto v = to_big_endian(s[i]);
					std::memcpy(start + i, &v, sizeof(T));
				}
			} else {
				std::memcpy(scratch_buffer_.data() + begin, s.data(), s.size_bytes());
			}
		}
		template <typename T>
		void put(T v)
		{
			v = to_big_endian(v);

			const auto begin = scratch_buffer_.size();
			scratch_buffer_.resize(scratch_buffer_.size() + sizeof(T));
			std::memcpy(scratch_buffer_.data() + begin, &v, sizeof(T));
		}
		template <typename... Ts>
		requires(sizeof...(Ts) > 1)
		void put(Ts... v)
		{
			(put(v), ...);
		}

		template <typename T>
		T to_big_endian(T v) const
		{
			if constexpr(std::is_integral_v<T> && sizeof(T) > 1 && little_endian)
				return byteswap(v);
			else
				return v;
		}
	};


	template <std::invocable<std::span<std::byte>> ByteReader>
	class Reader {
	  public:
		using Value =
		        std::variant<bool, std::int8_t, std::int32_t, std::int64_t, float, double, Vec2, Vec3, String>;

		Reader(std::string             filename,
		       std::vector<std::byte>& scratch_buffer,
		       ByteReader&             reader,
		       std::size_t             block_size = std::numeric_limits<std::size_t>::max())
		  : filename_(std::move(filename))
		  , scratch_buffer_(scratch_buffer)
		  , reader_(reader)
		  , byte_remaining_(reader_(std::span<std::byte>{}))
		  , block_size_(block_size)
		{
			read_next_block();
			parse_stack_ = parse_type(); // read the first element (should be an object)
		}

		auto read_position() const { return file_index_; }

		Type type() const { return domain_type(parse_stack_); }

		template <typename T>
		auto read_single() -> T
		{
			if constexpr(std::is_same_v<T, bool>) {
				assert(type() == Type::bool_t);
				return parse_stack_.type_byte == 'T';

			} else if constexpr(std::is_same_v<T, YGDL_Bool>) {
				assert(type() == Type::bool_t);
				return parse_stack_.type_byte == 'T' ? YGDL_TRUE : YGDL_FALSE;

			} else if constexpr(std::is_same_v<T, std::int8_t>) {
				assert(type() == Type::int8_t);
				return static_cast<int8_t>(get());

			} else if constexpr(std::is_same_v<T, std::int32_t>) {
				assert(type() == Type::int32_t);
				return get<std::int32_t>();

			} else if constexpr(std::is_same_v<T, std::int64_t>) {
				assert(type() == Type::int64_t);
				return get<std::int64_t>();

			} else if constexpr(std::is_same_v<T, float>) {
				assert(type() == Type::float_t);
				return get<float>();

			} else if constexpr(std::is_same_v<T, double>) {
				assert(type() == Type::double_t);
				return get<double>();

			} else if constexpr(std::is_same_v<T, Vec2>) {
				assert(type() == Type::vec2_t);
				auto v = Vec2{};
				// clang-format off
				read_object([&](auto key) {
					if(key == "x")
						v.x = get<float>();
					else if(key == "y")
						v.y = get<float>();
					else
						throw_error("Invalid key '", key, "' in Vec2 value.");
				}, false);
				// clang-format on
				return v;

			} else if constexpr(std::is_same_v<T, Vec3>) {
				assert(type() == Type::vec3_t);
				auto v = Vec3{};
				// clang-format off
				read_object([&](auto key) {
					if(key == "x")
						v.x = get<float>();
					else if(key == "y")
						v.y = get<float>();
					else if(key == "z")
						v.z = get<float>();
					else
						throw_error("Invalid key '", key, "' in Vec2 value.");
				}, false);
				// clang-format on
				return v;

			} else if constexpr(std::is_same_v<T, String>) {
				assert(type() == Type::string_t);
				const auto length = get_length();
				auto       str    = String{};
				str.resize(static_cast<index_t>(length));
				get_n(std::as_writable_bytes(std::span<char>(str)));
				return str;

			} else {
				static_assert(!std::is_same_v<T, T>, "read_single<T>() called for incompatible type.");
			}
		}

		auto read_single() -> Value
		{
			switch(type()) {
				case Type::bool_t: return read_single<bool>();
				case Type::int8_t: return read_single<std::int8_t>();
				case Type::int32_t: return read_single<std::int32_t>();
				case Type::int64_t: return read_single<std::int64_t>();
				case Type::float_t: return read_single<float>();
				case Type::double_t: return read_single<double>();
				case Type::vec2_t: return read_single<Vec2>();
				case Type::vec3_t: return read_single<Vec3>();
				case Type::string_t: return read_single<String>();

				default:
					assert(!"read_single() called for incompatible type. Should have been checked beforehand!");
					throw Precondition_violation_exception("Invalid operation during UBJSON parsing.");
			}
		}

		template <Container T>
		void read_array_into(T&& destination)
		{
			if(type() != Type::array_t) {
				throw_error("Unexpected/Incompatible type in UBJSON. Expected an array");
			}

			read_array([&](Type element_type, std::optional<std::size_t> count, auto&& read_elements) {
				read_elements(destination);
			});
		}

		template <typename T>
		void read_value_into(T&& destination)
		{
			using T_plain = std::remove_cvref_t<T>;
			static_assert(Readable<T_plain>, "read_value_into() can never succeed for the passed type T!");

			if(type() == Type::array_t || type() == Type::dict_t) {
				throw_error("Unexpected/Incompatible type in UBJSON.");
			} else {
				// clang-format off
				std::visit([&](auto&& v) {
					if constexpr(std::assignable_from<T&, decltype(v)>) {
						detail::narrow_into(destination, std::forward<decltype(v)>(v));
					} else
						throw_error("Unexpected/Incompatible type in UBJSON.");
				}, read_single());
				// clang-format on
			}
		}

		/// Returns the type and dimensions of the next statically typed array.
		/// Afterwards, the read-head is at the returned offset position and no other method may be called
		///   until the array has been manually read.
		auto read_array_header() -> std::tuple<Type, std::size_t /*offset*/, std::size_t /*element count*/>
		{
			if(type() != Type::array_t)
				throw Precondition_violation_exception(detail::concat(
				        "read_array_header() was called but current type() is ", parse_stack_.type_byte, "."));

			if(!parse_stack_.element_count)
				throw Io_error_exception("The high-level parsing code expected a fixed sized array.");

			const auto element_count      = *parse_stack_.element_count;
			const auto fixed_element_type = parse_stack_.element_type_byte;
			parse_stack_                  = parse_type(fixed_element_type);
			const auto fixed_type         = domain_type(parse_stack_);

			if(!fixed_element_type && fixed_type != Type::bool_t)
				throw Io_error_exception("The high-level parsing code expected a statically typed array.");

			return {fixed_type, file_index_, element_count};
		}

		// Callback(Type element_type, std::optional<std::size_t> count, READ), where READ is callable with either
		// - a lambda that is called once for each value
		// - a container instance with at least the methods: resize, reserve, push_back and conversion to std::span
		template <typename Callback>
		void read_array(Callback&& callback)
		{
			if(type() != Type::array_t)
				throw Precondition_violation_exception(detail::concat(
				        "read_array() was called but current type() is ", parse_stack_.type_byte, "."));

			const auto count = parse_stack_.element_count;

			if(count && *count == 0)
				return; // empty array

			const auto fixed_element_type = parse_stack_.element_type_byte;

			parse_stack_          = parse_type(fixed_element_type);
			const auto fixed_type = domain_type(parse_stack_);

			// slow path: iterate over each element and read them one by one
			const auto read_elements_slow = [&](auto&& per_element) {
				if(count) {
					for(std::size_t i = 0; i < *count; ++i) {
						if(i > 0)
							parse_stack_ = parse_type(fixed_element_type);

						if(domain_type(parse_stack_) != fixed_type)
							throw_error("Invalid array of mixed types in UBJSON.");

						per_element();
					}

				} else {
					auto first = true;
					while(peek() != ']') {
						if(first)
							first = false;
						else
							parse_stack_ = parse_type(fixed_element_type);

						if(domain_type(parse_stack_) != fixed_type)
							throw_error("Invalid array of mixed types in UBJSON.");

						per_element();
					}
					get(); // skip the closing ']'
				}
			};

			// Pass the actual type and size (optional) to our caller.
			// The caller will call the last parameter exactly once, to read the elements, either with a
			//   void() callback or by passing the container.
			callback(fixed_type, count, [&](auto&& element_destination) {
				if constexpr(std::is_invocable_v<decltype(element_destination)>) {
					read_elements_slow(element_destination);

				} else {
					static_assert(Container<std::remove_cvref_t<decltype(element_destination)>>);
					using T = std::remove_cvref_t<decltype(element_destination)>::value_type;

					static_assert(Readable<T>, "read_array() can never succeed for the given value_type T!");

					if(count && fixed_element_type && is_simple_type(*fixed_element_type)
					   && detail::c_api_type<T> == fixed_type) {
						// fast path: read bytes as a single block
						element_destination.resize(static_cast<index_t>(*count));
						get_n(std::as_writable_bytes(std::span<T>(element_destination)));

						// convert byte order to native, if required
						if constexpr(std::integral<T> && sizeof(T) > 1 && little_endian) {
							for(auto& e : std::span<T>(element_destination)) {
								e = byteswap(e);
							}
						}

					} else {
						element_destination.clear();
						if(count)
							element_destination.reserve(static_cast<index_t>(*count));

						if(detail::c_api_type<T> == fixed_type) {
							read_elements_slow([&] { element_destination.push_back(read_single<T>()); });
						} else {
							read_elements_slow([&] {
								// clang-format off
								std::visit([&](auto&& v) {
									if constexpr(std::assignable_from<T&, decltype(v)>)
										element_destination.push_back(detail::narrow<T>(std::forward<decltype(v)>(v)));
									else
										throw_error("Unexpected array element type in UBJSON.");
								}, read_single());
								// clang-format on
							});
						}
					}
				}
			});
		}

		template <std::invocable<std::string_view> Callback>
		void read_object(Callback&& callback, bool validate_type = true)
		{
			if(validate_type && type() != Type::dict_t)
				throw Precondition_violation_exception(detail::concat(
				        "read_object() was called but current type() is ", parse_stack_.type_byte, "."));

			const auto fixed_element_type = parse_stack_.element_type_byte;

			const auto read_key_value_pair = [&] {
				// read the key
				const auto length = get_length();
				auto       str    = std::string{};
				str.resize(length);
				get_n(std::as_writable_bytes(std::span(str)));

				// read the type info (if this is a simple type and the type was fixed, this doesn't read anything)
				// and set the type, so the callback can handle it correctly
				parse_stack_ = parse_type(fixed_element_type);
				return callback(str);
			};

			if(const auto size = parse_stack_.element_count) { // fixed size
				for(std::size_t i = 0; i < *size; ++i) {
					if constexpr(std::is_void_v<decltype(read_key_value_pair())>)
						read_key_value_pair();
					else {
						if(!read_key_value_pair())
							return;
					}
				}

			} else {
				while(peek() != '}') {
					if constexpr(std::is_void_v<decltype(read_key_value_pair())>)
						read_key_value_pair();
					else {
						if(!read_key_value_pair())
							return;
					}
				}
				get(); // skip the closing '}'
			}
		}

		const auto& filename() const { return filename_; }
		auto        file_position() const { return file_index_; }

	  private:
		struct Type_marker {
			char                       type_byte         = '?';
			std::optional<std::size_t> element_count     = std::nullopt;
			std::optional<char>        element_type_byte = std::nullopt;
		};

		std::string             filename_;
		std::vector<std::byte>& scratch_buffer_;
		ByteReader&             reader_;
		std::size_t             byte_remaining_ = 0;
		std::size_t             block_size_     = 0;

		std::size_t index_      = 0;
		std::size_t file_index_ = 0;
		Type_marker parse_stack_;

		bool is_simple_type(char type_byte) const
		{
			return type_byte == 'i' || type_byte == 'l' || type_byte == 'L' || type_byte == 'd'
			       || type_byte == 'D';
		}

		/// Reads the next type, either taking the argument as the type-marker-byte or reading it.
		/// After this function returns the read-head is located right after the type marker (or after length/type for array/object)
		Type_marker parse_type(std::optional<char> type_byte = std::nullopt, bool shallow = false)
		{
			auto type = Type_marker{};

			type.type_byte = type_byte ? *type_byte : get();
			while(type.type_byte == 'N')
				type.type_byte = get();

			switch(type.type_byte) {
				case 'Z':
				case 'T':
				case 'F':
				case 'i':
				case 'U':
				case 'I':
				case 'l':
				case 'L':
				case 'd':
				case 'D':
				case 'S': break;        // simple type
				case '[': {
					if(peek() == '$') { // = type
						get();          // skip $
						type.element_type_byte = get();
					}
					if(peek() == '#') { // = length
						get();          // skip #
						type.element_count = get_length();
					}
					if(type.element_type_byte)
						assert(type.element_count);
					break;
				}
				case '{':
					if(peek() == '$') { // = type
						get();          // skip $
						type.element_type_byte = get();
					}
					if(peek() == '#') { // = length
						get();          // skip #
						type.element_count = get_length();
					}
					if(type.element_type_byte)
						assert(type.element_count);
					break;

				case 'H': throw_error("Invalid type 'H' (high-precision number) in UBJSON.");
				case 'C': throw_error("Invalid type 'C' (char) in UBJSON file.");
				default: throw_error("Invalid type '", type.type_byte, "' (?) in UBJSON.");
			}
			return type;
		}

		Type domain_type(const Type_marker& marker) const
		{
			switch(marker.type_byte) {
				case 'Z': return Type::none;
				case 'T':
				case 'F': return Type::bool_t;
				case 'i': return Type::int8_t;
				case 'l': return Type::int32_t;
				case 'L': return Type::int64_t;
				case 'd': return Type::float_t;
				case 'D': return Type::double_t;
				case 'S': return Type::string_t;
				case '[': return Type::array_t;
				case '{':
					if(marker.element_type_byte == 'd') {
						if(marker.element_count == 2)
							return Type::vec2_t;
						if(marker.element_count == 3)
							return Type::vec3_t;
					}
					return Type::dict_t;

				case 'U': throw_error("Invalid type 'U' (uint8) in UBJSON.");
				case 'I': throw_error("Invalid type 'I' (int16) in UBJSON.");
				default:
					throw_error(std::string("Invalid type '") + marker.type_byte + "' (?) in UBJSON at byte ");
			}
		}


		char peek() const { return static_cast<char>(scratch_buffer_[index_]); }
		char get()
		{
			for(;;) {
				if(index_ >= scratch_buffer_.size()) {
					throw_error(
					        "Unexpected end-of-file parsing UBJSON. The file is likely either corrupted or "
					        "has been truncated.");
				}

				auto c = static_cast<char>(scratch_buffer_[index_]);
				index_++;
				file_index_++;
				if(index_ == scratch_buffer_.size())
					read_next_block();
				return c;
			}
		}
		void get_n(std::span<std::byte> out)
		{
			auto i = std::size_t(0);
			while(i < out.size()) {
				const auto count = std::min(out.size() - i, scratch_buffer_.size() - index_);
				std::memcpy(out.data() + i, scratch_buffer_.data() + index_, count);
				i += count;
				index_ += count;
				file_index_ += count;

				if(index_ >= scratch_buffer_.size()) {
					read_next_block();
					if(index_ >= scratch_buffer_.size())
						get(); // will throw
				}
			}
		}

		std::size_t get_length()
		{
			const auto next_byte = get();
			switch(next_byte) {
				case 'i': return static_cast<int8_t>(get());
				case 'U': return static_cast<uint8_t>(get());
				case 'I': return get<std::int16_t>();
				case 'l': return get<std::int32_t>();
				case 'L': return get<std::int64_t>();
				default: throw_error("Invalid type '", next_byte, "' for length in UBJSON.");
			}
		}

		template <std::integral T>
		T get()
		{
			T v;
			get_n(std::span(reinterpret_cast<std::byte*>(&v), sizeof(T)));
			if constexpr(sizeof(T) > 1 && little_endian)
				v = byteswap(v);
			return v;
		}
		template <std::floating_point T>
		T get()
		{
			T v;
			get_n(std::span(reinterpret_cast<std::byte*>(&v), sizeof(T)));
			return v;
		}

		void read_next_block()
		{
			const auto to_read = std::min(byte_remaining_, block_size_);
			if(to_read <= 0)
				return; // end of file

			scratch_buffer_.clear();
			scratch_buffer_.resize(to_read);
			auto read = reader_(scratch_buffer_);
			scratch_buffer_.resize(read);
			byte_remaining_ -= read;
			index_ = 0;
		}

		[[noreturn]] void throw_error(auto&&... message) const
		{
			throw Io_error_exception(detail::concat(message...,
			                                        " In file '",
			                                        filename_,
			                                        "' at position ",
			                                        file_index_,
			                                        " (0x",
			                                        std::hex,
			                                        file_index_,
			                                        ")."));
		}
	};

} // namespace yggdrasill::ubjson
