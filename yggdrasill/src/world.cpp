#include <yggdrasill/world.h>

#include "world.hpp"

#include <yggdrasill/dictionary.hpp>
#include <yggdrasill/error.hpp>

#include "cow_ptr.hpp"
#include "layer_impl.hpp"
#include "mesh_impl.hpp"
#include "random_impl.hpp"

#include <cassert>
#include <cstdint>
#include <span>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

using yggdrasill::detail::YGDL_Layer_impl;

static std::atomic<bool>& validate_mesh_on_release()
{
	static std::atomic<bool> flag = true;
	return flag;
};
static std::atomic<bool>& validate_layers_on_release()
{
	static std::atomic<bool> flag = true;
	return flag;
};

void YGDL_world_validate_mesh_on_release_set(YGDL_Bool value)
{
	validate_mesh_on_release().store(value == YGDL_TRUE, std::memory_order_relaxed);
}
YGDL_Bool YGDL_world_validate_mesh_on_release_get()
{
	return validate_mesh_on_release().load(std::memory_order_relaxed) ? YGDL_TRUE : YGDL_FALSE;
}

void YGDL_world_validate_layers_on_release_set(YGDL_Bool value)
{
	validate_layers_on_release().store(value == YGDL_TRUE, std::memory_order_relaxed);
}
YGDL_Bool YGDL_world_validate_layers_on_release_get(void)
{
	return validate_layers_on_release().load(std::memory_order_relaxed) ? YGDL_TRUE : YGDL_FALSE;
}


YGDL_Layer_definition YGDL_Layer_definition_create(
        YGDL_String_view id, YGDL_Type_t data_type, YGDL_Layer_ref_type_t ref_type)
{
	using VI = decltype(YGDL_Layer_definition::value_info);
	auto vi  = [&]() -> VI {
        switch(data_type) {
            case YGDL_TYPE_NONE:
                assert(!"Invalid data_type (YGDL_TYPE_NONE) passed to YGDL_Layer_definition_create()");
                std::abort();
                break;
            case YGDL_TYPE_BOOL: return VI{.bool_value = {.initial = YGDL_FALSE}};
            case YGDL_TYPE_INT8:
                return VI{.int8_value = {.min     = std::numeric_limits<int8_t>::lowest(),
				                          .initial = 0,
				                          .max     = std::numeric_limits<int8_t>::max()}};
                break;
            case YGDL_TYPE_INT32:
                return VI{.int32_value = {.min     = std::numeric_limits<int32_t>::lowest(),
				                           .initial = 0,
				                           .max     = std::numeric_limits<int32_t>::max()}};
                break;
            case YGDL_TYPE_INT64:
                return VI{.int64_value = {.min     = std::numeric_limits<int64_t>::lowest(),
				                           .initial = 0,
				                           .max     = std::numeric_limits<int64_t>::max()}};
                break;
            case YGDL_TYPE_FLOAT:
                return VI{.float_value = {.min       = std::numeric_limits<float>::lowest(),
				                           .initial   = 0.f,
				                           .max       = std::numeric_limits<float>::max(),
				                           .has_min   = false,
				                           .has_max   = false,
				                           .allow_nan = false,
				                           .allow_inf = false}};
                break;
            case YGDL_TYPE_DOUBLE:
                return VI{.double_value = {.min       = std::numeric_limits<double>::lowest(),
				                            .initial   = 0.0,
				                            .max       = std::numeric_limits<double>::max(),
				                            .has_min   = false,
				                            .has_max   = false,
				                            .allow_nan = false,
				                            .allow_inf = false}};
                break;
            case YGDL_TYPE_VEC2:
                return VI{.vec2_value = {.min            = YGDL_Vec2{std::numeric_limits<float>::lowest(),
                                                          std::numeric_limits<float>::lowest()},
				                          .initial        = YGDL_Vec2{0.f, 0.f},
				                          .max            = YGDL_Vec2{std::numeric_limits<float>::max(),
                                                          std::numeric_limits<float>::max()},
				                          .min_length     = 0.f,
				                          .max_length     = std::numeric_limits<float>::max(),
				                          .has_min        = false,
				                          .has_max        = false,
				                          .has_min_length = false,
				                          .has_max_length = false,
				                          .allow_nan      = false,
				                          .allow_inf      = false}};
                break;
            case YGDL_TYPE_VEC3:
                return VI{.vec3_value = {.min            = YGDL_Vec3{std::numeric_limits<float>::lowest(),
                                                          std::numeric_limits<float>::lowest(),
                                                          std::numeric_limits<float>::lowest()},
				                          .initial        = YGDL_Vec3{0.f, 0.f, 0.f},
				                          .max            = YGDL_Vec3{std::numeric_limits<float>::max(),
                                                          std::numeric_limits<float>::max(),
                                                          std::numeric_limits<float>::max()},
				                          .min_length     = 0.f,
				                          .max_length     = std::numeric_limits<float>::max(),
				                          .has_min        = false,
				                          .has_max        = false,
				                          .has_min_length = false,
				                          .has_max_length = false,
				                          .allow_nan      = false,
				                          .allow_inf      = false}};
                break;
            case YGDL_TYPE_STRING:
                assert(!"Invalid data_type (YGDL_TYPE_STRING) passed to YGDL_Layer_definition_create()");
                std::abort();
                break;
            case YGDL_TYPE_ARRAY:
                assert(!"Invalid data_type (YGDL_TYPE_ARRAY) passed to YGDL_Layer_definition_create()");
                std::abort();
                break;
            case YGDL_TYPE_DICT:
                assert(!"Invalid data_type (YGDL_TYPE_DICT) passed to YGDL_Layer_definition_create()");
                std::abort();
                break;
        }
        assert(!"Unexpected data_type passed to YGDL_Layer_definition_create()");
        std::abort();
	}();

	return YGDL_Layer_definition{.id            = id,
	                             .description   = YGDL_string_view_create(""),
	                             .data_type     = data_type,
	                             .ref_type      = ref_type,
	                             .invalidation  = YGDL_LAYER_INVALIDATION_RESET_AFFECTED,
	                             .interpolation = YGDL_LAYER_INTERPOLATION_RESET_AFFECTED,
	                             .value_info    = vi};
}

YGDL_World* YGDL_world_create()
{
	return new YGDL_World();
}
void YGDL_world_destroy(YGDL_World* world)
{
	if(!world)
		return;

	if(!world->valid) {
		assert(!"Double-Delete of YGDL_World");
		return;
	}

	world->valid = false;
	delete world;
}
YGDL_World* YGDL_world_copy(const YGDL_World* world)
{
	if(world == nullptr || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_copy.");
		return YGDL_world_create();
	}

	return new YGDL_World(*world);
}
YGDL_World* YGDL_world_move(YGDL_World** world)
{
	if(world == nullptr || *world == nullptr || !(*world)->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_move.");
		return YGDL_world_create();
	}

	auto ret = new YGDL_World(std::move(**world));
	YGDL_world_destroy(*world);
	*world = nullptr;
	return ret;
}
void YGDL_world_copy_assign(YGDL_World* target, const YGDL_World* other_world)
{
	if(target == nullptr || !target->valid) {
		assert(!"Invalid LHS passed to YGDL_world_copy_assign.");
		return;
	}
	if(other_world == nullptr || !other_world->valid) {
		assert(!"Invalid RHS passed to YGDL_world_copy_assign.");
		return;
	}

	*target = *other_world;
}
void YGDL_world_move_assign(YGDL_World* target, YGDL_World** other_world)
{
	if(target == nullptr || !target->valid) {
		assert(!"Invalid LHS passed to YGDL_world_copy_assign.");
		return;
	}
	if(other_world == nullptr || *other_world == nullptr || !(*other_world)->valid) {
		assert(!"Invalid RHS passed to YGDL_world_move_assign.");
		return;
	}

	*target = std::move(**other_world);
	YGDL_world_destroy(*other_world);
	*other_world = nullptr;
}

const YGDL_Mesh* YGDL_world_mesh(const YGDL_World* world)
{
	if(world == nullptr || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_mesh.");
		return nullptr;
	}

	return &world->mesh.get_const();
}
YGDL_Mesh* YGDL_world_mesh_acquire(YGDL_World* world, YGDL_Error* out_error)
{
	if(world == nullptr || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_mesh_acquire.");
		return nullptr;
	}

	auto mesh = world->mesh.acquire_mutable(out_error);
	if(!mesh)
		return nullptr;

	mesh->on_acquire(world->layers);
	return mesh;
}
void YGDL_world_mesh_release(YGDL_World* world, YGDL_Mesh* mesh_ptr, YGDL_Error* out_error)
{
	if(!world || !world->valid) {
		assert(!"The world parameter is required to be valid, when calling YGDL_world_mesh_release.");
		return;
	}
	if(!mesh_ptr)
		return;

	assert(mesh_ptr == &world->mesh.get_const());
	static_cast<yggdrasill::detail::Mesh_impl*>(mesh_ptr)->on_release(
	        validate_mesh_on_release().load(std::memory_order_relaxed), out_error);
	world->mesh.release_mutable();
}
YGDL_Data_version YGDL_world_mesh_version(const YGDL_World* world)
{
	if(world == nullptr || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_mesh_version.");
		return static_cast<YGDL_Data_version>(-1);
	}

	return world->mesh.version();
}


// layer
YGDL_Layer* YGDL_world_layer_acquire(YGDL_World* world, const YGDL_Layer_definition* layer_def, YGDL_Error* out_error)
{
	if(!world || !world->valid || !layer_def) {
		assert(!"The world and layer_def parameters are required to be valid, when calling "
			       "YGDL_world_layer_acquire.");
		return nullptr;
	}

	const auto size = [&] {
		auto& mesh = world->mesh.get_const();
		switch(layer_def->ref_type) {
			case YGDL_LAYER_REF_TYPE_VERTEX: return mesh.vertex_count;
			case YGDL_LAYER_REF_TYPE_FACE: return mesh.face_count;
			case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL: return mesh.edge_count >> 1u;
			case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL_DIRECTED: return mesh.edge_count;
			case YGDL_LAYER_REF_TYPE_EDGE_DUAL: return mesh.edge_count >> 1u;
			case YGDL_LAYER_REF_TYPE_EDGE_DUAL_DIRECTED: return mesh.edge_count;
		}
		return YGDL_Index(0);
	}();

	return world->layers.acquire(*layer_def, size, out_error);
}
void YGDL_world_layer_release(YGDL_World* world, YGDL_Layer* layer, YGDL_Error* out_error)
{
	if(!world || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_layer_release.");
		return;
	}
	if(!layer)
		return;

	world->layers.release(*layer,
	                      world->mesh.get_const(),
	                      validate_layers_on_release().load(std::memory_order_relaxed),
	                      out_error);
}
const YGDL_Layer* YGDL_world_layer(const YGDL_World* world, const YGDL_Layer_definition* layer_def)
{
	if(!world || !world->valid || !layer_def) {
		assert(!"The world and layer_def parameters are required to be valid, when calling "
			       "YGDL_world_layer.");
		return nullptr;
	}

	if(auto layer = world->layers.get(layer_def->id); layer && layer->compatible(*layer_def)) {
		return layer;
	} else {
		return nullptr;
	}
}
void YGDL_world_layer_remove(YGDL_World* world, YGDL_String_view id)
{
	if(!world || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_layer_remove.");
		return;
	}

	world->layers.remove(id);
}
YGDL_Data_version YGDL_world_layer_version(const YGDL_World* world, YGDL_String_view id)
{
	if(!world || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_layer_version.");
		return static_cast<YGDL_Data_version>(-1);
	}

	return world->layers.version(id);
}

YGDL_Bool YGDL_world_valid(const YGDL_World* world, YGDL_Error* out_error)
{
	if(!world || !world->valid)
		return false;

	return world->layers.validate(world->mesh.get_const(), out_error)
	                       && world->mesh.get_const().validation(out_error)
	               ? YGDL_TRUE
	               : YGDL_FALSE;
}

YGDL_Bool YGDL_world_layer_list(const YGDL_World* world, YGDL_Index* size, const YGDL_Layer** data)
{
	if(!world || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_layer_list.");
		return YGDL_FALSE;
	}

	if(!size) {
		assert(!"size passed to YGDL_world_layer_list was NULL.");
		return YGDL_FALSE;
	}

	const auto actual_size = world->layers.size();

	if(data && actual_size <= *size) {
		world->layers.list(std::span{data, std::size_t(size)});
		*size = actual_size;
		return YGDL_TRUE;

	} else {
		*size = actual_size;
	}

	return YGDL_FALSE;
}

void YGDL_world_layer_invalidate(YGDL_World* world, const YGDL_Layer_mesh_changes* req, YGDL_Error* out_error)
{
	if(!world || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_layer_invalidate.");
		return;
	}

	auto invalidation_to_span = [](const YGDL_Layer_element_invalidations& i) {
		return std::span<const YGDL_Index>(i.indices, static_cast<std::size_t>(i.count));
	};
	auto interpolation_to_span = [](const YGDL_Layer_element_interpolations& i) {
		return std::span<const YGDL_Layer_element_interpolation>(i.elements, static_cast<std::size_t>(i.count));
	};

	world->layers.on_mesh_change(invalidation_to_span(req->invalidated_vertices),
	                             invalidation_to_span(req->invalidated_faces),
	                             invalidation_to_span(req->invalidated_primal_edges),
	                             invalidation_to_span(req->invalidated_dual_edges),
	                             interpolation_to_span(req->interpolated_vertices),
	                             interpolation_to_span(req->interpolated_faces),
	                             interpolation_to_span(req->interpolated_primal_edges),
	                             interpolation_to_span(req->interpolated_dual_edges),
	                             req->interpolation_weight,
	                             out_error);
}


YGDL_Dict* YGDL_world_unstructured_layer_acquire(YGDL_World* world, YGDL_String_view id_view, YGDL_Error* out_error)
{
	if(!world || !world->valid || id_view.length == 0) {
		assert(!"The world and id parameters are required to be valid, when calling YGDL_world_unstructured_layer_acquire.");
		return nullptr;
	}

	auto id                = std::string(id_view.chars, id_view.length);
	auto&& [iter, created] = world->unstructured_layers.try_emplace(id);
	auto layer             = iter->second.acquire_mutable(out_error);
	if(!layer)
		return nullptr;

	world->locked_unstructured_layers.emplace(c_ptr(layer), std::move(id));
	return c_ptr(layer);
}
void YGDL_world_unstructured_layer_release(YGDL_World* world, YGDL_Dict* dict)
{
	if(!world || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_unstructured_layer_release.");
		return;
	}
	if(!dict)
		return;

	if(auto handle = world->locked_unstructured_layers.extract(dict)) {
		auto& layer = world->unstructured_layers[handle.mapped()];
		assert(dict == c_ptr(layer.get_const()));
		layer.release_mutable();

	} else {
		assert(!"The YGDL_Dict passed to YGDL_world_unstructured_layer_release has not been acquired previously");
	}
}
const YGDL_Dict* YGDL_world_unstructured_layer(const YGDL_World* world, YGDL_String_view id)
{
	if(!world || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_unstructured_layer.");
		return nullptr;
	}

	if(auto layer = world->unstructured_layers.find(yggdrasill::detail::from_ygdl_string_view(id));
	   layer != world->unstructured_layers.end()) {
		return c_ptr(layer->second.get_const());
	} else {
		return nullptr;
	}
}
void YGDL_world_unstructured_layer_remove(YGDL_World* world, YGDL_String_view id)
{
	if(!world || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_unstructured_layer_remove.");
		return;
	}

	if(auto iter = world->unstructured_layers.find(yggdrasill::detail::from_ygdl_string_view(id));
	   iter != world->unstructured_layers.end()) {
		world->unstructured_layers.erase(iter);
	}
}
YGDL_Data_version YGDL_world_unstructured_layer_version(const YGDL_World* world, YGDL_String_view id)
{
	if(!world || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_unstructured_layer_version.");
		return static_cast<YGDL_Data_version>(-1);
	}

	if(auto layer = world->unstructured_layers.find(yggdrasill::detail::from_ygdl_string_view(id));
	   layer != world->unstructured_layers.end()) {
		return layer->second.version();
	} else {
		return 0;
	}
}

YGDL_EXPORTED YGDL_Bool YGDL_world_unstructured_layer_list(
        const YGDL_World* world, YGDL_Index* size, YGDL_String_view* data)
{
	if(!world || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_unstructured_layer_list.");
		return YGDL_FALSE;
	}

	if(!size) {
		assert(!"size passed to YGDL_world_unstructured_layer_list was NULL.");
		return YGDL_FALSE;
	}

	const auto actual_size = static_cast<YGDL_Index>(world->unstructured_layers.size());

	if(data && actual_size <= *size) {
		auto i = 0;
		for(auto&& [key, _] : world->unstructured_layers) {
			data[i++] = yggdrasill::detail::to_ygdl_string_view(key);
		}
		*size = actual_size;
		return YGDL_TRUE;

	} else {
		*size = actual_size;
	}

	return YGDL_FALSE;
}

YGDL_Random* YGDL_world_random(YGDL_World* world)
{
	if(!world || !world->valid) {
		assert(!"Invalid YGDL_World passed to YGDL_world_random.");
		return nullptr;
	}

	return &world->random;
}
