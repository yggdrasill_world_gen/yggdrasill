#include <yggdrasill/error.h>

#include <yggdrasill/string.hpp>

#ifdef YGDL_STACKTRACE_SUPPORT
#include <version>
#if __cpp_lib_stacktrace >= 202011L
#include <stacktrace>
#endif
#endif

YGDL_Error YGDL_error_create(YGDL_Error_code_t code, YGDL_String_view string)
{
	auto stacktrace_str = std::string();

#ifdef YGDL_STACKTRACE_SUPPORT
#if __cpp_lib_stacktrace >= 202011L
	stacktrace_str = to_string(std::stacktrace::current());
#endif
#endif

	return YGDL_Error{code,
	                  YGDL_string_create(string),
	                  YGDL_string_create(yggdrasill::detail::to_ygdl_string_view(stacktrace_str))};
}
YGDL_Error YGDL_error_copy(const YGDL_Error* e)
{
	return YGDL_Error{e->code, YGDL_string_copy(&e->message), YGDL_string_copy(&e->stacktrace)};
}
void YGDL_error_set(YGDL_Error* error, YGDL_Error_code_t code, YGDL_String_view string)
{
	if(error) {
		YGDL_error_destroy(error);
		*error = YGDL_error_create(code, string);
	}
}
void YGDL_error_destroy(YGDL_Error* error)
{
	if(error) {
		YGDL_string_destroy(&error->message);
		YGDL_string_destroy(&error->stacktrace);
	}
}
void YGDL_error_reset(YGDL_Error* error)
{
	if(error) {
		YGDL_error_destroy(error);
		*error = YGDL_error_create_success();
	}
}
