#include <yggdrasill/serialization.h>

#include <yggdrasill/error.hpp>
#include <yggdrasill/string.hpp>

#ifdef YGDL_SERIALIZATION_SUPPORT

#include <yggdrasill/array.hpp>
#include <yggdrasill/dictionary.hpp>
#include <yggdrasill/world.hpp>

#include "nano_ubjson.hpp"
#include "world.hpp"

#include <miniz.h>

#include <charconv>
#include <concepts>
#include <filesystem>
#include <memory>
#include <optional>
#include <span>
#include <sstream>
#include <tuple>
#include <unordered_map>

using namespace yggdrasill;
using detail::String_map;
using yggdrasill::detail::Cow_ptr;
using yggdrasill::detail::finally;
using yggdrasill::detail::from_ygdl_string_view;
using yggdrasill::detail::Interned_definition;
using yggdrasill::detail::Mesh_impl;
using yggdrasill::detail::YGDL_Layer_impl;

namespace {
	constexpr auto compression_level = static_cast<mz_uint>(MZ_DEFAULT_LEVEL);

	enum class Entry_type : char {
		config             = 'c',
		world              = 'w',
		layer_unstructured = 'u',
		layer_structured   = 's',
		mesh               = 'm',
		layer_definition   = 'l',

		user_data = 'x'
	};

	std::optional<uint32_t> parse_id(std::string_view str)
	{
		auto       id  = uint32_t{0};
		const auto end = str.data() + str.size();
		const auto r   = std::from_chars(str.data(), end, id, 16);
		return r.ptr == end ? std::optional(id) : std::nullopt;
	}

	using Ptr_key = std::tuple<const void*, std::uint32_t>;
	struct Ptr_key_hash {
		size_t operator()(const Ptr_key& x) const
		{
			return std::hash<const void*>{}(get<0>(x)) ^ std::hash<std::uint32_t>{}(get<1>(x));
		}
	};
	struct World_entry_data {
		std::unordered_map<Ptr_key, std::uint32_t, Ptr_key_hash> ptr_to_key;
		std::unordered_map<std::uint32_t, mz_uint>               key_to_zip_entry;
		std::uint32_t                                            next_key = 1;
	};
} // namespace


struct YGDL_Serializer {

	mz_zip_archive    archive{};
	const std::string path;
	const bool        read_only;
	bool              opened = false;

	detail::String_map<mz_uint> entries_userdata;
	detail::String_map<mz_uint> entries_config;
	detail::String_map<mz_uint> entries_layer_definitions;

	std::unordered_map<Entry_type, World_entry_data> entries;

	std::vector<std::byte> scratch_buffer;

	detail::String_map<std::shared_ptr<const Interned_definition>>   loaded_layer_definitions;
	std::unordered_map<std::int32_t, Cow_ptr<Mesh_impl>::Weak>       loaded_meshes;
	std::unordered_map<std::int32_t, Cow_ptr<Dictionary>::Weak>      loaded_unstructured_layers;
	std::unordered_map<std::int32_t, Cow_ptr<YGDL_Layer_impl>::Weak> loaded_layers;


	YGDL_Serializer(YGDL_String_view path, YGDL_Bool write)
	  : path(std::string(detail::from_ygdl_string_view(path))), read_only(!write)
	{
	}
	YGDL_Serializer(const YGDL_Serializer&)            = delete;
	YGDL_Serializer(YGDL_Serializer&&)                 = delete;
	YGDL_Serializer& operator=(const YGDL_Serializer&) = delete;
	YGDL_Serializer& operator=(YGDL_Serializer&&)      = delete;
	~YGDL_Serializer() { close(); }

	void flush(YGDL_Error* out_error)
	{
		close();
		open(out_error);
	}

	const char* last_error() { return mz_zip_get_error_string(mz_zip_get_last_error(&archive)); }

	void shrink_read_caches()
	{
		// Remove all entries from the read-caches (loaded_* members) with a use_count <=1, i.e. no other copies.
		// Entries from loaded_layer_definitions are not discarded, even if they are currently unused, because
		//   they are small and there are generally only few instances.
		std::erase_if(loaded_meshes, [](auto& e) { return e.second.expired(); });
		std::erase_if(loaded_unstructured_layers, [](auto& e) { return e.second.expired(); });
		std::erase_if(loaded_layers, [](auto& e) { return e.second.expired(); });
	}

	bool open(YGDL_Error* out_error)
	{
		assert(!opened);
		entries_userdata.clear();
		entries_config.clear();
		entries_layer_definitions.clear();
		entries.clear();
		mz_zip_zero_struct(&archive);

		constexpr auto flags = compression_level | MZ_ZIP_FLAG_WRITE_ZIP64 | MZ_ZIP_FLAG_WRITE_ALLOW_READING
		                       | MZ_ZIP_FLAG_DO_NOT_SORT_CENTRAL_DIRECTORY;

		// if the file already exists, we have to open a reader first, to load the central directory
		if(read_only || std::filesystem::exists(path)) {
			if(!mz_zip_reader_init_file_v2(&archive, path.c_str(), flags, 0, 0)) {
				set_error(out_error, YGDL_ERROR_CODE_IO_ERROR, [&] {
					return "The given file \"" + path + "\" couldn't be opened for reading: " + last_error();
				});
				return false;
			}

			// ... and then convert it into a writer
			if(!read_only) {
				if(!mz_zip_writer_init_from_reader_v2(&archive, path.c_str(), flags)) {
					set_error(out_error, YGDL_ERROR_CODE_IO_ERROR, [&] {
						return "The given file \"" + path + "\" couldn't be opened for writing: " + last_error();
					});
					// archive is already closed by miniz
					return false;
				}
			}
		} else {
			if(!mz_zip_writer_init_file_v2(&archive, path.c_str(), 0, flags)) {
				set_error(out_error, YGDL_ERROR_CODE_IO_ERROR, [&] {
					return "The given file \"" + path + "\" couldn't be opened for writing: " + last_error();
				});
				return false;
			}
		}

		/* // currently not working, because of library problems
		if(!mz_zip_validate_archive(&archive, MZ_ZIP_FLAG_VALIDATE_HEADERS_ONLY)) {
			set_error(out_error, YGDL_ERROR_CODE_IO_ERROR, [&] {
				return "The given file \"" + path + "\" is not a valid archive: " + last_error();
			});
			return false;
		}
		 */

		// populate entry lists
		if(!foreach_file(out_error, [&](mz_uint zip_index, Entry_type type, std::string_view name) {
			   register_entry(zip_index, type, name);
		   }))
			return false;

		opened = true;
		return true;
	}
	void close()
	{
		if(opened) {
			mz_zip_writer_finalize_archive(&archive);
			mz_zip_end(&archive);
			opened = false;
		}
	}

	void register_entry(mz_uint zip_index, Entry_type type, std::string_view name)
	{
		if(type == Entry_type::user_data)
			entries_userdata.emplace(name, zip_index);
		else if(type == Entry_type::config)
			entries_config.emplace(name, zip_index);
		else if(type == Entry_type::layer_definition)
			entries_layer_definitions.emplace(name, zip_index);
		else if(auto id = parse_id(name)) {
			auto& entry = entries[type];
			entry.key_to_zip_entry.emplace(*id, zip_index);
			entry.next_key = std::max(entry.next_key, *id + 1u);
		} else
			assert(!"Unexpected entry in archive file");
	}

	bool foreach_file(YGDL_Error* out_error, std::invocable<mz_uint, Entry_type, std::string_view> auto&& callback)
	{
		mz_zip_archive_file_stat file_stat;

		const auto count = mz_zip_reader_get_num_files(&archive);
		for(mz_uint i = 0; i < count; ++i) {
			if(!mz_zip_reader_file_stat(&archive, i, &file_stat)) {
				set_error(out_error, YGDL_ERROR_CODE_IO_ERROR, [&] {
					return "Error reading files in archive \"" + path + "\": " + last_error();
				});
				return false;
			}

			const auto name = std::string_view(file_stat.m_filename);

			// skip any file that doesn't match our schema and add the others to the entry map
			if(name.size() >= 2 && name[1] == '/') {
				const auto type = static_cast<Entry_type>(name[0]);
				switch(type) {
					case Entry_type::config:
					case Entry_type::world:
					case Entry_type::layer_unstructured:
					case Entry_type::layer_structured:
					case Entry_type::mesh:
					case Entry_type::layer_definition:
					case Entry_type::user_data: callback(i, type, name.substr(2)); break;
				}
			}
		}
		return true;
	}

	std::string key_to_name(Entry_type type, std::uint64_t key)
	{
		auto name = std::string();
		name.resize(2 + 16);
		name[0] = static_cast<char>(type);
		name[1] = '/';
		name.erase(std::to_chars(name.data() + 2, name.data() + name.size(), key, 16).ptr - name.data());
		return name;
	}

	std::string key_to_name(Entry_type type, std::string_view key)
	{
		auto name = std::string();
		name.reserve(2 + key.size());
		name.push_back(static_cast<char>(type));
		name.push_back('/');
		name.append(key);
		return name;
	}

	template <Entry_type Type, typename Key>
	bool write_entry(const Key& key, std::span<const std::byte> data, YGDL_Error* out_error)
	{
		if(read_only) {
			set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] {
				return "Can't add files to archive \"" + path + "\" opened as read-only.";
			});
			return false;
		}

		if(auto existing_index = locate_entry<Type>(key); existing_index) {
			set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] {
				return "A file entry with the name " + key_to_name(Type, key) + " already exists in archive \""
				       + path + "\". Overwriting files is currently not supported.";
			});
			return false;
		}

		auto name = key_to_name(Type, key);

		if(!mz_zip_writer_add_mem(&archive, name.c_str(), data.data(), data.size(), compression_level)) {
			set_error(out_error, YGDL_ERROR_CODE_IO_ERROR, [&] {
				return "Error adding file to archive \"" + path + "\": " + last_error();
			});
			return false;
		}

		const auto index = mz_zip_reader_get_num_files(&archive) - 1;
		assert(index == mz_uint(mz_zip_reader_locate_file(&archive, name.c_str(), nullptr, 0)));

		register_entry(index, Type, std::string_view(name).substr(2));

		return true;
	}

	template <Entry_type Type, typename Key>
	std::string filename(const Key& key)
	{
		return path + ":" + key_to_name(Type, key);
	}

	template <Entry_type Type, typename Key>
	std::optional<mz_uint> locate_entry(const Key& key) const
	{
		if constexpr(Type == Entry_type::user_data) {
			if(auto iter = entries_userdata.find(key); iter != entries_userdata.end())
				return iter->second;

		} else if constexpr(Type == Entry_type::config) {
			if(auto iter = entries_config.find(key); iter != entries_config.end())
				return iter->second;

		} else if constexpr(Type == Entry_type::layer_definition) {
			if(auto iter = entries_layer_definitions.find(key); iter != entries_layer_definitions.end())
				return iter->second;

		} else {
			if(auto iter = entries.find(Type); iter != entries.end()) {
				if(auto iter_2 = iter->second.key_to_zip_entry.find(key);
				   iter_2 != iter->second.key_to_zip_entry.end()) {
					return iter_2->second;
				}
			}
		}

		return std::nullopt;
	}
	template <Entry_type Type, typename Key>
	bool read_entry(const Key& key, YGDL_Error* out_error, auto&& callback)
	{
		// locate the file
		auto index = locate_entry<Type>(key);
		if(!index) {
			set_error(out_error, YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS, [&] {
				return "Couldn't find file entry " + key_to_name(Type, key) + " in archive \"" + path + "\"";
			});
			return false;
		}

		// retrieve the file size
		mz_zip_archive_file_stat file_stat;
		if(!mz_zip_reader_file_stat(&archive, *index, &file_stat)) {
			set_error(out_error, YGDL_ERROR_CODE_IO_ERROR, [&] {
				return "Couldn't determine size of file entry " + key_to_name(Type, key) + " from archive \""
				       + path + "\": " + last_error();
			});
			return false;
		}
		auto size_remaining = file_stat.m_uncomp_size;

		// open a file iterator
		auto       close_failure = false;
		const auto free_iter     = [&](mz_zip_reader_extract_iter_state* i) {
            mz_zip_clear_last_error(&archive);
            if(!mz_zip_reader_extract_iter_free(i) && size_remaining == 0) {
                // failures on close are ignored if the file wasn't read completely, because then the miniz
                //   validation always fails...
                close_failure = mz_zip_peek_last_error(&archive) != MZ_ZIP_NO_ERROR;
            }
		};
		using I   = std::unique_ptr<mz_zip_reader_extract_iter_state, decltype(free_iter)>;
		auto iter = I{mz_zip_reader_extract_iter_new(&archive, *index, 0), free_iter};
		if(!iter) {
			set_error(out_error, YGDL_ERROR_CODE_IO_ERROR, [&] {
				return "Couldn't read file entry " + key_to_name(Type, key) + " from archive \"" + path
				       + "\": " + last_error();
			});
			return false;
		}

		// read the file
		callback([&](std::span<std::byte> buffer) -> size_t {
			if(buffer.size() == 0)
				return size_remaining;

			auto read = mz_zip_reader_extract_iter_read(iter.get(), buffer.data(), buffer.size());
			size_remaining -= read;
			return read;
		});

		// close the iterator and check its error flag
		iter.reset();
		if(close_failure) {
			set_error(out_error, YGDL_ERROR_CODE_IO_ERROR, [&] {
				return "Couldn't read file entry " + key_to_name(Type, key) + " from archive \"" + path
				       + "\" (closing the handle failed): " + last_error();
			});
			return false;
		}

		return true;
	}
};

YGDL_Bool YGDL_serialization_supported(void)
{
	return YGDL_TRUE;
}

YGDL_Serializer* YGDL_serializer_create(
        YGDL_String_view path, YGDL_Bool write, YGDL_Bool overwrite, YGDL_Error* out_error)
{
	if(overwrite && write) {
		auto fs_path = std::filesystem::path(from_ygdl_string_view(path));
		if(std::filesystem::exists(fs_path)) {
			auto error = std::error_code{};
			if(!std::filesystem::remove(fs_path, error)) {
				set_error(out_error, YGDL_ERROR_CODE_IO_ERROR, [&] {
					return detail::concat(
					        "The old archive file couldn't be deleted \"", fs_path, "\": ", error.message());
				});
				return nullptr;
			}
		}
	}

	auto serializer = std::make_unique<YGDL_Serializer>(path, write);
	if(!serializer->open(out_error))
		return nullptr;

	return serializer.release();
}

void YGDL_serializer_destroy(YGDL_Serializer* serializer)
{
	delete serializer;
}

void YGDL_serializer_flush(YGDL_Serializer* serializer, YGDL_Error* out_error)
{
	assert(serializer);
	serializer->flush(out_error);
}

void YGDL_serializer_shrink_read_caches(YGDL_Serializer* serializer)
{
	assert(serializer);
	serializer->shrink_read_caches();
}

YGDL_Bool YGDL_serializer_userdata_write(
        YGDL_Serializer* serializer, YGDL_String_view key, const void* data, size_t length, YGDL_Error* out_error)
{
	assert(serializer);
	assert(data);

	auto       data_span = std::span(static_cast<const std::byte*>(data), static_cast<std::size_t>(length));
	const auto r         = serializer->write_entry<Entry_type::user_data>(
            detail::from_ygdl_string_view(key), data_span, out_error);
	return r ? YGDL_TRUE : YGDL_FALSE;
}

YGDL_Bool YGDL_serializer_userdata_read(YGDL_Serializer*        serializer,
                                        YGDL_String_view        key,
                                        void*                   ctx,
                                        YGDL_Serializer_reader* reader,
                                        YGDL_Error*             out_error)
{
	assert(serializer);
	assert(reader);

	auto callback = [&](auto&& read) {
		reader(ctx, &read, [](void* reader_ctx, size_t bytes_to_read, void* buffer) -> size_t {
			auto& rr = *static_cast<std::remove_reference_t<decltype(read)>*>(reader_ctx);
			return rr(std::span(static_cast<std::byte*>(buffer), static_cast<std::size_t>(bytes_to_read)));
		});
	};
	const auto r = serializer->read_entry<Entry_type::user_data>(
	        detail::from_ygdl_string_view(key), out_error, callback);
	return r ? YGDL_TRUE : YGDL_FALSE;
}

void YGDL_serializer_userdata_iterate(
        const YGDL_Serializer* serializer, void* ctx, YGDL_Serializer_userdata_callback* callback)
{
	assert(serializer);
	assert(callback);

	for(auto&& [name, index] : serializer->entries_userdata) {
		callback(ctx, detail::to_ygdl_string_view(name));
	}
}


static void write_dict(ubjson::Writer& writer, Const_dictionary_view dict);
static void write_array(ubjson::Writer& writer, Const_array_view<void> array)
{
	array.visit_value_types(
	        [&](Const_array_view<Const_dictionary_view> nested_dicts) {
		        writer.begin_array(nested_dicts.size());
		        for(auto e : nested_dicts)
			        write_dict(writer, e);
	        },
	        [&](Const_array_view<Const_array_view<void>> nested_arrays) {
		        writer.begin_array(nested_arrays.size());
		        for(auto e : nested_arrays)
			        write_array(writer, e);
	        },
	        [&](const auto& value) { writer.write(std::span(value)); },
	        [&]() { writer.write_null(); });
}
static void write_dict_values(ubjson::Writer& writer, Const_dictionary_view dict)
{
	for(auto&& key : dict.keys()) {
		writer.write_key(key);

		dict[key].visit([&](Const_dictionary_view nested_dict) { write_dict(writer, nested_dict); },
		                [&](Const_array_view<void> nested_array) { write_array(writer, nested_array); },
		                [&](const auto& value) { writer.write(value); },
		                [&]() { writer.write_null(); });
	}
}
static void write_dict(ubjson::Writer& writer, Const_dictionary_view dict)
{
	auto _ = writer.begin_object();
	write_dict_values(writer, dict);
}
YGDL_Bool YGDL_serializer_config_write(
        YGDL_Serializer* serializer, YGDL_String_view key, const YGDL_Dict* dict, YGDL_Error* out_error)
{
	assert(serializer);
	assert(dict);

	return catch_error(out_error, YGDL_FALSE, [&]() -> YGDL_Bool {
		auto writer = ubjson::Writer{serializer->scratch_buffer};
		write_dict(writer, *dict);

		return serializer->write_entry<Entry_type::config>(
		               detail::from_ygdl_string_view(key), writer.bytes(), out_error)
		               ? YGDL_TRUE
		               : YGDL_FALSE;
	});
}

template <typename BR>
static void read_dict(ubjson::Reader<BR>& reader, Dictionary_ref dict, YGDL_Error* out_error);
template <typename BR>
static void read_array(ubjson::Reader<BR>& reader, Array_ref<void> array, YGDL_Error* out_error)
{
	reader.read_array([&](Type element_type, std::optional<std::size_t> count, auto&& read_elements) {
		switch(element_type) {
			case Type::none:
				set_error(out_error, YGDL_ERROR_CODE_IO_ERROR, [&] {
					return "Invalid array value of type 'none' in file";
				});
				break;

			case Type::dict_t: {
				auto typed_array = array.cast_to<Dictionary_ref>();
				if(count)
					typed_array.reserve(static_cast<index_t>(*count));

				read_elements([&] { read_dict(reader, typed_array.push_back(), out_error); });
				break;
			}
			case Type::array_t: {
				auto typed_array = array.cast_to<Array_ref<void>>();
				if(count)
					typed_array.reserve(static_cast<index_t>(*count));

				read_elements([&] { read_array(reader, typed_array.push_back(), out_error); });
				break;
			}

			case Type::bool_t: read_elements(array.cast_to<YGDL_Bool>()); break;
			case Type::int8_t: read_elements(array.cast_to<std::int8_t>()); break;
			case Type::int32_t: read_elements(array.cast_to<std::int32_t>()); break;
			case Type::int64_t: read_elements(array.cast_to<std::int64_t>()); break;
			case Type::float_t: read_elements(array.cast_to<float>()); break;
			case Type::double_t: read_elements(array.cast_to<double>()); break;
			case Type::vec2_t: read_elements(array.cast_to<Vec2>()); break;
			case Type::vec3_t: read_elements(array.cast_to<Vec3>()); break;
			case Type::string_t: read_elements(array.cast_to<String>()); break;
		}
	});
}
template <typename BR>
static void read_dict(ubjson::Reader<BR>& reader, Dictionary_ref dict, YGDL_Error* out_error)
{
	dict.clear();
	reader.read_object([&](auto key) {
		auto value = dict[key];
		switch(reader.type()) {
			case Type::none: break;
			case Type::dict_t: read_dict(reader, value.template get<Dictionary_ref>(), out_error); break;
			case Type::array_t: read_array(reader, value.template get<Array_ref<void>>(), out_error); break;
			default: std::visit([&](auto&& v) { value = std::move(v); }, reader.read_single()); break;
		}
	});
}
YGDL_Bool YGDL_serializer_config_read(
        YGDL_Serializer* serializer, YGDL_String_view key, YGDL_Dict* dict, YGDL_Error* out_error)
{
	assert(serializer);
	assert(dict);

	return catch_error(out_error, YGDL_FALSE, [&]() -> YGDL_Bool {
		const auto key_std = detail::from_ygdl_string_view(key);

		auto r = serializer->read_entry<Entry_type::config>(key_std, out_error, [&](auto&& read) {
			auto reader = ubjson::Reader{
			        serializer->filename<Entry_type::config>(key_std), serializer->scratch_buffer, read};
			read_dict(reader, *dict, out_error);
		});

		return r ? YGDL_TRUE : YGDL_FALSE;
	});
}
void YGDL_serializer_config_iterate(
        const YGDL_Serializer* serializer, void* ctx, YGDL_Serializer_config_callback* callback)
{
	assert(serializer);
	assert(callback);

	for(auto&& [name, index] : serializer->entries_config) {
		callback(ctx, detail::to_ygdl_string_view(name));
	}
}


template <Entry_type Type>
static std::int32_t write_ubson_object(YGDL_Serializer* serializer, std::invocable<ubjson::Writer&> auto&& callback)
{
	auto  writer        = ubjson::Writer{serializer->scratch_buffer};
	auto& world_entries = serializer->entries[Type];

	writer.write_object([&] { callback(writer); });

	const auto key = world_entries.next_key++;
	serializer->write_entry<Type>(key, writer.bytes(), Error_throw{});
	return static_cast<std::int32_t>(key);
}
template <Entry_type Type, typename T>
static std::int32_t write_ubson_object(
        YGDL_Serializer* serializer, const Cow_ptr<T>& ptr, std::invocable<ubjson::Writer&> auto&& callback)
{
	auto&      world_entries = serializer->entries[Type];
	const auto ptr_key       = Ptr_key{&ptr.get_const(), ptr.version()};
	if(auto existing = world_entries.ptr_to_key.find(ptr_key); existing != world_entries.ptr_to_key.end()) {
		return existing->second;
	}

	auto writer = ubjson::Writer{serializer->scratch_buffer};
	writer.write_object([&] { callback(writer); });

	const auto key = world_entries.next_key++;
	serializer->write_entry<Type>(key, writer.bytes(), Error_throw{});

	world_entries.ptr_to_key.emplace(ptr_key, key);
	return static_cast<std::int32_t>(key);
}
static std::int32_t write_world_part(YGDL_Serializer* serializer, const Cow_ptr<Mesh_impl>& mesh_ptr)
{
	return write_ubson_object<Entry_type::mesh>(serializer, mesh_ptr, [&](ubjson::Writer& writer) {
		auto& mesh = mesh_ptr.get_const();

		// all data is stored as sequences of 32-bit integers
		auto to_span = [](const auto* first, YGDL_Index count) {
			return std::span<const std::int32_t>(reinterpret_cast<const std::int32_t*>(first), count);
		};

		writer.write_object_value("vertex_edge", to_span(mesh.vertex_edge, mesh.vertex_count));
		writer.write_object_value("face_edge", to_span(mesh.face_edge, mesh.face_count));

		writer.write_object_value("primal_edge_origin", to_span(mesh.primal_edge_origin, mesh.edge_count));
		writer.write_object_value("primal_edge_next", to_span(mesh.primal_edge_next, mesh.edge_count));

		writer.write_object_value("dual_edge_origin", to_span(mesh.dual_edge_origin, mesh.edge_count));
		writer.write_object_value("dual_edge_next", to_span(mesh.dual_edge_next, mesh.edge_count));
	});
}
static std::int32_t write_world_part(YGDL_Serializer* serializer, const Cow_ptr<Dictionary>& layer_ptr)
{
	return write_ubson_object<Entry_type::layer_unstructured>(serializer, layer_ptr, [&](ubjson::Writer& writer) {
		write_dict_values(writer, layer_ptr.get_const());
	});
}
template <typename T>
static std::span<const T> layer_value_span(const YGDL_Layer& layer)
{
	return std::span(static_cast<const T*>(layer.data), layer.length);
};
static std::int32_t write_world_part(YGDL_Serializer* serializer, const Cow_ptr<YGDL_Layer_impl>& layer_ptr)
{
	const auto& layer = layer_ptr.get_const();
	const auto& def   = *layer.definition;

	// write the definition, if it doesn't already exist
	const auto definition_id = from_ygdl_string_view(def.id);
	if(serializer->entries_layer_definitions.find(definition_id) == serializer->entries_layer_definitions.end()) {
		auto writer = ubjson::Writer{serializer->scratch_buffer};
		writer.write_object([&] {
			writer.write_object_value("id", definition_id);
			writer.write_object_value("description", from_ygdl_string_view(def.description));
			writer.write_object_value("data_type", def.data_type);
			writer.write_object_value("ref_type", def.ref_type);
			writer.write_object_value("invalidation", def.invalidation);
			writer.write_object_value("interpolation", def.interpolation);

			switch(def.data_type) {
				case YGDL_TYPE_NONE:
				case YGDL_TYPE_DICT:
				case YGDL_TYPE_ARRAY:
				case YGDL_TYPE_STRING: assert(!"Layer of invalid type in serialization"); break;
				case YGDL_TYPE_BOOL:
					writer.write_object_value("initial", def.value_info.bool_value.initial);
					break;

				case YGDL_TYPE_INT8:
					writer.write_object_value("initial", def.value_info.int8_value.initial);
					writer.write_object_value("min", def.value_info.int8_value.min);
					writer.write_object_value("max", def.value_info.int8_value.max);
					break;

				case YGDL_TYPE_INT32:
					writer.write_object_value("initial", def.value_info.int32_value.initial);
					writer.write_object_value("min", def.value_info.int32_value.min);
					writer.write_object_value("max", def.value_info.int32_value.max);
					break;

				case YGDL_TYPE_INT64:
					writer.write_object_value("initial", def.value_info.int64_value.initial);
					writer.write_object_value("min", def.value_info.int64_value.min);
					writer.write_object_value("max", def.value_info.int64_value.max);
					break;

				case YGDL_TYPE_FLOAT:
					writer.write_object_value("initial", def.value_info.float_value.initial);
					writer.write_object_value("min", def.value_info.float_value.min);
					writer.write_object_value("max", def.value_info.float_value.max);
					writer.write_object_value("has_min", def.value_info.float_value.has_min);
					writer.write_object_value("has_max", def.value_info.float_value.has_max);
					writer.write_object_value("allow_nan", def.value_info.float_value.allow_nan);
					writer.write_object_value("allow_inf", def.value_info.float_value.allow_inf);
					break;

				case YGDL_TYPE_DOUBLE:
					writer.write_object_value("initial", def.value_info.double_value.initial);
					writer.write_object_value("min", def.value_info.double_value.min);
					writer.write_object_value("max", def.value_info.double_value.max);
					writer.write_object_value("has_min", def.value_info.double_value.has_min);
					writer.write_object_value("has_max", def.value_info.double_value.has_max);
					writer.write_object_value("allow_nan", def.value_info.double_value.allow_nan);
					writer.write_object_value("allow_inf", def.value_info.double_value.allow_inf);
					break;

				case YGDL_TYPE_VEC2:
					writer.write_object_value("initial", def.value_info.vec2_value.initial);
					writer.write_object_value("min", def.value_info.vec2_value.min);
					writer.write_object_value("max", def.value_info.vec2_value.max);
					writer.write_object_value("min_length", def.value_info.vec2_value.min_length);
					writer.write_object_value("max_length", def.value_info.vec2_value.max_length);
					writer.write_object_value("has_min", def.value_info.vec2_value.has_min);
					writer.write_object_value("has_max", def.value_info.vec2_value.has_max);
					writer.write_object_value("has_min_length", def.value_info.vec2_value.has_min_length);
					writer.write_object_value("has_max_length", def.value_info.vec2_value.has_max_length);
					writer.write_object_value("allow_nan", def.value_info.vec2_value.allow_nan);
					writer.write_object_value("allow_inf", def.value_info.vec2_value.allow_inf);
					break;

				case YGDL_TYPE_VEC3:
					writer.write_object_value("initial", def.value_info.vec3_value.initial);
					writer.write_object_value("min", def.value_info.vec3_value.min);
					writer.write_object_value("max", def.value_info.vec3_value.max);
					writer.write_object_value("min_length", def.value_info.vec3_value.min_length);
					writer.write_object_value("max_length", def.value_info.vec3_value.max_length);
					writer.write_object_value("has_min", def.value_info.vec3_value.has_min);
					writer.write_object_value("has_max", def.value_info.vec3_value.has_max);
					writer.write_object_value("has_min_length", def.value_info.vec3_value.has_min_length);
					writer.write_object_value("has_max_length", def.value_info.vec3_value.has_max_length);
					writer.write_object_value("allow_nan", def.value_info.vec3_value.allow_nan);
					writer.write_object_value("allow_inf", def.value_info.vec3_value.allow_inf);
					break;
			}
		});
		serializer->write_entry<Entry_type::layer_definition>(definition_id, writer.bytes(), Error_throw{});
		serializer->loaded_layer_definitions.emplace(definition_id, layer.interned_definition());
	} else {
		auto existing_iter = serializer->loaded_layer_definitions.find(definition_id);
		if(existing_iter == serializer->loaded_layer_definitions.end()
		   || !layer.compatible(*existing_iter->second)) {
			throw Precondition_violation_exception(
			        "A world was passed to the serializer, that contains a layer-instance of " + std::string(definition_id)
			        + " but isn't compatible with the preexisting definition of that layer from an already serialized world.");
		}
	}

	return write_ubson_object<Entry_type::layer_structured>(serializer, layer_ptr, [&](ubjson::Writer& writer) {
		writer.write_object_value("definition", definition_id);

		switch(def.data_type) {
			case YGDL_TYPE_NONE:
			case YGDL_TYPE_DICT:
			case YGDL_TYPE_ARRAY:
			case YGDL_TYPE_STRING: assert(!"Layer of invalid type in serialization"); break;

			case YGDL_TYPE_BOOL:
				writer.write_object_value("values", layer_value_span<YGDL_Bool>(layer));
				break;
			case YGDL_TYPE_INT8:
				writer.write_object_value("values", layer_value_span<std::int8_t>(layer));
				break;
			case YGDL_TYPE_INT32:
				writer.write_object_value("values", layer_value_span<std::int32_t>(layer));
				break;
			case YGDL_TYPE_INT64:
				writer.write_object_value("values", layer_value_span<std::int64_t>(layer));
				break;
			case YGDL_TYPE_FLOAT: writer.write_object_value("values", layer_value_span<float>(layer)); break;
			case YGDL_TYPE_DOUBLE:
				writer.write_object_value("values", layer_value_span<double>(layer));
				break;
			case YGDL_TYPE_VEC2:
				writer.write_object_value("values",
				                          std::span(static_cast<const float*>(layer.data), layer.length * 2));
				break;
			case YGDL_TYPE_VEC3:
				writer.write_object_value("values",
				                          std::span(static_cast<const float*>(layer.data), layer.length * 3));
				break;
		}
	});
}

YGDL_EXPORTED YGDL_Serializer_world_ref YGDL_serializer_world_write(
        YGDL_Serializer* serializer, const YGDL_World* world, YGDL_Error* out_error)
{
	assert(serializer);
	assert(world);

	return catch_error(out_error, 0, [&]() -> YGDL_Serializer_world_ref {
		// write the worlds individual parts (reusing existing files, if possible)
		const auto mesh_id = write_world_part(serializer, world->mesh);

		auto unstructured_layers = std::vector<std::tuple<std::string_view, std::int32_t>>();
		unstructured_layers.reserve(world->unstructured_layers.size());
		for(auto&& [layer_id, layer] : world->unstructured_layers) {
			unstructured_layers.emplace_back(layer_id, write_world_part(serializer, layer));
		}

		auto layers = std::vector<std::int32_t>();
		layers.reserve(world->layers.size());
		world->layers.foreach([&](auto& layer) { layers.push_back(write_world_part(serializer, layer)); });

		// write the world itself
		const auto world_key = write_ubson_object<Entry_type::world>(serializer, [&](ubjson::Writer& writer) {
			writer.write_key("random");
			writer.write_object([&] {
				writer.write_object_value("state", static_cast<std::int64_t>(world->random.state));
				writer.write_object_value("inc", static_cast<std::int64_t>(world->random.inc));
			});

			writer.write_object_value("mesh", mesh_id);

			writer.write_key("unstructured_layers");
			writer.write_object([&] {
				for(auto&& [key, layer] : unstructured_layers)
					writer.write_object_value(key, layer);
			});

			writer.write_key("mesh_layers");
			writer.write(layers);
		});

		return static_cast<YGDL_Serializer_world_ref>(world_key);
	});
}

template <Entry_type Type>
static void read_ubson_object(YGDL_Serializer* serializer, auto key, auto&& callback)
{
	serializer->read_entry<Type>(key, Error_throw{}, [&](auto&& read) {
		auto reader = ubjson::Reader{serializer->filename<Type>(key), serializer->scratch_buffer, read};

		reader.read_object([&](std::string_view key) { callback(key, reader); });
	});
}
namespace yggdrasill::detail {
	namespace {
		/// wrapper that makes vectors with int32_t-like elements (uint32_t, Vertex, Edge, Face, ...) compatible
		/// with the UBJSON parser
		template <typename T>
		struct Wrapper {
			std::vector<T>& container;

			using value_type = std::int32_t;

			static_assert(sizeof(T) == sizeof(value_type));
			static_assert(alignof(T) == alignof(value_type));
			static_assert(std::is_standard_layout_v<T>);

			Wrapper(std::vector<T>& container) : container(container)
			{
				static_assert(ubjson::Container<Wrapper<T>>);
			}

			void clear() { container.clear(); }
			void reserve(std::size_t capacity) { container.reserve(capacity); }
			void resize(std::size_t capacity) { container.resize(capacity); }
			void push_back(value_type v) { container.emplace_back(T(v)); }
			operator std::span<value_type>()
			{
				return {reinterpret_cast<value_type*>(container.data()), container.size()};
			}
		};
	} // namespace
	void read_mesh_data(Mesh_impl& mesh, std::string_view key, auto& reader)
	{
		using namespace std::string_view_literals;

		if(key == "vertex_edge"sv) {
			reader.read_array_into(Wrapper{mesh.vertex_edges_});
		} else if(key == "face_edge"sv) {
			reader.read_array_into(Wrapper{mesh.face_edges_});
		} else if(key == "primal_edge_origin"sv) {
			reader.read_array_into(Wrapper{mesh.primal_edge_origin_});
		} else if(key == "primal_edge_next"sv) {
			reader.read_array_into(Wrapper{mesh.primal_edge_next_});
		} else if(key == "dual_edge_origin"sv) {
			reader.read_array_into(Wrapper{mesh.dual_edge_origin_});
		} else if(key == "dual_edge_next"sv) {
			reader.read_array_into(Wrapper{mesh.dual_edge_next_});
		} else if(key == "used_vertices"sv || key == "used_faces"sv || key == "used_edges"sv) {
			// ignored; values used by previous version
			reader.read_array([&](Type, std::optional<std::size_t>, auto&& callback) {
				callback([&] { (void) reader.read_single(); });
			});
		} else {
			YGDL_warning_log(detail::concat("Unexpected key '",
			                                key,
			                                +"' parsing mesh file '",
			                                reader.filename(),
			                                "' near position ",
			                                reader.file_position(),
			                                " (0x",
			                                std::hex,
			                                reader.file_position(),
			                                ").")
			                         .c_str());
		}
	}
	void restore_free_lists(Mesh_impl& mesh)
	{
		// collect unused edges
		mesh.free_edges_.reserve(
		        1 + std::count(mesh.primal_edge_next_.begin(), mesh.primal_edge_next_.end(), no_edge));
		for(auto i = index_t(mesh.primal_edge_next_.size()) - 1; i >= 0; i -= 2) {
			if(mesh.primal_edge_next_[i] == no_edge) {
				mesh.free_edges_.push_back(Primal_edge{i}.base());
			}
		}

		// collect unused faces
		mesh.free_faces_.reserve(1 + std::count(mesh.face_edges_.begin(), mesh.face_edges_.end(), no_edge));
		for(auto i = index_t(mesh.face_edges_.size()) - 1; i >= 0; --i) {
			if(mesh.face_edges_[i] == no_edge) {
				mesh.free_faces_.push_back(Face{i});
			}
		}

		// collect unused vertices
		mesh.free_vertices_.reserve(1 + std::count(mesh.vertex_edges_.begin(), mesh.vertex_edges_.end(), no_edge));
		for(auto i = index_t(mesh.vertex_edges_.size()) - 1; i >= 0; --i) {
			if(mesh.vertex_edges_[i] == no_edge) {
				mesh.free_vertices_.push_back(Vertex{i});
			}
		}
	}
} // namespace yggdrasill::detail
static Cow_ptr<Mesh_impl> read_world_mesh(YGDL_Serializer* serializer, std::int32_t id)
{
	// lookup id in cache
	auto& cache = serializer->loaded_meshes;
	if(auto existing = cache.find(id); existing != cache.end()) {
		if(auto cached_mesh = existing->second.lock())
			return *cached_mesh;
	}

	auto  mesh     = Cow_ptr<Mesh_impl>();
	auto* mesh_mut = mesh.acquire_mutable(Error_throw{});
	auto  release  = finally([&] { mesh.release_mutable(); });
	read_ubson_object<Entry_type::mesh>(
	        serializer, id, [&](auto key, auto& reader) { read_mesh_data(*mesh_mut, key, reader); });

	restore_free_lists(*mesh_mut); // the free-lists are not stored directly and have to be re-constructed
	mesh_mut->update_all_data_pointers(); // set all pointers and sizes in the C-API to reference the loaded data

	release.call_early();
	cache.emplace(id, mesh.acquire_weak_ptr());

	return mesh;
}
static Cow_ptr<Dictionary> read_world_unstructured_layer(YGDL_Serializer* serializer, std::int32_t id)
{
	// lookup id in cache
	auto& cache = serializer->loaded_unstructured_layers;
	if(auto existing = cache.find(id); existing != cache.end()) {
		if(auto cached_layer = existing->second.lock())
			return *cached_layer;
	}

	auto  layer     = Cow_ptr<Dictionary>();
	auto* layer_mut = layer.acquire_mutable(Error_throw{});
	auto  release   = finally([&] { layer.release_mutable(); });

	serializer->read_entry<Entry_type::layer_unstructured>(id, Error_throw{}, [&](auto&& read) {
		auto reader = ubjson::Reader{
		        serializer->filename<Entry_type::layer_unstructured>(id), serializer->scratch_buffer, read};
		read_dict(reader, *layer_mut, Error_throw{});
	});

	release.call_early();
	cache.emplace(id, layer.acquire_weak_ptr());

	return layer;
}
static std::shared_ptr<const Interned_definition> read_world_layer_definition(YGDL_Serializer* serializer,
                                                                              std::string_view id)
{
	using namespace std::string_view_literals;

	// lookup id in cache
	auto& cache = serializer->loaded_layer_definitions;
	if(auto existing = cache.find(id); existing != cache.end()) {
		return existing->second;
	}

	auto def = std::make_shared<Interned_definition>();
	read_ubson_object<Entry_type::layer_definition>(serializer, id, [&](auto key, auto& reader) {
		if(key == "id"sv) {
			reader.read_value_into(def->interned_id);
			def->id = detail::to_ygdl_string_view(def->interned_id);
		} else if(key == "description"sv) {
			reader.read_value_into(def->interned_description);
			def->description = detail::to_ygdl_string_view(def->interned_description);
		} else if(key == "data_type"sv) {
			reader.read_value_into(def->data_type);
		} else if(key == "ref_type"sv) {
			reader.read_value_into(def->ref_type);
		} else if(key == "invalidation"sv) {
			reader.read_value_into(def->invalidation);
		} else if(key == "interpolation"sv) {
			reader.read_value_into(def->interpolation);

		} else {
			switch(def->data_type) {
				case YGDL_TYPE_NONE:
				case YGDL_TYPE_DICT:
				case YGDL_TYPE_ARRAY:
				case YGDL_TYPE_STRING:
					throw Io_error_exception("Invalid, missing or out-of-order data_type in layer definition "
					                         + serializer->filename<Entry_type::layer_definition>(id));

				case YGDL_TYPE_BOOL:
					if(key == "initial"sv) {
						return reader.read_value_into(def->value_info.bool_value.initial);
					}
					break;

				case YGDL_TYPE_INT8:
					if(key == "initial"sv) {
						return reader.read_value_into(def->value_info.int8_value.initial);
					} else if(key == "min"sv) {
						return reader.read_value_into(def->value_info.int8_value.min);
					} else if(key == "max"sv) {
						return reader.read_value_into(def->value_info.int8_value.max);
					}
					break;

				case YGDL_TYPE_INT32:
					if(key == "initial"sv) {
						return reader.read_value_into(def->value_info.int32_value.initial);
					} else if(key == "min"sv) {
						return reader.read_value_into(def->value_info.int32_value.min);
					} else if(key == "max"sv) {
						return reader.read_value_into(def->value_info.int32_value.max);
					}
					break;

				case YGDL_TYPE_INT64:
					if(key == "initial"sv) {
						return reader.read_value_into(def->value_info.int64_value.initial);
					} else if(key == "min"sv) {
						return reader.read_value_into(def->value_info.int64_value.min);
					} else if(key == "max"sv) {
						return reader.read_value_into(def->value_info.int64_value.max);
					}
					break;

				case YGDL_TYPE_FLOAT:
					if(key == "initial"sv) {
						return reader.read_value_into(def->value_info.float_value.initial);
					} else if(key == "min"sv) {
						return reader.read_value_into(def->value_info.float_value.min);
					} else if(key == "max"sv) {
						return reader.read_value_into(def->value_info.float_value.max);
					} else if(key == "has_min"sv) {
						return reader.read_value_into(def->value_info.float_value.has_min);
					} else if(key == "has_max"sv) {
						return reader.read_value_into(def->value_info.float_value.has_max);
					} else if(key == "allow_nan"sv) {
						return reader.read_value_into(def->value_info.float_value.allow_nan);
					} else if(key == "allow_inf"sv) {
						return reader.read_value_into(def->value_info.float_value.allow_inf);
					}
					break;

				case YGDL_TYPE_DOUBLE:
					if(key == "initial"sv) {
						return reader.read_value_into(def->value_info.double_value.initial);
					} else if(key == "min"sv) {
						return reader.read_value_into(def->value_info.double_value.min);
					} else if(key == "max"sv) {
						return reader.read_value_into(def->value_info.double_value.max);
					} else if(key == "has_min"sv) {
						return reader.read_value_into(def->value_info.double_value.has_min);
					} else if(key == "has_max"sv) {
						return reader.read_value_into(def->value_info.double_value.has_max);
					} else if(key == "allow_nan"sv) {
						return reader.read_value_into(def->value_info.double_value.allow_nan);
					} else if(key == "allow_inf"sv) {
						return reader.read_value_into(def->value_info.double_value.allow_inf);
					}
					break;

				case YGDL_TYPE_VEC2:
					if(key == "initial"sv) {
						return reader.read_value_into(def->value_info.vec2_value.initial);
					} else if(key == "min"sv) {
						return reader.read_value_into(def->value_info.vec2_value.min);
					} else if(key == "max"sv) {
						return reader.read_value_into(def->value_info.vec2_value.max);
					} else if(key == "min_length"sv) {
						return reader.read_value_into(def->value_info.vec2_value.min_length);
					} else if(key == "max_length"sv) {
						return reader.read_value_into(def->value_info.vec2_value.max_length);
					} else if(key == "has_min"sv) {
						return reader.read_value_into(def->value_info.vec2_value.has_min);
					} else if(key == "has_max"sv) {
						return reader.read_value_into(def->value_info.vec2_value.has_max);
					} else if(key == "has_min_length"sv) {
						return reader.read_value_into(def->value_info.vec2_value.has_min_length);
					} else if(key == "has_max_length"sv) {
						return reader.read_value_into(def->value_info.vec2_value.has_max_length);
					} else if(key == "allow_nan"sv) {
						return reader.read_value_into(def->value_info.vec2_value.allow_nan);
					} else if(key == "allow_inf"sv) {
						return reader.read_value_into(def->value_info.vec2_value.allow_inf);
					}
					break;

				case YGDL_TYPE_VEC3:
					if(key == "initial"sv) {
						return reader.read_value_into(def->value_info.vec3_value.initial);
					} else if(key == "min"sv) {
						return reader.read_value_into(def->value_info.vec3_value.min);
					} else if(key == "max"sv) {
						return reader.read_value_into(def->value_info.vec3_value.max);
					} else if(key == "min_length"sv) {
						return reader.read_value_into(def->value_info.vec3_value.min_length);
					} else if(key == "max_length"sv) {
						return reader.read_value_into(def->value_info.vec3_value.max_length);
					} else if(key == "has_min"sv) {
						return reader.read_value_into(def->value_info.vec3_value.has_min);
					} else if(key == "has_max"sv) {
						return reader.read_value_into(def->value_info.vec3_value.has_max);
					} else if(key == "has_min_length"sv) {
						return reader.read_value_into(def->value_info.vec3_value.has_min_length);
					} else if(key == "has_max_length"sv) {
						return reader.read_value_into(def->value_info.vec3_value.has_max_length);
					} else if(key == "allow_nan"sv) {
						return reader.read_value_into(def->value_info.vec3_value.allow_nan);
					} else if(key == "allow_inf"sv) {
						return reader.read_value_into(def->value_info.vec3_value.allow_inf);
					}
					break;
			}

			YGDL_warning_log(detail::concat("Unexpected key '",
			                                key,
			                                +"' parsing layer definition file '",
			                                reader.filename(),
			                                "' near position ",
			                                reader.file_position(),
			                                " (0x",
			                                std::hex,
			                                reader.file_position(),
			                                ").")
			                         .c_str());
		}
	});

	cache.emplace(id, def);

	return def;
}

static Cow_ptr<YGDL_Layer_impl> read_world_layer(YGDL_Serializer* serializer, std::int32_t id)
{
	using namespace std::string_view_literals;

	// lookup id in cache
	auto& cache = serializer->loaded_layers;
	if(auto existing = cache.find(id); existing != cache.end()) {
		if(auto cached_layer = existing->second.lock())
			return *cached_layer;
	}

	const auto filename = serializer->filename<Entry_type::layer_structured>(id);

	// parse the beginning of the file, to determine its definition-id and the offset of the data
	auto definition_name    = String();
	auto data_type          = Type::none;
	auto data_offset        = std::size_t(0);
	auto data_element_count = std::size_t(0);
	serializer->read_entry<Entry_type::layer_structured>(id, Error_throw{}, [&](auto&& read) {
		auto reader = ubjson::Reader{filename, serializer->scratch_buffer, read, 64};

		reader.read_object([&](std::string_view key) {
			if(key == "definition"sv) {
				reader.read_value_into(definition_name);
			} else if(key == "values"sv) {
				if(definition_name.empty())
					throw Io_error_exception(
					        "Missing or out-of-order definition in layer file. The definition has to be "
					        "the "
					        "first element.");

				std::tie(data_type, data_offset, data_element_count) = reader.read_array_header();
				return false;
			} else {
				YGDL_warning_log(detail::concat("Unexpected key '",
				                                key,
				                                +"' parsing layer file '",
				                                reader.filename(),
				                                "' near position ",
				                                reader.file_position(),
				                                " (0x",
				                                std::hex,
				                                reader.file_position(),
				                                ").")
				                         .c_str());
			}
			return true;
		});
	});
	if(definition_name.empty() || data_element_count == 0)
		throw Io_error_exception("Invalid layer in " + filename);

	// create the layer
	auto layer = [&] {
		auto definition = read_world_layer_definition(serializer, definition_name);

		switch(static_cast<Type>(definition->data_type)) {
			case Type::bool_t:
				return Cow_ptr<YGDL_Layer_impl>(std::in_place,
				                                std::move(definition),
				                                static_cast<index_t>(data_element_count),
				                                YGDL_Bool{});
			case Type::int8_t:
				return Cow_ptr<YGDL_Layer_impl>(std::in_place,
				                                std::move(definition),
				                                static_cast<index_t>(data_element_count),
				                                std::int8_t{});
			case Type::int32_t:
				return Cow_ptr<YGDL_Layer_impl>(std::in_place,
				                                std::move(definition),
				                                static_cast<index_t>(data_element_count),
				                                std::int32_t{});
			case Type::int64_t:
				return Cow_ptr<YGDL_Layer_impl>(std::in_place,
				                                std::move(definition),
				                                static_cast<index_t>(data_element_count),
				                                std::int64_t{});
			case Type::float_t:
				return Cow_ptr<YGDL_Layer_impl>(
				        std::in_place, std::move(definition), static_cast<index_t>(data_element_count), float{});
			case Type::double_t:
				return Cow_ptr<YGDL_Layer_impl>(std::in_place,
				                                std::move(definition),
				                                static_cast<index_t>(data_element_count),
				                                std::double_t{});
			case Type::vec2_t:
				return Cow_ptr<YGDL_Layer_impl>(std::in_place,
				                                std::move(definition),
				                                static_cast<index_t>(data_element_count / 2),
				                                YGDL_Vec2{});
			case Type::vec3_t:
				return Cow_ptr<YGDL_Layer_impl>(std::in_place,
				                                std::move(definition),
				                                static_cast<index_t>(data_element_count / 3),
				                                YGDL_Vec3{});

			default: throw Io_error_exception("Invalid/Unexpected element type in layer " + filename);
		}
	}();
	auto* layer_mut  = layer.acquire_mutable(Error_throw{});
	auto  release    = finally([&] { layer.release_mutable(); });
	auto& layer_data = layer_mut->unsafe_direct_storage_access();

	// load the data directly into the layers storage
	serializer->read_entry<Entry_type::layer_structured>(id, Error_throw{}, [&](auto&& read) {
		// skip initial header
		serializer->scratch_buffer.resize(data_offset);
		auto read_count = read(serializer->scratch_buffer);
		assert(read_count == data_offset);
		(void) read_count;

		// read the raw binary data into the layers storage
		auto data_view = std::visit(
		        [&](auto& v) -> std::span<std::byte> { return as_writable_bytes(std::span(v)); }, layer_data);
		read(data_view);

		// adjust/convert the read data to the concrete type
		switch(static_cast<Type>(layer_mut->definition->data_type)) {
			case Type::bool_t:
				for(auto& e : std::get<detail::Aligned_vector<YGDL_Bool>>(layer_data))
					e = e == 'T' ? YGDL_TRUE : YGDL_FALSE;
				break;
			case Type::int32_t:
				if constexpr(ubjson::little_endian) {
					for(auto& e : std::get<detail::Aligned_vector<int32_t>>(layer_data))
						e = ubjson::byteswap(e);
				}
				break;
			case Type::int64_t:
				if constexpr(ubjson::little_endian) {
					for(auto& e : std::get<detail::Aligned_vector<int64_t>>(layer_data))
						e = ubjson::byteswap(e);
				}
				break;

			case Type::int8_t:
			case Type::float_t:
			case Type::double_t:
			case Type::vec2_t:
			case Type::vec3_t: break; // nothing to do, data can be used as is

			default: break; // already handled above
		}
	});

	release.call_early();
	cache.emplace(id, layer.acquire_weak_ptr());

	return layer;
}
YGDL_EXPORTED YGDL_Bool YGDL_serializer_world_read(
        YGDL_Serializer* serializer, YGDL_Serializer_world_ref key, YGDL_World* world, YGDL_Error* out_error)
{
	using namespace std::string_view_literals;

	assert(serializer);
	assert(world);

	return catch_error(out_error, YGDL_FALSE, [&]() -> YGDL_Bool {
		const auto key_int = static_cast<std::uint32_t>(key);

		auto mesh                = std::int32_t(0);
		auto unstructured_layers = std::vector<std::pair<std::string, std::int32_t>>();
		auto layers              = std::vector<std::int32_t>();

		// load the world description file
		read_ubson_object<Entry_type::world>(serializer, key_int, [&](auto key, auto& reader) {
			if(key == "random"sv) {
				reader.read_object([&](std::string_view inner_key) {
					if(inner_key == "state"sv)
						reader.read_value_into(world->random.state);
					if(inner_key == "inc"sv)
						reader.read_value_into(world->random.inc);
				});
			} else if(key == "mesh"sv) {
				reader.read_value_into(mesh);
			} else if(key == "unstructured_layers"sv) {
				reader.read_object([&](auto key) {
					auto id = std::int32_t(0);
					reader.read_value_into(id);
					unstructured_layers.emplace_back(key, id);
				});
			} else if(key == "mesh_layers"sv) {
				reader.read_array_into(layers);
			} else {
				YGDL_warning_log(detail::concat("Unexpected key '",
				                                key,
				                                +"' parsing world file '",
				                                reader.filename(),
				                                "' near position ",
				                                reader.file_position(),
				                                " (0x",
				                                std::hex,
				                                reader.file_position(),
				                                ").")
				                         .c_str());
			}
		});

		// load the individual parts
		world->mesh = read_world_mesh(serializer, mesh);

		world->unstructured_layers.clear();
		for(auto&& [layer_name, id] : unstructured_layers) {
			world->unstructured_layers.emplace(std::move(layer_name),
			                                   read_world_unstructured_layer(serializer, id));
		}

		world->layers.clear();
		for(auto id : layers) {
			world->layers.insert(read_world_layer(serializer, id));
		}

		assert(YGDL_world_valid(world, out_error));

		return YGDL_TRUE;
	});
}
YGDL_EXPORTED void YGDL_serializer_world_iterate(
        const YGDL_Serializer* serializer, void* ctx, YGDL_Serializer_world_callback* callback)
{
	assert(serializer);
	assert(callback);

	auto entries_iter = serializer->entries.find(Entry_type::world);
	if(entries_iter == serializer->entries.end())
		return;

	for(auto&& [key, index] : entries_iter->second.key_to_zip_entry) {
		callback(ctx, static_cast<YGDL_Serializer_world_ref>(key));
	}
}

#else

using yggdrasill::set_error;

static constexpr auto error_msg =
        "Serialization is not available in this Yggdrasill build. Enable it by rebuilding with "
        "-DYGGDRASILL_SERIALIZATION=ON";

YGDL_Bool YGDL_serialization_supported(void)
{
	return YGDL_FALSE;
}
YGDL_Serializer* YGDL_serializer_create(YGDL_String_view, YGDL_Bool, YGDL_Bool, YGDL_Error* out_error)
{
	set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] { return error_msg; });
	return nullptr;
}
void YGDL_serializer_destroy(YGDL_Serializer*) {}
void YGDL_serializer_flush(YGDL_Serializer*, YGDL_Error* out_error)
{
	set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] { return error_msg; });
}
void      YGDL_serializer_shrink_read_caches(YGDL_Serializer*) {}
YGDL_Bool YGDL_serializer_userdata_write(
        YGDL_Serializer*, YGDL_String_view, const void*, size_t, YGDL_Error* out_error)
{
	set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] { return error_msg; });
	return YGDL_FALSE;
}
YGDL_Bool YGDL_serializer_userdata_read(
        YGDL_Serializer*, YGDL_String_view, void*, YGDL_Serializer_reader*, YGDL_Error* out_error)
{
	set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] { return error_msg; });
	return YGDL_FALSE;
}
void YGDL_serializer_userdata_iterate(const YGDL_Serializer*, void*, YGDL_Serializer_userdata_callback*) {}
YGDL_Bool YGDL_serializer_config_write(YGDL_Serializer*, YGDL_String_view, const YGDL_Dict*, YGDL_Error* out_error)
{
	set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] { return error_msg; });
	return YGDL_FALSE;
}
YGDL_Bool YGDL_serializer_config_read(YGDL_Serializer*, YGDL_String_view, YGDL_Dict*, YGDL_Error* out_error)
{
	set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] { return error_msg; });
	return YGDL_FALSE;
}
void YGDL_serializer_config_iterate(const YGDL_Serializer*, void*, YGDL_Serializer_config_callback*) {}
YGDL_Serializer_world_ref YGDL_serializer_world_write(YGDL_Serializer*, const YGDL_World*, YGDL_Error* out_error)
{
	set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] { return error_msg; });
	return 0;
}
YGDL_Bool YGDL_serializer_world_read(YGDL_Serializer*, YGDL_Serializer_world_ref, YGDL_World*, YGDL_Error* out_error)
{
	set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] { return error_msg; });
	return YGDL_FALSE;
}
void YGDL_serializer_world_iterate(const YGDL_Serializer*, void*, YGDL_Serializer_world_callback*) {}

#endif
