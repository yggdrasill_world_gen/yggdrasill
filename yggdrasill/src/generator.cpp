#include <yggdrasill/generator.h>

#include <yggdrasill/module.hpp>
#include <yggdrasill/string.hpp>
#include <yggdrasill/world.hpp>

#include <algorithm>
#include <filesystem>
#include <map>
#include <memory>
#include <optional>
#include <unordered_map>
#include <vector>

#if _WIN32
#include <Windows.h>
#else
#include <dlfcn.h>
#endif

namespace fs = std::filesystem;


struct YGDL_Plugin_init_handle {
	YGDL_Generator*            generator;
	std::vector<YGDL_Module*>* modules;
	YGDL_Error*                error;
	bool                       success = true;
};

struct YGDL_Module {
	yggdrasill::String                     id;
	YGDL_Data_version                      version = 1;
	std::unique_ptr<void, void (*)(void*)> userdata;
	bool                                   loaded = true;

	void* (*create)(YGDL_Error*);
	void (*destroy)(void*);
	void (*validate)(void*, YGDL_Dict*, YGDL_Error*);
	void (*execute)(void*, YGDL_World*, const YGDL_Dict*, YGDL_Error*);

	explicit YGDL_Module(const YGDL_Module_info* module_info)
	  : id(module_info->id)
	  , userdata(
	            module_info->create ? module_info->create(yggdrasill::Error_throw{}) : nullptr,
	            module_info->destroy ? module_info->destroy : +[](void*) {})
	  , validate(module_info->validate)
	  , execute(module_info->execute)
	{
	}

	// not copy- or movable because pointers to these objects are used externally and need to be stable for their whole lifetime
	YGDL_Module(YGDL_Module&&)                 = delete;
	YGDL_Module(const YGDL_Module&)            = delete;
	YGDL_Module& operator=(YGDL_Module&&)      = delete;
	YGDL_Module& operator=(const YGDL_Module&) = delete;
	~YGDL_Module()                             = default;

	// frees the userdata and marks the module as unavailable
	void unload()
	{
		userdata.reset();
		loaded   = false;
		create   = nullptr;
		destroy  = nullptr;
		validate = nullptr;
		execute  = nullptr;
	}
	void replace(const YGDL_Module_info* module_info)
	{
		assert(!loaded);
		userdata = {module_info->create ? module_info->create(yggdrasill::Error_throw{}) : nullptr,
		            module_info->destroy ? module_info->destroy : +[](void*) {}};
		validate = module_info->validate;
		execute  = module_info->execute;
		version++;
		loaded = true;
	}
};

namespace {
	using Library_handle = std::unique_ptr<void, void (*)(void*)>;

	using Init_func = void(YGDL_Plugin_init_handle*);

	struct Library {
		fs::file_time_type        last_write;
		Library_handle            os_handle;
		std::vector<YGDL_Module*> owned_modules;
		Library(const std::string& path, Library_handle&& os_handle)
		  : last_write(fs::last_write_time(path)), os_handle(std::move(os_handle))
		{
		}
	};

	constexpr auto library_suffix =
#if _WIN32
	        ".dll";
#else
	        ".so";
#endif

	std::optional<std::string> get_library_error()
	{
#if _WIN32
		auto error_code = ::GetLastError();
		if(error_code == 0)
			return {};

		auto buffer = (LPSTR) nullptr;
		auto size   = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM
                                           | FORMAT_MESSAGE_IGNORE_INSERTS,
                                   nullptr,
                                   error_code,
                                   MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                                   (LPSTR) &buffer,
                                   0,
                                   nullptr);

		auto message = std::string(buffer, size);
		LocalFree(buffer);
		return {std::move(message)};
#else
		auto error_msg = dlerror();
		return error_msg ? std::optional<std::string>(std::string(error_msg)) : std::optional<std::string>();
#endif
	}

	Library_handle load_library(std::string path, YGDL_Error* out_error)
	{
		void* handle =
#if _WIN32
		        LoadLibraryExA(
		                path.c_str(), NULL, LOAD_IGNORE_CODE_AUTHZ_LEVEL | LOAD_LIBRARY_SEARCH_APPLICATION_DIR);
#else
		        dlopen(path.c_str(), RTLD_NOW | RTLD_LOCAL);
#endif

		if(!handle) {
			yggdrasill::set_error(out_error, YGDL_ERROR_CODE_INVALID_PLUGIN, [&] {
				return "Error in load_library for \"" + path
				       + "\":" + get_library_error().value_or("No error message.");
			});
		}


		return
		{
			handle,
#if _WIN32
			        +[](void* h) { FreeLibrary(static_cast<HMODULE>(h)); }
#else
			        +[](void* h) { dlclose(h); }
#endif
		};
	}

	template <typename F>
	F* load_symbol(void* lib, const char* name)
	{
#if _WIN32
		return reinterpret_cast<F*>(GetProcAddress((HMODULE) lib, name));
#else
		return reinterpret_cast<F*>(dlsym(lib, name));
#endif
	}

	std::optional<Library> load_plugin(const std::string& lib_path, YGDL_Generator* gen, YGDL_Error* out_error)
	{
		auto os_handle = load_library(lib_path, out_error);
		if(!os_handle)
			return std::nullopt;

		auto init_function = load_symbol<Init_func>(os_handle.get(), "YGDL_init_plugin");
		if(!init_function) {
			const auto error_str = get_library_error();
			yggdrasill::set_error(out_error, YGDL_ERROR_CODE_INVALID_PLUGIN, [&] {
				return "Error in load_symbol for  \"" + lib_path
				       + "\": " + (error_str ? *error_str : "Couldn't locate init function: YGDL_init_plugin");
			});
			return std::nullopt;
		}

		auto lib = Library{lib_path, std::move(os_handle)};

		auto init_handle = YGDL_Plugin_init_handle{gen, &lib.owned_modules, out_error};
		init_function(&init_handle);
		return lib;
	}
} // namespace

struct YGDL_Generator {
	std::vector<std::string>                        loaded_paths;
	std::unordered_map<std::string, Library>        libraries;
	std::map<std::string, YGDL_Module, std::less<>> modules;
	std::vector<YGDL_Module*>                       module_list;
	std::vector<YGDL_Module*>                       last_added;
	bool                                            reloading = false;

	YGDL_Module* add(const YGDL_Module_info* info, YGDL_Error* out_error)
	{
		auto id_view = yggdrasill::detail::from_ygdl_string_view(info->id);

		return yggdrasill::catch_error(out_error, nullptr, [&]() -> YGDL_Module* {
			if(auto iter = modules.find(id_view); iter != modules.end()) {
				if(reloading) {
					iter->second.replace(info);
					last_added.emplace_back(&iter->second);

				} else {
					yggdrasill::set_error(out_error, YGDL_ERROR_CODE_INVALID_PLUGIN, [&] {
						return "A module with the name '" + std::string(id_view)
						       + "' already exists with different functions!";
					});
				}
				return &iter->second;

			} else {
				auto [module, inserted] = modules.emplace(id_view, info);
				assert(inserted && "Possible race condition in YGDL_Generator::add");

				last_added.emplace_back(&module->second);
				module_list.emplace_back(&module->second);
				return &module->second;
			}
		});
	}

	void load(const std::string& path, YGDL_Error* out_error)
	{
		auto load = [&](std::string lib_path) -> bool {
			if(libraries.contains(lib_path))
				return true;

			YGDL_error_reset(out_error);
			if(auto plugin = load_plugin(lib_path, this, out_error)) {
				libraries.emplace(std::move(lib_path), std::move(plugin.value()));
				return true;

			} else {
				return false;
			}
		};

		auto p = fs::path(path);

		if(load(path))
			return;

		if(p.is_relative() && load((fs::current_path() / p).string()))
			return;

		// not a valid library => is it a directory?
		if(fs::is_directory(p)) {
			auto any_loaded = false;
			for(auto& file : fs::directory_iterator(p)) {
				if(!fs::is_directory(file) && file.path().extension() == library_suffix) {
					if(load(file.path().string()))
						any_loaded = true;
					else if(file.path().is_relative())
						any_loaded |= load((fs::current_path() / file.path()).string());
				}
			}

			if(any_loaded)
				YGDL_error_reset(out_error); // ignore errors, if at least one plugin was loaded

		} else {
			// add platform dependent suffix and retry
			if(load(path + library_suffix))
				return;

			p = fs::current_path() / p;
			p.replace_extension(library_suffix);
			if(p.is_relative() && load(p.string()))
				return;
		}
	}
};

YGDL_Generator* YGDL_generator_create()
{
	return new YGDL_Generator();
}
void YGDL_generator_destroy(YGDL_Generator* gen)
{
	delete gen;
}

Module_list YGDL_generator_module_load(YGDL_Generator* gen, YGDL_String_view path_or_name, YGDL_Error* out_error)
{
	gen->last_added.clear();

	auto path_str = std::string(path_or_name.chars, path_or_name.length);
	gen->load(path_str, out_error);
	gen->loaded_paths.emplace_back(std::move(path_str));
	return YGDL_generator_module_last(gen);
}
void YGDL_plugin_module_add(YGDL_Plugin_init_handle* handle, const YGDL_Module_info* module)
{
	if(auto instance = handle->generator->add(module, handle->error)) {
		(*handle->modules).emplace_back(instance);
		std::cout << "[YGDL] successfully loaded module \""
		          << yggdrasill::detail::from_ygdl_string_view(module->id) << "\"\n";
	} else {
		handle->success = false;
		std::cerr << "[YGDL] failed to load module \""
		          << yggdrasill::detail::from_ygdl_string_view(module->id) << "\"";
		if(handle->error) {
			std::cerr << ": "
			          << yggdrasill::detail::from_ygdl_string_view(YGDL_string_get(&handle->error->message));
		}
		std::cerr << "\n";
	}
}

YGDL_Module* YGDL_generator_module_add(YGDL_Generator* gen, const YGDL_Module_info* module_info, YGDL_Error* out_error)
{
	gen->last_added.clear();
	return gen->add(module_info, out_error);
}

Module_list YGDL_generator_module_last(YGDL_Generator* gen)
{
	if(gen->last_added.empty()) {
		return {nullptr, 0};
	}

	return {gen->last_added.data(), static_cast<YGDL_Index>(gen->last_added.size())};
}

Module_list YGDL_generator_module_list(YGDL_Generator* gen)
{
	if(gen->module_list.empty()) {
		return {nullptr, 0};
	}

	return {gen->module_list.data(), static_cast<YGDL_Index>(gen->module_list.size())};
}

Module_list YGDL_generator_module_reload(YGDL_Generator* gen, YGDL_Error* out_error)
{
	gen->reloading = true;
	gen->last_added.clear();
	for(auto&& [path, lib] : gen->libraries) {
		if(fs::last_write_time(path) > lib.last_write) {
			// Unload modules loaded from the previous version (incl. destroying any created state)
			// but keep the objects around to
			//   (a) generate useful errors if they aren't replaced by the new library and
			//   (b) keep pointers to YGDL_Module used by external code alive/stable
			for(auto* module : lib.owned_modules) {
				module->unload();
			}

			// unload the old version
			lib.os_handle.reset();

			// load the new version
			auto reloaded = load_plugin(path, gen, out_error);
			if(!reloaded)
				break;

			lib = std::move(reloaded.value());
		}
	}

	// remove unloaded modules from the published module-list (should normally not happen, unless a module dll/so is broken or misbehaving)
	std::erase_if(gen->module_list, [](auto* module) { return !module->loaded; });

	gen->reloading = false;

	// also reload directories initially passed to YGDL_generator_module_load, to pick up new DLLs
	for(auto& path : gen->loaded_paths) {
		gen->load(path, nullptr);
	}

	return YGDL_generator_module_last(gen);
}
YGDL_Bool YGDL_generator_module_reload_available(YGDL_Generator* gen)
{
	for(auto&& [path, lib] : gen->libraries)
		if(fs::last_write_time(path) > lib.last_write)
			return YGDL_TRUE;

	return YGDL_FALSE;
}

YGDL_Module* YGDL_generator_module_get(YGDL_Generator* gen, YGDL_String_view id)
{
	if(auto iter = gen->modules.find(yggdrasill::detail::from_ygdl_string_view(id)); iter != gen->modules.end()) {
		return &iter->second;
	} else {
		return nullptr;
	}
}

void YGDL_generator_module_validate(const YGDL_Module* module, YGDL_Dict* dict, YGDL_Error* out_error)
{
	if(!module->loaded) {
		yggdrasill::set_error(out_error, YGDL_ERROR_CODE_INVALID_PLUGIN, [&] {
			return "The requested module '" + module->id + "' has already been unloaded. This is usually caused by an error while reloading the generator.";
		});
		return;
	}

	module->validate(module->userdata.get(), dict, out_error);
}
void YGDL_generator_module_execute(
        YGDL_Module* module, YGDL_World* world, const YGDL_Dict* dict, YGDL_Error* out_error)
{
	if(!module->loaded) {
		yggdrasill::set_error(out_error, YGDL_ERROR_CODE_INVALID_PLUGIN, [&] {
			return "The requested module '" + module->id + "' has already been unloaded. This is usually caused by an error while reloading the generator.";
		});
		return;
	}

	module->execute(module->userdata.get(), world, dict, out_error);
}
YGDL_String_view YGDL_generator_module_id(const YGDL_Module* module)
{
	return yggdrasill::detail::to_ygdl_string_view(module->id);
}
YGDL_Data_version YGDL_generator_module_version(const YGDL_Module* module)
{
	return static_cast<YGDL_Data_version>(module->version);
}
