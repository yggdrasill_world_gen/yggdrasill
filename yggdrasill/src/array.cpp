#include <yggdrasill/array.h>

#include <yggdrasill/dictionary.h>
#include <yggdrasill/commons.hpp>
#include <yggdrasill/dictionary.hpp>
#include <yggdrasill/error.hpp>
#include <yggdrasill/string.hpp>

#include "dictionary_impl.h"

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <memory>
#include <new>
#include <type_traits>
#include <utility>

using namespace yggdrasill;
using namespace yggdrasill::detail;

namespace {
	template <typename T, std::size_t Begin, std::size_t... I, typename F>
	void constexpr_for_impl(std::index_sequence<I...>, F&& f)
	{
		using RT = std::invoke_result_t<F, std::integral_constant<T, static_cast<T>(0)>>;
		if constexpr(std::is_same_v<bool, RT>)
			(f(std::integral_constant<T, static_cast<T>(I + Begin)>{}) || ...);
		else
			(f(std::integral_constant<T, static_cast<T>(I + Begin)>{}), ...);
	}

	template <auto Begin, auto End, typename F>
	void constexpr_for(F&& f)
	{
		using T = std::common_type_t<decltype(Begin), decltype(End)>;
		constexpr_for_impl<T, static_cast<std::size_t>(Begin)>(
		        std::make_index_sequence<static_cast<std::size_t>(End) - static_cast<std::size_t>(Begin) + 1u>{},
		        std::forward<F>(f));
	}
} // namespace

namespace yggdrasill::detail {

	Array_impl::Array_impl(YGDL_Type_t type) : YGDL_Array{type, 0, 0, nullptr} {}
	Array_impl::Array_impl(Array_impl&& rhs) noexcept : YGDL_Array{rhs}
	{
		rhs.size = rhs.capacity = 0;
		rhs.data                = nullptr;
	}
	Array_impl::Array_impl(const Array_impl& rhs) : YGDL_Array{rhs.type, rhs.size, rhs.size, nullptr}
	{
		if(rhs.data) {
			constexpr_for<YGDL_TYPE_BOOL, YGDL_TYPE_DICT>([&](auto i) {
				constexpr auto I = decltype(i)::value;
				using T          = id_to_type<I>;
				if(type == I) {
					data = new char[sizeof(T) * capacity];
					std::uninitialized_copy_n(cast_to<I>(rhs.data), size, cast_to<I>(data));
					return true;
				}
				return false;
			});
		}
	}
	Array_impl& Array_impl::operator=(Array_impl&& rhs) noexcept
	{
		free();
		YGDL_Array::operator=(static_cast<YGDL_Array&&>(rhs));
		rhs.size     = 0;
		rhs.capacity = 0;
		rhs.data     = nullptr;
		return *this;
	}
	Array_impl& Array_impl::operator=(const Array_impl& rhs)
	{
		*this = Array_impl(rhs);
		return *this;
	}
	Array_impl::~Array_impl()
	{
		free();
	}
	void Array_impl::free()
	{
		if(data) {
			assert(type != YGDL_TYPE_NONE && "Non-empty array of type none!");

			constexpr_for<YGDL_TYPE_BOOL, YGDL_TYPE_DICT>([&](auto i) {
				constexpr auto I = decltype(i)::value;
				if(type == I) {
					std::destroy_n(cast_to<I>(data), size);
					return true;
				}
				return false;
			});

			delete[] static_cast<char*>(data);
			data = nullptr;
		}
	}

} // namespace yggdrasill::detail


template <YGDL_Type I>
static void reserve_impl(YGDL_Array* array, YGDL_Index minimum_capacity)
{
	using T       = id_to_type<I>;
	auto old_data = cast_to<I>(array->data);
	auto new_data = new char[sizeof(T) * minimum_capacity];
	std::uninitialized_move_n(old_data, array->size, reinterpret_cast<T*>(new_data));
	std::destroy_n(old_data, array->size);
	delete[] static_cast<char*>(array->data);
	array->data     = new_data;
	array->capacity = minimum_capacity;
}

void YGDL_array_reserve(YGDL_Array* array, YGDL_Index minimum_capacity, YGDL_Type_t element_type)
{
	assert(array && "Invalid (NULL) YGDL_Array");

	if(array->capacity < minimum_capacity) {
		if(array->type == YGDL_TYPE_NONE)
			array->type = element_type;

		assert(array->type != YGDL_TYPE_NONE && "Can't reserve space for an array of type NONE");

		if(element_type != YGDL_TYPE_NONE)
			assert(element_type == array->type && "The passed type doesn't match this arrays element type.");


		constexpr_for<YGDL_TYPE_BOOL, YGDL_TYPE_DICT>([&](auto i) {
			constexpr auto I = decltype(i)::value;
			if(array->type == I) {
				reserve_impl<decltype(i)::value>(array, minimum_capacity);
				return true;
			}
			return false;
		});
	}
}
void YGDL_array_resize(YGDL_Array* array, YGDL_Index size, YGDL_Type_t element_type)
{
	if(array->type == YGDL_TYPE_NONE)
		array->type = element_type;

	assert(array->type != YGDL_TYPE_NONE && "Can't reserve space for an array of type NONE");

	if(element_type != YGDL_TYPE_NONE)
		assert(element_type == array->type && "The passed type doesn't match this arrays element type.");

	if(array->size > size) {
		constexpr_for<YGDL_TYPE_BOOL, YGDL_TYPE_DICT>([&](auto i) {
			constexpr auto I = decltype(i)::value;
			if(array->type == I) {
				auto data = cast_to<I>(array->data);
				std::destroy(data + size, data + array->size);
				array->size = size;
				return true;
			}
			return false;
		});

	} else if(array->size < size) {
		constexpr_for<YGDL_TYPE_BOOL, YGDL_TYPE_DICT>([&](auto i) {
			constexpr auto I = decltype(i)::value;
			if(array->type == I) {
				reserve_impl<I>(array, size);
				auto data = cast_to<I>(array->data);
				std::uninitialized_value_construct(data + array->size, data + size);
				array->size = size;
				return true;
			}
			return false;
		});
	}
}

template <YGDL_Type Type, typename... Args>
static auto* push_impl(YGDL_Array* array, YGDL_Error* error, Args&&... args)
{
	using T         = id_to_type<Type>;
	const auto size = array->size;

	assert(array && "Invalid (NULL) YGDL_Array");

	if(array->type == YGDL_TYPE_NONE) {
		array->type = Type;
	} else if(array->type != Type) {
		set_error(error, YGDL_ERROR_CODE_TYPE_MISMATCH, [&] { return "Type of array elements doesn't match"; });
		return static_cast<T*>(nullptr);
	}

	if(array->capacity < size + 1) {
		reserve_impl<Type>(array, std::max(array->capacity * 2, size + 1));
	}

	array->size++;
	return new(static_cast<T*>(array->data) + size) T(std::forward<Args>(args)...);
}

YGDL_Dict* YGDL_array_push_dict(YGDL_Array* array, YGDL_Error* error)
{
	return push_impl<YGDL_TYPE_DICT>(array, error);
}
YGDL_Array* YGDL_array_push_array(YGDL_Array* array, YGDL_Type_t value_type, YGDL_Error* error)
{
	return push_impl<YGDL_TYPE_ARRAY>(array, error, value_type);
}
YGDL_Bool* YGDL_array_push_bool(YGDL_Array* array, YGDL_Error* error)
{
	return push_impl<YGDL_TYPE_BOOL>(array, error, false);
}
int8_t* YGDL_array_push_int8(YGDL_Array* array, YGDL_Error* error)
{
	return push_impl<YGDL_TYPE_INT8>(array, error, int8_t(0));
}
int32_t* YGDL_array_push_int32(YGDL_Array* array, YGDL_Error* error)
{
	return push_impl<YGDL_TYPE_INT32>(array, error, 0);
}
int64_t* YGDL_array_push_int64(YGDL_Array* array, YGDL_Error* error)
{
	return push_impl<YGDL_TYPE_INT64>(array, error, 0);
}
float* YGDL_array_push_float(YGDL_Array* array, YGDL_Error* error)
{
	return push_impl<YGDL_TYPE_FLOAT>(array, error, 0.f);
}
double* YGDL_array_push_double(YGDL_Array* array, YGDL_Error* error)
{
	return push_impl<YGDL_TYPE_DOUBLE>(array, error, 0.f);
}
YGDL_Vec2* YGDL_array_push_vec2(YGDL_Array* array, YGDL_Error* error)
{
	return push_impl<YGDL_TYPE_VEC2>(array, error);
}
YGDL_Vec3* YGDL_array_push_vec3(YGDL_Array* array, YGDL_Error* error)
{
	return push_impl<YGDL_TYPE_VEC3>(array, error);
}
YGDL_String* YGDL_array_push_string(YGDL_Array* array, YGDL_Error* error)
{
	return reinterpret_cast<YGDL_String*>(push_impl<YGDL_TYPE_STRING>(array, error));
}

template <typename Eraser>
static void erase_impl(YGDL_Array* array, YGDL_Index index, YGDL_Error* error, Eraser&& erase)
{
	if(index < 0 || index >= array->size) {
		set_error(error, YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS, [&] {
			return "Index is out of array bounds in YGDL_array_erase_X";
		});
		return;
	}

	assert(array->type != YGDL_TYPE_NONE && "Can't erase elements from an array of type NONE");

	constexpr_for<YGDL_TYPE_BOOL, YGDL_TYPE_DICT>([&](auto i) {
		constexpr auto I = decltype(i)::value;
		if(array->type == I) {
			auto data = cast_to<I>(array->data);
			if(index < array->size - 1)
				erase(data);
			std::destroy_at(data + array->size - 1);
			array->size--;
			return true;
		}
		return false;
	});
}
void YGDL_array_erase_ordered(YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	erase_impl(array, index, error, [&](auto data) {
		std::move(data + index + 1, data + array->size, data + index);
	});
}
void YGDL_array_erase_unordered(YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	erase_impl(array, index, error, [&](auto data) { data[index] = std::move(data[array->size - 1]); });
}
void YGDL_array_clear(YGDL_Array* array)
{
	if(array->size == 0)
		return;

	assert(array->type != YGDL_TYPE_NONE && "Can't clear an array of type NONE");

	constexpr_for<YGDL_TYPE_BOOL, YGDL_TYPE_DICT>([&](auto i) {
		constexpr auto I = decltype(i)::value;
		if(array->type == I) {
			auto data = cast_to<I>(array->data);
			std::destroy_n(cast_to<I>(data), array->size);
			array->size = 0;
			return true;
		}
		return false;
	});
}


YGDL_Dict* YGDL_array_get_dict(YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return const_cast<YGDL_Dict*>(YGDL_array_get_const_dict(array, index, error));
}
YGDL_Array* YGDL_array_get_array(YGDL_Array* array, YGDL_Index index, YGDL_Type_t value_type, YGDL_Error* error)
{
	return const_cast<YGDL_Array*>(YGDL_array_get_const_array(array, index, value_type, error));
}
YGDL_Bool* YGDL_array_get_bool(YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return const_cast<YGDL_Bool*>(YGDL_array_get_const_bool(array, index, error));
}
int8_t* YGDL_array_get_int8(YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return const_cast<int8_t*>(YGDL_array_get_const_int8(array, index, error));
}
int32_t* YGDL_array_get_int32(YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return const_cast<int32_t*>(YGDL_array_get_const_int32(array, index, error));
}
int64_t* YGDL_array_get_int64(YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return const_cast<int64_t*>(YGDL_array_get_const_int64(array, index, error));
}
float* YGDL_array_get_float(YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return const_cast<float*>(YGDL_array_get_const_float(array, index, error));
}
double* YGDL_array_get_double(YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return const_cast<double*>(YGDL_array_get_const_double(array, index, error));
}
YGDL_Vec2* YGDL_array_get_vec2(YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return const_cast<YGDL_Vec2*>(YGDL_array_get_const_vec2(array, index, error));
}
YGDL_Vec3* YGDL_array_get_vec3(YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return const_cast<YGDL_Vec3*>(YGDL_array_get_const_vec3(array, index, error));
}
YGDL_String* YGDL_array_get_string(YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return const_cast<YGDL_String*>(YGDL_array_get_const_string(array, index, error));
}


template <YGDL_Type Type>
static const auto* get_impl(const YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	using T = id_to_type<Type>;

	assert(array && "Invalid (NULL) YGDL_Array");

	if(array->type != Type) {
		set_error(error, YGDL_ERROR_CODE_TYPE_MISMATCH, [&] { return "Type of array elements doesn't match"; });
		return static_cast<T*>(nullptr);

	} else if(index < 0 || index >= array->size) {
		set_error(error, YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS, [&] {
			return "Index is out of array bounds in YGDL_array_get_X";
		});
		return static_cast<T*>(nullptr);
	}

	return std::launder(static_cast<const T*>(array->data) + index);
}

const YGDL_Dict* YGDL_array_get_const_dict(const YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return get_impl<YGDL_TYPE_DICT>(array, index, error);
}
const YGDL_Array* YGDL_array_get_const_array(
        const YGDL_Array* array, YGDL_Index index, YGDL_Type_t value_type, YGDL_Error* error)
{
	auto result = get_impl<YGDL_TYPE_ARRAY>(array, index, error);
	if(result && value_type != YGDL_TYPE_NONE && result->type != value_type) {
		set_error(error, YGDL_ERROR_CODE_TYPE_MISMATCH, [&] { return "Type of array elements doesn't match"; });
		return nullptr;
	}
	return result;
}
const YGDL_Bool* YGDL_array_get_const_bool(const YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return get_impl<YGDL_TYPE_BOOL>(array, index, error);
}
const int8_t* YGDL_array_get_const_int8(const YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return get_impl<YGDL_TYPE_INT8>(array, index, error);
}
const int32_t* YGDL_array_get_const_int32(const YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return get_impl<YGDL_TYPE_INT32>(array, index, error);
}
const int64_t* YGDL_array_get_const_int64(const YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return get_impl<YGDL_TYPE_INT64>(array, index, error);
}
const float* YGDL_array_get_const_float(const YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return get_impl<YGDL_TYPE_FLOAT>(array, index, error);
}
const double* YGDL_array_get_const_double(const YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return get_impl<YGDL_TYPE_DOUBLE>(array, index, error);
}
const YGDL_Vec2* YGDL_array_get_const_vec2(const YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return get_impl<YGDL_TYPE_VEC2>(array, index, error);
}
const YGDL_Vec3* YGDL_array_get_const_vec3(const YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return get_impl<YGDL_TYPE_VEC3>(array, index, error);
}
const YGDL_String* YGDL_array_get_const_string(const YGDL_Array* array, YGDL_Index index, YGDL_Error* error)
{
	return reinterpret_cast<const YGDL_String*>(get_impl<YGDL_TYPE_STRING>(array, index, error));
}
