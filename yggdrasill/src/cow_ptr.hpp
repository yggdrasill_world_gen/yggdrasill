#pragma once

#include <yggdrasill/error.hpp>

#include <atomic>
#include <memory>
#include <variant>
#include <vector>

namespace yggdrasill::detail {

	struct Noop {
		bool operator()() const { return true; }
	};

	template <typename F>
	class [[nodiscard]] Finally {
		F    f_;
		bool called_ = false;

	  public:
		template <typename Func>
		Finally(Func&& func) : f_(std::forward<Func>(func))
		{
		}
		~Finally() noexcept(false) { call_early(); }

		void call_early()
		{
			if(!called_) {
				called_ = true;
				f_();
			}
		}

		Finally(const Finally&)            = delete;
		Finally(Finally&&)                 = delete;
		Finally& operator=(const Finally&) = delete;
		Finally& operator=(Finally&&)      = delete;
	};

	template <typename F>
	auto finally(F&& f) -> Finally<std::remove_reference_t<F>>
	{
		return {std::forward<F>(f)};
	}


	/// Implements copy-on-write semantics for a heap object of type T
	/// The stored object is copied on each call to get_mutable(), if the Cow_ptr is not the unique owner.
	/// The behaviour is undefined if multiple threads access the same Cow_ptr object concurrently without external synchronization.
	template <typename T>
	class Cow_ptr {
	  private:
		struct Data {
			T                          value;
			std::atomic<int>           readers{0};
			std::atomic<int>           writers{0};
			std::atomic<std::uint32_t> version = 1;

			template <typename... Ts>
			Data(Ts&&... args) : value(std::forward<Ts>(args)...)
			{
			}
		};
		Cow_ptr(std::shared_ptr<Data> ptr) : ptr_(std::move(ptr))
		{
			ptr_->readers++;
			if(ptr_->writers > 0) {
				YGDL_warning_log(
				        "Copying a Cow_ptr with an active writer. Creating a deep-copy, but this might "
				        "indicate "
				        "a race-condition.");
				copy();
			}
		}

	  public:
		class Weak {
		  public:
			std::optional<Cow_ptr<T>> lock()
			{
				if(auto owning_ptr = ptr_.lock())
					return Cow_ptr<T>{std::move(owning_ptr)};
				else
					return std::nullopt;
			}

			bool expired() const { return ptr_.expired(); }

		  private:
			friend class Cow_ptr<T>;
			std::weak_ptr<Data> ptr_;

			explicit Weak(std::weak_ptr<Data> ptr) : ptr_(ptr) {}
		};

		Cow_ptr() : Cow_ptr(std::make_shared<Data>()) {}
		template <typename... Ts>
		Cow_ptr(std::in_place_t, Ts&&... args) : Cow_ptr(std::make_shared<Data>(std::forward<Ts>(args)...))
		{
		}
		Cow_ptr(Cow_ptr&& rhs) noexcept : ptr_(std::move(rhs.ptr_)), mutable_refcount_(rhs.mutable_refcount_)
		{
			rhs.ptr_.reset();
			rhs.mutable_refcount_ = 0;
		}
		Cow_ptr(const Cow_ptr& rhs) : Cow_ptr(rhs.ptr_) {}
		~Cow_ptr()
		{
			if(mutable_refcount_ > 0) {
				YGDL_warning_log("Destroying a Cow_ptr with an active writer. Releasing the lock now.");
				mutable_refcount_ = 1;
				release_mutable();
			}
			if(ptr_) {
				ptr_->readers--;
			}
		}
		Cow_ptr& operator=(Cow_ptr&& rhs) noexcept
		{
			if(ptr_ == rhs.ptr_)
				return *this;

			ptr_ = std::move(rhs.ptr_);
			rhs.ptr_.reset();
			mutable_refcount_ = std::exchange(rhs.mutable_refcount_, 0);

			return *this;
		}
		Cow_ptr& operator=(const Cow_ptr& rhs) noexcept
		{
			assert(mutable_refcount_ == 0);
			if(ptr_ == rhs.ptr_)
				return *this;

			if(ptr_) {
				ptr_->readers--;
			}
			ptr_ = rhs.ptr_;
			ptr_->readers++;
			if(ptr_->writers > 0) {
				YGDL_warning_log(
				        "Copying a Cow_ptr with an active writer. Creating a deep-copy, but this might "
				        "indicate a race-condition.");
				copy();
			}

			return *this;
		}

		/// \returns A weak reference to the stored value, that may later be used to reconstruct an owning Cow_ptr
		/// \attention Once this method is called, the reader-count is incremented and can never become zero again.
		/// So, once this method was called, acquiring a mutable reference will always copy!
		Weak acquire_weak_ptr() const
		{
			ptr_->readers++; // prohibit new mutable references
			return Weak{ptr_};
		}

		/// Lifetime: The returned reference is invalidated if any non-const member of Cow_ptr is called
		auto get_const() const -> const T& { return ptr_->value; }

		/// Integer, incremented on each release_mutable operation
		auto version() const { return ptr_ ? ptr_->version.load() : 0; }

		/// Copies the underlying object, if required, and returns a reference to it.
		/// Lifetime: The returned reference is invalidated if the assignment-operator of Cow_ptr is called
		[[nodiscard]] auto acquire_mutable(YGDL_Error* out_error) -> T*
		{
			if(mutable_refcount_ > 0) {
				mutable_refcount_++;
				return &ptr_->value;
			}

			if(ptr_->writers > 0) {
				set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] {
					return "There is already an existing lock for the requested resource.";
				});
				return nullptr;
			}

			if(ptr_->readers > 1) {
				copy();
				ptr_->writers = 1;

			} else if(auto writers = 0; !ptr_->writers.compare_exchange_strong(writers, 1)) {
				set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] {
					return "Possible race-condition trying to acquire the requested lock";
				});
				return nullptr;
			}

			mutable_refcount_++;
			return &ptr_->value;
		}
		bool release_mutable()
		{
			static std::atomic<std::uint32_t> next_version{2};

			if(mutable_refcount_ <= 0) {
				YGDL_warning_log(
				        "Copy-On-Write pointer was tried to be released through a non-owning instance");
				return false;
			}

			ptr_->version = next_version++;

			if(--mutable_refcount_ == 0)
				ptr_->writers--;

			return true;
		}

	  private:
		std::shared_ptr<Data> ptr_;
		int                   mutable_refcount_ = 0;

		void copy()
		{
			assert(mutable_refcount_ == 0);
			auto np     = std::make_shared<Data>(ptr_->value);
			np->version = ptr_->version.load();
			np->readers++;
			ptr_->readers--;
			ptr_ = np;
		}

		template <typename TT, typename F, typename PreExecute>
		friend bool modify(std::vector<Cow_ptr<TT>*>&, YGDL_Error*, F&&, PreExecute&&);
	};


	template <typename T, typename F, typename PreExecute = Noop>
	bool modify(std::vector<Cow_ptr<T>*>& cow_ptrs, YGDL_Error* out_error, F&& func, PreExecute&& pre_exec = {})
	{
		auto init_iter  = cow_ptrs.begin();
		auto successful = true;

		auto cleanup = finally([&] {
			// release all aquired pointers in reverse order
			for(auto release_iter = std::reverse_iterator(init_iter); release_iter != cow_ptrs.rend();
			    release_iter++) {
				auto r = (*release_iter)->release_mutable();
				if(!r) {
					YGDL_warning_log(
					        "One of the release_mutable calls in modify(std::vector<Cow_ptr<T>*>&, ...) "
					        "failed. This is likely to be a programming error in Yggdrasill that should be "
					        "reported.");
					assert(!"If this assert triggers, this indicates a bug in Yggdrasill.");
				}
			}
		});

		// acquire all pointers
		for(; init_iter != cow_ptrs.end(); ++init_iter) {
			if(auto ptr = (*init_iter)->acquire_mutable(out_error); !ptr) {
				// we drop the returned ptr here, because we can retrieve it from get_const later
				successful = false;
				return false;
			}
		}

		if(!pre_exec()) {
			return false;
		}

		// execute callback for each pointer
		for(auto ptr : cow_ptrs) {
			func(ptr->ptr_->value);
		}

		return successful;
	}

	/// \returns a copy of the given vector. The new vectors capacity is identical to the original ones
	template <typename... Ts>
	std::vector<Ts...> copy_with_capacity(const std::vector<Ts...>& in)
	{
		auto r = std::vector<Ts...>{};
		r.reserve(in.capacity());
		r.insert(r.end(), in.begin(), in.end());
		return r;
	}
	template <typename... Ts>
	auto copy_with_capacity(const std::variant<Ts...>& in)
	{
		return std::visit([](const auto& v) -> std::variant<Ts...> { return copy_with_capacity(v); }, in);
	}

} // namespace yggdrasill::detail
