#pragma once

#include <yggdrasill/world.h>

#include <yggdrasill/dictionary.hpp>
#include <yggdrasill/error.hpp>

#include "cow_ptr.hpp"
#include "dictionary_impl.h"
#include "layer_impl.hpp"
#include "mesh_impl.hpp"
#include "random_impl.hpp"

#include <string>
#include <unordered_map>
#include <utility>
#include <vector>


struct YGDL_World {
	yggdrasill::detail::Layer_container                                                 layers;
	yggdrasill::detail::Cow_ptr<yggdrasill::detail::Mesh_impl>                          mesh;
	YGDL_Random                                                                         random;
	yggdrasill::detail::String_map<yggdrasill::detail::Cow_ptr<yggdrasill::Dictionary>> unstructured_layers;
	std::unordered_map<YGDL_Dict*, std::string> locked_unstructured_layers;
	bool                                        valid = true;
};
