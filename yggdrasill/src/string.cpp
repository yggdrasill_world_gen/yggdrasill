#include <yggdrasill/string.h>

#include <algorithm>
#include <cstring>

namespace {
	constexpr uint8_t size_left_mask = uint32_t(1) | uint8_t(128);

#ifdef YGDL_BIG_ENDIAN
	constexpr auto capacity_mask = uint32_t(1);
#else
	constexpr auto capacity_mask = ~((~uint32_t(0)) >> 1);
#endif

	constexpr uint32_t to_sso_capacity(int32_t in)
	{
#ifndef YGDL_BIG_ENDIAN
		in += (in & 1); // make even
#endif
		return static_cast<uint32_t>(in) | capacity_mask;
	}
	constexpr YGDL_Index from_sso_capacity(uint32_t in)
	{
		return static_cast<int32_t>(in & (~capacity_mask));
	}

	constexpr char to_size_left(int32_t length)
	{
		return static_cast<char>((YGDL_SMALL_STRING_SIZE - length) << 1) & ~size_left_mask;
	}
	constexpr YGDL_Index from_size_left(char size_left)
	{
		return YGDL_SMALL_STRING_SIZE - (size_left >> 1);
	}

	bool is_small_string(const YGDL_String& str)
	{
		uint8_t size_left = 0;
		std::memcpy(&size_left, reinterpret_cast<const char*>(&str.data_) + YGDL_SMALL_STRING_SIZE, sizeof(uint8_t));
		return (size_left & size_left_mask) == 0;
	}

	void allocate(YGDL_String_data_large_t& data, YGDL_Index len)
	{
		const auto cap = to_sso_capacity(len + 1);
		data.capacity  = cap;
		data.chars     = new char[from_sso_capacity(cap)];
	}
} // namespace

YGDL_String YGDL_string_create(YGDL_String_view content)
{
	const auto len = std::max(YGDL_Index(0), content.length);

	auto str = YGDL_String{};

	if(len > YGDL_SMALL_STRING_SIZE) {
		allocate(str.data_.large, len);
		str.data_.large.length = len;
		if(len > 0)
			std::memcpy(str.data_.large.chars, content.chars, len);
		str.data_.large.chars[len] = '\0';

	} else {
		str.data_.small.chars[YGDL_SMALL_STRING_SIZE] = to_size_left(len);
		if(len > 0)
			std::memcpy(str.data_.small.chars, content.chars, len);
		if(len < YGDL_SMALL_STRING_SIZE)
			str.data_.small.chars[len] = '\0';
	}

	return str;
}
YGDL_String YGDL_string_copy(const YGDL_String* str)
{
	return YGDL_string_create(YGDL_string_get(str));
}
void YGDL_string_destroy(YGDL_String* str)
{
	if(!str)
		return;

	if(!is_small_string(*str)) {
		delete[] str->data_.large.chars;
		// clear data structure
		*str = YGDL_string_create_empty();
	}
}
YGDL_String_view YGDL_string_get(const YGDL_String* str)
{
	if(!str)
		return YGDL_String_view{"", 0};

	return YGDL_String_view{YGDL_string_const_data(str), YGDL_string_size(str)};
}
YGDL_Index YGDL_string_size(const YGDL_String* str)
{
	if(!str)
		return 0;

	if(is_small_string(*str)) {
		return from_size_left(str->data_.small.chars[YGDL_SMALL_STRING_SIZE]);
	} else {
		return str->data_.large.length;
	}
}
YGDL_Index YGDL_string_capacity(const YGDL_String* str)
{
	if(!str)
		return 0;

	return is_small_string(*str) ? YGDL_SMALL_STRING_SIZE : from_sso_capacity(str->data_.large.capacity) - 1;
}
const char* YGDL_string_const_data(const YGDL_String* str)
{
	if(!str)
		return "";

	if(is_small_string(*str)) {
		return str->data_.small.chars;
	} else {
		return str->data_.large.chars;
	}
}
char* YGDL_string_data(YGDL_String* str)
{
	if(!str)
		return nullptr;

	if(is_small_string(*str)) {
		return str->data_.small.chars;
	} else {
		return str->data_.large.chars;
	}
}
void YGDL_string_reserve(YGDL_String* str, YGDL_Index minimum_capacity)
{
	if(!str)
		return;

	if(is_small_string(*str)) {
		if(minimum_capacity <= YGDL_SMALL_STRING_SIZE)
			return;

		auto new_data = YGDL_String_data_large_t{};
		allocate(new_data, minimum_capacity);
		new_data.length = from_size_left(str->data_.small.chars[YGDL_SMALL_STRING_SIZE]);
		std::memcpy(new_data.chars, str->data_.small.chars, new_data.length + 1);
		str->data_.large = new_data;

	} else if(auto capacity = from_sso_capacity(str->data_.large.capacity); capacity < minimum_capacity) {
		auto old_data = str->data_.large.chars;
		allocate(str->data_.large, minimum_capacity);
		std::memcpy(str->data_.large.chars, old_data, str->data_.large.length + 1);
		delete[] old_data;
	}
}
void YGDL_string_resize(YGDL_String* str, YGDL_Index size, char c)
{
	if(!str)
		return;

	YGDL_string_reserve(str, size);
	const auto new_len = std::max(YGDL_Index(0), size);

	if(is_small_string(*str)) {
		const auto old_len = from_size_left(str->data_.small.chars[YGDL_SMALL_STRING_SIZE]);
		if(old_len < new_len) {
			std::memset(str->data_.small.chars + old_len, c, new_len - old_len + 1);
		}
		str->data_.small.chars[YGDL_SMALL_STRING_SIZE] = to_size_left(new_len);

	} else {
		const auto old_len = str->data_.large.length;
		if(old_len < new_len) {
			std::memset(str->data_.large.chars + old_len, c, new_len - old_len + 1);
		}

		str->data_.large.length = new_len;
	}
}
void YGDL_string_set(YGDL_String* str, YGDL_String_view content)
{
	if(!str)
		return;

	const auto len = std::max(int32_t(0), content.length);

	if(is_small_string(*str)) {
		if(len <= YGDL_SMALL_STRING_SIZE) {
			std::memcpy(str->data_.small.chars, content.chars, len);
			str->data_.small.chars[YGDL_SMALL_STRING_SIZE] = to_size_left(len);
			if(len < YGDL_SMALL_STRING_SIZE)
				str->data_.small.chars[len] = '\0';
			return;
		} else {
			allocate(str->data_.large, len + 1);
		}

	} else if(auto capacity = from_sso_capacity(str->data_.large.capacity); len > capacity) {
		delete[] str->data_.large.chars;
		allocate(str->data_.large, len + 1);
	}

	if(len > 0)
		std::memcpy(str->data_.large.chars, content.chars, len);

	str->data_.large.chars[len] = '\0';
	str->data_.large.length     = len;
}
void YGDL_string_append(YGDL_String* str, YGDL_String_view additional_content)
{
	if(!str)
		return;

	const auto initial_len    = YGDL_string_size(str);
	const auto additional_len = std::max(int32_t(0), additional_content.length);
	const auto len            = initial_len + additional_len;
	YGDL_string_reserve(str, len);

	if(is_small_string(*str)) {
		if(additional_len > 0)
			std::memcpy(str->data_.small.chars + initial_len, additional_content.chars, additional_len);
		if(len < YGDL_SMALL_STRING_SIZE)
			str->data_.small.chars[len] = '\0';
		str->data_.small.chars[YGDL_SMALL_STRING_SIZE] = to_size_left(initial_len + additional_len);
	} else {
		if(additional_len > 0)
			std::memcpy(str->data_.large.chars + initial_len, additional_content.chars, additional_len);
		str->data_.large.length     = initial_len + additional_len;
		str->data_.large.chars[len] = '\0';
	}
}
