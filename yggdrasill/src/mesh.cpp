#include <yggdrasill/mesh.h>

#include <yggdrasill/world.h>
#include <yggdrasill/error.hpp>
#include <yggdrasill/mesh.hpp>

#include "cow_ptr.hpp" // for yggdrasill::detail::copy_with_capacity
#include "layer_impl.hpp"
#include "mesh_impl.hpp"

#include <array>
#include <cassert>
#include <cstdlib>
#include <functional>
#include <limits>
#include <span>
#include <utility>
#include <variant>
#include <vector>


YGDL_Bool YGDL_mesh_vertex_add(YGDL_Mesh* mesh, YGDL_Index count, YGDL_Vertex* out, YGDL_Error* out_error)
{
	return static_cast<yggdrasill::detail::Mesh_impl*>(mesh)->add_vertices(count, out, out_error);
}
YGDL_Bool YGDL_mesh_vertex_remove(YGDL_Mesh* mesh, YGDL_Vertex v)
{
	return static_cast<yggdrasill::detail::Mesh_impl*>(mesh)->remove_vertex(v);
}
YGDL_Face YGDL_mesh_face_add(YGDL_Mesh* mesh, YGDL_Vertex v_a, YGDL_Vertex v_b, YGDL_Vertex v_c, YGDL_Error* out_error)
{
	return static_cast<yggdrasill::detail::Mesh_impl*>(mesh)->add_face(v_a, v_b, v_c, out_error);
}

YGDL_Edge YGDL_mesh_edge_split(YGDL_Mesh* mesh, YGDL_Edge e, float split_point, YGDL_Error* out_error)
{
	return static_cast<yggdrasill::detail::Mesh_impl*>(mesh)->split(e, split_point, out_error);
}
YGDL_Vertex YGDL_mesh_edge_collapse(YGDL_Mesh* mesh, YGDL_Edge e, float join_point, YGDL_Error* out_error)
{
	return static_cast<yggdrasill::detail::Mesh_impl*>(mesh)->collapse(e, join_point, out_error);
}
YGDL_Bool YGDL_mesh_edge_flip(YGDL_Mesh* mesh, YGDL_Edge e, YGDL_Error* out_error)
{
	return static_cast<yggdrasill::detail::Mesh_impl*>(mesh)->flip(e, out_error);
}

namespace yggdrasill::detail {

	template <typename T>
	inline bool empty_state_element(T e)
	{
		return e == T{};
	}
	template <typename T>
	constexpr bool empty_state_element(Valid<T>)
	{
		return false;
	}

	bool Mesh_impl::validation(YGDL_Error* out_error) const
	{
		const auto mesh = Const_mesh_view(*this);

		auto on_error = [&](auto&&... msg) {
			auto ss = std::ostringstream{};
			(ss << ... << msg);
			YGDL_error_set(out_error,
			               YGDL_ERROR_CODE_MESH_CORRUPTION,
			               detail::to_ygdl_string_view(std::move(ss).str()));
			return false;
		};

		// unused edges are not part or any edge-rings and are not referenced by any vertices or faces
		for(index_t i = 0; i <= mesh.edge_max_index(); i++) {
			if(const auto e = Primal_edge{i}; !e.valid(mesh)) {
				auto found = std::find(primal_edge_next, primal_edge_next + edge_count, e);
				if(found != primal_edge_next + edge_count) {
					const auto other = Primal_edge{static_cast<index_t>(found - primal_edge_next)};
					return on_error(e, ", that is marked as unused, is still referenced by ", other);
				}

				found = std::find(vertex_edge, vertex_edge + vertex_count, e);
				if(found != vertex_edge + vertex_count) {
					const auto other = Vertex{static_cast<index_t>(found - vertex_edge)};
					return on_error(e, ", that is marked as unused, is still referenced by ", other);
				}

				found = std::find(face_edge, face_edge + face_count, e);
				if(found != face_edge + face_count) {
					const auto other = Face{static_cast<index_t>(found - face_edge)};
					return on_error(e, ", that is marked as unused, is still referenced by ", other);
				}
			}

			if(const auto e = Dual_edge{i}; !e.valid(mesh)) {
				auto found = std::find(dual_edge_next, dual_edge_next + edge_count, e);
				if(found != dual_edge_next + edge_count) {
					const auto other = Dual_edge{static_cast<index_t>(found - face_edge)};
					return on_error(e, ", that is marked as unused, is still referenced by ", other);
				}
			}
		}

		for(auto f : mesh.faces()) {
			const auto e = f.edge(mesh);
			if(!e.valid(mesh))
				return on_error(f, "doesn't have an associated edge");
			if(e.left(mesh) != f)
				return on_error(f, ".edge().left() (==", e, ") doesn't equal itself");
		}

		for(auto v : mesh.vertices()) {
			if(auto e = v.edge(mesh); e != no_edge && e.origin(mesh) != v)
				return on_error(v, ".edge().origin() (==", e, ") doesn't equal itself");
		}


		// clang-format off
		auto validate_edge = [&](const auto e) -> bool {
			if(!e.valid(mesh))
				return on_error(e, " is invalid");

			if(e.origin_next(mesh) == e)
				return on_error(e, " is not part of any face (edge-ring of size 1)");

			auto count = index_t(0);
			for(auto n : e.origin_ccw(mesh)) {
				if(n == e.sym())
					return on_error("The edge-ring of ", e, " contains its own e.sym()");
				if(count>mesh.edge_used_count())
					return on_error("The CCW edge-ring iteration of ", e, " doesn't terminate");
			}
			count = index_t(0);
			for(auto n : e.sym().origin_ccw(mesh)) {
				(void) n;
				if(count>mesh.edge_used_count())
					return on_error("The CCW edge-ring iteration of ", e.sym(), " doesn't terminate");
			}

			// check if there are multiple connections between the same vertices
			if(!empty_state_element(e.origin(mesh)) && !empty_state_element(e.dest(mesh))) {
				const auto origin_ccw = e.origin_ccw(mesh);
				const auto dst_ccw    = e.sym().origin_ccw(mesh);
				const auto same       = std::find_if(dst_ccw.begin(), dst_ccw.end_iterator(), [&](auto oe) {
                    return std::count(origin_ccw.begin(), origin_ccw.end_iterator(), oe.sym()) > 0 && oe.sym() != e;
                });
				if(same != dst_ccw.end_iterator())
					return on_error("Multiple edges between ", e.origin(mesh), " and ", e.dest(mesh), ": ", e, ", ", *same);
			}

			if(e.origin(mesh) != e.origin_next(mesh).origin(mesh)) {
				return on_error("Inconsistent vertices in edge-ring of edge ", e, ": ", e.origin(mesh),
				                " and ", e.origin_next(mesh).origin(mesh));
			}
			if(e.origin(mesh) != e.origin_prev(mesh).origin(mesh)) {
				return on_error("Inconsistent vertices in edge-ring of edge ", e, ": ", e.origin(mesh),
				                " and ", e.origin_prev(mesh).origin(mesh));
			}

			if(const auto oe = e.origin_next(mesh).origin_prev(mesh); e != oe)
				return on_error("Inconsistent edge-ring at ", e, "origin_next().origin_prev() doesn't equal itself but ", oe);

			if(const auto oe = e.dest_next(mesh).dest_prev(mesh); e != oe)
				return on_error("Inconsistent edge-ring at ", e, "dest_next().dest_prev() doesn't equal itself but ", oe);

			if(const auto oe = e.left_next(mesh).left_prev(mesh); e != oe)
				return on_error("Inconsistent edge-ring at ", e, "left_next().left_prev() doesn't equal itself but ", oe);

			if(const auto oe = e.right_next(mesh).right_prev(mesh); e != oe)
				return on_error("Inconsistent edge-ring at ", e, "right_next().right_prev() doesn't equal itself but ", oe);


			if(e.type() == yggdrasill::Edge_type::primal) {
				if(const auto f = e.left_next(mesh).left(mesh); e.left(mesh) != f)
					return on_error("Inconsistent edge-ring at ", e, "left_next().left() doesn't equal e.left() (==",
					                e.left(mesh), ") but ", f);

				if(const auto f = e.right_next(mesh).right(mesh); e.right(mesh) != f)
					return on_error("Inconsistent edge-ring at ", e, "right_next().right() doesn't equal e.right() (==",
					                e.right(mesh), ") but ", f);

				if((!empty_state_element(e.left(mesh)) && !empty_state_element(e.right(mesh))) && e.origin_next(mesh) == e.origin_prev(mesh))
					return on_error(e.origin(mesh), " has ", e, " with two faces (", e.left(mesh), ", ",  e.right(mesh),
					                " but only one other edge in its edge-ring, ", e.origin_next(mesh));
			}
			return true;
		};
		// clang-format on

		int count = 0;
		for(auto e : mesh.directed_edges<yggdrasill::Edge_type::primal>()) {
			count++;
			if(!validate_edge(e))
				return false;
		}
		if(count != mesh.edge_used_count()) {
			return on_error(
			        "Mesh::edge_used_count() doesn't match the number of edges returned by "
			        "Mesh::directed_edges<Edge_type::primal>()");
		}

		count = 0;
		for(auto e : mesh.directed_edges<yggdrasill::Edge_type::dual>()) {
			count++;
			if(!validate_edge(e))
				return false;
		}
		if(count != mesh.edge_used_count()) {
			return on_error(
			        "Mesh::edge_used_count() doesn't match the number of edges returned by "
			        "Mesh::directed_edges<Edge_type::dual>()");
		}

		count = 0;
		for(auto e : mesh.undirected_edges<yggdrasill::Edge_type::primal>()) {
			(void) e;
			count++;
		}
		if(count * 2 != mesh.edge_used_count()) {
			return on_error(
			        "Mesh::edge_used_count() doesn't match the number of edges returned by "
			        "Mesh::undirected_edges<Edge_type::primal>()");
		}

		return true;
	}

	Mesh_impl::Mesh_impl()
	  : YGDL_Mesh{0, 0, 0, 0, 0, 0, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr}
	{
		update_all_data_pointers();
	}
	Mesh_impl::Mesh_impl(const Mesh_impl& rhs)
	  : YGDL_Mesh(rhs)
	  , vertex_edges_(yggdrasill::detail::copy_with_capacity(rhs.vertex_edges_))
	  , face_edges_(yggdrasill::detail::copy_with_capacity(rhs.face_edges_))
	  , primal_edge_origin_(yggdrasill::detail::copy_with_capacity(rhs.primal_edge_origin_))
	  , primal_edge_next_(yggdrasill::detail::copy_with_capacity(rhs.primal_edge_next_))
	  , dual_edge_origin_(yggdrasill::detail::copy_with_capacity(rhs.dual_edge_origin_))
	  , dual_edge_next_(yggdrasill::detail::copy_with_capacity(rhs.dual_edge_next_))
	  , free_edges_(rhs.free_edges_)
	  , free_faces_(rhs.free_faces_)
	  , free_vertices_(rhs.free_vertices_)
	{
		update_all_data_pointers();
	}
	Mesh_impl& Mesh_impl::operator=(const Mesh_impl& rhs)
	{
		if(this == &rhs)
			return *this;

		assert(!layers_);
		YGDL_Mesh::operator=(rhs);
		update_all_data_pointers();
		return *this;
	}


	bool Mesh_impl::add_vertices(YGDL_Index count, YGDL_Vertex* out, YGDL_Error* out_error)
	{
		assert(layers_);
		assert(count > 0);

		auto from_free_list = YGDL_Index(0);

		// try free list first
		if(free_vertices_.size() > 1) {
			from_free_list = std::min(static_cast<YGDL_Index>(free_vertices_.size() - 1), count);
		}

		// try to reserve the rest
		if(count > from_free_list) {
			const auto new_count = static_cast<YGDL_Index>(vertex_edges_.size() + count - from_free_list);
			if(!layers_->resize_vertex_layers(new_count, out_error)) {
				return false;
			}

			vertex_edges_.resize(new_count);
			vertex_count = static_cast<YGDL_Index>(vertex_edges_.size());
			vertex_edge  = vertex_edges_.data();
		}

		if(from_free_list > 0) {
			// write vertex ids to out
			if(out) {
				for(YGDL_Index i = 1; i <= from_free_list; i++) {
					*(out++) = free_vertices_[free_vertices_.size() - i];
				}
			}
			// remove them from free list
			free_vertices_.resize(free_vertices_.size() - from_free_list);
		}

		if(count > from_free_list) {
			// write vertex ids to out
			if(out) {
				for(auto i = vertex_count - count + from_free_list; i < vertex_count; i++) {
					*(out++) = Vertex(i);
				}
			}
		}

		unused_vertex_count = static_cast<YGDL_Index>(free_vertices_.size() - 1);
		return true;
	}

	bool Mesh_impl::remove_vertex(YGDL_Vertex v)
	{
		if(static_cast<index_t>(vertex_edges_.size()) <= v.index || vertex_edges_[v.index] != no_edge)
			return false;

		mark_free(v);
		return true;
	}

	bool Mesh_impl::create_faces(YGDL_Index count, Face* out, YGDL_Error* out_error)
	{
		assert(layers_);
		assert(count > 0);

		auto from_free_list = 0;

		// try free list first
		if(free_faces_.size() > 1) {
			from_free_list = std::min(static_cast<YGDL_Index>(free_faces_.size() - 1), count);
		}

		// try to reserve the rest
		if(count > from_free_list) {
			const auto new_count = static_cast<YGDL_Index>(face_edges_.size() + count - from_free_list);
			if(!layers_->resize_face_layers(new_count, out_error)) {
				return false;
			}

			face_edges_.resize(new_count);
			face_count = static_cast<YGDL_Index>(face_edges_.size());
			face_edge  = face_edges_.data();
		}

		if(from_free_list > 0) {
			// write face ids to out
			if(out) {
				for(YGDL_Index i = 1; i <= from_free_list; i++) {
					*(out++) = free_faces_[free_faces_.size() - i];
				}
			}
			// remove them from free list
			free_faces_.resize(free_faces_.size() - from_free_list);
		}

		if(count > from_free_list) {
			// write vertex ids to out
			if(out) {
				for(auto i = face_count - count + from_free_list; i < face_count; i++) {
					*(out++) = Face(i);
				}
			}
		}

		unused_face_count = static_cast<YGDL_Index>(free_faces_.size() - 1);
		return true;
	}


	auto Mesh_impl::add_quad_edge(YGDL_Vertex origin, YGDL_Vertex dest, YGDL_Face left, YGDL_Face right) -> Primal_edge
	{
		Edge edge_p_0, edge_p_1, edge_d_0, edge_d_1;

		if(free_edges_.size() > 1) {
			edge_p_0 = free_edges_.back();
			edge_p_1 = edge_p_0;
			edge_p_1.mask += 1;
			edge_d_0 = Edge(Edge_type::dual, edge_p_0.mask);
			edge_d_1 = Edge(Edge_type::dual, edge_p_1.mask);

			free_edges_.pop_back();

			assert(primal_edge_origin_[edge_p_0.mask] == no_vertex);
			assert(primal_edge_origin_[edge_p_1.mask] == no_vertex);
			assert(dual_edge_origin_[edge_p_0.mask] == no_face);
			assert(dual_edge_origin_[edge_p_1.mask] == no_face);

		} else {
			const auto new_size = primal_edge_origin_.size() + 2;
			assert(new_size < std::numeric_limits<YGDL_Index>::max());

			edge_p_0 = Edge(Edge_type::primal, static_cast<uint32_t>(primal_edge_next_.size()));
			edge_p_1 = Edge(Edge_type::primal, static_cast<uint32_t>(primal_edge_next_.size() + 1));

			edge_d_0 = Edge(Edge_type::dual, static_cast<uint32_t>(dual_edge_next_.size()));
			edge_d_1 = Edge(Edge_type::dual, static_cast<uint32_t>(dual_edge_next_.size() + 1));

			primal_edge_origin_.resize(new_size);
			primal_edge_next_.resize(new_size);
			dual_edge_origin_.resize(new_size);
			dual_edge_next_.resize(new_size);
		}

		// add 2 primal edges
		primal_edge_origin_[edge_p_0.mask] = origin;
		primal_edge_origin_[edge_p_1.mask] = dest;
		primal_edge_next_[edge_p_0.mask]   = edge_p_0;
		primal_edge_next_[edge_p_1.mask]   = edge_p_1;

		// add 2 dual edges
		dual_edge_origin_[edge_p_0.mask] = right;
		dual_edge_origin_[edge_p_1.mask] = left;
		dual_edge_next_[edge_p_0.mask]   = edge_d_0;
		dual_edge_next_[edge_p_1.mask]   = edge_d_1;

		if(vertex_edges_[origin.index] == no_edge) {
			vertex_edges_[origin.index] = edge_p_0;
		}
		if(vertex_edges_[dest.index] == no_edge) {
			vertex_edges_[dest.index] = edge_p_1;
		}

		return edge_p_0;
	}

	namespace {
		template <typename std::size_t Size, typename F, std::size_t... Is>
		constexpr auto make_array_impl(F&& f, std::index_sequence<Is...>)
		{
			// <std::invoke_result_t<F, std::size_t>, Size>
			return std::array{f(Is)...};
		}

		template <typename std::size_t Size, typename F>
		constexpr auto make_array(F&& f)
		{
			return make_array_impl<Size>(std::forward<F>(f), std::make_index_sequence<Size>());
		}

		template <typename T, std::size_t Size, typename F>
		constexpr auto map(const std::array<T, Size>& array, F&& f)
		{
			return make_array<Size>([&](auto i) {
				if constexpr(std::is_invocable_v<F, T>)
					return std::invoke(f, array[i]);
				else
					return std::apply(f, array[i]);
			});
		}

	} // namespace

	/// Code to handle the special case, that a new face shares a vertex with another face without being connected through edges.
	/// Extracted into a separate class because it affects multiple of the normal cases and requires two steps.
	/// First it needs to analyze the current structure to extract the existing edges around the vertex and
	/// later (after all other modifications) it uses these to patch the modified structure.
	class Mesh_impl::Lonely_edge_case_handler {
	  public:
		Lonely_edge_case_handler(Mesh_view                                       mesh,
		                         const std::array<std::pair<Vertex, Vertex>, 3>& edge_vertices,
		                         const std::array<Primal_edge, 3>&               edges,
		                         YGDL_Error*                                     out_error)
		{
			for(auto i = 0u; i < 3u; i++) {
				auto  vertex    = edge_vertices[i].first;
				auto& edge      = edges[i];
				auto& prev_edge = edges[(i + 2) % 3];

				if(edge == no_edge && prev_edge == no_edge) {
					if(const auto vert_edge = vertex.edge(mesh); vert_edge != no_edge) {
						auto range = vert_edge.origin_next(mesh).origin_ccw(mesh);

						auto right_most_edge_iter =
						        std::find_if(range.begin(), range.end_iterator(), [&](auto e) {
							        return e.right(mesh) == no_face;
						        });

						if(right_most_edge_iter == range.end()) {
							valid_ = false;
							yggdrasill::set_error(out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] {
								return "Corrupted Mesh: Single vertex connection, but no edge is an external "
								       "edge";
							});
							return;
						}

						auto right_most_edge = *right_most_edge_iter;

						++right_most_edge_iter;
						auto other = std::find_if(right_most_edge_iter, range.end_iterator(), [&](auto e) {
							return e.right(mesh) == no_face;
						});
						if(other != range.end()) {
							valid_ = false;
							yggdrasill::set_error(out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] {
								return "Corrupted Mesh: Multiple unconnected faces sharing a single vertex";
							});
							return;
						}

						data_[count_].new_edge         = &edge;
						data_[count_].prev_new_edge    = &prev_edge;
						data_[count_].next_preexisting = right_most_edge;
						data_[count_].prev_preexisting = right_most_edge.origin_prev(mesh);
						count_++;
					}
				}
			}
		}

		auto is_valid() const { return valid_; }

		void operator()(Mesh_impl& mesh)
		{
			for(auto i = 0; i < count_; i++) {
				auto& e = data_[i];

				mesh.next_dual(e.new_edge->rot())        = e.prev_preexisting.inv_rot();
				mesh.next_dual(e.next_preexisting.rot()) = e.prev_new_edge->rot();

				mesh.next_primal(e.prev_preexisting)     = *e.new_edge;
				mesh.next_primal(*e.new_edge)            = e.prev_new_edge->sym();
				mesh.next_primal(e.prev_new_edge->sym()) = e.next_preexisting;
			}
		}

	  private:
		struct Data {
			const Primal_edge* new_edge;      //< problematic new edge
			const Primal_edge* prev_new_edge; //< previous (next clock-wise) edge in the new face
			Primal_edge next_preexisting; //< next edge in CCW order from the new_edge, that was already there
			Primal_edge prev_preexisting; //< previous (next clock-wise) edge from next_preexisting
		};
		std::array<Data, 3> data_;
		int                 count_ = 0;
		bool                valid_ = true;
	};

	Primal_edge& Mesh_impl::next_primal(Primal_edge e)
	{
		return primal_edge_next_[e.mask];
	}
	Dual_edge& Mesh_impl::next_dual(Dual_edge e)
	{
		return dual_edge_next_[e.index()];
	}

	YGDL_Face Mesh_impl::add_face(Vertex v_a, Vertex v_b, Vertex v_c, YGDL_Error* out_error)
	{
		assert(layers_);

		auto       mesh = Mesh_view(*this);
		const auto face =
		        free_faces_.size() <= 1 ? Face{static_cast<index_t>(face_edges_.size())} : free_faces_.back();

		const auto edge_vertices = std::array{std::pair(v_a, v_b), std::pair(v_b, v_c), std::pair(v_c, v_a)};
		auto       edges = map(edge_vertices, [&](auto from, auto to) { return mesh.find_edge(from, to); });
		const auto is_edge_new     = map(edges, [](auto e) { return e == no_edge; });
		const auto new_edges_count = is_edge_new[0] + is_edge_new[1] + is_edge_new[2];

		for(auto i = 0u; i < 3u; i++) {
			if(edges[i] == no_edge)
				continue;

			const auto f = dual_edge_origin_[edges[i].inv_rot().index()];
			if(f != no_face) {
				yggdrasill::set_error(out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] {
					return concat("The ", i, "th requested edge (", edges[i], ") is already part of face ", f);
				});
				return no_face;
			}
		}

		if(new_edges_count == 0) {
			// clang-format off
			if(auto left = edges[0].left(mesh); left!=no_face && left == edges[1].left(mesh) && left == edges[2].left(mesh)) {
				yggdrasill::set_error(out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] {
					return concat("The face that should be created [",v_a,", ",v_b,", ",v_c,"] already exists as ", left);
				});
				return no_face;
			}
			if(auto right=edges[0].right(mesh); right!=no_face && right == edges[1].left(mesh) && right == edges[2].left(mesh)) {
				yggdrasill::set_error(out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] {
					return concat("The face that should be created [",v_a,", ",v_b,", ",v_c,"] already exists as ", right);
				});
				return no_face;
			}
			// clang-format on
		}

		// validate winding order
		for(auto e : edges) {
			if(e != no_edge) {
				// check that the face-loop of the neighboring triangle is valid, which would be violated for inconsistent winding orders
				if(auto left = e.sym().left(mesh); left == no_face || left != e.sym().left_next(mesh).left(mesh)) {
					yggdrasill::set_error(out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] {
						return "The new faces winding order is inconsistent with its neighbors";
					});
					return no_face;
				}
			}
		}

		// fetch data about original mesh to handle edge-cases later
		auto lonely_edge_handler = Lonely_edge_case_handler(mesh, edge_vertices, edges, out_error);
		if(!lonely_edge_handler.is_valid())
			return no_face;

		// resize edge and face layers
		if(!layers_->resize_edge_layers(static_cast<YGDL_Index>(primal_edge_origin_.size() + new_edges_count),
		                                out_error)) {
			return no_face;
		}
		if(!layers_->resize_face_layers(static_cast<YGDL_Index>(face_edges_.size() + 1), out_error)) {
			return no_face;
		}

		// create missing edges and the new face
		edges = make_array<3>([&](auto i) {
			return is_edge_new[i] ? add_quad_edge(edge_vertices[i].first, edge_vertices[i].second, face)
			                      : edges[i];
		});

		if(free_faces_.size() <= 1) {
			face_edges_.emplace_back(edges[0]);
		} else {
			face_edges_[face.index()] = edges[0];
			free_faces_.pop_back();
		}
		update_all_data_pointers();

		// identify and handle the concrete case we are facing based on which edges are new
		switch(new_edges_count) {
			case 0: break;
			case 1: {
				const auto  new_edge_index = is_edge_new[0] ? 0 : (is_edge_new[1] ? 1 : 2);
				const auto& existing_1     = edges[(new_edge_index + 2) % 3];
				const auto& new_edge       = edges[new_edge_index];
				const auto& existing_2     = edges[(new_edge_index + 1) % 3];

				const auto next_edge = next_dual(existing_1.inv_rot());

				next_dual(primal_edge_next_[existing_2.mask].rot()) = new_edge.rot();
				next_dual(new_edge.rot())                           = next_edge;

				std::swap(next_primal(new_edge.sym()), next_primal(existing_2));
				std::swap(next_primal(new_edge), next_primal(next_edge.rot()));
				break;
			}

			case 2: {
				const auto  existing_edge_index = !is_edge_new[0] ? 0 : (!is_edge_new[1] ? 1 : 2);
				const auto& new_1               = edges[(existing_edge_index + 2) % 3];
				const auto& existing            = edges[existing_edge_index];
				const auto& new_2               = edges[(existing_edge_index + 1) % 3];

				const auto existing_sym_prev = existing.sym().origin_prev(mesh);

				next_dual(existing.inv_rot().origin_prev(mesh)) = new_1.rot();
				next_dual(new_1.rot())                          = new_2.rot();
				next_dual(new_2.rot())                          = existing.inv_rot().origin_next(mesh);

				std::swap(next_primal(existing), next_primal(new_1.sym()));
				std::swap(next_primal(new_2), next_primal(existing_sym_prev));
				std::swap(next_primal(new_1), next_primal(new_2.sym()));
				break;
			}

			case 3:
				for(auto i = 0u; i < 3u; i++) {
					auto edge = edges[i];
					auto prev = edges[(i + 2) % 3];

					next_dual(edge.rot()) = prev.rot();
					std::swap(next_primal(edge), next_primal(prev.sym()));
				}
				break;

			default:
				std::cerr << "Invariant violation in " __FILE__ << " at " << __LINE__ << ", aborting.\n";
				std::abort();
		}

		// connect outgoing dual edges into face-loop
		for(auto i = 0u; i < 3u; i++) {
			const auto index       = edges[i].inv_rot().index();
			dual_edge_next_[index] = edges[(i + 1) % 3].inv_rot();
			assert(dual_edge_origin_[index] == face || dual_edge_origin_[index] == no_face);
			dual_edge_origin_[index] = face;
		}

		// handle edge-case
		lonely_edge_handler(*this);

		return face;
	}

	bool Mesh_impl::flip(const Edge<> e_in, YGDL_Error* out_error)
	{
		assert(layers_);

		auto mesh = Mesh_view(*this);

		// flipping any part of a quad edge is the same as flipping any other, so if we should flip a dual-edge, we flip its rot() instead
		const auto e = e_in.base();

		const auto left_face  = e.left(mesh);
		const auto right_face = e.right(mesh);

		if(left_face == no_face || right_face == no_face) {
			yggdrasill::set_error(
			        out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] { return "Can't flip a boundary-edge"; });
			return false;
		}

		const auto e_prev     = e.origin_prev(mesh);
		const auto e_next     = e.origin_next(mesh);
		const auto sym_e_prev = e.sym().origin_prev(mesh);
		const auto sym_e_next = e.sym().origin_next(mesh);
		const auto origin     = e.origin(mesh);
		const auto dest       = e.dest(mesh);

		const auto invalid_flip = e_prev.dest(mesh)
		                                  .neighbors(mesh, Neighbor_type::just_primal_edges)
		                                  .find_if([vv = e_next.dest(mesh)](auto& v) { return v == vv; })
		                                  .has_value();
		if(invalid_flip) {
			yggdrasill::set_error(out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] {
				return concat("Can't flip ",
				              e_in,
				              " as it would cause two edges to connect ",
				              e_prev.dest(mesh),
				              " and ",
				              e_next.dest(mesh));
			});
			return false;
		}

		const auto dual_e = e.rot();
		// works, because dual edge-loops consist of exactly three edges
		const auto dual_e_next     = next_dual(dual_e);
		const auto dual_e_prev     = next_dual(dual_e_next);
		const auto sym_dual_e_next = next_dual(dual_e.sym());
		const auto sym_dual_e_prev = next_dual(sym_dual_e_next);

		if(!layers_->on_mesh_change(
		           std::span<YGDL_Index>{},
		           std::array{left_face.index(), right_face.index()},
		           std::array{e.index(), e.sym().index()},
		           std::array{dual_e.index(), dual_e_prev.index(), sym_dual_e_next.index(), sym_dual_e_prev.index()},
		           std::span<YGDL_Layer_element_interpolation>{},
		           std::span<YGDL_Layer_element_interpolation>{},
		           std::span<YGDL_Layer_element_interpolation>{},
		           std::span<YGDL_Layer_element_interpolation>{},
		           0.f,
		           out_error)) {
			return false;
		}

		// flip primal edges
		// remove edge from old edge-loop and add it into new edge-loop
		next_primal(e_prev) = e_next;
		next_primal(e)      = std::exchange(next_primal(sym_e_next.sym()), e);

		// remove sym-edge from old edge-loop and add it into new edge-loop
		next_primal(sym_e_prev) = sym_e_next;
		next_primal(e.sym())    = std::exchange(next_primal(e_next.sym()), e.sym());

		// update origin
		primal_edge_origin_[e.index()]       = primal_edge_origin_[sym_e_next.sym().index()];
		primal_edge_origin_[e.sym().index()] = primal_edge_origin_[e_next.sym().index()];

		// flip dual edges
		// update loop with dual-edge
		next_dual(sym_dual_e_next) = dual_e;
		next_dual(dual_e)          = dual_e_prev;
		next_dual(dual_e_prev)     = sym_dual_e_next;

		// update loop with sym-dual-edge
		next_dual(dual_e_next)     = dual_e.sym();
		next_dual(dual_e.sym())    = sym_dual_e_prev;
		next_dual(sym_dual_e_prev) = dual_e_next;

		// update faces
		dual_edge_origin_[dual_e_next.index()] = left_face;
		assert(dual_edge_origin_[dual_e.sym().index()] == left_face);
		assert(dual_edge_origin_[sym_dual_e_prev.index()] == left_face);

		dual_edge_origin_[sym_dual_e_next.index()] = right_face;
		assert(dual_edge_origin_[dual_e.index()] == right_face);
		assert(dual_edge_origin_[dual_e_prev.index()] == right_face);

		// update references from vertices to edges
		if(vertex_edges_[origin.index()] == e) {
			vertex_edges_[origin.index()] = e_next;
		}
		if(vertex_edges_[dest.index()] == e.sym()) {
			vertex_edges_[dest.index()] = sym_e_next;
		}
		face_edges_[left_face.index()]  = e;
		face_edges_[right_face.index()] = e.sym();

		return true;
	}

	namespace {
		template <typename T, YGDL_Index SmallSize>
		class Temporary_storage {
		  public:
			T& push_back(T v) { return (*this)[size_] = v; }

			T& operator[](YGDL_Index index)
			{
				return std::visit(overloaded{[&](std::array<T, SmallSize>& array) -> T& {
					                             if(index < SmallSize) {
						                             if(index >= size_)
							                             size_ = index + 1;
						                             return array[index];
					                             } else {
						                             auto& vec = storage_.template emplace<std::vector<T>>(
						                                     std::vector<T>(array.begin(), array.end()));
						                             vec.resize(index + 1);
						                             size_ = index + 1;
						                             return vec[index];
					                             }
				                             },
				                             [&](std::vector<T>& vec) -> T& {
					                             if(index >= static_cast<index_t>(vec.size())) {
						                             vec.resize(index + 1);
						                             size_ = index + 1;
					                             }
					                             return vec[index];
				                             }},
				                  storage_);
			}

			std::span<T> data()
			{
				return std::visit([&](auto& c) -> std::span<T> { return c; }, storage_).subspan(0, size_);
			}

		  private:
			std::variant<std::array<T, SmallSize>, std::vector<T>> storage_;
			index_t                                                size_ = 0;
		};

		template <typename Value, std::size_t N>
		class Edge_changes {
		  public:
			Value& operator[](Edge<> e) { return (*this)[e.index()]; }
			Value& operator[](YGDL_Index index)
			{
				return storage_.push_back(std::make_pair(index, Value{})).second;
			}

			void apply(std::vector<Value>& out)
			{
				for(auto&& [index, val] : storage_.data()) {
					out[index] = val;
				}
			}

		  private:
			Temporary_storage<std::pair<YGDL_Index, Value>, N> storage_;
		};
	} // namespace

	Primal_edge Mesh_impl::split(Primal_edge e, float split_point, YGDL_Error* out_error)
	{
		assert(layers_);

		auto mesh = Mesh_view(*this);

		const auto src            = e.origin(mesh);
		const auto dest           = e.dest(mesh); // Vertex that will be deleted
		const auto left           = e.left(mesh);
		const auto right          = e.right(mesh);
		const auto new_face_count = (left != no_face) + (right != no_face);

		if(src == no_vertex) {
			yggdrasill::set_error(out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] {
				return "Invalid edge as input into split()";
			});
			return no_edge;
		}

		if(left == no_face && right == no_face) {
			yggdrasill::set_error(out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] {
				return "Corrupted mesh in split(). Edge without faces";
			});
			return no_edge;
		}

		const auto left_vertex  = e.origin_next(mesh).dest(mesh);
		const auto right_vertex = e.origin_prev(mesh).dest(mesh);

		// resize layers
		if(!layers_->resize_edge_layers(
		           static_cast<YGDL_Index>(primal_edge_origin_.size() + (new_face_count + 1) * 2), out_error)) {
			return no_edge;
		}

		auto new_vertex = Vertex(0);
		if(!add_vertices(1, &new_vertex, out_error)) {
			return no_edge;
		}


		auto new_left  = no_face;
		auto new_right = no_face;
		if(std::array<Face, 2> new_faces; !create_faces(new_face_count, new_faces.data(), out_error)) {
			return no_edge;
		} else {
			if(left != no_face)
				new_left = new_faces[0];
			if(right != no_face)
				new_right = new_faces[left != no_face ? 1 : 0];
		}

		std::array<YGDL_Layer_element_interpolation, 3> edge_interpolations;
		std::size_t                                     edge_interpolation_count = 0;

		std::array<YGDL_Index, 2> changed_dual_edges;
		std::size_t               changed_dual_edge_count = 0;

		auto primal_next = Edge_changes<Primal_edge, 10>{};
		auto dual_next   = Edge_changes<Dual_edge, 10>{};


		const auto central_edge = add_quad_edge(new_vertex, dest, new_left, new_right);
		const auto left_new_edge =
		        left != no_face ? add_quad_edge(new_vertex, left_vertex, left, new_left) : Primal_edge{};
		const auto right_new_edge =
		        right != no_face ? add_quad_edge(new_vertex, right_vertex, new_right, right) : Primal_edge{};
		update_all_data_pointers();

		auto enqueue_edge = [&, prev_edge = central_edge](Primal_edge edge) mutable {
			primal_next[prev_edge] = edge;
			prev_edge              = edge;
		};
		edge_interpolations[edge_interpolation_count++] =
		        YGDL_Layer_element_interpolation{central_edge.index(), e.index(), e.index()};

		// replace e.sym() with central_edge.sym()
		primal_next[e.sym().origin_prev(mesh)] = central_edge.sym();
		primal_next[central_edge.sym()]        = e.sym().origin_next(mesh);

		if(left != no_face) {
			face_edges_[left.index()] = e;

			enqueue_edge(left_new_edge);

			face_edges_[new_left.index()] = central_edge;

			// connect primal edges
			auto prev                        = e.origin_next(mesh).sym();
			primal_next[prev]                = left_new_edge.sym();
			primal_next[left_new_edge.sym()] = prev.origin_next(mesh);

			// connect bottom dual edges
			dual_next[e.inv_rot()]             = left_new_edge.inv_rot();
			dual_next[left_new_edge.inv_rot()] = prev.inv_rot();

			// connect top dual edges
			auto dual_prev                 = e.inv_rot().origin_next(mesh);
			dual_next[dual_prev]           = left_new_edge.rot();
			dual_next[left_new_edge.rot()] = central_edge.inv_rot();

			dual_edge_origin_[dual_prev.index()]          = new_left;
			changed_dual_edges[changed_dual_edge_count++] = dual_prev.index();

			edge_interpolations[edge_interpolation_count++] = YGDL_Layer_element_interpolation{
			        left_new_edge.index(), prev.sym().index(), dual_prev.inv_rot().index()};

		} else {
			dual_next[e.inv_rot()] = central_edge.inv_rot();
		}

		enqueue_edge(e.sym());

		if(right != no_face) {
			face_edges_[right.index()] = e.sym();

			enqueue_edge(right_new_edge);

			face_edges_[new_right.index()] = central_edge.sym();

			// connect primal edges
			auto prev                         = e.sym().origin_next(mesh).sym();
			primal_next[prev]                 = right_new_edge.sym();
			primal_next[right_new_edge.sym()] = prev.origin_next(mesh);

			// connect bottom dual edges
			dual_next[e.rot().origin_next(mesh)] = right_new_edge.rot();
			dual_next[right_new_edge.rot()]      = e.rot();

			// connect top dual edges
			auto dual_prev                      = e.sym().origin_next(mesh).rot();
			dual_next[central_edge.rot()]       = right_new_edge.inv_rot();
			dual_next[right_new_edge.inv_rot()] = dual_prev;

			dual_edge_origin_[dual_prev.index()]          = new_right;
			changed_dual_edges[changed_dual_edge_count++] = dual_prev.index();

			edge_interpolations[edge_interpolation_count++] = YGDL_Layer_element_interpolation{
			        right_new_edge.index(), prev.sym().index(), e.origin_prev(mesh).index()};

		} else {
			dual_next[central_edge.rot()] = e.rot();
		}

		enqueue_edge(central_edge);

		dual_next[central_edge.inv_rot()]    = e.inv_rot().origin_next(mesh);
		dual_next[e.rot().origin_prev(mesh)] = central_edge.rot();

		primal_edge_origin_[e.sym().mask]                 = new_vertex;
		primal_edge_next_[e.sym().origin_prev(mesh).mask] = e.sym().origin_next(mesh);

		primal_next.apply(primal_edge_next_);
		dual_next.apply(dual_edge_next_);

		if(vertex_edges_[dest.index()] == e.sym()) {
			vertex_edges_[dest.index()] = central_edge.sym();
		}


		// interpolate new vertices/edges/faces in layers
		std::array<YGDL_Layer_element_interpolation, 2> face_interpolations;
		std::size_t                                     face_interpolation_count = 0;
		if(left != no_face) {
			face_interpolations[face_interpolation_count++] =
			        YGDL_Layer_element_interpolation{new_left.index(), left.index(), left.index()};
		}
		if(right != no_face) {
			face_interpolations[face_interpolation_count++] =
			        YGDL_Layer_element_interpolation{new_right.index(), right.index(), right.index()};
		}

		if(!layers_->on_mesh_change(
		           std::span<YGDL_Index>{},
		           std::span<YGDL_Index>{},
		           std::array{e.index()},
		           std::span{changed_dual_edges.data(), changed_dual_edge_count},
		           std::array{YGDL_Layer_element_interpolation{new_vertex.index(), src.index(), dest.index()}},
		           std::span{edge_interpolations.data(), edge_interpolation_count},
		           std::span{edge_interpolations.data(), edge_interpolation_count},
		           std::span{face_interpolations.data(), face_interpolation_count},
		           split_point,
		           out_error)) {
			return no_edge;
		}

		return central_edge;
	}

	Vertex Mesh_impl::collapse(Primal_edge e, float join_point, YGDL_Error* out_error)
	{
		assert(layers_);
		assert(!YGDL_mesh_edge_is_dual(e));

		auto mesh = Mesh_view(*this);

		auto src  = e.origin(mesh);
		auto dest = e.dest(mesh); // Vertex that will be deleted

		auto left  = e.left(mesh);
		auto right = e.right(mesh);

		if(src == no_vertex)
			return no_vertex; // edge already deleted

		// check link-condition: one-ring(a) intersected with one-ring(b) == one-ring(ab)
		const auto origin_edges = e.origin_ccw(mesh).skip(left != no_face ? 2 : 1);
		const auto dest_edges   = e.sym().origin_ccw(mesh).skip(right != no_face ? 2 : 1);
		const auto iter         = std::find_first_of(
                origin_edges.begin(),
                origin_edges.end_iterator(),
                dest_edges.begin(),
                dest_edges.end_iterator(),
                [&](Primal_edge oe, Primal_edge de) { return oe.dest(mesh) == de.dest(mesh); });
		if(iter != origin_edges.end()) {
			yggdrasill::set_error(out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] {
				return "The link-condition is not satisfied for the edge passed to collapse()";
			});
			return no_vertex;
		}

		// record changes and apply them later, because we can't modify them during the traversal
		auto primal_next = Edge_changes<Primal_edge, 32>{};
		auto dual_next   = Edge_changes<Dual_edge, 4>{};
		auto dual_origin = Edge_changes<Face, 2>{};

		auto removed_faces       = Temporary_storage<YGDL_Index, 2>{};
		auto modified_edges      = Temporary_storage<YGDL_Index, 32>{};
		auto modified_dual_edges = Temporary_storage<YGDL_Index, 32>{};

		modified_edges.push_back(e.mask);

		auto remove_edge = [&](Primal_edge edge) {
			primal_next[edge.origin_prev(mesh)] = edge.origin_next(mesh);

			modified_edges.push_back(edge.mask);
			modified_dual_edges.push_back(edge.rot().index());
		};
		auto replace_dual_edge = [&](Dual_edge target, Dual_edge replacement) {
			assert(replacement.origin_next(mesh) != no_edge);
			assert(replacement.origin_next(mesh).origin_next(mesh) != no_edge);
			assert(target.origin_next(mesh) != no_edge);

			dual_next[target.origin_prev(mesh)] = replacement;
			dual_next[replacement]              = target.origin_next(mesh);
			dual_origin[replacement]            = dual_edge_origin_[target.index()];

			modified_dual_edges.push_back(replacement.index());
		};

		// check if the operation is allowed
		if(left != no_face && right != no_face) {
			auto has_right_boundary = [&](const auto& edge_range) {
				return std::any_of(edge_range.begin(), edge_range.end_iterator(), [&](Primal_edge e) {
					return e.right(mesh) == no_face;
				});
			};

			if(has_right_boundary(e.origin_ccw(mesh)) && has_right_boundary(e.sym().origin_ccw(mesh))) {
				yggdrasill::set_error(out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] {
					return "Requested edge collapse would cause a corrupted mesh (multiple unconnected faces "
					       "on the same vertex) or a dangling vertex";
				});
				return no_vertex;
			}

		} else if(left == no_face && right == no_face) {
			yggdrasill::set_error(out_error, YGDL_ERROR_CODE_MESH_CORRUPTION, [&] {
				return "Corrupted mesh. The edge that should be collapsed is not part of a triangle";
			});
			return no_vertex;
		}

		auto dest_prev = Primal_edge{};
		auto dest_next = Primal_edge{};

		auto begin_dest = e.sym();
		auto end_dest   = e.sym();

		if(left != no_face) {
			dest_prev = e.dest_prev(mesh);
			remove_edge(dest_prev);

			end_dest = dest_prev.sym();

			auto transplanted_dual_edge_right = e.origin_next(mesh).rot();
			replace_dual_edge(dest_prev.inv_rot(), transplanted_dual_edge_right);

			removed_faces.push_back(left.index());

		} else {
			assert(e.dest_prev(mesh).rot().origin_next(mesh) != no_edge);
			dual_next[e.inv_rot().origin_prev(mesh)] = e.dest_prev(mesh).rot();
		}

		if(right != no_face) {
			dest_next = e.dest_next(mesh);
			remove_edge(dest_next);

			begin_dest = dest_next.sym();

			auto transplanted_dual_edge_left = e.origin_prev(mesh).inv_rot();

			if(left != no_face && dest_next == dest_prev.dest_prev(mesh)) {
				// special-case: dest_prev and dest_next belong to the same face
				dual_next[e.origin_next(mesh).rot()]     = transplanted_dual_edge_left;
				dual_next[transplanted_dual_edge_left]   = dest_next.rot().origin_next(mesh);
				dual_origin[transplanted_dual_edge_left] = dual_edge_origin_[dest_prev.inv_rot().index()];
				modified_dual_edges.push_back(transplanted_dual_edge_left.index());

			} else {
				replace_dual_edge(dest_next.rot(), transplanted_dual_edge_left);
			}

			removed_faces.push_back(right.index());

		} else {
			assert(e.origin_prev(mesh).rot().origin_next(mesh) != no_edge);
			dual_next[e.rot().origin_prev(mesh)] = e.origin_prev(mesh).inv_rot();
		}

		// remove edges
		remove_edge(e);

		// move dest edges to src
		auto prev = e.origin_prev(mesh);
		if(auto first = begin_dest.origin_next(mesh); dest_prev == no_edge || first != dest_prev.sym()) {
			for(auto edge : first.origin_ccw(mesh)) {
				if(edge == end_dest)
					break;

				primal_next[prev] = edge;
				prev              = edge;
			}
		}
		primal_next[prev] = e.origin_next(mesh);

		for(auto edge : e.sym().origin_ccw(mesh)) {
			modified_edges.push_back(edge.index());
		}
		for(auto edge : e.origin_ccw(mesh)) {
			modified_edges.push_back(edge.index());
		}

		if(!layers_->on_mesh_change(
		           std::array{dest.index()},
		           removed_faces.data(),
		           modified_edges.data(),
		           modified_dual_edges.data(),
		           std::array{YGDL_Layer_element_interpolation{src.index(), src.index(), dest.index()}},
		           std::span<YGDL_Layer_element_interpolation>{},
		           std::span<YGDL_Layer_element_interpolation>{},
		           std::span<YGDL_Layer_element_interpolation>{},
		           join_point,
		           out_error)) {
			return no_vertex;
		}

		// set origin of all dest-edges to src
		for(auto edge : e.sym().origin_ccw(mesh)) {
			primal_edge_origin_[edge.index()] = src;
		}

		// fix face_edges if it references one of the edges we will remove
		if(dest_prev != no_edge) {
			if(auto face = dest_prev.left(mesh); face != no_face) {
				if(auto& fe = face_edges_[face.index()]; fe == dest_prev) {
					fe = dest_prev.left_next(mesh);
				}
			}
			if(auto vertex = dest_prev.origin(mesh); vertex != no_vertex) {
				if(auto& ve = vertex_edges_[vertex.index()]; ve == dest_prev) {
					ve = dest_prev.origin_next(mesh);
				}
			}
		}
		if(dest_next != no_edge) {
			if(auto face = dest_next.right(mesh); face != no_face) {
				if(auto& fe = face_edges_[face.index()]; fe == dest_next.sym()) {
					fe = dest_next.sym().left_next(mesh);
				}
			}
			if(auto vertex = dest_next.origin(mesh); vertex != no_vertex) {
				if(auto& ve = vertex_edges_[vertex.index()]; ve == dest_next) {
					ve = dest_next.origin_next(mesh);
				}
			}
		}

		// fix vertex_edges if it references one of the edges we will remove
		if(vertex_edges_[src.index()] == e) {
			vertex_edges_[src.index()] = prev;
		}

		primal_next.apply(primal_edge_next_);
		dual_next.apply(dual_edge_next_);
		dual_origin.apply(dual_edge_origin_);

		[&](auto... e) { (mark_free(e), ...); }(e, dest_prev, dest_next, left, right, dest);

		assert(mesh.edge_used_count() == edge_count - static_cast<YGDL_Index>((free_edges_.size() - 1) * 2));

		return src;
	}

	void Mesh_impl::mark_free(Edge<> e_in)
	{
		if(e_in == no_edge)
			return;

		const auto e = e_in.base(); // remove bitflags for dual-edges and sym-edges

		primal_edge_origin_[e.index()] = no_vertex;
		primal_edge_next_[e.index()]   = no_edge;

		primal_edge_origin_[e.sym().index()] = no_vertex;
		primal_edge_next_[e.sym().index()]   = no_edge;

		dual_edge_origin_[e.rot().index()] = no_face;
		dual_edge_next_[e.rot().index()]   = no_edge;

		dual_edge_origin_[e.inv_rot().index()] = no_face;
		dual_edge_next_[e.inv_rot().index()]   = no_edge;

		const auto free_iter =
		        std::lower_bound(free_edges_.begin(), free_edges_.end(), e.decay(), [](auto lhs, auto rhs) {
			        return lhs.mask > rhs.mask;
		        });
		assert(free_iter == free_edges_.end() || *free_iter != e);
		free_edges_.insert(free_iter, e);
		unused_edge_count = static_cast<YGDL_Index>(free_edges_.size() - 1);
		unused_edge_list  = free_edges_.empty() ? nullptr : free_edges_.data() + 1;
	}
	void Mesh_impl::mark_free(Vertex v)
	{
		if(v == no_vertex)
			return;

		vertex_edges_[v.index()] = no_edge;
		const auto free_iter =
		        std::lower_bound(free_vertices_.begin(), free_vertices_.end(), v, [](auto lhs, auto rhs) {
			        return static_cast<std::uint32_t>(lhs.index()) > static_cast<std::uint32_t>(rhs.index());
		        });
		assert(free_iter == free_vertices_.end() || *free_iter != v);
		free_vertices_.insert(free_iter, v);
		unused_vertex_count = static_cast<YGDL_Index>(free_vertices_.size() - 1);
		unused_vertex_list  = free_vertices_.empty() ? nullptr : free_vertices_.data() + 1;
	}
	void Mesh_impl::mark_free(Face f)
	{
		if(f == no_face)
			return;

		face_edges_[f.index()] = no_edge;
		const auto free_iter =
		        std::lower_bound(free_faces_.begin(), free_faces_.end(), f, [](auto lhs, auto rhs) {
			        return static_cast<std::uint32_t>(lhs.index()) > static_cast<std::uint32_t>(rhs.index());
		        });
		assert(free_iter == free_faces_.end() || *free_iter != f);
		free_faces_.insert(free_iter, f);
		unused_face_count = static_cast<YGDL_Index>(free_faces_.size() - 1);
		unused_face_list  = free_faces_.empty() ? nullptr : free_faces_.data() + 1;
	}

	void Mesh_impl::set_edge_origin(YGDL_Edge e, YGDL_Vertex v)
	{
		assert(!YGDL_mesh_edge_is_dual(e));
		assert(e.mask < primal_edge_origin_.size());
		primal_edge_origin_[e.mask] = v;
	}

	void Mesh_impl::update_all_data_pointers()
	{
		vertex_count = static_cast<YGDL_Index>(vertex_edges_.size());
		face_count   = static_cast<YGDL_Index>(face_edges_.size());
		edge_count   = static_cast<YGDL_Index>(primal_edge_origin_.size());

		unused_vertex_count = static_cast<YGDL_Index>(free_vertices_.size() - 1);
		unused_face_count   = static_cast<YGDL_Index>(free_faces_.size() - 1);
		unused_edge_count   = static_cast<YGDL_Index>(free_edges_.size() - 1);

		vertex_edge        = vertex_edges_.empty() ? nullptr : vertex_edges_.data();
		face_edge          = face_edges_.empty() ? nullptr : face_edges_.data();
		primal_edge_origin = primal_edge_origin_.empty() ? nullptr : primal_edge_origin_.data();
		primal_edge_next   = primal_edge_next_.empty() ? nullptr : primal_edge_next_.data();
		dual_edge_origin   = dual_edge_origin_.empty() ? nullptr : dual_edge_origin_.data();
		dual_edge_next     = dual_edge_next_.empty() ? nullptr : dual_edge_next_.data();

		unused_vertex_list = free_vertices_.empty() ? nullptr : free_vertices_.data() + 1;
		unused_face_list   = free_faces_.empty() ? nullptr : free_faces_.data() + 1;
		unused_edge_list   = free_edges_.empty() ? nullptr : free_edges_.data() + 1;
	}


} // namespace yggdrasill::detail
