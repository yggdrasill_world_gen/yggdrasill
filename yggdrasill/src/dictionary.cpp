#include <yggdrasill/dictionary.h>

#include <yggdrasill/commons.hpp>
#include <yggdrasill/error.hpp>
#include <yggdrasill/string.hpp>

#include "dictionary_impl.h"

#include <stddef.h>
#include <cassert>
#include <functional>
#include <locale>
#include <sstream>
#include <string>
#include <string_view>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <variant>
#include <vector>


using namespace yggdrasill;
using namespace yggdrasill::detail;

static auto regenerate_dict_keys(const auto& keys_in, const auto& map)
{
	auto keys = std::vector<YGDL_String_view>{};
	keys.reserve(map.size());
	assert(keys_in.size() == map.size());

	for(auto k : keys_in) {
		auto iter = map.find(from_ygdl_string_view(k));
		assert(iter != map.end());
		keys.emplace_back(to_ygdl_string_view(iter->first));
	}

	return keys;
}

YGDL_Dict::YGDL_Dict() : user_allocated_(false) {}
YGDL_Dict::YGDL_Dict(bool user_allocated) : user_allocated_(user_allocated) {}
YGDL_Dict::YGDL_Dict(const YGDL_Dict& rhs, bool user_allocated)
  : entries_(rhs.entries_), keys_(regenerate_dict_keys(rhs.keys_, entries_)), user_allocated_(user_allocated)
{
}
YGDL_Dict::YGDL_Dict(YGDL_Dict&& rhs) noexcept
  : entries_(std::move(rhs.entries_)), keys_(std::move(rhs.keys_)), user_allocated_(false)
{
}
YGDL_Dict::~YGDL_Dict() = default;

YGDL_Dict& YGDL_Dict::operator=(YGDL_Dict&& rhs) noexcept
{
	entries_ = std::move(rhs.entries_);
	keys_    = std::move(rhs.keys_);
	return *this;
}
YGDL_Dict& YGDL_Dict::operator=(const YGDL_Dict& rhs)
{
	entries_ = rhs.entries_;
	keys_    = regenerate_dict_keys(rhs.keys_, entries_);
	return *this;
}

[[nodiscard]] Value_variant* YGDL_Dict::get(std::string_view key)
{
	if(auto iter = entries_.find(key); iter != entries_.end()) {
		return &iter->second;
	} else {
		auto&& [inserted_iter, _] = entries_.emplace(key, std::monostate{});
		keys_.emplace_back(to_ygdl_string_view(inserted_iter->first));
		return &inserted_iter->second;
	}
}
[[nodiscard]] const Value_variant* YGDL_Dict::get(std::string_view key) const
{
	if(auto iter = entries_.find(key); iter != entries_.end()) {
		return &iter->second;
	} else {
		return nullptr;
	}
}

void YGDL_Dict::erase(std::string_view key)
{
	if(auto iter = entries_.find(key); iter != entries_.end()) {
		std::erase_if(keys_, [&](auto& k) { return from_ygdl_string_view(k) == key; });
		entries_.erase(iter);
	}
}

[[nodiscard]] YGDL_Dict_key_list YGDL_Dict::keys() const
{
	return YGDL_Dict_key_list{keys_.data(), static_cast<YGDL_Index>(keys_.size())};
}

void YGDL_Dict::clear()
{
	entries_.clear();
	keys_.clear();
}


int32_t YGDL_dict_sizeof(void)
{
	return static_cast<int32_t>(sizeof(YGDL_Dict));
}

YGDL_Bool YGDL_type_is_convertible(YGDL_Type_t from, YGDL_Type_t to)
{
	if(from == to) {
		return true;
	} else {
		switch(from) {
			case YGDL_TYPE_BOOL:
				return to == YGDL_TYPE_INT8 || to == YGDL_TYPE_INT32 || to == YGDL_TYPE_INT64
				       || to == YGDL_TYPE_FLOAT || to == YGDL_TYPE_DOUBLE || to == YGDL_TYPE_STRING;
			case YGDL_TYPE_INT8:
				return to == YGDL_TYPE_BOOL || to == YGDL_TYPE_INT32 || to == YGDL_TYPE_INT64
				       || to == YGDL_TYPE_FLOAT || to == YGDL_TYPE_DOUBLE || to == YGDL_TYPE_STRING;
			case YGDL_TYPE_INT32:
				return to == YGDL_TYPE_BOOL || to == YGDL_TYPE_INT8 || to == YGDL_TYPE_INT64
				       || to == YGDL_TYPE_FLOAT || to == YGDL_TYPE_DOUBLE || to == YGDL_TYPE_STRING;
			case YGDL_TYPE_INT64:
				return to == YGDL_TYPE_BOOL || to == YGDL_TYPE_INT8 || to == YGDL_TYPE_INT32
				       || to == YGDL_TYPE_FLOAT || to == YGDL_TYPE_DOUBLE || to == YGDL_TYPE_STRING;
			case YGDL_TYPE_FLOAT:
				return to == YGDL_TYPE_INT32 || to == YGDL_TYPE_INT8 || to == YGDL_TYPE_INT64
				       || to == YGDL_TYPE_DOUBLE || to == YGDL_TYPE_STRING || to == YGDL_TYPE_VEC2
				       || to == YGDL_TYPE_VEC3;
			case YGDL_TYPE_DOUBLE:
				return to == YGDL_TYPE_INT32 || to == YGDL_TYPE_INT8 || to == YGDL_TYPE_INT64
				       || to == YGDL_TYPE_FLOAT || to == YGDL_TYPE_STRING || to == YGDL_TYPE_VEC2
				       || to == YGDL_TYPE_VEC3;
			case YGDL_TYPE_VEC2: return to == YGDL_TYPE_VEC3;
			case YGDL_TYPE_NONE:
			case YGDL_TYPE_VEC3:
			case YGDL_TYPE_STRING:
			case YGDL_TYPE_ARRAY:
			case YGDL_TYPE_DICT: return false;
		}
	}

	return false;
}

YGDL_Dict* YGDL_dict_create()
{
	return new YGDL_Dict(true);
}
void YGDL_dict_destroy(YGDL_Dict* dict)
{
	if(!dict)
		return;

	if(!dict->user_allocated()) {
		assert(!"YGDL_dict_destroy shall only be invoked on YGDL_Dicts returned by YGDL_dict_create.");
		return;
	}

	delete dict;
}
YGDL_Dict* YGDL_dict_copy(const YGDL_Dict* other)
{
	return new YGDL_Dict(*other, true);
}
void YGDL_dict_copy_assign(YGDL_Dict* lhs, const YGDL_Dict* rhs)
{
	if(lhs == rhs)
		return;

	*lhs = *rhs;
}
void YGDL_dict_move_assign(YGDL_Dict* lhs, YGDL_Dict* rhs)
{
	if(lhs == rhs)
		return;

	*lhs = std::move(*rhs);
	rhs->clear();
}

YGDL_Dict_key_list YGDL_dict_keys(const YGDL_Dict* dict)
{
	assert(dict);
	return dict->keys();
}

void YGDL_dict_erase(YGDL_Dict* dict, YGDL_String_view key)
{
	assert(dict);
	dict->erase(from_ygdl_string_view(key));
}
void YGDL_dict_clear(YGDL_Dict* dict)
{
	assert(dict);
	dict->clear();
}

YGDL_Dict_value* YGDL_dict_get(YGDL_Dict* dict, YGDL_String_view key)
{
	assert(dict);
	assert(key.length > 0);
	auto* value_ptr = dict->get(from_ygdl_string_view(key));
	return reinterpret_cast<YGDL_Dict_value*>(value_ptr);
}
const YGDL_Dict_value* YGDL_dict_get_const(const YGDL_Dict* dict, YGDL_String_view key)
{
	assert(dict);
	auto* value_ptr = dict->get(from_ygdl_string_view(key));
	return reinterpret_cast<const YGDL_Dict_value*>(value_ptr);
}
YGDL_Type_t YGDL_dict_value_type(const YGDL_Dict_value* value_ptr)
{
	if(!value_ptr)
		return YGDL_TYPE_NONE;

	auto& value_variant = *reinterpret_cast<const Value_variant*>(value_ptr);
	return static_cast<YGDL_Type_t>(value_variant.index());
}

void YGDL_dict_value_copy_assign(YGDL_Dict_value* lhs, const YGDL_Dict_value* rhs)
{
	assert(lhs);
	assert(rhs);
	auto& lhs_variant = *reinterpret_cast<Value_variant*>(lhs);
	auto& rhs_variant = *reinterpret_cast<const Value_variant*>(rhs);
	lhs_variant       = rhs_variant;
}

YGDL_Dict* YGDL_dict_value_dict(YGDL_Dict_value* value_ptr)
{
	assert(value_ptr);
	auto& value_variant = *reinterpret_cast<Value_variant*>(value_ptr);
	if(auto* element = std::get_if<Recursive_wrapper<YGDL_Dict>>(&value_variant)) {
		return &**element;
	} else {
		return &*value_variant.emplace<Recursive_wrapper<YGDL_Dict>>();
	}
}
YGDL_Array* YGDL_dict_value_array(YGDL_Dict_value* value_ptr, YGDL_Type_t value_type)
{
	assert(value_ptr);
	auto& value_variant = *reinterpret_cast<Value_variant*>(value_ptr);
	if(auto element = std::get_if<Array_impl>(&value_variant);
	   element && (value_type == YGDL_TYPE_NONE || element->type == value_type)) {
		return element;
	} else {
		return &value_variant.emplace<Array_impl>(value_type);
	}
}
YGDL_Bool* YGDL_dict_value_bool(YGDL_Dict_value* value_ptr)
{
	assert(value_ptr);
	auto& var = *reinterpret_cast<Value_variant*>(value_ptr);
	return yggdrasill::visit(
	        var,
	        [](YGDL_Bool& v) { return &v; },
	        [&](int8_t& v) { return &var.emplace<YGDL_Bool>(static_cast<bool>(v)); },
	        [&](int32_t& v) { return &var.emplace<YGDL_Bool>(static_cast<bool>(v)); },
	        [&](int64_t& v) { return &var.emplace<YGDL_Bool>(static_cast<bool>(v)); },
	        [&](auto&) { return &var.emplace<YGDL_Bool>(false); });
}
int8_t* YGDL_dict_value_int8(YGDL_Dict_value* value_ptr)
{
	assert(value_ptr);
	auto& var = *reinterpret_cast<Value_variant*>(value_ptr);
	return yggdrasill::visit(
	        var,
	        [](int8_t& v) { return &v; },
	        [&](int32_t& v) { return &var.emplace<int8_t>(static_cast<int8_t>(v)); },
	        [&](int64_t& v) { return &var.emplace<int8_t>(static_cast<int8_t>(v)); },
	        [&](YGDL_Bool& v) { return &var.emplace<int8_t>(static_cast<int8_t>(v ? 1 : 0)); },
	        [&](float& v) { return &var.emplace<int8_t>(static_cast<int8_t>(v)); },
	        [&](double& v) { return &var.emplace<int8_t>(static_cast<int8_t>(v)); },
	        [&](auto&) { return &var.emplace<int8_t>(static_cast<int8_t>(0)); });
}
int32_t* YGDL_dict_value_int32(YGDL_Dict_value* value_ptr)
{
	assert(value_ptr);
	auto& var = *reinterpret_cast<Value_variant*>(value_ptr);
	return yggdrasill::visit(
	        var,
	        [](int32_t& v) { return &v; },
	        [&](int8_t& v) { return &var.emplace<int32_t>(static_cast<int32_t>(v)); },
	        [&](int64_t& v) { return &var.emplace<int32_t>(static_cast<int32_t>(v)); },
	        [&](YGDL_Bool& v) { return &var.emplace<int32_t>(v ? 1 : 0); },
	        [&](float& v) { return &var.emplace<int32_t>(static_cast<int32_t>(v)); },
	        [&](double& v) { return &var.emplace<int32_t>(static_cast<int32_t>(v)); },
	        [&](auto&) { return &var.emplace<int32_t>(0); });
}
int64_t* YGDL_dict_value_int64(YGDL_Dict_value* value_ptr)
{
	assert(value_ptr);
	auto& var = *reinterpret_cast<Value_variant*>(value_ptr);
	return yggdrasill::visit(
	        var,
	        [](int64_t& v) { return &v; },
	        [&](int8_t& v) { return &var.emplace<int64_t>(static_cast<int64_t>(v)); },
	        [&](int32_t& v) { return &var.emplace<int64_t>(static_cast<int64_t>(v)); },
	        [&](YGDL_Bool& v) { return &var.emplace<int64_t>(v ? 1 : 0); },
	        [&](float& v) { return &var.emplace<int64_t>(static_cast<int64_t>(v)); },
	        [&](double& v) { return &var.emplace<int64_t>(static_cast<int64_t>(v)); },
	        [&](auto&) { return &var.emplace<int64_t>(0); });
}
float* YGDL_dict_value_float(YGDL_Dict_value* value_ptr)
{
	assert(value_ptr);
	auto& var = *reinterpret_cast<Value_variant*>(value_ptr);
	return yggdrasill::visit(
	        var,
	        [](float& v) { return &v; },
	        [&](YGDL_Bool& v) { return &var.emplace<float>(v ? 1.f : 0.f); },
	        [&](int8_t& v) { return &var.emplace<float>(static_cast<float>(v)); },
	        [&](int32_t& v) { return &var.emplace<float>(static_cast<float>(v)); },
	        [&](int64_t& v) { return &var.emplace<float>(static_cast<float>(v)); },
	        [&](double& v) { return &var.emplace<float>(static_cast<float>(v)); },
	        [&](auto&) { return &var.emplace<float>(0.f); });
}
double* YGDL_dict_value_double(YGDL_Dict_value* value_ptr)
{
	assert(value_ptr);
	auto& var = *reinterpret_cast<Value_variant*>(value_ptr);
	return yggdrasill::visit(
	        var,
	        [](double& v) { return &v; },
	        [&](YGDL_Bool& v) { return &var.emplace<double>(v ? 1.f : 0.f); },
	        [&](int8_t& v) { return &var.emplace<double>(static_cast<double>(v)); },
	        [&](int32_t& v) { return &var.emplace<double>(static_cast<double>(v)); },
	        [&](int64_t& v) { return &var.emplace<double>(static_cast<double>(v)); },
	        [&](float& v) { return &var.emplace<double>(static_cast<double>(v)); },
	        [&](auto&) { return &var.emplace<double>(0.f); });
}
YGDL_Vec2* YGDL_dict_value_vec2(YGDL_Dict_value* value_ptr)
{
	assert(value_ptr);
	auto& var = *reinterpret_cast<Value_variant*>(value_ptr);
	return yggdrasill::visit(
	        var,
	        [](yggdrasill::Vec2& v) { return &v; },
	        [&](float& v) { return &var.emplace<yggdrasill::Vec2>(v, v); },
	        [&](double& v) { // clang-format off
	        	auto vf = static_cast<float>(v);
	        	return &var.emplace<yggdrasill::Vec2>(vf, vf);
	        }, // clang-format on
	        [&](auto&) { return &var.emplace<yggdrasill::Vec2>(); });
}
YGDL_Vec3* YGDL_dict_value_vec3(YGDL_Dict_value* value_ptr)
{
	assert(value_ptr);
	auto& var = *reinterpret_cast<Value_variant*>(value_ptr);
	return yggdrasill::visit(
	        var,
	        [](yggdrasill::Vec3& v) { return &v; },
	        [&](float& v) { return &var.emplace<yggdrasill::Vec3>(v, v, v); },
	        [&](double& v) { // clang-format off
	        	auto vf = static_cast<float>(v);
	        	return &var.emplace<yggdrasill::Vec3>(vf, vf, vf);
	        }, // clang-format on
	        [&](yggdrasill::Vec2& v) { return &var.emplace<yggdrasill::Vec3>(v, 0.f); },
	        [&](auto&) { return &var.emplace<yggdrasill::Vec3>(); });
}
YGDL_String* YGDL_dict_value_string(YGDL_Dict_value* value_ptr)
{
	assert(value_ptr);
	auto& var = *reinterpret_cast<Value_variant*>(value_ptr);
	auto* str = yggdrasill::visit(
	        var,
	        [](yggdrasill::String& v) { return &v; },
	        [&](YGDL_Bool& v) { return &var.emplace<yggdrasill::String>(v ? "true" : "false"); },
	        [&](int8_t& v) { return &var.emplace<yggdrasill::String>(std::to_string(v)); },
	        [&](int32_t& v) { return &var.emplace<yggdrasill::String>(std::to_string(v)); },
	        [&](int64_t& v) { return &var.emplace<yggdrasill::String>(std::to_string(v)); },
	        [&](float& v) { // clang-format off
	        	std::ostringstream ss; // TODO: this is where I would but std::to_chars, if I had any...
	        	ss.imbue(std::locale::classic());
	        	ss << v;
	        	return &var.emplace<yggdrasill::String>(ss.str());
	        }, // clang-format on
	        [&](double& v) { // clang-format off
	        	std::ostringstream ss; // TODO: this is where I would but std::to_chars, if I had any...
	        	ss.imbue(std::locale::classic());
	        	ss << v;
	        	return &var.emplace<yggdrasill::String>(ss.str());
	        }, // clang-format on
	        [&](auto&) { return &var.emplace<yggdrasill::String>(); });

	return reinterpret_cast<YGDL_String*>(str);
}

template <typename T>
static const T* get_const_impl(const YGDL_Dict_value* value_ptr, YGDL_Error* error)
{
	auto var = reinterpret_cast<const Value_variant*>(value_ptr);
	if(auto element = var ? std::get_if<T>(var) : nullptr) {
		return element;

	} else {
		auto empty = !var || std::get_if<std::monostate>(var);
		set_error(error, YGDL_ERROR_CODE_TYPE_MISMATCH, [&] {
			return empty ? "No value with that key in the Dictionary"
			             : "The value has a different type than requested";
		});
		return nullptr;
	}
}
const YGDL_Dict* YGDL_dict_value_const_dict(const YGDL_Dict_value* value_ptr, YGDL_Error* error)
{
	return &**get_const_impl<Recursive_wrapper<YGDL_Dict>>(value_ptr, error);
}
const YGDL_Array* YGDL_dict_value_const_array(
        const YGDL_Dict_value* value_ptr, YGDL_Type_t value_type, YGDL_Error* error)
{
	auto array = get_const_impl<Array_impl>(value_ptr, error);
	if(array && value_type != YGDL_TYPE_NONE && array->type != value_type) {
		set_error(error, YGDL_ERROR_CODE_TYPE_MISMATCH, [&] { return "Type of array elements doesn't match"; });
		return nullptr;
	}
	return array;
}
const YGDL_Bool* YGDL_dict_value_const_bool(const YGDL_Dict_value* value_ptr, YGDL_Error* error)
{
	return get_const_impl<YGDL_Bool>(value_ptr, error);
}
const int8_t* YGDL_dict_value_const_int8(const YGDL_Dict_value* value_ptr, YGDL_Error* error)
{
	return get_const_impl<int8_t>(value_ptr, error);
}
const int32_t* YGDL_dict_value_const_int32(const YGDL_Dict_value* value_ptr, YGDL_Error* error)
{
	return get_const_impl<int32_t>(value_ptr, error);
}
const int64_t* YGDL_dict_value_const_int64(const YGDL_Dict_value* value_ptr, YGDL_Error* error)
{
	return get_const_impl<int64_t>(value_ptr, error);
}
const float* YGDL_dict_value_const_float(const YGDL_Dict_value* value_ptr, YGDL_Error* error)
{
	return get_const_impl<float>(value_ptr, error);
}
const double* YGDL_dict_value_const_double(const YGDL_Dict_value* value_ptr, YGDL_Error* error)
{
	return get_const_impl<double>(value_ptr, error);
}
const YGDL_Vec2* YGDL_dict_value_const_vec2(const YGDL_Dict_value* value_ptr, YGDL_Error* error)
{
	return get_const_impl<Vec2>(value_ptr, error);
}
const YGDL_Vec3* YGDL_dict_value_const_vec3(const YGDL_Dict_value* value_ptr, YGDL_Error* error)
{
	return get_const_impl<Vec3>(value_ptr, error);
}
const YGDL_String* YGDL_dict_value_const_string(const YGDL_Dict_value* value_ptr, YGDL_Error* error)
{
	return reinterpret_cast<const YGDL_String*>(get_const_impl<String>(value_ptr, error));
}
