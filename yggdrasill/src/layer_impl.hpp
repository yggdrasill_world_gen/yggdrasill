#pragma once

#include <yggdrasill/world.h>
#include <yggdrasill/commons.hpp>
#include <yggdrasill/mesh.hpp>
#include <yggdrasill/utils.hpp>

#include "cow_ptr.hpp"
#include "dictionary_impl.h"

#include <algorithm>
#include <cstddef>
#include <cstdlib>
#include <memory>
#include <new>
#include <span>
#include <sstream>
#include <stdexcept>
#include <unordered_map>
#include <variant>
#include <vector>


namespace yggdrasill::detail {

	inline YGDL_Bool lerp(const YGDL_Bool& a, const YGDL_Bool& b, float t)
	{
		return t >= 0.5f ? b : a;
	}
	inline float lerp(const float& a, const float& b, float t)
	{
		return std::lerp(a, b, t);
	}
	inline double lerp(const double& a, const double& b, float t)
	{
		return std::lerp(a, b, static_cast<double>(t));
	}
	inline int8_t lerp(const int8_t& a, const int8_t& b, float t)
	{
		return static_cast<int8_t>(std::round(std::lerp(static_cast<float>(a), static_cast<float>(b), t)));
	}
	inline int32_t lerp(const int32_t& a, const int32_t& b, float t)
	{
		return static_cast<int32_t>(
		        std::round(std::lerp(static_cast<double>(a), static_cast<double>(b), static_cast<double>(t))));
	}
	inline int64_t lerp(const int64_t& a, const int64_t& b, float t)
	{
		return static_cast<int64_t>(
		        std::round(std::lerp(static_cast<double>(a), static_cast<double>(b), static_cast<double>(t))));
	}
	inline YGDL_Vec2 lerp(const YGDL_Vec2& a, const YGDL_Vec2& b, float t)
	{
		return {std::lerp(a.x, b.x, t), std::lerp(a.y, b.y, t)};
	}
	inline YGDL_Vec3 lerp(const YGDL_Vec3& a, const YGDL_Vec3& b, float t)
	{
		return {std::lerp(a.x, b.x, t), std::lerp(a.y, b.y, t), std::lerp(a.z, b.z, t)};
	}


	inline YGDL_Bool slerp(const YGDL_Bool& a, const YGDL_Bool& b, float t)
	{
		return lerp(a, b, t);
	}
	inline float slerp(const float& a, const float& b, float t)
	{
		return lerp(a, b, t);
	}
	inline double slerp(const double& a, const double& b, float t)
	{
		return lerp(a, b, t);
	}
	inline int8_t slerp(const int8_t& a, const int8_t& b, float t)
	{
		return lerp(a, b, t);
	}
	inline int32_t slerp(const int32_t& a, const int32_t& b, float t)
	{
		return lerp(a, b, t);
	}
	inline int64_t slerp(const int64_t& a, const int64_t& b, float t)
	{
		return lerp(a, b, t);
	}
	inline YGDL_Vec2 slerp(const YGDL_Vec2& a, const YGDL_Vec2& b, float t)
	{
		return slerp(static_cast<const yggdrasill::Vec2&>(a), static_cast<const yggdrasill::Vec2&>(b), t);
	}
	inline YGDL_Vec3 slerp(const YGDL_Vec3& a, const YGDL_Vec3& b, float t)
	{
		return slerp(static_cast<const yggdrasill::Vec3&>(a), static_cast<const yggdrasill::Vec3&>(b), t);
	}


	inline YGDL_Bool min(const YGDL_Bool& a, const YGDL_Bool& b)
	{
		return a && b;
	}
	inline float min(const float& a, const float& b)
	{
		return std::min(a, b);
	}
	inline double min(const double& a, const double& b)
	{
		return std::min(a, b);
	}
	inline int8_t min(const int8_t& a, const int8_t& b)
	{
		return std::min(a, b);
	}
	inline int32_t min(const int32_t& a, const int32_t& b)
	{
		return std::min(a, b);
	}
	inline int64_t min(const int64_t& a, const int64_t& b)
	{
		return std::min(a, b);
	}
	inline YGDL_Vec2 min(const YGDL_Vec2& a, const YGDL_Vec2& b)
	{
		return {std::min(a.x, b.x), std::min(a.y, b.y)};
	}
	inline YGDL_Vec3 min(const YGDL_Vec3& a, const YGDL_Vec3& b)
	{
		return {std::min(a.x, b.x), std::min(a.y, b.y), std::min(a.z, b.z)};
	}


	inline YGDL_Bool max(const YGDL_Bool& a, const YGDL_Bool& b)
	{
		return a || b;
	}
	inline float max(const float& a, const float& b)
	{
		return std::max(a, b);
	}
	inline double max(const double& a, const double& b)
	{
		return std::max(a, b);
	}
	inline int8_t max(const int8_t& a, const int8_t& b)
	{
		return std::max(a, b);
	}
	inline int32_t max(const int32_t& a, const int32_t& b)
	{
		return std::max(a, b);
	}
	inline int64_t max(const int64_t& a, const int64_t& b)
	{
		return std::max(a, b);
	}
	inline YGDL_Vec2 max(const YGDL_Vec2& a, const YGDL_Vec2& b)
	{
		return {std::max(a.x, b.x), std::max(a.y, b.y)};
	}
	inline YGDL_Vec3 max(const YGDL_Vec3& a, const YGDL_Vec3& b)
	{
		return {std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z, b.z)};
	}

	inline constexpr float square(float v)
	{
		return v * v;
	}

	/// An immutable copy of the definition, shared between all layer instances of a type.\n
	/// Both to reduce memory consumption and avoid strict lifetime requirements for the
	/// definition and its members.
	struct Interned_definition : public YGDL_Layer_definition {
		String interned_id;
		String interned_description;

		explicit Interned_definition() = default;
		Interned_definition(const YGDL_Layer_definition& def)
		  : YGDL_Layer_definition(def), interned_id(def.id), interned_description(def.description)
		{
			id          = detail::to_ygdl_string_view(interned_id);
			description = detail::to_ygdl_string_view(interned_description);
		}
	};

	template <typename T>
	using Aligned_vector = detail::Aligned_vector<T>;


	class YGDL_Layer_impl : public YGDL_Layer {
	  public:
		template <typename T>
		YGDL_Layer_impl(std::shared_ptr<const Interned_definition> def, YGDL_Index size, const T& value)
		  : YGDL_Layer{nullptr, nullptr, 0}, storage_(Aligned_vector<T>(size, value)), definition_(std::move(def))
		{
			definition = definition_.get();
			update_all_data_pointers();
		}

		template <typename T>
		YGDL_Layer_impl(const YGDL_Layer_definition& def, YGDL_Index size, const T& value)
		  : YGDL_Layer{nullptr, nullptr, 0}
		  , storage_(Aligned_vector<T>(size, value))
		  , definition_(std::make_shared<const Interned_definition>(def))
		{
			definition = definition_.get();
			update_all_data_pointers();
		}
		YGDL_Layer_impl(const YGDL_Layer_impl& rhs)
		  : YGDL_Layer{rhs.definition, nullptr, 0}
		  , storage_(yggdrasill::detail::copy_with_capacity(rhs.storage_))
		  , definition_(rhs.definition_)
		{
			update_all_data_pointers();
		}

		YGDL_Layer_impl& operator=(const YGDL_Layer_impl& rhs)
		{
			definition  = rhs.definition;
			storage_    = rhs.storage_;
			definition_ = rhs.definition_;
			update_all_data_pointers();
			return *this;
		}

		auto interned_definition() const { return definition_; }

		bool compatible(const YGDL_Layer_definition& other) const
		{
			return definition->data_type == other.data_type && definition->ref_type == other.ref_type;
		}

		bool validate(const YGDL_Mesh& mesh, YGDL_Error* out_error) const
		{
			const auto cpp_mesh = yggdrasill::Const_mesh_view(mesh);

			switch(definition->ref_type) {
				case YGDL_LAYER_REF_TYPE_VERTEX:
					return std::visit(
					        [&](auto& vec) { return validate_impl(vec, cpp_mesh.vertices(), out_error); },
					        storage_);
				case YGDL_LAYER_REF_TYPE_FACE:
					return std::visit(
					        [&](auto& vec) { return validate_impl(vec, cpp_mesh.faces(), out_error); }, storage_);
				case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL:
					return std::visit(
					        [&](auto& vec) {
						        return validate_impl(vec, cpp_mesh.undirected_edges<Edge_type::primal>(), out_error);
					        },
					        storage_);
				case YGDL_LAYER_REF_TYPE_EDGE_DUAL:
					return std::visit(
					        [&](auto& vec) {
						        return validate_impl(vec, cpp_mesh.undirected_edges<Edge_type::dual>(), out_error);
					        },
					        storage_);
				case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL_DIRECTED:
					return std::visit(
					        [&](auto& vec) {
						        return validate_impl(vec, cpp_mesh.directed_edges<Edge_type::primal>(), out_error);
					        },
					        storage_);
				case YGDL_LAYER_REF_TYPE_EDGE_DUAL_DIRECTED:
					return std::visit(
					        [&](auto& vec) {
						        return validate_impl(vec, cpp_mesh.directed_edges<Edge_type::dual>(), out_error);
					        },
					        storage_);
			}
			return true;
		}

		void update_all_data_pointers()
		{
			std::visit(
			        [&](auto& vec) {
				        data   = vec.data();
				        length = static_cast<int32_t>(vec.size());
			        },
			        storage_);
		}

		void resize(YGDL_Index size)
		{
			if(this->length >= size)
				return;

			std::visit(
			        [&](auto& vec) {
				        resize_impl(vec, size);
				        this->data   = vec.data();
				        this->length = static_cast<int32_t>(vec.size());
			        },
			        storage_);
		}

		template <typename IndexMapping>
		void reset(std::span<const YGDL_Index> indices, IndexMapping map_index, YGDL_Index stride = 1)
		{
			std::visit(
			        [&](auto& vec) {
				        if(definition->invalidation == YGDL_LAYER_INVALIDATION_RESET_ALL) {
					        reset_all_impl(vec);
				        } else {
					        for(auto index : indices) {
						        const auto mapped = map_index(index);
						        for(int i = 0; i < stride; i++) {
							        reset_impl(vec[mapped + i]);
						        }
					        }
				        }
			        },
			        storage_);
		}
		void reset(std::span<const YGDL_Index> indices)
		{
			reset(indices, [](auto i) { return i; });
		}

		template <typename IndexMapping>
		void interpolate(std::span<const YGDL_Layer_element_interpolation> indices,
		                 float                                             weight,
		                 IndexMapping                                      map_index,
		                 bool                                              two_elements = false)
		{
			std::visit(
			        [&](auto& vec) {
				        if(definition->interpolation == YGDL_LAYER_INTERPOLATION_RESET_ALL) {
					        reset_all_impl(vec);

				        } else if(definition->interpolation == YGDL_LAYER_INTERPOLATION_RESET_AFFECTED) {
					        for(auto& [target, origin, dest] : indices) {
						        reset_impl(vec[map_index(target)]);
					        }

				        } else {
					        for(auto& [target, origin, dest] : indices) {
						        vec[map_index(target)] =
						                interpolate_impl(vec[map_index(origin)], vec[map_index(dest)], weight);

						        if(two_elements)
							        vec[map_index(target) + 1] = interpolate_impl(
							                vec[map_index(dest)], vec[map_index(origin)], weight);
					        }
				        }
			        },
			        storage_);
		}
		void interpolate(std::span<const YGDL_Layer_element_interpolation> indices, float weight)
		{
			interpolate(indices, weight, [](auto i) { return i; });
		}

		/// direct access to underlying storage; required by `read_world_layer` to load layer from files
		auto& unsafe_direct_storage_access() { return storage_; }

	  private:
		std::variant<Aligned_vector<YGDL_Bool>,
		             Aligned_vector<int8_t>,
		             Aligned_vector<int32_t>,
		             Aligned_vector<int64_t>,
		             Aligned_vector<float>,
		             Aligned_vector<double>,
		             Aligned_vector<YGDL_Vec2>,
		             Aligned_vector<YGDL_Vec3>>
		                                           storage_;
		std::shared_ptr<const Interned_definition> definition_;

		void reset_impl(YGDL_Bool& e) const { e = this->definition->value_info.bool_value.initial; }
		void reset_impl(int8_t& e) const { e = this->definition->value_info.int8_value.initial; }
		void reset_impl(int32_t& e) const { e = this->definition->value_info.int32_value.initial; }
		void reset_impl(int64_t& e) const { e = this->definition->value_info.int64_value.initial; }
		void reset_impl(float& e) const { e = this->definition->value_info.float_value.initial; }
		void reset_impl(double& e) const { e = this->definition->value_info.double_value.initial; }
		void reset_impl(YGDL_Vec2& e) const { e = this->definition->value_info.vec2_value.initial; }
		void reset_impl(YGDL_Vec3& e) const { e = this->definition->value_info.vec3_value.initial; }

		void reset_all_impl(Aligned_vector<YGDL_Bool>& vec) const
		{
			std::fill(vec.begin(), vec.end(), this->definition->value_info.bool_value.initial);
		}
		void reset_all_impl(Aligned_vector<int8_t>& vec) const
		{
			std::fill(vec.begin(), vec.end(), this->definition->value_info.int8_value.initial);
		}
		void reset_all_impl(Aligned_vector<int32_t>& vec) const
		{
			std::fill(vec.begin(), vec.end(), this->definition->value_info.int32_value.initial);
		}
		void reset_all_impl(Aligned_vector<int64_t>& vec) const
		{
			std::fill(vec.begin(), vec.end(), this->definition->value_info.int64_value.initial);
		}
		void reset_all_impl(Aligned_vector<float>& vec) const
		{
			std::fill(vec.begin(), vec.end(), this->definition->value_info.float_value.initial);
		}
		void reset_all_impl(Aligned_vector<double>& vec) const
		{
			std::fill(vec.begin(), vec.end(), this->definition->value_info.double_value.initial);
		}
		void reset_all_impl(Aligned_vector<YGDL_Vec2>& vec) const
		{
			std::fill(vec.begin(), vec.end(), this->definition->value_info.vec2_value.initial);
		}
		void reset_all_impl(Aligned_vector<YGDL_Vec3>& vec) const
		{
			std::fill(vec.begin(), vec.end(), this->definition->value_info.vec3_value.initial);
		}

		void resize_impl(Aligned_vector<YGDL_Bool>& vec, YGDL_Index new_size) const
		{
			vec.resize(new_size, this->definition->value_info.bool_value.initial);
		}
		void resize_impl(Aligned_vector<int8_t>& vec, YGDL_Index new_size) const
		{
			vec.resize(new_size, this->definition->value_info.int8_value.initial);
		}
		void resize_impl(Aligned_vector<int32_t>& vec, YGDL_Index new_size) const
		{
			vec.resize(new_size, this->definition->value_info.int32_value.initial);
		}
		void resize_impl(Aligned_vector<int64_t>& vec, YGDL_Index new_size) const
		{
			vec.resize(new_size, this->definition->value_info.int64_value.initial);
		}
		void resize_impl(Aligned_vector<float>& vec, YGDL_Index new_size) const
		{
			vec.resize(new_size, this->definition->value_info.float_value.initial);
		}
		void resize_impl(Aligned_vector<double>& vec, YGDL_Index new_size) const
		{
			vec.resize(new_size, this->definition->value_info.double_value.initial);
		}
		void resize_impl(Aligned_vector<YGDL_Vec2>& vec, YGDL_Index new_size) const
		{
			vec.resize(new_size, this->definition->value_info.vec2_value.initial);
		}
		void resize_impl(Aligned_vector<YGDL_Vec3>& vec, YGDL_Index new_size) const
		{
			vec.resize(new_size, this->definition->value_info.vec3_value.initial);
		}

		template <typename T>
		T interpolate_impl(const T& origin, const T& dest, float weight)
		{
			switch(definition->interpolation) {
				case YGDL_LAYER_INTERPOLATION_DONT_CARE:
				case YGDL_LAYER_INTERPOLATION_REMOVE_LAYER:
				case YGDL_LAYER_INTERPOLATION_RESET_AFFECTED:
				case YGDL_LAYER_INTERPOLATION_RESET_ALL:
				case YGDL_LAYER_INTERPOLATION_KEEP_ORIGIN: break;

				case YGDL_LAYER_INTERPOLATION_KEEP_DEST: return dest;
				case YGDL_LAYER_INTERPOLATION_LERP: return lerp(origin, dest, weight);
				case YGDL_LAYER_INTERPOLATION_NLERP: return slerp(origin, dest, weight);
				case YGDL_LAYER_INTERPOLATION_MIN_VALUE: return min(origin, dest);
				case YGDL_LAYER_INTERPOLATION_MAX_VALUE: return max(origin, dest);
				case YGDL_LAYER_INTERPOLATION_MIN_WEIGHT: return weight >= 0.5 ? origin : dest;
				case YGDL_LAYER_INTERPOLATION_MAX_WEIGHT: return weight >= 0.5 ? dest : origin;
			}

			return origin;
		}

		void set_validation_error(auto e, auto v, YGDL_Error* out_error) const
		{
			if(!out_error)
				return;

			auto msg = std::ostringstream{};
			msg << "Invalid value " << v << " for " << e << " in layer '"
			    << detail::from_ygdl_string_view(definition->id) << "'";
			YGDL_error_set(out_error,
			               YGDL_ERROR_CODE_PRECONDITION_VIOLATION,
			               detail::to_ygdl_string_view(std::move(msg).str()));
		}
		bool validate_impl(const Aligned_vector<YGDL_Bool>&, auto&&, YGDL_Error*) const { return true; }
		bool validate_impl(const Aligned_vector<int8_t>& vec, auto&& elements, YGDL_Error* out_error) const
		{
			const auto min = definition->value_info.int8_value.min;
			const auto max = definition->value_info.int8_value.max;
			if(min == std::numeric_limits<int8_t>::min() && max == std::numeric_limits<int8_t>::max())
				return true;

			const auto index_shift = definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_PRIMAL
			                                         || definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_DUAL
			                                 ? 1
			                                 : 0;

			for(auto& e : elements) {
				const auto v = vec[e.index() >> index_shift];
				if(v < min || v > max) {
					set_validation_error(e, v, out_error);
					return false;
				}
			}
			return true;
		}
		bool validate_impl(const Aligned_vector<int32_t>& vec, auto&& elements, YGDL_Error* out_error) const
		{
			const auto min = definition->value_info.int32_value.min;
			const auto max = definition->value_info.int32_value.max;
			if(min == std::numeric_limits<int32_t>::min() && max == std::numeric_limits<int32_t>::max())
				return true;

			const auto index_shift = definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_PRIMAL
			                                         || definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_DUAL
			                                 ? 1
			                                 : 0;

			for(auto& e : elements) {
				const auto v = vec[e.index() >> index_shift];
				if(v < min || v > max) {
					set_validation_error(e, v, out_error);
					return false;
				}
			}
			return true;
		}
		bool validate_impl(const Aligned_vector<int64_t>& vec, auto&& elements, YGDL_Error* out_error) const
		{
			const auto min = definition->value_info.int64_value.min;
			const auto max = definition->value_info.int64_value.max;
			if(min == std::numeric_limits<int64_t>::min() && max == std::numeric_limits<int64_t>::max())
				return true;

			const auto index_shift = definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_PRIMAL
			                                         || definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_DUAL
			                                 ? 1
			                                 : 0;

			for(auto& e : elements) {
				const auto v = vec[e.index() >> index_shift];
				if(v < min || v > max) {
					set_validation_error(e, v, out_error);
					return false;
				}
			}
			return true;
		}
		bool validate_impl(const Aligned_vector<float>& vec, auto&& elements, YGDL_Error* out_error) const
		{
			const auto min       = definition->value_info.float_value.min;
			const auto max       = definition->value_info.float_value.max;
			const auto has_min   = definition->value_info.float_value.has_min;
			const auto has_max   = definition->value_info.float_value.has_max;
			const auto check_nan = !definition->value_info.float_value.allow_nan;
			const auto check_inf = !definition->value_info.float_value.allow_inf;

			const auto index_shift = definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_PRIMAL
			                                         || definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_DUAL
			                                 ? 1
			                                 : 0;

			for(auto& e : elements) {
				const auto v = vec[e.index() >> index_shift];
				if((has_min && v < min) || (has_max && v > max)) {
					set_validation_error(e, v, out_error);
					return false;
				}
				if((check_nan && std::isnan(v)) || (check_inf && std::isinf(v))) {
					set_validation_error(e, v, out_error);
					return false;
				}
			}
			return true;
		}
		bool validate_impl(const Aligned_vector<double>& vec, auto&& elements, YGDL_Error* out_error) const
		{
			const auto min       = definition->value_info.double_value.min;
			const auto max       = definition->value_info.double_value.max;
			const auto has_min   = definition->value_info.double_value.has_min;
			const auto has_max   = definition->value_info.double_value.has_max;
			const auto check_nan = !definition->value_info.double_value.allow_nan;
			const auto check_inf = !definition->value_info.double_value.allow_inf;

			const auto index_shift = definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_PRIMAL
			                                         || definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_DUAL
			                                 ? 1
			                                 : 0;

			for(auto& e : elements) {
				const auto v = vec[e.index() >> index_shift];
				if((has_min && v < min) || (has_max && v > max)) {
					set_validation_error(e, v, out_error);
					return false;
				}
				if((check_nan && std::isnan(v)) || (check_inf && std::isinf(v))) {
					set_validation_error(e, v, out_error);
					return false;
				}
			}
			return true;
		}
		bool validate_impl(const Aligned_vector<YGDL_Vec2>& vec, auto&& elements, YGDL_Error* out_error) const
		{
			const auto min            = definition->value_info.vec2_value.min;
			const auto max            = definition->value_info.vec2_value.max;
			const auto has_min        = definition->value_info.vec2_value.has_min;
			const auto has_max        = definition->value_info.vec2_value.has_max;
			const auto check_nan      = !definition->value_info.vec2_value.allow_nan;
			const auto check_inf      = !definition->value_info.vec2_value.allow_inf;
			const auto min_length_2   = square(definition->value_info.vec2_value.min_length);
			const auto max_length_2   = square(definition->value_info.vec2_value.max_length);
			const auto has_min_length = definition->value_info.vec2_value.has_min_length;
			const auto has_max_length = definition->value_info.vec2_value.has_max_length;
			const auto has_length     = has_min_length || has_max_length;

			const auto index_shift = definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_PRIMAL
			                                         || definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_DUAL
			                                 ? 1
			                                 : 0;

			for(auto& e : elements) {
				const auto v = vec[e.index() >> index_shift];
				if((has_min && (v.x < min.x || v.y < min.y)) || (has_max && (v.x > max.x || v.y > max.y))) {
					set_validation_error(e, Vec2(v), out_error);
					return false;
				}
				if((check_nan && (std::isnan(v.x) || std::isnan(v.y)))
				   || (check_inf && (std::isinf(v.x) || std::isinf(v.y)))) {
					set_validation_error(e, Vec2(v), out_error);
					return false;
				}
				if(has_length) {
					const auto length_2 = length2(Vec2(v));
					if((has_min_length && length_2 < min_length_2)
					   || (has_max_length && length_2 > max_length_2)) {
						set_validation_error(e, Vec2(v), out_error);
						return false;
					}
				}
			}
			return true;
		}
		bool validate_impl(const Aligned_vector<YGDL_Vec3>& vec, auto&& elements, YGDL_Error* out_error) const
		{
			const auto min            = definition->value_info.vec3_value.min;
			const auto max            = definition->value_info.vec3_value.max;
			const auto has_min        = definition->value_info.vec3_value.has_min;
			const auto has_max        = definition->value_info.vec3_value.has_max;
			const auto check_nan      = !definition->value_info.vec3_value.allow_nan;
			const auto check_inf      = !definition->value_info.vec3_value.allow_inf;
			const auto min_length_2   = square(definition->value_info.vec3_value.min_length);
			const auto max_length_2   = square(definition->value_info.vec3_value.max_length);
			const auto has_min_length = definition->value_info.vec3_value.has_min_length;
			const auto has_max_length = definition->value_info.vec3_value.has_max_length;
			const auto has_length     = has_min_length || has_max_length;

			const auto index_shift = definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_PRIMAL
			                                         || definition->ref_type == YGDL_LAYER_REF_TYPE_EDGE_DUAL
			                                 ? 1
			                                 : 0;

			for(auto& e : elements) {
				const auto v = vec[e.index() >> index_shift];
				if((has_min && (v.x < min.x || v.y < min.y || v.z < min.z))
				   || (has_max && (v.x > max.x || v.y > max.y || v.z > max.z))) {
					set_validation_error(e, Vec3(v), out_error);
					return false;
				}
				if((check_nan && (std::isnan(v.x) || std::isnan(v.y) || std::isnan(v.z)))
				   || (check_inf && (std::isinf(v.x) || std::isinf(v.y) || std::isinf(v.z)))) {
					set_validation_error(e, Vec3(v), out_error);
					return false;
				}
				if(has_length) {
					const auto length_2 = length2(Vec3(v));
					if((has_min_length && length_2 < min_length_2)
					   || (has_max_length && length_2 > max_length_2)) {
						set_validation_error(e, Vec3(v), out_error);
						return false;
					}
				}
			}
			return true;
		}
	};


	class Layer_container {
	  public:
		Layer_container()                             = default;
		Layer_container(Layer_container&&)            = delete;
		Layer_container& operator=(Layer_container&&) = delete;
		Layer_container(const Layer_container& rhs) : layers_(rhs.layers_)
		{
			for(auto&& [_, value] : layers_) {
				add_layer_to_lookup_table(value);
			}
		}
		Layer_container& operator=(const Layer_container& rhs)
		{
			if(this == &rhs)
				return *this;

			layers_ = rhs.layers_;

			vertex_layers_.clear();
			face_layers_.clear();
			edge_layers_.clear();
			directed_edge_layers_.clear();

			for(auto&& [_, value] : layers_) {
				add_layer_to_lookup_table(value);
			}

			return *this;
		}

		void insert(Cow_ptr<YGDL_Layer_impl> layer)
		{
			auto id   = from_ygdl_string_view(layer.get_const().definition->id);
			auto iter = layers_.emplace(id, std::move(layer)).first;
			add_layer_to_lookup_table(iter->second);
		}

		YGDL_Layer_impl* acquire(const YGDL_Layer_definition& layer_def, YGDL_Index size, YGDL_Error* out_error)
		{
			auto id = from_ygdl_string_view(layer_def.id);
			if(auto layer = layers_.find(id); layer != layers_.end()) {
				if(!layer->second.get_const().compatible(layer_def)) {
					yggdrasill::set_error(out_error, YGDL_ERROR_CODE_TYPE_MISMATCH, [&] {
						return "The given YGDL_Layer_definition is incompatible with the existing layer";
					});
					return nullptr;
				}

				return layer->second.acquire_mutable(out_error);
			}
			// create layer
			auto layer_iter = [&] {
				switch(layer_def.data_type) {
					case YGDL_TYPE_BOOL:
						return layers_
						        .emplace(std::piecewise_construct,
						                 std::forward_as_tuple(std::move(id)),
						                 std::forward_as_tuple(std::in_place,
						                                       layer_def,
						                                       size,
						                                       layer_def.value_info.bool_value.initial))
						        .first;
					case YGDL_TYPE_INT8:
						return layers_
						        .emplace(std::piecewise_construct,
						                 std::forward_as_tuple(std::move(id)),
						                 std::forward_as_tuple(std::in_place,
						                                       layer_def,
						                                       size,
						                                       layer_def.value_info.int8_value.initial))
						        .first;
					case YGDL_TYPE_INT32:
						return layers_
						        .emplace(std::piecewise_construct,
						                 std::forward_as_tuple(std::move(id)),
						                 std::forward_as_tuple(std::in_place,
						                                       layer_def,
						                                       size,
						                                       layer_def.value_info.int32_value.initial))
						        .first;
					case YGDL_TYPE_INT64:
						return layers_
						        .emplace(std::piecewise_construct,
						                 std::forward_as_tuple(std::move(id)),
						                 std::forward_as_tuple(std::in_place,
						                                       layer_def,
						                                       size,
						                                       layer_def.value_info.int64_value.initial))
						        .first;
					case YGDL_TYPE_FLOAT:
						return layers_
						        .emplace(std::piecewise_construct,
						                 std::forward_as_tuple(std::move(id)),
						                 std::forward_as_tuple(std::in_place,
						                                       layer_def,
						                                       size,
						                                       layer_def.value_info.float_value.initial))
						        .first;
					case YGDL_TYPE_DOUBLE:
						return layers_
						        .emplace(std::piecewise_construct,
						                 std::forward_as_tuple(std::move(id)),
						                 std::forward_as_tuple(std::in_place,
						                                       layer_def,
						                                       size,
						                                       layer_def.value_info.double_value.initial))
						        .first;
					case YGDL_TYPE_VEC2:
						return layers_
						        .emplace(std::piecewise_construct,
						                 std::forward_as_tuple(std::move(id)),
						                 std::forward_as_tuple(std::in_place,
						                                       layer_def,
						                                       size,
						                                       layer_def.value_info.vec2_value.initial))
						        .first;
					case YGDL_TYPE_VEC3:
						return layers_
						        .emplace(std::piecewise_construct,
						                 std::forward_as_tuple(std::move(id)),
						                 std::forward_as_tuple(std::in_place,
						                                       layer_def,
						                                       size,
						                                       layer_def.value_info.vec3_value.initial))
						        .first;

					case YGDL_TYPE_STRING:
					case YGDL_TYPE_NONE:
					case YGDL_TYPE_ARRAY:
					case YGDL_TYPE_DICT: break;
				}
				yggdrasill::set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] {
					return "Unexpected data_type";
				});
				return layers_.end();
			}();

			if(layer_iter != layers_.end()) {
				add_layer_to_lookup_table(layer_iter->second);

				return layer_iter->second.acquire_mutable(out_error);
			} else {
				return nullptr;
			}
		}
		void release(YGDL_Layer& layer, const YGDL_Mesh& mesh, YGDL_Bool validate, YGDL_Error* out_error)
		{
			if(auto layer_iter = layers_.find(from_ygdl_string_view(layer.definition->id));
			   layer_iter != layers_.end()) {
				assert(&layer == &layer_iter->second.get_const());
				if(validate)
					static_cast<YGDL_Layer_impl&>(layer).validate(mesh, out_error);

				layer_iter->second.release_mutable();

			} else {
				yggdrasill::set_error(out_error, YGDL_ERROR_CODE_PRECONDITION_VIOLATION, [&] {
					return "Couldn't find the given layer";
				});
			}
		}

		const YGDL_Layer_impl* get(YGDL_String_view id) const
		{
			if(auto layer_iter = layers_.find(from_ygdl_string_view(id)); layer_iter != layers_.end()) {
				auto& layer = layer_iter->second.get_const();
				return &layer;
			}

			return nullptr;
		}

		void remove(YGDL_String_view id)
		{
			if(auto layer_iter = layers_.find(from_ygdl_string_view(id)); layer_iter != layers_.end()) {
				auto& layer = layer_iter->second;
				switch(layer.get_const().definition->ref_type) {
					case YGDL_LAYER_REF_TYPE_VERTEX: vertex_layers_.erase(layer); break;
					case YGDL_LAYER_REF_TYPE_FACE: face_layers_.erase(layer); break;
					case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL:
					case YGDL_LAYER_REF_TYPE_EDGE_DUAL: edge_layers_.erase(layer); break;
					case YGDL_LAYER_REF_TYPE_EDGE_DUAL_DIRECTED:
					case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL_DIRECTED: directed_edge_layers_.erase(layer); break;
				}

				layers_.erase(layer_iter);
			}
		}

		void clear()
		{
			layers_.clear();
			vertex_layers_.clear();
			face_layers_.clear();
			edge_layers_.clear();
			directed_edge_layers_.clear();
		}

		std::uint32_t version(YGDL_String_view id) const
		{
			if(auto layer_iter = layers_.find(from_ygdl_string_view(id)); layer_iter != layers_.end()) {
				return layer_iter->second.version();
			} else {
				return 0;
			}
		}

		YGDL_Index size() const { return static_cast<YGDL_Index>(layers_.size()); }
		void       list(std::span<const YGDL_Layer*> out) const
		{
			for(auto i = std::size_t(0); auto&& [key, value] : layers_) {
				out[i++] = &value.get_const();
			}
		}

		template <std::invocable<const Cow_ptr<YGDL_Layer_impl>&> Callback>
		void foreach(Callback&& callback) const
		{
			for(auto&& [key, value] : layers_)
				callback(value);
		}

		bool validate(const YGDL_Mesh& mesh, YGDL_Error* out_error) const
		{
			for(auto&& [key, value] : layers_) {
				if(!value.get_const().validate(mesh, out_error))
					return false;
			}
			return true;
		}

		bool resize_vertex_layers(YGDL_Index size, YGDL_Error* out_error)
		{
			return resize_layer(vertex_layers_.all, size, out_error);
		}
		bool resize_face_layers(YGDL_Index size, YGDL_Error* out_error)
		{
			return resize_layer(face_layers_.all, size, out_error);
		}
		bool resize_edge_layers(YGDL_Index size, YGDL_Error* out_error)
		{
			return resize_layer(directed_edge_layers_.all, size, out_error)
			       && resize_layer(edge_layers_.all, size / 2, out_error);
		}


		bool on_mesh_change(std::span<const YGDL_Index>                       vertices,
		                    std::span<const YGDL_Index>                       faces,
		                    std::span<const YGDL_Index>                       primal_edges,
		                    std::span<const YGDL_Index>                       dual_edges,
		                    std::span<const YGDL_Layer_element_interpolation> vertex_interpolations,
		                    std::span<const YGDL_Layer_element_interpolation> primal_edge_interpolations,
		                    std::span<const YGDL_Layer_element_interpolation> dual_edge_interpolations,
		                    std::span<const YGDL_Layer_element_interpolation> face_interpolations,
		                    float                                             interpolation_weight,
		                    YGDL_Error*                                       out_error)
		{
			// We build a cascade of lambdas that call modify on the Cow_ptrs and wait for the previous lambda
			//   in the cascade.
			// The effect of this construct is that we first try to aquire locks for each layer we need to
			//   modify, only then actually modify them and finally releasing all locks.
			// The reason for using this _interesting_ construct is to (a) keep the logic for each aspect close
			//   together and (b) so we don't need any additional storage at runtime.

			auto vertex_interpolation = [&] {
				if(vertex_interpolations.empty())
					return true;

				return modify(vertex_layers_.interpolated, out_error, [&](auto& l) {
					l.interpolate(vertex_interpolations, interpolation_weight);
				});
			};

			auto face_interpolation = [&] {
				if(face_interpolations.empty())
					return vertex_interpolation();

				return modify(
				        face_layers_.interpolated,
				        out_error,
				        [&](auto& l) { l.interpolate(face_interpolations, interpolation_weight); },
				        vertex_interpolation);
			};

			auto edge_interpolation = [&] {
				if(primal_edge_interpolations.empty() && dual_edge_interpolations.empty())
					return face_interpolation();

				auto interpolate = [&](auto& layer) {
					switch(layer.definition->ref_type) {
						case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL:
							layer.interpolate(primal_edge_interpolations, interpolation_weight, [](auto i) {
								return i >> 1u;
							});
							break;
						case YGDL_LAYER_REF_TYPE_EDGE_DUAL:
							layer.interpolate(dual_edge_interpolations, interpolation_weight, [](auto i) {
								return i >> 1u;
							});
							break;
						default: assert(!"unreachable, ref_type doesn't match edge_layers_-list");
					}
				};

				return modify(edge_layers_.interpolated, out_error, interpolate, face_interpolation);
			};

			auto directed_edge_interpolation = [&] {
				if(primal_edge_interpolations.empty() && dual_edge_interpolations.empty())
					return edge_interpolation();

				auto interpolate = [&](auto& layer) {
					switch(layer.definition->ref_type) {
						case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL_DIRECTED:
							layer.interpolate(
							        primal_edge_interpolations,
							        interpolation_weight,
							        [](auto i) { return i & ~1u; },
							        true);
							break;
						case YGDL_LAYER_REF_TYPE_EDGE_DUAL_DIRECTED:
							layer.interpolate(
							        dual_edge_interpolations,
							        interpolation_weight,
							        [](auto i) { return i & ~1u; },
							        true);
							break;
						default: assert(!"unreachable, ref_type doesn't match edge_layers_-list");
					}
				};

				return modify(directed_edge_layers_.interpolated, out_error, interpolate, edge_interpolation);
			};

			auto vertex_reset = [&] {
				if(vertices.empty())
					return directed_edge_interpolation();

				return modify(
				        vertex_layers_.on_change_reset,
				        out_error,
				        [&](auto& l) { l.reset(vertices); },
				        directed_edge_interpolation);
			};

			auto face_reset = [&] {
				if(faces.empty())
					return vertex_reset();

				return modify(
				        face_layers_.on_change_reset, out_error, [&](auto& l) { l.reset(faces); }, vertex_reset);
			};

			auto edge_reset = [&] {
				if(primal_edges.empty() && dual_edges.empty()) {
					return face_reset();
				}

				auto reset = [&](auto& layer) {
					switch(layer.definition->ref_type) {
						case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL:
							layer.reset(primal_edges, [](auto i) { return i >> 1u; });
							break;
						case YGDL_LAYER_REF_TYPE_EDGE_DUAL:
							layer.reset(dual_edges, [](auto i) { return i >> 1u; });
							break;
						default: assert(!"unreachable, ref_type doesn't match edge_layers_-list");
					}
				};
				return modify(edge_layers_.on_change_reset, out_error, reset, face_reset);
			};

			auto directed_edge_reset = [&] {
				if(primal_edges.empty() && dual_edges.empty()) {
					return edge_reset();
				}

				auto reset = [&](auto& layer) {
					switch(layer.definition->ref_type) {
						case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL_DIRECTED:
							layer.reset(
							        primal_edges, [](auto i) { return i & ~1u; }, 2);
							break;
						case YGDL_LAYER_REF_TYPE_EDGE_DUAL_DIRECTED:
							layer.reset(
							        dual_edges, [](auto i) { return i & ~1u; }, 2);
							break;
						default: assert(!"unreachable, ref_type doesn't match edge_layers_-list");
					}
				};
				return modify(directed_edge_layers_.on_change_reset, out_error, reset, edge_reset);
			};

			// start executing the cascade
			if(!directed_edge_reset()) {
				return false;
			}


			// erase layers on changes
			if(!vertices.empty()) {
				vertex_layers_.trigger_remove_on_change(layers_);
			}
			if(!faces.empty()) {
				face_layers_.trigger_remove_on_change(layers_);
			}
			if(!primal_edges.empty() || !dual_edges.empty()) {
				edge_layers_.trigger_remove_on_change(layers_);
				directed_edge_layers_.trigger_remove_on_change(layers_);
			}

			// erase layers on interpolation
			if(!vertex_interpolations.empty()) {
				vertex_layers_.trigger_remove_on_interpolate(layers_);
			}
			if(!primal_edge_interpolations.empty() || !dual_edge_interpolations.empty()) {
				edge_layers_.trigger_remove_on_interpolate(layers_);
				directed_edge_layers_.trigger_remove_on_interpolate(layers_);
			}

			return true;
		}

	  private:
		struct Layer_lookup_tables {
			std::vector<Cow_ptr<YGDL_Layer_impl>*> all;
			std::vector<Cow_ptr<YGDL_Layer_impl>*> on_change_reset;
			std::vector<std::string>               on_change_remove;
			std::vector<Cow_ptr<YGDL_Layer_impl>*> interpolated;
			std::vector<std::string>               on_interpolate_remove;

			void insert(Cow_ptr<YGDL_Layer_impl>& layer)
			{
				all.emplace_back(&layer);

				const auto& def = *layer.get_const().definition;
				switch(def.invalidation) {
					case YGDL_LAYER_INVALIDATION_RESET_ALL:
					case YGDL_LAYER_INVALIDATION_RESET_AFFECTED: on_change_reset.emplace_back(&layer); break;
					case YGDL_LAYER_INVALIDATION_REMOVE_LAYER:
						on_change_remove.emplace_back(def.id.chars, def.id.length);
						break;
					case YGDL_LAYER_INVALIDATION_KEEP: break;
				}

				switch(def.interpolation) {
					case YGDL_LAYER_INTERPOLATION_DONT_CARE: break; // no operation required
					case YGDL_LAYER_INTERPOLATION_REMOVE_LAYER:
						on_interpolate_remove.emplace_back(def.id.chars, def.id.length);
						break;
					default: interpolated.emplace_back(&layer); break;
				}
			}
			void erase(Cow_ptr<YGDL_Layer_impl>& layer)
			{
				std::erase(all, &layer);

				erase_change_listener(layer);
				erase_interpolation_listener(layer);
			}
			void erase_change_listener(Cow_ptr<YGDL_Layer_impl>& layer)
			{
				const auto& def = *layer.get_const().definition;
				switch(def.invalidation) {
					case YGDL_LAYER_INVALIDATION_RESET_ALL:
					case YGDL_LAYER_INVALIDATION_RESET_AFFECTED: std::erase(on_change_reset, &layer); break;
					case YGDL_LAYER_INVALIDATION_REMOVE_LAYER:
						std::erase(on_change_remove, std::string(def.id.chars, def.id.length));
						break;
					case YGDL_LAYER_INVALIDATION_KEEP: break;
				}
			}
			void erase_interpolation_listener(Cow_ptr<YGDL_Layer_impl>& layer)
			{
				const auto& def = *layer.get_const().definition;
				switch(def.interpolation) {
					case YGDL_LAYER_INTERPOLATION_DONT_CARE: break; // no operation required
					case YGDL_LAYER_INTERPOLATION_REMOVE_LAYER:
						std::erase(on_interpolate_remove, std::string(def.id.chars, def.id.length));
						break;
					default: std::erase(interpolated, &layer); break;
				}
			}

			void clear()
			{
				all.clear();
				on_change_reset.clear();
				on_change_remove.clear();
				on_interpolate_remove.clear();
				interpolated.clear();
			}
			void trigger_remove_on_change(String_map<Cow_ptr<YGDL_Layer_impl>>& layers)
			{
				for(const auto& layer_id : on_change_remove) {
					if(auto iter = layers.find(layer_id); iter != layers.end()) {
						std::erase(all, &iter->second);
						erase_interpolation_listener(iter->second);
						layers.erase(iter);
					}
				}
				on_change_remove.clear();
			}
			void trigger_remove_on_interpolate(String_map<Cow_ptr<YGDL_Layer_impl>>& layers)
			{
				for(const auto& layer_id : on_interpolate_remove) {
					if(auto iter = layers.find(layer_id); iter != layers.end()) {
						std::erase(all, &iter->second);
						erase_change_listener(iter->second);
						layers.erase(iter);
					}
				}
				on_interpolate_remove.clear();
			}
		};

		String_map<Cow_ptr<YGDL_Layer_impl>> layers_;

		Layer_lookup_tables vertex_layers_;
		Layer_lookup_tables face_layers_;
		Layer_lookup_tables edge_layers_;
		Layer_lookup_tables directed_edge_layers_;

		void add_layer_to_lookup_table(Cow_ptr<YGDL_Layer_impl>& layer)
		{
			switch(layer.get_const().definition->ref_type) {
				case YGDL_LAYER_REF_TYPE_VERTEX: vertex_layers_.insert(layer); break;
				case YGDL_LAYER_REF_TYPE_FACE: face_layers_.insert(layer); break;
				case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL:
				case YGDL_LAYER_REF_TYPE_EDGE_DUAL: edge_layers_.insert(layer); break;
				case YGDL_LAYER_REF_TYPE_EDGE_DUAL_DIRECTED:
				case YGDL_LAYER_REF_TYPE_EDGE_PRIMAL_DIRECTED: directed_edge_layers_.insert(layer); break;
			}
		}

		static bool resize_layer(std::vector<Cow_ptr<YGDL_Layer_impl>*>& layer, YGDL_Index size, YGDL_Error* out_error)
		{
			if(std::none_of(layer.begin(), layer.end(), [=](auto& l) { return l->get_const().length < size; })) {
				return true;
			}

			return modify(layer, out_error, [=](auto& l) { l.resize(size); });
		}
	};

} // namespace yggdrasill::detail
