/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#ifndef YGDL_INCLUDE_GUARD_MESH
#define YGDL_INCLUDE_GUARD_MESH

#include <yggdrasill/commons.h>
#include <yggdrasill/dictionary.h>
#include <yggdrasill/error.h>

#include <assert.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
	/// Constant used as an :cpp:member:`YGDL_Vertex::index` to mark invalid or missing vertices
	#define YGDL_NO_VERTEX YGDL_Vertex{static_cast<int32_t>(~static_cast<uint32_t>(0u))}
	/// Constant used as an :cpp:member:`YGDL_Face::index` to mark invalid or missing faces
	#define YGDL_NO_FACE YGDL_Face{static_cast<int32_t>(~static_cast<uint32_t>(0u))}
	/// Constant used as an :cpp:member:`YGDL_Edge::mask` to mark invalid or missing edges
	#define YGDL_NO_EDGE YGDL_Edge{~static_cast<uint32_t>(0u)}

	/// Bit-Mask for the part of the :cpp:member:`YGDL_Edge::mask` that indicates if's a primal or dual edge
	#define YGDL_EDGE_TYPE_BIT (static_cast<uint32_t>(1u) << 31u)
#else
	/// Constant used as an :cpp:member:`YGDL_Vertex::index` to mark invalid or missing vertices
	#define YGDL_NO_VERTEX (YGDL_Vertex) {~((uint32_t)0)}
	/// Constant used as an :cpp:member:`YGDL_Face::index` to mark invalid or missing faces
	#define YGDL_NO_FACE (YGDL_Face) {~((uint32_t)0)}
	/// Constant used as an :cpp:member:`YGDL_Edge::mask` to mark invalid or missing edges
	#define YGDL_NO_EDGE (YGDL_Edge) {~((uint32_t)0)}

	/// Constant used as an :cpp:member:`YGDL_Edge::mask` to mark invalid or missing edges
	#define YGDL_EDGE_TYPE_BIT (((uint32_t) 1u) << 31u)
#endif

	/**Handle to a vertex.\n
	 * This is only an index/handle. All data associated with the vertex is stored in the :cpp:any:`data layers <YGDL_Layer>` and the two `YGDL_Mesh` fields:
	 * :cpp:member:`YGDL_Mesh::vertex_edge` and :cpp:member:`YGDL_Mesh::used_vertices_bitset`.
	 * \attention The behaviour is undefined if an invalid vertex (either `YGDL_NO_VERTEX` or a vertex for which `YGDL_mesh_vertex_valid()` returns `YGDL_FALSE`)
	 * is passed to *any* function other than `YGDL_mesh_vertex_valid()`.
	 */
	typedef struct YGDL_Vertex {
		/// The index of this vertex. Used both to access its data in `YGDL_Mesh` and as an index into vertex-layers.
		/// \attention Both the mesh and the layers may contain holes, left by removed vertices. So, indices don't necessarily reference valid data.
		YGDL_Index index;
	} YGDL_Vertex;

	/**Handle to a face.\n
	 * This is only an index/handle. All data associated with the face is stored in the :cpp:any:`data layers <YGDL_Layer>` and the two `YGDL_Mesh` fields:
	 * :cpp:member:`YGDL_Mesh::face_edge` and :cpp:member:`YGDL_Mesh::used_faces_bitset`.
	 * \attention The behaviour is undefined if an invalid face (either `YGDL_NO_FACE` or a face for which `YGDL_mesh_face_valid()` returns `YGDL_FALSE`)
	 * is passed to *any* function other than `YGDL_mesh_face_valid()`.
	 */
	typedef struct YGDL_Face {
		/// The index of this face. Used both to access its data in `YGDL_Mesh` and as an index into face-layers.
		/// \attention Both the mesh and the layers may contain holes, left by removed faces. So, not all indices may reference valid data.
		YGDL_Index index;
	} YGDL_Face;

	/**Handle to an edge.\n
	 * This is only an index/handle. All data associated with the edge is stored in the :cpp:any:`data layers <YGDL_Layer>` and the following `YGDL_Mesh` fields:
	 * - :cpp:member:`YGDL_Mesh::used_edges_bitset`
	 * - :cpp:member:`YGDL_Mesh::primal_edge_origin`
	 * - :cpp:member:`YGDL_Mesh::primal_edge_next`
	 * - :cpp:member:`YGDL_Mesh::dual_edge_origin`
	 * - :cpp:member:`YGDL_Mesh::dual_edge_next`
	 * \note Contrary to vertices and faces, edges don't store their index directly. Use `YGDL_mesh_edge_index()` to retrieve the index for an edge, instead.
	 * \attention The behaviour is undefined if an invalid edge (either `YGDL_NO_EDGE` or an edge for which `YGDL_mesh_edge_valid()` returns `YGDL_FALSE`)
	 * is passed to *any* function other than `YGDL_mesh_edge_valid()`.
	 */
	typedef struct YGDL_Edge {
		/**The mask that stores both the edge's type (primal/dual) in its most-signification-bit and its index in the remaining bits.\n
		 * Since edges are always created in packets of four, the other parts of the quad-edge share the same value, except for the most- and
		 * least-signification-bit.
		 * \attention Both the mesh and the layers may contain holes, left by removed edges. So, not all indices may reference valid data.
		 */
		uint32_t mask; // MSB := 1 for dual, 0 for primal edge; LSB := 0/1 for opposite
	} YGDL_Edge;


	/**
	 * The description of all topological information for a `YGDL_World`, i.e. what vertices/faces/edges exist and how are they connected. Note however, that
	 * this explicitly doesn't include positional information.
	 *
	 * The mesh is owned by the `YGDL_World` that it was acquired from and subject to the copy-on-write semantic outlined there. So, the referenced storage
	 * might change when a mutable pointer is acquired through `YGDL_world_world_acquire`.
	 *
	 * The implementation is loosely based on `this paper <https://www.cs.cmu.edu/afs/andrew/scs/cs/15-463/2001/pub/src/a2/quadedge.html>`_, with some
	 * additional features and restricted to only support triangular faces.
	 *
	 * \attention All faces in the mesh have to use counter-clockwise winding-order.
	 *
	 * \tip Except for the number of existing elements, `vertex_edge` and `face_edge`, the members of this object are usually not accessed directly, but through the utility functions below.
	 *
	 * \attention Also this isn't an opaque handle and the members are non-const when a mutable pointer has been acquired, the members are managed by
	 * the system and may not be manually modified. The definition is only provided to enable efficient inline access operations.
	 * Thus, the behaviour is undefined if an instance of this type is manually created/copied/destroyed or any of its members is modified directly!
	 */
	typedef struct YGDL_Mesh {
		/// The current number of vertices, some of which may currently be unused (see `YGDL_mesh_vertex_valid()`).\n
		/// This is both the length of the `vertex_edge` array and can be used to iterate over all vertices.
		YGDL_Index vertex_count;
		/// The current number of faces, some of which may currently be unused (see `YGDL_mesh_face_valid()`).\n
		/// This is both the length of the `face_edge` array and can be used to iterate over all faces.
		YGDL_Index face_count;
		/// The **half** number of edges, i.e. the number of primal and dual edges separately, some of which may currently be unused (see `YGDL_mesh_edge_valid()`).\n
		/// This can be used to iterate over all edges and the length of the following arrays: `primal_edge_origin`, `primal_edge_next`, `dual_edge_origin`, `dual_edge_next`
		YGDL_Index edge_count;

		/// The number of currently unused vertices, i.e. the length of the array pointed to by `unused_vertex_list`.
		YGDL_Index unused_vertex_count;
		/// The number of currently unused faces, i.e. the length of the array pointed to by `unused_face_list`.
		YGDL_Index unused_face_count;
		/// The number of currently unused edges (only counting one edge per quad-edge), i.e. the length of the array pointed to by `unused_edge_list`.
		YGDL_Index unused_edge_count;

		/**Array of length `vertex_count`, containing one of the out-going primal edges for each vertex.\n
		 * For vertices that aren't currently part of any face or edge, `YGDL_NO_EDGE` is stored as a marker.
		 * \attention This pointer may be invalidated by any modification operation, i.e. any function below that takes a non-const `YGDL_Mesh`.
		 */
		const YGDL_Edge* vertex_edge;
		/// Array of length `face_count`, containing one of the three primal edges that rotate counter-clockwise around it, for each face.
		/// \attention This pointer may be invalidated by any modification operation, i.e. any function below that takes a non-const `YGDL_Mesh`.
		const YGDL_Edge* face_edge;

		/// Array of length `edge_count`, containing the origin vertex for each primal edge.
		/// \attention This pointer may be invalidated by any modification operation, i.e. any function below that takes a non-const `YGDL_Mesh`.
		const YGDL_Vertex* primal_edge_origin;
		/// Array of length `edge_count`, containing the next (CCW) edge of their origin-edge-ring for each primal edge.
		/// \attention This pointer may be invalidated by any modification operation, i.e. any function below that takes a non-const `YGDL_Mesh`.
		const YGDL_Edge*   primal_edge_next;

		/// Array of length `edge_count`, containing the origin face for each dual edge.
		/// \attention This pointer may be invalidated by any modification operation, i.e. any function below that takes a non-const `YGDL_Mesh`.
		const YGDL_Face*   dual_edge_origin;
		/// Array of length `edge_count`, containing the next (CCW) edge of their origin-edge-ring for each dual edge.
		/// \attention This pointer may be invalidated by any modification operation, i.e. any function below that takes a non-const `YGDL_Mesh`.
		const YGDL_Edge*   dual_edge_next;

		/**Array of length `unused_vertex_count`, that contains all currently unused vertices, in descending order by their index.
		 * \note To allow for efficient validation checks during iterations, the values are sorted by their index and a `NO_VERTEX` sentinel value is stored at index ``-1``. 
		 * \attention This pointer may be invalidated by any modification operation, i.e. any function below that takes a non-const `YGDL_Mesh`.
		 */
		const YGDL_Vertex* unused_vertex_list;
		/**Array of length `unused_face_count`, that contains all currently unused vertices, in descending order by their index.
		 * \note To allow for efficient validation checks during iterations, the values are sorted by their index and a `NO_VERTEX` sentinel value is stored at index ``-1``. 
		 * \attention This pointer may be invalidated by any modification operation, i.e. any function below that takes a non-const `YGDL_Mesh`.
		 */
		const YGDL_Face* unused_face_list;
		/**Array of length `unused_edge_count`, that contains all currently unused vertices, in descending order by their index.
		 * \note To allow for efficient validation checks during iterations, the values are sorted by their index and a `NO_VERTEX` sentinel value is stored at index ``-1``.\n
		 * \note Since the lifetime of quad-edges and their individual edges are identical, only one edge per quad-edge is stored in this list. Which is the edge returned by `YGDL_mesh_edge_base()`
		 * \attention This pointer may be invalidated by any modification operation, i.e. any function below that takes a non-const `YGDL_Mesh`.
		 */
		const YGDL_Edge* unused_edge_list;
	} YGDL_Mesh;


	/**\returns The index used to access this edges data, both in `YGDL_Mesh` and the data layers.
	 * \note Some layers and arrays may use _undirected_ edges (e.g. `YGDL_LAYER_REF_TYPE_EDGE_PRIMAL` and :cpp:member:`YGDL_Mesh::used_edges_bitset`) and
	 * may thus require dividing the return value by two.
	 */
	YGDL_CONSTEXPR inline YGDL_Index YGDL_mesh_edge_index(YGDL_Edge e) {
		return e.mask & ~YGDL_EDGE_TYPE_BIT;
	}
	/// \returns Whether the given edge is a dual edge (`YGDL_TRUE`) or primal edge (`YGDL_FALSE`)
	YGDL_CONSTEXPR inline YGDL_Bool YGDL_mesh_edge_is_dual(YGDL_Edge e) {
		return e.mask>>31u ? YGDL_TRUE : YGDL_FALSE;
	}
	/**Combines the given edge-type and index back into an edge handle.
	 * \param dual Whether the edge is a dual edge. Value as returned by `YGDL_mesh_edge_is_dual()`
	 * \param index The index of the edge, as returned by `YGDL_mesh_edge_index()`
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_from_index(YGDL_Bool dual, YGDL_Index index) {
#ifdef __cplusplus
		YGDL_Edge edge = {static_cast<uint32_t>(index)};
#else
		YGDL_Edge edge;
		edge.mask = index;
#endif
		if(dual) {
			edge.mask = edge.mask | YGDL_EDGE_TYPE_BIT;
		}
		return edge;
	}


	/// \returns `YGDL_TRUE` if the given vertex is valid and currently used by the mesh, or `YGDL_FALSE` otherwise.
	/// \note Checks both if the vertex is out of range (or `YGDL_NO_VERTEX`) and if its currently marked as unused.
	YGDL_CONSTEXPR inline YGDL_Bool YGDL_mesh_vertex_valid(const YGDL_Mesh* mesh, YGDL_Vertex v) {
#ifdef __cplusplus
		if(static_cast<uint32_t>(v.index) >= static_cast<uint32_t>(mesh->vertex_count))
			return YGDL_FALSE;
#else
		if(((uint32_t)v.index) >= ((uint32_t)mesh->vertex_count))
			return YGDL_FALSE;
#endif
		return mesh->vertex_edge[v.index].mask != YGDL_NO_EDGE.mask;
	}
	/// \returns `YGDL_TRUE` if the given face is valid and currently used by the mesh, or `YGDL_FALSE` otherwise.
	/// \note Checks both if the face is out of range (or `YGDL_NO_FACE`) and if its currently marked as unused.
	YGDL_CONSTEXPR inline YGDL_Bool YGDL_mesh_face_valid(const YGDL_Mesh* mesh, YGDL_Face f) {
#ifdef __cplusplus
		if(static_cast<uint32_t>(f.index) >= static_cast<uint32_t>(mesh->face_count))
			return YGDL_FALSE;
#else
		if(((uint32_t)f.index) >= ((uint32_t)mesh->face_count))
			return YGDL_FALSE;
#endif
		return mesh->face_edge[f.index].mask != YGDL_NO_EDGE.mask;
	}
	/// \returns `YGDL_TRUE` if the given edge is valid and currently used by the mesh, or `YGDL_FALSE` otherwise.
	/// \note Checks both if the edge is out of range (or `YGDL_NO_EDGE`) and if its currently marked as unused.
	YGDL_CONSTEXPR inline YGDL_Bool YGDL_mesh_edge_valid(const YGDL_Mesh* mesh, YGDL_Edge e) {
		YGDL_Index index = YGDL_mesh_edge_index(e);
		if(index >= mesh->edge_count)
			return YGDL_FALSE;
		return mesh->primal_edge_next[index].mask != YGDL_NO_EDGE.mask;
	}


	/**\returns The symmetric edge for the given edge `e`, i.e. the edge from the same quad-edge that points in the opposite direction (swapped origin/dest).
	 * \note The type (dual/primal) of the returned edge is the same as the inputs.
	 * \pre The passed edge `e` has to be a valid edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_sym(YGDL_Edge e) {
		YGDL_Edge r;
		r.mask = e.mask ^ 1u;
		YGDL_ASSUME(YGDL_mesh_edge_is_dual(r) == YGDL_mesh_edge_is_dual(e));
		return r;
	}
	/**\returns The "first" edge of the given primal or dual edge, which will always be a primal edge with an even index.
	 * \note This is both usefully to identify the quad-edge itself and to implement functions that don't need to distinguish between different parts of the
	 * quad-edge, like `YGDL_mesh_edge_flip`.
	 * \pre The passed edge `e` has to be a valid edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_base(YGDL_Edge e) {
		YGDL_Edge r;
		r.mask = e.mask & ~1u & ~YGDL_EDGE_TYPE_BIT;
		YGDL_ASSUME(YGDL_mesh_edge_is_dual(r) == YGDL_FALSE);
		return r;
	}
	/**\returns The counter-clockwise rotation of the given edge `e`, i.e. the next edge in the quad-edge.
	 * \note The type (dual/primal) of the returned edge is always the opposite of the inputs.
	 * \pre The passed edge `e` has to be a valid edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_rot(YGDL_Edge e) {
		YGDL_Edge r;
		r.mask = (e.mask ^ YGDL_EDGE_TYPE_BIT) ^ (e.mask>>31u);
		YGDL_ASSUME(YGDL_mesh_edge_is_dual(r) != YGDL_mesh_edge_is_dual(e));
		return r;
	}
	/**\returns The clockwise rotation of the given edge `e`, i.e. the previous edge in the quad-edge.
	 * \note The type (dual/primal) of the returned edge is always the opposite of the inputs.
	 * \pre The passed edge `e` has to be a valid edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_inv_rot(YGDL_Edge e) {
		YGDL_Edge r;
		r.mask = (e.mask ^ YGDL_EDGE_TYPE_BIT) ^ ((e.mask>>31u) ^ 1u);
		YGDL_ASSUME(YGDL_mesh_edge_is_dual(r) != YGDL_mesh_edge_is_dual(e));
		return r;
	}


	/**\returns The next edge from `e`, when rotating counter-clockwise around the **origin edge-ring**. This might be `e` again, if the edge-ring is otherwise
	 * empty, but will always be a valid edge.
	 * \note This function can be used to iterate counter-clockwise over all edges that have the same origin vertex/face.
	 * \pre The passed edge `e` has to be a valid edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_origin_next(const YGDL_Mesh* mesh, YGDL_Edge e) {
		YGDL_Index index = YGDL_mesh_edge_index(e);
		assert(index<mesh->edge_count);
		const YGDL_Edge* array = (YGDL_mesh_edge_is_dual(e) ? mesh->dual_edge_next : mesh->primal_edge_next);
		const YGDL_Edge r = array[index];
		YGDL_ASSUME(YGDL_mesh_edge_is_dual(r) == YGDL_mesh_edge_is_dual(e));
		return r;
	}
	/**\returns The previous edge from `e`, when rotating counter-clockwise around the **origin edge-ring**. This might be `e` again, if the edge-ring is otherwise
	 * empty, but will always be a valid edge.
	 * \note This function can be used to iterate clockwise over all edges that have the same origin vertex/face.
	 * \pre The passed edge `e` has to be a valid edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_origin_prev(const YGDL_Mesh* mesh, YGDL_Edge e) {
		return YGDL_mesh_edge_rot(YGDL_mesh_edge_origin_next(mesh, YGDL_mesh_edge_rot(e)));
	}
	/**\returns The next edge from `e`, when rotating counter-clockwise around the **destination edge-ring**. This might be `e` again, if the edge-ring is otherwise
	 * empty, but will always be a valid edge.
	 * \note This function can be used to iterate counter-clockwise over all edges that have the same destination vertex/face.
	 * \pre The passed edge `e` has to be a valid edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_destination_next(const YGDL_Mesh* mesh, YGDL_Edge e) {
		return YGDL_mesh_edge_sym(YGDL_mesh_edge_origin_next(mesh, YGDL_mesh_edge_sym(e)));
	}
	/**\returns The previous edge from `e`, when rotating counter-clockwise around the **destination edge-ring**. This might be `e` again, if the edge-ring is otherwise
	 * empty, but will always be a valid edge.
	 * \note This function can be used to iterate clockwise over all edges that have the same destination vertex/face.
	 * \pre The passed edge `e` has to be a valid edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_destination_prev(const YGDL_Mesh* mesh, YGDL_Edge e) {
		return YGDL_mesh_edge_inv_rot(YGDL_mesh_edge_origin_next(mesh, YGDL_mesh_edge_inv_rot(e)));
	}
	/**\returns The next edge from `e`, when rotating counter-clockwise around the **left face**. This will always be a valid edge other than `e`.
	 * \note This function can be used to iterate counter-clockwise over all edges that have the same vertex/face on their left.
	 * \pre The passed edge `e` has to be a valid edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_left_next(const YGDL_Mesh* mesh, YGDL_Edge e) {
		return YGDL_mesh_edge_rot(YGDL_mesh_edge_origin_next(mesh, YGDL_mesh_edge_inv_rot(e)));
	}
	/**\returns The previous edge from `e`, when rotating counter-clockwise around the **left face**. This will always be a valid edge other than `e`.
	 * \note This function can be used to iterate clockwise over all edges that have the same vertex/face on their left.
	 * \pre The passed edge `e` has to be a valid edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_left_prev(const YGDL_Mesh* mesh, YGDL_Edge e) {
		return YGDL_mesh_edge_sym(YGDL_mesh_edge_origin_next(mesh, e));
	}
	/**\returns The next edge from `e`, when rotating counter-clockwise around the **right face**. This will always be a valid edge other than `e`.
	 * \note This function can be used to iterate counter-clockwise over all edges that have the same vertex/face on their right.
	 * \pre The passed edge `e` has to be a valid edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_right_next(const YGDL_Mesh* mesh, YGDL_Edge e) {
		return YGDL_mesh_edge_inv_rot(YGDL_mesh_edge_origin_next(mesh, YGDL_mesh_edge_rot(e)));
	}
	/**\returns The previous edge from `e`, when rotating counter-clockwise around the **right face**. This will always be a valid edge other than `e`.
	 * \note This function can be used to iterate clockwise over all edges that have the same vertex/face on their right.
	 * \pre The passed edge `e` has to be a valid edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Edge YGDL_mesh_edge_right_prev(const YGDL_Mesh* mesh, YGDL_Edge e) {
		return YGDL_mesh_edge_origin_next(mesh, YGDL_mesh_edge_sym(e));
	}


	/**\returns The origin vertex for the given primal edge. This will always return a valid edge, since every primal edge has at least an origin and
	 * destination vertex.
	 * \pre The passed edge `e` has to be a valid primal edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Vertex YGDL_mesh_edge_primal_origin(const YGDL_Mesh* mesh, YGDL_Edge e) {
		assert(!YGDL_mesh_edge_is_dual(e));
#ifdef __cplusplus
		assert(static_cast<YGDL_Index>(e.mask) < mesh->edge_count);
#else
		assert((YGDL_Index) e.mask < mesh->edge_count);
#endif
		return mesh->primal_edge_origin[e.mask];
	}
	/**\returns The destination vertex for the given primal edge. This will always return a valid edge, since every primal edge has at least an origin and
	 * destination vertex.
	 * \pre The passed edge `e` has to be a valid primal edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Vertex YGDL_mesh_edge_primal_destination(const YGDL_Mesh* mesh, YGDL_Edge e) {
		return YGDL_mesh_edge_primal_origin(mesh, YGDL_mesh_edge_sym(e));
	}
	/// \returns The left face for the given primal edge *or* `YGDL_NO_FACE` if the edge has no such face.
	/// \pre The passed edge `e` has to be a valid primal edge.
	YGDL_CONSTEXPR inline YGDL_Face YGDL_mesh_edge_primal_left(const YGDL_Mesh* mesh, YGDL_Edge e) {
		assert(!YGDL_mesh_edge_is_dual(e));
		YGDL_Index index = YGDL_mesh_edge_index(YGDL_mesh_edge_inv_rot(e));
		assert(index < mesh->edge_count);
		return mesh->dual_edge_origin[index];
	}
	/// \returns The right face for the given primal edge *or* `YGDL_NO_FACE` if the edge has no such face.
	/// \pre The passed edge `e` has to be a valid primal edge.
	YGDL_CONSTEXPR inline YGDL_Face YGDL_mesh_edge_primal_right(const YGDL_Mesh* mesh, YGDL_Edge e) {
		assert(!YGDL_mesh_edge_is_dual(e));
		YGDL_Index index = YGDL_mesh_edge_index(YGDL_mesh_edge_rot(e));
		assert(index < mesh->edge_count);
		return mesh->dual_edge_origin[index];
	}

	/// \returns The origin face for the given dual edge *or* `YGDL_NO_FACE` if the edge has no such face.
	/// \pre The passed edge `e` has to be a valid dual edge.
	YGDL_CONSTEXPR inline YGDL_Face YGDL_mesh_edge_dual_origin(const YGDL_Mesh* mesh, YGDL_Edge e) {
		assert(YGDL_mesh_edge_is_dual(e));
		YGDL_Index index = YGDL_mesh_edge_index(e);
		assert(index < mesh->edge_count);
		return mesh->dual_edge_origin[index];
	}
	/// \returns The destination face for the given dual edge *or* `YGDL_NO_FACE` if the edge has no such face.
	/// \pre The passed edge `e` has to be a valid dual edge.
	YGDL_CONSTEXPR inline YGDL_Face YGDL_mesh_edge_dual_destination(const YGDL_Mesh* mesh, YGDL_Edge e) {
		return YGDL_mesh_edge_dual_origin(mesh, YGDL_mesh_edge_sym(e));
	}
	/**\returns The left vertex for the given dual edge. This will always return a valid edge, since every primal edge has at least an origin and
	 * destination vertex.
	 * \pre The passed edge `e` has to be a valid dual edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Vertex YGDL_mesh_edge_dual_left(const YGDL_Mesh* mesh, YGDL_Edge e) {
		return YGDL_mesh_edge_primal_origin(mesh, YGDL_mesh_edge_inv_rot(e));
	}
	/**\returns The right vertex for the given dual edge. This will always return a valid edge, since every primal edge has at least an origin and
	 * destination vertex.
	 * \pre The passed edge `e` has to be a valid dual edge.
	 */
	YGDL_CONSTEXPR inline YGDL_Vertex YGDL_mesh_edge_dual_right(const YGDL_Mesh* mesh, YGDL_Edge e) {
		return YGDL_mesh_edge_primal_origin(mesh, YGDL_mesh_edge_rot(e));
	}


	// write access to mesh data
	/**Creates `count` new vertices in the mesh, resizing any `YGDL_LAYER_REF_TYPE_VERTEX` layers accordingly.
	 * \returns `YGDL_TRUE` if the operation was successful.
	 * \param out If not NULL, this point has to reference a valid array of at least `count` elements, to which the handles of the newly created vertices
	 * will be written.
	 * \pre `out` is either NULL or points to an array at least `count` many `YGDL_Vertex` objects.
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the mesh couldn't acquire mutable pointers to the
	 * vertex based data layers, to resize them. This is usually caused by a race-condition.
	 * \attention This function will call `YGDL_world_layer_acquire()` on the associated `YGDL_World` to update the data layers, with all consequences stated there.
	 */
	YGDL_EXPORTED YGDL_Bool YGDL_mesh_vertex_add   (YGDL_Mesh*, YGDL_Index count, YGDL_Vertex* out, YGDL_Error* out_error);
	
	/// Removes the given vertex, if it's currently not used by any edges.
	/// \returns `true` if the vertex has been removed
	YGDL_EXPORTED YGDL_Bool YGDL_mesh_vertex_remove(YGDL_Mesh*, YGDL_Vertex out);
	
	/**Creates a new triangle face ABC between the three vertices.
	 * \returns The newly created face, if the operation was successful.
	 * \pre The vertices `v_a`, `v_b` and `v_c` have to be valid.
	 * \throw YGDL_ERROR_CODE_MESH_CORRUPTION Sets `out_error` (if not NULL) to this code, if adding the face would violate one of the meshes invariants,
	 * or if the mesh is already in an invalid state. This is generally caused by breaking one of the following conditions:\n
	 *   - There must be no pre-existing face connecting these three vertices.
	 *   - The winding-order of the passed vertices has to be consistent with all its neighbors.
	 *   - If one of the vertices already has two faces, which aren't connected to each other, the new face has to share an edge with at least one of them.
	 * 
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the mesh couldn't acquire mutable pointers to the
	 * vertex based data layers, to resize them. This is usually caused by a race-condition.
	 * \attention This function will call `YGDL_world_layer_acquire()` on the associated `YGDL_World` to update the data layers, with all consequences stated there.
	 */
	YGDL_EXPORTED YGDL_Face   YGDL_mesh_face_add     (YGDL_Mesh*, YGDL_Vertex v_a, YGDL_Vertex v_b, YGDL_Vertex v_c, YGDL_Error* out_error);

	/**Flips the given edge, i.e. rotating it inside the quad formed by merging the two triangles on its left and right side.
	 * \returns `YGDL_TRUE` if the operation was successful. If `YGDL_FALSE` is returned, see `out_error` for additional details about the cause of the failure.
	 * \pre The given edge is a valid primal or dual edge and has both a left and right face.
	 * \throw YGDL_ERROR_CODE_MESH_CORRUPTION Sets `out_error` (if not NULL) to this code, if the precondition is violated, which would cause a corruption of the mesh, or if the mesh is already in an invalid state.
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the mesh couldn't acquire mutable pointers to the
	 * data layers, to update them. This is usually caused by a race-condition.
	 * \attention This function will call `YGDL_world_layer_acquire()` on the associated `YGDL_World` to update the data layers, with all consequences stated there.
	 * \note While calling this function during traversal operations is generally safe, as long as the potentially changed pointers in `YGDL_Mesh` are handled correctly, since this operation changes the topology, might behave in unexpected ways.\n
	 * \note Invalidates all data layer values (according to their `YGDL_Layer_invalidation` setting) for: the edge, the other parts of the quad-edge, its
	 * left and right face and all dual-edges that are connected to these faces.
	 */
	YGDL_EXPORTED YGDL_Bool   YGDL_mesh_edge_flip    (YGDL_Mesh*, YGDL_Edge, YGDL_Error* out_error);

	/**Splits the given edge, inserting a new vertex, up to two faces and up to three quad-edges.
	 * \param split_point The weight that is used to interpolate the data for the new vertex, where ``0.0`` stand for the origin and ``1.0`` for the destination
	 * of the original edge.
	 * \returns The newly created primal edge between the new vertex and the original destination, if the operation was successful. Or `YGDL_NO_EDGE` on a failure.
	 * \pre The given edge is a valid primal edge and has at least one face.
	 * \throw YGDL_ERROR_CODE_MESH_CORRUPTION Sets `out_error` (if not NULL) to this code, if the precondition is violated, which would cause a corruption of the mesh, or if the mesh is already in an invalid state.
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the mesh couldn't acquire mutable pointers to the
	 * data layers, to update them. This is usually caused by a race-condition.
	 * \attention This function will call `YGDL_world_layer_acquire()` on the associated `YGDL_World` to update the data layers, with all consequences stated there.
	 * \note While calling this function during traversal operations is generally safe, as long as the potentially changed pointers in `YGDL_Mesh` are handled correctly, since this operation changes the topology, might behave in unexpected ways.\n
	 * \note Invalidates all data layer values (according to their `YGDL_Layer_invalidation` setting) for: the passed edge and all dual edges that had to be
	 * updated to insert the new faces.\n
	 * \note Evaluates the data layers' `YGDL_Layer_interpolation` for: the new vertex, the new faces (with both origin and dest identical to either the
	 * original left or right face) and the primal and dual edges that had to be newly inserted.
	 */
	YGDL_EXPORTED YGDL_Edge   YGDL_mesh_edge_split   (YGDL_Mesh*, YGDL_Edge, float split_point, YGDL_Error* out_error);

	/**Collapses the given edge, removing its destination vertex and updating the data associated with its origin vertex to move it according to `join_point`.
	 * \param join_point The weight that is used to interpolate the data for the moved origin vertex, where ``0.0`` stands for its old value and ``1.0`` for
	 * the one from the edge's destination.
	 * \returns The remaining vertex (origin) of the passed edge, if the operation was successful. Or `YGDL_NO_VERTEX` on a failure.
	 * \pre The given edge is a valid primal edge with at least one face and satisfies the link condition.
	 * \throw YGDL_ERROR_CODE_MESH_CORRUPTION Sets `out_error` (if not NULL) to this code, if the precondition is violated, which would cause a corruption of the mesh, or if the mesh is already in an invalid state.
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the mesh couldn't acquire mutable pointers to the
	 * data layers, to update them. This is usually caused by a race-condition.
	 * \attention This function will call `YGDL_world_layer_acquire()` on the associated `YGDL_World` to update the data layers, with all consequences stated there.
	 * \note While calling this function during traversal operations is generally safe, as long as the potentially changed pointers in `YGDL_Mesh` are handled correctly, since this operation changes the topology, might behave in unexpected ways.
	 * \attention This function invalidates the passed edge, its destination vertex and both its faces, which might have to be considered during traversal.\n
	 * \note Invalidates all data layer values (according to their `YGDL_Layer_invalidation` setting) for: the passed edge, its destination vertex,
	 * the faces on either side of the edge, as well as all primal and dual edges that are either part of the deleted faces or of the edge-ring around the
	 * deleted vertex.
	 * \note Evaluates the data layers' `YGDL_Layer_interpolation` for the origin vertex of the passed edge, i.e. the vertex that was kept and is returned.
	 */
	YGDL_EXPORTED YGDL_Vertex YGDL_mesh_edge_collapse(YGDL_Mesh*, YGDL_Edge, float join_point, YGDL_Error* out_error);
	
#ifdef __cplusplus
}
#endif
#endif
