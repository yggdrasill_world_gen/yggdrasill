/**********************************************************************************
* Copyright (c) 2023 Florian Oetke                                               *
* This file is part of the Yggdrasill project and licensed under the MIT License *
* See LICENSE file for details.                                                  *
\**********************************************************************************/
#ifndef YGDL_INCLUDE_GUARD_SERIALIZATION
#define YGDL_INCLUDE_GUARD_SERIALIZATION

#include <yggdrasill/commons.h>
#include <yggdrasill/dictionary.h>
#include <yggdrasill/world.h>


#ifdef __cplusplus
extern "C" {
#endif
	
	/// \returns `YGDL_TRUE` if the library was compiled with serialization support.
	/// Support is usually included, but can be explicitly disabled by setting the CMake variable ``YGGDRASILL_SERIALIZATION`` to ``OFF``.
	/// \attention If this function returns false, no other functions in this header may be called.
	YGDL_EXPORTED YGDL_Bool YGDL_serialization_supported(void);


		
	/// An opaque handle to a de-/serializer that can be used to persist and load simulation states, configurations and other arbitrary data.
	typedef struct YGDL_Serializer YGDL_Serializer;

	/**Creates a new serializer instance based on a file in the local filesystem and returns a pointer to it.\n
	 * The file is opened either read-only or read-writable if `writeable` is `YGDL_TRUE`.\n 
	 * If the file is opened for writing, it will be created if it doesn't already exist; otherwise an error is generated.
	 * 
	 * \param path The path to an existing archive file or the path where a new one should be created.
	 * \param writeable Whether the file should be writable or read-only.
	 * \param overwrite If both this and `writeable` are `YGDL_TRUE` any existing file will be deleted first, to remove any preexisting data.
	 * \throw YGDL_ERROR_CODE_IO_ERROR Sets `out_error` (if not NULL) to this code, if the file couldn't be opened (e.g. because it doesn't exist and was 
	 * opened read-only or the user lacks the necessary permissions) or isn't a valid archive.
	 * 
	 * \note Each type of object stored uses a different namespace for keys. Thus, for example, user-data and a configuration may share the same key without causing conflicts.
	 * \attention Writing data to the archive doesn't immediately update its header, which leaves it in an invalid state. To actually create a readable file,
	 * either `YGDL_serializer_destroy` or `YGDL_serializer_flush` have to be called afterward.
	 * \attention The returned object has to be freed using `YGDL_serializer_destroy` once it's no longer used.
	 */
	YGDL_EXPORTED YGDL_Serializer* YGDL_serializer_create(YGDL_String_view path, YGDL_Bool writeable, YGDL_Bool overwrite, YGDL_Error* out_error);

	/**Destroys the given serializer.\n
	 * The parameter has to be a non-null pointer to a `YGDL_Serializer` created by one of the functions above.\n
	 * \attention The behavior is undefined if the passed serializer is used afterward.
	 */
	YGDL_EXPORTED void YGDL_serializer_destroy(YGDL_Serializer*);
	
	/**Writes all buffered data and the archive header into the underlying file, so that it becomes a valid archive file, by closing and reopening it.
	 * \throw YGDL_ERROR_CODE_IO_ERROR Sets `out_error` (if not NULL) to this code, if the file couldn't be reopened (e.g. because it doesn't exist and was 
	 * opened read-only or the user lacks the necessary permissions) or isn't a valid archive.
	 */
	YGDL_EXPORTED void YGDL_serializer_flush(YGDL_Serializer* serializer, YGDL_Error* out_error);

	/// Removes entries from the internal read-caches, that aren't used by any external objects.\n
	/// The system keeps copy-on-write pointer for the individual parts of worlds read with `YGDL_serializer_world_read()`, to reuse them for later read-requests.
	YGDL_EXPORTED void YGDL_serializer_shrink_read_caches(YGDL_Serializer* serializer);


	/**Writes a user-defined byte-sequence to the archive using the given `key` to identify it.
	 * \param key The string used to identify and later retrieve the data.
	 * \param bytes A pointer to the first element of a byte array.
	 * \param byte_count The number of bytes in the array pointed to by `bytes` that should be written.
	 * \return `YGDL_TRUE` if the operation was successful.
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the serializer was opened read-only or an entry with this name already exists.
	 * \throw YGDL_ERROR_CODE_IO_ERROR Sets `out_error` (if not NULL) to this code, if the entry couldn't be written.
	 * \attention Individual entries currently can't be overwritten or deleted.
	 */
	YGDL_EXPORTED YGDL_Bool YGDL_serializer_userdata_write(YGDL_Serializer*, YGDL_String_view key, const void* bytes, size_t byte_count, YGDL_Error* out_error);

	/**\skip (missing Sphinx support for function pointers)
	 * Type of the callback returned to the user in `YGDL_serializer_userdata_read()` to read a user-data entry.
	 * \param reader_ctx The `reader_ctx` parameter passed into `YGDL_Serializer_reader`.
	 * \param bytes_to_read The number of bytes that should be read into `buffer`. If the parameter is zero, no bytes are read and the number of remaining bytes is returned.
	 * \param buffer Pointer to a memory segment with at least `bytes_to_read` many bytes of space.
	 * \returns Either the number of bytes actually read or the number of remaining bytes in the entry if `bytes_to_read` was zero.
	 * \note The number of actually read bytes may be smaller than requested.
	 */
	typedef size_t(YGDL_Serializer_reader_req)(void* reader_ctx, size_t bytes_to_read, void* buffer);

	/**\skip (missing Sphinx support for function pointers)
	 * Type of the callback passed to `YGDL_serializer_userdata_read()` to read the bytes of a user-data entry.
	 * \param ctx The unchanged `ctx` parameter passed to `YGDL_serializer_userdata_read()`.
	 * \param reader_ctx An opaque context object, that has to be passed to `read()`.
	 * \param read A callable that can be called multiple times to read subsequent bytes from the entry.
	 */
	typedef void(YGDL_Serializer_reader)(void* ctx, void* reader_ctx, YGDL_Serializer_reader_req* read);

	/**Reads the user-data entry identified by the given `key` from the archive.\n
	 * If the entry exists and can be opened, the given `reader` will be called exactly once, with the passed `ctx` parameter and a callback that can be used 
	 * to determine the number of remaining bytes and read bytes from the entry.
	 * 
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if no such entry exists in the archive.
	 * \throw YGDL_ERROR_CODE_IO_ERROR Sets `out_error` (if not NULL) to this code, if the entry couldn't be accessed or read.
	 */
	YGDL_EXPORTED YGDL_Bool YGDL_serializer_userdata_read(YGDL_Serializer*, YGDL_String_view key, void* ctx, YGDL_Serializer_reader* reader, YGDL_Error* out_error);

	/// The type of the callback passed to `YGDL_serializer_userdata_iterate()`.
	typedef void(YGDL_Serializer_userdata_callback)(void* ctx, YGDL_String_view key);
	/// Calls the given `callback` once for every user-data entry in the archive, with the given `ctx` and their keys.
	YGDL_EXPORTED void YGDL_serializer_userdata_iterate(const YGDL_Serializer*, void* ctx, YGDL_Serializer_userdata_callback* callback);
	


	/**Writes the given `YGDL_Dict` to the archive, using the given `key` to identify it.
	 * \return `YGDL_TRUE` if the operation was successful.
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the serializer was opened read-only or an entry with this name already exists.
	 * \throw YGDL_ERROR_CODE_IO_ERROR Sets `out_error` (if not NULL) to this code, if the entry couldn't be written.
	 * \attention Individual entries currently can't be overwritten or deleted.
	 */
	YGDL_EXPORTED YGDL_Bool YGDL_serializer_config_write(YGDL_Serializer*, YGDL_String_view key, const YGDL_Dict*, YGDL_Error* out_error);

	/**Reads the `YGDL_Dict` identified by the given `key` from the archive and writes it into the object pointed to by `config`.
	 * \return `YGDL_TRUE` if the operation was successful.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if no such entry exists in the archive.
	 * \throw YGDL_ERROR_CODE_IO_ERROR Sets `out_error` (if not NULL) to this code, if the entry couldn't be accessed or read, e.g. because it didn't match the expected format.
	 * \note The passed `YGDL_Dict` will be cleared, so none of its previous state survives this operation, but its storage might be reused.
	 * \attention If the operation fails, `config` may have been partially updated and will be in an unspecified but valid state.
	 */
	YGDL_EXPORTED YGDL_Bool YGDL_serializer_config_read(YGDL_Serializer*, YGDL_String_view key, YGDL_Dict* config, YGDL_Error* out_error);

	/// The type of the callback passed to `YGDL_serializer_config_iterate()`.
	typedef void(YGDL_Serializer_config_callback)(void* ctx, YGDL_String_view key);
	/// Calls the given `callback` once for every configuration entry in the archive, with the given `ctx` and their keys.
	YGDL_EXPORTED void YGDL_serializer_config_iterate(const YGDL_Serializer*, void* ctx, YGDL_Serializer_config_callback* callback);


	/// A handle used to identify a world stored in the `YGDL_Serializer` archive.
	/// \danger Handles from different `YGDL_Serializer` instances or archives are *not* comparable nor compatible!
	typedef int32_t YGDL_Serializer_world_ref;

	/**Writes the given `YGDL_World` to the archive.\n
	 * In contrast to the other functions, a unique handle for the passed world will automatically be generated and returned.
	 * \return A handle that can be used to later reference and retrieve the written world (e.g. by writing it into a config entry), or ``0`` if the operation failed.
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the serializer was opened read-only or an entry with this name already exists.
	 * \throw YGDL_ERROR_CODE_IO_ERROR Sets `out_error` (if not NULL) to this code, if the entry couldn't be written.
	 * \note The same `YGDL_World` object may be written multiple times. Each call will generate a new distinct entry with its own unique handle.
	 * \attention Individual entries currently can't be overwritten or deleted.
	 */
	YGDL_EXPORTED YGDL_Serializer_world_ref YGDL_serializer_world_write(YGDL_Serializer*, const YGDL_World*, YGDL_Error* out_error);
	
	/**Reads the `YGDL_World` identified by the given `handle` from the archive and writes it into the object pointed to by `world`.
	 * \return `YGDL_TRUE` if the operation was successful.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if no such entry exists in the archive.
	 * \throw YGDL_ERROR_CODE_IO_ERROR Sets `out_error` (if not NULL) to this code, if the entry couldn't be accessed or read, e.g. because it didn't match the expected format.
	 * \note The passed `YGDL_World` will be cleared, so none of its previous state survives this operation, but its storage might be reused.
	 * \note The serializer keeps a cache of the individual parts (mesh, layers, ...) of previously loaded worlds and reuses them.
	 * \attention If the operation fails, `world` may have been partially updated and will be in an unspecified but valid state.
	 */
	YGDL_EXPORTED YGDL_Bool YGDL_serializer_world_read(YGDL_Serializer*, YGDL_Serializer_world_ref handle, YGDL_World* world, YGDL_Error* out_error);

	/// The type of the callback passed to `YGDL_serializer_world_iterate()`.
	typedef void(YGDL_Serializer_world_callback)(void* ctx, YGDL_Serializer_world_ref);
	/// Calls the given `callback` once for every world entry in the archive, with the given `ctx` and their handles.
	YGDL_EXPORTED void YGDL_serializer_world_iterate(const YGDL_Serializer*, void* ctx, YGDL_Serializer_world_callback* callback);

#ifdef __cplusplus
}
#endif
#endif
