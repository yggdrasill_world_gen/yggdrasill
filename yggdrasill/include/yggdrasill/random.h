/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#ifndef YGDL_INCLUDE_GUARD_RANDOM
#define YGDL_INCLUDE_GUARD_RANDOM

#include <yggdrasill/commons.h>

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

	/// A 128-bit seed that can be used to initialize/re-seed `YGDL_Random` by calling `YGDL_random_seed`
	/// \note For optimal results each of the array elements should be filled with random bits, ideally obtained from `YGDL_seed_create`.
	typedef struct YGDL_Seed {
		uint32_t data[4];
	} YGDL_Seed;

	/**An opaque handle to the internally managed pseudo random number generator that belongs to a `YGDL_World` object.\n
	 * The generator as well as the distribution functions below are guaranteed to be portable and generate the same output on all platforms.
	 * \note The generator should be seeded using `YGDL_random_seed()` to guarantee the generation of predictable results.
	 */
	typedef struct YGDL_Random YGDL_Random;

	/// Creates a new random seed based on the OS provided random number source
	YGDL_EXPORTED YGDL_Seed YGDL_seed_create(void);

	/// Initializes the random number generator with the given seed data.\n
	/// For the same seed, the generator will output the same random numbers, as long as they are requested in the same order.
	YGDL_EXPORTED void YGDL_random_seed(YGDL_Random*, const YGDL_Seed* seed);


	/**Generates `count` pseudo random float numbers and writes them into the array pointed to by `out`.
	 * \note The numbers follow a normal distribution with the given `mean` and `standard_deviation`, in the range in a way compatible with `std::normal_distribution`
	 * \attention The behaviour is undefined if `out` doesn't point to a valid array of at least `count` float numbers.
	 */
	YGDL_EXPORTED void YGDL_random_normal_float(YGDL_Random*, float mean, float standard_deviation, YGDL_Index count, float* out);

	/**Generates `count` pseudo random double numbers and writes them into the array pointed to by `out`.
	 * \note The numbers follow a normal distribution with the given `mean` and `standard_deviation`, in the range in a way compatible with `std::normal_distribution`
	 * \attention The behaviour is undefined if `out` doesn't point to a valid array of at least `count` double numbers.
	 */
	YGDL_EXPORTED void YGDL_random_normal_double(YGDL_Random*, double mean, double standard_deviation, YGDL_Index count, double* out);


	/**Generates `count` pseudo random float numbers in the range [min,max] (inclusive) and writes them into the array pointed to by `out`.
	 * \note The numbers are uniformly distributed in the range in a way compatible with `std::uniform_real_distribution`
	 * \attention The behaviour is undefined if `out` doesn't point to a valid array of at least `count` float numbers.
	 */
	YGDL_EXPORTED void YGDL_random_uniform_float(YGDL_Random*, float min, float max, YGDL_Index count, float* out);

	/**Generates `count` pseudo random double numbers in the range [min,max] (inclusive) and writes them into the array pointed to by `out`.
	 * \note The numbers are uniformly distributed in the range in a way compatible with `std::uniform_real_distribution`
	 * \attention The behaviour is undefined if `out` doesn't point to a valid array of at least `count` double numbers.
	 */
	YGDL_EXPORTED void YGDL_random_uniform_double(YGDL_Random*, double min, double max, YGDL_Index count, double* out);


	/**Generates `count` pseudo random 8-bit integer numbers in the range [min,max] (inclusive) and writes them into the array pointed to by `out`.
	 * \note The numbers are uniformly distributed in the range in a way compatible with `std::uniform_int_distribution`
	 * \attention The behaviour is undefined if `out` doesn't point to a valid array of at least `count` float numbers.
	 */
	YGDL_EXPORTED void YGDL_random_uniform_int8(YGDL_Random*, int8_t min, int8_t max, YGDL_Index count, int8_t* out);

	/**Generates `count` pseudo random 32-bit integer numbers in the range [min,max] (inclusive) and writes them into the array pointed to by `out`.
	 * \note The numbers are uniformly distributed in the range in a way compatible with `std::uniform_int_distribution`
	 * \attention The behaviour is undefined if `out` doesn't point to a valid array of at least `count` float numbers.
	 */
	YGDL_EXPORTED void YGDL_random_uniform_int32(YGDL_Random*, int32_t min, int32_t max, YGDL_Index count, int32_t* out);

	/**Generates `count` pseudo random 64-bit integer numbers in the range [min,max] (inclusive) and writes them into the array pointed to by `out`.
	 * \note The numbers are uniformly distributed in the range in a way compatible with `std::uniform_int_distribution`
	 * \attention The behaviour is undefined if `out` doesn't point to a valid array of at least `count` float numbers.
	 */
	YGDL_EXPORTED void YGDL_random_uniform_int64(YGDL_Random*, int64_t min, int64_t max, YGDL_Index count, int64_t* out);


	/// Generates `count` pseudo random booleans with the given `probability` to be `YGDL_TRUE` and writes them into the array pointed to by `out`.
	/// \attention The behaviour is undefined if `out` doesn't point to a valid array of at least `count` float numbers.
	YGDL_EXPORTED void YGDL_random_bool(YGDL_Random*, float probability, YGDL_Index count, YGDL_Bool* out);
	
#ifdef __cplusplus
}
#endif
#endif
