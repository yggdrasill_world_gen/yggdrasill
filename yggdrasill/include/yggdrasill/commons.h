/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#ifndef YGDL_INCLUDE_GUARD_COMMONS
#define YGDL_INCLUDE_GUARD_COMMONS

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

// define YGDL_EXPORTED as the necessary platform dependent attribute/__declspec to import/export symbols, if we are building or importing a shared library
#ifndef YGDL_STATIC
	#if defined _WIN32 || defined __CYGWIN__ || defined __MINGW32__
		#ifdef YGDL_BUILDING_LIBRARY
			#ifdef __GNUC__
				#define YGDL_EXPORTED __attribute__ ((dllexport))
			#else
				#define YGDL_EXPORTED __declspec(dllexport)
			#endif
		#else
			#ifdef __GNUC__
				#define YGDL_EXPORTED __attribute__ ((dllimport))
			#else
				#define YGDL_EXPORTED __declspec(dllimport)
			#endif
		#endif
	 
	#else
		#if __GNUC__ >= 4
			#define YGDL_EXPORTED __attribute__ ((visibility ("default")))
		#else
			#define YGDL_EXPORTED
		#endif
	#endif

#else
	#define YGDL_EXPORTED
#endif

#ifdef __cplusplus
/// some functions (e.g. mesh traversal) are defined inline for performance and should also be marked as constexpr when compiling for C++
#define YGDL_CONSTEXPR constexpr
#else
#define YGDL_CONSTEXPR
#endif

// detect if the system is 32 or 64 bit
#if _WIN32 || _WIN64
   #if _WIN64
     #define YGDL_ENV_64_BIT
  #else
    #define YGDL_ENV_32_BIT
  #endif
#elif __GNUC__
  #if __x86_64__ || __ppc64__
    #define YGDL_ENV_64_BIT
  #else
    #define YGDL_ENV_32_BIT
  #endif
#endif

#if !defined(YGDL_ENV_64_BIT) && !defined(YGDL_ENV_32_BIT)
#error "Couldn't detect if the system/project is 32 or 64 bit"
#endif


#ifdef YGDL_ASSUME_CHECK
	// assumptions are checked instead of used for optimization
	#ifdef __cplusplus
		// C++ version with nicer error message
		#define YGDL_ASSUME_TOSTR_IMPL(x) #x
		#define YGDL_ASSUME_TOSTR(x) YGDL_ASSUME_TOSTR_IMPL(x)
		/// A function macro that can be used to pass invariants to the optimizer. For debug build the condition is checked at runtime and for all other targets it is assumed to be true by the optimizer.
		#define YGDL_ASSUME(...) do{ if(!(__VA_ARGS__)) [[unlikely]] {\
		                         fputs("YGDL_ASSUME " #__VA_ARGS__ " at " __FILE__ ":" YGDL_ASSUME_TOSTR(__LINE__) " failed.\n", stderr);\
		                         abort();} }while(false)
	#else
		// C version using assert
		/// A function macro that can be used to pass invariants to the optimizer. For debug build the condition is checked at runtime and for all other targets it is assumed to be true by the optimizer.
		#define YGDL_ASSUME(...) assert((__VA_ARGS__))
	#endif

#elif defined(YGDL_ASSUME_USE)
	// assumptions are passed to the optimizer, either using [[assume(...)]] if supported, or by compiler-specific build-ins
	#if defined(__cplusplus) && defined(__has_cpp_attribute)
		#if __has_cpp_attribute(assume)
			#define YGDL_HAS_ASSUME_ATTRIBUTE
		#endif
	#endif

	#ifdef YGDL_HAS_ASSUME_ATTRIBUTE
		/// A function macro that can be used to pass invariants to the optimizer. For debug build the condition is checked at runtime and for all other targets it is assumed to be true by the optimizer.
		#define YGDL_ASSUME(...) [[assume(__VA_ARGS__)]]
	#else
		#if defined(__clang__)
			/// A function macro that can be used to pass invariants to the optimizer. For debug build the condition is checked at runtime and for all other targets it is assumed to be true by the optimizer.
			#define YGDL_ASSUME(...)  __builtin_assume(__VA_ARGS__)
		#elif defined(__GNUC__) || defined(__GNUG__)
			/// A function macro that can be used to pass invariants to the optimizer. For debug build the condition is checked at runtime and for all other targets it is assumed to be true by the optimizer.
			#define YGDL_ASSUME(...)  do { if (!(__VA_ARGS__)) __builtin_unreachable(); } while (0)
		#elif defined(_MSC_VER)
			/// A function macro that can be used to pass invariants to the optimizer. For debug build the condition is checked at runtime and for all other targets it is assumed to be true by the optimizer.
			#define YGDL_ASSUME(...)  __assume(__VA_ARGS__)
		#endif
	#endif

#else
	/// A function macro that can be used to pass invariants to the optimizer. For debug build the condition is checked at runtime and for all other targets it is assumed to be true by the optimizer.
	#define YGDL_ASSUME(...) do {} while(0)
#endif

#ifdef __cplusplus
extern "C" {
#endif

	/**Enum used to indicate the underlying type if there are multiple alternatives at runtime.
	 * Used for example in dictionaries, arrays and layers.
	 * Each value corresponds to the concrete C type listed below.
	 * 
	 * \note The type actually used to store these values if `YGDL_Type_t` defined below.
	 * The enum type isn't used directly, because `sizeof(YGDL_Type)` is implementation defined in C and thus not portable.
	 */
	typedef enum YGDL_Type {
		YGDL_TYPE_NONE,  ///< `void`, used for unknown, unassigned or missing types
		YGDL_TYPE_BOOL,  ///< `YGDL_Bool`, with values `YGDL_TRUE` and `YGDL_FALSE`
		YGDL_TYPE_INT8,  ///< `int8_t`
		YGDL_TYPE_INT32, ///< `int32_t`
		YGDL_TYPE_INT64, ///< `int64_t`
		YGDL_TYPE_FLOAT, ///< `float`
		YGDL_TYPE_DOUBLE,///< `double`
		YGDL_TYPE_VEC2,  ///< `YGDL_Vec2`
		YGDL_TYPE_VEC3,  ///< `YGDL_Vec3`
		YGDL_TYPE_STRING,///< `YGDL_String`
		YGDL_TYPE_ARRAY, ///< `YGDL_Array`
		YGDL_TYPE_DICT   ///< `YGDL_Dict`
	} YGDL_Type;

	/// Type used to store and pass `YGDL_Type` values
	typedef int8_t YGDL_Type_t;

	/**Type used to store and pass truth-values (true/false).\n
	 * We can't directly use `bool`, because the C++ `bool` type and the equivalent C type `_Bool` are not guaranteed to be identical or of a fixed size.
	 * \attention The only valid values for this type are `YGDL_TRUE` (1) and `YGDL_FALSE` (0).
	 */
	typedef uint8_t YGDL_Bool;

	/// Equivalent for `true` in `YGDL_Bool`
	#define YGDL_TRUE  1
	/// Equivalent for `false` in `YGDL_Bool`
	#define YGDL_FALSE 0

	/// Type used for all indices and sizes
	typedef int32_t YGDL_Index;

	/**Type used to indicate version numbers, both for plugins after hot-reloading and to mark different revisions of copy-on-write data.\n
	 * Values are only guaranteed to be different for two distinct recent versions but don't define any ordering,
	 * so e.g. `version_a < version_b` is not a meaningful comparison!
	 */
	typedef uint32_t YGDL_Data_version;

	/// A two-dimensional vector type with components `x` and `y`
	typedef struct YGDL_Vec2 {
		float x, y;
	} YGDL_Vec2;

	/// A three-dimensional vector type with components `x`, `y` and `z`
	typedef struct YGDL_Vec3 {
		float x, y, z;
	} YGDL_Vec3;


	/**Writes a warning message to the set handler (stderr if not set).\n
	 * Should be prefered to directly writing messages to stdout/stderr,
	 * because it gives the host application control about how these messages should be handled.
	 */
	YGDL_EXPORTED void YGDL_warning_log(const char*);

	/// Sets the function that is called by `YGDL_warning_log`. If not set manually this defaults to writing the string directly to stderr.
	YGDL_EXPORTED void YGDL_set_warning_log_handler(void (*)(const char*));
	
#ifdef __cplusplus
}
#endif
#endif
