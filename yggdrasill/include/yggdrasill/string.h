/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#ifndef YGDL_INCLUDE_GUARD_STRING
#define YGDL_INCLUDE_GUARD_STRING

#include <yggdrasill/commons.h>

#include <stdint.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

	/// The maximum size of a string that can be stored inside YGDL_String without requiring heap allocations (excluding the null terminator)
	#define YGDL_SMALL_STRING_SIZE 15

	/// A read-only view of a string or sub-string. The references sequence is contiguous but doesn't have to be null-terminated.
	typedef struct YGDL_String_view {
		const char* chars;  ///< A pointer to the first character of the sequence
		YGDL_Index  length; ///< The number of characters in the sequence
	} YGDL_String_view;

	/// Contents of `YGDL_String`, for strings with more than `YGDL_SMALL_STRING_SIZE` characters
	struct YGDL_String_data_large_t {
		char*      chars; ///< A pointer to the heap-allocated byte buffer
		#ifdef YGDL_ENV_32_BIT
		uint32_t padding_; ///< Padding that needs to be added on 32 bit systems, to correctly align `YGDL_String_data_large_t` and `YGDL_String_data_small_t`
		#endif
		YGDL_Index length; ///< The number of bytes currently stored in the buffer
		/**The size of the allocated buffer, i.e. the maximum `length` the string can grow to before we have to re-allocate
		 * \note The actually stored value as its least significant bit set to 1, as a marker for large strings for the small-string-optimization.
		 * So, this bit has to be masked to get the actual capacity
		 */
		uint32_t   capacity;
	};
	/// Contents of `YGDL_String`, for strings with <= `YGDL_SMALL_STRING_SIZE` characters
	struct YGDL_String_data_small_t {
		/**Small-string buffer\n\n
		 * The first `YGDL_SMALL_STRING_SIZE` bytes contain the stored characters and the last element `chars[YGDL_SMALL_STRING_SIZE]` contains the number
		 * of unused byte left-shifted by one, i.e. `(YGDL_SMALL_STRING_SIZE - length)<<1`.
		 * Hence, when the string reaches the maximum size, the last byte is zero and becomes the null terminator.\n
		 */
		char   chars[YGDL_SMALL_STRING_SIZE+1];
	};

	/**A sequence of characters, similar to `std::string`.\n
	 * Characters are 8-Bit ASCII. UTF-8 characters are supported for storage, but access operators and traversal operate on bytes instead of code units.\n
	 * Small strings are stored inline, instead of incurring a heap allocation, if their length is <= `YGDL_SMALL_STRING_SIZE`.
	 * If the string is larger, like `std::string`, the managed heap-buffer will reserve more space than necessary to avoid frequent reallocations.
	 * \note Which of the zwo union members is active/used is determined by the reading the last byte, i.e. `data_.small.chars[YGDL_SMALL_STRING_SIZE]` and
	 * checking its least significant bit. If it's `0`, `YGDL_String_data_small_t` is used and otherwise `YGDL_String_data_large_t`
	 */
	typedef struct YGDL_String {
		union {
			struct YGDL_String_data_small_t small;
			struct YGDL_String_data_large_t large;
		} data_;
	} YGDL_String;
	
	/// \returns A string-view for the given null-terminated C-String
	inline YGDL_String_view YGDL_string_view_create(const char* str) {
		YGDL_String_view v;
		v.chars  = str;
#ifdef __cplusplus
		v.length = static_cast<int32_t>(strlen(str));
#else
		v.length = (int32_t) strlen(str);
#endif
		return v;
	}

#ifdef __cplusplus
	/**Creates a new empty `YGDL_String`.\n
	 * Because of the small-string-optimization this can be executed at compile-time and doesn't allocate, thus `YGDL_string_destroy` is a no-op.\n
	 * \attention Once the string has been grown beyond the small-string capacity, `YGDL_string_destroy` has to be called once it's no longer used to free its allocated resources.
	 */
	constexpr inline YGDL_String YGDL_string_create_empty()
	{
		YGDL_String str = YGDL_String{};
		str.data_.small.chars[YGDL_SMALL_STRING_SIZE] = (YGDL_SMALL_STRING_SIZE) << 1 & ~(uint32_t(1) | uint8_t(128));
		str.data_.small.chars[0]  = '\0';
		return str;
	}
#else
	/**Creates a new empty `YGDL_String`.\n
	 * Because of the small-string-optimization this can be executed at compile-time and doesn't allocate, thus `YGDL_string_destroy` is a no-op.\n
	 * \attention Once the string has been grown beyond the small-string capacity, `YGDL_string_destroy` has to be called once it's no longer used to free its allocated resources.
	 */
	inline YGDL_String YGDL_string_create_empty(void)
	{
		YGDL_String str;
		str.data_.small.chars[YGDL_SMALL_STRING_SIZE] = (YGDL_SMALL_STRING_SIZE) << 1 & ~(((uint32_t) 1) | ((uint8_t) 128));
		str.data_.small.chars[0]  = '\0';
		return str;
	}
#endif
	/// Creates a new `YGDL_String` with the given contents.
	/// \attention The returned string has to be destroyed using `YGDL_string_destroy` to free its allocated resources.
	YGDL_EXPORTED YGDL_String      YGDL_string_create    (YGDL_String_view);
	/// Creates a new `YGDL_String` with the given contents.
	/// \attention The returned string has to be destroyed using `YGDL_string_destroy` to free its allocated resources.
	YGDL_EXPORTED YGDL_String      YGDL_string_copy      (const YGDL_String*);
	/// Frees any resources held by the given string.
	/// \attention The behaviour is undefined if the string is passed to any other function after this call
	YGDL_EXPORTED void             YGDL_string_destroy   (YGDL_String*);
	/// \returns A string-view with the current contents of the given string
	/// \note The returned view/pointer is invalidated by any call to a function in this file that takes a non-const reference to the string, except for `YGDL_string_data`
	YGDL_EXPORTED YGDL_String_view YGDL_string_get       (const YGDL_String*);
	/// \returns The number of character currently stored in the given string
	YGDL_EXPORTED YGDL_Index       YGDL_string_size      (const YGDL_String*);
	/// \returns The maximum number of characters the given string could grow without requiring a new heap allocation
	YGDL_EXPORTED YGDL_Index       YGDL_string_capacity  (const YGDL_String*);
	/// \returns An immutable pointer to the first byte/character of the stored null-terminated sequence
	/// \note The returned view/pointer is invalidated by any call to a function in this file that takes a non-const reference to the string, except for `YGDL_string_data`
	YGDL_EXPORTED const char*      YGDL_string_const_data(const YGDL_String*);
	/// \returns A pointer to the first byte/character of the stored null-terminated sequence
	/// \note The returned view/pointer is invalidated by any call to a function in this file that takes a non-const reference to the string, except for `YGDL_string_data`
	YGDL_EXPORTED char*            YGDL_string_data      (YGDL_String*);
	/// Growth the allocated buffer, if necessary, to accommodate at least `minimum_capacity` bytes
	YGDL_EXPORTED void             YGDL_string_reserve   (YGDL_String*, YGDL_Index minimum_capacity);
	/// Sets the number of stored characters by either removing bytes from the end or appending `c`
	YGDL_EXPORTED void             YGDL_string_resize    (YGDL_String*, YGDL_Index size, char c);
	/// Overrides the contents of `lhs` with those of `rhs`
	YGDL_EXPORTED void             YGDL_string_set       (YGDL_String* lhs, YGDL_String_view rhs);
	/// Appends `rhs` to the string `lhs`
	YGDL_EXPORTED void             YGDL_string_append    (YGDL_String* lhs, YGDL_String_view rhs);
	
#ifdef __cplusplus
}
#endif
#endif

