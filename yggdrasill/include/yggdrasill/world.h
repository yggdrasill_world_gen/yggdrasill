/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#ifndef YGDL_INCLUDE_GUARD_WORLD
#define YGDL_INCLUDE_GUARD_WORLD

#include <yggdrasill/commons.h>
#include <yggdrasill/dictionary.h>
#include <yggdrasill/error.h>
#include <yggdrasill/mesh.h>
#include <yggdrasill/random.h>
#include <yggdrasill/string.h>
#include <yggdrasill/layer.h>

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

	/**An opaque handle to a heap allocated simulation state.\n
	 * The state consists of a `YGDL_Mesh`, a `YGDL_Random` and a number of unstructured (`YGDL_Dict`) and structured (`YGDL_Layer`) data layers.\n
	 * Each of these sub-objects is a separate heap-allocated object and all but YGDL_Random are copied using a copy-on-write semantic, to enable
	 * efficient deep copies. Thus, parts of this object may be shared with other instances and their address may change if they are modified.
	 * 
	 * \attention Thread-Safety: Multiple threads may access different `World` objects (or views of them)
	 * even if they share some parts due to the copy-on-write system. But any calls on the same objects have
	 * to externally synchronized.
	 * 
	 * \see See the project documentation for details on the internal structure and how copies and modifications work:
	 * https://yggdrasill_world_gen.gitlab.io/yggdrasill/api_c/world
	 */
	typedef struct YGDL_World YGDL_World;

	/**Changes if the release of a mutable lock on world objects triggers a mesh validation.\n
	 * If set to true, when a ``YGDL_world_*_release()`` function is called, the data that might have been modified is checked against the valid range
	 * specified in the `YGDL_Layer_definition`, as well as other invariants.\n
	 * While these validations incur some additional cost, they are valuable to detect programming errors.
	 * \note Validation is enabled by default.\n
	 * \note This setting is shared between all world objects and threads, but changing it might not take
	 * effect immediately for concurrent threads.
	 */
	YGDL_EXPORTED void      YGDL_world_validate_mesh_on_release_set(YGDL_Bool);
	/// \returns If the worlds mesh is validated after modifications.
	/// \see `YGDL_world_validate_on_release_set(YGDL_Bool)`
	YGDL_EXPORTED YGDL_Bool YGDL_world_validate_mesh_on_release_get(void);
	
	/**Changes if the release of a mutable lock on world objects triggers a layer validation.\n
	 * If set to true, when a ``YGDL_world_*_release()`` function is called, the data that might have been modified is checked against the valid range
	 * specified in the `YGDL_Layer_definition`, as well as other invariants.\n
	 * While these validations incur some additional cost, they are valuable to detect programming errors.
	 * \note Validation is enabled by default.\n
	 * \note This setting is shared between all world objects and threads, but changing it might not take
	 * effect immediately for concurrent threads.
	 */
	YGDL_EXPORTED void      YGDL_world_validate_layers_on_release_set(YGDL_Bool);
	/// \returns If the worlds data layers are validated after modifications.
	/// \see `YGDL_world_validate_on_release_set(YGDL_Bool)`
	YGDL_EXPORTED YGDL_Bool YGDL_world_validate_layers_on_release_get(void);
	

	/// Creates a new empty world object and returns a pointer to it.
	/// \attention The returned object has to be freed using `YGDL_world_destroy` once it's no longer used.
	YGDL_EXPORTED YGDL_World* YGDL_world_create(void);

	/**Destroys the given world objec.\n
	 * The parameter has to be a pointer to a valid `YGDL_World` object, created by either `YGDL_world_create`, `YGDL_world_copy` or `YGDL_world_move`.\n
	 * \attention The behaviour is undefined if the passed objects or any of its parts is used afterwards or if there are any unreleased mutable pointers to its sub-objects.
	 */
	YGDL_EXPORTED void        YGDL_world_destroy(YGDL_World* world);

	/**Creates a new simulation state, that is an exact copy of the one passed as the parameter, and returns a pointer to it.\n
	 * While the function behaves like a deep copy, it initially only creates shallow copies of the sub-objects that contain the simulation state. Thus, the
	 * initial space requirements and runtime are comparatively small and bounded by ``O(L)``, where ``L`` is the number of data layers.
	 * Actual deep copies of the sub-objects are only created when a mutable pointer to them is acquired using one of the ``YGDL_world_*_acquire()`` functions below.
	 * \attention The behaviour is undefined if there are any unreleased mutable pointers to the parameters sub-objects.
	 * \attention The returned object has to be freed using `YGDL_world_destroy` once it's no longer used.
	 */
	YGDL_EXPORTED YGDL_World* YGDL_world_copy(const YGDL_World* other_world);

	/**Creates a new simulation state, that is identical to the one passed as the parameter, and returns a pointer to it.\n
	 * In contrast to `YGDL_world_copy`, this function will reuse any resources owned by its parameter and invalidate the passed world object in the process.
	 * \attention After this function returns, the object pointed to by `other_world` will be destroyed, as if `YGDL_world_destroy` was called on it.
	 * That is, this function performs a *destructive move operation* and it isn't allowed to call `YGDL_world_destroy` again on the same object.
	 * To prevent accidental misuse, the function sets `*other_world` to NULL after is has been destroyed but the caller must take care that there are 
	 * no other pointers to the destroyed object.
	 * \attention The behaviour is undefined if there are any unreleased mutable pointers to the parameters sub-objects.
	 * \attention The returned object has to be freed using `YGDL_world_destroy` once it's no longer used.
	 */
	YGDL_EXPORTED YGDL_World* YGDL_world_move(YGDL_World** other_world);

	/**Change the state of `target` to make it an exact copy of `other_world`.\n
	 * While the function behaves like a deep copy, it initially only creates shallow copies of the sub-objects that contain the simulation state. Thus, the
	 * initial space requirements and runtime are comparatively small and bounded by ``O(L)``, where ``L`` is the number of data layers.
	 * Actual deep copies of the sub-objects are only created when a mutable pointer to them is acquired using one of the ``YGDL_world_*_acquire()`` functions below.
	 * \attention This function invalidates all pointers to sub-objects of `target`.
	 * \attention The behaviour is undefined if there are any unreleased mutable pointers to one of the parameters sub-objects.
	 * \tip Higher-level language bindings should use this function, instead of manually re-assigning the opaque handles, to preserve the validity of other
	 * pointers to the same object.
	 */
	YGDL_EXPORTED void YGDL_world_copy_assign(YGDL_World* target, const YGDL_World* other_world);

	/**Change the state of `target` to make it identical to that of `other_world`.\n
	 * In contrast to `YGDL_world_copy`, this function will reuse any resources owned by `other_world` and invalidate it in the process.
	 * \attention After this function returns, the object pointed to by `other_world` will be destroyed, as if `YGDL_world_destroy` was called on it.
	 * That is, this function performs a *destructive move operation* and it isn't allowed to call `YGDL_world_destroy` again on the same object.
	 * To prevent accidental misuse, the function sets `*other_world` to NULL after is has been destroyed but the caller must take care that there are 
	 * no other pointers to the destroyed object.
	 * \attention This function invalidates all pointers to sub-objects of `target`.
	 * \attention The behaviour is undefined if there are any unreleased mutable pointers to one of the parameters sub-objects.
	 * \tip Higher-level language bindings should use this function, instead of manually re-assigning the opaque handles, to preserve the validity of other
	 * pointers to the same object 
	 */
	YGDL_EXPORTED void YGDL_world_move_assign(YGDL_World* target, YGDL_World** other_world);

	/**Validates the data layers against their specification, as well as the invariants of e.g. the mesh.
	 * \returns ``true`` if the validation finished successfully.
	 * \throws YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if one of the layers is invalid or one of the invariants
	 * has been breached.
	 */
	YGDL_EXPORTED YGDL_Bool YGDL_world_valid(const YGDL_World* world, YGDL_Error* out_error);


// ////////////////////////////////////////////////////////////////////////////
// API TO ACCESS/MODIFY TO WORLD'S MESH
// ////////////////////////////////////////////////////////////////////////////

	/**\returns An immutable pointer to the world's mesh. The pointer is never NULL.
	 * \attention The returned pointer is invalidated if a deep-copy of the mesh has to be performed, i.e. by calling `YGDL_world_mesh_acquire` for the first time.
	 * But once the deep-copy is made, the pointer remains stable and valid for the lifetime of the `world` object.
	 */
	YGDL_EXPORTED const YGDL_Mesh*  YGDL_world_mesh(const YGDL_World* world);

	/**Acquires a mutable pointer to the world's mesh.
	 * \returns A pointer that can be used to access and modify the layer's data, or NULL if an error occurred.
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the mutable pointer couldn't be acquired, usually because
	 * a race-condition occurred.
	 * \note Because this function may need to perform a deep-copy of the mesh, the worst-case runtime complexity is linear in the number of vertices, faces
	 * and edges in the mesh.
	 * \attention This function may invalidate all pointers retrieved from `YGDL_world_mesh()` using the same world.\n
	 * \attention The returned pointer has to be released by calling `YGDL_world_mesh_release()`, once it's no longer needed.
	 */
	YGDL_EXPORTED YGDL_Mesh*        YGDL_world_mesh_acquire(YGDL_World* world, YGDL_Error* out_error);

	/**Releases a mutable pointer previously acquired from `YGDL_world_mesh_acquire`.
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the performed modifications left the data layers in an invalid state.
	 * \throw YGDL_ERROR_CODE_MESH_CORRUPTION Sets `out_error` (if not NULL) to this code, if the performed modifications left the mesh in an invalid state.
	 * \note Even if an error occurred, the mutable pointer will still be released.
	 * \note The passed `mesh` is allowed to be NULL, in which case this operation has no effect.\n
	 * \note Because this function performs validation, if `YGDL_world_validate_on_release_get()` is true, the runtime complexity is linear in the number of
	 * vertices, faces and edges in the mesh.
	 * \attention The behaviour is undefined if the `mesh` pointer wasn't acquired from the given world or if it has already been released.\n
	 * \attention The behaviour is undefined if the passed `world` pointer is used afterwards.
	 */
	YGDL_EXPORTED void              YGDL_world_mesh_release(YGDL_World* world, YGDL_Mesh* mesh, YGDL_Error* out_error);

	/// \returns A operator== comparable object, that changes every time a mutable pointer to the mesh is released by calling `YGDL_world_mesh_release()`.
	YGDL_EXPORTED YGDL_Data_version YGDL_world_mesh_version(const YGDL_World* world);


// ////////////////////////////////////////////////////////////////////////////
// API TO ACCESS/MODIFY TO WORLD'S RANDOM NUMBER GENERATOR
// ////////////////////////////////////////////////////////////////////////////

	/// \returns A pointer to the world's pseudo random number generator.
	/// \note In contrast to the data layers and mesh, because of its small state size, the RNG doesn't use copy-on-write and is instead always copied eagerly.
	YGDL_EXPORTED YGDL_Random*      YGDL_world_random(YGDL_World* world);


// ////////////////////////////////////////////////////////////////////////////
// API TO ACCESS/MODIFY TO WORLD'S STRUCTURED (MESH-BASED) DATA LAYERS
// ////////////////////////////////////////////////////////////////////////////

	/**\returns An immutable pointer to the requested mesh-based layer or NULL if no such layer exists or the existing layer is incompatible.
	 * \attention The returned pointer is invalidated if a deep-copy of the layer has to be performed. That means, the first call to `YGDL_world_layer_acquire`
	 * or modifying the mesh in a way that affects this layer, might invalidate the pointer. But once the deep-copy is made, the pointer remains stable and
	 * valid for the lifetime of the `world` object or until the layer is explicitly removed with `YGDL_world_layer_remove()`.
	 */
	YGDL_EXPORTED const YGDL_Layer* YGDL_world_layer(const YGDL_World* world, const YGDL_Layer_definition*);

	/**Acquires a mutable pointer to the requested mesh-based layer, inserting it if it doesn't exist.
	 * \returns A pointer that can be used to access and modify the layer's data, or NULL if an error occurred.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if the layer exists but is incompatible with the passed `YGDL_Layer_definition`.
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the mutable pointer couldn't be acquired, usually because
	 * a race-condition occurred.
	 * \note Because this function may need to perform a deep-copy of the layer, the worst-case runtime complexity is linear in the length of the layer.
	 * \attention This function may invalidate all pointers retrieved from `YGDL_world_layer()` using the same world and layer.\n
	 * \attention The returned pointer has to be released by calling `YGDL_world_layer_release()`, once it's no longer needed.
	 */
	YGDL_EXPORTED YGDL_Layer*       YGDL_world_layer_acquire(YGDL_World* world, const YGDL_Layer_definition*, YGDL_Error* out_error);

	/**Releases a mutable pointer previously acquired from `YGDL_world_layer_acquire`.
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the performed modifications left the data layers in an invalid state.
	 * \note Even if an error occurred, the mutable pointer will still be released.
	 * \note The passed `layer` is allowed to be NULL, in which case this operation has no effect.\n
	 * \note Because this function performs validation, if `YGDL_world_validate_on_release_get()` is true, the runtime complexity is linear in the length of the layer.
	 * \attention The behaviour is undefined if the `layer` wasn't acquired from the given world or if it has already been released.\n
	 * \attention The behaviour is undefined if the passed `layer` pointer is used afterwards.
	 */
	YGDL_EXPORTED void              YGDL_world_layer_release(YGDL_World* world, YGDL_Layer* layer, YGDL_Error* out_error);

	/**Removes the mesh-based layer with the given `id` from this world.
	 * \attention This invalidates all pointers to this layer retrieved from the given world.\n
	 * \attention The behaviour is undefined if there are any unreleased mutable pointers to the layer.
	 */
	YGDL_EXPORTED void              YGDL_world_layer_remove(YGDL_World* world, YGDL_String_view id);

	/// \returns A operator== comparable object, that changes every time a mutable pointer to the layer with the given id is released by calling `YGDL_world_layer_release()`.
	YGDL_EXPORTED YGDL_Data_version YGDL_world_layer_version(const YGDL_World* world, YGDL_String_view id);

	/**Retrieves a list of immutable pointers to all mesh-based layers.\n
	 * \param size In-Out-Parameter that contains the maximum size of the passed `data` array. After the function returns it is populated with the number of
	 * entries written to the array.
	 * \param data A pointer to an array provided by the caller, with space for at least `*size` elements.
	 * \returns `YGDL_TRUE` if `data` has been written to and `*size` now contains the number of written elements. `YGDL_FALSE` if the array was not passed or
	 * too small and only `*size` was written.
	 * 
	 * If `data` is NULL or `*size` is smaller than the actual number of layers, nothing is written to `data` and `*size` is instead set to the current
	 * number of elements.\n
	 * Thus, the usual procedure is to call this function twice, first with `data==NULL` to retrieve the size, and a second time with an allocated array of
	 * sufficient size.
	 */
	YGDL_EXPORTED YGDL_Bool YGDL_world_layer_list(const YGDL_World* world, YGDL_Index* size, const YGDL_Layer** data);

	/**Invalidates part of the data stored in the mesh-based layers, as described in the parameter.\n
	 * How each layer is modified by this operation depends on its configuration. Specifically their
	 * settings for `YGDL_Layer_invalidation` and `YGDL_Layer_interpolation`.
	 * \attention This call acquires mutable pointers to any affected layers, which may invalidate any pointer retrieved from `YGDL_world_layer()` for the world,
	 * that hasn't been copied yet.
	 * \note Invalidation of data layers normally happens automatically when the mesh is modified. This function is provided as an escape hatch for the rare
	 * use case that part of the data becomes outdated because of other reasons than changes in topology. For example, if the vertex positions are modified
	 * and other layers may depend on them and needs to be re-interpolated or invalidated.
	 */
	YGDL_EXPORTED void YGDL_world_layer_invalidate(YGDL_World* world, const YGDL_Layer_mesh_changes*, YGDL_Error* out_error);


// ////////////////////////////////////////////////////////////////////////////
// API TO ACCESS/MODIFY TO WORLD'S UNSTRUCTURED DATA LAYERS
// ////////////////////////////////////////////////////////////////////////////

	/**\returns An immutable pointer to the requested unstructured layer or NULL if no such layer exists.
	 * \attention The returned pointer is invalidated if a deep-copy of the layer has to be performed, i.e. the first call to `YGDL_world_unstructured_layer_acquire()`.
	 * But once the deep-copy is made, the pointer remains stable and valid for the lifetime of the `world` object or until the layer is explicitly
	 * removed with `YGDL_world_unstructured_layer_remove()`.
	 */
	YGDL_EXPORTED const YGDL_Dict*  YGDL_world_unstructured_layer(const YGDL_World* world, YGDL_String_view id);

	/**Acquires a mutable pointer to the requested unstructured layer, inserting it if it doesn't exist.
	 * \returns A pointer that can be used to access and modify the layer's data, or NULL if an error occurred.
	 * \throw YGDL_ERROR_CODE_PRECONDITION_VIOLATION Sets `out_error` (if not NULL) to this code, if the mutable pointer couldn't be acquired, usually because
	 * a race-condition occurred.
	 * \note Because this function may need to perform a deep-copy of the layer, the worst-case runtime complexity is linear in the size of the layer.
	 * \attention This function may invalidate all pointers retrieved from `YGDL_world_unstructured_layer()` using the same world and layer id.\n
	 * \attention The returned pointer has to be released by calling `YGDL_world_unstructured_layer_release()`, once it's no longer needed.
	 */
	YGDL_EXPORTED YGDL_Dict*        YGDL_world_unstructured_layer_acquire(YGDL_World* world, YGDL_String_view id, YGDL_Error* out_error);

	/**Releases a mutable pointer previously acquired from `YGDL_world_unstructured_layer_acquire`.
	 * \note Even if an error occurred, the mutable pointer will still be released.
	 * \note The passed `layer` is allowed to be NULL, in which case this operation has no effect.
	 * \attention The behaviour is undefined if the `layer` wasn't acquired from the given world or if it has already been released.\n
	 * \attention The behaviour is undefined if the passed `layer` pointer is used afterwards.
	 */
	YGDL_EXPORTED void              YGDL_world_unstructured_layer_release(YGDL_World* world, YGDL_Dict* layer);

	/**Removes the unstructured layer with the given `id` from this world.
	 * \attention This invalidates all pointers to this layer retrieved from the given world.\n
	 * \attention The behaviour is undefined if there are any unreleased mutable pointers to the layer.
	 */
	YGDL_EXPORTED void              YGDL_world_unstructured_layer_remove(YGDL_World* world, YGDL_String_view id);

	/// \returns A operator== comparable object, that changes every time a mutable pointer to the layer with the given id is released by calling `YGDL_world_unstructured_layer_release()`.
	YGDL_EXPORTED YGDL_Data_version YGDL_world_unstructured_layer_version(const YGDL_World* world, YGDL_String_view id);

	/**Retrieves a list of all unstructured layers' IDs.\n
	 * \param size In-Out-Parameter that contains the maximum size of the passed `data` array. After the function returns it is populated with the number of
	 * entries written to the array.
	 * \param data A pointer to an array provided by the caller, with space for at least `*size` elements.
	 * \returns `YGDL_TRUE` if `data` has been written to and `*size` now contains the number of written elements. `YGDL_FALSE` if the array was not passed or
	 * too small and only `*size` was written.
	 * 
	 * If `data` is NULL or `*size` is smaller than the actual number of layers, nothing is written to `data` and `*size` is instead set to the current
	 * number of elements.\n
	 * Thus, the usual procedure is to call this function twice, first with `data==NULL` to retrieve the size, and a second time with an allocated array of
	 * sufficient size.
	 */
	YGDL_EXPORTED YGDL_Bool YGDL_world_unstructured_layer_list(const YGDL_World* world, YGDL_Index* size, YGDL_String_view* data);

#ifdef __cplusplus
}
#endif
#endif
