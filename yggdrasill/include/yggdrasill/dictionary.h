/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#ifndef YGDL_INCLUDE_GUARD_DICTIONARY
#define YGDL_INCLUDE_GUARD_DICTIONARY

#include <yggdrasill/commons.h>
#include <yggdrasill/error.h>
#include <yggdrasill/string.h>

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct YGDL_Array YGDL_Array; // forward declaration; see array.h

	/// An opaque handle to a JSON-like key-value store, with string keys and values that are either concrete values, sequences of values or nested key-value stores.
	typedef struct YGDL_Dict YGDL_Dict;

	/// An opaque handle to one of the stored values, whose value can be accessed through the functions below.
	typedef struct YGDL_Dict_value YGDL_Dict_value;

	/// A view of the contiguous array of keys, stored in the requested level of the key-value store, in insertion-order
	typedef struct YGDL_Dict_key_list {
		const YGDL_String_view* keys; ///< A pointer to the first string of the sequence
		YGDL_Index              size; ///< The number of elements contained in the array pointed to by `keys`
	} YGDL_Dict_key_list;

	/// \returns The size of YGDL_Dict in bytes
	YGDL_EXPORTED int32_t YGDL_dict_sizeof(void);

	/**\returns true if a (possibly lossy) conversion between the two types is possible.
	 * \note If this function returns `YGDL_TRUE` the system will conserve some of the previous value if a conversion is requested by changing the type or requesting a different type than the stored one.
	 * Otherwise a value-initialized object will be used, instead.
	 */
	YGDL_EXPORTED YGDL_Bool YGDL_type_is_convertible(YGDL_Type_t from, YGDL_Type_t to);

	/// Creates a new dictionary instance and returns a pointer to it.
	/// \attention The returned object has to be freed using `YGDL_dict_destroy` once it's no longer used.
	YGDL_EXPORTED YGDL_Dict* YGDL_dict_create(void);
	/// Creates a new dictionary, that is a deep copy of the one passed as the parameter, and returns a pointer to it.
	/// \attention The returned object has to be freed using `YGDL_dict_destroy` once it's no longer used.
	YGDL_EXPORTED YGDL_Dict* YGDL_dict_copy(const YGDL_Dict*);
	/**Destroys the given dictionary.\n
	 * The parameter has to be a non-null pointer to a `YGDL_Dict` created by one of the functions above.\n
	 * \attention  The behaviour is undefined if the passed dictionary or any of its values is used afterwards.
	 */
	YGDL_EXPORTED void       YGDL_dict_destroy(YGDL_Dict*);
	/// Creates a deep copy of `rhs` in `lhs`, replacing its previous contents and invalidating all pointers to values or keys of `lhs`.
	YGDL_EXPORTED void       YGDL_dict_copy_assign(YGDL_Dict* lhs, const YGDL_Dict* rhs);
	/// Creates a deep copy of `rhs` in `lhs`, replacing its previous contents and invalidating all pointers to values or keys of `lhs`.
	/// \attention `rhs` is modified to reuse its resources and will be empty after this operation!
	YGDL_EXPORTED void       YGDL_dict_move_assign(YGDL_Dict* lhs, YGDL_Dict* rhs);

	/// \returns An immutable view of the keys currently stored in the given dictionary, in the order they were inserted in.
	/// \note The returned view becomes invalid if any function taking a non-const `YGDL_Dict*` is called.
	YGDL_EXPORTED YGDL_Dict_key_list     YGDL_dict_keys(const YGDL_Dict*);

	/// Removes the given `key` from the dictionary.
	/// If the key isn't currently mapped, no modification takes place and no pointers are invalidated.
	YGDL_EXPORTED void                   YGDL_dict_erase(YGDL_Dict*, YGDL_String_view key);

	/// Removes all mappings from the dictionary.
	YGDL_EXPORTED void                   YGDL_dict_clear(YGDL_Dict*);

	/**Retrieves the value currently mapped to the given key or inserts it, if it's not currently mapped.
	 * \returns An opaque pointer that serves as a handle to inspect the mapped type and its value.
	 * \note Invalidates the view returned by `YGDL_dict_keys` if the key had to be inserted.
	 * \note The returned pointer remains valid until its explicitly removed by calling `YGDL_dict_erase`, `YGDL_dict_destroy`, `YGDL_dict_copy_assign`, or `YGDL_dict_move_assign`.
	 */
	YGDL_EXPORTED YGDL_Dict_value*       YGDL_dict_get       (      YGDL_Dict*, YGDL_String_view key);
	/**Retrieves an immutable view of the value currently mapped to the given key.
	 * \returns An opaque pointer, that serves as a handle to inspect the mapped type and its value, or NULL if the key isn't currently mapped.
	 * \note The returned pointer remains valid until its explicitly removed by calling `YGDL_dict_erase`, `YGDL_dict_destroy`, `YGDL_dict_copy_assign`, or `YGDL_dict_move_assign`.
	 */
	YGDL_EXPORTED const YGDL_Dict_value* YGDL_dict_get_const (const YGDL_Dict*, YGDL_String_view key);
	/// \returns The type of the value represented by the given handle or `YGDL_TYPE_NONE` if it hasn't been assigned a value/type or isn't mapped.
	/// \note For ease of use, this function may be called with NULL and returns `YGDL_TYPE_NONE` in this case.
	YGDL_EXPORTED YGDL_Type_t            YGDL_dict_value_type(const YGDL_Dict_value*);

	/// Creates a deep copy of `rhs` in `lhs`, replacing its previous contents and invalidating all pointers to values or keys of `lhs`.
	YGDL_EXPORTED void YGDL_dict_value_copy_assign(YGDL_Dict_value* lhs, const YGDL_Dict_value* rhs);

	/**\returns A pointer to the value represented by the given handle, converting it if it doesn't contain the requested type.
	 * \note Conversions might be necessary and incur a loss of precision (if `YGDL_type_is_convertible` returns true) or discard the original value entirely.
	 * \note The returned pointer remains valid until the corresponding key is removed (see `YGDL_dict_get`), the dictionary is destroyed or the value is converted to a different type.
	 */
	YGDL_EXPORTED YGDL_Dict*   YGDL_dict_value_dict  (YGDL_Dict_value*);
	/// \see `YGDL_dict_value_dict`
	/// \param value_type If not `YGDL_TYPE_NONE`, a new empty array is created if the element type of the stored array doesn't match this type.
	YGDL_EXPORTED YGDL_Array*  YGDL_dict_value_array (YGDL_Dict_value*, YGDL_Type_t value_type);
	YGDL_EXPORTED YGDL_Bool*   YGDL_dict_value_bool  (YGDL_Dict_value*); ///< \see `YGDL_dict_value_dict`
	YGDL_EXPORTED int8_t*      YGDL_dict_value_int8  (YGDL_Dict_value*); ///< \see `YGDL_dict_value_dict`
	YGDL_EXPORTED int32_t*     YGDL_dict_value_int32 (YGDL_Dict_value*); ///< \see `YGDL_dict_value_dict`
	YGDL_EXPORTED int64_t*     YGDL_dict_value_int64 (YGDL_Dict_value*); ///< \see `YGDL_dict_value_dict`
	YGDL_EXPORTED float*       YGDL_dict_value_float (YGDL_Dict_value*); ///< \see `YGDL_dict_value_dict`
	YGDL_EXPORTED double*      YGDL_dict_value_double(YGDL_Dict_value*); ///< \see `YGDL_dict_value_dict`
	YGDL_EXPORTED YGDL_Vec2*   YGDL_dict_value_vec2  (YGDL_Dict_value*); ///< \see `YGDL_dict_value_dict`
	YGDL_EXPORTED YGDL_Vec3*   YGDL_dict_value_vec3  (YGDL_Dict_value*); ///< \see `YGDL_dict_value_dict`
	YGDL_EXPORTED YGDL_String* YGDL_dict_value_string(YGDL_Dict_value*); ///< \see `YGDL_dict_value_dict`

	/// \returns A pointer to the value represented by the given handle or NULL if the value is either not mapped or contains a different type than requested.
	/// \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if the value is not mapped or contains a different type
	YGDL_EXPORTED const YGDL_Dict*   YGDL_dict_value_const_dict  (const YGDL_Dict_value*, YGDL_Error* out_error);
	/// \see `YGDL_dict_value_const_dict`
	/// \param value_type If not YGDL_TYPE_NONE, the `YGDL_ERROR_CODE_TYPE_MISMATCH` error is also caused if the element type of the stored array doesn't match the passed value.
	YGDL_EXPORTED const YGDL_Array*  YGDL_dict_value_const_array (const YGDL_Dict_value*, YGDL_Type_t value_type, YGDL_Error* out_error);
	YGDL_EXPORTED const YGDL_Bool*   YGDL_dict_value_const_bool  (const YGDL_Dict_value*, YGDL_Error* out_error); ///< \see `YGDL_dict_value_const_dict`
	YGDL_EXPORTED const int8_t*      YGDL_dict_value_const_int8  (const YGDL_Dict_value*, YGDL_Error* out_error); ///< \see `YGDL_dict_value_const_dict`
	YGDL_EXPORTED const int32_t*     YGDL_dict_value_const_int32 (const YGDL_Dict_value*, YGDL_Error* out_error); ///< \see `YGDL_dict_value_const_dict`
	YGDL_EXPORTED const int64_t*     YGDL_dict_value_const_int64 (const YGDL_Dict_value*, YGDL_Error* out_error); ///< \see `YGDL_dict_value_const_dict`
	YGDL_EXPORTED const float*       YGDL_dict_value_const_float (const YGDL_Dict_value*, YGDL_Error* out_error); ///< \see `YGDL_dict_value_const_dict`
	YGDL_EXPORTED const double*      YGDL_dict_value_const_double(const YGDL_Dict_value*, YGDL_Error* out_error); ///< \see `YGDL_dict_value_const_dict`
	YGDL_EXPORTED const YGDL_Vec2*   YGDL_dict_value_const_vec2  (const YGDL_Dict_value*, YGDL_Error* out_error); ///< \see `YGDL_dict_value_const_dict`
	YGDL_EXPORTED const YGDL_Vec3*   YGDL_dict_value_const_vec3  (const YGDL_Dict_value*, YGDL_Error* out_error); ///< \see `YGDL_dict_value_const_dict`
	YGDL_EXPORTED const YGDL_String* YGDL_dict_value_const_string(const YGDL_Dict_value*, YGDL_Error* out_error); ///< \see `YGDL_dict_value_const_dict`
	
#ifdef __cplusplus
}
#endif
#endif
