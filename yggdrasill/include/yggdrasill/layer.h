/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#ifndef YGDL_INCLUDE_GUARD_LAYER
#define YGDL_INCLUDE_GUARD_LAYER

#include <yggdrasill/commons.h>
#include <yggdrasill/error.h>

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

	/**Constants that specify, which part of the mesh a structures layer is based on.\n
	 * This also defines the type (`YGDL_Vertex`/`YGDL_Face`/`YGDL_Edge`) that should
	 * be used to derive the index to access the layer's elements, as well as
	 * its length and behaviour when the mesh is modified.
	 * 
	 * There are multiple ways to link data to edges. The first distinction is between
	 * primal and dual edges, that is already part of their definition in the
	 * mesh (see `YGDL_mesh_edge_is_dual()`).
	 * But as it's also common for data to be identical in both directions of an edge,
	 * layers can also use edges as either directed or undirected. Undirected edge-layers
	 * discard the least-significant-bit of the index are thus only half as large as
	 * those that use directed edges.
	 * 
	 * \note The different types af edge-references can be easily distinguished by their
	 * bit-pattern.
	 * All values for edges have the bit ``0b100`` set.
	 * If bit ``0b010`` is also set it's a dual-edge, otherwise it's a primal-edge.
	 * And finally, directed edges have the bit ``0n001`` set, while undirected edges have not.
	 * 
	 * \note The type actually used to store these values if `YGDL_Layer_ref_type_t` defined below.
	 * The enum type isn't used directly, because `sizeof(YGDL_Layer_ref_type)` is implementation defined in C and thus not portable.
	 */
	typedef enum YGDL_Layer_ref_type {
		YGDL_LAYER_REF_TYPE_VERTEX = 0,                 ///< ``=0b000``\n Use `YGDL_Vertex::index` as the index into `YGDL_Layer::data`
		YGDL_LAYER_REF_TYPE_FACE = 1,                   ///< ``=0b001``\n Use `YGDL_Face::index` as the index into `YGDL_Layer::data`
		YGDL_LAYER_REF_TYPE_EDGE_PRIMAL = 4,			///< ``=0b100``\n Use `(YGDL_mesh_edge_index() >> 1)` as the index into `YGDL_Layer::data`
		YGDL_LAYER_REF_TYPE_EDGE_PRIMAL_DIRECTED = 5,   ///< ``=0b101``\n Use `(YGDL_mesh_edge_index())` as the index into `YGDL_Layer::data`
		YGDL_LAYER_REF_TYPE_EDGE_DUAL = 6,              ///< ``=0b110``\n Use `(YGDL_mesh_edge_index() >> 1)` as the index into `YGDL_Layer::data`
		YGDL_LAYER_REF_TYPE_EDGE_DUAL_DIRECTED = 7      ///< ``=0b111``\n Use `(YGDL_mesh_edge_index())` as the index into `YGDL_Layer::data`
	} YGDL_Layer_ref_type;

	/// Type used to store and pass `YGDL_Layer_ref_type` values
	typedef int8_t YGDL_Layer_ref_type_t;


	/**Constants that specify how a structured layer reacts to changes to the underlying mesh that invalidate
	 * parts connected to the data.\n
	 * Vertices and face are invalidated when they are removed from the mesh by `YGDL_mesh_edge_collapse()`.
	 * Edges, on the other hand, are not only invalidated when they are removed but also when their connectivity
	 * changes, i.e. when they connect different vertices or faces than before.
	 * See `YGDL_mesh_edge_flip()`, `YGDL_mesh_edge_split()` and `YGDL_mesh_edge_collapse()` for details when a specific edge
	 * may be invalidated.
	 * 
	 * \note Independent of the value, layers are still resized to match the number of vertices/faces/edges,
	 * as specified by `Layer_type`, and values will be updated, according to the chosen `YGDL_Layer_interpolation`.
	 * 
	 * \note The type actually used to store these values if `YGDL_Layer_invalidation_t` defined below.
	 * The enum type isn't used directly, because `sizeof(YGDL_Layer_invalidation)` is implementation defined in C and thus not portable.
	 */
	typedef enum YGDL_Layer_invalidation {
		/// Any existing value will be left unmodified
		/// \attention While this is the fastest option, it might cause unintended behaviour.
		YGDL_LAYER_INVALIDATION_KEEP = 0,
		YGDL_LAYER_INVALIDATION_RESET_AFFECTED,///< The value will be reset to the initial value specified in `YGDL_Layer_definition`
		YGDL_LAYER_INVALIDATION_RESET_ALL,     ///< **All** values in the layer will be reset to their initial value
		YGDL_LAYER_INVALIDATION_REMOVE_LAYER   ///< The layer will be removed from the `YGDL_World` entirely
	} YGDL_Layer_invalidation;

	/// Type used to store and pass `YGDL_Layer_invalidation` values
	typedef int8_t YGDL_Layer_invalidation_t;


	/**Constants that specify how values for a structured layer can be computed based on existing ones.
	 * 
	 * This information is primarily used by mutating operations on `YGDL_Mesh`, to computed values for new
	 * vertices/faces/edges or to update existing ones, e.g. when an edge is split.
	 * For these cases this enum defines how a new value can be computed based on two existing ones
	 * (''origin'' and ''dest'') and an interpolation factor (''t'').
	 * The two existing values are defined based on the edge that was split (see `Mesh_view::split()`).
	 * 
	 * \note While the mesh implementation itself only interpolates values in the specific well-defined
	 * cases, modules may explicitly invoke interpolation using `YGDL_world_layer_invalidate()`.
	 * Modules may assume that all layers interpolate similar to positional data. Thus, if your data can't
	 * be interpolated in these cases or your use-case isn't covered by the options below, you should use
	 * `YGDL_LAYER_INTERPOLATION_RESET_AFFECTED` to override the data with a well-known value, that can later be
	 * replaced with the actual value, as re-calculated by your own module.
	 * \see `YGDL_mesh_edge_flip()`, `YGDL_mesh_edge_split()` and `YGDL_mesh_edge_collapsee()` for details when this
	 * operation takes place.
	 * 
	 * \note The type actually used to store these values if `YGDL_Layer_interpolation_t` defined below.
	 * The enum type isn't used directly, because `sizeof(YGDL_Layer_interpolation)` is implementation defined in C and thus not portable.
	 */
	typedef enum YGDL_Layer_interpolation {
		/**The new value is unspecified\n
		 * This means that the implementation won't modify the stored values, so what ever values was previously
		 * stored will be kept, which _may_ be the initial/default value.
		 * \attention While this is the fastest option, it might cause unintended behaviour and can cause the
		 * layer's validation to fail, if the element was previously unused and contained invalid data.
		 */
		YGDL_LAYER_INTERPOLATION_DONT_CARE,
		YGDL_LAYER_INTERPOLATION_RESET_AFFECTED, ///< The value will be reset to the initial value specified in `YGDL_Layer_definition`
		YGDL_LAYER_INTERPOLATION_RESET_ALL,      ///< **All** values in the layer will be reset to their initial value
		YGDL_LAYER_INTERPOLATION_REMOVE_LAYER,   ///< The layer will be removed from the `YGDL_World` entirely
		YGDL_LAYER_INTERPOLATION_KEEP_ORIGIN,    ///< The new value is ``origin``
		YGDL_LAYER_INTERPOLATION_KEEP_DEST,      ///< The new value is ``dest``
		YGDL_LAYER_INTERPOLATION_LERP,           ///< The new value is ``lerp(origin, dest, t)``
		YGDL_LAYER_INTERPOLATION_NLERP,          ///< The new value is ``lerp(normalized(origin), normalized(dest), t) * lerp(length(origin), length(dest), t)``
		YGDL_LAYER_INTERPOLATION_MIN_VALUE,      ///< The new value is ``min(origin, dest)``
		YGDL_LAYER_INTERPOLATION_MAX_VALUE,      ///< The new value is ``max(origin, dest)``
		YGDL_LAYER_INTERPOLATION_MIN_WEIGHT,     ///< The new value is ``t>=0.5 ? origin : dest``
		YGDL_LAYER_INTERPOLATION_MAX_WEIGHT      ///< The new value is ``t>=0.5 ? dest : origin``
	} YGDL_Layer_interpolation;

	/// Type used to store and pass `YGDL_Layer_interpolation` values
	typedef int8_t YGDL_Layer_interpolation_t;


	/**Definition of a layer, that specifies what data is stored in it, what values
	 * are valid and how it will be updated when the underlying mesh changes.
	 * 
	 * A layer is identified only by their given string ID. But all usages of a layer have to use the same or
	 * a compatible definition-> For two definitions to be compatible, they have to have the same value for
	 * `data_type` and `ref_type`. Other difference don't generate an error, but may produce a warning.
	 * Only the definition used to create a layer is used to define validation, interpolation and invalidation
	 * information.
	 * 
	 * An object of this type is used when constructing a new layer by calling `YGDL_world_layer_acquire`,
	 * stored internally in the world object and used on all later accesses to check if the stored layer
	 * matches the requested definition->
	 * 
	 * \note Objects of this type are copied if they are retained by the rest of the API, e.g. when
	 * passed to `YGDL_world_layer_acquire`, and thus don't have to live longer than the respective call.
	 * This includes their ID and description, for which an internal deep-copy is created. So the strings
	 * the `YGDL_String_view` objects point to doesn't have to outlive the functions calls.
	 */
	typedef struct YGDL_Layer_definition {
		YGDL_String_view           id;           ///< The unique ID, used to identify this layer.
		YGDL_String_view           description;  ///< An arbitrary description text.
		YGDL_Type_t                data_type;    ///< The type of values stored in the layer.
		YGDL_Layer_ref_type_t      ref_type;     ///< The part of the element the layer is linked to, which defines its size and how indices have to be determined.
		YGDL_Layer_invalidation_t  invalidation; ///< What should happen to the layers values when the part of the mesh they are based on is invalidated.
		YGDL_Layer_interpolation_t interpolation;///< What should happen when the topology changed and new values need to be automatically computed from existing ones.

		/**The type specific part of the definition, consisting of:
		 * - The initial value for newly inserted and reset elements
		 * - The smallest and largest allowed values (both inclusive)
		 * - If NaN and infinite values are allowed for floating point types
		 * - The smallest and largest allowed length of vectors (both inclusive)
		 * \attention The active union member has to match the set `data_type`.
		 */
		union {
			/// Type specific part of `YGDL_Layer_definition` for `YGDL_TYPE_BOOL`
			struct YGDL_Layer_definition_bool  { YGDL_Bool initial; }         bool_value;
			/// Type specific part of `YGDL_Layer_definition` for `YGDL_TYPE_INT8`
			struct YGDL_Layer_definition_int8  { int8_t  min, initial, max; } int8_value;
			/// Type specific part of `YGDL_Layer_definition` for `YGDL_TYPE_INT32`
			struct YGDL_Layer_definition_int32 { int32_t min, initial, max; } int32_value;
			/// Type specific part of `YGDL_Layer_definition` for `YGDL_TYPE_INT64`
			struct YGDL_Layer_definition_int64 { int64_t min, initial, max; } int64_value;
			/// Type specific part of `YGDL_Layer_definition` for `YGDL_TYPE_FLOAT`
			struct YGDL_Layer_definition_float {
				float min, initial, max;
				YGDL_Bool has_min, has_max;
				YGDL_Bool allow_nan, allow_inf;
			}  float_value;
			/// Type specific part of `YGDL_Layer_definition` for `YGDL_TYPE_DOUBLE`
			struct YGDL_Layer_definition_double {
				double min, initial, max;
				YGDL_Bool has_min, has_max;
				YGDL_Bool allow_nan, allow_inf;
			} double_value;
			/// Type specific part of `YGDL_Layer_definition` for `YGDL_TYPE_VEC2`
			struct YGDL_Layer_definition_vec2 {
				YGDL_Vec2 min, initial, max;
				float min_length, max_length;
				YGDL_Bool has_min, has_max;
				YGDL_Bool has_min_length, has_max_length;
				YGDL_Bool allow_nan, allow_inf;
			} vec2_value;
			/// Type specific part of `YGDL_Layer_definition` for `YGDL_TYPE_VEC3`
			struct YGDL_Layer_definition_vec3 {
				YGDL_Vec3 min, initial, max;
				float min_length, max_length;
				YGDL_Bool has_min, has_max;
				YGDL_Bool has_min_length, has_max_length;
				YGDL_Bool allow_nan, allow_inf;
			} vec3_value;
		} value_info;
	} YGDL_Layer_definition;


	/**
	 * The data currently stored in the structured layer (defined by `definition`) in a given `YGDL_World` instance.
	 *
	 * A layer's data is stored in a continues array with a type specified by `YGDL_Layer_definition::data_type`.
	 * However, some elements in this array may not currently be valid, if the vertex/face/edge they reference is currently unused.
	 * These values may still be accessed, which can be preferable for performance reasons, but no guarantee is made concerning their value.
	 *
	 * The layer is managed by the `YGDL_World` that it was acquired from and tightly connected to its `YGDL_Mesh`.
	 * A layer's elements are linked to a specific part of the mesh (vertices/faces/edges) and automatically resized and updated when the mesh changes.
	 * As such the layer is also subject to the copy-on-write semantic outlined in `YGDL_World` and the referenced storage might change when a mutable
	 * pointer is acquired through `YGDL_world_layer_acquire` or the mesh is modified in a way that affects this layer.
	 *
	 * \attention Also this isn't an opaque handle and the members are non-const when a mutable pointer has been acquired, the members are managed by
	 * the system and may not be manually modified. The definition is only provided to enable efficient inline access operations.
	 * Thus, the behaviour is undefined if an instance of this type is manually created/copied/destroyed or any of its members is modified directly!
	 */
	typedef struct YGDL_Layer {
		const YGDL_Layer_definition* definition; ///< The definition this layer was created with
		/// The pointer to the first element. Has to be converted to the type specified by `YGDL_Layer_definition::data_type`.
		/// \see The ``YGDL_world_layer_values_*()`` functions below for a safe way to access the data
		void*                 data;
		YGDL_Index            length;     ///< The number of elements in the layer
	} YGDL_Layer;


	/// A non-owning sequence of IDs, to be invalidated as part of `YGDL_Layer_mesh_changes`
	typedef struct YGDL_Layer_element_invalidations {
		const YGDL_Index* indices;
		YGDL_Index        count;
	} YGDL_Layer_element_invalidations;
	/**A single interpolation to be performed as part of `YGDL_Layer_mesh_changes`.\n
	 * In all affected layers the element with ID `target` is overriden to the interpolation between `origin` and `dest`,
	 * with the interpolation weight `YGDL_Layer_mesh_changes::interpolation_weight` and the
	 * interpolation function specified by the layers `YGDL_Layer_interpolation` value.
	 */
	typedef struct YGDL_Layer_element_interpolation {
		YGDL_Index target;
		YGDL_Index origin;
		YGDL_Index dest;
	} YGDL_Layer_element_interpolation;
	/// A non-owning sequence of interpolations, to be performed as part of `YGDL_Layer_mesh_changes`
	typedef struct YGDL_Layer_element_interpolations {
		const YGDL_Layer_element_interpolation* elements;
		YGDL_Index                              count;
	} YGDL_Layer_element_interpolations;

	/**Structure that describes, which parts of the mesh-based layers are affected by the manuel invalidation triggered by
	 * `YGDL_world_layer_invalidate()`.\n
	 * This consists of vertices/faces/edges that have become invalid (as if the element was removed from the mesh),
	 * and should be handled according to the layers `YGDL_Layer_invalidation`, and vertices/faces/edges whose data should be
	 * re-interpolated (as if e.g. a new vertex were inserted) according to the layers `YGDL_Layer_interpolation`.
	 */
	typedef struct YGDL_Layer_mesh_changes {
		YGDL_Layer_element_invalidations invalidated_vertices;
		YGDL_Layer_element_invalidations invalidated_faces;
		YGDL_Layer_element_invalidations invalidated_primal_edges;
		YGDL_Layer_element_invalidations invalidated_dual_edges;

		YGDL_Layer_element_interpolations interpolated_vertices;
		YGDL_Layer_element_interpolations interpolated_faces;
		YGDL_Layer_element_interpolations interpolated_primal_edges;
		YGDL_Layer_element_interpolations interpolated_dual_edges;
		float                             interpolation_weight;
	} YGDL_Layer_mesh_changes;


	/**Creates a new layer-definition with the given unique `id` and the following default values:\n
	 * - Elements are value-initialized, i.e. zero/false or equivalent
	 * - No validation of min/max values
	 * - NaN and INF are forbidden
	 * - Values are reset to their initial value, when they are invalidated (see `YGDL_LAYER_INVALIDATION_RESET_AFFECTED`)
	 * - Modified elements are reset to their initial value and never automatically computed/interpolated (see `YGDL_LAYER_INTERPOLATION_RESET_AFFECTED`)
	 */
	YGDL_EXPORTED YGDL_Layer_definition YGDL_Layer_definition_create(YGDL_String_view id, YGDL_Type_t data_type, YGDL_Layer_ref_type_t ref_type);

	/// \returns A pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `YGDL_Bool`
	YGDL_CONSTEXPR inline YGDL_Bool*   YGDL_world_layer_values_bool  (YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_BOOL ?
#ifdef __cplusplus
        nullptr : static_cast<YGDL_Bool*>(layer->data);
#else
		0 : (YGDL_Bool*) layer->data;
#endif
	}
	/// \returns A pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `int8_t`
	YGDL_CONSTEXPR inline int8_t*      YGDL_world_layer_values_int8  (YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_INT8 ?
#ifdef __cplusplus
		nullptr : static_cast<int8_t*>(layer->data);
#else
		0 : (int8_t*) layer->data;
#endif
	}
	/// \returns A pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `int32_t`
	YGDL_CONSTEXPR inline int32_t*     YGDL_world_layer_values_int32 (YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_INT32 ?
#ifdef __cplusplus
		nullptr : static_cast<int32_t*>(layer->data);
#else
		0 : (int32_t*) layer->data;
#endif
	}
	/// \returns A pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `int64_t`
	YGDL_CONSTEXPR inline int64_t*     YGDL_world_layer_values_int64 (YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_INT64 ?
#ifdef __cplusplus
		nullptr : static_cast<int64_t*>(layer->data);
#else
		0 : (int64_t*) layer->data;
#endif
	}
	/// \returns A pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `float`
	YGDL_CONSTEXPR inline float*       YGDL_world_layer_values_float (YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_FLOAT ?
#ifdef __cplusplus
		nullptr : static_cast<float*>(layer->data);
#else
		0 : (float*) layer->data;
#endif
	}
	/// \returns A pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `double`
	YGDL_CONSTEXPR inline double*       YGDL_world_layer_values_double (YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_DOUBLE ?
#ifdef __cplusplus
		nullptr : static_cast<double*>(layer->data);
#else
		0 : (double*) layer->data;
#endif
	}
	/// \returns A pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `YGDL_Vec2`
	YGDL_CONSTEXPR inline YGDL_Vec2*   YGDL_world_layer_values_vec2  (YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_VEC2 ?
#ifdef __cplusplus
		nullptr : static_cast<YGDL_Vec2*>(layer->data);
#else
		0 : (YGDL_Vec2*) layer->data;
#endif
	}
	/// \returns A pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `YGDL_Vec3`
	YGDL_CONSTEXPR inline YGDL_Vec3*   YGDL_world_layer_values_vec3  (YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_VEC3 ?
#ifdef __cplusplus
		nullptr : static_cast<YGDL_Vec3*>(layer->data);
#else
		0 : (YGDL_Vec3*) layer->data;
#endif
	}

	/// \returns An immutable pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `YGDL_Bool`
	YGDL_CONSTEXPR inline const YGDL_Bool*   YGDL_world_layer_values_const_bool  (const YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_BOOL ?
#ifdef __cplusplus
		nullptr : static_cast<const YGDL_Bool*>(layer->data);
#else
		0 : (const YGDL_Bool*) layer->data;
#endif
	}
	/// \returns An immutable pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `int8_t`
	YGDL_CONSTEXPR inline const int8_t*      YGDL_world_layer_values_const_int8  (const YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_INT8 ?
#ifdef __cplusplus
		nullptr : static_cast<const int8_t*>(layer->data);
#else
		0 : (const int8_t*) layer->data;
#endif
	}
	/// \returns An immutable pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `int32_t`
	YGDL_CONSTEXPR inline const int32_t*     YGDL_world_layer_values_const_int32 (const YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_INT32 ?
#ifdef __cplusplus
		nullptr : static_cast<const int32_t*>(layer->data);
#else
		0 : (const int32_t*) layer->data;
#endif
	}
	/// \returns An immutable pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `int64_t`
	YGDL_CONSTEXPR inline const int64_t*     YGDL_world_layer_values_const_int64 (const YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_INT64 ?
#ifdef __cplusplus
		nullptr : static_cast<const int64_t*>(layer->data);
#else
		0 : (const int64_t*) layer->data;
#endif
	}
	/// \returns An immutable pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `float`
	YGDL_CONSTEXPR inline const float*       YGDL_world_layer_values_const_float (const YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_FLOAT ?
#ifdef __cplusplus
		nullptr : static_cast<const float*>(layer->data);
#else
		0 : (const float*) layer->data;
#endif
	}
	/// \returns An immutable pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `double`
	YGDL_CONSTEXPR inline const double*       YGDL_world_layer_values_const_double (const YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_DOUBLE ?
#ifdef __cplusplus
		nullptr : static_cast<const double*>(layer->data);
#else
		0 : (const double*) layer->data;
#endif
	}
	/// \returns An immutable pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `YGDL_Vec2`
	YGDL_CONSTEXPR inline const YGDL_Vec2*   YGDL_world_layer_values_const_vec2  (const YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_VEC2 ?
#ifdef __cplusplus
		nullptr : static_cast<const YGDL_Vec2*>(layer->data);
#else
		0 : (const YGDL_Vec2*) layer->data;
#endif
	}
	/// \returns An immutable pointer to the layer's first element, or ``NULL`` if the layer doesn't store values of type `YGDL_Vec3`
	YGDL_CONSTEXPR inline const YGDL_Vec3*   YGDL_world_layer_values_const_vec3  (const YGDL_Layer* layer) {
		return layer->definition->data_type!=YGDL_TYPE_VEC3 ?
#ifdef __cplusplus
		nullptr : static_cast<const YGDL_Vec3*>(layer->data);
#else
		0 : (const YGDL_Vec3*) layer->data;
#endif
	}

#ifdef __cplusplus
}
#endif
#endif
