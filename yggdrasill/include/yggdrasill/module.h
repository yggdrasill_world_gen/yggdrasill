/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#ifndef YGDL_INCLUDE_GUARD_MODULE
#define YGDL_INCLUDE_GUARD_MODULE

#include <yggdrasill/commons.h>
#include <yggdrasill/dictionary.h>
#include <yggdrasill/error.h>
#include <yggdrasill/world.h>

#ifdef __cplusplus
extern "C" {
#endif

	/**Structure that contains the metadata for a module, in the format expected by `YGDL_Generator`.\n
	 * A valid module has to consist of at least its unique ID and pointers to its `validate` and `execute` functions.
	 * 
	 * Additionally, modules may use temporary storage as a cache or to store loaded constants. These are allocated and freed by the `YGDL_Generator`
	 * using the provided `create` and `destroy` functions and passed to `validate` / `execute`.
	 * However, the contents of any non-const part of this storage is unspecified at the beginning of each `validate` / `execute` call and should be either 
	 * reset or otherwise overwritten. The `YGDL_Generator` is explicitly allowed to allocate multiple such storages (e.g. one per thread) and it is unspecified
	 * if subsequent calls are passed the same storage or if the storage has been used by other intermittent calls, possibly with different configurations or world states.
	 * 
	 * \attention While the object itself can be short-lived, i.e. you may add a temporary `YGDL_Module_info` to a generator, the contained function pointers
	 * have to remain valid for the whole remaining lifetime of the generator it is added to.
	 */
	typedef struct YGDL_Module_info {
		YGDL_String_view id; ///< The unique ID of this module

		/**This function can optionally be provided to initialize any semi-persistent state. If the module doesn't require any state it should be set to NULL.\n
		 * If the initialization failed you should set the passed error to a non-success code. If that is the case, the module won't be added to the generator
		 * and no further functions from this struct will be called.
		 * \returns An opaque userdata-pointer, that is stored and passed unmodified to all other functions of this module instance
		 */
		void* (*create)  (YGDL_Error* out_error);
		/// This function can optionally be provided to un-initialize this module. If the module doesn't require any state it should be set to NULL.
		/// Otherwise the function will be called with the return value of `create`, when the module is unloaded.
		void  (*destroy) (void* userdata);
		/**Pointer to the validation function of this module. Has to be non-NULL.\n
		 * The passed `configuration` should be parsed in the same way as in ``execute``, reporting any potential errors by setting `out_error`. This function
		 * may also be used to populate the configuration with default values for all relevant settings.
		 * \attention For ease of implementation the passed `userdata` is mutable, but since there is no guarantee if and when this function will be called
		 * the behaviour is undefined if `userdata` is modified in any way that changes the observable behaviour of `execute`.
		 * \attention The behaviour is undefined if any references to `configuration`, `out_error` or any of their parts is retained beyond this call!
		 */
		void  (*validate) (void* userdata, YGDL_Dict* configuration, YGDL_Error* out_error);
		/**Pointer to the execution function of this module. Has to be non-NULL.\n
		 * This function should implement the core functionality of the module, i.e. it interprets the passed `configuration` and applies the desired
		 * modifications to the passed `world` state.
		 * \attention The behaviour is undefined if any references to `world`, `configuration`, `out_error` or any of their parts is retained beyond this call!
		 */
		void  (*execute) (void* userdata, YGDL_World* world, const YGDL_Dict* configuration, YGDL_Error* out_error);
	} YGDL_Module_info;

	/**An opaque handle to the context that is currently loading this module.\n
	 * A plugin shared library has to contain a single exported function ``YGDL_init_plugin(YGDL_Plugin_init_handle*)``, that accepts a pointer to this type.
	 * The passed handle can than be used to register any modules contained in the plugin by calling `YGDL_plugin_module_add`..
	 */
	typedef struct YGDL_Plugin_init_handle YGDL_Plugin_init_handle;

	/// This function can be called inside the `YGDL_init_plugin` of a plugin shared library to register the given module
	YGDL_EXPORTED void YGDL_plugin_module_add(YGDL_Plugin_init_handle*, const YGDL_Module_info*);
	
#ifdef __cplusplus
}
#endif


#ifndef YGDL_STATIC
	#ifdef __cplusplus
		#define YGDL_PLUGIN_EXTC extern "C"
	#else
		#define YGDL_PLUGIN_EXTC
	#endif
	#if defined _WIN32 || defined __CYGWIN__ || defined __MINGW32__
		#ifdef __GNUC__
			#define YGDL_PLUGIN_INIT __attribute__ ((dllexport))
		#else
			#define YGDL_PLUGIN_INIT __declspec(dllexport)
		#endif
	#else
		#if __GNUC__ >= 4
			#define YGDL_PLUGIN_INIT __attribute__ ((visibility ("default")))
		#else
			#define YGDL_PLUGIN_INIT
		#endif
	#endif

	/**A macro that automatically generates a `YGDL_init_plugin` with the correct calling convention and attributes, that registers the given plugins.\n
	 * Example:
	 * \code{cpp}
	 * static const YGDL_Module_info module_a_def = {...};
	 * static const YGDL_Module_info module_b_def = {...};
	 * YGDL_REGISTER_PLUGIN_MODULES(&module_a_def, &module_b_def)
	 * \endcode
	 */
	#define YGDL_REGISTER_PLUGIN_MODULES(...)                                                    \
		YGDL_PLUGIN_EXTC YGDL_PLUGIN_INIT void YGDL_init_plugin(YGDL_Plugin_init_handle* handle) \
		{                                                                                        \
			const YGDL_Module_info* modules[] = {__VA_ARGS__};                                   \
            for(size_t i=0; i<sizeof(modules)/sizeof(modules[0]); ++i) {                         \
				YGDL_plugin_module_add(handle, modules[i]);                                      \
			}                                                                                    \
		}

#else
	#define YGDL_REGISTER_PLUGIN_MODULES(...)
#endif

#endif
