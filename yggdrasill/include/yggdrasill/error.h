/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#ifndef YGDL_INCLUDE_GUARD_ERROR
#define YGDL_INCLUDE_GUARD_ERROR

#include <yggdrasill/commons.h>
#include <yggdrasill/string.h>

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

	/**A number of predefined error codes for common causes of errors.\n
	 * All codes >= `YGDL_ERROR_CODE_MODULE_ERRORS` are reserved for plugin/module implementors.
	 * New library error codes with codes < `YGDL_ERROR_CODE_MODULE_ERRORS` may be added in future versions.
	 */
	typedef enum YGDL_Error_code {
		YGDL_ERROR_CODE_SUCCESS = 0,                ///< Code reserved to represent the absents of errors, i.e. successful return
		YGDL_ERROR_CODE_TYPE_MISMATCH = 1,          ///< Set if the runtime type doesn't match the expected type (e.g. when accessing dictionary values)
		YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS = 2,    ///< Set if an array is accessed with an invalid index
		YGDL_ERROR_CODE_PRECONDITION_VIOLATION = 3, ///< Set if a precondition specified in the API wasn't satisfied, i.e. programming errors
		/// Set if  if either a method is called to travers or modify an invalid mesh or if an operation would invalidate the mesh and was thus aborted
		YGDL_ERROR_CODE_MESH_CORRUPTION = 4,
		/// Set if a plugin couldn't be loaded, usually because it's either not a valid/compatible shared library or because the entry function wasn't found
		YGDL_ERROR_CODE_INVALID_PLUGIN = 5,
		YGDL_ERROR_CODE_IO_ERROR = 6, ///< Set if an unexpected error occurred reading/writing files
		
		/// Codes >= this value are reserved for use in user-defined modules. See the documentation of the used modules and the attached message for more details
		YGDL_ERROR_CODE_MODULE_ERRORS = 1024
	} YGDL_Error_code;

	/// Type used to store and pass `YGDL_Error_code` values
	typedef int32_t YGDL_Error_code_t;

	/**Type used to report errors, consisting of an error code, as defined in `YGDL_Error_code`, and an additional message with further details.\n
	 * This type is usually found in C-API functions that might fail. These accept an optional/null-able pointer to this type a their last parameter and
	 * populate it with details about the error that occurred, if the parameter was not NULL and the operation failed.
	 * 
	 * \warning Since this type owns the error-message, creating, destroying and copying instances of this type has to use the functions defined below.
	 * If an instance of this type is created/copied/destroyed directly, the behaviour is undefined.
	 * 
	 */
	typedef struct YGDL_Error {
		YGDL_Error_code_t code;    ///< The general category of error that occurred.\n \see `YGDL_Error_code` for details
		YGDL_String       message; ///< A detailed human-readable message that describes the cause of the error
		///< Stacktrace of the program position where `YGDL_error_create()` or `YGDL_error_set()` was last called, if the library was build with support for std::stacktrace
		YGDL_String       stacktrace;
	} YGDL_Error;


#ifdef __cplusplus
	/**Creates a new empty error, i.e. a success
	 * \attention The returned object initially doesn't hold any state and can be safely discarded without calling any further function.
	 * But once it has been populated, `YGDL_error_destroy` has to be called at the end of its lifetime to free any allocated resources.
	 */
	constexpr inline YGDL_Error YGDL_error_create_success() {
		return {0, YGDL_string_create_empty(), YGDL_string_create_empty()};
	}
#else
	/// Creates a new empty error, i.e. a success
	/// \attention The returned object has be destroyed using `YGDL_error_destroy` to free its allocated resources.
	inline YGDL_Error YGDL_error_create_success(void) {
		YGDL_Error e;
		e.code = 0;
		e.message = YGDL_string_create_empty();
		return e;
	}
#endif

	/// Creates an error with the given `code` and error `message`
	/// \attention The returned object has to be destroyed using `YGDL_error_destroy` to free its allocated resources.
	YGDL_EXPORTED YGDL_Error YGDL_error_create (YGDL_Error_code_t code, YGDL_String_view message);
	/// Creates an error that is an exact copy of the passed instance
	/// \attention The returned object has to be destroyed using `YGDL_error_destroy` to free its allocated resources.
	YGDL_EXPORTED YGDL_Error YGDL_error_copy   (const YGDL_Error*);
	/// Frees any resources held by the given error.
	/// \attention The behaviour is undefined if the object is passed to any other function after this call
	YGDL_EXPORTED void       YGDL_error_destroy(YGDL_Error* error);

	/// Sets the error-code and message to the passed values
	/// \param error A pointer to the error that should be modified. May be NULL, in which case this function becomes a NOP.
	YGDL_EXPORTED void       YGDL_error_set    (YGDL_Error* error, YGDL_Error_code_t code, YGDL_String_view message);
	/// Resets the given error to the default state, i.e. success
	YGDL_EXPORTED void       YGDL_error_reset  (YGDL_Error* error);

#ifdef __cplusplus
}
#endif
#endif
