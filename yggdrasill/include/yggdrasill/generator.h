/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#ifndef YGDL_INCLUDE_GUARD_GENERATOR
#define YGDL_INCLUDE_GUARD_GENERATOR

#include <yggdrasill/commons.h>
#include <yggdrasill/dictionary.h>
#include <yggdrasill/error.h>
#include <yggdrasill/string.h>
#include <yggdrasill/module.h>
#include <yggdrasill/world.h>

#ifdef __cplusplus
extern "C" {
#endif

	/**An opaque handle to a module definition.\n
	 * Modules represent generation algorithms, or parts of them and can be executed on a `YGDL_World` definition to modify it.\n
	 * They are usually loaded from shared libraries (plugins) and are owned by the `YGDL_Generator` instance that loaded them.\n
	 * Modules may contain semi-persistent state (e.g. caching) and have two main functions: ''validate'', to check and populate a configuration `YGDL_Dict`, and
	 * ''execute'', to actually apply it to a `YGDL_World`.
	 * \note References to modules are valid until they are removed from the generator they were obtained from, either because it was freed using
	 * `YGDL_generator_destroy` or because `YGDL_generator_module_reload` was called after the file they were loaded from was removed.
	 */
	typedef struct YGDL_Module YGDL_Module;

	/// An opaque handle to a world generator, managing a collection of `YGDL_Module` objects.\n
	/// These objects manage both a collections of loaded modules, as well as the OS-resources they were loaded from.
	typedef struct YGDL_Generator YGDL_Generator;

	/**A view of a contiguous array of modules. Usually either a list of all modules managed by a generator or just a list of the modules loaded by the last API call.
	 * \note The view remains valid until the generator is destroyed or modules are added or removed by calling
	 * `YGDL_generator_module_load`, `YGDL_generator_module_add` or `YGDL_generator_module_reload`.
	 */
	typedef struct Module_list {
		YGDL_Module** modules; ///< A pointer to the first module of the sequence
		YGDL_Index    count;   ///< The number of elements contained in the array pointed to by `modules`
	} Module_list;

	/// Creates a new generator instance and returns a pointer to it.
	/// \attention The returned object has to be freed using `YGDL_generator_destroy` once it's no longer used
	YGDL_EXPORTED YGDL_Generator* YGDL_generator_create(void);
	/**Destroys the given generator, as well as all its modules and frees any OS-resources it might occupy.\n
	 * The parameter has to be a non-null pointer to a `YGDL_Generator` created by one of the functions above.\n
	 * The behaviour is undefined if the passed objects or any of its values is used afterwards or if any of its modules is still being executed when this function is called.
	 */
	YGDL_EXPORTED void            YGDL_generator_destroy(YGDL_Generator*);
	
#ifndef YGDL_STATIC
	/**Load any shared libraries at the given `path` and adds them to the generator.\n
	 * \param path Either the path to a single shared library that should be loaded or to a directory that may contain multiple shared libraries. In The later case,
	 * each of the files must have the correct platform dependent suffix (.so or .dll for Windows).
	 * \returns A list of all modules loaded during this call. Same as returned by `YGDL_generator_module_last`.
	 * \throws YGDL_ERROR_CODE_INVALID_PLUGIN Sets `out_error` (if not NULL) to this code, if the shared library couldn't be loaded, didn't contain a
	 * ``YGDL_init_plugin`` function or the modules initialisation failed.
	 * \attention If multiple plugins are loaded during this call only the first error is reported and one or more modules might still have been loaded successfully!
	 * \note Already loaded files are ignored but loading a plugin might fail if a module with the same id already exists.
	 * Use `YGDL_generator_module_reload` instead to reload modules after their shared library file was updated.
	 * \note Because plugins require access to the library API, this function is only available if the library itself was compiled as a shared library.
	 * If you are using a static build, you'll have to add modules manually using `YGDL_generator_module_add`
	 */
	YGDL_EXPORTED Module_list YGDL_generator_module_load(YGDL_Generator*, YGDL_String_view path, YGDL_Error* out_error);
#endif

	/**Adds the given module to this generator.\n
	 * This function is used both to manually add modules when using a statically linked build and (indirectly) to add modules from the plugin entry-function.
	 * \param info A pointer to a struct that contains the modules ID and function pointers used to interact with it.
	 * \returns A pointer to the loaded module or NULL if the module couldn't be loaded
	 * \throws YGDL_ERROR_CODE_INVALID_PLUGIN Sets `out_error` (if not NULL) to this code, if a module with the given name already exists in the generator
	 * \note The referenced `YGDL_Module_info` only has to outlive this function call but all function pointers contained within it have to remain valid for
	 * at least the whole lifetime of the generator.
	 */
	YGDL_EXPORTED YGDL_Module* YGDL_generator_module_add (YGDL_Generator*, const YGDL_Module_info* info, YGDL_Error* out_error);
	/// \returns A pointer to the module with the given id or NULL if the generator contains no such module
	YGDL_EXPORTED YGDL_Module* YGDL_generator_module_get (YGDL_Generator*, YGDL_String_view id);

	/// \returns A list of all modules added by the last call to `YGDL_generator_module_load`, `YGDL_generator_module_add` or `YGDL_generator_module_reload`
	YGDL_EXPORTED Module_list YGDL_generator_module_last(YGDL_Generator*);

	/// \returns A list of all modules managed by the generator
	YGDL_EXPORTED Module_list YGDL_generator_module_list(YGDL_Generator*);

	/**Reloads any shared libraries that have been modified (filesystem last modified date) since they were loaded or have been added to loaded directories.
	 * \returns A list of all modules loaded during this call. Same as returned by `YGDL_generator_module_last`.
	 * \throws YGDL_ERROR_CODE_INVALID_PLUGIN Sets `out_error` (if not NULL) to this code, if one of the shared libraries couldn't be loaded, didn't contain a
	 * ``YGDL_init_plugin`` function or the modules initialisation failed.
	 * \note Any state stored by the reloaded modules is reset in the process.
	 * \attention The behaviour is undefined if any of the reloaded modules is currently being used in a concurrent call to
	 * `YGDL_generator_module_validate` or `YGDL_generator_module_execute`.
	 * \attention This functions only provides the weak exception guarantee! Plugins may be missing after this call if their file was removed or loading them
	 * failed and if a failure occurred the previous state can't be restored.
	 * \attention This function is generally not usable if multiple generator instances loaded the same plugins. Because the support for true isolation
	 * of shared libraries is limited in OS APIs, multiple generators might end up sharing the same shared-library object. In this case unloading and reloading
	 * the shared library won't have any effect.
	 */
	YGDL_EXPORTED Module_list YGDL_generator_module_reload(YGDL_Generator*, YGDL_Error* out_error);

	/**\returns true if a call to `YGDL_generator_module_reload` *might* reload modules, i.e. if there could be changes that require a reload.
	 * \note In contrast to `YGDL_generator_module_reload` this function can be safely called concurrently with module executions. So it can be used as a
	 * quick check if further expensive synchronization and a call `YGDL_generator_module_reload` is required
	 */
	YGDL_EXPORTED YGDL_Bool YGDL_generator_module_reload_available(YGDL_Generator*);

	/**Calls the validation function of the given module on `configuration`.
	 * \throws YGDL_ERROR_CODE_INVALID_PLUGIN Sets `out_error` (if not NULL) to this code, if the given plugin has already been unloaded.
	 * \throws YGDL_Error_code Sets `error` (if not NULL) to any error code, if the validation code encountered an unrecoverable error.
	 * \note While the modules may contain private state, none of the passed pointers will be retained beyond this call.
	 * Thus, there are no further lifetime requirements for the parameters.
	 */
	YGDL_EXPORTED void YGDL_generator_module_validate(const YGDL_Module*, YGDL_Dict* configuration, YGDL_Error* out_error);
	/**Calls the execution function of the given module on `world`.
	 * \throws YGDL_ERROR_CODE_INVALID_PLUGIN Sets `out_error` (if not NULL) to this code, if the given plugin has already been unloaded.
	 * \throws YGDL_Error_code Sets `out_error` (if not NULL) to any error code, if the validation code encountered an unrecoverable error.
	 * \attention Because the module might contain its own local state, this function may not be called concurrently on the same module, even on different `world` instances.
	 * \note While the modules may contain private state, none of the passed pointers will be retained beyond this call.
	 * Thus, there are no further lifetime requirements for the parameters.
	 */
	YGDL_EXPORTED void YGDL_generator_module_execute (YGDL_Module*, YGDL_World* world, const YGDL_Dict*, YGDL_Error* out_error);

	/// \returns The unique id of the given module
	YGDL_EXPORTED YGDL_String_view YGDL_generator_module_id(const YGDL_Module*);
	/**\returns An integer that indicates the version of the given module. The number itself has no specific meaning, but after a newer version of the module
	 * has been loaded with `YGDL_generator_module_reload` the number will change. So, the returned value can be used to check if the underlying implementation
	 * has changed, for example since it was executed.
	 */
	YGDL_EXPORTED YGDL_Data_version YGDL_generator_module_version(const YGDL_Module*);
	
#ifdef __cplusplus
}
#endif
#endif
