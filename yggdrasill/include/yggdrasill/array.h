/**********************************************************************************
 * Copyright (c) 2022 Florian Oetke                                               *
 * This file is part of the Yggdrasill project and licensed under the MIT License *
 * See LICENSE file for details.                                                  *
\**********************************************************************************/
#ifndef YGDL_INCLUDE_GUARD_ARRAY
#define YGDL_INCLUDE_GUARD_ARRAY

#include <yggdrasill/commons.h>
#include <yggdrasill/error.h>
#include <yggdrasill/string.h>

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

	typedef struct YGDL_Dict YGDL_Dict;

	/**A generic array type, that contains a heap-allocated sequence of elements of a dynamically defined type.
	 * 
	 * This type is a sequence container similar to std::vector, which means the elements are stored contiguously, pointers to elements can be used to
	 * access any other element in the array and appending elements might incur re-allocations (allocation a larger memory block and moving elements over),
	 * which also invalidates any pointers to elements. Which operations might invalidate pointers is also indicated on the functions below.
	 * 
	 * \note Since `YGDL_Dict` is an opaque handle, you need to use `YGDL_dict_sizeof()` to access other array elements based on a pointer returned from the array.
	 * 
	 * \note Instances of this type can't be created directly, but are managed automatically by an owning `YGDL_Dict` instance.
	 * 
	 * \warning If an instance of this type is created/copied/destroyed or any of its members is modified directly, the behaviour is undefined.
	 * All mutating operations have to use the utility functions below.
	 */
	typedef struct YGDL_Array YGDL_Array;

	/**\skip
	 * Semi-private definition of YGDL_Array. This definition is only public to allow for efficient inline implementation for access operations.
	 * \warning If an instance of this type is created/copied/destroyed or any of its members is modified directly, the behaviour is undefined.
	 * All mutating operations have to use the utility functions below.
	 */
	struct YGDL_Array {
		YGDL_Type_t type;      ///< The type of element currently stored in the array
		int32_t     size;      ///< The currently stored number of elements
		int32_t     capacity;  ///< The maximum number of elements that can be stored, without requiring a re-allocation
		void*       data;      ///< Pointer to the underlying storage for internal usage. Use YGDL_array_get* functions below to access the elements!
	};


	/// \returns The type of element stored inside the passed array instance
	inline YGDL_CONSTEXPR YGDL_Type_t YGDL_array_type(const YGDL_Array* array) {
		if(array)
			return array->type;
		else
			return YGDL_TYPE_NONE;
	}

	/// \returns The number of elements stored inside the passed array instance
	inline YGDL_CONSTEXPR int32_t YGDL_array_size(const YGDL_Array* a) {return a ? a->size : 0;}

	/**Increases the capacity of the array, so it has at least enough room for `minimum_capacity` many elements of its current type
	 * \pre The current type of the elements needs to be set to a valid `YGDL_Type_t` other than `YGDL_TYPE_NONE` or the `element_type` has to be passed.
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED void        YGDL_array_reserve(YGDL_Array*, YGDL_Index minimum_capacity, YGDL_Type_t element_type);
	/**Changes the number of elements stored in the array by either removing elements or appending new default-constructed elements, until the array reached the requested `size`.
	 * \pre The current type of the elements needs to be set to a valid `YGDL_Type_t` other than `YGDL_TYPE_NONE` or the `element_type` has to be passed.
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED void        YGDL_array_resize (YGDL_Array*, YGDL_Index size, YGDL_Type_t element_type);

	/**Appends a new value-initialized `YGDL_Dict` at the end of `array` and returns a pointer to it.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array)` is neither `YGDL_TYPE_NONE` nor `YGDL_TYPE_DICT`
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED YGDL_Dict*   YGDL_array_push_dict  (YGDL_Array* array, YGDL_Error* out_error);
	/**Appends a new value-initialized `YGDL_Array`, with the given `element_type`, at the end of `array` and returns a pointer to it.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array)` is neither `YGDL_TYPE_NONE` nor `YGDL_TYPE_ARRAY`
	 * \note `element_type` maybe be set to `YGDL_TYPE_NONE` and only later overriden with the actual type
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED YGDL_Array*  YGDL_array_push_array (YGDL_Array* array, YGDL_Type_t element_type, YGDL_Error* out_error);
	/**Appends a new value-initialized `YGDL_Bool` at the end of `array` and returns a pointer to it.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array)` is neither `YGDL_TYPE_NONE` nor `YGDL_TYPE_BOOL`
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED YGDL_Bool*   YGDL_array_push_bool  (YGDL_Array* array, YGDL_Error* out_error);
	/**Appends a new value-initialized `int8_t` at the end of `array` and returns a pointer to it.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array)` is neither `YGDL_TYPE_NONE` nor `YGDL_TYPE_INT8`
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED int8_t*      YGDL_array_push_int8  (YGDL_Array* array, YGDL_Error* out_error);
	/**Appends a new value-initialized `int32_t` at the end of `array` and returns a pointer to it.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array)` is neither `YGDL_TYPE_NONE` nor `YGDL_TYPE_INT32`
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED int32_t*     YGDL_array_push_int32 (YGDL_Array* array, YGDL_Error* out_error);
	/**Appends a new value-initialized `int64_t` at the end of `array` and returns a pointer to it.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array)` is neither `YGDL_TYPE_NONE` nor `YGDL_TYPE_INT64`
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED int64_t*     YGDL_array_push_int64 (YGDL_Array* array, YGDL_Error* out_error);
	/**Appends a new value-initialized `float` at the end of `array` and returns a pointer to it.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array)` is neither `YGDL_TYPE_NONE` nor `YGDL_TYPE_FLOAT`
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED float*       YGDL_array_push_float (YGDL_Array* array, YGDL_Error* out_error);
	/**Appends a new value-initialized `double` at the end of `array` and returns a pointer to it.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array)` is neither `YGDL_TYPE_NONE` nor `YGDL_TYPE_DOUBLE`
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED double*      YGDL_array_push_double(YGDL_Array* array, YGDL_Error* out_error);
	/**Appends a new value-initialized `YGDL_Vec2` at the end of `array` and returns a pointer to it.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array)` is neither `YGDL_TYPE_NONE` nor `YGDL_TYPE_VEC2`
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED YGDL_Vec2*   YGDL_array_push_vec2  (YGDL_Array* array, YGDL_Error* out_error);
	/**Appends a new value-initialized `YGDL_Vec3` at the end of `array` and returns a pointer to it.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array)` is neither `YGDL_TYPE_NONE` nor `YGDL_TYPE_VEC3`
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED YGDL_Vec3*   YGDL_array_push_vec3  (YGDL_Array* array, YGDL_Error* out_error);
	/**Appends a new value-initialized `YGDL_String` at the end of `array` and returns a pointer to it.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array)` is neither `YGDL_TYPE_NONE` nor `YGDL_TYPE_STRING`
	 * \note Invalidates all pointers to the elements
	 */
	YGDL_EXPORTED YGDL_String* YGDL_array_push_string(YGDL_Array* array, YGDL_Error* out_error);

	/**Removes the element at the given `index` from the `array`, moving all subsequent elements one space to the left.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is not valid.
	 * \pre `YGDL_array_type(array)` has to be a valid type other than `YGDL_TYPE_NONE`
	 * \note Invalidates pointers to the last element. Pointers to elements >= index will reference different but valid objects afterwards
	 */
	YGDL_EXPORTED void YGDL_array_erase_ordered  (YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Removes the element at the given `index` from the `array`, swapping/moving some elements to fill in the old position.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \pre `YGDL_array_type(array)` has to be a valid type other than `YGDL_TYPE_NONE`
	 * \note Invalidates pointers to the last element. Pointers to elements >= index might reference different but valid objects afterwards
	 */
	YGDL_EXPORTED void YGDL_array_erase_unordered(YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/// Removes all elements from the `array`.
	/// \note Invalidates all pointers to the elements
	YGDL_EXPORTED void YGDL_array_clear          (YGDL_Array* array);

	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_DICT`
	 */
	YGDL_EXPORTED YGDL_Dict*   YGDL_array_get_dict  (YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_ARRAY`
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `element_type != YGDL_TYPE_NONE` and the element type of the nested array doesn't match the requested type
	 */
	YGDL_EXPORTED YGDL_Array*  YGDL_array_get_array (YGDL_Array* array, YGDL_Index index, YGDL_Type_t element_type, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_BOOL`
	 */
	YGDL_EXPORTED YGDL_Bool*   YGDL_array_get_bool  (YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_INT8`
	 */
	YGDL_EXPORTED int8_t*      YGDL_array_get_int8  (YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_INT32`
	 */
	YGDL_EXPORTED int32_t*     YGDL_array_get_int32 (YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_INT64`
	 */
	YGDL_EXPORTED int64_t*     YGDL_array_get_int64 (YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_FLOAT`
	 */
	YGDL_EXPORTED float*       YGDL_array_get_float (YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_DOUBLE`
	 */
	YGDL_EXPORTED double*      YGDL_array_get_double(YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_Vec2`
	 */
	YGDL_EXPORTED YGDL_Vec2*   YGDL_array_get_vec2  (YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_VEC3`
	 */
	YGDL_EXPORTED YGDL_Vec3*   YGDL_array_get_vec3  (YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_STRING`
	 */
	YGDL_EXPORTED YGDL_String* YGDL_array_get_string(YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);

	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_DICT`
	 */
	YGDL_EXPORTED const YGDL_Dict*   YGDL_array_get_const_dict  (const YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_ARRAY`
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `element_type != YGDL_TYPE_NONE` and the element type of the nested array doesn't match the requested type
	 */
	YGDL_EXPORTED const YGDL_Array*  YGDL_array_get_const_array (const YGDL_Array* array, YGDL_Index index, YGDL_Type_t element_type, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_BOOL`
	 */
	YGDL_EXPORTED const YGDL_Bool*   YGDL_array_get_const_bool  (const YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_INT8`
	 */
	YGDL_EXPORTED const int8_t*      YGDL_array_get_const_int8  (const YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_INT32`
	 */
	YGDL_EXPORTED const int32_t*     YGDL_array_get_const_int32 (const YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_INT64`
	 */
	YGDL_EXPORTED const int64_t*     YGDL_array_get_const_int64 (const YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_FLOAT`
	 */
	YGDL_EXPORTED const float*       YGDL_array_get_const_float (const YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_DOUBLE`
	 */
	YGDL_EXPORTED const double*      YGDL_array_get_const_double(const YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_VEC2`
	 */
	YGDL_EXPORTED const YGDL_Vec2*   YGDL_array_get_const_vec2  (const YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_VEC3`
	 */
	YGDL_EXPORTED const YGDL_Vec3*   YGDL_array_get_const_vec3  (const YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	/**Retrieves a pointer to the object at `index` in `array`.
	 * \throw YGDL_ERROR_CODE_INDEX_OUT_OF_BOUNDS Sets `out_error` (if not NULL) to this code, if the `index` is invalid.
	 * \throw YGDL_ERROR_CODE_TYPE_MISMATCH Sets `out_error` (if not NULL) to this code, if `YGDL_array_type(array) != YGDL_TYPE_STRING`
	 */
	YGDL_EXPORTED const YGDL_String* YGDL_array_get_const_string(const YGDL_Array* array, YGDL_Index index, YGDL_Error* out_error);
	
#ifdef __cplusplus
}
#endif
#endif
