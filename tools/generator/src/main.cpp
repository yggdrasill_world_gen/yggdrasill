#include <yggdrasill/generator.hpp>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4267)
#endif

#include <indicators/block_progress_bar.hpp>
#include <indicators/cursor_control.hpp>

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include <csignal>
#include <iostream>

using namespace indicators;

namespace yggdrasill::modules {
	extern const Module_info generate_sphere;

	namespace tectonics {
		extern const Module_info generate_plates;
		extern const Module_info simulate_plates;
		extern const Module_info simple_erosion;
		extern const Module_info fix_enclaves;
		extern const Module_info merge_plates;
		extern const Module_info split_plates;
		extern const Module_info spawn_ridges;
		extern const Module_info refinement;
	} // namespace tectonics
} // namespace yggdrasill::modules

static void on_exit()
{
	show_console_cursor(true);
	std::cout << termcolor::reset << "\n" << std::flush;
}

static inline void signal_handler(int signal)
{
	on_exit();
	exit(signal);
}

int main(int argc, char** argv)
{
	const auto seed       = argc > 1 ? yggdrasill::Seed::from_str(argv[1]) : yggdrasill::Seed::random();
	const auto iterations = argc > 2 ? std::atoi(argv[2]) : 500;

	std::cout << "Seed: " << seed << "\n";

	auto bar = BlockProgressBar{option::BarWidth{80},
	                            option::Start{"["},
	                            option::End{"]"},
	                            option::PostfixText{"Generate World"},
	                            option::ForegroundColor{Color::white},
	                            option::ShowElapsedTime{true},
	                            option::ShowRemainingTime{true},
	                            option::FontStyles{std::vector<FontStyle>{FontStyle::bold}}};

	std::signal(SIGABRT, signal_handler);
	std::signal(SIGTERM, signal_handler);
	std::signal(SIGSEGV, signal_handler);
	std::signal(SIGINT, signal_handler);
	show_console_cursor(false);

	yggdrasill::validate_mesh_on_release(false);
	yggdrasill::validate_layers_on_release(false);

	try {
		auto gen             = yggdrasill::Generator();
		auto generate_sphere = gen.add(yggdrasill::modules::generate_sphere);
		auto generate_plates = gen.add(yggdrasill::modules::tectonics::generate_plates);
		auto simulate_plates = gen.add(yggdrasill::modules::tectonics::simulate_plates);
		auto simple_erosion  = gen.add(yggdrasill::modules::tectonics::simple_erosion);
		auto fix_enclaves    = gen.add(yggdrasill::modules::tectonics::fix_enclaves);
		auto merge_plates    = gen.add(yggdrasill::modules::tectonics::merge_plates);
		auto split_plates    = gen.add(yggdrasill::modules::tectonics::split_plates);
		auto spawn_ridges    = gen.add(yggdrasill::modules::tectonics::spawn_ridges);
		auto refinement      = gen.add(yggdrasill::modules::tectonics::refinement);

		auto dict = yggdrasill::Dictionary{};

		generate_sphere.validate(dict).throw_as_exception();
		generate_plates.validate(dict).throw_as_exception();
		refinement.validate(dict).throw_as_exception();
		simulate_plates.validate(dict).throw_as_exception();
		simple_erosion.validate(dict).throw_as_exception();
		spawn_ridges.validate(dict).throw_as_exception();
		fix_enclaves.validate(dict).throw_as_exception();
		merge_plates.validate(dict).throw_as_exception();
		split_plates.validate(dict).throw_as_exception();

		auto world = yggdrasill::World(seed);

		generate_sphere.execute(world, dict);
		bar.tick();
		generate_plates.execute(world, dict);
		bar.tick();

		bar.set_option(option::PostfixText{"Simulate Plates"});

		const auto progress_factor = 92.f / iterations;

		for(int i = 0; i < iterations; i++) {
			simulate_plates.execute(world, dict);
			fix_enclaves.execute(world, dict);
			simple_erosion.execute(world, dict);
			spawn_ridges.execute(world, dict);
			merge_plates.execute(world, dict);
			split_plates.execute(world, dict);
			fix_enclaves.execute(world, dict);
			refinement.execute(world, dict);
			bar.set_progress(2 + i * progress_factor);
		}

		bar.set_option(option::PostfixText{"Done"});
		bar.set_option(option::ForegroundColor{Color::green});
		bar.set_progress(100.f);

		on_exit();

	} catch(...) {
		bar.set_option(option::PostfixText{"Error"});
		bar.set_option(option::ForegroundColor{Color::red});
		on_exit();
		throw;
	}
}
