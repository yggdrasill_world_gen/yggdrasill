[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](/LICENSE)

# Yggdrasill

Yggdrasill is a WIP world generation toolkit, that currently consists of 4 main parts:

- A low-level C-API, designed for easy portability and as a basis for high-level APIs written in other languages
- A high-level C++-API
- A number of modules that solve different aspects of the world-generation process
- An interactive editor that utilizes these modules to generate worlds in real-time

The APIs and modules are published under the MIT license at the [main repository](https://gitlab.com/yggdrasill_world_gen/yggdrasill) and the editor at [this repository](https://gitlab.com/yggdrasill_world_gen/studio). The latest build is available as
a [WebGL](https://yggdrasill.second-system.de) (requires a recent version Chrome [>=68], Firefox [>=79] or Edge [>=79])
, [Windows](https://yggdrasill.second-system.de/yggdrasill_studio_windows.zip) and [Linux](https://yggdrasill.second-system.de/yggdrasill_studio_linux.zip) application.

For more information see the project documentation at https://yggdrasill_world_gen.gitlab.io/yggdrasill

## Dependencies

All of these dependencies are also provided by the projects docker image: `registry.gitlab.com/yggdrasill_world_gen/docker/yggdrasill-build-linux:latest`

- CMake
- A recent C++ compiler
    - GCC >= 13
    - Clang >= 16
    - MSVC >= 19.30 (Visual Studio 2022)

## Additional dependencies to build the documentation

- LLVM, libclang and libtooling >= 12
- Boost >= 1.70
- Sphinx, Furo theme for Sphinx, sphinx-inline-tabs and th python regex library
    - `pip install sphinx furo regex sphinx-inline-tabs`

## Build

```bash
git checkout https://gitlab.com/yggdrasill_world_gen/yggdrasill.git
mkdir yggdrasill/build
cd yggdrasill/build

cmake ..
cmake --build .
```

### CMake build options

- `YGGDRASILL_BUILD_TESTS`: Build the unit tests (Default: ON)
- `YGGDRASILL_BUILD_DOC`: Build the project's documentation (Default: OFF)
- `YGGDRASILL_WARNINGS`: Enables all applicable compiler warnings (Default: ON)
- `YGGDRASILL_WARNINGS_AS_ERRORS`: Enables warnings-as-errors (Default: OFF)
- `YGGDRASILL_COVERAGE`: Enables coverage reporting on GCC/Clang (Default: OFF)
- `YGGDRASILL_SANITIZER_ADDRESS`: Enables the address sanitizer on GCC/Clang (Default: OFF)
- `YGGDRASILL_SANITIZER_MEMORY`: Enables the memory sanitizer on GCC/Clang (Default: OFF)
- `YGGDRASILL_SANITIZER_UB`: Enables the undefined behavior sanitizer on GCC/Clang (Default: OFF)
- `YGGDRASILL_SANITIZER_THREAD`: Enables the thread sanitizer on GCC/Clang (Default: OFF)
- `YGGDRASILL_BUILD_TYPE`: Determines if the project should be build as a dynamic/shared (SHARED) or static (STATIC) library (Default: ALL)
