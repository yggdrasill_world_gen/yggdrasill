Utilities
======================================

The following is a list of all utility functions and smaller helper types -- like strings, math-vectors and iterators, used by other parts of the library.

API
--------------------------------------

Iterators
~~~~~~~~~~~

.. ucpp:file:: yggdrasill/iterator.hpp

String
~~~~~~~~~~~

.. ucpp:file:: yggdrasill/string.hpp

Commons
~~~~~~~~~~~

.. ucpp:file:: yggdrasill/commons.hpp
