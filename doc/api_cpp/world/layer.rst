Data Layers
======================================

.. cpp:namespace:: yggdrasill

Besides the mesh topology itself, all data is stored in layers managed and owned by a `World` object. There are two distinct types of layer
unstructured layers, that are just a `Dictionary` instance identified by a unique ID (see :doc:`../configuration`), and structured layers, that
are used to store data for specific vertices, faces or edges.

Structured layers are always linked to a specific element of the mesh (either vertices, faces or edges) and are used to attach data to them.
Layers function similar to sequence containers, except that they are managed by the system and automatically updated when the mesh the layer is based on is modified. So, values can only be read and modified, but layers can't be manually resized or otherwise structurally modified. There are also some other modifications, outlined below, that are performed automatically when the mesh it modified.

Some of the values in a layer may also be left "unused", if they correspond to a mesh element that is currently not part of the mesh. To avoid costly copying, the values in a layer are not compacted when e.g. a vertex is removed because of `Mesh_view::collapse()`. Instead, these elements are left as-is and reused if a new vertex is required. The layer values for these elements remain accessible and can still be read and modified, but are not used for anything else. When iterating over a layer, there are options to both include and skip these unused elements, depending on the performance characteristics of your specific operation.

What kind of data a layer contains, which part of the mesh they are based on and how they react to changes is all part of their `Layer_definition`, which is usually globally defined and later used to acquire a `Layer_view`/`Const_layer_view` from the `World`.

Example
--------------------------------------

.. cpp:ucpp_var_ctx:: yggdrasill::Const_world_view world

.. cpp:ucpp_var_ctx:: yggdrasill::Layer_definition position_layer

.. cpp:ucpp_var_ctx:: yggdrasill::Layer_definition velocity_layer

.. cpp:ucpp_var_ctx:: yggdrasill::Const_layer_view opt_ro_positions

.. cpp:ucpp_var_ctx:: yggdrasill::Const_layer_view ro_positions

.. cpp:ucpp_var_ctx:: yggdrasill::Const_mesh_view mesh

.. cpp:ucpp_var_ctx:: yggdrasill::Layer_definition

.. ucpp:include_example:: layer_example


Basic definition
--------------------------------------

All elements in a layer have to be of the same type, which is specified in its definition and limited to a subset of those listed in `Type`:

- `YGDL_Bool`
- `std::int8_t`, `std::int32_t` and `std::int64_t` as well as enumerations with one of these as their underlying type
- `float` and `double`
- `Vec2` and `Vec3`

The values that can be stored in a layer can be further restricted by specifying valid ranges (both min/max values and min/max length for `Vec2` and `Vec3`). These are checked when a layer has been modified, i.e. when the mutable view is released again, and generate an exception if an invalid value is found. This is especially important for enumerations, to limit the possible values to only those that are named in its definition. But they are also useful for other domain-specific restrictions, e.g. the radius of the sphere shouldn't change, and to detect errors like unexpected values or NaNs as early as possible. See `Layer_definition` for a full list of available options. The default configuration doesn't restrict the range of values, but does forbid NaNs and infinite values in floating-point numbers and vectors.

The other important information about a layer is which part of the mesh it's based on, which is defined by its `Ref_type`. This also controls what type is used in `Layer_view::operator[]` to access the layer's elements.
The two simple ones are `Ref_type::vertex` and `Ref_type::face`, which are used to attach data to the meshes `Vertex`/`Face` elements.
In addition, layers can also attach data to the primal edges, that connect vertices, and dual edges, that connect faces. For each of these, there are two `Ref_type` values because it's also possible to differentiate between directed and undirected edges. The quad-edge structure used in the :doc:`mesh <mesh>` consists of directed edges, so there are two edges for each set of connected vertices ``a`` and ``b``, once from ``a`` to ``b`` and another from ``b`` to ``a``. But the data often depends only on which vertices/faces are connected, and is independent of the edge's direction. To avoid having to store identical data for both edges, an undirected layer can be used, which is half as large and returns the same value for an `Edge` and its `Edge::sym()`.


Automatic changes on mesh modifications
----------------------------------------

Since a layer is tightly connected to an element of the mesh, it also has to be updated if the underlying mesh changes.

The layers are automatically resized if new vertices/faces/edges are added or old ones are removed. As outlined at the top, if a mesh element is removed, neither the mesh nor any of the layers are compacted to actually remove the now unused elements. Instead, they are left in as-is and may later be reused for new elements.

A layer can define what should happen when part of the mesh that it depends on is removed using the `Invalidation` enumeration. For vertices and faces, this happens when the corresponding vertex/face in the mesh is removed by `Mesh_view::collapse()`. But for edges this happens both when the edge is removed and when it is modified significantly, i.e. when it `Edge::origin()` or `Edge::dest()` is changed. The usual reaction is to either reset the layer's element to the initial value specified in the `Layer_definition` (`Invalidation::reset_affected`) or to keep the existing value (`Invalidation::keep`). But it's also possible to reset or removed the whole layer on any such invalidation, usually followed by a re-computation of the values the next time the module is executed.

Modifications of the mesh can not only remove elements, but also create new ones. In the case of simple manual additions (`Mesh_view::add_vertices()`/`Mesh_view::add_face()`) the new layer values are simply set to the value specified in `Layer_definition::initial()` and there is no way to define more complex behavior. This is normally not a problem because these operations are usually only used to create the initial mesh when there aren't many layers. However, new elements are also created by the `Mesh_view::split()` operation, which is often executed later to locally increase the mesh resolution. As such, it needs to generate meaningful values for new layer elements because the layer that generated the original values may not be run again afterwards to update the values itself. How these new values are computed can be specified in `Layer_definition::interpolation()` using one of the following values:

.. table:: Options for interpolation operations
   :widths: auto
   
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------------------------------------------------------------+
   |                                                           |                                             |                                                                                           |                                Example                                |
   |                                                           |                                             |                                                                                           +-----------------+-----------------+--------+--------------------------+
   | Name                                                      | Description                                 | Equation                                                                                  | origin          | dest            | weight | new value                |
   +===========================================================+=============================================+===========================================================================================+=================+=================+========+==========================+
   | :cpp:any:`dont_care <Interpolation::dont_care>`           | Don't initialize the value                  | --                                                                                        | --              | --              | --     | ???                      |
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------+-----------------+--------+--------------------------+
   | :cpp:any:`reset_affected <Interpolation::reset_affected>` | Reset to initial value (usually 0)          | `Layer_definition::initial()`                                                             | --              | --              | --     | 0                        |
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------+-----------------+--------+--------------------------+
   | :cpp:any:`reset_all <Interpolation::reset_all>`           | Reset **all** values in the layer           | `Layer_definition::initial()`                                                             | --              | --              | --     | 0                        |
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------+-----------------+--------+--------------------------+
   | :cpp:any:`remove_layer <Interpolation::remove_layer>`     | Remove the entire layer                     | --                                                                                        | --              | --              | --     | --                       |
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------+-----------------+--------+--------------------------+
   | :cpp:any:`keep_origin <Interpolation::keep_origin>`       | Always use origin value                     | ``= origin``                                                                              | 2               | --              | --     | 2                        |
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------+-----------------+--------+--------------------------+
   | :cpp:any:`keep_dest <Interpolation::keep_dest>`           | Always use dest value                       | ``= dest``                                                                                | --              | 5               | --     | 5                        |
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------+-----------------+--------+--------------------------+
   | :cpp:any:`lerp <Interpolation::lerp>`                     | Linearly interpolate between the two value  | ``lerp(origin, dest, t)``                                                                 | 0.2             | 0.8             | 0.5    | 0.5                      |
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------+-----------------+--------+--------------------------+
   | :cpp:any:`nlerp <Interpolation::nlerp>`                   | Linearly interpolate vectors on a sphere    | ``lerp(normalized(origin), normalized(dest), t) * lerp(length(origin), length(dest), t)`` | `Vec3{1, 0, 0}` | `Vec3{0, 1, 0}` | 0.5    | `Vec3{0.71f, 0.71f, 0f}` |
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------+-----------------+--------+--------------------------+
   | :cpp:any:`min_value <Interpolation::min_value>`           | Use the smaller value                       | ``min(origin, dest)``                                                                     | 8               | 3               | --     | 3                        |
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------+-----------------+--------+--------------------------+
   | :cpp:any:`max_value <Interpolation::max_value>`           | Use the larger value                        | ``max(origin, dest)``                                                                     | 8               | 3               | --     | 8                        |
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------+-----------------+--------+--------------------------+
   | :cpp:any:`min_weight <Interpolation::min_weight>`         | Use the value with the smaller weight       | ``t>=0.5 ? origin : dest``                                                                | 8               | 3               | 0.2    | 3                        |
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------+-----------------+--------+--------------------------+
   | :cpp:any:`max_weight <Interpolation::max_weight>`         | Use the value with the larger weight        | ``t>=0.5 ? dest :origin``                                                                 | 8               | 3               | 0.2    | 8                        |
   +-----------------------------------------------------------+---------------------------------------------+-------------------------------------------------------------------------------------------+-----------------+-----------------+--------+--------------------------+

The default value is :cpp:any:`reset_affected <Interpolation::reset_affected>`, which should work for most layers. A good value for continues fields would also be :cpp:any:`lerp <Interpolation::lerp>`, or :cpp:any:`nlerp <Interpolation::nlerp>` for positions. If a layer's values can't be interpolated, e.g. because they are IDs, one of the last 4 values may be used, depending on the specific requirements.

The values for ``origin`` and ``dest`` depend on the operation that triggered the interpolation and the `Ref_type`. For `Mesh_view::split()` a single new vertex is created and ``origin``/``dest`` match the values associated with the edge that was split. Faces are usually only based on a single other face, which will be passed for both ``origin`` and ``dest``. Finally, for edges two appropriate edges are chosen to interpolate from. See `Mesh_view::split()` for more details.

.. note::
   
   How exactly the layers are being updated when the mesh is modified is still subject to change and may be revised once there are more modules implemented and the domain-specific requirements are better understood.


Temporary data
--------------------------------------

It's sometimes desirable to attach temporary data to the mesh, that should behave like a normal layer but doesn't have to be automatically update when the mesh is modified, e.g. because it's discarded and recomputed each time the module is run. While this could be achieved with a layer using `Invalidation::remove_layer` and `Interpolation::dont_care` or by manually removing the layer at the end of the modules `execute()` function, that would be relatively costly.

For this use-case, the `Temporary_layer` class is provided as a simple wrapper over a `std::vector` that implements the `Layer_view` interface.

.. cpp:ucpp_var_ctx:: yggdrasill::Temporary_layer forces
.. cpp:ucpp_var_ctx:: yggdrasill::Temporary_layer ids
.. cpp:ucpp_var_ctx:: yggdrasill::Face f

.. ucpp:include_example:: temporary_layer_example

.. tip::
   
   When using a `Temporary_layer` in a module, it's usually a good idea to define a stateful module and cache the `Temporary_layer` object to avoid allocating a new one for every invocation.
   
   But it's important to remember to :cpp:any:`reset <Temporary_layer::reset>` the layer before using it if the mesh might have been modified.

.. cpp:namespace:: 0


API
--------------------------------------

.. ucpp:file:: yggdrasill/layer.hpp
