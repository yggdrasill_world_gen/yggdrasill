Mesh
======================================

.. cpp:namespace:: yggdrasill

.. cpp:ucpp_var_ctx:: yggdrasill::Edge e

.. cpp:ucpp_var_ctx:: yggdrasill::Vertex v

.. cpp:ucpp_var_ctx:: yggdrasill::Face f

.. cpp:ucpp_var_ctx:: yggdrasill::Const_mesh_view mesh

.. cpp:ucpp_var_ctx:: yggdrasill::Edge other_edge

.. cpp:ucpp_var_ctx:: yggdrasill::Iterator_range left_ring

.. cpp:ucpp_var_ctx:: yggdrasill::Iterator_range origin_ring

.. cpp:ucpp_var_ctx:: yggdrasill::Const_world_view world

.. cpp:ucpp_var_ctx:: yggdrasill::Iterator mesh.faces()


The generated world in Yggdrasill is represented primarily by an unstructured grid, that contains data like elevation, plate-movement or temperature in the form of data layers. This grid is stored as a triangle mesh, to facilitate easy traversal of grid cells. It consists of vertices--points on the surface--which are connected by edges to form triangular faces.

To make the system as flexible as possible, there is a strict division between the topology of the mesh--which vertices, faces and edge exist and how are they connected--and the data that is associated with each of these mesh elements, the :doc:`data layers <layer>`. This even includes the position of each vertex on the sphere.

In addition to the normal (primal) mesh, the system also allows working with its dual mesh. Here vertices and faces switch places, that is each face in the primal mesh becomes a vertex in the dual mesh, which are connected by edges to form cells, that surround a single primal vertex. This dual mesh is especially interesting in the context of `Delaunay triangulations <https://en.wikipedia.org/wiki/Delaunay_triangulation>`_ and their dual,  `Voronoi diagrams <https://en.wikipedia.org/wiki/Voronoi_diagram>`_, which are used in many procedural generation technics.

.. TODO
  replace the graphic with the final one from the paper (once it's written)
.. rst-class:: side-caption
.. figure:: quad_edge_03_dual.png
   :figwidth: 500
   
   A Delaunay triangulation (gray) of the vertices (red) and its dual, consisting of the circumcenters of the faces (blue) and connecting edges (cyan), forming the Voronoi cells.

.. tip::
   
   Most operations in this API can be described as rotations, both around a given vertex or face and of edges themselves. Most functions are named to indicate the direction of rotation--either ``cw`` for clockwise or ``ccw`` for counter-clockwise--but the default direction for all rotations is *counter-clockwise*.


Data Structure
--------------------------------------

This topology information is represented using the `Quad-Edge data structure <https://en.wikipedia.org/wiki/Quad-edge>`_, which provides a large variety of efficient functions to traverse both interconnected vertices and faces.

As the name suggest, the main element of this structure are :cpp:any:`edges <Edge>`. Our edges are directed, which means they know which element they are coming from and which one they are going to.
An edge connects an :cpp:any:`origin <Edge::origin()>` to a destination (short :cpp:any:`dest <Edge::dest()>`) vertex and sits between two faces, one on its :cpp:any:`left <Edge::left()>` and one on its :cpp:any:`right <Edge::right()>`.

.. TODO
  replace the graphic with the final one from the paper (once it's written)
.. figure:: quad_edge_01_basics.png
   :figwidth: 300
   :align: right
   
   A single edge ``e`` with its origin/dest vertices and its left/right face marked.

Since edges are directed, there are always two edges that connect two vertices, one from the :cpp:any:`origin <Edge::origin()>` to the :cpp:any:`destination <Edge::dest()>` and one from the :cpp:any:`destination <Edge::dest()>` to the :cpp:any:`origin <Edge::origin()>`. These edges that point in the opposite direction are called the symmetric edges (short :cpp:any:`sym <Edge::sym()>`) of each other.

As stated above, the API also allows working with the dual of the mesh, which is one of the core features of the quad-edge data structure. For this, we need two dual edges, in addition to the two primal edges, that connect the two faces on either side of the primal edges. This brings us to a total of four edges to describe a single connection, which form a quad-edge. Each quad-edge knows not just the vertices and faces around it, but also the quad-edge that it's a part of. This allows us to easily iterate over them to change direction or switch between the primal and dual mesh. As all iterations, this is modelled as a counter-clockwise :cpp:any:`rotation <Edge::rot()>`. So, rotating an edge e with `e.rot()`, gives us the dual edge that points from its `e.right()` to its `e.left()` face. Rotating twice gives us its symmetric edge, so `e.rot().rot()==e.sym()`, and rotating three times is the same as rotating once in the clockwise direction, so `e.rot().rot()==e.inv_rot()`.

.. TODO
  combine these figure into once single graphic
.. figure:: quad_edge_04_qedge_a.png
   :figwidth: 300
   :align: right
.. figure:: quad_edge_04_qedge_b.png
   :figwidth: 300
   :align: right
   
   A complete quad-edge.

Edges can belong either to the primal mesh (connecting vertices) or the dual mesh (connecting faces). Since this also influences their API--mainly what type `e.origin()`, `e.dest()`, `e.left()` and `e.right()` return--this distinction is also encoded in the type-system, by making `Edge` a class template with the `Edge_type` as its template parameter. Besides :cpp:any:`primal <Edge_type::primal>` and :cpp:any:`dual <Edge_type::dual>`, there is also a specialization that can hold :cpp:any:`any <Edge_type::any>` kind of edge, to simplify generic functions that work on both primal and dual edges. There are also aliases defined for each of these specializations--`Generic_edge`, `Primal_edge`, `Dual_edge`-- that should be used in most simple cases.

There is one final piece of information required to traverse the mesh structure freely, determining the outgoing edges for a given `Vertex` or `Face`, i.e. the edges that have that vertex--or face for dual edges-- as their `e.origin()`. Since each edge has exactly one origin, this partitions the edges into groups, which are referred to here as *edge-rings*. The edges that belong to a ring are stored as a linked-list, with each edge pointing to the next one, rotating in counter-clockwise order around their origin.

In addition, all vertices and faces know one edge that can be used as a starting point to traverse the topologies. For vertices, this is a single edge that is part of their edge-ring, so one edge where for vertex v `v.edge().origin()==v`. But for faces this isn't a dual-edge, but instead one of the counter-clockwise oriented primal edges of that face, so an edge where for face f `f.edge().right()==f`.

.. TODO
  replace the graphic with the final one from the paper (once it's written)
.. figure:: quad_edge_02_edge_ring.png
   :figwidth: 300
   :align: right
   
   Iteration around a vertex edge ring, consisting of four edges, starting with the edge ``e``.


Invalid Elements
~~~~~~~~~~~~~~~~~~~~~~~~

.. caution::
   
   Not all `Edge`/`Vertex`/`Face` objects are valid or part of the current mesh.

It's important to remember that an edge, vertex or face isn't necessarily a valid element nor part of the current mesh. Both because the element might since have been removed from the mesh (or was never part of it) and because some elements may simply just not exists (e.g. an edge in an unclosed mesh may have only one face and not two).

The `Edge`, `Vertex` and `Face` classes are written to be regular types to make them easy to use, which means they are default-constructible. This default-value is used to mark missing elements and has a special constant per type: `no_edge`, `no_vertex` and `no_face`.
These are, for example, returned by `Vertex::edge()` for vertices that are not currently part of any face. They are also often returned by functions when to indicate a missing value or as a default for uninitialized values.

But elements other than those specific three can also be invalid. Either because they were explicitly created as such, e.g. creating a `Vertex{1024}` if there are only 500 vertices in the mesh, or became invalid later because they were removed from the mesh. Since the indices of edges, vertices and faces are often used to later identify them again or to reference data (e.g. as an index in the layers), their indices *have to be stable*. Thus, when an element is removed from the mesh (e.g. by `Mesh_view::collapse`) the resulting gap can't be filled by moving the subsequent elements. Instead, the gap is left, both in the index-space and in the layers, and is later reused when a new edge/vertex/face is created. The validity of an element can be checked using the ``valid()`` method of :cpp:any:`edges <Edge::valid>`, :cpp:any:`vertices <Vertex::valid>` and :cpp:any:`faces <Face::valid>`.
These invalid objects can be freely constructed, passed around and compared, but *the behavior is undefined* if they are used in any other operation! Also, debug-builds may perform additional runtime checks.

The mesh API only ever returns the special constants `no_edge`/`no_vertex`/`no_face` to indicate missing elements, all other returned values are guaranteed to be valid. Additionally, most API functions are guaranteed to always return valid values, which is part of the type-system in the form of the `Valid<T>` wrapper. This wrapper can be used for any of the element types and provides the same operations, except that it isn't default-constructible and can't be set to an empty-state (i.e. ``no_...``).

.. important::
   
   If the mesh API returns an element that is wrapped inside a `Valid<T>` (i.e. `Valid<Edge>`/`Valid<Vertex>`/`Valid<Face>`) it is guaranteed to be valid/used and can be used without any additional checks. But if a function returns `Edge`, `Vertex` or `Face`, the value might be `no_edge`/`no_vertex`/`no_face` and should be checked before using it.


Traversal
--------------------------------------

The first step to traverse the mesh is to find a starting point. This may be a vertex, edge or face retrieved from another system, but there are also a couple of functions in `Const_mesh_view` that can be used to locate specific edges or iterate over all elements of a given type.

.. ucpp:include_example:: mesh_base_iterators_example

.. hint::
   
   Because the mesh may contain unused elements, the largest ever used index returned by the aforementioned ``..._max_index()`` methods (`Const_mesh_view::vertex_max_index()`, `Const_mesh_view::face_max_index()` and `Const_mesh_view::edge_max_index()`) may not actually be what you want to know. These methods are primarily useful to iterate over all elements, but they may be far larger than the actual size of the mesh, as in the number of vertices/faces/edges that are actually being used.

   Therefore, there are also three methods to determine the number of actively used elements: `Const_mesh_view::vertex_used_count()`, `Const_mesh_view::face_used_count()` and `Const_mesh_view::edge_used_count()`


As with the data structure itself, the core element for traversing the mesh are the edges. There are three brooder categories of traversal operations we can use once we have acquired an edge (or vertex/face), each associated with different costs:

1. Accessing the other four edges of the quad-edge, using `e.rot()`, `e.sym()` or `e.inv_rot()`.
2. Accessing the direct neighborhood of the element, like determining an edge's :cpp:any:`origin <Edge::origin()>` or iterating over an :cpp:any:`edge-ring <Edge::origin_ccw()>`.
3. Higher-level functions, composed of the first two, that traverse over a larger part of the mesh, like calculating the area around a vertex or finding a path from one vertex to another.

The first category is the cheapest type of operation. Since the quad-edge affiliation is stored implicitly, accessing other parts of it doesn't actually require complex calculations or memory access, but just a couple of cheap bit-wise operations. This can also be seen directly in their signatures, as these functions don't take any parameters besides the edge itself.

Functions from the second category, on the other hand, do require the mesh as a parameter, as they have to access the connectivity information stored there. These function all require *exactly* one memory read, in addition to some bit-operations.

Some of these functions were already alluded to in the previous section: if we have an edge, we can access its immediate surroundings using `e.origin()`, `e.dest()`, `e.left()` and `e.right()`. On the other hand, if we have a vertex, we can get one of its outgoing edges with `Vertex::edge()`, and if we have a face, we can get one of the primal edges that circle around it with `Face::edge()`.

These functions already allow us to visit parts of the mesh, but to actually traverse the entire mesh, we need to iterate over the edge-rings, i.e. the list of edges that start at the vertices and faces. For this, there are 8 functions in `Edge`, two for each face and vertex directly around it, one that returns the next edge going counter-clockwise (``..._next()``) and one clockwise (``..._prev()``).

.. TODO
  replace the graphic with the final one from the paper (once it's written)
.. rst-class:: side-caption
.. figure:: quad_edge_05_iteration_a.png
   :figwidth: 500
   
   Iteration around the origin vertex/face, both counter-clockwise with `e.origin_next()` and clockwise with `e.origin_prev()`.

.. rst-class:: side-caption
.. figure:: quad_edge_05_iteration_b.png
   :figwidth: 500
   
   Iteration around the destination vertex/face, with `e.dest_next()` and `e.dest_prev()`.
   Note that the returned edges still point towards the same destination.

.. rst-class:: side-caption
.. figure:: quad_edge_05_iteration_c.png
   :figwidth: 500
   
   Iteration around the left or right face with `e.left_next()`, `e.left_prev()`, `e.right_next()`, `e.right_prev()`.
   Note that the iteration is counter-clockwise from the perspective of the respective face, so `e.left_next()` appears to rotate in the opposite direction compared to `e.right_next()`.


While these functions are enough to arbitrarily traverse over the mesh, they are comparatively hard to use and error-prone. Thus, the API also provides some higher-level functionality on top of these functions. The most notable of these are a number of functions that create ranges to iterate over the edge-rings:

.. ucpp:include_example:: mesh_edge_iterators_example


There are also some other small convenience functions on the element types:

- `Edge::vertices()` and `Edge::faces()` returns all vertices and faces of the edge, for both primal and dual edges.
- `Vertex::neighbors()` returns a range to directly iterate over all neighbor-vertices of a vertex.
- `Face::vertices()` returns the three vertices that make up the triangle.

And finally, there are a number of more complex traversal functions, like flood-fill and path-finding algorithms, in the ``<yggdrasill/mesh_utils.hpp>`` header, which are listed in the API listing at the bottom of this page.


Construction
--------------------------------------

.. cpp:ucpp_var_ctx:: yggdrasill::Mesh_view mesh

The first step of every world generation process is usually to create the mesh. For this initial mesh construction, the `Mesh_view` class provides two methods (since we are now modifying the mesh, we need to acquire a mutable view from the `World`).

.. ucpp:include_example:: mesh_construct_example

It's important to remember that working with the mesh during construction requires some extra care because it may be in an *usual* state. For example, during later steps of the generation the mesh will usually be closed, without any holes or unconnected parts. But during construction that is usually not the case. Thus, edges might not have two neighboring faces, but only one. These missing edges form a boundary, which is modelled via a special edge-ring around all boundary-edges that isn't assigned to any face.

.. TODO
  replace the graphic with the final one from the paper (once it's written)
.. figure:: quad_edge_boundary.png
   :figwidth: 300
   :align: right
   
   Virtual edge-ring that contains all boundary faces.


Adding new vertices is a relatively simple operation. But creating a new face is more complex and might fail if one of its two pre-conditions is not met:

- Winding Order: Since the mesh only stores the topological information and not the actual positions of vertices, there is no fixed preferred winding-order for faces. But the winding-order is still required to be consistent and it's *strongly advised* to use a counter-clockwise winding-order to stay compatible with other modules.
- Connecting Islands: To simplify the creation of the mesh, faces can be added in nearly any order. This includes adding multiple unconnected faces, which are only later connected to form the final mesh. But there is one major exception, which is explicitly forbidden: A face can't be added if one of its vertices already has two unconnected faces and it shares no edges with any of them.

.. TODO
  replace the graphic with the final one from the paper (once it's written)
.. figure:: quad_edge_forbidden.png
   :figwidth: 300
   :align: right
   
   Example of the forbidden case. The vertex already has two faces (left/right) and we try to add a new one (dashed line). But because it shares no edges with the other faces and vertices have no position information, it's undecidable where the new edges should be inserted into the edge-ring (A or B).

.. tip::
   
   All modification operations in `Mesh_view` can fail if they would corrupt the mesh, in which case they throw a `Mesh_corruption_exception`. In most cases, this can be safely ignored, e.g. delay adding the face until the error condition is resolved. To avoid the cost and complexity associated with exception-handling, there are additional functions that don't throw this error and only indicated it through their return type, e.g. `Mesh_view::try_add_face()`. However, these function may still throw on other errors, for example if they resizing the data-layers fails.

.. tip::
   
   The ``generate_sphere`` module should be a viable option for most algorithms that want to generate earth-like planets.
   
   It creates a sphere mesh with a fixed radius and vertex count. The vertices are positioned uniformly on the sphere using the Fibonacci sphere algorithm and connected into a Delaunay triangulation. Using its ``perturbation`` parameter, they can also be randomly spaces to give a less ordered and more organic appearance.


Modification
--------------------------------------

Once the mesh was created, it's usually not modified by directly adding new vertices and faces, since that would make it difficult to correctly handle the arbitrary data other modules might have stored in mesh-based layers.

Instead, there are three basic operations that can be used to modify the mesh (and that correctly propagate these changes to any stored data), all of which are working on a single `Primal_edge`:

- Edge Flip: Rotates the edge counter-clockwise in the quad, that is formed by the edges left and right face. Because there are only two ways to split a quad into two triangles, this operation is supported for dual-edges too and behaves identical, no matter which part of a quad-edge it's executed on.

  **Precondition:** The edge can't be a boundary edge, i.e. it has to have both a left and right face.

- Edge Split: Splits the given edge into two, creating a new vertex at the position indicated by a passed interpolation-weight. The position of the new vertex, as well as any other data, is interpolated based on the passed weight, where ``0.0`` stands for the origin- and ``1.0`` for the destination-data. So, ``0.5`` would split the edge right down its center.

  **Precondition:** The edge has to have at least one face, which should be the case for all valid meshes.

- Edge Collapse: Removes the passed edge by merging `e.origin()` into `e.dest()`. So, this operation can be thought of as the inverse of the edge split.

  **Precondition:** The edge has to satisfy the link condition, which means that every vertex that is connected to both `e.origin()` and `e.dest()` also has to be part of either the left or right face of `e`. That is, after we collapsed the edge, there can't be multiple distinct edges between the same two vertices.


.. TODO
  replace the graphic with the final one from the paper (once it's written)
.. figure:: quad_edge_flip.png
   :figwidth: 800
   
   The initial mesh (left) and the mesh after flipping the edge ``e`` (right).

.. TODO
  replace the graphic with the final one from the paper (once it's written)
.. figure:: quad_edge_collapse_split.png
   :figwidth: 800
   
   Both the result of splitting the edge ``e`` at ``0.25`` (left to right) and of removing the new edge ``e'`` again by collapsing it into its destination (right to left).

.. ucpp:include_example:: mesh_modify_example

.. cpp:ucpp_var_ctx:: yggdrasill::Const_mesh_view mesh

.. cpp:namespace:: 0


API
--------------------------------------

.. ucpp:file:: yggdrasill/mesh.hpp

.. ucpp:file:: yggdrasill/mesh_utils.hpp
