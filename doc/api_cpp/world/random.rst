Random
======================================

Because pseudo random numbers are an important part of many procedural generation algorithms, the `World` contains a random number generator (RNG) in addition to the state stored in the mesh and data layers. This generator is exposed as `Random_view` and has two main advantages compared to using other sources of randomness, like `std::rand`:

1. The generator is seeded when the `World` is created and duplicated when the `World` is copied. This means that the `Random_view` can be used to generate random numbers that are reproducible. Both when restarting the application and reusing the same seed and when a previous copy of the `World` is restored and the same modules are executed again.
2. The generator is portable, which means that the same random numbers will be generated on different platforms and even when using different programming languages.

.. tip::
   
   If you need multiple numbers, it is generally advised to use one of the overloads that generates multiple numbers at once. Both to minimize the number of function calls across shared-library-boundaries and to reduce the amount of redundant calculations inside the generator.

.. cpp:ucpp_var_ctx:: yggdrasill::Random_view rng

.. cpp:ucpp_var_ctx:: yggdrasill::World_view world

.. ucpp:include_example:: random_example

API
--------------------------------------

.. ucpp:file:: yggdrasill/random.hpp
