World Model
======================================

.. toctree::
  :hidden:
  
  mesh
  layer
  random

.. cpp:namespace:: yggdrasill

The `World` class stores the current simulation state, which is updated and extended by executing modules that modify it (see :doc:`../generator`).
A world's data is mostly stored in an unstructured grid in the form of a triangular mesh, where each vertex, face and edge may contain further information stored as data layers.
Hence, the state consists of the following parts:

- The :doc:`mesh` that stores the topology of the world, i.e. the vertices, edges and faces of the globe and how they are connected.
- The :doc:`RNG state <random>` that is used to generate pseudo random numbers based on an initial seed.
- A number of :doc:`layers <layer>`, identified by unique IDs, that store the actual data, i.e. the vertices positions, which plate they belong to and how they are moving. A layer can either be connected to a specific part of the mesh or be an unstructured layer, that consist only of a `Dictionary` key-value-store.

Since they store the complete state of the simulation, `World` object tend to be pretty large. But being able to copy them is important save and restore previous stages of the simulation, which is for example required to implement undo/redo functionality in the UI. To enable this, `World` implements a copy-on-write semantic. This means that copying a `World` object initially only creates a shallow copy and multiple objects may share the same mesh, RNG-state or layers. A deep copy of the individual parts is only performed when that specific part is modified and shared between multiple worlds.

To implement this copy-on-write semantic, the system needs to keep track of all mutable references to the stored parts. Because of this, only immutable views are directly accessible using the member functions in `Const_world_view` and mutable views have to be explicitly acquired and released again.

Mutable views of the parts can be acquired using the `World_view::lock_random()`, `World_view::lock_mesh()` and `World_view::lock_layer(...)` functions, which return a RAII-wrapper that automatically releases the write access on destruction and should immediately be deconstructed using structured bindings to access the actual view:

.. cpp:ucpp_var_ctx:: yggdrasill::World_view world

.. ucpp:include_example:: world_lock_sample

As a side effect of the copy-on-write system, the individual parts of the state have a version number of type `Data_version`, that can be access using `Const_world_view::mesh_version()` and `Const_world_view::layer_version()`. The returned number changes each time a mutable reference to that part is unlocked/released, which can be used to efficiently check for possible modifications. See also: `Const_world_view::mesh_modified()` and `Const_world_view::layer_modified()`

.. cpp:namespace:: 0

API
--------------------------------------

.. ucpp:file:: yggdrasill/world.hpp
