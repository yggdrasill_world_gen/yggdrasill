Generators
======================================

.. cpp:namespace:: yggdrasill

Worlds in Yggdrasill are generated iteratively by sequentially executing multiple modules on a `World` object to populate it with data or modify existing information. Each of these modules usually represents a small step in the larger generation process, like creating a first rough mesh,  moving tectonic plates a small amount, process plate interactions, etc.

A module can be loaded from shared libraries as a plugin or be included directly in the main application. There are multiple ways their functionality can be defined (see :doc:`modules`) but ultimately they provide two functions of interest to us:

.. cpp:function:: void execute(World_view world, Const_dictionary_view configuration)
   
   This function is executed to apply the module to the given `world`, e.g. to move tectonic plates or to increase/decrease the mesh resolution.
   
   :param world: In-Out parameter. The world object that contains the initial state and will be modified by the module.
   :param configuration: In parameter. A structure that contains configuration parameters to influence and control the modules behaviour.

.. cpp:function:: void validate(Dictionary_view configuration)
   
   This function may be called before `execute`, to check if the given `configuration` matches the requirements of the module, i.e. does it contain all mandatory options and do they have the expected type and value range. The configuration may also be modified to include any optional parameters or fix any defects that can be resolved automatically.
   
   :param config: In-Out parameter. A configuration that will later be passed to `execute` and should be validated against this module.


Since modules might have to be loaded from plugins or contain private state that needs managing, they are not used directly but are instead managed by the `Generator` class. This is the main type we'll use for generating worlds, usually by first adding a couple of modules and then retrieving the references to the modules (`Module_view`) and executing them using `Module_view::execute`.

.. cpp:ucpp_var_ctx:: yggdrasill::Generator gen

.. cpp:ucpp_var_ctx:: yggdrasill::Module_view generate_sphere_module

.. ucpp:include_example:: generator_sample

.. cpp:namespace:: 0

API
--------------------------------------

.. ucpp:file:: yggdrasill/generator.hpp
