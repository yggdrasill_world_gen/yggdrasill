Error Handling
======================================

.. cpp:namespace:: yggdrasill

Failures are usually reported using exception, derived from `Exception`, and functions provide a strong exception guarantee, so if an operation fails it doesn't have any effect. The only notable exception to this rule are `Module_view::validate` and `Module_view::execute`, which execute user-defined plugins and are limited to the weak exception guarantee, for ease of implementation. Therefore, it's generally advised to copy the `World` state before calling `Module_view::execute` and discard the passed object if the execution failed because it might have been partially modified.

Since the interface to the implementation is written in C and not all client languages may support exceptions, no exceptions may cross the API boundary. Instead, the error-code based data structure `Error` is used to communicate failures to other parts of the library. This type encapsulates both an error code that indicates the general category of error and a more detailed human-readable message.

There are also some functions that use this facility instead of exceptions. Usually to avoid the inherent costs of throwing and catching exceptions for operations that are expected to fail relatively frequently, like `Mesh_view::try_split`. These functions accept an additional parameter of type `Error*` and should be called with a stack-allocated `Error` object, that will then be populated with any error that might've occurred.

.. cpp:ucpp_var_ctx:: yggdrasill::Error error

.. ucpp:include_example:: error_handling_cpp


To report errors from your module functions, you can just throw an exception derived from `Exception` and the library will take care to wrap it correctly. In the rare event that you have to return errors directly through an `YGDL_Error*`, e.g. if you are implementing a function called directly from C, it might be a good idea to use the `set_error` helper function listed below.

.. cpp:namespace:: 0

API
--------------------------------------

.. ucpp:file:: yggdrasill/error.hpp
