Configuration
======================================

.. cpp:namespace:: yggdrasill

.. cpp:ucpp_var_ctx:: yggdrasill::Dictionary cfg

.. cpp:ucpp_var_ctx:: yggdrasill::Array_view array

.. cpp:ucpp_var_ctx:: yggdrasill::Const_world_view world

.. cpp:ucpp_var_ctx:: yggdrasill::Const_dictionary_value_view cfg["sequence"]

.. cpp:ucpp_var_ctx:: yggdrasill::Const_dictionary_value_view cfg["a"]

.. cpp:ucpp_var_ctx:: yggdrasill::Const_dictionary_value_view cfg["b"]

.. cpp:ucpp_var_ctx:: yggdrasill::Const_dictionary_value_view cfg["c"]

.. cpp:ucpp_var_ctx:: yggdrasill::Const_dictionary_value_view cfg["d"]

As we've seen in :doc:`generator`, we can pass a configuration data structure into the ``generate`` function, to control its behavior.
Now we'll take a closer look at this structure, how to populate it and how to parse it, when we want to write our own modules.

The structure is similar to JSON. It's core, and the type we have to pass into ``generate``, is the `Dictionary` type. It represents a key-value store with string keys and values that are either numbers/strings, sequences of values (`Array_view`) or another nested key-value store. But contrary to JSON, the structure is strongly typed with the following supported value types:

- `bool`
- `std::int8_t`, `std::int32_t` and `std::int64_t`
- `float` and `double`
- `Vec2` and `Vec3`
- `String`
- `Dictionary`
- `Array_view`

Populating the Configuration
--------------------------------------

The structure can be populated and modified using nested ``[]``-accesses:

.. ucpp:include_example:: dictionary_sample_create

The ``[]``-access and assignments automatically cause the construction of the matching type (right side of assignments or `Dictionary` for `cfg["string_keys"]`), overriding any previous value.

More complicated structured, like nested arrays, need to be constructed explicitly:

.. ucpp:include_example:: dictionary_sample_array

Most modules will have requirements for the allowed values, as well as default values for missing or invalid inputs. To validate your configuration and populate it with default values, you can call `Module_view::validate` on a handle returned by `Generator`.


Reading the Configuration
--------------------------------------

If you are writing your own module, you'll want to also parse this structure to accept configuration parameters.

Most C++-modules are written with a single `execute(World_view world, Any_dictionary auto cfg)`, that is callable with both `Dictionary&` (during validation) and `const Dictionary&` (during execution). The access methods are overloaded accordingly and throw on ``const`` objects or if explicitly requested and override invalid/missing values with a default otherwise. The exception thrown on errors is `Type_mismatch_exception`. Note that `INF` and `NaN` are forbidden when validating floating-point types and need to be explicitly allowed using `nan_allowed` and `inf_allowed` in the validation options.

.. ucpp:include_example:: dictionary_sample_read

Calling :cpp:any:`get() <Const_dictionary_value_view::get()>` with a different type than what is stored will try to convert the existing value if possible. During validation, this will always succeed, but might override the value with the default. But during execution, a conversion is not always possible.	The overload that returns by-value has more leeway because it also takes implicit conversions into account, as long as there would be no information loss, even if the stored value can't be overridden. The reference-based overload is more restricted, and so should only be used if required because it returns a (possibly mutable) reference to the stored value, which means that the type has to be an exact match.

.. cpp:namespace:: 0

API
--------------------------------------

Dictionary
~~~~~~~~~~~

.. ucpp:file:: yggdrasill/dictionary.hpp


Array
~~~~~~~~~~~

.. ucpp:file:: yggdrasill/array.hpp
