Writing Modules
======================================

.. cpp:namespace:: yggdrasill

As described in :doc:`generator`, modules are primarily defined by their unique ID and the two functions `validate` and `execute`. To define a module, you have to provide an implementation for these functions and a global variable of type `Module_info` that references them, which can be created by calling the `to_module_info` function.

While the canonical signature of `validate` and `execute` is the one shown in the linked documentation, `to_module_info` supports multiple ways to define them, depending on your requirements.

Both functions can optionally return `Error` instead of `void`, if you don't want to pay for throwing and catching exceptions, which is especially interesting for `validate`. Note that even if the function is defined to return an `Error`, any exceptions thrown from the function are still caught and wrapped into a corresponding `Error` type.

The `validate` function may also take a `World_view` as its first parameter, that will always be set to `nullptr`. This allows `execute` and `validate` to share a single template function as an implementation, that only differs in the type of its second parameter, `Dictionary_view` for `validate` and `Const_dictionary_view` for `execute`.

And finally, it's also possible to provide a class type instead of free functions as a module implementation. The type will be instantiated when the module is added to a `Generator` and automatically destructed when it's unloaded. This can be utilized to cache information across multiple calls to `execute` or reuse heap allocations. Though, it should be noted that there is no guarantee on the order and number of calls to `validate` and `execute` that a client will perform. So, the current state of the object should have no influence on the execution result and no pointers or references to the passed world or configuration must outlive the call.

Like in the C-API, module definitions have to be explicitly exported if a shared library is used, by calling the :c:macro:`YGDL_REGISTER_PLUGIN_MODULES` macro to generate a ``YGDL_init_plugin`` function.

Taking all of this into account, a simple plugin may be defined as follows:

.. cpp:ucpp_var_ctx:: yggdrasill::Layer_definition position_def

.. cpp:ucpp_var_ctx:: yggdrasill::World_view world

.. cpp:ucpp_var_ctx:: yggdrasill::Dictionary_view cfg

.. cpp:ucpp_var_ctx:: yggdrasill::Const_mesh_view mesh

.. cpp:ucpp_var_ctx:: yggdrasill::Layer_view positions

.. cpp:ucpp_var_ctx:: yggdrasill::Layer_definition

.. code-block:: cpp
    :linenos:
    
    // my_module.cpp
    #include <yggdrasill/layer.hpp>
    #include <yggdrasill/mesh.hpp>
    #include <yggdrasill/module.hpp>
    #include <yggdrasill/world.hpp>
    
    using namespace yggdrasill;
    
    namespace my_namespace {
        static constexpr auto position_def = Layer_definition<Vec3, Ref_type::vertex>("position")
                                                 .interpolation(Interpolation::nlerp)
                                                 .invalidation(Invalidation::reset_affected);
        
        static void my_module_impl(World_view world, Any_dictionary auto cfg)
        {
            const auto config_value = cfg["my_module"]["option"].get(1024, {.min = 16});
            
            if(!world)
                return;
            
            auto mesh        = world.mesh();
            auto [positions] = world.lock_layer(position_def);
            
            for(auto&& [vertex, position] : range(mesh, positions)) {
                // ...
            }
        }
        
        // Create a module description, instantiating my_module_impl using Dictionary_view and Const_dictionary_view as the second parameter.
        // The object is used below for shared libraries and can be accessed using a forward declaration for static builds.
        extern const auto my_module = to_module_info<my_module_impl, my_module_impl>("my_module");
    }
    
    // register the module, if we are building a shared library (does nothing on static builds)
    YGDL_REGISTER_PLUGIN_MODULES(&my_namespace::my_module)


If you are using CMake to build your project, there is also a helper function ``yggdrasill_def_module`` to generate a project for a module. The complete CMakeLists.txt for the module defined above would be:

.. code-block:: cmake
   :linenos:
   
   cmake_minimum_required(VERSION 3.14 FATAL_ERROR)
   
   yggdrasill_def_module(
       NAME my_module
       SOURCE my_module.cpp # ... + any other source files
       # Optional parameters
       #   DEPENDENCIES followed by any targets the module depends on, in addition to the yggdrasill library
       #   COMPILE_OPTIONS followed by any compiler options specific to this module, e.g. -fno-math-errno -funsafe-math-optimizations
   )

.. cpp:namespace:: 0

API
--------------------------------------

.. ucpp:file:: yggdrasill/module.hpp
