Serialization
======================================

.. cpp:namespace:: yggdrasill

Yggdrasill provides a `Serializer` and `Deserializer` type to persist simulation states in the file-system. The files created and read by these are ZIP-Archives that can contain multiple `World` objects, as well as :cpp:any:`configurations <Dictionary>` and arbitrary user-defined entries. This enables them to be used both to export a single simulation result and more complex collections of multiple snapshots and configurations (e.g. editor save files).

The `Serializer` can be used both for writing entries and to read them. This allows archive files to be used as temporary storage for data that may be too large to be stored in main memory. For this use-case, and for long-lived `Deserializer` objects in general, it is usually advised to regularly call `Deserializer::shrink_read_caches()` to free any currently used cached file entries.

The processing of ZIP-files is implemented using the `miniz <https://github.com/richgel999/miniz>`_ library, that has to either be installed on the local system or will be downloaded and build automatically if it isn't found. If this dependency can't be satisfied or serialization is not required, this part of the API can be disabled by setting the CMake option ``YGGDRASILL_SERIALIZATION`` to ``OFF``. If serialization support is disabled, the functions are still available, but calling them will return error codes and constructing the `Serializer`/`Deserializer` objects will throw.

Usage Example
--------------------------------------

.. cpp:ucpp_var_ctx:: yggdrasill::Serializer serializer
.. cpp:ucpp_var_ctx:: yggdrasill::Deserializer deserializer
.. cpp:ucpp_var_ctx:: Byte_reader reader

Writing archives
~~~~~~~~~~~~~~~~~~~~~~~~

.. ucpp:include_example:: serialization_sample_write

Reading archives
~~~~~~~~~~~~~~~~~~~~~~~~

.. ucpp:include_example:: serialization_sample_read

.. cpp:namespace:: 0

API
--------------------------------------

.. ucpp:file:: yggdrasill/serialization.hpp
