# -- required to use ucpp -----------------------------------------------------
import os
import sys
sys.path.insert(0, os.path.abspath('.'))

extensions = ['ucpp', 'sphinx_toolbox.collapse']
html_static_path = ['_static']

def ucpp_resolve(file, line):
    file = file[file.find('doc/../')+7:]
    return "https://gitlab.com/yggdrasill_world_gen/yggdrasill/-/blob/develop/"+file+"#L"+line


# -- Project information -----------------------------------------------------

project = 'Yggdrasill'
html_title = "Yggdrasill 0.0.1"
copyright = '2021, Florian Oetke'
author = 'Florian Oetke'

# The full version, including alpha/beta/rc tags
release = '0.0.1'

html_logo = 'logo.png'


# -- General configuration ---------------------------------------------------

# Add any paths that contain templates here, relative to this directory.
templates_path = []

# The suffix of source filenames.
source_suffix = '.rst'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_static']

# The reST default role (used for this markup: `text`) to use for all documents.
default_role = 'cpp:expr'

cpp_index_common_prefix = ['yggdrasill::', 'std::']

highlight_language = 'c++'

primary_domain = 'cpp'

# -- Options for HTML output -------------------------------------------------

html_theme = 'furo'
pygments_style = "sphinx"
pygments_dark_style = "monokai"

html_css_files = ['css/custom.css', 'fork-awesome/css/fork-awesome.min.css']
html_js_files = ['Hyphenopoly_Loader.js', 'custom.js']

html_permalinks_icon = ''
