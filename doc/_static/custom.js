var labels_by_text = {};

function ready() {
	var li = document.getElementsByClassName("tab-label");
	for (const label of li) {
		label.onclick = onLabelClick;
		const text = label.textContent;
		if (!labels_by_text[text]) {
			labels_by_text[text] = [];
		}
		labels_by_text[text].push(label);
	}

	console.log("Cookies: "+document.cookie);
	var keyValuePairs = document.cookie.split(';');
	for(var i = 0; i < keyValuePairs.length; i++) {
		var name = keyValuePairs[i].substring(0, keyValuePairs[i].indexOf('='));
		var value = keyValuePairs[i].substring(keyValuePairs[i].indexOf('=')+1);
		if(name.startsWith("tab_")) {
			console.log("Setting tab "+name.substring(4)+" to "+value);
			for (label of labels_by_text[name.substring(4)]) {
				label.previousSibling.checked = value;
			}
		}
	}
}

function onLabelClick() {
	// Activate other labels with the same text.
	for (label of labels_by_text[this.textContent]) {
		label.previousSibling.checked = true;
	}

	for (var key in labels_by_text) {
		var checked = labels_by_text[key][0].previousSibling.checked;
		document.cookie = "tab_"+key+"="+checked+"; path=/";
	}
	
	console.log("Cookies: "+document.cookie);
}

document.addEventListener("DOMContentLoaded", ready, false);

/*
Hyphenopoly.config({
	require: {
		"en": "FORCEHYPHENOPOLY",
		"en-us": "FORCEHYPHENOPOLY"
	},
	setup: {
		selectors: {
			".article-container": {}
		}
	}
});
*/
