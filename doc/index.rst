Yggdrasill
======================================

.. toctree::
  :hidden:
  
  self
  
.. toctree::
  :caption: C++ API
  :hidden:
  :numbered:

  api_cpp/generator
  api_cpp/configuration
  api_cpp/error_handling
  api_cpp/world/index
  api_cpp/modules
  api_cpp/serialization
  api_cpp/utilities

.. toctree::
  :caption: C API
  :hidden:
  :numbered:

  api_c/generator
  api_c/configuration
  api_c/error_handling
  api_c/world/index
  api_c/modules
  api_c/serialization
  api_c/utilities

.. toctree::
  :caption: Development
  :hidden:
  
  license
  Repository <https://gitlab.com/yggdrasill_world_gen>

Yggdrasill is a work-in-progress world generation toolkit that currently consists of four main parts:

- A low-level C-API, designed for easy portability and as a basis for high-level APIs written in other languages
- A high-level C++-API
- Several modules that solve different aspects of the world-generation process
- An interactive editor that utilizes these modules to generate worlds in real-time

The APIs and modules are published under the MIT license at the `main repository <https://gitlab.com/yggdrasill_world_gen/yggdrasill>`_.
While the editor is currently closed source, as it's still in a very early stage of development, the latest version is available as a `WebGL <https://cranky-swartz-3250cb.netlify.app>`_ (requires a very recent browser), `Windows <https://cranky-swartz-3250cb.netlify.app/windows.zip>`_ and `Linux <https://cranky-swartz-3250cb.netlify.app/linux.zip>`_ application.


This document
--------------------------------------

The API documentation is written to be as concise as possible and is heavily based on usage examples.
Therefore, it demonstrates the most common use case but does not utilize every possible operation. Because of this, there will usually be an API documentation block at the end of every section that contains the complete public API.

Since Yggdrasill supports multiple programming languages, the rest of this document is split into multiple chapters, one per API.

C++ API
~~~~~~~~~

The C++ API is currently the main target and the one you most likely want to use. It is a thin header-only wrapper over the C-API, both to guarantee a relatively stable ABI and to define a common ground for future additional language bindings.

In this API, there are multiple wrapper types that denote the ownership of the underlying object:

- Value-based wrappers: These are normal C++ types that are usually allocated by the user with automatic storage duration and can be moved (and in some cases copied) like any other types. For types that support copying, a deep-copy is performed or a copy-on-write schema is used that behaves like a deep-copy was performed. In most cases, the wrapper-type only contains a handle to a heap-allocated object managed by the C-API functions. So, the objects are generally small and cheap to move, and heap-allocating them should typically be avoided. Examples of this kind of wrapper are: `yggdrasill::Generator`, `yggdrasill::String`, `yggdrasill::Dictionary` and `yggdrasill::World`
- ``Const_..._view`` wrappers: These are non-owning const references to objects that are either part of a larger structure (e.g. values of an `yggdrasill::Dictionary`) or reference a value-based wrapper. They can be copied/moved/assigned but behave like pointers, i.e. they don't modify the original object but rebind the view. Examples of these include: `yggdrasill::Const_world`, `yggdrasill::Const_layer_view`
- ``..._view`` wrappers: Like the const-views, these only reference objects owned/managed by other parts of the code, except that the referenced objects can be modified using these views. Examples of these are: `yggdrasill::Array_view`, `yggdrasill::Mesh_view` and `yggdrasill::Layer_view`
- ``..._ref`` wrappers: Like the views, these are non-owning references, but unlike the views they behave like references, i.e. assignments change the referenced object instead of rebinding. These are generally used sparingly and only with the configuration API and thus the only wrappers of this type are: `yggdrasill::Array_ref`, `yggdrasill::Dictionary_ref`, `yggdrasill::Dictionary_value_ref`

Some types only have the first kind of wrapper, but most have all but the ``..._ref`` kind. In this case, the wrapper types form a class hierarchy ``Const_x_view <- X_view <- X_ref <- X``, so implicit conversions to the less "powerful" wrappers are supported.

**Thread Safety:** Following the usual best practices, const methods may be called concurrently without additional synchronization, unless otherwise stated in the documentation.

**Exception Safety:** Except where otherwise specified, all functions provide a strong exception guarantee. In all other cases, at least a weak exception guarantee is provided, i.e. partial modifications on failures are possible. This is the case for both modifications of the mesh and the execution of modules. But the partial modifications are localized to the `yggdrasill::World` object they are working on, which means the partial-updates can easily be rolled back and the partially modified state may actually be of use to debug the cause of the failure, e.g. to inspect the partially modified mesh. Explicitly excluded from this is `std::bad_alloc`, which, except for rare cases, is not explicitly handled and will terminate the process.

C API
~~~~~~~~~

The C API, while feature-complete, is not designed to be used directly. Instead, it's intended as a common interface to the underlying implementation that should be wrapped by a more user-friendly language-specific API, like the API-Wrapper provided for C++.

So, this is the section you'll want to look at if you are currently writing a new language binding.

Because it's not meant for normal users and its usage is often more complicated, usage examples for the C-API are usually omitted.

**Thread Safety:** Following the usual best practices, functions taking their arguments as const may be called concurrently without additional synchronization, unless otherwise stated in the documentation.

**Null Safety:** Unless otherwise stated, all pointers passed into functions have to be non-NULL and refer to valid objects.

**Exception Safety:** Functions that can fail have a `YGDL_Error*` as their last parameter; all other functions always succeed, except for resource exhaustion (`std::bad_alloc`) which will terminate the process. Except where otherwise specified, these functions all provide a strong exception guarantee. In all other cases, at least a weak exception guarantee is provided, i.e. partial modifications on failures are possible. This is the case for both modifications of the mesh and the execution of modules. But the partial modifications are localized to the `YGDL_World` object they are working on, which means the partial-updates can easily be rolled back and the partially modified state may actually be of use to debug the cause of the failure, e.g. to inspect the partially modified mesh.
