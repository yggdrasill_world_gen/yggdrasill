Utilities
======================================

The following is a list of all utility functions and smaller helper types -- like strings, math-vectors and iterators, used by other parts of the library.

API
--------------------------------------

String
~~~~~~~~~~~

.. ucpp:file:: yggdrasill/string.h

Commons
~~~~~~~~~~~

.. ucpp:file:: yggdrasill/commons.h

