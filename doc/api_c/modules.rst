Writing Modules
======================================

.. cpp:namespace:: yggdrasill

As described in :doc:`generator`, modules are primarily defined by their unique ID and the two functions `validate` and `execute`. To define a module, you have to provide an implementation for these functions and a global variable of type `YGDL_Module_info` that references them. Bindings for other languages also provide convenience functions to simplify this process, like the `yggdrasill::to_module_info` function template of the C++ API.

There are two main ways to export your `YGDL_Module_info` objects to be used for generation.

If you are linking statically or are using a module directly in the application where you defined it, it's enough to forward declare the global variable in a header and then reference it to add it to a generator.

Modules can also be packaged into shared library plugins, to dynamically load them at runtime. When a shared library is loaded by a generator, its entry function ``YGDL_init_plugin(YGDL_Plugin_init_handle*)`` is called, that can be used to add modules to the generator that loaded it by calling `YGDL_plugin_module_add`. You can manually define this function in your shared library if you need to execute code on load. But there is also the :c:macro:`YGDL_REGISTER_PLUGIN_MODULES` macro that can be called with a comma separated list of pointers to  `YGDL_Module_info` objects, that generates an init function to register all of them.

.. cpp:namespace:: 0

API
--------------------------------------

.. ucpp:file:: yggdrasill/module.h

