Configuration
======================================

As we've seen in :doc:`generator`, we can pass a configuration data structure into the ``YGDL_generator_module_execute`` function, to control its behavior.
Now we'll take a closer look at this structure, how to populate it and how to parse it, when we want to write our own modules.

The structure is similar to JSON. It's core, and the type we have to pass into ``YGDL_generator_module_execute``, is the `YGDL_Dict` type. It represents a key-value store with string keys and values that are either numbers/strings, sequences of values (`YGDL_Array`) or another nested key-value store. But contrary to JSON, the structure is strongly typed with the following supported value types:

- `YGDL_Bool`
- `int8_t`, `int32_t` and `int64_t`
- `float` and `double`
- `YGDL_Vec2` and `YGDL_Vec3`
- `YGDL_String`
- `YGDL_Dict`
- `YGDL_Array`

API
--------------------------------------

Dictionary
~~~~~~~~~~~

.. ucpp:file:: yggdrasill/dictionary.h

Array
~~~~~~~~~~~

.. ucpp:file:: yggdrasill/array.h

