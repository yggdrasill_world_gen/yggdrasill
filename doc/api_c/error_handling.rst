Error Handling
======================================

Since the C-API can't use exceptions, error handling is instead purely based on error-codes in the form of the `YGDL_Error` struct shown below.
Every function that might fail takes an additional `YGDL_Error*` as the last parameter, that is populated with information about any error that occurred during execution. These functions also accept a NULL-Pointer if errors don't need to be handled. But in most cases, you would call them with a pointer to a stack-allocated instance and check `YGDL_Error::code` afterwards. Any pointers or structures returned by the function may only be used if the set code equals `YGDL_ERROR_CODE_SUCCESS`.

.. ucpp:include_example:: error_handling_c


If you are writing your own functions, you may also accept such a `YGDL_Error*` parameter and populate it with an error like this:

.. ucpp:include_example:: error_handling_set_c


Most functions provide a strong exception guarantee, so if an operation fails, it doesn't have any effect. The only notable exception to this rule are `YGDL_generator_module_validate` and `YGDL_generator_module_execute`, which execute user-defined plugins and are limited to the weak exception guarantee, for ease of implementation. Therefore, it's generally advised to copy the `YGDL_World` state before calling `YGDL_generator_module_execute` and discard the passed object if the execution failed because it might have been partially modified.


API
--------------------------------------

.. ucpp:file:: yggdrasill/error.h

