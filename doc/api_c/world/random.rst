Random
======================================

Because pseudo random numbers are an important part of many procedural generation algorithms, the `YGDL_World` contains a random number generator (RNG) in addition to the state stored in the mesh and data layers. This generator is exposed as `YGDL_Random` and has two main advantages compared to using other sources of randomness, like `rand`:

1. The generator is seeded when the `YGDL_World` is created and duplicated when `YGDL_world_copy()` is called. This means that the `YGDL_Random` can be used to generate random numbers that are reproducible. Both when restarting the application and reusing the same seed and when a previous copy of the `YGDL_World` is restored and the same modules are executed again.
2. The generator is portable, which means that the same random numbers will be generated on different platforms and even when using different programming languages.

The C-API for random number generation is kept relatively simple and primarily consists of a couple of functions that take a `YGDL_Random`, a set of parameters and an output array and fill that array with randomly generated numbers. Higher-level abstraction can then be implemented in the language-specific bindings.

API
--------------------------------------

.. ucpp:file:: yggdrasill/random.h