World Model
======================================

.. toctree::
  :hidden:

  mesh
  layer
  random

The current simulation state is stored in a heap object referenced by the opaque handle `YGDL_World`, which is updated and extended by executing modules that modify it (see :doc:`../generator`).
A world's data is mostly stored in an unstructured grid in the form of a triangular mesh, where each vertex, face and edge may contain further information stored as data layers.
Hence, the state consists of the following parts:

- The :doc:`mesh` that stores the topology of the world, i.e. the vertices, edges and faces of the globe and how they are connected.
- The :doc:`RNG state <random>` that is used to generate pseudo random numbers based on an initial seed.
- A number of :doc:`layers <layer>`, identified by unique IDs, that store the actual data, i.e. the vertices positions, which plate they belong to and how they are moving. A layer can either be connected to a specific part of the mesh or be an unstructured layer, that consist only of a `YGDL_Dict` key-value-store.

Since they store the complete state of the simulation, the heap-object referenced by a `YGDL_World` tends to be pretty large. But being able to copy them is important save and restore previous stages of the simulation, which is for example required to implement undo/redo functionality in the UI. To enable this, `YGDL_World` implements a copy-on-write semantic. This means that calling `YGDL_world_copy()` initially only creates a shallow copy and multiple worlds may share the same mesh, RNG-state or layers. A deep copy of the individual parts is only performed when that specific part is modified and shared between multiple worlds.

To implement this copy-on-write semantic, the system needs to keep track of all mutable references to the stored parts. Because of this, mutable pointers need to be explicitly acquired by calling one of the ``YGDL_world_*_acquire()`` functions below. These pointers **have** to be freed again as soon as they are no longer needed, by calling the corresponding ``YGDL_world_*_release()`` function. While a mutable pointer is held calling `YGDL_world_copy()` is forbidden. Additionally, holding a mutable pointer for longer than required may incur additional cost in the form of unnecessary internal copies and can mask bugs, as this may delay validation of the modified data.

When creating a binding for a new language, it is generally advised to use the language's resource-management-facilities (e.g. RAII for C++) to automate this process.

As a side effect of the copy-on-write system, the individual parts of the state have a version number of type `YGDL_Data_version`, that can be access using `YGDL_world_mesh_version()`, `YGDL_world_random_version()`, `YGDL_world_layer_version()` and `YGDL_world_unstructured_layer_version()`. The returned number changes each time a mutable reference to that part is unlocked/released, which can be used to efficiently check for possible modifications.

.. admonition::  Note for languages with stronger ownership semantics
   :class: hint

   While multiple world objects may share immutable references to the same internal parts, once a mutable reference is acquired it becomes uniquely owned by that specific `YGDL_World` object. But multiple immutable and mutable references to the same internal part are still permitted by this API, as long as they are acquired using the *same* `YGDL_World` handle.

   This may be used to implement stronger pointer semantics, e.g. to forbid immutable references to coexist with any other reference to the same internal part, by adding additional guard conditions around the immutable-accessor and acquire functions.

   It is important to note however, that modifying the `YGDL_Mesh` may have to internally acquire mutable references to the structured layers to apply resizing, invalidation and interpolations. Hence, if such a stronger guarantee is desired, calling `YGDL_world_mesh_acquire()` has to be mutually exclusive with holding pointers to *any* layers of that world object.


API
--------------------------------------

.. ucpp:file:: yggdrasill/world.h
