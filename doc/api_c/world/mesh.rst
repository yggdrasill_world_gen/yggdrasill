Mesh
======================================

The generated world in Yggdrasill is represented primarily by an unstructured grid, that contains data like elevation, plate-movement or temperature in the form of data layers. This grid is stored as a triangle mesh, to facilitate easy traversal of grid cells. It consists of vertices--points on the surface--which are connected by edges to form triangular faces.

To make the system as flexible as possible, there is a strict division between the topology of the mesh--which vertices, faces and edge exist and how are they connected--and the data that is associated with each of these mesh elements, the :doc:`data layers <layer>`. This even includes the position of each vertex on the sphere.

In addition to the normal (primal) mesh, the system also allows working with its dual mesh. Here vertices and faces switch places, that is each face in the primal mesh becomes a vertex in the dual mesh, which are connected by edges to form cells, that surround a single primal vertex. This dual mesh is especially interesting in the context of `Delaunay triangulations <https://en.wikipedia.org/wiki/Delaunay_triangulation>`_ and their dual,  `Voronoi diagrams <https://en.wikipedia.org/wiki/Voronoi_diagram>`_, which are used in many procedural generation technics.

.. TODO
  replace the graphic with the final one from the paper (once it's written)
.. rst-class:: side-caption
.. figure:: ../../api_cpp/world/quad_edge_03_dual.png
   :figwidth: 500

   A Delaunay triangulation (gray) of the vertices (red) and its dual, consisting of the circumcenters of the faces (blue) and connecting edges (cyan), forming the Voronoi cells.

.. tip::

   Most operations in this API can be described as rotations, both around a given vertex or face and of edges themselves. Most functions are named to indicate the direction of rotation--either ``cw`` for clockwise or ``ccw`` for counter-clockwise--but the default direction for all rotations is *counter-clockwise*.


The mesh API is based on the `quad-edge data structure <https://www.cs.cmu.edu/afs/andrew/scs/cs/15-463/2001/pub/src/a2/quadedge.html>`_, which structure and usage is already described in great detail in the :doc:`C++ part <../../api_cpp/world/mesh>` of this documentation. Additional details about the implementation are also available in `projects blog <https://second-system.de/2021/04/11/data_structure>`_.

The C-API follows a data-driven design approach. The `YGDL_Vertex`, `YGDL_Face` and `YGDL_Edge` structs are just indices into a couple of flat arrays, which hold the actual data.

All traversal operations are based on edges and only actively read `YGDL_Mesh::primal_edge_next` and `YGDL_Mesh::dual_edge_next`. The other arrays are usually just used at the beginning and end of a traversal to retrieve a corresponding edge for a vertex/face or wise-versa.

The two main operations during traversal are rotating an edge to access other parts of its quad-edge, which is implemented purely through integer bit-operations, and iterating over edge-rings, which requires a single additional read from one of the two aforementioned arrays. There are a number of functions for edge-ring iteration, which are described in the C++-API documentation, but all of these are implemented in terms of a single call to `YGDL_mesh_edge_origin_next()` in addition to a small number of edge rotations.

.. TODO
  replace the graphic with the final one from the paper (once it's written)
.. rst-class:: side-caption
.. figure:: quad_edge_04_qedge_bits.png
   :figwidth: 500

   Visualization of the bit-wise operations applied to :cpp:any:`YGDL_Edge::mask` to rotate it.

Like most of the C-API, the number of operations is kept relatively low, with the express intention that high-level APIs implement additional, possibly language specific, features to simplify its use. For example, the C++-API implements iterators and ranges on top of the simple ``..._next()`` and ``..._prev()`` functions.

.. admonition::  Note for languages with support for compile-time execution
   :class: hint

   Since accessing the different parts of a quad-edge is implemented through a series of simple integer arithmetic, most of the operations can be heavily optimized and partly executed/folded at compile-time. For this purpose, these functions are implemented inline and marked as :c:macro:`YGDL_CONSTEXPR`, which is sufficient for all currently supported languages. However, other languages might require different definitions or implementing them directly in the target language. This is explicitly supported and the implementation of these inline functions are guaranteed to be stable.


API
--------------------------------------

.. ucpp:file:: yggdrasill/mesh.h
