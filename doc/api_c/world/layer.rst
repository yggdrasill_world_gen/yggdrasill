Data Layers
======================================

Besides the mesh topology itself, all data is stored in layers managed and owned by a `YGDL_World` object. There are two distinct types of layer
unstructured layers, that are just a `YGDL_Dict` instance identified by a unique ID (see :doc:`../configuration`), and structured layers, that
are used to store data for specific vertices, faces or edges.

Structured layers are always linked to a specific element of the mesh (either vertices, faces or edges, specified by its `YGDL_Layer_ref_type_t`) and are used to attach data to them.
Layers function similar to sequence containers, except that they are managed by the system and automatically updated when the mesh the layer is based on is modified. So, values can only be read and modified, but layers can't be manually resized or otherwise structurally modified. There are also some other modifications, outlined below, that are performed automatically when the mesh it modified.

Some of the values in a layer may also be left "unused", if they correspond to a mesh element that is currently not part of the mesh. To avoid costly copying, the values in a layer are not compacted when e.g. a vertex is removed because of `YGDL_mesh_edge_collapse()`. Instead, these elements are left as-is and reused if a new vertex is required. The layer values for these elements remain accessible and can still be read and modified, but are not used for anything else. When iterating over a layer, there are options to both include and skip these unused elements, depending on the performance characteristics of your specific operation.

What kind of data a layer contains, which part of the mesh they are based on and how they react to changes is all part of their `YGDL_Layer_definition`, which is usually globally defined and later used to acquire a `YGDL_Layer*` from the `YGDL_World`.


All elements in a layer have to be of the same type, which is specified in its definition and limited to a subset of those listed in `YGDL_Type`:

- `YGDL_Bool`
- `int8_t`, `int32_t` and `int64_t` as well as enumerations with one of these as their underlying type
- `float` and `double`
- `YGDL_Vec2` and `YGDL_Vec3`

The values that may be stored in a layer may also be subject to additional restriction (e.g. limited min/max values), that are specified in its definition. These restrictions are validated when a mutable pointer is released by calling `YGDL_world_layer_release()`.

Since a layer is tightly connected to an element of the mesh, it also has to be updated if the underlying mesh changes. This can happen automatically when the mesh is modified or manually by calling `YGDL_world_layer_invalidate()`.

See the API documentation below, the documentation page for the :doc:`mesh`, as well as the C++-specific documentations for more details.


API
--------------------------------------

.. ucpp:file:: yggdrasill/layer.h
