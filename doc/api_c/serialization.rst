Serialization
======================================

Yggdrasill provides functions to persist simulation states in the file-system. The files created and read by these are ZIP-Archives that can contain multiple `YGDL_World` objects, as well as :cpp:any:`configurations <YGDL_Dict>` and arbitrary user-defined entries. This enables them to be used both to export a single simulation result and more complex collections of multiple snapshots and configurations (e.g. editor save files).

The provided functions can be used both for writing entries and to read them. This allows archive files to be used as temporary storage for data that may be too large to be stored in main memory. For this use-case, and for long-lived `YGDL_Serializer` objects in general, it is usually advised to regularly call `YGDL_serializer_shrink_read_caches()` to free any currently used cached file entries.

The processing of ZIP-files is implemented using the `miniz <https://github.com/richgel999/miniz>`_ library, that has to either be installed on the local system or will be downloaded and build automatically if it isn't found. If this dependency can't be satisfied or serialization is not required, this part of the API can be disabled by setting the CMake option ``YGGDRASILL_SERIALIZATION`` to ``OFF``. If serialization support is disabled, the functions are still available, but calling them will set an error code and return NULL or equivalent values.


File Format
--------------------------------------

The used file format is intentionally designed to be simple and usable by other applications. The files themselves are ZIP 64 archives, which contain a number of different `UBJSON <https://ubjson.org>`_ binary files divided into corresponding folders. The currently used folder names are:

- ``/c/`` for configurations, i.e. `YGDL_Dict`.
- ``/x/`` for user-defined binary blobs (see `YGDL_serializer_userdata_write()` and `YGDL_serializer_userdata_read()`).
- ``/w/`` for simulation states, i.e. `YGDL_World`.
- ``/u/`` for unstructured layers, that are part of a persisted simulation state.
- ``/l/`` for definitions of mesh-based layer.
- ``/s/`` for mesh-based layers, that are part of a persisted simulation state.
- ``/m/`` for meshes, that are part of a persisted simulation state.


The files in these directories are either named based on the provided key-string, for configurations, user-data and layer definitions, or with hexadecimal encoded integer-keys, that are automatically generated for the other entry-types.

.. attention::

   Any files in the archive that don't match these patterns are ignored by the implementation. But any currently unused filenames are reserved for future extensions, except those in the ``/x/`` folder, which is specifically designed to hold user-defined arbitrary data.

Except for the user-defined entries, or all other files that are part of the archive are `UBJSON <https://ubjson.org>`_ objects with the following provisions and additional requirements/guarantees:

- The types of values carry semantic information and may not be arbitrarily changed. For example, 8-Bit and 32-Bit integers are deserialized to different element-types in a `YGDL_Dict`. This restriction means that the conversion to JSON and back to UBJSON is **not** necessarily lossless!
- All floating-point numbers are encoded as IEEE-754. This, contrary to the UBJSON specification, **also** includes NaNs, infinite and denormalized numbers. This makes the serialization lossless for these types of values.
- Except where specifically allowed, ``null`` values may not be used.
- Values of the ``high-precision number`` and ``char`` types are currently unused and not supported by the parser.
- Additionally, ``uint8`` and ``int16`` are only supported for container sizes and string length, but not for normal values.
- Arrays of mixed types are not supported.
- All arrays written by Yggdrasill have a fixed element number and usually also have a fixed type specified.
- The types `YGDL_Vec2` and `YGDL_Vec3` are encoded as objects with a fixed size of 2/3 and a fixed type of ``float32``. All objects with these fixed sizes and types are expected to be vectors and have fields named ``x``, ``y`` and ``z``.


The following sections describe the current fields and types used for the different entry-types, which are guaranteed to be stable. Any other field names are reserved for future extensions.


Configurations (``/c/``) and Unstructured Layers (``/u/``)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`YGDL_Dict` objects, so both configurations and unstructured layers, are serialized directly to their corresponding UBJSON object, as the value types perfectly match those defined by the API. 


Worlds (``/w/``)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`YGDL_World` objects are serialized to one UBJSON object with the following fields, as well as additional files for their individual parts (meshes, layer definitions and structured/unstructured layers).

.. table:: Structure of `YGDL_World` objects
   :widths: auto
   
   +--------------------------+------------------------------------------------------------+
   | Field Name               | Type                                                       |
   +==========================+============================================================+
   | random                   | ``Object`` with two ``int64`` fields: ``state`` and ``inc``|
   +--------------------------+------------------------------------------------------------+
   | mesh                     | ``int32``, referencing an entry in ``/m/``                 |
   +--------------------------+------------------------------------------------------------+
   | unstructured_layers      | ``int32``, referencing an entry in ``/u/``                 |
   +--------------------------+------------------------------------------------------------+
   | mesh_layers              | ``int32``, referencing an entry in ``/s/``                 |
   +--------------------------+------------------------------------------------------------+


Meshes (``/c/``)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`YGDL_Mesh` objects are serialized to one UBJSON object with the following fields.

Note that the free-lists of unused vertices/faces/edges is not stored directly but reconstructed on load, based on which elements in ``vertex_edge``, ``vertex_edge`` and ``vertex_edge`` are set to `YGDL_NO_VERTEX`/`YGDL_NO_FACE`/`YGDL_NO_EDGE`.

.. table:: Structure of `YGDL_World` objects
   :widths: auto
   
   +--------------------------+-----------------------------------------------------------+
   | Field Name               | Type                                                      |
   +==========================+===========================================================+
   | vertex_edge              | An array of ``int32`` values                              |
   +--------------------------+-----------------------------------------------------------+
   | face_edge                | An array of ``int32`` values                              |
   +--------------------------+-----------------------------------------------------------+
   | primal_edge_origin       | An array of ``int32`` values                              |
   +--------------------------+-----------------------------------------------------------+
   | primal_edge_next         | An array of ``int32`` values                              |
   +--------------------------+-----------------------------------------------------------+
   | dual_edge_origin         | An array of ``int32`` values                              |
   +--------------------------+-----------------------------------------------------------+
   | dual_edge_next           | An array of ``int32`` values                              |
   +--------------------------+-----------------------------------------------------------+


Structured Layers (``/s/``)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`YGDL_Layer` objects are serialized to one UBJSON object with the following fields

To allow for efficient streaming and parsing, there are two special cases for this type:

- The ``definition`` field has to be the first in the file.
- Values of type `YGDL_Vec2` and `YGDL_Vec3` are encoded as arrays of ``float32`` instead of objects, like they are in other files.

.. table:: Structure of `YGDL_World` objects
   :widths: auto
   
   +--------------------------+-------------------------------------------------------------------------------------+
   | Field Name               | Type                                                                                |
   +==========================+=====================================================================================+
   | definition               | ``String``, referencing an entry in ``/l/``                                         |
   +--------------------------+-------------------------------------------------------------------------------------+
   |                          | - Fixed size type array for `int8_t`, `int32_t`, `int64_t` and `float`.             |
   | values                   | - Booleans are encoded as a fixed size untyped array with values ``T`` and ``F``.   |
   |                          | - `YGDL_Vec2` and `YGDL_Vec3` are encoded as a fixed size array of `float32` values.|
   +--------------------------+-------------------------------------------------------------------------------------+


Layer definitions (``/l/``)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`YGDL_Layer_definition` objects are serialized to one UBJSON object with the following fields:

.. table:: Structure of `YGDL_Layer_definition` objects
   :widths: auto
   
   +--------------------------+-----------------------------------------------------------+
   | Field Name               | Type                                                      |
   +==========================+===========================================================+
   | id                       | ``String``                                                |
   +--------------------------+-----------------------------------------------------------+
   | description              | ``String``                                                |
   +--------------------------+-----------------------------------------------------------+
   | data_type                | ``int8``                                                  |
   +--------------------------+-----------------------------------------------------------+
   | ref_type                 | ``int8``                                                  |
   +--------------------------+-----------------------------------------------------------+
   | invalidation             | ``int8``                                                  |
   +--------------------------+-----------------------------------------------------------+
   | interpolation            | ``int8``                                                  |
   +--------------------------+-----------------------------------------------------------+
   | initial                  | [type specified by ``data_type``]                         |
   +--------------------------+-----------------------------------------------------------+
   | min                      | [type specified by ``data_type``] (not for booleans)      |
   +--------------------------+-----------------------------------------------------------+
   | max                      | [type specified by ``data_type``] (not for booleans)      |
   +--------------------------+-----------------------------------------------------------+
   | min_length               | ``float32`` (only for vector types)                       |
   +--------------------------+-----------------------------------------------------------+
   | max_length               | ``float32`` (only for vector types)                       |
   +--------------------------+-----------------------------------------------------------+
   | has_min                  | ``T`` or ``F`` (only for floating point and vector types) |
   +--------------------------+-----------------------------------------------------------+
   | has_max                  | ``T`` or ``F`` (only for floating point and vector types) |
   +--------------------------+-----------------------------------------------------------+
   | has_min_length           | ``T`` or ``F`` (only for vector types)                    |
   +--------------------------+-----------------------------------------------------------+
   | has_max_length           | ``T`` or ``F`` (only for vector types)                    |
   +--------------------------+-----------------------------------------------------------+
   | allow_nan                | ``T`` or ``F`` (only for floating point and vector types) |
   +--------------------------+-----------------------------------------------------------+
   | allow_inf                | ``T`` or ``F`` (only for floating point and vector types) |
   +--------------------------+-----------------------------------------------------------+



API
--------------------------------------

To allow for efficient partial-reading of user-defined entries, the API provides a callback-based read function `YGDL_serializer_userdata_read()`. This function is passed a function pointer (see `YGDL_Serializer_reader`), which is invoked to read the actual entry-data. When the user-provided function is invoked, it is passed another function pointer (see `YGDL_Serializer_reader_req`), which it can invoke multiple times to request the next chunk of bytes. The following two function declarations are provided as exposition only, for readability. The code itself is based on function-pointers with identical names and signatures.

.. cpp:function:: void YGDL_Serializer_reader(void* ctx, void* reader_ctx, YGDL_Serializer_reader_req* read)

  Type of the callback passed to `YGDL_serializer_userdata_read()` to read the bytes of a user-data entry.

  :param ctx: The unchanged `ctx` parameter passed to `YGDL_serializer_userdata_read()`.
  :param reader_ctx: An opaque context object, that has to be passed to `read()`.
  :param read: A callable that can be called multiple times to read subsequent bytes from the entry- 

.. cpp:function:: size_t YGDL_Serializer_reader_req(void* reader_ctx, size_t bytes_to_read, void* buffer)

  Type of the callback returned to the user in `YGDL_serializer_userdata_read()` to read a user-data entry.

  :param reader_ctx: The `reader_ctx` parameter passed into `YGDL_Serializer_reader`.
  :param bytes_to_read: The number of bytes that should be read into `buffer`. If the parameter is zero, no bytes are read and the number of remaining bytes is returned.
  :param buffer: Pointer to a memory segment with at least `bytes_to_read` many bytes of space.
  :return: Either the number of bytes actually read or the number of remaining bytes in the entry, if `bytes_to_read` was zero.

  .. note::

    The number of actually read bytes may be smaller than requested.


.. ucpp:file:: yggdrasill/serialization.h
