#include <yggdrasill/layer.hpp>
#include <yggdrasill/mesh.hpp>
#include <yggdrasill/module.hpp>

#include "quickhull/QuickHull.hpp"

#include <algorithm>
#include <array>
#include <cmath>
#include <cstdint>
#include <numbers>

namespace yggdrasill::modules {

	constexpr auto position_info = Layer_definition<Vec3, Ref_type::vertex>("position")
	                                       .interpolation(Interpolation::nlerp)
	                                       .invalidation(Invalidation::reset_affected)
	                                       .min_length(0.9f)
	                                       .max_length(1.1f);

	static void execute(World_view world, yggdrasill::Any_dictionary auto cfg)
	{
		const auto vertices     = cfg["generate"]["vertices"].get(4096, {.min = 16});
		const auto radius       = cfg["generate"]["radius"].get(6'371'000.f, {.min = 1.f});
		const auto perturbation = cfg["generate"]["perturbation"].get(0.05f, {.min = 0.f, .max = 0.99f});

		if(!world)
			return;


		auto [meta]      = world.lock_unstructured_layer("meta");
		auto [mesh]      = world.lock_mesh();
		auto [positions] = world.lock_layer(position_info);
		auto rand        = world.random();

		meta["radius"] = radius;

		mesh.add_vertices(vertices);

		constexpr auto golden_angle = 2.f * std::numbers::pi_v<float> * (2.f - std::numbers::phi_v<float>);
		const auto     step_size    = 1.f / (vertices - 1);

		for(std::int32_t i = 0; i < vertices; i++) {
			const auto offset = perturbation > 0 ? rand.uniform(0.f, perturbation) : 0.f;
			const auto ii     = std::min(i + offset, vertices - 1.f);
			const auto theta  = golden_angle * ii;

			const auto y = 1.f - step_size * 2.f * ii;
			const auto r = std::abs(std::abs(y) - 1) <= std::numeric_limits<float>::epsilon()
			                       ? 0.f
			                       : std::sqrt(1 - y * y);

			const auto x = std::cos(theta) * r;
			const auto z = std::sin(theta) * r;

			positions[Vertex(i)] = normalized(Vec3{x, y, z});
		}

		// imported https://github.com/akuukka/quickhull as a starting point. Replace with my own implementation based on quad-edges, if required later
		quickhull::QuickHull<float> qh;
		auto  hull    = qh.getConvexHull(&positions.begin()->x, positions.size(), false, true);
		auto& indices = hull.getIndexBuffer();

		auto error_faces = std::vector<std::array<Vertex, 3>>();
		for(std::size_t i = 0; i < indices.size(); i += 3) {
			const auto face = std::array<Vertex, 3>{Vertex(static_cast<std::uint32_t>(indices[i])),
			                                        Vertex(static_cast<std::uint32_t>(indices[i + 1])),
			                                        Vertex(static_cast<std::uint32_t>(indices[i + 2]))};

			if(mesh.try_add_face(face[0], face[1], face[2]) == no_face) {
				// might fail if triangles come in the wrong order => retry afterwards
				error_faces.emplace_back(face);
			}
		}

		if(!error_faces.empty()) {
			std::cerr << error_faces.size() << " faces in the wrong order\n";

			auto i = 0;
			while(!error_faces.empty()) {
				std::erase_if(error_faces, [&](auto& face) {
					if(mesh.try_add_face(face[0], face[1], face[2]) != no_face)
						return true;
					else if(i >= 2)
						throw;
					else
						return false;
				});

				i++;
			}

			auto unused_vertices = 0;
			for(auto vertex_index = index_t(0); vertex_index < mesh.vertex_max_index(); ++vertex_index) {
				const auto v = Vertex(vertex_index);
				if(v.edge(mesh) == no_edge) {
					unused_vertices++;
					mesh.remove_unused_vertex(v);
				}
			}
			if(unused_vertices > 0) {
				std::cerr << unused_vertices << "/" << vertices
				          << " vertices weren't used by generate_sphere. This happens if the generated "
				             "points aren't part of the convex hull. Try to reduce 'generate.perturbation' "
				             "or the vertex count.\n";
			}
		}
	}

	extern const auto generate_sphere = to_module_info<execute, execute>("generate_sphere");

} // namespace yggdrasill::modules

YGDL_REGISTER_PLUGIN_MODULES(&yggdrasill::modules::generate_sphere)
