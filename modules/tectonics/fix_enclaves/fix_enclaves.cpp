#include <shared_tectonics_layer.hpp>

#include <yggdrasill/layer.hpp>
#include <yggdrasill/mesh.hpp>
#include <yggdrasill/mesh_utils.hpp>
#include <yggdrasill/module.hpp>

#include <algorithm>
#include <unordered_set>
#include <vector>

namespace yggdrasill::modules::tectonics {
	namespace {
		enum class State : std::uint8_t { open, visited, backlog };

		struct Connected_subplates {
			Plate_id            preferred_id;
			std::vector<Vertex> vertices;
		};

		struct Fix_enclaves {

			/// seen vertices that didn't match the id and must be visited once the open_list is empty
			std::vector<Vertex>                      backlog_list;
			std::unordered_set<Plate_id>             used_ids;
			std::vector<Connected_subplates>         plates;
			std::unordered_map<Plate_id, int>        neighbor_plate_count_same_type;
			std::unordered_map<Plate_id, int>        neighbor_plate_count;
			Temporary_layer<State, Ref_type::vertex> vertex_state;
			std::vector<Vertex>                      change_to_continental;
			std::vector<Vertex>                      limit_elevation;

			static std::pair<const Plate_id, int> max_element(const std::unordered_map<Plate_id, int>& map)
			{
				const auto max = std::max_element(map.begin(), map.end(), [](const auto& lhs, const auto& rhs) {
					return lhs.second < rhs.second;
				});

				return max != map.end() ? *max : std::pair<const Plate_id, int>(0, 0);
			}

			void execute(World_view world, yggdrasill::Any_dictionary auto cfg)
			{
				const auto min_plate_vertices = cfg["plate_sim"]["min_plate_vertices"].get(16, {.min = 1});
				const auto min_plate_vertices_percent =
				        cfg["plate_sim"]["min_plate_vertices_percent"].get(0.01f, {.min = 0.f, .max = 1.f});

				const auto min_oceanic_plate_vertices =
				        cfg["plate_sim"]["min_oceanic_plate_vertices"].get(8, {.min = 1});

				if(!world)
					return;

				const auto mesh         = world.mesh();
				const auto [ids, types] = world.required_layer(layer_plate_id, layer_crust_type);

				auto any_vertex = mesh.vertices().first();
				if(!any_vertex)
					return;

				const auto min_vertices = std::max(
				        min_plate_vertices,
				        static_cast<std::int32_t>(min_plate_vertices_percent * mesh.vertex_used_count()));

				vertex_state.reset(mesh, State::open);
				backlog_list.clear();
				used_ids.clear();
				plates.clear();
				change_to_continental.clear();
				limit_elevation.clear();

				auto enclaves_found = false;

				// start with one arbitrary vertex in the backlog
				backlog_list.push_back(*any_vertex);
				vertex_state[*any_vertex] = State::backlog;

				while(!backlog_list.empty()) {
					const auto next_v = backlog_list.back();
					backlog_list.pop_back();

					if(vertex_state[next_v] == State::backlog) {
						const auto original_id = ids[next_v];

						auto& subplate_collection        = plates.emplace_back();
						subplate_collection.preferred_id = original_id;

						enclaves_found = enclaves_found || !used_ids.insert(original_id).second;

						flood_fill(mesh, std::vector{next_v}, Neighbor_type::just_primal_edges, [&](Vertex n) {
							if(vertex_state[n] != State::visited) {
								const auto n_id = ids[n];
								if(n_id == original_id) {
									subplate_collection.vertices.emplace_back(n);
									vertex_state[n] = State::visited;
									return true;
								} else if(vertex_state[n] == State::open) {
									// remember for later
									backlog_list.emplace_back(n);
									vertex_state[n] = State::backlog;
								}
							}
							return false;
						});

						enclaves_found = enclaves_found
						                 || static_cast<int>(subplate_collection.vertices.size()) < min_vertices;
					}
				}

				if(!enclaves_found)
					return;

				std::sort(plates.begin(), plates.end(), [](auto& lhs, auto& rhs) {
					return lhs.vertices.size() > rhs.vertices.size();
				});

				auto next_plate_id = 1 + *std::max_element(ids.begin(), ids.end());

				auto [ids_mut] = world.lock_layer(layer_plate_id);

				used_ids.clear();
				for(auto& plate : plates) {
					// assign small plates to their biggest neighbor
					if(static_cast<int>(plate.vertices.size()) < min_vertices) {
						// count direct neighbors
						neighbor_plate_count.clear();
						neighbor_plate_count_same_type.clear();
						const auto org_id                     = plate.preferred_id;
						auto       neighbor_count             = 0;
						auto       continental_neighbor_count = 0;
						for(auto v : plate.vertices) {
							const auto type = types[v];

							for(const auto n : v.neighbors(mesh, Neighbor_type::just_primal_edges)) {
								if(const auto id = ids[n]; id != org_id) {
									neighbor_plate_count[id]++;
									neighbor_count++;
									if(types[n] == Crust_type::continental)
										continental_neighbor_count++;
									if(types[n] == type)
										neighbor_plate_count_same_type[id]++;
								}
							}
						}

						// change small oceanic plates, that are surrounded by continental crust to continental
						if(static_cast<float>(continental_neighbor_count) / neighbor_count >= 0.8f) {
							change_to_continental.insert(
							        change_to_continental.end(), plate.vertices.begin(), plate.vertices.end());

							if(plate.vertices.size() <= static_cast<std::size_t>(min_oceanic_plate_vertices)) {
								limit_elevation.insert(
								        limit_elevation.end(), plate.vertices.begin(), plate.vertices.end());
							}
						}

						// assign to neighbor with largest shared edges, preferring same-type-plates
						const auto max_same_type = max_element(neighbor_plate_count_same_type);
						const auto max =
						        max_same_type.second > 0 ? max_same_type : max_element(neighbor_plate_count);
						if(max.second > 0) {
							const auto new_id = max.first;
							for(auto v : plate.vertices) {
								ids_mut[v] = new_id;
							}
							continue;
						}
					}

					if(!used_ids.insert(plate.preferred_id).second) {
						const auto new_id = next_plate_id++;
						for(auto v : plate.vertices)
							ids_mut[v] = new_id;
					}
				}

				// change isolated oceanic crust to continental
				if(!change_to_continental.empty()) {
					auto [types_mut, elevations] = world.lock_layer(layer_crust_type, layer_elevation);

					if(!limit_elevation.empty()) {
						for(auto v : limit_elevation) {
							auto min_elevation = 0.f;
							for(const auto n : v.neighbors(mesh, Neighbor_type::just_primal_edges)) {
								if(types_mut[n] == Crust_type::continental && min_elevation < elevations[n])
									min_elevation = elevations[n];
							};

							if(elevations[v] < min_elevation)
								elevations[v] = min_elevation;
						}
					}

					std::sort(change_to_continental.begin(), change_to_continental.end());
					for(auto v : change_to_continental) {
						types_mut[v] = Crust_type::continental;
					}
				}
			}
		};
	} // namespace

	/// finds unconnected clusters of sub-plates that share the same id and assigns one of them a new id
	/// i.e. after this function has run, all vertices that share the same id are connected to each other
	extern const auto fix_enclaves = to_module_info<Fix_enclaves>("tectonics_fix_enclaves");

} // namespace yggdrasill::modules::tectonics

YGDL_REGISTER_PLUGIN_MODULES(&yggdrasill::modules::tectonics::fix_enclaves)
