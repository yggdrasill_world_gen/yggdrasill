#include <shared_tectonics_layer.hpp>

#include <yggdrasill/error.hpp>
#include <yggdrasill/layer.hpp>
#include <yggdrasill/mesh.hpp>
#include <yggdrasill/mesh_utils.hpp>
#include <yggdrasill/module.hpp>

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <utility>
#include <vector>


namespace yggdrasill::modules::tectonics {

	namespace {
		Vec3 normalized_velocity(const Vec3& position, const Vec3& velocity)
		{
			constexpr auto dt = 100.f;

			// move position one step with given velocity and normalize it back to spheres surface
			const auto next_position = normalized(position + velocity * dt) * length(position);
			return (next_position - position) / dt; // calculate actual velocity
		}
	} // namespace

	static void execute(World_view world, yggdrasill::Any_dictionary auto cfg)
	{
		const auto plates_min = cfg["generate"]["plates"]["min"].get(12, {.min = 1});
		const auto plates_max = cfg["generate"]["plates"]["max"].get(18, {.min = plates_min});
		const auto ocean_prop = cfg["generate"]["ocean"]["probability"].get(0.6f, {.min = 0.f, .max = 1.f});
		const auto ocean_elevation = cfg["generate"]["ocean"]["elevation"].get(-6000.f, {.max = 0.f});
		const auto continent_elevation =
		        cfg["generate"]["continent"]["elevation"].get(100.f, {.min = ocean_elevation + 1});

		// in years
		const auto world_age = cfg["generate"]["world"]["initial_age"].get(40'000'000.f);
		const auto ocean_age = cfg["generate"]["ocean"]["initial_age"].get(20'000'000.f, {.max = world_age});
		const auto continent_age =
		        cfg["generate"]["continent"]["initial_age"].get(40'000'000.f, {.max = world_age});

		// in meter/year
		const auto max_ocean_speed     = cfg["generate"]["ocean"]["velocity"].get(0.2f, {.min = 0.f});
		const auto max_continent_speed = cfg["generate"]["continent"]["velocity"].get(0.2f, {.min = 0.f});


		if(!world)
			return;

		const auto radius = [&] {
			auto [meta] = world.lock_unstructured_layer("meta");
			meta["age"] = world_age;
			return meta["radius"].get<float>();
		}();

		auto rand = world.random();
		auto mesh = world.mesh();

		auto [ids, types, velocities, elevations, created_times] = world.lock_layer(
		        layer_plate_id, layer_crust_type, layer_velocity, layer_elevation, layer_created);

		auto positions = world.layer(layer_position);
		if(!positions)
			throw Module_exception("Missing 'position' layer");

		const auto count       = rand.uniform(plates_min, plates_max);
		const auto ocean_count = static_cast<int>(std::floor(count * ocean_prop));

		auto open_vertices = std::vector<Vertex>();
		open_vertices.reserve(static_cast<std::size_t>(count) * 10u);

		auto next_id = std::int32_t(1);

		for(auto i = 0; i < count; i++) {
			if(auto seed_vertex = random_vertex(mesh, rand, 32, [&](auto v) { return ids[v] == 0; })) {
				const auto ocean = i < ocean_count;

				types[*seed_vertex] = ocean ? Crust_type::oceanic : Crust_type::continental;
				ids[*seed_vertex]   = next_id++;

				const auto angle  = rand.uniform(0.2f, 2.f * 3.141f);
				const auto speed  = rand.uniform(0.5f, 1.f) * (ocean ? max_ocean_speed : max_continent_speed);
				const auto normal = normalized((*positions)[*seed_vertex]);
				const auto tangent       = cross(normal, Vec3(-normal.z, normal.x, normal.y));
				const auto bitangent     = cross(normal, tangent);
				velocities[*seed_vertex] = (tangent * std::sin(angle) + bitangent * std::cos(angle)) * speed;

				elevations[*seed_vertex] =
				        ocean ? ocean_elevation : (continent_elevation * rand.uniform(0.25f, 1.25f));

				created_times[*seed_vertex] =
				        world_age - (ocean ? ocean_age : continent_age) * rand.uniform(0.75f, 1.f);

				open_vertices.emplace_back(*seed_vertex);
			}
		}

		// flood fill
		auto new_opened_vertices = std::vector<Vertex>();
		new_opened_vertices.reserve(open_vertices.size());

		while(!open_vertices.empty()) {
			for(auto& v : open_vertices) {
				const auto type         = types[v];
				const auto id           = ids[v];
				const auto velocity     = velocities[v];
				const auto elevation    = elevations[v];
				const auto created_time = created_times[v];

				for(Vertex n : v.neighbors(mesh, Neighbor_type::just_primal_edges)) {
					auto& new_type = types[n];
					if(new_type == Crust_type::none) {
						new_type      = type;
						ids[n]        = id;
						velocities[n] = normalized_velocity((*positions)[n] * radius, velocity);
						elevations[n] = elevation;
						created_times[n] = type == Crust_type::oceanic ? created_time * 0.9f : created_time * 1.1f;
						new_opened_vertices.emplace_back(n);

						if(type == Crust_type::continental) {
							elevations[n] = std::clamp(elevations[n] + rand.uniform(-50.f, 200.f),
							                           -50.f,
							                           continent_elevation * 50.f);
						}
					}
				}
			}

			rand.shuffle(new_opened_vertices);
			std::swap(new_opened_vertices, open_vertices);
			new_opened_vertices.clear();
		}

		// add continental shelfs
		auto shelf_vertices = std::vector<std::pair<Vertex, std::int32_t>>();
		for(auto v : mesh.vertices()) {
			if(types[v] != Crust_type::oceanic)
				continue;

			for(auto n : v.neighbors(mesh, Neighbor_type::just_primal_edges)) {
				if(types[n] != Crust_type::oceanic) {
					shelf_vertices.emplace_back(v, ids[n]);
					break;
				}
			}
		}
		for(auto [v, id] : shelf_vertices) {
			ids[v]        = id;
			types[v]      = Crust_type::continental;
			elevations[v] = rand.uniform(-200.f, -10.f);
		}
	}

	extern const auto generate_plates = to_module_info<execute, execute>("tectonics_generate_plates");

} // namespace yggdrasill::modules::tectonics

YGDL_REGISTER_PLUGIN_MODULES(&yggdrasill::modules::tectonics::generate_plates)
