#include <shared_tectonics_layer.hpp>
#include <shared_tectonics_util.hpp>

#include <yggdrasill/layer.hpp>
#include <yggdrasill/mesh.hpp>
#include <yggdrasill/mesh_utils.hpp>
#include <yggdrasill/module.hpp>

#include <algorithm>
#include <array>
#include <cmath>
#include <optional>
#include <tuple>
#include <unordered_set>
#include <utility>
#include <vector>


namespace yggdrasill::modules::tectonics {

	namespace {

		struct Spring_constant {
			float compressed, stretched, damping;
		};

		Vec3 spring_force(const Vec3&            difference,
		                  const Vec3&            velocity_a,
		                  const Vec3&            velocity_b,
		                  const float            target_distance,
		                  const Spring_constant& spring_constant)
		{
			const auto dist = length(difference);
			if(dist < 0.00001f)
				return Vec3{0, 0, 0};

			const auto direction    = difference / dist;
			const auto displacement = target_distance - dist;
			const auto k = displacement > 0.f ? spring_constant.compressed : spring_constant.stretched;

			const auto relative_velocity = dot(velocity_a - velocity_b, direction);

			return direction * (displacement * k + relative_velocity * spring_constant.damping);
		}

		using Forces = Temporary_layer<Vec3, Ref_type::vertex>;


		struct Velocity_range {
			float start;
			float stop;
		};

		struct Plate_sim {
			Forces              forces;
			std::vector<Vertex> seen_vertices;
			Delaunay_restorer   delaunay_restorer;


			void clear_invalid_plate_assignments(const Const_mesh_view            mesh,
			                                     const Layer_plate_id::const_view ids,
			                                     const Layer_position::const_view positions,
			                                     Layer_p_distance                 primal_distances,
			                                     Layer_p_boundaries               primal_boundaries,
			                                     Layer_d_distance                 dual_distances,
			                                     Layer_d_boundaries               dual_boundaries)
			{
				auto impl = [&](auto distances, auto boundary_types) {
					for(auto&& [e, boundary_type] : range(mesh, boundary_types)) {
						const auto [a, b] = edge_vertices(mesh, e);

						const auto same_plate = ids[a] == ids[b];

						if(same_plate && boundary_type != Boundary_type::joined) {
							distances[e]  = length(positions[a] - positions[b]);
							boundary_type = Boundary_type::joined;
						} else if(!same_plate && boundary_type == Boundary_type::joined) {
							boundary_type = Boundary_type::transform;
						}
					}
				};

				impl(primal_distances, primal_boundaries);
				impl(dual_distances, dual_boundaries);
			}

			void identify_boundaries(const Const_mesh_view              mesh,
			                         const Layer_plate_id::const_view   ids,
			                         const Layer_crust_type::const_view types,
			                         const Layer_created::const_view    creation_times,
			                         const Layer_position::const_view   positions,
			                         const Layer_velocity::const_view   velocities,
			                         Layer_p_distance                   primal_distances,
			                         Layer_p_boundaries                 primal_boundaries,
			                         Layer_d_distance                   dual_distances,
			                         Layer_d_boundaries                 dual_boundaries,
			                         const Velocity_range               collision_velocity,
			                         const Velocity_range               subduction_velocity,
			                         const Velocity_range               ridge_velocity,
			                         const float                        min_ridge_distance,
			                         const float                        radius)
			{
				auto impl = [&](auto distances, auto boundary_types) {
					for(auto&& [e, boundary_type] : range(mesh, boundary_types)) {
						const auto [a, b] = edge_vertices(mesh, e);

						if(ids[a] == ids[b]) {
							if(boundary_type != Boundary_type::joined) {
								distances[e]  = length(positions[a] - positions[b]);
								boundary_type = Boundary_type::joined;
							}

						} else {
							const auto diff                = (positions[b] - positions[a]) * radius;
							const auto dist                = length(diff);
							const auto converging_velocity = -dot(velocities[b] - velocities[a], diff / dist);

							switch(boundary_type) {
								case Boundary_type::joined:
									boundary_type = Boundary_type::transform;
									[[fallthrough]];
								case Boundary_type::transform:
									if(converging_velocity <= ridge_velocity.start
									   || (converging_velocity < ridge_velocity.stop
									       && dist >= min_ridge_distance)) {
										boundary_type = Boundary_type::ridge;

									} else if(converging_velocity > 0.f) {
										const auto subducted = get_subducting_plate(a, b, types, creation_times);
										if(!subducted) {
											if(converging_velocity >= collision_velocity.start)
												boundary_type = Boundary_type::collision;
										} else {
											if(converging_velocity >= subduction_velocity.start)
												boundary_type = subducted == a
												                        ? Boundary_type::subducting_origin
												                        : Boundary_type::subducting_dest;
										}
									}
									break;
								case Boundary_type::subducting_origin:
								case Boundary_type::subducting_dest: {
									const auto subducted = get_subducting_plate(a, b, types, creation_times);
									if(converging_velocity < subduction_velocity.stop || !subducted) {
										boundary_type = Boundary_type::transform;
									} else {
										boundary_type = subducted == a ? Boundary_type::subducting_origin
										                               : Boundary_type::subducting_dest;
									}
									break;
								}
								case Boundary_type::collision:
									if(converging_velocity < collision_velocity.stop) {
										boundary_type = Boundary_type::transform;
									}
									break;
								case Boundary_type::ridge:
									if(converging_velocity > ridge_velocity.stop) {
										boundary_type = Boundary_type::transform;
									}
									break;
							}
						}
					}
				};

				impl(primal_distances, primal_boundaries);
				impl(dual_distances, dual_boundaries);
			}

			void update_plate_properties(const Const_mesh_view                mesh,
			                             const Layer_crust_type::const_view   types,
			                             const Layer_position::const_view     positions,
			                             const Layer_velocity::const_view     velocities,
			                             const Layer_p_boundaries::const_view primal_boundaries,
			                             const Layer_d_boundaries::const_view dual_boundaries,
			                             Layer_p_distance                     primal_distances,
			                             Layer_d_distance                     dual_distances,
			                             const float                          dt,
			                             const float                          radius,
			                             const float                          slab_pull_force,
			                             const float                          collision_min_distance,
			                             const float                          collision_max_distance,
			                             const float                          collision_force_multiplier,
			                             const float                          ridge_push_force,
			                             const Spring_constant                spring_constant_same_plate)
			{
				// calculate forces based on neighboring vertices
				forces.reset(mesh);

				auto impl = [&](auto boundary_layer, auto distance_layer) {
					// calculate expected distances (required for spring-forces)
					for(auto&& [e, distance] : range(mesh, distance_layer)) {
						if(distance <= 0.f) {
							auto [a, b] = edge_vertices(mesh, e);
							distance    = length(positions[a] - positions[b]);
							assert(!std::isnan(distance) && !std::isinf(distance));
						}
					}

					for(const auto [e, boundary_type] : range(mesh, boundary_layer)) {
						const auto [src, dst] = edge_vertices(mesh, e);

						switch(boundary_type) {
							case Boundary_type::subducting_origin:
								// pull the subducting plate in
								forces[src] += normalized(positions[dst] - positions[src]) * slab_pull_force;
								break;
							case Boundary_type::subducting_dest:
								// pull the subducting plate in
								forces[dst] += normalized(positions[src] - positions[dst]) * slab_pull_force;
								break;

							case Boundary_type::transform:
								if(types[src] != Crust_type::continental || types[dst] != Crust_type::continental)
									break;
								[[fallthrough]];
							case Boundary_type::collision: {
								const auto src_pos   = positions[src] * radius;
								const auto dst_pos   = positions[dst] * radius;
								const auto diff      = dst_pos - src_pos;
								const auto distance2 = length2(diff);

								if(distance2 >= collision_max_distance * collision_max_distance)
									break;

								const auto dist = std::sqrt(distance2);
								const auto dir  = diff / dist;

								const auto relative_velocity   = velocities[src] - velocities[dst];
								const auto converging_velocity = dot(relative_velocity, dir);
								if(converging_velocity > 0.f) {
									const auto d = std::max(0.f, dist - collision_min_distance)
									               / collision_min_distance;
									const auto alpha = std::clamp(1.f / (1.f + d * d), 0.f, 1.f);
									const auto force = dir
									                   * (alpha * converging_velocity / dt / 2.f
									                      * collision_force_multiplier);
									assert(std::isfinite(force.x) && std::isfinite(force.y)
									       && std::isfinite(force.z));
									forces[src] -= force;
									forces[dst] += force;
								}
								break;
							}

							case Boundary_type::ridge: {
								// push both plate away from the ridge
								const auto src_to_dst = normalized(positions[dst] - positions[src]);
								assert(std::isfinite(src_to_dst.x) && std::isfinite(src_to_dst.y)
								       && std::isfinite(src_to_dst.z));
								forces[src] -= src_to_dst * ridge_push_force;
								forces[dst] += src_to_dst * ridge_push_force;
								break;
							}

							case Boundary_type::joined: {
								// stiff spring force to keep constant distance
								const auto F = spring_force(positions[dst] - positions[src],
								                            velocities[src],
								                            velocities[dst],
								                            distance_layer[e],
								                            spring_constant_same_plate);
								assert(std::isfinite(F.x) && std::isfinite(F.y) && std::isfinite(F.z));
								forces[src] += -F;
								forces[dst] += F;
								break;
							}
						}

						assert(std::isfinite(forces[src].x) && std::isfinite(forces[src].y)
						       && std::isfinite(forces[src].z));
						assert(std::isfinite(forces[dst].x) && std::isfinite(forces[dst].y)
						       && std::isfinite(forces[dst].z));
					}
				};

				impl(primal_boundaries, primal_distances);
				impl(dual_boundaries, dual_distances);
			}

			void move_plates(Layer_position positions,
			                 Layer_velocity velocities,
			                 const float    radius,
			                 const float    sub_step_dt,
			                 const float    plate_drag,
			                 const float    plate_mass)
			{
				const auto drag_factor = std::max(0.f, 1.f - plate_drag * sub_step_dt);

				// verlet step
				for(auto&& [position, velocity, force] : unchecked_range(positions, velocities, forces)) {
					const auto movement = velocity * sub_step_dt + force / plate_mass * sub_step_dt * sub_step_dt;
					const auto new_pos = normalized(position * radius + movement * drag_factor);
					velocity           = (new_pos - position) * radius / sub_step_dt;
					position           = new_pos;
				}
			}

			struct Elevation_response {
				float uplift_speed;
				float width_scale;
				float peak_distance;
				float peak_width;
				float rise_weight;
				float min_elevation;
				float max_elevation;
			};
			enum class Elevation_event_type {
				collision  = 0,
				subduction = 1, // index 1 (oceanic) or 2 (continental)
				trench     = 3,
			};
			struct Elevation_event {
				Vec3                 position; // already scaled by radius!
				Vec3                 direction;
				Vec3                 perpendicular_direction;
				float                scale_factor;
				Elevation_event_type type;
			};
			std::unordered_map<Plate_id, std::vector<Elevation_event>> elevation_events;

			void update_elevation(const Const_mesh_view                   mesh,
			                      const Layer_position::const_view        positions,
			                      const Layer_velocity::const_view        velocities,
			                      const Layer_p_boundaries::const_view    primal_boundaries,
			                      const Layer_d_boundaries::const_view    dual_boundaries,
			                      Layer_plate_id                          ids,
			                      Layer_crust_type                        crust_types,
			                      Layer_elevation                         elevations,
			                      const float                             dt,
			                      const float                             min_elevation,
			                      const float                             max_elevation,
			                      const float                             radius,
			                      const float                             max_velocity,
			                      const std::array<Elevation_response, 4> responses_by_event_type,
			                      const float                             continental_crust_formation_height)
			{
				elevation_events.clear();

				// collect elevation-events, based on relative velocities at subduction/collision boundaries
				auto impl = [&](auto boundary_layer) {
					for(const auto [e, boundary] : range(mesh, boundary_layer)) {
						if(boundary != Boundary_type::subducting_origin
						   && boundary != Boundary_type::subducting_dest && boundary != Boundary_type::collision)
							continue;

						const auto [remaining, subducted] =
						        edge_vertices(mesh, boundary == Boundary_type::subducting_origin ? e.sym() : e);

						const auto dir = normalized(positions[subducted] - positions[remaining]);

						const auto relative_velocity = -dot(velocities[subducted] - velocities[remaining], dir);
						const auto velocity_factor =
						        std::clamp(relative_velocity / (max_velocity / 2.f), 0.f, 1.f);

						if(boundary == Boundary_type::collision) {
							const auto collision_point = normalized(positions[subducted] + positions[remaining]);
							const auto perp_collision_dir = cross(dir, collision_point);

							elevation_events[ids[subducted]].emplace_back(collision_point,
							                                              dir,
							                                              perp_collision_dir,
							                                              velocity_factor,
							                                              Elevation_event_type::collision);
							elevation_events[ids[remaining]].emplace_back(collision_point,
							                                              dir,
							                                              perp_collision_dir,
							                                              velocity_factor,
							                                              Elevation_event_type::collision);
						} else {
							const auto collision_point    = positions[remaining];
							const auto perp_collision_dir = cross(dir, collision_point);

							elevation_events[ids[remaining]].emplace_back(collision_point,
							                                              dir,
							                                              perp_collision_dir,
							                                              velocity_factor,
							                                              Elevation_event_type::subduction);

							elevation_events[ids[subducted]].emplace_back(collision_point,
							                                              dir,
							                                              perp_collision_dir,
							                                              velocity_factor,
							                                              Elevation_event_type::trench);
						}
					}
				};
				impl(primal_boundaries);
				impl(dual_boundaries);


				// apply elevation changes
				for(auto&& [v, id, type, elevation, position] :
				    range(mesh, ids, crust_types, elevations, positions)) {
					auto event_iter = elevation_events.find(id);
					if(event_iter == elevation_events.end())
						continue;

					const auto& [plate_id, events] = *event_iter;

					auto new_elevation = elevation;
					for(auto& event : events) {
						if(event.type == Elevation_event_type::trench && type == Crust_type::continental)
							continue;

						// determine the type of the elevation-event
						const auto continental = new_elevation >= continental_crust_formation_height
						                         && type == Crust_type::continental;
						const auto resp_index =
						        static_cast<std::size_t>(event.type)
						        + (event.type != Elevation_event_type::subduction ? 0 : continental);
						const auto& resp = responses_by_event_type[resp_index];

						// Check if the vertex is in range. Using euclidean as an upper bound,
						//   to avoid the costly calculation of the actual great-circle-distance, if possible.
						const auto max_dist             = resp.peak_distance + resp.peak_width;
						const auto euclidean_distance_2 = length2((position - event.position) * radius);
						if(euclidean_distance_2 >= max_dist * max_dist)
							continue;

						const auto great_circle_distance = [&] {
							if(euclidean_distance_2 <= 1e-7)
								return 0.f;

							const auto scaled_p =
							        normalized(position
							                   - event.perpendicular_direction
							                             * (dot(position, event.perpendicular_direction)
							                                * (1.f - resp.width_scale)));
							return std::acos(std::clamp(dot(scaled_p, event.position), -1.f, 1.f)) * radius;
						}();

						if(great_circle_distance >= max_dist)
							continue;

						const auto weight = event.scale_factor * [&] {
							if(great_circle_distance <= resp.peak_distance - resp.peak_width)
								return resp.rise_weight;
							else if(great_circle_distance <= resp.peak_distance) {
								return std::lerp(resp.rise_weight,
								                 1.f,
								                 smootherstep(resp.peak_distance - resp.peak_width,
								                              resp.peak_distance,
								                              great_circle_distance));
							} else if(great_circle_distance <= resp.peak_distance + resp.peak_width) {
								return smootherstep(resp.peak_distance + resp.peak_width,
								                    resp.peak_distance,
								                    great_circle_distance);
							} else
								return 0.f;
						}();

						const auto max = std::lerp(resp.min_elevation, resp.max_elevation, weight);
						if(event.type == Elevation_event_type::trench) {
							if(new_elevation > max)
								new_elevation = std::max(max, new_elevation + resp.uplift_speed * weight * dt);
						} else {
							if(new_elevation < max)
								new_elevation = std::min(max, new_elevation + resp.uplift_speed * weight * dt);
						}
					}

					elevation = std::clamp(new_elevation, min_elevation, max_elevation);

					// transform oceanic to continental crust
					if(type != Crust_type::continental && new_elevation > continental_crust_formation_height) {
						type = Crust_type::continental;

						// check the surroundings for other continental plates
						auto first_continental_plate = true;
						auto other_id                = std::optional<std::int32_t>();
						for(auto n : v.neighbors(mesh, Neighbor_type::with_dual_edges)) {
							if(crust_types[n] == Crust_type::continental) {
								if(id == ids[n]) {
									first_continental_plate = false;
									break;
								}
								if(!other_id)
									other_id = ids[n];
							}
						}

						// if there is no other continental crust of this plate nearby, but there is one from another plate => attach it to that plate instead
						if(other_id && first_continental_plate) {
							id = *other_id;
						}
					}
				}
			}


			bool try_solve_collision(const Face                           f,
			                         Mesh_view                            mesh,
			                         Layer_plate_id                       ids,
			                         Layer_crust_type                     types,
			                         Layer_created                        creation_times,
			                         Layer_position                       positions,
			                         Layer_velocity                       velocities,
			                         Layer_elevation                      elevations,
			                         const Layer_p_boundaries::const_view boundaries,
			                         const float                          sub_step_dt,
			                         const float                          radius)
			{
				const auto ab    = f.edge(mesh);
				const auto a     = ab.origin(mesh);
				auto&      a_pos = positions[a];

				const auto bc    = ab.left_next(mesh);
				const auto b     = bc.origin(mesh);
				auto&      b_pos = positions[b];

				const auto ca    = bc.left_next(mesh);
				const auto c     = ca.origin(mesh);
				auto&      c_pos = positions[c];

				auto options = std::array{std::make_tuple(length2(a_pos - b_pos), a, b, ab),
				                          std::make_tuple(length2(b_pos - c_pos), b, c, bc),
				                          std::make_tuple(length2(c_pos - a_pos), c, a, ca)};

				// prefer shorter edges, but try to collapse subduction zones and other external boundaries first
				std::sort(options.begin(), options.end(), [&](auto& lhs, auto& rhs) {
					const auto lhs_b = boundaries[std::get<3>(lhs)];
					const auto rhs_b = boundaries[std::get<3>(rhs)];

					if(lhs_b != rhs_b) {
						if(lhs_b == Boundary_type::joined)
							return false;
						if(rhs_b == Boundary_type::joined)
							return true;

						const auto lhs_subduction = lhs_b == Boundary_type::subducting_dest
						                            || lhs_b == Boundary_type::subducting_origin;
						const auto rhs_subduction = rhs_b == Boundary_type::subducting_dest
						                            || rhs_b == Boundary_type::subducting_origin;
						if(lhs_subduction != rhs_subduction)
							return lhs_subduction ? true : false;
					}

					return std::get<0>(lhs) < std::get<0>(rhs);
				});

				for(auto& [len, v1, v2, edge] : options) {
					// positions=null => ignores normal-inversion condition
					if(try_collapse_subducting(
					           mesh, ids, types, creation_times, velocities, std::nullopt, elevations, boundaries, v1, v2, edge))
						return true;
				}

				// move points back to last position
				const auto new_a = a_pos - velocities[a] / radius * sub_step_dt;
				const auto new_b = b_pos - velocities[b] / radius * sub_step_dt;
				const auto new_c = c_pos - velocities[c] / radius * sub_step_dt;
				if(!is_wrong_winding_order(new_a, new_b, new_c)) {
					a_pos = new_a;
					b_pos = new_b;
					c_pos = new_c;
					return true;
				}

				return false;
			}

			bool solve_collisions_error_shown = false;
			bool solve_collisions(Mesh_view                            mesh,
			                      Layer_plate_id                       ids,
			                      Layer_crust_type                     types,
			                      Layer_created                        creation_times,
			                      Layer_velocity                       velocities,
			                      Layer_position                       positions,
			                      Layer_elevation                      elevations,
			                      const Layer_p_boundaries::const_view boundaries,
			                      const float                          sub_step_dt,
			                      const float                          radius)
			{
				auto solved_collisions = false;
				auto modified_topology = false;

				constexpr auto max_tries = 64;
				auto           tries     = max_tries;

				// check each face if the winding order has changed => triangle got flipped
				do {
					solved_collisions = false;

					for(const auto f : mesh.faces(concurrent_modifications)) {
						auto [a, b, c] = f.vertices(mesh);
						if(is_wrong_winding_order(positions[a], positions[b], positions[c])) {
							solved_collisions = true;
							modified_topology = true;

							if(!try_solve_collision(f,
							                        mesh,
							                        ids,
							                        types,
							                        creation_times,
							                        positions,
							                        velocities,
							                        elevations,
							                        boundaries,
							                        sub_step_dt,
							                        radius)
							   && !solve_collisions_error_shown) {
								solve_collisions_error_shown = true;
								std::cerr << "Couldn't solve inverted face in plate simulation. This should "
								             "be cleaned up by a later refinement step, but might create "
								             "artefacts\n";
							}
						}
					}

					tries--;
					if(tries <= 0) {
						break;
					}
				} while(solved_collisions);

				return modified_topology;
			}


			void execute(World_view world, yggdrasill::Any_dictionary auto args)
			{
				const auto sub_steps   = args["plate_sim"]["sub_steps"].get(64, {.min = 1});
				const auto delta_time  = args["plate_sim"]["delta_time"].get(1'000'000.f, {.min = 0.01f});
				const auto sub_step_dt = delta_time / sub_steps;

				const auto min_elevation = args["elevation"]["min"].get(-1e4f);
				const auto max_elevation = args["elevation"]["max"].get(1e4f, {.min = min_elevation});

				// meters / year
				const auto max_velocity = args["plate"]["max_velocity"].get(0.2f, {.min = 0.001f});

				const auto plate_mass = args["plate"]["mass"].get(1.f, {.min = 0.001f}); // kg
				const auto plate_drag = args["plate"]["drag"].get(3e-8f, {.min = 0.f, .max = 1.f});

				// meters / year
				const auto collision_velocity =
				        Velocity_range{args["plate_sim"]["collision"]["start_velocity"].get(0.01f),
				                       args["plate_sim"]["collision"]["stop_collision"].get(-0.01f)};
				const auto subduction_velocity =
				        Velocity_range{args["plate_sim"]["subduction"]["start_velocity"].get(0.05f),
				                       args["plate_sim"]["subduction"]["stop_velocity"].get(-0.05f)};
				const auto ridge_velocity =
				        Velocity_range{args["plate_sim"]["ridge"]["start_velocity"].get(-0.2f),
				                       args["plate_sim"]["ridge"]["stop_velocity"].get(-0.1f)};

				const auto collision_min_distance = args["plate_sim"]["collision"]["min_distance"].get(100'000.f);
				const auto collision_max_distance = args["plate_sim"]["collision"]["max_distance"].get(300'000.f);
				const auto collision_force_multiplier =
				        args["plate_sim"]["collision"]["force_multiplier"].get(0.9f);

				const auto slab_pull_force  = args["plate_sim"]["subduction"]["pull_force"].get(4e-7f);
				const auto ridge_push_force = args["plate_sim"]["ridge"]["push_force"].get(5e-8f);
				const auto spring_constant_same_plate =
				        Spring_constant{args["plate_sim"]["spring_constants"]["compressed"].get(1.2e-4f),
				                        args["plate_sim"]["spring_constants"]["stretched"].get(6e-5f),
				                        args["plate_sim"]["spring_constants"]["damping"].get(3e-6f)};

				const auto ridge_formation_distance =
				        args["plate_sim"]["ridge"]["formation_distance"].get(800'000.f, {.min = 0.001f});

				const auto elevation_response_collision = Elevation_response{
				        args["plate_sim"]["collision"]["uplift_speed"].get(5.5e-5f, {.min = 0.f}), // meters / year
				        args["plate_sim"]["collision"]["width_scale"].get(0.5f, {.min = 0.f}),
				        args["plate_sim"]["collision"]["peak_distance"].get(700'000.f),
				        args["plate_sim"]["collision"]["peak_width"].get(700'000.f),
				        args["plate_sim"]["collision"]["rise_weight"].get(0.6f, {.min = 0.f, .max = 1.f}),
				        args["plate_sim"]["collision"]["min_elevation"].get(7'000.f, {.max = max_elevation}),
				        args["plate_sim"]["collision"]["max_elevation"].get(10'000.f, {.max = max_elevation})};

				const auto elevation_response_subduction = Elevation_response{
				        args["plate_sim"]["subduction"]["uplift_speed"].get(4e-5f, {.min = 0.f}), // meters / year
				        args["plate_sim"]["subduction"]["width_scale"].get(0.2f, {.min = 0.f}),
				        args["plate_sim"]["subduction"]["peak_distance"].get(100'000.f),
				        args["plate_sim"]["subduction"]["peak_width"].get(300'000.f),
				        args["plate_sim"]["subduction"]["rise_weight"].get(0.01f, {.min = 0.01f, .max = 1.f}),
				        args["plate_sim"]["subduction"]["min_elevation"].get(5'000.f, {.max = max_elevation}),
				        args["plate_sim"]["subduction"]["max_elevation"].get(9'000.f, {.max = max_elevation})};

				const auto elevation_response_subduction_oceanic = Elevation_response{
				        args["plate_sim"]["subduction_oceanic"]["uplift_speed"].get(1e-4f, {.min = 0.f}), // meters / year
				        args["plate_sim"]["subduction_oceanic"]["width_scale"].get(0.2f, {.min = 0.f}),
				        args["plate_sim"]["subduction_oceanic"]["peak_distance"].get(100'000.f),
				        args["plate_sim"]["subduction_oceanic"]["peak_width"].get(250'000.f),
				        args["plate_sim"]["subduction_oceanic"]["rise_weight"].get(0.05f,
				                                                                   {.min = 0.01f, .max = 1.f}),
				        args["plate_sim"]["subduction_oceanic"]["min_elevation"].get(-200.f,
				                                                                     {.max = max_elevation}),
				        args["plate_sim"]["subduction_oceanic"]["max_elevation"].get(1'000.f,
				                                                                     {.max = max_elevation})};

				const auto elevation_response_trench = Elevation_response{
				        args["plate_sim"]["trench"]["uplift_speed"].get(-1.2e-4f, {.max = 0.f}), // meters / year
				        args["plate_sim"]["trench"]["width_scale"].get(0.2f, {.min = 0.f}),
				        args["plate_sim"]["trench"]["peak_distance"].get(300'000.f),
				        args["plate_sim"]["trench"]["peak_width"].get(400'000.f),
				        args["plate_sim"]["trench"]["rise_weight"].get(1.f, {.min = 0.f, .max = 1.f}),
				        args["plate_sim"]["trench"]["min_elevation"].get(-7'000.f, {.max = max_elevation}),
				        args["plate_sim"]["trench"]["max_elevation"].get(-9'000.f, {.max = max_elevation})};

				const auto continental_crust_formation_height =
				        args["plate_sim"]["continental_crust_formation_height"].get(500.f);


				if(!world)
					return;

				solve_collisions_error_shown = false;

				auto [meta_data]  = world.lock_unstructured_layer("meta");
				const auto radius = meta_data["radius"].get<float>();

				auto [data_ids,
				      data_types,
				      data_velocities,
				      data_elevations,
				      data_positions,
				      data_boundary_types_primal,
				      data_boundary_types_dual,
				      data_distances_primal,
				      data_distances_dual,
				      data_creation_times] = world.lock_layer(layer_plate_id,
				                                              layer_crust_type,
				                                              layer_velocity,
				                                              layer_elevation,
				                                              layer_position,
				                                              layer_p_boundaries,
				                                              layer_d_boundaries,
				                                              layer_p_distance,
				                                              layer_d_distance,
				                                              layer_created);

				auto [mutable_mesh] = world.lock_mesh();
				const auto mesh     = const_view(mutable_mesh);

				// populate boundary types for primal and dual edges
				identify_boundaries(mesh,
				                    data_ids.as_const(),
				                    data_types.as_const(),
				                    data_creation_times.as_const(),
				                    data_positions.as_const(),
				                    data_velocities.as_const(),
				                    data_distances_primal,
				                    data_boundary_types_primal,
				                    data_distances_dual,
				                    data_boundary_types_dual,
				                    collision_velocity,
				                    subduction_velocity,
				                    ridge_velocity,
				                    ridge_formation_distance,
				                    radius);

				for(auto iteration = 0; iteration < sub_steps; iteration++) {
					update_plate_properties(mesh,
					                        data_types.as_const(),
					                        data_positions.as_const(),
					                        data_velocities.as_const(),
					                        data_boundary_types_primal.as_const(),
					                        data_boundary_types_dual.as_const(),
					                        data_distances_primal,
					                        data_distances_dual,
					                        sub_step_dt,
					                        radius,
					                        slab_pull_force,
					                        collision_min_distance,
					                        collision_max_distance,
					                        collision_force_multiplier,
					                        ridge_push_force,
					                        spring_constant_same_plate);

					move_plates(data_positions, data_velocities, radius, sub_step_dt, plate_drag, plate_mass);

					update_elevation(mesh,
					                 data_positions.as_const(),
					                 data_velocities.as_const(),
					                 data_boundary_types_primal.as_const(),
					                 data_boundary_types_dual.as_const(),
					                 data_ids,
					                 data_types,
					                 data_elevations,
					                 sub_step_dt,
					                 min_elevation,
					                 max_elevation,
					                 radius,
					                 max_velocity,
					                 {elevation_response_collision,
					                  elevation_response_subduction_oceanic,
					                  elevation_response_subduction,
					                  elevation_response_trench},
					                 continental_crust_formation_height);

					if(solve_collisions(mutable_mesh,
					                    data_ids,
					                    data_types,
					                    data_creation_times,
					                    data_velocities,
					                    data_positions,
					                    data_elevations,
					                    data_boundary_types_primal.as_const(),
					                    sub_step_dt,
					                    radius)) {
						clear_invalid_plate_assignments(mesh,
						                                data_ids.as_const(),
						                                data_positions.as_const(),
						                                data_distances_primal,
						                                data_boundary_types_primal,
						                                data_distances_dual,
						                                data_boundary_types_dual);
					}
				}

				// populate boundary types for primal and dual edges
				identify_boundaries(mesh,
				                    data_ids.as_const(),
				                    data_types.as_const(),
				                    data_creation_times.as_const(),
				                    data_positions.as_const(),
				                    data_velocities.as_const(),
				                    data_distances_primal,
				                    data_boundary_types_primal,
				                    data_distances_dual,
				                    data_boundary_types_dual,
				                    collision_velocity,
				                    subduction_velocity,
				                    ridge_velocity,
				                    ridge_formation_distance,
				                    radius);

				meta_data["age"].get<float&>() += delta_time;
			}
		};
	} // namespace

	extern const auto simulate_plates = to_module_info<Plate_sim>("tectonics_simulate_plates");

} // namespace yggdrasill::modules::tectonics

YGDL_REGISTER_PLUGIN_MODULES(&yggdrasill::modules::tectonics::simulate_plates)
