#pragma once

#include <yggdrasill/layer.hpp>

#include <cstdint>

namespace yggdrasill::modules::tectonics {

	enum class Crust_type : std::int8_t { none = 0, oceanic = 1, continental = 2 };

	enum class Boundary_type : int8_t {
		transform,
		subducting_origin, /// origin/dest as seen from the smallest of the two possible edges
		subducting_dest,
		collision,
		joined,
		ridge
	};

	using Plate_id = std::int32_t;


	// input
	constexpr auto layer_position = Layer_definition<Vec3, Ref_type::vertex>("position");
	using Layer_position          = view_of_def<layer_position>;



	// vertex layers
	constexpr auto layer_plate_id = Layer_definition<Plate_id, Ref_type::vertex>("plate_id")
	                                        .invalidation(Invalidation::reset_affected)
	                                        .interpolation(Interpolation::max_weight)
	                                        .min(0);
	using Layer_plate_id = view_of_def<layer_plate_id>;

	constexpr auto layer_crust_type = Layer_definition<Crust_type, Ref_type::vertex>("crust_type")
	                                          .interpolation(Interpolation::max_weight)
	                                          .min(Crust_type::none)
	                                          .max(Crust_type::continental);
	using Layer_crust_type = view_of_def<layer_crust_type>;

	constexpr auto layer_velocity =
	        Layer_definition<Vec3, Ref_type::vertex>("velocity").interpolation(Interpolation::max_weight);
	using Layer_velocity = view_of_def<layer_velocity>;

	constexpr auto layer_elevation =
	        Layer_definition<float, Ref_type::vertex>("elevation").interpolation(Interpolation::lerp);
	using Layer_elevation = view_of_def<layer_elevation>;

	constexpr auto layer_created =
	        Layer_definition<float, Ref_type::vertex>("crust_created").interpolation(Interpolation::lerp);
	using Layer_created = view_of_def<layer_created>;

	constexpr auto layer_density = Layer_definition<float, Ref_type::vertex>("refine_target_density")
	                                       .interpolation(Interpolation::lerp)
	                                       .invalidation(Invalidation::reset_affected)
	                                       .initial(-1.f)
	                                       .max(1.f);
	using Layer_density = view_of_def<layer_density>;


	// edge layers
	constexpr auto layer_p_distance = Layer_definition<float, Ref_type::edge_primal>("primal_plate_distance")
	                                          .initial(-1.f)
	                                          .min(-1.f)
	                                          .invalidation(Invalidation::reset_affected)
	                                          .interpolation(Interpolation::lerp);
	using Layer_p_distance = view_of_def<layer_p_distance>;

	constexpr auto layer_p_boundaries =
	        Layer_definition<Boundary_type, Ref_type::edge_primal>("primal_plate_boundaries")
	                .initial(Boundary_type::transform)
	                .min(Boundary_type::transform)
	                .max(Boundary_type::ridge)
	                .invalidation(Invalidation::reset_affected)
	                .interpolation(Interpolation::reset_affected);
	using Layer_p_boundaries = view_of_def<layer_p_boundaries>;

	constexpr auto layer_d_distance = Layer_definition<float, Ref_type::edge_dual>("dual_plate_distance")
	                                          .initial(-1.f)
	                                          .min(-1.f)
	                                          .invalidation(Invalidation::reset_affected)
	                                          .interpolation(Interpolation::lerp);
	using Layer_d_distance = view_of_def<layer_d_distance>;

	constexpr auto layer_d_boundaries =
	        Layer_definition<Boundary_type, Ref_type::edge_dual>("dual_plate_boundaries")
	                .initial(Boundary_type::transform)
	                .min(Boundary_type::transform)
	                .max(Boundary_type::ridge)
	                .invalidation(Invalidation::reset_affected)
	                .interpolation(Interpolation::reset_affected);
	using Layer_d_boundaries = view_of_def<layer_d_boundaries>;

} // namespace yggdrasill::modules::tectonics
