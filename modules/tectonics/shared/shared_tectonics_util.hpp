#pragma once

#include <shared_tectonics_layer.hpp>

#include <yggdrasill/mesh_utils.hpp>

#include <array>
#include <unordered_set>
#include <vector>


namespace yggdrasill::modules::tectonics {

	/// returns a function that flips the given edge, but also swaps the boundary-type and distance of the edge and its dual
	inline auto flip_edge(Mesh_view                        mesh,
	                      const Layer_position::const_view positions,
	                      Layer_p_distance                 distance_p,
	                      Layer_p_boundaries               boundaries_p,
	                      Layer_d_distance                 distance_d,
	                      Layer_d_boundaries               boundaries_d)
	{
		return [=](Primal_edge e) mutable {
			const auto boundary = std::pair{boundaries_p[e], boundaries_d[e.rot()]};
			const auto distance = std::pair{distance_p[e], distance_d[e.rot()]};
			if(try_flip(mesh, e, positions)) {
				std::tie(boundaries_d[e.rot()], boundaries_p[e]) = boundary;
				std::tie(distance_d[e.rot()], distance_p[e])     = distance;
				return true;
			}
			return false;
		};
	}

	inline std::array<Valid<Vertex>, 2> edge_vertices(const Const_mesh_view mesh, const Primal_edge e)
	{
		return e.vertices(mesh);
	}
	inline std::array<Valid<Vertex>, 2> edge_vertices(const Const_mesh_view mesh, const Dual_edge e)
	{
		const auto pe = e.rot();
		return {pe.origin_next(mesh).dest(mesh), pe.origin_prev(mesh).dest(mesh)};
	}

	// required to pass floats as non-type template parameters because clang somehow managed to
	//   implement the struct- but not the float-part of P0732R2...
	struct Float_wrapper {
		float value;
	};

	namespace detail {
		template <Float_wrapper Alpha, typename T>
		T constexpr_lerp(const T& a, const T& b)
		{
			if constexpr(Alpha.value == 0.f)
				return a;
			else if constexpr(Alpha.value == 1.f)
				return b;
			else
				return normalized(lerp(a, b, Alpha.value));
		}

		struct Noop_validate {
			constexpr bool operator()(const Vertex v, const Vec3& v_position, const Vec3& new_position)
			{
				return true;
			}
		};
	} // namespace detail

	template <Float_wrapper... Alphas, typename Validator = detail::Noop_validate>
	inline bool try_any_collapse(Mesh_view                                      mesh,
	                             const Primal_edge                              edge,
	                             const Const_layer_view<Vec3, Ref_type::vertex> positions,
	                             Validator&&                                    validate = {})
	{
		constexpr auto alphas = std::array{Alphas.value...};

		const auto origin         = positions[edge.origin(mesh)];
		const auto dest           = positions[edge.dest(mesh)];
		const auto new_positions  = std::array{detail::constexpr_lerp<Alphas>(origin, dest)...};
		auto       valid_position = std::array{(Alphas.value == Alphas.value)...};

		const auto check_winding_order = [&](const Primal_edge e) {
			const auto range = e.origin_ccw(mesh).skip(1);
			auto       iter  = range.begin();

			auto prev = positions[iter->dest(mesh)];
			++iter;

			std::for_each(iter, range.end_iterator(), [&](Primal_edge e) {
				const auto v    = e.dest(mesh);
				const auto curr = positions[v];
				for(auto i = std::size_t(0); i < sizeof...(Alphas); i++) {
					valid_position[i] = valid_position[i]
					                    && !is_wrong_winding_order(new_positions[i], prev, curr)
					                    && validate(v, curr, new_positions[i]);
				}

				prev = curr;
			});
		};

		check_winding_order(edge);
		check_winding_order(edge.sym());

		for(auto i = std::size_t(0); i < sizeof...(Alphas); i++) {
			if(valid_position[i])
				return mesh.try_collapse(edge, alphas[i]) != no_vertex;
		}

		return false;
	}


	constexpr std::optional<Vertex> get_subducting_plate(const Vertex     a,
	                                                     const Vertex     b,
	                                                     const Crust_type a_type,
	                                                     const Crust_type b_type,
	                                                     const float      a_created,
	                                                     const float      b_created)
	{
		switch(a_type) {
			case Crust_type::none: return a;
			case Crust_type::oceanic:
				if(b_type == Crust_type::oceanic)
					return a_created > b_created ? b : a; // older subducts
				else
					return a; // oceanic subducts

			case Crust_type::continental:
				if(b_type == Crust_type::continental)
					return std::nullopt;
				else
					return b;
		}

		assert(!"unreachable");
		return std::nullopt;
	}


	inline std::optional<Vertex> get_subducting_plate(const Vertex                       a,
	                                                  const Vertex                       b,
	                                                  const Layer_crust_type::const_view types,
	                                                  const Layer_created::const_view    creation_times)
	{
		return get_subducting_plate(a, b, types[a], types[b], creation_times[a], creation_times[b]);
	}


	/// Tries to collapse the edge into one of its vertices.
	/// Prefers to keep the vertex that would not be subducted (lower density).
	/// Updates the layer data appropriately.
	/// Note: only uses the primal edge boundary, because (a) this function is only called to try to
	///   collapse primal edges and (b) when one vertex is subducted, the edge between them is extremely
	///   short, which (after delaunay normalization) has a high probability of being a primal
	///   instead of a dual edge.
	template <typename Validator = detail::Noop_validate>
	inline auto try_collapse_subducting(Mesh_view                                       mesh,
	                                    Layer_plate_id                                  ids,
	                                    Layer_crust_type                                types,
	                                    Layer_created                                   creation_times,
	                                    Layer_velocity                                  velocities,
	                                    const std::optional<Layer_position::const_view> positions,
	                                    Layer_elevation                                 elevations,
	                                    Layer_p_boundaries::const_view                  boundaries,
	                                    Vertex                                          v1_in,
	                                    Vertex                                          v2_in,
	                                    Primal_edge                                     edge_in,
	                                    Validator&&                                     validate = {})
	{
		const auto boundary = boundaries[edge_in];
		if(boundary == Boundary_type::ridge)
			return false; // ignore ridges, to don't disturb plate separation

		// normalize edge
		if(!edge_in.is_base()) {
			edge_in = edge_in.sym();
			std::swap(v1_in, v2_in);
		}

		auto impl = [&](const Vertex remaining_vertex, const Vertex subducting_vertex, const Primal_edge e) {
			assert(remaining_vertex == e.origin(mesh));
			assert(subducting_vertex == e.dest(mesh));

			const auto remaining_id        = ids[remaining_vertex];
			const auto remaining_type      = types[remaining_vertex];
			const auto remaining_velocity  = velocities[remaining_vertex];
			const auto remaining_elevation = elevations[remaining_vertex];

			if(positions) {
				if(!try_any_collapse<Float_wrapper{0.0f}, Float_wrapper{0.4f}, Float_wrapper{1.f}>(
				           mesh, e, *positions, validate))
					return false;

			} else {
				if(mesh.try_collapse(e, 0.f) == no_vertex)
					return false;
			}

			// restore layer values, which might have been modified by collapse
			ids[remaining_vertex]        = remaining_id;
			types[remaining_vertex]      = remaining_type;
			velocities[remaining_vertex] = remaining_velocity;
			elevations[remaining_vertex] = remaining_elevation;

			return true;
		};


		// calc e := edge from the remaining to the subducted plate
		const auto subducting_v = [&]() -> std::optional<Vertex> {
			if(boundary == Boundary_type::subducting_origin)
				return v1_in;
			else if(boundary == Boundary_type::subducting_dest)
				return v2_in;
			else
				return get_subducting_plate(v1_in, v2_in, types, creation_times);
		}();

		if(!subducting_v) {
			if(positions) {
				if(!try_any_collapse<Float_wrapper{0.5f}, Float_wrapper{0.0f}, Float_wrapper{1.f}>(
				           mesh, edge_in, *positions))
					return false;
			} else if(mesh.try_collapse(edge_in, 0.5f) == no_vertex)
				return false;

			return true;

		} else if(*subducting_v == v1_in)
			return impl(v2_in, v1_in, edge_in.sym());

		else
			return impl(v1_in, v2_in, edge_in);
	}

	class Delaunay_restorer {
	  private:
		struct Edge_data {
			Primal_edge edge;
			Vertex      origin;
			Vertex      dest;
			Vertex      opposite;
		};

	  public:
		void restore(Mesh_view                          mesh,
		             const Layer_position::const_view   positions,
		             const Layer_crust_type::const_view types,
		             const Layer_plate_id::const_view   ids,
		             Layer_p_distance                   distances_p,
		             Layer_p_boundaries                 boundaries_p,
		             Layer_d_distance                   distances_d,
		             Layer_d_boundaries                 boundaries_d,
		             const index_t                      max_tries)
		{
			auto flip = flip_edge(mesh, positions, distances_p, boundaries_p, distances_d, boundaries_d);

			auto process_face = [&](const Face f) {
				if(!f.valid(mesh))
					return; // invalidated by previous operations

				// retrieve the edges
				const auto e1 = f.edge(mesh);
				const auto e2 = e1.left_next(mesh);
				const auto e3 = e1.left_prev(mesh);

				// retrieve the 3 vertices
				const auto v1 = e1.origin(mesh);
				const auto v2 = e1.dest(mesh);
				const auto v3 = e2.dest(mesh);

				// derive all vertices and edges from these
				const auto edges = std::array{
				        Edge_data{e1, v1, v2, v3}, Edge_data{e2, v2, v3, v1}, Edge_data{e3, v3, v1, v2}};

				const auto p1 = positions[edges[0].origin];
				const auto p2 = positions[edges[1].origin];
				const auto p3 = positions[edges[2].origin];

				const auto center = circumcenter(p1, p2, p3);
				const auto radius = dist2(p1, center);

				for(const auto& e : edges) {
					const auto e_prev = e.edge.origin_prev(mesh);
					const auto v      = e_prev.dest(mesh);

					const auto distance = dist2(positions[v], center);
					if(distance >= radius * 0.9f)
						continue;

					// preserve the boundary between different plates, except for collision boundaries
					const auto origin_id   = ids[e.origin];
					const auto dest_id     = ids[e.dest];
					const auto v_id        = ids[v];
					const auto opposite_id = ids[e.opposite];
					if((origin_id == dest_id) & (origin_id != v_id) & (origin_id != opposite_id)) {
						if(boundaries_d[e.edge.rot()] != Boundary_type::collision)
							continue;
					}
					if((v_id == opposite_id) & (v_id != origin_id) & (v_id != dest_id)) {
						if(boundaries_p[e.edge] != Boundary_type::collision)
							continue;
					}

					// also preserve oceanic/continental boundaries
					const auto origin_type = types[e.origin];
					if(origin_type == types[e.dest] && origin_type != types[v] && types[v] == types[e.opposite])
						continue;

					if(flip(e.edge)) {
						if(const auto lf = e.edge.left(mesh); delaunay_seen_faces_.insert(lf).second)
							delaunay_open_lists_[delaunay_open_lists_next_].push_back(lf);
						if(const auto rf = e.edge.right(mesh); delaunay_seen_faces_.insert(rf).second)
							delaunay_open_lists_[delaunay_open_lists_next_].push_back(rf);

						break;
					}
				}
			};


			delaunay_open_lists_[delaunay_open_lists_next_].clear();
			delaunay_seen_faces_.clear();

			for(const auto f : mesh.faces(concurrent_modifications)) {
				process_face(f);
			}

			auto tries = index_t(0);
			while(!delaunay_open_lists_[delaunay_open_lists_next_].empty() && tries++ < max_tries) {
				const auto& curr          = delaunay_open_lists_[delaunay_open_lists_next_];
				delaunay_open_lists_next_ = delaunay_open_lists_next_ == 1 ? 0 : 1;
				delaunay_open_lists_[delaunay_open_lists_next_].clear();
				delaunay_seen_faces_.clear();
				for(const auto f : curr)
					process_face(f);
			}
		}

	  private:
		std::unordered_set<Face>         delaunay_seen_faces_;
		std::array<std::vector<Face>, 2> delaunay_open_lists_;
		int                              delaunay_open_lists_next_ = 0;
	};


	inline void foreach_edge_without_new(const Const_mesh_view mesh, auto&& f)
	{
		const auto max_index = mesh.edge_max_index();

		for(auto i = index_t(0); i < max_index; i += 2) {
			const auto e = Primal_edge{i};
			if(e.valid(mesh))
				f(e);
		}
	}


	constexpr float smootherstep_unclamped(float x)
	{
		return x * x * x * (x * (x * 6.f - 15.f) + 10.f);
	}
	constexpr float smootherstep(float x)
	{
		return smootherstep_unclamped(std::clamp(x, 0.0f, 1.0f));
	}
	constexpr float smootherstep(float edge0, float edge1, float x)
	{
		return smootherstep((x - edge0) / (edge1 - edge0));
	}

} // namespace yggdrasill::modules::tectonics
