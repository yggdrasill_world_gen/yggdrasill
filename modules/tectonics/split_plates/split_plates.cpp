#include <shared_tectonics_layer.hpp>

#include <yggdrasill/layer.hpp>
#include <yggdrasill/mesh.hpp>
#include <yggdrasill/mesh_utils.hpp>
#include <yggdrasill/module.hpp>

#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <limits>
#include <numbers>
#include <numeric>
#include <unordered_map>
#include <utility>
#include <vector>



namespace yggdrasill::modules::tectonics {
	namespace {

		constexpr auto deg_to_rad = (std::numbers::pi_v<float> / 180.f);
		constexpr auto deg_360    = 360 * deg_to_rad;

		const auto     max_distance = (2.f * std::numbers::pi_v<float>) *0.4f;
		constexpr auto max_angle    = 120.f / 2.f * deg_to_rad;


		auto great_circle_angle(const Vec3& normal_a, const Vec3& normal_b, bool forward)
		{
			const auto cos_alpha = dot(normal_a, normal_b);
			auto       alpha     = cos_alpha <= -1.0f ? std::numbers::pi_v<float>
			                                          : (cos_alpha >= 1.f ? 0.f : std::acos(cos_alpha));

			if(!forward)
				alpha += 180.f * deg_to_rad;

			// normalize angle between +-180°
			return alpha - std::floor(alpha / deg_360 + 0.5f) * deg_360;
		}


		struct Meta_plate {
			Vec3   position_sum = Vec3(0, 0, 0);
			float  area         = 0.f;
			Vertex any_vertex;
			bool   continental_vertex = false;
		};

		struct Split_plates {
			std::unordered_map<Plate_id, Meta_plate> plate_areas;
			Plate_id                                 next_plate_id = 1;

			void collect_meta_plates(World_view world, const Const_mesh_view mesh, const float oceanic_influence)
			{
				plate_areas.clear();
				next_plate_id = 1;

				auto&& [ids, types, positions] =
				        world.required_layer(layer_plate_id, layer_crust_type, layer_position);

				for(const auto [v, id] : range(mesh, ids)) {
					if(id == 0)
						continue;

					next_plate_id = std::max(next_plate_id, id + 1);

					auto& plate = plate_areas[id];

					if(!plate.continental_vertex) {
						plate.any_vertex         = v;
						plate.continental_vertex = types[v] == Crust_type::continental;
					}

					if(types[v] == Crust_type::continental) {
						plate.area += normalized_voronoi_area(mesh, positions, v);
						plate.position_sum += positions[v];

					} else {
						plate.area += normalized_voronoi_area(mesh, positions, v) * oceanic_influence;
						plate.position_sum += positions[v] * oceanic_influence;
					}
				}
			}

			/// locate a suitable starting point for the split and pick one of its neighbors to define the direction of the split-line
			auto find_start_and_direction(const Const_mesh_view              mesh,
			                              Random_view&                       random,
			                              const Layer_plate_id::const_view   ids,
			                              const Layer_crust_type::const_view types,
			                              const Layer_position::const_view   positions,
			                              const Plate_id                     id,
			                              const Vertex                       any_vertex,
			                              const Vec3&                        avg_position)
			{
				// find starting point by locating approximate center of plate and picking the outgoing edge with the lowest stress
				const auto start_target_position = normalized(avg_position + random.uniform_vec3(-0.01f, 0.01f));
				const auto start_vertex = minimizing_hill_climb(
				        mesh, any_vertex, 256, Neighbor_type::with_dual_edges, [&](Vertex v) {
					        if(ids[v] != id)
						        return std::numeric_limits<float>::infinity();

					        return dist2(positions[v], start_target_position);
				        });

				// pick the initial direction, depending on the highest priority neighbor: plate-boundary => ocean-continental boundary => random
				auto next_vertex =
				        start_vertex.neighbors(mesh, Neighbor_type::just_primal_edges).find_if([&](Vertex v) {
					        return ids[v] != id;
				        });

				if(!next_vertex) {
					const auto start_type = types[start_vertex];
					next_vertex =
					        start_vertex.neighbors(mesh, Neighbor_type::just_primal_edges).find_if([&](Vertex v) {
						        return types[v] != start_type;
					        });
				}

				if(!next_vertex) {
					next_vertex =
					        start_vertex.edges(mesh).min([&](auto&&) { return random.uniform(0.f, 1.f); })->dest(mesh);
				}

				return std::pair{start_vertex, *next_vertex};
			}

			/// Calculate the path the split should take, starting from the given vertex (and first step).
			/// Returns two paths. The first from start_vertex, through next_vertex to the nearest reachable boundary
			///   and a second path in the opposite direction
			auto find_split_path(const Const_mesh_view              mesh,
			                     Random_view&                       random,
			                     const Layer_plate_id::const_view   ids,
			                     const Layer_p_distance::const_view target_distances,
			                     const Layer_position::const_view   positions,
			                     const Plate_id                     id,
			                     const Vertex                       start_vertex,
			                     const Vertex                       next_vertex)
			{
				using namespace yggdrasill::path_finding;

				const auto origin_pos       = positions[start_vertex];
				const auto direction        = cross(positions[next_vertex], origin_pos);
				const auto direction_len    = length(direction);
				const auto direction_normal = direction / direction_len;

				const auto origin_distance_and_angle = [&](Vertex v, bool forward) {
					const auto p = positions[v];

					const auto p_cross_origin     = cross(p, origin_pos);
					const auto p_cross_origin_len = length(p_cross_origin);

					const auto distance = std::atan2(p_cross_origin_len, dot(p, origin_pos));

					return std::pair{
					        distance,
					        great_circle_angle(p_cross_origin / p_cross_origin_len, direction_normal, forward)};
				};

				// 0 := distance==target_distance
				const auto plate_stress = [&](Primal_edge e) {
					if(e == no_edge)
						return 0.f;

					const auto base_e = e.base();
					const auto origin = base_e.origin(mesh);
					const auto dest   = base_e.dest(mesh);

					const auto target_distance_2  = target_distances[base_e] * target_distances[base_e];
					const auto current_distance_2 = dist2(positions[origin], positions[dest]);

					if(target_distance_2 > current_distance_2)
						return 1.f - current_distance_2 / target_distance_2;
					else
						return 1.f - target_distance_2 / current_distance_2;
				};

				auto cost_function = [&](bool forward, Vertex start, Vertex other) {
					return [&, forward, start, other](Primal_edge e, Vertex prev, Vertex from, Vertex to) -> Cost {
						if(to == start)
							return 1.f; // start vertex
						if(to == other)
							return Unreachable{};

						const auto [distance, alpha] = origin_distance_and_angle(to, forward);
						const auto abs_alpha         = std::abs(alpha);
						if(distance > max_distance || abs_alpha > max_angle)
							return Unreachable{};

						if(ids[to] != id)
							return Target_found{};

						const auto angle_cost = [&] {
							if(prev == no_vertex || from == no_vertex)
								return 0.1f;

							const auto prev_p = positions[prev];
							const auto from_p = positions[from];
							const auto to_p   = positions[to];

							const auto direction_change = std::abs(great_circle_angle(
							        normalized(cross(from_p, prev_p)), normalized(cross(to_p, from_p)), true));

							if(direction_change < 25 * deg_to_rad)
								return 2.f * abs_alpha;

							return 10.f * (direction_change / deg_360) + 2.f * abs_alpha;
						}();

						const auto stress      = plate_stress(e);
						const auto stress_cost = std::min(4.f, stress * stress * 4e6f);

						const auto noise =
						        10.f * static_cast<float>(((31 * from.index()) ^ to.index()) % 100) / 100.f;

						return 1.f + stress_cost * 0.1f + angle_cost * 0.5f + noise * 0.2f;
					};
				};

				auto paths = std::pair{
				        shortest_path(mesh, next_vertex, cost_function(true, next_vertex, start_vertex)),
				        shortest_path(mesh, start_vertex, cost_function(false, start_vertex, next_vertex))};

				if(paths.first.empty() || paths.second.empty()) {
					// no valid path found => give up
					paths.first.clear();
					paths.second.clear();
				} else {
					// remove the last vertex from each path, because the destination by-definition
					//   already lies beyond our plate
					paths.first.pop_back();
					paths.second.pop_back();
					assert(!paths.second.empty()); // always contains at least start_vertex
				}

				return paths;
			}

			template <typename Positions>
			void reassign_plates(World_view                 world,
			                     Const_mesh_view            mesh,
			                     const Positions&           positions,
			                     const float                radius,
			                     const float                dt,
			                     const float                split_push,
			                     const Plate_id             id,
			                     const Plate_id             new_id,
			                     const Vertex               start_vertex,
			                     const Vertex               next_vertex,
			                     const std::vector<Vertex>& path_a,
			                     const std::vector<Vertex>& path_b)
			{
				auto [ids, velocities] = world.lock_layer(layer_plate_id, layer_velocity);

				// 1. Assign each vertex on the path to the new plate
				for(auto& p : Ref_range{path_a, path_b}) {
					for(auto v : p) {
						ids[v] = new_id;
					}
				}

				// 2. Memorize each vertex on the left and right side.
				//    One side will be assigned to new_id and the other (temporarily) to new_id+1.
				auto flood_fill_seeds = std::vector<std::pair<Vertex, Plate_id>>();
				flood_fill_seeds.reserve(path_a.size() + path_b.size());
				for(auto& p : Ref_range{path_a, path_b}) {
					// Which side gets assigned which id is different for the two halves of the path, because
					//   one of them is flipped relative to the other
					const auto [left, right] =
					        &p == &path_a ? std::pair{new_id, new_id + 1} : std::pair{new_id + 1, new_id};

					for(std::size_t i = 1; i < p.size(); ++i) {
						const auto e = mesh.find_edge(p[i - 1], p[i]);

						if(auto v = e.origin_next(mesh).dest(mesh); ids[v] == id)
							flood_fill_seeds.emplace_back(v, left);

						if(auto v = e.origin_prev(mesh).dest(mesh); ids[v] == id)
							flood_fill_seeds.emplace_back(v, right);
					}
				}

				// 3. Use the seed vertices and ids memorized in 2. to change the id of all sub-plates previously assigned to id
				flood_fill(mesh,
				           std::move(flood_fill_seeds),
				           Neighbor_type::just_primal_edges,
				           [&](Vertex v, Plate_id target_id) {
					           if(ids[v] != id)
						           return false;

					           ids[v] = target_id;
					           return true;
				           });

				// 4. Last pass over all vertices to (a) push them apart and (b) reassign new_id+1 to the original id
				const auto velocity_push_angle = -1.f * split_push;
				const auto velocity_push_axis  = [&] {
                    if(!path_a.empty()) {
                        return normalized(positions[path_a.back()] - positions[path_b.back()]);
                    } else if(path_b.size() > 1) {
                        return normalized(positions[path_b.front()] - positions[path_b.back()]);
                    } else {
                        // split-path is only a single vertex long
                        return normalized(positions[next_vertex] - positions[start_vertex]);
                    }
				}();
				const auto push_velocity = [&](const Vec3& p, Vec3& velocity, float factor = 1.f) {
					const auto alpha = velocity_push_angle * factor;
					const auto np    = p * std::cos(alpha) + cross(velocity_push_axis, p) * std::sin(alpha)
					                + velocity_push_axis * dot(velocity_push_axis, p) * (1.f - std::cos(alpha))
					                + velocity * dt;
					velocity = (normalized(np) * radius - p) / dt;
				};

				for(auto v : mesh.vertices()) {
					if(ids[v] == new_id) {
						push_velocity(positions[v] * radius, velocities[v], 1.f);
					}
					if(ids[v] == new_id + 1) {
						ids[v] = id;
						push_velocity(positions[v] * radius, velocities[v], -1.f);
					}
				}
			}

			/// Partitions the plate into 2-3 new plates, assigning new plate-ids and modifies the velocity to separate them
			void split_plate(World_view      world,
			                 Const_mesh_view mesh,
			                 Random_view&    random,
			                 const float     radius,
			                 const float     dt,
			                 const float     split_push,
			                 const Plate_id  id,
			                 const Plate_id  new_id,
			                 const Vertex    any_vertex,
			                 const Vec3&     avg_position)
			{
				auto [ids, types, positions, target_distances] =
				        world.required_layer(layer_plate_id, layer_crust_type, layer_position, layer_p_distance);

				// select a starting point
				const auto [start_vertex, next_vertex] = find_start_and_direction(
				        mesh, random, ids, types, positions, id, any_vertex, avg_position);

				// find two paths, in opposite directions, to the nearest border
				const auto [path_a, path_b] = find_split_path(
				        mesh, random, ids, target_distances, positions, id, start_vertex, next_vertex);
				if(path_a.empty() && path_b.empty()) {
					std::cerr << "Couldn't find a valid path to split plate " << id << '\n';
					return;
				}

				// assign one half of the calculated path to a new plate and give it a small push away from the boundary
				reassign_plates(
				        world, mesh, positions, radius, dt, split_push, id, new_id, start_vertex, next_vertex, path_a, path_b);
			}

			std::array<std::vector<Vertex>, 2> open_vertices;

			/// basically uses the same method as the generate_plates module to split the sphere into 2-4 new plates
			void split_single_plate(
			        World_view world, const Const_mesh_view mesh, Random_view& random, const float radius)
			{
				auto [ids, velocities] = world.lock_layer(layer_plate_id, layer_velocity);
				const auto positions   = world.required_layer(layer_position);

				const auto count = random.uniform(2, 4);

				for(auto& v : open_vertices) {
					v.clear();
					v.reserve(static_cast<std::size_t>(count) * 10);
				}

				ids.reset(0);

				for(auto i = 0; i < count; i++) {
					if(auto v = random_vertex(mesh, random, 32, [&](auto v) { return ids[v] == 0; })) {
						open_vertices[0].push_back(*v);
						ids[*v] = i + 1;

						// set random direction
						const auto angle     = random.uniform(0.2f, 2.f * 3.141f);
						const auto speed     = random.uniform(0.05f, 0.2f);
						const auto normal    = positions[*v];
						const auto tangent   = cross(normal, Vec3(-normal.z, normal.x, normal.y));
						const auto bitangent = cross(normal, tangent);
						velocities[*v] = (tangent * std::sin(angle) + bitangent * std::cos(angle)) * speed;
					}
				}

				// flood-fill to assign remaining sub-plates, starting from N seed vertices
				auto current = 0;
				while(!open_vertices[current].empty()) {
					auto next = current == 0 ? 1 : 0;

					for(auto v : open_vertices[current]) {
						const auto id       = ids[v];
						const auto velocity = velocities[v];

						for(auto n : v.neighbors(mesh, Neighbor_type::just_primal_edges)) {
							if(ids[n] == 0) {
								ids[n] = id;

								auto p        = positions[n] * radius;
								auto np       = normalized(p + velocity * 100.f) * length(p);
								velocities[n] = (np - p) / 100.f;

								open_vertices[next].push_back(n);
							}
						}
					}

					open_vertices[current].clear();
					random.shuffle(open_vertices[next]);
					current = next;
				}

				next_plate_id = count + 1;
			}


			void execute(World_view world, yggdrasill::Any_dictionary auto cfg)
			{
				const auto dt = cfg["plate_sim"]["delta_time"].get(1'000'000.f, {.min = 0.01f}); // years
				const auto rift_events_per_year =
				        cfg["plate"]["rifting"]["events_per_year"].get(6e-8f, {.min = 0.f});
				const auto oceanic_influence =
				        cfg["plate"]["rifting"]["oceanic_influence"].get(1e-8f, {.min = 0.f});
				const auto split_push_meters =
				        cfg["plate"]["rifting"]["split_push"].get(0.05f, {.min = 0.f}); // meters / year
				const auto split_push_percent = split_push_meters / (2.f * std::numbers::pi_v<float>) *deg_360;


				if(!world)
					return;

				const auto radius = world.required_unstructured_layer("meta")["radius"].get<float>();
				auto       random = world.random();
				const auto mesh   = world.mesh();

				collect_meta_plates(world, mesh, oceanic_influence);

				if(plate_areas.size() <= 1) {
					// Special-case: The algorithm in split_plate has a high error-rate if the continent
					//   spans the entire sphere and loops back onto itself and doesn't work at all if
					//   there is only one continent left.
					// So we handle the most problematic case here. If there are less than two continents,
					//   we use a simple flood-fill to split it into 2-4 new plates.
					split_single_plate(world, mesh, random, radius);
					collect_meta_plates(world, mesh, oceanic_influence);
				}

				const auto area_sum = std::accumulate(
				        plate_areas.begin(), plate_areas.end(), 0.f, [](float sum, const auto& e) {
					        return sum + e.second.area;
				        });

				for(auto&& [id, plate] : plate_areas) {
					const auto area_ratio = plate.area / area_sum;
					if(area_ratio <= 0.1f)
						continue;

					// calculate if the plate should be split
					const auto expected_rifts = rift_events_per_year * dt * std::pow(area_ratio, 2.f);
					// P(X>=1) for poisson distribution with average frequency in timestep of expected_rifts
					//   = 1 - (expected_rifts^0 * e^-expected_rifts) / 0!
					const auto probability = 1.f - std::exp(-expected_rifts);

					if(random.probability(probability)) {
						split_plate(world,
						            mesh,
						            random,
						            radius,
						            dt,
						            split_push_percent,
						            id,
						            next_plate_id++,
						            plate.any_vertex,
						            normalized(plate.position_sum));
					}
				}
			}
		};
	} // namespace

	extern const auto split_plates = to_module_info<Split_plates>("tectonics_split_plates");

} // namespace yggdrasill::modules::tectonics

YGDL_REGISTER_PLUGIN_MODULES(&yggdrasill::modules::tectonics::split_plates)
