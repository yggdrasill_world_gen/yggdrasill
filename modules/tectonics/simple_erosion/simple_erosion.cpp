#include <shared_tectonics_layer.hpp>
#include <shared_tectonics_util.hpp>

#include <yggdrasill/layer.hpp>
#include <yggdrasill/mesh.hpp>
#include <yggdrasill/module.hpp>

#include <algorithm>

namespace yggdrasill::modules::tectonics {

	namespace {
		void execute(World_view world, yggdrasill::Any_dictionary auto cfg)
		{
			const auto dt = cfg["plate_sim"]["delta_time"].get(1'000'000.f, {.min = 0.01f}); // years
			const auto max_velocity = cfg["plate"]["max_velocity"].get(0.2f, {.min = 0.001f});

			// meters
			const auto min_elevation             = cfg["elevation"]["min"].get(-1e4f);
			const auto max_elevation             = cfg["elevation"]["max"].get(1e4f, {.min = min_elevation});
			const auto ocean_elevation           = cfg["elevation"]["abyssal_plains"].get(-6000.f);
			const auto min_continental_elevation = cfg["elevation"]["min_continental"].get(-500.f);

			// meter/year
			const auto oceanic_dampening  = cfg["erosion"]["oceanic_dampening"].get(5e-5f, {.min = 0.f});
			const auto sediment_accretion = cfg["erosion"]["sediment_accretion"].get(1e-4f, {.min = 0.f});
			const auto simple_erosion     = cfg["erosion"]["continental_erosion"].get(6e-5f, {.min = 0.f});

			const auto ridge_width     = cfg["ridge"]["width"].get(250'000.f);
			const auto ridge_elevation = cfg["ridge"]["elevation"]["min"].get(-2'000.f, {.min = -10'000.f});

			const auto sink_duration = ridge_width / max_velocity; // in years
			const auto ridge_dampening =
			        ocean_elevation / (ridge_elevation - (1.f - ridge_elevation / min_elevation) * sink_duration);


			if(!world)
				return;

			const auto now        = world.required_unstructured_layer("meta")["age"].get<float>();
			auto [types, created] = world.required_layer(layer_crust_type, layer_created);
			auto [elevations]     = world.lock_layer(layer_elevation);


			for(auto&& [v, elevation] : range(world.mesh(), elevations)) {
				if(types[v] == Crust_type::continental) {
					if(elevation > 1)
						elevation = std::max(std::min(elevation, 50.f),
						                     elevation - elevation / max_elevation * simple_erosion * dt);
					else if(elevation < min_continental_elevation)
						elevation =
						        std::min(min_continental_elevation, elevation + sediment_accretion * 2.f * dt);

				} else if(elevation > ocean_elevation) {
					const auto age       = now - created[v];
					const auto dampening = age < sink_duration ? ridge_dampening : oceanic_dampening;
					const auto delta     = std::max(0.f, (1.f - elevation / min_elevation) * dampening * dt);
					elevation            = std::max(ocean_elevation, elevation - delta);

				} else {
					elevation = std::min(ocean_elevation, elevation + sediment_accretion * dt);
				}
			}
		}
	} // namespace

	extern const auto simple_erosion = to_module_info<execute, execute>("tectonics_simple_erosion");

} // namespace yggdrasill::modules::tectonics

YGDL_REGISTER_PLUGIN_MODULES(&yggdrasill::modules::tectonics::simple_erosion)
