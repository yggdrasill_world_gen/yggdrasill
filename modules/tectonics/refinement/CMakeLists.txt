cmake_minimum_required(VERSION 3.14 FATAL_ERROR)

yggdrasill_def_module(
	NAME yggdrasill_module_tectonics_refinement
	SOURCE refinement.cpp
	INCLUDE_DIRECTORIES "${CMAKE_CURRENT_SOURCE_DIR}/../shared"
)
