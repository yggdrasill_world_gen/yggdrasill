#include <shared_tectonics_layer.hpp>
#include <shared_tectonics_util.hpp>

#include <yggdrasill/layer.hpp>
#include <yggdrasill/mesh.hpp>
#include <yggdrasill/mesh_utils.hpp>
#include <yggdrasill/module.hpp>

#include <unordered_set>

namespace yggdrasill::modules::tectonics {
	namespace {
		struct Refinement {
			Delaunay_restorer delaunay_restorer;


			void update_density(World_view  world,
			                    const float elevation_diff_min,
			                    const float elevation_diff_max,
			                    const float neighbor_weight,
			                    const float history_weight)
			{
				const auto& mesh = world.mesh();
				auto [densities] = world.lock_layer(layer_density);

				auto elevations = world.required_layer(layer_elevation);
				auto boundaries = world.layer(layer_p_boundaries);

				for(const auto [vertex, density, elevation] : range(mesh, densities, elevations)) {
					auto elevation_diff = 0.f;
					auto base_density   = 0.f;
					auto water_boundary = false;
					for(const auto e : vertex.edges(mesh)) {
						const auto n = e.dest(mesh);

						water_boundary |= std::signbit(elevation) != std::signbit(elevations[n]);
						elevation_diff = std::max(elevation_diff, std::abs(elevation - elevations[n]));
						base_density   = std::max(base_density, [&] {
                            if(!boundaries)
                                return 0.5f;
                            switch((*boundaries)[e]) {
                                case Boundary_type::transform: return 0.5f;
                                case Boundary_type::subducting_origin:
                                case Boundary_type::subducting_dest: return 0.5f;
                                case Boundary_type::collision: return 1.0f;
                                case Boundary_type::joined: return 0.f;
                                case Boundary_type::ridge: return 1.f;
                            }
                            return 0.f;
                        }());
					}

					const auto elevation_factor =
					        water_boundary && elevation_diff > elevation_diff_min
					                ? 0.6f
					                : std::min(0.6f,
					                           (elevation_diff - elevation_diff_min)
					                                   / (elevation_diff_max - elevation_diff_min));

					auto new_density = base_density + elevation_factor;
					if(!water_boundary && elevation < -100.f)
						new_density *= 0.5f; // reduce water details

					new_density = std::clamp(new_density, 0.f, 1.f);
					if(density > 0)
						new_density = std::max(new_density, std::lerp(new_density, density, history_weight));

					density = new_density;
				}

				// blend with the neighborhood
				for(const auto [vertex, density, elevation] : range(mesh, densities, elevations)) {
					auto neighbor_count       = 0;
					auto neighbor_density_sum = 0.f;
					for(const auto n : vertex.neighbors(mesh)) {
						neighbor_count++;
						neighbor_density_sum += densities[n];
					}

					if(neighbor_count > 0)
						density = std::lerp(density, (neighbor_density_sum / neighbor_count), neighbor_weight);
				}
			}

			void incremental_remeshing(World_view  world,
			                           const float radius,
			                           const int   iterations,
			                           const float continental_distance_min,
			                           const float continental_distance_max,
			                           const float oceanic_distance_min,
			                           const float oceanic_distance_max,
			                           const float normalize_short_length,
			                           const float normalize_long_length,
			                           const float force_normalize_length)
			{
				auto [mesh] = world.lock_mesh();

				// lock layers read-write, so our references won't be affected by delayed copy-on-write
				auto [ids,
				      types,
				      creation_times,
				      velocities,
				      elevations,
				      positions,
				      densities,
				      distances_p,
				      boundaries_p,
				      distances_d,
				      boundaries_d] = world.lock_layer(layer_plate_id,
				                                       layer_crust_type,
				                                       layer_created,
				                                       layer_velocity,
				                                       layer_elevation,
				                                       layer_position,
				                                       layer_density,
				                                       layer_p_distance,
				                                       layer_p_boundaries,
				                                       layer_d_distance,
				                                       layer_d_boundaries);

				const auto max_dist = std::max(continental_distance_max, oceanic_distance_max);

				const auto calc_target_dist = [&](const auto elevation, const auto density) {
					return elevation > -500.f
					               ? std::lerp(continental_distance_max, continental_distance_min, density)
					               : std::lerp(oceanic_distance_max, oceanic_distance_min, density);
				};
				auto collapse = [=](const Vertex      v1,
				                    const Vertex      v2,
				                    const Primal_edge e,
				                    const float       high = std::numeric_limits<float>::max()) {
					const auto d = high < max_dist ? high : max_dist;
					return try_collapse_subducting(
					        mesh,
					        ids,
					        types,
					        creation_times,
					        velocities,
					        positions,
					        elevations,
					        boundaries_p,
					        v1,
					        v2,
					        e,
					        [&](const Vertex v, const Vec3& v_position, const Vec3& new_position) {
						        return dist2(v_position * radius, new_position * radius) <= d * d;
					        });
				};
				auto collapse_same_plate = [=](const Vertex      v1,
				                               const Vertex      v2,
				                               const Primal_edge e,
				                               const float       high = std::numeric_limits<float>::max()) {
					if(ids[v1] != ids[v2])
						return false;

					return collapse(v1, v2, e, high);
				};

				const auto increase_resolution = [&] {
					foreach_edge_without_new(mesh, [&](const Primal_edge e) {
						const auto src = e.origin(mesh);
						const auto dst = e.dest(mesh);

						if(ids[src] != ids[dst])
							return; // ignore boundaries

						const auto position_src = positions[src] * radius;
						const auto position_dst = positions[dst] * radius;

						const auto elevation_src = elevations[src];
						const auto elevation_dst = elevations[dst];

						const auto distance_2  = dist2(position_src, position_dst);
						const auto target_dist = std::min(calc_target_dist(elevation_src, densities[src]),
						                                  calc_target_dist(elevation_dst, densities[dst]));


						const auto low  = 0.5f * target_dist;
						const auto high = 2.f * target_dist;

						if(distance_2 > high * high) { // split long edges
							const auto t = 0.5f;

							const auto left_p  = positions[e.origin_next(mesh).dest(mesh)] * radius;
							const auto right_p = positions[e.origin_prev(mesh).dest(mesh)] * radius;

							const auto new_p = normalized(lerp(position_src, position_dst, t)) * radius;

							if(dist2(left_p, new_p) > low * low && dist2(right_p, new_p) > low * low) {
								if(const auto new_edge = mesh.try_split(e, t); new_edge != no_edge) {
									if(types[src] == Crust_type::continental
									   || types[dst] == Crust_type::continental) {
										types[new_edge.origin(mesh)] =
										        std::lerp(elevation_src, elevation_dst, t) < 0.f
										                ? Crust_type::oceanic
										                : Crust_type::continental;
									}
								}
							}
						}
					});
				};

				const auto decrease_resolution = [&] {
					foreach_edge_without_new(mesh, [&](const auto e) {
						const auto src = e.origin(mesh);
						const auto dst = e.dest(mesh);

						if(ids[src] != ids[dst])
							return; // ignore boundaries

						const auto position_src = positions[src] * radius;
						const auto position_dst = positions[dst] * radius;

						const auto distance2   = dist2(position_src, position_dst);
						const auto target_dist = std::min(calc_target_dist(elevations[src], densities[src]),
						                                  calc_target_dist(elevations[dst], densities[dst]));


						const auto low  = 0.5f * target_dist;
						const auto high = 2.f * target_dist;

						if(distance2 < low * low) { // collapse short edges
							try_any_collapse<Float_wrapper{0.5f},
							                 Float_wrapper{0.7f},
							                 Float_wrapper{0.3f},
							                 Float_wrapper{0.f},
							                 Float_wrapper{1.0f}>(
							        mesh,
							        e,
							        positions,
							        [&](const Vertex v, const Vec3& v_position, const Vec3& new_position) {
								        return dist2(v_position * radius, new_position * radius) <= high * high;
							        });
						}
					});
				};


				auto flip = flip_edge(mesh, positions, distances_p, boundaries_p, distances_d, boundaries_d);
				auto solve_long_edge = [&](const Vertex      v1,
				                           const Vertex      v2,
				                           const Primal_edge e,
				                           const Vertex      opposite,
				                           const float       edge_len,
				                           const Primal_edge other_edge_1,
				                           const Primal_edge other_edge_2,
				                           const float       other_edge_len_percent_1,
				                           const float       other_edge_len_percent_2) {
					if(other_edge_len_percent_1 < 0.1f
					   && collapse(other_edge_1.origin(mesh), other_edge_1.dest(mesh), other_edge_1))
						return true;

					if(other_edge_len_percent_2 < 0.1f
					   && collapse(other_edge_2.origin(mesh), other_edge_2.dest(mesh), other_edge_2))
						return true;

					if((ids[v1] != ids[v2] || ids[v1] != ids[opposite]) && edge_len < force_normalize_length)
						return false;

					if(!flip(e)) {
						return false;
					}

					// move opposite to the point directly between v1 and v2 if that wouldn't invert one of its faces
					const auto new_position = normalized(positions[v1] + positions[v2]);
					if(can_move_vertex(mesh, opposite, positions, new_position)) {
						positions[opposite] = new_position;
					}

					return true;
				};

				const auto remove_skinny_faces = [&] {
					for(const auto f : mesh.faces(concurrent_modifications)) {
						const auto ab    = f.edge(mesh);
						const auto a     = ab.origin(mesh);
						const auto a_pos = positions[a];

						const auto bc    = ab.left_next(mesh);
						const auto b     = bc.origin(mesh);
						const auto b_pos = positions[b];

						const auto ca    = bc.left_next(mesh);
						const auto c     = ca.origin(mesh);
						const auto c_pos = positions[c];

						const auto ab_len = length(a_pos - b_pos);
						const auto bc_len = length(b_pos - c_pos);
						const auto ca_len = length(c_pos - a_pos);

						// check if one side is significantly shorter and try to collapse that edge
						const auto max_len   = std::max(ab_len, std::max(bc_len, ca_len));
						const auto short_len = max_len * normalize_short_length;
						if(ab_len < short_len)
							collapse_same_plate(a, b, ab);
						else if(bc_len < short_len)
							collapse_same_plate(b, c, bc);
						else if(ca_len < short_len)
							collapse_same_plate(c, a, ca);

						else {
							// check if the triangles height is close to zero
							if(ab_len > (bc_len + ca_len) * normalize_long_length)
								solve_long_edge(a, b, ab, c, ab_len, bc, ca, bc_len / ab_len, ca_len / ab_len);
							else if(bc_len > (ab_len + ca_len) * normalize_long_length)
								solve_long_edge(b, c, bc, a, bc_len, ca, ab, ca_len / bc_len, ab_len / bc_len);
							else if(ca_len > (ab_len + bc_len) * normalize_long_length)
								solve_long_edge(c, a, ca, b, ca_len, ab, bc, ab_len / ca_len, bc_len / ca_len);
						}
					}
				};

				const auto restore_delaunay = [&] {
					delaunay_restorer.restore(
					        mesh, positions, types, ids, distances_p, boundaries_p, distances_d, boundaries_d, 128);
				};

				restore_delaunay();
				remove_skinny_faces();
				restore_delaunay();

				// TODO[optimization]: early-out if no changes were made
				for(int i = 0; i < iterations; ++i) {
					decrease_resolution();
					increase_resolution();
					remove_skinny_faces();
					restore_delaunay();
				}

				remove_skinny_faces();
				restore_delaunay();
			}

			void execute(World_view world, yggdrasill::Any_dictionary auto cfg)
			{
				const auto iterations    = cfg["refine"]["iterations"].get(128, {.min = 1});
				const auto elevation_min = cfg["refine"]["elevation_min"].get(200.f, {.min = 1.f});
				const auto elevation_max = cfg["refine"]["elevation_max"].get(2000.f, {.min = elevation_min});
				const auto neighbor_weight =
				        cfg["refine"]["neighbor_weight"].get(0.4f, {.min = 0.f, .max = 0.99f});
				const auto history_weight =
				        cfg["refine"]["history_weight"].get(0.6f, {.min = 0.f, .max = 0.99f});

				const auto continental_distance_min =
				        cfg["refine"]["continent"]["min_dist"].get(80'000.f, {.min = 1.f});
				const auto continental_distance_max =
				        cfg["refine"]["continent"]["max_dist"].get(600'000.f, {.min = continental_distance_min});
				const auto oceanic_distance_min =
				        cfg["refine"]["ocean"]["min_dist"].get(160'000.f, {.min = 1.f});
				const auto oceanic_distance_max =
				        cfg["refine"]["ocean"]["max_dist"].get(1'200'000.f, {.min = oceanic_distance_min});

				const auto normalize_short_length = cfg["refine"]["short_length"].get(0.05f);
				const auto normalize_long_length  = cfg["refine"]["long_length"].get(0.98f);

				// edges longer than this (on the unit sphere) are subjected to the remove_skinny_faces step,
				//   even if their vertices belong to different plates
				const auto force_normalize_length = cfg["refine"]["force_normalize_length"].get(0.2f);

				if(!world)
					return;

				const auto radius = world.required_unstructured_layer("meta")["radius"].get<float>();

				update_density(world, elevation_min, elevation_max, neighbor_weight, history_weight);

				incremental_remeshing(world,
				                      radius,
				                      iterations,
				                      continental_distance_min,
				                      continental_distance_max,
				                      oceanic_distance_min,
				                      oceanic_distance_max,
				                      normalize_short_length,
				                      normalize_long_length,
				                      force_normalize_length);
			}
		};
	} // namespace

	extern const auto refinement = to_module_info<Refinement>("tectonics_refinement");

} // namespace yggdrasill::modules::tectonics

YGDL_REGISTER_PLUGIN_MODULES(&yggdrasill::modules::tectonics::refinement)
