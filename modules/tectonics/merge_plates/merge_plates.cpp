#include <shared_tectonics_layer.hpp>

#include <yggdrasill/layer.hpp>
#include <yggdrasill/mesh.hpp>
#include <yggdrasill/mesh_utils.hpp>
#include <yggdrasill/module.hpp>

#include <algorithm>
#include <cmath>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

namespace yggdrasill::modules::tectonics {
	namespace {
		struct Plate_join : std::pair<Plate_id, Plate_id> {
			using pair::pair;
		};
	} // namespace
} // namespace yggdrasill::modules::tectonics

template <>
struct std::hash<yggdrasill::modules::tectonics::Plate_join> {
	size_t operator()(const yggdrasill::modules::tectonics::Plate_join& v) const
	{
		return v.first ^ v.second;
	}
};

namespace yggdrasill::modules::tectonics {
	namespace {
		struct Join_request {
			std::vector<Vertex> vertices_a;
			std::vector<Vertex> vertices_b;
		};

		struct Merge_plates {
			std::unordered_map<Plate_id, int>            plate_boundary_size;
			std::unordered_map<Plate_join, Join_request> join_requests;
			std::unordered_set<Vertex>                   seen_vertices;

			void collect_join_requests(const Const_world_view world,
			                           const Const_mesh_view  mesh,
			                           const float            join_distance,
			                           const float            force_join_distance,
			                           const float            max_converging_velocity,
			                           const float            max_diverging_velocity,
			                           const int              min_join_points,
			                           const float            min_join_point_percentage)
			{
				join_requests.clear();
				plate_boundary_size.clear();

				const auto  join_distance_2       = join_distance * join_distance;
				const float force_join_distance_2 = force_join_distance * force_join_distance;

				const auto radius = world.required_unstructured_layer("meta")["radius"].get<float>();

				auto&& [ids, types, boundaries, positions, velocities] = world.required_layer(
				        layer_plate_id, layer_crust_type, layer_p_boundaries, layer_position, layer_velocity);

				// check each collision boundary if the plates should be joined
				for(const auto [e, boundary] : range(mesh, boundaries)) {
					if(boundary != Boundary_type::collision && boundary != Boundary_type::transform)
						continue;

					const auto origin = e.origin(mesh);
					const auto dest   = e.dest(mesh);

					if(types[origin] != Crust_type::continental || types[dest] != Crust_type::continental)
						continue;

					const auto id_origin = ids[origin];
					const auto id_dest   = ids[dest];

					plate_boundary_size[id_origin]++;
					plate_boundary_size[id_dest]++;

					const auto diff   = (positions[dest] - positions[origin]) * radius;
					const auto dist_2 = length2(diff);

					if(dist_2 > join_distance_2)
						continue;

					if(dist_2 > force_join_distance_2) {
						const auto dist = std::sqrt(dist_2);
						const auto converging_velocity = -dot(velocities[dest] - velocities[origin], diff / dist);

						if(converging_velocity > max_converging_velocity
						   || converging_velocity < -max_diverging_velocity)
							continue;
					}

					// count edge and remember the vertex that belongs to the plate with the larger id (starting point for flood-fill)
					if(id_origin < id_dest) {
						auto& req = join_requests[Plate_join{id_origin, id_dest}];
						req.vertices_a.emplace_back(origin);
						req.vertices_b.emplace_back(dest);
					} else {
						auto& req = join_requests[Plate_join{id_dest, id_origin}];
						req.vertices_a.emplace_back(dest);
						req.vertices_b.emplace_back(origin);
					}
				}

				// remove joins that weren't requested by enough boundaries
				std::erase_if(join_requests, [&](const auto& e) {
					const auto& [plates, join_points] = e;
					const auto join_count             = static_cast<int>(join_points.vertices_a.size());

					const auto min =
					        std::max(min_join_points,
					                 static_cast<int>(std::floor(std::min(plate_boundary_size[plates.first],
					                                                      plate_boundary_size[plates.second])
					                                             * min_join_point_percentage)));
					return join_count < min;
				});
			}

			void execute(World_view world, yggdrasill::Any_dictionary auto cfg)
			{
				const auto join_distance =
				        cfg["plate_sim"]["merge"]["join_distance"].get(400'000.f, {.min = 1.f});
				const auto force_join_distance =
				        cfg["plate_sim"]["merge"]["force_join_distance"].get(70'000.f, {.min = 1.f});
				const auto min_join_points = cfg["plate_sim"]["merge"]["min_points"].get(16, {.min = 1});
				const auto min_join_point_percentage =
				        cfg["plate_sim"]["merge"]["min_percentage"].get(0.7f, {.min = 0.f, .max = 1.f});

				const auto max_converging_velocity =
				        cfg["plate_sim"]["merge"]["max_converging_velocity"].get(0.005f);
				const auto max_diverging_velocity = cfg["plate_sim"]["merge"]["max_diverging_velocity"].get(0.1f);

				if(!world)
					return;

				auto mesh = world.mesh();

				// determine, which plates should be merged
				collect_join_requests(world,
				                      mesh,
				                      join_distance,
				                      force_join_distance,
				                      max_converging_velocity,
				                      max_diverging_velocity,
				                      min_join_points,
				                      min_join_point_percentage);
				if(join_requests.empty())
					return;


				auto [ids]       = world.lock_layer(layer_plate_id);
				const auto types = world.required_layer(layer_crust_type);

				// build function to determine next free plate ids
				auto get_next_id = [&] {
					auto used_ids = std::unordered_set<Plate_id>();
					auto next_id  = Plate_id(1);
					for(auto id : ids) {
						used_ids.insert(id);
						next_id = std::max(next_id, id + 1);
					}

					auto free_ids = std::vector<Plate_id>();
					for(auto i = next_id - 1; i >= Plate_id(1); i--) {
						if(!used_ids.contains(i))
							free_ids.push_back(i);
					}

					return [next_id = std::move(next_id), free_ids = std::move(free_ids)]() mutable {
						if(free_ids.empty())
							return next_id++;

						auto id = free_ids.back();
						free_ids.pop_back();
						return id;
					};
				}();

				// function that reassigned all parts of the old plate, that are connected to the given vertices
				auto reassign_plate = [&](const Plate_id old_id, const Plate_id new_id, std::vector<Vertex>&& seeds) {
					if(old_id != new_id) {
						flood_fill(mesh, std::move(seeds), Neighbor_type::just_primal_edges, [&](Vertex n) {
							auto& id = ids[n];
							if(id == old_id && types[n] == Crust_type::continental) {
								id = new_id;
								return true;
							} else {
								return false;
							}
						});
					}
				};

				// iterate over all to-be-joined plates and use flood-fill to change their plate-ids
				for(auto& [plates, req] : join_requests) {
					if(req.vertices_a.empty() || req.vertices_b.empty())
						continue;

					const auto a_id   = ids[req.vertices_a[0]];
					const auto b_id   = ids[req.vertices_b[0]];
					const auto new_id = get_next_id();

					std::cout << "join " << a_id << " + " << b_id << " => " << new_id << "\n";

					// re-assign the connected parts of both plates
					reassign_plate(a_id, new_id, std::move(req.vertices_a));
					reassign_plate(b_id, new_id, std::move(req.vertices_b));
				}
			}
		};
	} // namespace

	/// merge tectonic plates at collision boundaries (aka suturing)
	extern const auto merge_plates = to_module_info<Merge_plates>("tectonics_merge_plates");

} // namespace yggdrasill::modules::tectonics

YGDL_REGISTER_PLUGIN_MODULES(&yggdrasill::modules::tectonics::merge_plates)
