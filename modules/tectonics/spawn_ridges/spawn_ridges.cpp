#include <shared_tectonics_layer.hpp>
#include <shared_tectonics_util.hpp>

#include <yggdrasill/mesh.hpp>
#include <yggdrasill/module.hpp>

#include <algorithm>
#include <cmath>
#include <unordered_set>


namespace yggdrasill::modules::tectonics {

	namespace {
		struct Spawn_ridges {
			std::unordered_set<Primal_edge> seen_edges;

			void execute(World_view world, yggdrasill::Any_dictionary auto args)
			{
				const auto max_velocity = args["plate"]["max_velocity"].get(0.1f, {.min = 0.001f});

				const auto ridge_elevation_min =
				        args["ridge"]["elevation"]["min"].get(-2'000.f, {.min = -10'000.f});
				const auto ridge_elevation_max =
				        args["ridge"]["elevation"]["max"].get(-4'000.f, {.min = -10'000.f});

				const auto ridge_width          = args["ridge"]["width"].get(1'000'000.f);
				const auto ridge_spawn_distance = args["ridge"]["spawn_distance"].get(150'000.f);
				const auto split_distance_ridge = args["ridge"]["max_distance"].get(400'000.f, {.min = 0.001f});
				const auto force_split_distance_ridge =
				        args["ridge"]["force_split_distance"].get(1'000'000.f, {.min = 0.001f});

				const auto continental_crust_formation_min =
				        args["ridge"]["continental_crust_formation_min"].get(-100.f);

				const auto continental_crust_formation_max =
				        args["ridge"]["continental_crust_formation_max"].get(1'000.f);

				if(!world)
					return;


				auto       meta_data = world.required_unstructured_layer("meta");
				const auto radius    = meta_data["radius"].get<float>();
				const auto now       = meta_data["age"].get<float>();

				auto [ids,
				      types,
				      velocities,
				      elevations,
				      positions,
				      boundary_types,
				      distances_primal,
				      distances_dual,
				      creation_times] = world.lock_layer(layer_plate_id,
				                                         layer_crust_type,
				                                         layer_velocity,
				                                         layer_elevation,
				                                         layer_position,
				                                         layer_p_boundaries,
				                                         layer_p_distance,
				                                         layer_d_distance,
				                                         layer_created);

				auto [mesh] = world.lock_mesh();

				seen_edges.clear();

				// check each edge if the vertices are too far
				foreach_edge_without_new(mesh, [&](const Primal_edge e) {
					if(boundary_types[e] == Boundary_type::joined)
						return;

					const auto src = e.origin(mesh);
					const auto dst = e.dest(mesh);

					const auto src_pos   = positions[src];
					const auto dst_pos   = positions[dst];
					const auto diff      = (dst_pos - src_pos) * radius;
					const auto distance2 = length2(diff);

					if(boundary_types[e] != Boundary_type::ridge
					   && distance2 < force_split_distance_ridge * force_split_distance_ridge)
						return;

					if(distance2 < split_distance_ridge * split_distance_ridge
					   || !seen_edges.insert(e.base()).second)
						return;

					const auto src_velocity        = velocities[src];
					const auto dst_velocity        = velocities[dst];
					const auto distance            = std::sqrt(distance2);
					const auto converging_velocity = -dot(dst_velocity - src_velocity, diff / distance);

					const auto src_elevation = elevations[src];
					const auto dst_elevation = elevations[dst];
					const auto src_id        = ids[src];
					const auto dst_id        = ids[dst];

					const auto ridge_height =
					        std::lerp(ridge_elevation_max,
					                  ridge_elevation_min,
					                  std::clamp(-converging_velocity / max_velocity, 0.f, 1.f));

					// calculate split-point percentage for the two new vertices,
					//   preferably ridge_spawn_distance from their center.
					const auto alpha_dst =
					        std::clamp(ridge_spawn_distance / (distance / 2.f), 0.2f, 0.8f) * 0.5f + 0.5f;
					const auto alpha_src = (1.f - alpha_dst) / alpha_dst;

					const auto elevation_alpha =
					        smootherstep((distance * alpha_dst - distance / 2.f) / ridge_width);

					// split weights are required with these values to interpolate other layers correctly (near src/dest node).
					const auto e_near_dst_to_dst = mesh.try_split(e, alpha_dst);
					if(e_near_dst_to_dst == no_edge)
						return;
					const auto near_dst = e_near_dst_to_dst.origin(mesh);
					const auto e_new    = mesh.try_split(e, alpha_src); // edge between the two new vertices
					if(e_new == no_edge) {
						std::cerr << "Second edge-split in spawn_plates_at_ridges() failed.\n";
						return;
					}
					const auto near_src = e_new.origin(mesh);

					velocities[near_src] = src_velocity;
					velocities[near_dst] = dst_velocity;

					ids[near_src] = src_id;
					ids[near_dst] = dst_id;

					creation_times[near_src] = now;
					creation_times[near_dst] = now;

					elevations[near_src] = std::lerp(ridge_height, src_elevation, elevation_alpha);
					elevations[near_dst] = std::lerp(ridge_height, dst_elevation, elevation_alpha);

					// limit the initial splits elevation, to reduce the number of visible failed-rifts
					if(types[src] == Crust_type::continental && types[dst] == Crust_type::continental) {
						if(src_elevation > continental_crust_formation_max && elevations[near_src] < 0)
							elevations[near_src] = 0;
						if(dst_elevation > continental_crust_formation_max && elevations[near_dst] < 0)
							elevations[near_dst] = 0;
					} else {
						elevations[near_src] = std::min(elevations[near_src], continental_crust_formation_min);
						elevations[near_dst] = std::min(elevations[near_dst], continental_crust_formation_min);
					}

					types[near_src] = elevations[near_src] <= continental_crust_formation_min
					                          ? Crust_type::oceanic
					                          : Crust_type::continental;
					types[near_dst] = elevations[near_dst] <= continental_crust_formation_min
					                          ? Crust_type::oceanic
					                          : Crust_type::continental;

					boundary_types[e]                 = Boundary_type::joined;
					boundary_types[e_near_dst_to_dst] = Boundary_type::joined;
					boundary_types[e_new]             = Boundary_type::ridge;

					// ignore new edges for future splits in this iteration
					const auto dst_edges = near_dst.edges(mesh);
					seen_edges.insert(dst_edges.begin(), dst_edges.end_iterator());
					const auto src_edges = near_src.edges(mesh);
					seen_edges.insert(src_edges.begin(), src_edges.end_iterator());
				});
			}
		};
	} // namespace

	extern const auto spawn_ridges = to_module_info<Spawn_ridges>("tectonics_spawn_ridges");

} // namespace yggdrasill::modules::tectonics

YGDL_REGISTER_PLUGIN_MODULES(&yggdrasill::modules::tectonics::spawn_ridges)
